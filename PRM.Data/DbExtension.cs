﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRM.Data
{
    public  static class DbExtension
    {
        public static void Invoke()
        {
            string database = ConfigurationManager.AppSettings["DatabaseProvider"].ToString();
            if (database.Equals("MSSQL", StringComparison.InvariantCultureIgnoreCase)) {
                SimpleCRUD.SetDialect(SimpleCRUD.Dialect.SQLServer);
            } else {
                SimpleCRUD.SetDialect(SimpleCRUD.Dialect.MySQL);
            }
        }
    }
}
