﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace PRM.Core.Models
{
    [DataContract]
    public class CredentialUpload
    {
        [DataMember(Name = "fileStream")]
        public byte[] FileStream { get; set; }

        [DataMember(Name = "fileType")]
        public string FileType { get; set; }

        [DataMember(Name = "isVerified")]
        public int IsVerified { get; set; }

        [DataMember(Name = "credentialID")]
        public string CredentialID { get; set; }

        [DataMember(Name = "phone")]
        public string Phone { get; set; }
    }
}