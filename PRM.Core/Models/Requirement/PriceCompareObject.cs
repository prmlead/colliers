﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace PRM.Core.Models
{
    [DataContract]
    public class PriceCompareObject
    {
        [DataMember(Name = "itemID")]
        public int ItemID { get; set; }

        [DataMember(Name = "minPrice")]
        public double MinPrice { get; set; }

        [DataMember(Name = "minPriceBidder")]
        public int MinPriceBidder { get; set; }

        [DataMember(Name = "leastBidderPrice")]
        public double LeastBidderPrice { get; set; }

        string vendorName = string.Empty;
        [DataMember(Name = "vendorName")]
        public string VendorName
        {
            get
            {
                if (!string.IsNullOrEmpty(vendorName))
                { return vendorName; }
                else
                { return ""; }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                { vendorName = value; }
            }
        }

        string productName = string.Empty;
        [DataMember(Name = "productName")]
        public string ProductName
        {
            get
            {
                if (!string.IsNullOrEmpty(productName))
                { return productName; }
                else
                { return ""; }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                { productName = value; }
            }
        }

        string productDescription = string.Empty;
        [DataMember(Name = "productDescription")]
        public string ProductDescription
        {
            get
            {
                if (!string.IsNullOrEmpty(productDescription))
                { return productDescription; }
                else
                { return ""; }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                { productDescription = value; }
            }
        }






        [DataMember(Name = "quotationID")]
        public int QuotationID { get; set; }

        [DataMember(Name = "userID")]
        public int UserID { get; set; }

        string brandL1 = string.Empty;
        [DataMember(Name = "brandL1")]
        public string BrandL1
        {
            get
            {
                if (!string.IsNullOrEmpty(brandL1))
                { return brandL1; }
                else
                { return ""; }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                { brandL1 = value; }
            }
        }

        string durationL1 = string.Empty;
        [DataMember(Name = "durationL1")]
        public string DurationL1
        {
            get
            {
                if (!string.IsNullOrEmpty(durationL1))
                { return durationL1; }
                else
                { return ""; }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                { durationL1 = value; }
            }
        }

        string paymentL1 = string.Empty;
        [DataMember(Name = "paymentL1")]
        public string PaymentL1
        {
            get
            {
                if (!string.IsNullOrEmpty(paymentL1))
                { return paymentL1; }
                else
                { return ""; }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                { paymentL1 = value; }
            }
        }


        string brand = string.Empty;
        [DataMember(Name = "brand")]
        public string Brand
        {
            get
            {
                if (!string.IsNullOrEmpty(brand))
                { return brand; }
                else
                { return ""; }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                { brand = value; }
            }
        }

        string duration = string.Empty;
        [DataMember(Name = "duration")]
        public string Duration
        {
            get
            {
                if (!string.IsNullOrEmpty(duration))
                { return duration; }
                else
                { return ""; }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                { duration = value; }
            }
        }

        string payment = string.Empty;
        [DataMember(Name = "payment")]
        public string Payment
        {
            get
            {
                if (!string.IsNullOrEmpty(payment))
                { return payment; }
                else
                { return ""; }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                { payment = value; }
            }
        }

        [DataMember(Name = "minUnitPrice")]
        public double MinUnitPrice { get; set; }

        [DataMember(Name = "leastBidderUnitPrice")]
        public double LeastBidderUnitPrice { get; set; }



        string otherProperties = string.Empty;
        [DataMember(Name = "otherProperties")]
        public string OtherProperties
        {
            get
            {
                if (!string.IsNullOrEmpty(otherProperties))
                { return otherProperties; }
                else
                { return ""; }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                { otherProperties = value; }
            }
        }


        string otherPropertiesL1 = string.Empty;
        [DataMember(Name = "otherPropertiesL1")]
        public string OtherPropertiesL1
        {
            get
            {
                if (!string.IsNullOrEmpty(otherPropertiesL1))
                { return otherPropertiesL1; }
                else
                { return ""; }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                { otherPropertiesL1 = value; }
            }
        }



        [DataMember(Name = "quantity")]
        public double Quantity { get; set; }


        string units = string.Empty;
        [DataMember(Name = "units")]
        public string Units
        {
            get
            {
                if (!string.IsNullOrEmpty(units))
                { return units; }
                else
                { return ""; }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                { units = value; }
            }
        }


    }
}