﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace PRM.Core.Models
{
    [DataContract]
    public class AppInfo : Response
    {
        [DataMember(Name = "appName")]
        public string AppName { get; set; }

        [DataMember(Name = "appversion")]
        public string AppVersion { get; set; }

        [DataMember(Name = "appVersionDetails")]
        public string AppVersionDetails { get; set; }

        [DataMember(Name = "isMandatory")]
        public int IsMandatory { get; set; }

        [DataMember(Name = "releaseDate")]
        public DateTime ReleaseDate { get; set; }

        [DataMember(Name = "appversionCode")]
        public int AppVersionCode { get; set; }


        [DataMember(Name = "endUser")]
        public string EndUser { get; set; }
    }
}