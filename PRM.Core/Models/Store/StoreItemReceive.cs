﻿using System;
using System.Runtime.Serialization;

namespace PRM.Core.Models
{
    [DataContract]
    public class StoreItemReceive : ResponseAudit
    {
        [DataMember(Name = "grnID")]
        public int GRNID { get; set; }

        string _grnCode = string.Empty;
        [DataMember(Name = "grnCode")]
        public string GRNCode
        {
            get
            {
                return string.IsNullOrEmpty(_grnCode) ? string.Empty : _grnCode;                
            }
            set
            {
                _grnCode = value;
            }
        }        

        [DataMember(Name = "storeItems")]
        public StoreItem[] StoreItems { get; set; }

        string _indentNo = string.Empty;
        [DataMember(Name = "indentNo")]
        public string IndentNo
        {
            get
            {
                return string.IsNullOrEmpty(_indentNo) ? string.Empty : _indentNo;
            }
            set
            {
                _indentNo = value;
            }
        }

        string _poNo = string.Empty;
        [DataMember(Name = "poNo")]
        public string PONo
        {
            get
            {
                return string.IsNullOrEmpty(_poNo) ? string.Empty : _poNo;
            }
            set
            {
                _poNo = value;
            }
        }

        string _supplierName = string.Empty;
        [DataMember(Name = "supplierName")]
        public string SupplierName
        {
            get
            {
                return string.IsNullOrEmpty(_supplierName) ? string.Empty : _supplierName;
            }
            set
            {
                _supplierName = value;
            }
        }

        DateTime receiveDate = DateTime.MaxValue;
        [DataMember(Name = "receiveDate")]
        public DateTime ReceiveDate
        {
            get
            {
                return this.receiveDate;
            }
            set
            {
                this.receiveDate = value;
            }
        }

        string receiveLocation = string.Empty;
        [DataMember(Name = "receiveLocation")]
        public string ReceiveLocation
        {
            get
            {
                return string.IsNullOrEmpty(receiveLocation) ? string.Empty : receiveLocation;
            }
            set
            {
                receiveLocation = value;
            }
        }

        string costCenter = string.Empty;
        [DataMember(Name = "costCenter")]
        public string CostCenter
        {
            get
            {
                return string.IsNullOrEmpty(costCenter) ? string.Empty : costCenter;
            }
            set
            {
                costCenter = value;
            }
        }

        [DataMember(Name = "orderQty")]
        public decimal OrderQty { get; set; }

        [DataMember(Name = "receivedQty")]
        public decimal ReceivedQty { get; set; }

        string receivedBy = string.Empty;
        [DataMember(Name = "receivedBy")]
        public string ReceivedBy
        {
            get
            {
                return string.IsNullOrEmpty(receivedBy) ? string.Empty : receivedBy;
            }
            set
            {
                receivedBy = value;
            }
        }

        string checkedBy = string.Empty;
        [DataMember(Name = "checkedBy")]
        public string CheckedBy
        {
            get
            {
                return string.IsNullOrEmpty(checkedBy) ? string.Empty : checkedBy;
            }
            set
            {
                checkedBy = value;
            }
        }

        string itemCondition = string.Empty;
        [DataMember(Name = "itemCondition")]
        public string ItemCondition
        {
            get
            {
                return string.IsNullOrEmpty(itemCondition) ? string.Empty : itemCondition;
            }
            set
            {
                itemCondition = value;
            }
        }

        string comments = string.Empty;
        [DataMember(Name = "comments")]
        public string Comments
        {
            get
            {
                return string.IsNullOrEmpty(comments) ? string.Empty : comments;
            }
            set
            {
                comments = value;
            }
        }
    }
}