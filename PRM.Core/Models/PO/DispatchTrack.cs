﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace PRM.Core.Models
{
    [DataContract]
    public class DispatchTrack : ResponseAudit
    {


        [DataMember(Name = "dTID")]
        public int DTID { get; set; }

        [DataMember(Name = "poItemsEntity")]
        public List<POItems> POItemsEntity { get; set; }

        [DataMember(Name = "vendorPOObject")]
        public VendorPO VendorPOObject { get; set; }

        [DataMember(Name = "dispatchQuantity")]
        public decimal DispatchQuantity { get; set; }

        [DataMember(Name = "vendorPOQuantity")]
        public decimal VendorPOQuantity { get; set; }

        DateTime dispatchDate = DateTime.MaxValue;
        [DataMember(Name = "dispatchDate")]
        public DateTime? DispatchDate
        {
            get
            {
                return dispatchDate;
            }
            set
            {
                if (value != null && value.HasValue)
                {
                    dispatchDate = value.Value;
                }
            }
        }

        string dispatchType = string.Empty;
        [DataMember(Name = "dispatchType")]
        public string DispatchType
        {
            get
            {
                if (!string.IsNullOrEmpty(dispatchType))
                { return dispatchType; }
                else
                { return ""; }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                { dispatchType = value; }
            }
        }

        string dispatchComments = string.Empty;
        [DataMember(Name = "dispatchComments")]
        public string DispatchComments
        {
            get
            {
                if (!string.IsNullOrEmpty(dispatchComments))
                { return dispatchComments; }
                else
                { return ""; }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                { dispatchComments = value; }
            }
        }

        string dispatchCode = string.Empty;
        [DataMember(Name = "dispatchCode")]
        public string DispatchCode
        {
            get
            {
                if (!string.IsNullOrEmpty(dispatchCode))
                { return dispatchCode; }
                else
                { return ""; }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                { dispatchCode = value; }
            }
        }

        string dispatchMode = string.Empty;
        [DataMember(Name = "dispatchMode")]
        public string DispatchMode
        {
            get
            {
                if (!string.IsNullOrEmpty(dispatchMode))
                { return dispatchMode; }
                else
                { return ""; }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                { dispatchMode = value; }
            }
        }

        string deliveryTrackID = string.Empty;
        [DataMember(Name = "deliveryTrackID")]
        public string DeliveryTrackID
        {
            get
            {
                if (!string.IsNullOrEmpty(deliveryTrackID))
                { return deliveryTrackID; }
                else
                { return ""; }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                { deliveryTrackID = value; }
            }
        }

        string dispatchLink = string.Empty;
        [DataMember(Name = "dispatchLink")]
        public string DispatchLink
        {
            get
            {
                if (!string.IsNullOrEmpty(dispatchLink))
                { return dispatchLink; }
                else
                { return ""; }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                { dispatchLink = value; }
            }
        }


        [DataMember(Name = "recivedQuantity")]
        public decimal RecivedQuantity { get; set; }

        DateTime recivedDate = DateTime.MaxValue;
        [DataMember(Name = "recivedDate")]
        public DateTime? RecivedDate
        {
            get
            {
                return recivedDate;
            }
            set
            {
                if (value != null && value.HasValue)
                {
                    recivedDate = value.Value;
                }
            }
        }

        string recivedComments = string.Empty;
        [DataMember(Name = "recivedComments")]
        public string RecivedComments
        {
            get
            {
                if (!string.IsNullOrEmpty(recivedComments))
                { return recivedComments; }
                else
                { return ""; }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                { recivedComments = value; }
            }
        }

        string recivedCode = string.Empty;
        [DataMember(Name = "recivedCode")]
        public string RecivedCode
        {
            get
            {
                if (!string.IsNullOrEmpty(recivedCode))
                { return recivedCode; }
                else
                { return ""; }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                { recivedCode = value; }
            }
        }

        [DataMember(Name = "recivedLink")]
        public int RecivedLink { get; set; }

        string recivedBy = string.Empty;
        [DataMember(Name = "recivedBy")]
        public string RecivedBy
        {
            get
            {
                if (!string.IsNullOrEmpty(recivedBy))
                { return recivedBy; }
                else
                { return ""; }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                { recivedBy = value; }
            }
        }

        string status = string.Empty;
        [DataMember(Name = "status")]
        public string Status
        {
            get
            {
                if (!string.IsNullOrEmpty(status))
                { return status; }
                else
                { return ""; }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                { status = value; }
            }
        }

        [DataMember(Name = "poFile")]
        public FileUpload POFile { get; set; }

        [DataMember(Name = "poID")]
        public int POID { get; set; }
        
        string indentID = string.Empty;
        [DataMember(Name = "indentID")]
        public string IndentID
        {
            get
            {
                if (!string.IsNullOrEmpty(indentID))
                { return indentID; }
                else
                { return ""; }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                { indentID = value; }
            }
        }
        

        string purchaseID = string.Empty;
        [DataMember(Name = "purchaseID")]
        public string PurchaseID
        {
            get
            {
                if (!string.IsNullOrEmpty(purchaseID))
                { return purchaseID; }
                else
                { return ""; }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                { purchaseID = value; }
            }
        }

        [DataMember(Name = "returnQuantity")]
        public decimal ReturnQuantity { get; set; }


        [DataMember(Name = "receivedQtyPrice")]
        public decimal ReceivedQtyPrice { get; set; }

        [DataMember(Name = "paymentPrice")]
        public decimal PaymentPrice { get; set; }


    }
}