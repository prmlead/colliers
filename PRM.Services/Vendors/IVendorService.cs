﻿using PRM.Core.Domain.Vendors;
using System.Collections.Generic;

namespace PRM.Services.Vendors
{
    public interface IVendorService
    {

        void InsertVendor(Vendor entity);

        void UpdateVendor(Vendor entity);

        void DeleteVendor(Vendor entity);

        Vendor GetVendorById(int Id);

        IList<Vendor> GetVendorList(object filter);

        int CalculateScore(int companyId, int vendorId);

        IList<Dictionary<string, object>> GetVendorScore(int companyId, int vendorId);

        void SaveVendorScore(int vendorId, List<VendorScore> scores);

        void UpdateVedorSubCategory(int vendorId, int companyId, int[] subCategories);

        int[] GetVendorSubCategort(int vendorId);

        void UpdateVedorCategory(int vendorId, string categoryName);

    }
}