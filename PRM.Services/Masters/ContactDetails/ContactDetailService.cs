﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PRM.Core.Domain.Masters.ContactDetails;
using PRM.Core.Pagers;
using PRM.Data;
using Dapper;
using PRM.Core.Domain.Acl;
using PRM.Services.Acl;

namespace PRM.Services.Masters.ContactDetails
{
    public class ContactDetailService : IContactDetailService
    {

        public ContactDetailService(IRepository<ContactDetail> contactDetailRep,
          IDbProvider dbProvider, IAclMappingService aclMappingService)
        {
            _contactDetailRep = contactDetailRep;
            _dbProvider = dbProvider;
            _aclMappingService = aclMappingService;
        }

        private readonly IRepository<ContactDetail> _contactDetailRep;
        private readonly IDbProvider _dbProvider;
        private readonly IAclMappingService _aclMappingService;
        public void DeleteContactDetail(ContactDetail entity)
        {
            _contactDetailRep.Delete(entity);
        }

        public ContactDetail GetContactDetailById(int id)
        {

            var entity = _contactDetailRep.GetById(id);

            var mapper = _aclMappingService.GetAclMappingList(new { EntityName = "ContactDetail", EntityId = entity.Id });
            if (entity.LimitedToDepartment)
                entity.AllowedDepartments = mapper.Where(e => e.Type == (int)AclMappingType.Department).Select(e => e.Value).ToArray();

            if (entity.LimitedToProject)
                entity.AllowedProjects = mapper.Where(e => e.Type == (int)AclMappingType.Project).Select(e => e.Value).ToArray();

            if (entity.LimitedToUser)
                entity.AllowedUsers = mapper.Where(e => e.Type == (int)AclMappingType.User).Select(e => e.Value).ToArray();

            return entity;
        }

        public IPagedList<ContactDetail> GetContactDetailList(Pager pager, int companyId)
        {
            using (var con = _dbProvider.Connection())
            {
                var query = "SELECT SQL_CALC_FOUND_ROWS  * FROM master_contactdetail";

                List<String> WhereColumns = new List<string>();

                WhereColumns.Add(string.Format(" Company_Id = {0} ", companyId));
                WhereColumns.Add(string.Format(" Deleted = {0}", 0));

                if(!string.IsNullOrEmpty(pager.Criteria.SearchTerm))
                {
                    WhereColumns.Add(string.Format(" Tttle Like %{0}%", pager.Criteria.SearchTerm));
                    WhereColumns.Add(string.Format(" Details Like %{0}%", pager.Criteria.SearchTerm));
                }

                if (pager.Criteria.Filter.Any())
                {

                }

                if (WhereColumns.Any())
                    query = query + string.Format(" WHERE {0}", string.Join(" AND ", WhereColumns));

                query = query + " ORDER BY CreatedOnUtc DESC ";

                if (pager.PageIndex >= 0 && pager.PageSize > 0)
                {
                    query = query + string.Format(" LIMIT {0},{1} ", pager.PageIndex * pager.PageSize, pager.PageSize);
                }

                query += " ; SELECT FOUND_ROWS()";

                var multi = con.QueryMultiple(query);

                var lst = multi.Read<ContactDetail>().ToList();
                int total = multi.Read<int>().SingleOrDefault();

                return new PagedList<ContactDetail>(lst, pager.PageIndex, pager.PageSize, total);
            }
        }

        public IList<ContactDetail> GetContactDetailList(object filter)
        {
            return _contactDetailRep.GetList(filter).ToList();
        }

        public void InsertContactDetail(ContactDetail entity)
        {
           var id = _contactDetailRep.Insert(entity);
            entity.Id = id;
            INsertMapping(entity);

        }

        public void UpdateContactDetail(ContactDetail entity)
        {
            _contactDetailRep.Update(entity);

            var mapper = _aclMappingService.GetAclMappingList(new { EntityName = "ContactDetail", EntityId = entity.Id });

            foreach (var map in mapper)
                _aclMappingService.DeleteAclMapping(map);

            INsertMapping(entity);


        }

        private void INsertMapping(ContactDetail entity)
        {
            if (entity.LimitedToDepartment)
            {
                _aclMappingService.InsertDepartmentMapping(entity.Company_Id, "ContactDetail", entity.Id, entity.AllowedDepartments);
            }

            if (entity.LimitedToProject)
            {
                _aclMappingService.InsertProjectMapping(entity.Company_Id, "ContactDetail", entity.Id, entity.AllowedProjects);

            }

            if (entity.LimitedToUser)
            {
                _aclMappingService.InsertUserMapping(entity.Company_Id, "ContactDetail", entity.Id, entity.AllowedUsers);

            }
        }
    }
}
