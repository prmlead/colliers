﻿using PRM.Core.Domain.Configurations;
using PRM.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PRM.Services.Configurations
{
    public class RegistrationFormService : IRegistrationFormService
    {
        public RegistrationFormService(IRepository<RegistrationForm> registrationFormRepository)
        {
            this._registrationFormRepository = registrationFormRepository;

        }

        private readonly IRepository<RegistrationForm> _registrationFormRepository;

        public IList<RegistrationForm> GetRegistrationForm(int companyId)
        {
            return _registrationFormRepository.GetList(new { Company_Id = companyId,Deleted = false }).ToList();
        }

        public void InsertRegistrationFormField(RegistrationForm entity)
        {
          entity.Id =  _registrationFormRepository.Insert(entity);
        }

        public void UpdateRegistrationFormField(RegistrationForm entity)
        {
            _registrationFormRepository.Update(entity);
        }

        public void DeleteRegistrationFormField(RegistrationForm entity)
        {
            _registrationFormRepository.Delete(entity);
        }

        public RegistrationForm GetRegistrationFormField(int id)
        {
            return _registrationFormRepository.GetById(id);
        }




    }


}