﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using MySql.Data.MySqlClient;

namespace PRMServices.SQLHelper
{
    public class MySQLBizClass : IDatabaseHelper
    {
        private static NLog.Logger logger = NLog.LogManager.GetCurrentClassLogger();
        PRMMySqlHelper objDAL = new PRMMySqlHelper();
        //All Business Method here
        #region ALL Business method here
        public DataSet SelectList(String SP_NAME, SortedDictionary<object, object> sd, int timeout = 0)
        {
            try
            {
                return objDAL.SP_Dataset_return(SP_NAME, timeout, GetSdParameter(sd));
            }
            catch (Exception ex)
            {
                logger.Error(ex, ex.Message);
                throw ex;
            }
        }

        public DataTable SelectQuery(String query)
        {
            try
            {
                return objDAL.DataTable_return(query);
            }
            catch (Exception ex)
            {
                logger.Error(ex, ex.Message);
                throw ex;
            }
        }

        public DataSet ExecuteQuery(String query)
        {
            try
            {
                return objDAL.DataSet_return(query);
            }
            catch (Exception ex)
            {
                logger.Error(ex, ex.Message);
                throw ex;
            }
        }


        // Insert /update and Delete by Query
        public int ExecuteNonQuery_IUD(String Query)
        {
            return objDAL.ExecuteNonQuery_IUD(Query);
        }

        public void BulkInsert(DataTable dt, string tableName, List<SqlBulkCopyColumnMapping> columnMappings)
        {
            try
            {
                tableName = cleanMsSqlSyntax(tableName);
                objDAL.BulkInsert(dt, tableName);
            }
            catch (Exception ex)
            {
                logger.Error(ex, ex.Message);
                throw ex;
            }
        }


        #endregion

        #region Methods Parameter

        /// <summary>
        /// This method Sorted-Dictionary key values to an array of SqlParameters
        /// </summary>
        MySqlParameter[] GetSdParameter(SortedDictionary<object, object> sortedDictionary)
        {
            MySqlParameter[] paramArray = new MySqlParameter[] { };

            foreach (string key in sortedDictionary.Keys)
            {
                AddParameter(ref paramArray, new MySqlParameter(key, sortedDictionary[key]));
            }

            return paramArray;
        }

        void AddParameter(ref MySqlParameter[] paramArray, string parameterName, object parameterValue)
        {
            MySqlParameter parameter = new MySqlParameter(parameterName, parameterValue);

            AddParameter(ref paramArray, parameter);
        }

        void AddParameter(ref MySqlParameter[] paramArray, string parameterName, object parameterValue, object parameterNull)
        {
            MySqlParameter parameter = new MySqlParameter();
            parameter.ParameterName = parameterName;

            if (parameterValue.ToString() == parameterNull.ToString())
                parameter.Value = DBNull.Value;
            else
                parameter.Value = parameterValue;

            AddParameter(ref paramArray, parameter);
        }

        void AddParameter(ref MySqlParameter[] paramArray, string parameterName, SqlDbType dbType, object parameterValue)
        {
            MySqlParameter parameter = new MySqlParameter(parameterName, dbType);
            parameter.Value = parameterValue;

            AddParameter(ref paramArray, parameter);
        }

        void AddParameter(ref MySqlParameter[] paramArray, string parameterName, SqlDbType dbType, ParameterDirection direction, object parameterValue)
        {
            MySqlParameter parameter = new MySqlParameter(parameterName, dbType);
            parameter.Value = parameterValue;
            parameter.Direction = direction;

            AddParameter(ref paramArray, parameter);
        }

        void AddParameter(ref MySqlParameter[] paramArray, params MySqlParameter[] newParameters)
        {
            MySqlParameter[] newArray = Array.CreateInstance(typeof(MySqlParameter), paramArray.Length + newParameters.Length) as MySqlParameter[];
            paramArray.CopyTo(newArray, 0);
            newParameters.CopyTo(newArray, paramArray.Length);

            paramArray = newArray;
        }


        string cleanMsSqlSyntax(string query)
        {
            //if (!string.IsNullOrEmpty(query) && query.ToLower().Contains("utc_timestamp"))
            //{
            //    if (query.ToLower().Contains("utc_timestamp()"))
            //    {
            //        query = query.Replace("utc_timestamp()", "GETUTCDATE()").Replace("UTC_TIMESTAMP()", "GETUTCDATE()");
            //    }
            //    else if (query.ToLower().Contains("utc_timestamp"))
            //    {
            //        query = query.Replace("utc_timestamp", "GETUTCDATE()").Replace("UTC_TIMESTAMP", "GETUTCDATE()");
            //    }
            //}

            //if (!string.IsNullOrEmpty(query) && query.ToLower().Contains("now()"))
            //{
            //    query = query.Replace("now()", "GETUTCDATE()").Replace("NOW()", "GETUTCDATE()");
            //}

            if (!string.IsNullOrEmpty(query) && query.ToLower().Contains("dbo."))
            {
                query = query.ToLower().Replace("dbo.", "");
                if (query.StartsWith("[") && query.EndsWith("]")) {
                    query = query.Replace("[","").Replace("]","");
                }
            }

            return query;
        }

        #endregion
    }
}