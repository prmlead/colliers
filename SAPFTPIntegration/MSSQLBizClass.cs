﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace PRMServices.SQLHelper
{
    public class MSSQLBizClass : IDatabaseHelper
    {
        private static NLog.Logger logger = NLog.LogManager.GetCurrentClassLogger();
        PRMMSSqlHelper objDAL = new PRMMSSqlHelper();
        //All Business Method here
        #region ALL Business method here
        public DataSet SelectList(String SP_NAME, SortedDictionary<object, object> sd, int timeout = 0)
        {
            try
            {
                return objDAL.SP_Dataset_return(SP_NAME, timeout, GetSdParameter(sd));
            }
            catch (Exception ex)
            {
                logger.Error(ex, ex.Message);
                throw ex;
            }
        }

        public DataTable SelectQuery(String query)
        {
            try
            {
                return objDAL.DataTable_return(query);
            }
            catch (Exception ex)
            {
                logger.Error(ex, ex.Message);
                throw ex;
            }
        }

        public DataSet ExecuteQuery(String query)
        {
            try
            {
                query = cleanMySqlSyntax(query);
                return objDAL.DataSet_return(query);
            }
            catch (Exception ex)
            {
                logger.Error(ex, ex.Message);
                throw ex;
            }
        }


        // Insert /update and Delete by Query
        public int ExecuteNonQuery_IUD(String Query)
        {
            Query = cleanMySqlSyntax(Query);
            return objDAL.ExecuteNonQuery_IUD(Query);
        }

        public void BulkInsert(DataTable dt, string tableName, List<SqlBulkCopyColumnMapping> columnMappings)
        {
            try
            {
                objDAL.BulkInsert(dt, tableName, columnMappings);
            }
            catch (Exception ex)
            {
                logger.Error(ex, ex.Message);
                throw ex;
            }
        }


        #endregion

        #region Methods Parameter

        /// <summary>
        /// This method Sorted-Dictionary key values to an array of SqlParameters
        /// </summary>
        SqlParameter[] GetSdParameter(SortedDictionary<object, object> sortedDictionary)
        {
            SqlParameter[] paramArray = new SqlParameter[] { };

            foreach (string key in sortedDictionary.Keys)
            {
                AddParameter(ref paramArray, new SqlParameter($"@{key}", sortedDictionary[key]));
            }

            return paramArray;
        }

        void AddParameter(ref SqlParameter[] paramArray, string parameterName, object parameterValue)
        {
            SqlParameter parameter = new SqlParameter(parameterName, parameterValue);

            AddParameter(ref paramArray, parameter);
        }

        void AddParameter(ref SqlParameter[] paramArray, string parameterName, object parameterValue, object parameterNull)
        {
            SqlParameter parameter = new SqlParameter();
            parameter.ParameterName = parameterName;

            if (parameterValue.ToString() == parameterNull.ToString())
                parameter.Value = DBNull.Value;
            else
                parameter.Value = parameterValue;

            AddParameter(ref paramArray, parameter);
        }

        void AddParameter(ref SqlParameter[] paramArray, string parameterName, SqlDbType dbType, object parameterValue)
        {
            SqlParameter parameter = new SqlParameter(parameterName, dbType);
            parameter.Value = parameterValue;

            AddParameter(ref paramArray, parameter);
        }

        void AddParameter(ref SqlParameter[] paramArray, string parameterName, SqlDbType dbType, ParameterDirection direction, object parameterValue)
        {
            SqlParameter parameter = new SqlParameter(parameterName, dbType);
            parameter.Value = parameterValue;
            parameter.Direction = direction;

            AddParameter(ref paramArray, parameter);
        }

        void AddParameter(ref SqlParameter[] paramArray, params SqlParameter[] newParameters)
        {
            SqlParameter[] newArray = Array.CreateInstance(typeof(SqlParameter), paramArray.Length + newParameters.Length) as SqlParameter[];
            paramArray.CopyTo(newArray, 0);
            newParameters.CopyTo(newArray, paramArray.Length);

            paramArray = newArray;
        }

        string cleanMySqlSyntax(string query)
        {
            if (!string.IsNullOrEmpty(query) && query.ToLower().Contains("utc_timestamp"))
            {
                if (query.ToLower().Contains("utc_timestamp()"))
                {
                    query = query.Replace("utc_timestamp()", "GETUTCDATE()").Replace("UTC_TIMESTAMP()", "GETUTCDATE()");
                }
                else if (query.ToLower().Contains("utc_timestamp"))
                {
                    query = query.Replace("utc_timestamp", "GETUTCDATE()").Replace("UTC_TIMESTAMP", "GETUTCDATE()");
                }
            }

            if (!string.IsNullOrEmpty(query) && query.ToLower().Contains("now()"))
            {
                query = query.Replace("now()", "GETUTCDATE()").Replace("NOW()", "GETUTCDATE()");
            }

            if (!string.IsNullOrEmpty(query) && query.ToLower().Contains("call "))
            {
                query = query.Replace("CALL ", "EXEC ").Replace("call ", "EXEC ").Replace("(", " ").Replace(")", "");
            }

            return query;
        }

        #endregion
    }
}