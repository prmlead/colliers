prmApp
.factory('SignalRFactory',
    ["$http", "$rootScope", "$location", "Hub", "$timeout",
    function ($http, $rootScope, $location, Hub, $timeout) {
        function backendFactory(serverUrl, hubName) {
            $.connection.hub.logging = true;
            var requirementHub = $.connection.requirementHub;
            requirementHub.client.updateRequirement = function (objectID, timeLeft) {
                
            };

            $.connection.hub.start("~/signalr").done(function () {
                $('#sendmessage').click();
            });


            var connection = $.hubConnection("http://ptmapps.com");
            var proxy = connection.createHubProxy(hubName);
            connection.start({jsonp:true,xdomain:true,logging:true}).done(function () { });
            return {
                on: function (eventName, callback) {
                    proxy.on(eventName, function (result) {
                        $rootScope.$apply(function () {
                            if (callback) {
                                callback(result);
                            }
                        });
                    });
                },
                invoke: function (methodName, callback) {
                    proxy.invoke(methodName)
                        .done(function (result) {
                            $rootScope.$apply(function () {
                                if (callback) {
                                    callback(result);
                                }
                            });
                        });
                },
                start: function (fn) {
                    return connection.hub.start().done(fn);
                }
            };
        }

        return backendFactory;
    }]);
