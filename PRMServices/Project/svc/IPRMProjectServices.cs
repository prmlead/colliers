﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using System.Configuration;
using System.Data.SqlClient;
using PRMServices.Models;

namespace PRMServices
{
    [ServiceContract]
    public interface IPRMProjectServices
    {
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getProjectList?COMP_ID={COMP_ID}&USER_ID={USER_ID}&DEPT_ID={DEPT_ID}&DESIG_ID={DESIG_ID}&pendingApproval={pendingApproval}&PROJECT_ID={PROJECT_ID}&COST_CENTER={COST_CENTER}&LOCATION={LOCATION}&SEARCH={SEARCH}&sessionid={sessionid}&PageSize={PageSize}&NumberOfRecords={NumberOfRecords}")]
        List<ProjectDetails> GetProjectList(int COMP_ID, int USER_ID, string DEPT_ID, string DESIG_ID, int pendingApproval, string PROJECT_ID, string COST_CENTER, string LOCATION, string SEARCH, string sessionid, int PageSize = 0, int NumberOfRecords = 0);

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "GetProjectListByFilter?COMP_ID={COMP_ID}&USER_ID={USER_ID}&TYPE={TYPE}&sessionid={sessionid}")]
        List<ProjectDetails> GetProjectListByFilter(int COMP_ID, int USER_ID,string TYPE, string sessionid);


        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getProjectDetails?PROJECT_ID={PROJECT_ID}&sessionid={sessionid}")]
        ProjectDetails GetProjectDetails(int PROJECT_ID, string sessionid);

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getProjectAmendmentDetails?PROJECT_ID={PROJECT_ID}&sessionid={sessionid}")]
        ProjectDetails GetProjectAmendmentDetails(int PROJECT_ID, string sessionid);

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getBudgetDetails?BUDGET_ID={BUDGET_ID}&sessionid={sessionid}")]
        BudgetDetails GetBudgetDetails(int BUDGET_ID, string sessionid);

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getBudgetDetailsByProject?projectId={projectId}&budgetId={budgetId}&onlyApproved={onlyApproved}&subPackageIds={subPackageIds}&sessionId={sessionId}")]
        List<BudgetDetails> GetBudgetDetailsByProject(int projectId, int budgetId, bool onlyApproved, string subPackageIds, string sessionId);

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getBudgetDetailsbyProjectAmendment?projectId={projectId}&budgetId={budgetId}&amendmentId={amendmentId}&sessionId={sessionId}")]
        List<BudgetDetails> GetBudgetDetailsbyProjectAmendment(int projectId, int budgetId, int amendmentId, string sessionId);

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "AreValidToPostRFQS?PROJECT_ID={PROJECT_ID}&SUB_PACKAGE_IDS={SUB_PACKAGE_IDS}&sessionId={sessionId}")]
        bool AreValidToPostRFQS(int PROJECT_ID, string SUB_PACKAGE_IDS, string sessionId);

        [WebInvoke(Method = "POST",
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        ResponseFormat = WebMessageFormat.Json,
        RequestFormat = WebMessageFormat.Json,
        UriTemplate = "saveProjectDetails")]
        Response SaveProjectDetails( ProjectDetails projectdetails, string sessionid);

        [WebInvoke(Method = "POST",
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        ResponseFormat = WebMessageFormat.Json,
        RequestFormat = WebMessageFormat.Json,
        UriTemplate = "updateBudgetWorkflow")]
        Response UpdateBudgetWorkflow(int wID,int moduleID,int projectID,int user,string sessionid);
        

        [WebInvoke(Method = "POST",
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        ResponseFormat = WebMessageFormat.Json,
        RequestFormat = WebMessageFormat.Json,
        UriTemplate = "deleteProject")]
        Response DeleteProject(int PROJECT_ID, string sessionID);

        [WebInvoke(Method = "POST",
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        ResponseFormat = WebMessageFormat.Json,
        RequestFormat = WebMessageFormat.Json,
        UriTemplate = "saveBudgetDetails")]
        Response SaveBudgetDetails(List<BudgetDetails> budgetDetailsList, int projectId, int budgetId, string sessionid);

        [WebInvoke(Method = "POST",
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        ResponseFormat = WebMessageFormat.Json,
        RequestFormat = WebMessageFormat.Json,
        UriTemplate = "saveProjectAmendmentDetails")]
        Response SaveProjectAmendmentDetails(List<BudgetDetails> budgetDetailsList, int projectId, int budgetId, int projectAmendmentId,
            decimal PROJECT_PO_VALUE, decimal NET_REVENUE_MARGIN, decimal TARGETED_REVENUE_BUDGET, string sessionid); 

        [WebInvoke(Method = "POST",
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        ResponseFormat = WebMessageFormat.Json,
        RequestFormat = WebMessageFormat.Json,
        UriTemplate = "updateVendorWorkflow")]
        Response UpdateVendorWorkflow(List<BudgetDetails> budgetDetailsList, int projectId, int budgetId, string sessionid);

        [WebInvoke(Method = "POST",
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        ResponseFormat = WebMessageFormat.Json,
        RequestFormat = WebMessageFormat.Json,
        UriTemplate = "updateBudgetDetails")]
        Response UpdateBudgetDetails(List<BudgetDetails> details, string sessionid);

        [WebInvoke(Method = "POST",
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        ResponseFormat = WebMessageFormat.Json,
        RequestFormat = WebMessageFormat.Json,
        UriTemplate = "assignPackageApproval")]
        Response AssignPackageApproval(List<BudgetDetails> details, string sessionid);

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "GetPackageApprovalList?COMP_ID={COMP_ID}&USER_ID={USER_ID}&PROJECT_ID={PROJECT_ID}&COST_CENTER={COST_CENTER}&LOCATION={LOCATION}&SEARCH={SEARCH}&sessionid={sessionid}&PageSize={PageSize}&NumberOfRecords={NumberOfRecords}")]
        List<BudgetDetails> GetPackageApprovalList(int COMP_ID, int USER_ID, string PROJECT_ID, string COST_CENTER, string LOCATION, string SEARCH, string sessionid, int PageSize = 0, int NumberOfRecords = 0);

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "GetApprovedPackages?subpackageIds={subpackageIds}&projectId={projectId}")]
        List<BudgetDetails> GetApprovedPackages(string subpackageIds, int projectId);

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "GetProjectAudit?COMP_ID={COMP_ID}&PROJECT_ID={PROJECT_ID}&sessionid={sessionid}")]
        string GetProjectAudit(int COMP_ID, int PROJECT_ID, string sessionid);

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "GetBudgetAudit?COMP_ID={COMP_ID}&BUDGET_ID={BUDGET_ID}&sessionid={sessionid}")]
        string GetBudgetAudit(int COMP_ID, int BUDGET_ID, string sessionid);

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "GetProjectBillToDetails?U_ID={U_ID}&COMP_ID={COMP_ID}&searchString={searchString}&sessionID={sessionID}")]
        string GetProjectBillToDetails(int U_ID, int COMP_ID, string searchString, string sessionID);

        [WebInvoke(Method = "POST",
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        ResponseFormat = WebMessageFormat.Json,
        RequestFormat = WebMessageFormat.Json,
        UriTemplate = "SaveProjectBillToDetails")]
        Response SaveProjectBillToDetails(BillTo billTo, string sessionid);

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getDashboardFilters?COMP_ID={COMP_ID}&USER_ID={USER_ID}&FROM_DATE={FROM_DATE}&TO_DATE={TO_DATE}&DEPT_ID={DEPT_ID}&DESIG_ID={DESIG_ID}&sessionid={sessionid}")]
        List<ProjectDashboard> GetProjectDashboardFilters(int COMP_ID, int USER_ID, string FROM_DATE, string TO_DATE, string DEPT_ID, string DESIG_ID, string sessionid);

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getProjectDashboardStats?COMP_ID={COMP_ID}&USER_ID={USER_ID}&costCenter={costCenter}" +

       "&location={location}&clientName={clientName}&projectCode={projectCode}&projectId={projectId}&fromDate={fromDate}&toDate={toDate}&sessionid={sessionid}&type={type}&DEPT_ID={DEPT_ID}&DESIG_ID={DESIG_ID}")]

        ProjectDashboard GetProjectDashboardStats(int COMP_ID, int USER_ID, string costCenter, string location, string clientName, string projectCode, string projectId, string fromDate, string toDate, string sessionid,int type, string DEPT_ID, string DESIG_ID);

    }
}
