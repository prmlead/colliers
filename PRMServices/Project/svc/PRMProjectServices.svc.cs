﻿using System;
using System.Data;
using System.Linq;
using System.Collections.Generic;
using System.ServiceModel.Activation;
using PRMServices.Models;
using PRMServices.SQLHelper;
using CORE = PRM.Core.Common;
using Newtonsoft.Json;
using System.Web;

namespace PRMServices
{
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class PRMProjectServices : IPRMProjectServices
    {

        private static NLog.Logger logger = NLog.LogManager.GetCurrentClassLogger();
        private IDatabaseHelper sqlHelper = DatabaseProvider.GetDatabaseProvider();
        PRMServices prm = new PRMServices();

        #region Services
        public List<ProjectDetails> GetProjectList( int COMP_ID, int USER_ID, string DEPT_ID, string DESIG_ID, int pendingApproval, string PROJECT_ID, string COST_CENTER, 
            string LOCATION, string STATUS, string sessionid, int PageSize = 0, int NumberOfRecords = 0)
        {
            List<ProjectDetails> details = new List<ProjectDetails>();
            try
            {
                Utilities.ValidateSession(sessionid);
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
              
                sd.Add("P_COMP_ID", COMP_ID);
                sd.Add("P_U_ID", USER_ID);
                sd.Add("P_DEPT_ID", DEPT_ID);
                sd.Add("P_DESIG_ID", DESIG_ID);
                sd.Add("P_PENDING_APPROVAL", pendingApproval);
                sd.Add("P_PROJ_ID", PROJECT_ID);
                sd.Add("P_COST_CENTER", COST_CENTER);
                sd.Add("P_LOCATION", LOCATION);
                sd.Add("P_SEARCH", STATUS);
                sd.Add("P_PAGE", PageSize);
                sd.Add("P_PAGE_SIZE", NumberOfRecords);
                CORE.DataNamesMapper<ProjectDetails> mapper = new CORE.DataNamesMapper<ProjectDetails>();
                var dataset = sqlHelper.SelectList("proj_getProjectList", sd);
                details = mapper.Map(dataset.Tables[0]).ToList();

                //if (dataset.Tables.Count > 1)
                //{
                //    List<ProjectDetails> details1 = mapper.Map(dataset.Tables[1]).ToList();
                //    foreach (var detail in details)
                //    {
                //        detail.STATUS = "OPEN";
                //        if (details1.Any(d => d.PROJECT_ID == detail.PROJECT_ID))
                //        {
                //            detail.STATUS = "IN-PROGRESS";
                //        }
                //    }
                //}
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error in retrieving Data");
            }

            return details;
        }
        public List<ProjectDetails> GetProjectListByFilter(int COMP_ID, int USER_ID,string TYPE, string sessionid)
        {
            List<ProjectDetails> details = new List<ProjectDetails>();
            try
            {
                Utilities.ValidateSession(sessionid);
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };

                sd.Add("P_COMP_ID", COMP_ID);
                sd.Add("P_U_ID", USER_ID);
                sd.Add("P_TYPE", TYPE);
                CORE.DataNamesMapper<ProjectDetails> mapper = new CORE.DataNamesMapper<ProjectDetails>();
                var dataset = sqlHelper.SelectList("proj_GetFilteredProjectList", sd);
                details = mapper.Map(dataset.Tables[0]).ToList();

            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error in retrieving Data");
            }

            return details;
        }
        public Response SaveProjectDetails(ProjectDetails projectdetails, string sessionid)
        {

            Response details = new Response();

            try
            {
                Utilities.ValidateSession(sessionid);
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };

                sd.Add("P_PROJECT_ID", projectdetails.PROJECT_ID);
                sd.Add("P_COMP_ID", projectdetails.COMP_ID);
                sd.Add("P_U_ID", projectdetails.U_ID);
                sd.Add("P_PROJ_ID", projectdetails.PROJ_ID);
                sd.Add("P_PROJECT_NAME", projectdetails.PROJECT_NAME);
                sd.Add("P_COST_CENTER", projectdetails.COST_CENTER);
                sd.Add("P_LOCATION", projectdetails.LOCATION);
                sd.Add("P_PO_VALUE", projectdetails.PO_VALUE);
                sd.Add("P_PO_NUMBER", projectdetails.PO_NUMBER);
                sd.Add("P_NET_REVENUE_MARGIN", projectdetails.NET_REVENUE_MARGIN);
                sd.Add("P_SHIP_TO_ADDRESS", projectdetails.SHIP_TO_ADDRESS);
                sd.Add("P_BILL_TO_ADDRESS", projectdetails.BILL_TO_ADDRESS_JSON);
                sd.Add("P_TARGETED_REVENUE_BUDGET", projectdetails.TARGETED_REVENUE_BUDGET);
                sd.Add("P_IS_VALID", projectdetails.IS_VALID);

                sd.Add("P_CLIENT_NAME", projectdetails.CLIENT_NAME);
                sd.Add("P_ADDRESS", projectdetails.ADDRESS);
                sd.Add("P_SPOC_EMAIL", projectdetails.SPOC_EMAIL);
                sd.Add("P_GST_NUMBER", projectdetails.GST_NUMBER);
                sd.Add("P_PAN_NUMBER", projectdetails.PAN_NUMBER);
                sd.Add("P_ASSIGNED_U_ID", projectdetails.ASSIGNED_U_ID);
                sd.Add("P_ASSIGNED_U_IDS", string.Join(",", projectdetails.ASSIGNED_U_IDS));
                sd.Add("P_DEPT_IDS", string.Join(",", projectdetails.DEPT_IDS));
                sd.Add("P_PROCUREMENT_U_IDS", string.Join(",", projectdetails.PROCUREMENT_U_IDS));
                sd.Add("P_BILL_U_IDS", string.Join(",", projectdetails.BILL_U_IDS));

                CORE.DataNamesMapper<Response> mapper = new CORE.DataNamesMapper<Response>();
                var dataset = sqlHelper.SelectList("proj_SaveProjectDetails", sd);
                details = mapper.Map(dataset.Tables[0]).FirstOrDefault();

                if (dataset != null && dataset.Tables.Count > 0 && dataset.Tables[0].Rows.Count > 0 && dataset.Tables[0].Rows[0][0] != null)
                {
                    details.ObjectID = dataset.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToInt32(dataset.Tables[0].Rows[0][0].ToString()) : -1;
                    details.ErrorMessage = dataset.Tables[0].Rows[0][1] != DBNull.Value ? Convert.ToString(dataset.Tables[0].Rows[0][1].ToString()) : string.Empty;
                }

            }
            catch (Exception ex)
            {
                logger.Error("Error in SaveProjectDetails()>>>" + ex.Message);
            }
            return details;
        }
        public Response SaveBudgetDetails(List<BudgetDetails> budgetDetailsList, int projectId, int budgetId, string sessionid)
        {
            Response details = new Response();
            try
            {
                Utilities.ValidateSession(sessionid);
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };

                string budgetidsstr = string.Empty;
                var budgetids = budgetDetailsList.Where(b => b.ROW_ID > 0).Select(a => a.ROW_ID).ToArray();
                budgetidsstr = string.Join(",", budgetids);
                if (!string.IsNullOrEmpty(budgetidsstr))
                {
                    if (budgetDetailsList[0].IS_FROM_PROJECT_AMENDMENT)
                    {
                        sqlHelper.ExecuteQuery($@"delete from budgetS where ROW_ID not in ({budgetidsstr}) and BUDGET_ID = {budgetId}");
                    }

                }

                string attachStr = string.Empty;
                foreach (BudgetDetails budgetdetails in budgetDetailsList) 
                {
                    if (string.IsNullOrEmpty(attachStr) && budgetdetails != null && budgetdetails.MULTIPLE_ATTACHMENTS != null && budgetdetails.MULTIPLE_ATTACHMENTS.Count > 0)
                    {
                        Response response = new Response();
                        List<FileUpload> attachments = new List<FileUpload>();
                        foreach (FileUpload fd in budgetdetails.MULTIPLE_ATTACHMENTS)
                        {
                            var newFileObj = new FileUpload();
                            if (fd.FileID <= 0)
                            {
                                var attachName = string.Empty;
                                long tick = DateTime.UtcNow.Ticks;
                                attachName = HttpContext.Current.Server.MapPath(Utilities.FILE_URL + fd.FileName);
                                prm.SaveFile(attachName, fd.FileStream);
                                attachName = fd.FileName;
                                Response res = prm.SaveAttachment(attachName);
                                if (res.ErrorMessage != "")
                                {
                                    response.ErrorMessage = res.ErrorMessage;
                                }

                                fd.FileID = res.ObjectID;
                                newFileObj.FileID = fd.FileID;
                                newFileObj.FileName = fd.FileName;
                                newFileObj.FileStream = null;
                            }
                            else if (fd.FileID > 0)
                            {
                                var fileData = prm.DownloadFile(Convert.ToString(fd.FileID), sessionid);
                                newFileObj.FileID = fd.FileID;
                                newFileObj.FileName = fd.FileName;
                                newFileObj.FileStream = null;
                            }
                            attachments.Add(newFileObj);
                        }

                        attachStr = Newtonsoft.Json.JsonConvert.SerializeObject(attachments);
                    }

                    budgetdetails.ATTACHMENTS = attachStr;

                    sd = new SortedDictionary<object, object>() { };
                    sd.Add("P_ROW_ID", budgetdetails.ROW_ID);
                    sd.Add("P_BUDGET_ID", budgetId);
                    sd.Add("P_COMP_ID", budgetdetails.COMP_ID);
                    sd.Add("P_U_ID", budgetdetails.U_ID);
                    sd.Add("P_PROJECT_ID", projectId);
                    sd.Add("P_PACKAGE_ID", budgetdetails.PACKAGE_ID);
                    sd.Add("P_PO_VALUE", budgetdetails.PO_VALUE);
                    sd.Add("P_NET_REVENUE_MARGIN", budgetdetails.NET_REVENUE_MARGIN);
                    sd.Add("P_SUB_PACKAGE_ID", budgetdetails.SUB_PACKAGE_ID);
                    sd.Add("P_ALLOCATION_PERCENTAGE", budgetdetails.ALLOCATION_PERCENTAGE);
                    sd.Add("P_EST_BUDGET_AMOUNT", budgetdetails.EST_BUDGET_AMOUNT);                
                    sd.Add("P_PACKAGE_STATUS", budgetdetails.PACKAGE_STATUS);
                    sd.Add("P_BUDGET_WF_ID", budgetdetails.BUDGET_WF_ID);
                    sd.Add("P_VENDOR_IDS", budgetdetails.VENDOR_IDS);
                    sd.Add("P_SUB_PACKAGE_WF_ID", budgetdetails.SUB_PACKAGE_WF_ID);
                    sd.Add("P_IS_VALID", budgetdetails.IS_VALID);
                    sd.Add("P_IS_BUDGET_APPROVED", budgetdetails.IS_BUDGET_APPROVED ? 1 : 0);
                    sd.Add("P_IS_FROM_PROJECT_AMENDMENT", budgetdetails.IS_FROM_PROJECT_AMENDMENT ? 1 : 0);
                    sd.Add("P_ATTACHMENTS", budgetdetails.ATTACHMENTS);

                    var dataset = sqlHelper.SelectList("proj_SavebudgetDetails", sd);

                    if (budgetId <= 0 && dataset.Tables.Count > 0 && dataset.Tables[0].Rows.Count > 0)
                    {
                        budgetId = Convert.ToInt32(dataset.Tables[0].Rows[0]["BUDGET_ID"]);
                    }
                }

                details.ObjectID = budgetId;

                if (budgetDetailsList[0].IS_FROM_PROJECT_AMENDMENT) 
                {
                    sd = new SortedDictionary<object, object>() { };
                    sd.Add("P_PROJECT_ID", budgetDetailsList[0].PROJECT_ID);
                    sd.Add("P_PROJECT_PO_VALUE", budgetDetailsList[0].PROJECT_PO_VALUE);
                    sd.Add("P_PROJECT_NET_REVENUE_MARGIN", budgetDetailsList[0].PROJECT_NET_REVENUE_MARGIN);
                    sd.Add("P_PROJECT_TARGETED_REVENUE_BUDGET", budgetDetailsList[0].PROJECT_TARGETED_REVENUE_BUDGET);
                    sqlHelper.SelectList("proj_UpdateProjectAmendement", sd);
                }

            }
            catch (Exception ex)
            {
                details.ObjectID = 0;
                details.ErrorMessage = ex.Message;
                logger.Error("Error in SaveBudgetDetails()>>>", ex.Message);
            }

            return details;
        }

        public Response SaveProjectAmendmentDetails(List<BudgetDetails> budgetDetailsList, int projectId, int budgetId, int projectAmendmentId,
            decimal PROJECT_PO_VALUE, decimal NET_REVENUE_MARGIN, decimal TARGETED_REVENUE_BUDGET, string sessionid)
        {
            Response details = new Response();
            try
            {
                Utilities.ValidateSession(sessionid);
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                var dataset = new DataSet();
                foreach (BudgetDetails budgetdetails in budgetDetailsList) 
                {
                    sd = new SortedDictionary<object, object>() { };
                    sd.Add("P_ROW_ID", budgetdetails.ROW_ID);
                    sd.Add("P_BUDGET_ROW_ID", budgetdetails.BUDGET_ROW_ID);
                    sd.Add("P_PROJECT_AMENDMENT_ID", projectAmendmentId);
                    sd.Add("P_BUDGET_ID", budgetdetails.BUDGET_ID);
                    sd.Add("P_COMP_ID", budgetdetails.COMP_ID);
                    sd.Add("P_U_ID", budgetdetails.U_ID);
                    sd.Add("P_PROJECT_ID", projectId);
                    sd.Add("P_PACKAGE_ID", budgetdetails.PACKAGE_ID);
                    sd.Add("P_PO_VALUE", budgetdetails.PO_VALUE);
                    sd.Add("P_NET_REVENUE_MARGIN", budgetdetails.NET_REVENUE_MARGIN);
                    sd.Add("P_SUB_PACKAGE_ID", budgetdetails.SUB_PACKAGE_ID);
                    sd.Add("P_ALLOCATION_PERCENTAGE", budgetdetails.ALLOCATION_PERCENTAGE);
                    sd.Add("P_EST_BUDGET_AMOUNT", budgetdetails.EST_BUDGET_AMOUNT);                
                    sd.Add("P_PACKAGE_STATUS", budgetdetails.PACKAGE_STATUS);
                    sd.Add("P_BUDGET_WF_ID", budgetdetails.BUDGET_WF_ID);
                    sd.Add("P_VENDOR_IDS", budgetdetails.VENDOR_IDS);
                    sd.Add("P_SUB_PACKAGE_WF_ID", budgetdetails.SUB_PACKAGE_WF_ID);
                    sd.Add("P_IS_VALID", budgetdetails.IS_VALID);
                    sd.Add("P_IS_BUDGET_APPROVED", budgetdetails.IS_BUDGET_APPROVED ? 1 : 0);
                    sd.Add("P_PROJECT_PO_VALUE", PROJECT_PO_VALUE);
                    sd.Add("P_PROJECT_NET_REVENUE_MARGIN", NET_REVENUE_MARGIN);
                    sd.Add("P_PROJECT_TARGETED_REVENUE_BUDGET", TARGETED_REVENUE_BUDGET);
                    dataset = sqlHelper.SelectList("proj_SaveprojectAmendmentDetails", sd);


                    if (dataset != null && dataset.Tables.Count > 0 && dataset.Tables[0].Rows.Count > 0 && dataset.Tables[0].Rows[0][0] != null)
                    {
                        details.ObjectID = dataset.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToInt32(dataset.Tables[0].Rows[0][0].ToString()) : -1;
                        projectAmendmentId = details.ObjectID;
                        details.ErrorMessage = dataset.Tables[0].Rows[0][1] != DBNull.Value ? Convert.ToString(dataset.Tables[0].Rows[0][1].ToString()) : string.Empty;
                    }

                    if (projectAmendmentId <= 0 && dataset.Tables.Count > 0 && dataset.Tables[0].Rows.Count > 0 && string.IsNullOrEmpty(details.ErrorMessage))
                    {
                        projectAmendmentId = Convert.ToInt32(dataset.Tables[0].Rows[0]["PROJECT_AMENDMENT_ID"]);
                    }
                }

                details.ObjectID = projectAmendmentId;

                if (details.ObjectID > 0 && string.IsNullOrEmpty(details.ErrorMessage)) 
                {
                    PRMWFService pRMWF = new PRMWFService();
                    Response res2 = pRMWF.AssignWorkflow(budgetDetailsList[0].BUDGET_WF_ID, Convert.ToInt32(details.ObjectID), budgetDetailsList[0].U_ID, sessionid, "PROJECT_AMENDMENT");
                }

            }
            catch (Exception ex)
            {
                details.ObjectID = 0;
                details.ErrorMessage = ex.Message;
                logger.Error("Error in SaveProjectAmendmentDetails()>>>", ex.Message);
            }

            return details;
        }

        public ProjectDetails GetProjectDetails(int PROJECT_ID, string sessionid)
        {
            ProjectDetails details = new ProjectDetails();
            try
            {
                Utilities.ValidateSession(sessionid);
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                sd.Add("P_PROJECT_ID", PROJECT_ID);
                CORE.DataNamesMapper<ProjectDetails> mapper = new CORE.DataNamesMapper<ProjectDetails>();
                var dataset = sqlHelper.SelectList("proj_GetProjectDetails", sd);
                details = mapper.Map(dataset.Tables[0]).FirstOrDefault();

                if (!string.IsNullOrEmpty(details.ASSIGNED_U_IDS_STR))
                {
                    details.ASSIGNED_U_IDS = details.ASSIGNED_U_IDS_STR.Split(',').Select(a=> Convert.ToInt32(a)).ToArray();
                }

                if (!string.IsNullOrEmpty(details.DEPT_IDS_STR))
                {
                    details.DEPT_IDS = details.DEPT_IDS_STR.Split(',').Select(a => Convert.ToInt32(a)).ToArray();
                }

                if (!string.IsNullOrEmpty(details.PROCUREMENT_U_IDS_STR))
                {
                    details.PROCUREMENT_U_IDS = details.PROCUREMENT_U_IDS_STR.Split(',').Select(a => Convert.ToInt32(a)).ToArray();
                }
            }
            catch (Exception ex)
            {
                logger.Error("Error in GetProjectDetails()>>>",ex.Message);
            }

            return details;
        }
        public ProjectDetails GetProjectAmendmentDetails(int PROJECT_ID, string sessionid)
        {
            ProjectDetails details = new ProjectDetails();
            try
            {
                Utilities.ValidateSession(sessionid);
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                sd.Add("P_PROJECT_ID", PROJECT_ID);
                CORE.DataNamesMapper<ProjectDetails> mapper = new CORE.DataNamesMapper<ProjectDetails>();
                var dataset = sqlHelper.SelectList("proj_GetProjectAmendmentDetails", sd);
                details = mapper.Map(dataset.Tables[0]).FirstOrDefault();
            }
            catch (Exception ex)
            {
                logger.Error("Error in GetProjectAmendmentDetails()>>>", ex.Message);
            }

            return details;
        }
        public BudgetDetails GetBudgetDetails(int BUDGET_ID, string sessionid)
        {
            BudgetDetails details = new BudgetDetails();
            try
            {
                Utilities.ValidateSession(sessionid);
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                sd.Add("P_BUDGET_ID", BUDGET_ID);
                CORE.DataNamesMapper<BudgetDetails> mapper = new CORE.DataNamesMapper<BudgetDetails>();
                var dataset = sqlHelper.SelectList("proj_GetBudgetDetails", sd);
                details = mapper.Map(dataset.Tables[0]).FirstOrDefault();
            }
            catch (Exception ex)
            {
                logger.Error("Error in GetBudgetDetails()>>>", ex.Message);
            }

            return details;
        }
        public List<BudgetDetails> GetBudgetDetailsByProject(int projectId, int budgetId, bool onlyApproved, string subPackageIds, string sessionId)
        {
            List<BudgetDetails> details = new List<BudgetDetails>();
            try
            {
                Utilities.ValidateSession(sessionId);
                CORE.DataNamesMapper<BudgetDetails> mapper = new CORE.DataNamesMapper<BudgetDetails>();
                string query = $@"SELECT *, C1.CategoryCode AS PACKAGE_NAME, C2.CategoryCode AS SUB_PACKAGE_NAME,
                                  (select string_agg(concat(po.po_number,'$@',v.COMP_NAME),'@$') from POScheduleDetails po
								  inner join vendors v on po.VENDOR_ID = v.u_id
                                  where package_id = B.PACKAGE_ID and SUB_PACKAGE_ID = B.SUB_PACKAGE_ID and PROJECT_ID = b.PROJECT_ID) as PO_NUMBER,
                                  (select top 1 isnull(total_amount,0) from POScheduleDetails 
                                  where project_id = B.PROJECT_ID and BUDGET_ID = B.BUDGET_ID and PACKAGE_ID = B.PACKAGE_ID and SUB_PACKAGE_ID = B.SUB_PACKAGE_ID) as VENDOR_PO_AMOUNT
                                  FROM budgets B
                                  INNER JOIN cm_category C1 ON C1.CategoryId = B.PACKAGE_ID
                                  LEFT JOIN cm_category C2 ON C2.CategoryId = B.SUB_PACKAGE_ID
                                  WHERE PROJECT_ID = {projectId}";
                if (budgetId > 0)
                {
                    query = $@"SELECT *, C1.CategoryCode AS PACKAGE_NAME, C2.CategoryCode AS SUB_PACKAGE_NAME,
                                (select string_agg(concat(po.po_number,'$@',v.COMP_NAME),'@$') from POScheduleDetails po
								  inner join vendors v on po.VENDOR_ID = v.u_id
                                  where package_id = B.PACKAGE_ID and SUB_PACKAGE_ID = B.SUB_PACKAGE_ID and PROJECT_ID = b.PROJECT_ID) as PO_NUMBER,
                                 (select top 1 isnull(total_amount,0) from POScheduleDetails 
                                 where project_id = B.PROJECT_ID and BUDGET_ID = B.BUDGET_ID and PACKAGE_ID = B.PACKAGE_ID and SUB_PACKAGE_ID = B.SUB_PACKAGE_ID) as VENDOR_PO_AMOUNT
                                FROM budgets B 
                                INNER JOIN cm_category C1 ON C1.CategoryId = B.PACKAGE_ID
                                LEFT JOIN cm_category C2 ON C2.CategoryId = B.SUB_PACKAGE_ID
                                WHERE PROJECT_ID = {projectId} AND BUDGET_ID = {budgetId}";
                }
                if (onlyApproved)
                {
                    query = $@"select B.*, C1.CategoryCode AS PACKAGE_NAME, C2.CategoryCode AS SUB_PACKAGE_NAME,
                            (select string_agg(concat(po.po_number,'$@',v.COMP_NAME),'@$') from POScheduleDetails po
								  inner join vendors v on po.VENDOR_ID = v.u_id
                                  where package_id = B.PACKAGE_ID and SUB_PACKAGE_ID = B.SUB_PACKAGE_ID and PROJECT_ID = b.PROJECT_ID) as PO_NUMBER,
                            (select top 1 isnull(total_amount,0) from POScheduleDetails 
                            where project_id = B.PROJECT_ID and BUDGET_ID = B.BUDGET_ID and PACKAGE_ID = B.PACKAGE_ID and SUB_PACKAGE_ID = B.SUB_PACKAGE_ID) as VENDOR_PO_AMOUNT 
                            from budgets B
                            INNER JOIN cm_category C1 ON C1.CategoryId = B.PACKAGE_ID
                            INNER JOIN cm_category C2 ON C2.CategoryId = B.SUB_PACKAGE_ID
                            WHERE SUB_PACKAGE_WF_APPROVAL_STATUS = 'APPROVED' 
                            and ISNULL(SUB_PACKAGE_WF_PENDING_APPROVERS,'') = '' 
                            AND ISNULL(VENDOR_IDS,'') <> '' 
                            AND PROJECT_ID = {projectId}";
                }

                if (!string.IsNullOrWhiteSpace(subPackageIds))
                {
                    query += $" AND SUB_PACKAGE_ID IN ({subPackageIds})";
                }

                var dt = sqlHelper.SelectQuery(query);
                details = mapper.Map(dt).ToList();

                foreach (var budgetDetail in details)
                {
                    budgetDetail.IS_BUDGET_APPROVED = false;
                    if (budgetDetail.BUDGET_WF_ID > 0 && budgetDetail.BUDGET_WF_APPROVAL_STATUS == "APPROVED" && string.IsNullOrEmpty(budgetDetail.BUDGET_WF_PENDING_APPROVERS)) 
                    {
                        budgetDetail.IS_BUDGET_APPROVED = true;
                    }

                    if (!string.IsNullOrEmpty(budgetDetail.VENDOR_IDS))
                    {
                        DataSet ds = sqlHelper.ExecuteQuery($"select (SELECT U_ID,COMP_NAME,U_EMAIL,U_PHONE FROM vendors WHERE u_id in ({budgetDetail.VENDOR_IDS}) AND IS_PRIMARY = 1 FOR JSON PATH) as VENDORS_LIST");
                        if (ds != null && ds.Tables[0].Rows.Count > 0)
                        {
                            budgetDetail.Vendors = Convert.ToString(ds.Tables[0].Rows[0]["VENDORS_LIST"]);
                        }
                    }

                    if (!string.IsNullOrEmpty(budgetDetail.PO_VENDOR_IDS))
                    {
                        DataSet ds = sqlHelper.ExecuteQuery($"select (SELECT U_ID,COMP_NAME,U_EMAIL,U_PHONE FROM vendors WHERE u_id in ({budgetDetail.PO_VENDOR_IDS}) AND IS_PRIMARY = 1 FOR JSON PATH) as VENDORS_LIST");
                        if (ds != null && ds.Tables[0].Rows.Count > 0)
                        {
                            budgetDetail.POVendors = Convert.ToString(ds.Tables[0].Rows[0]["VENDORS_LIST"]);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Error("Error in GetBudgetDetails()>>>", ex.Message);
            }

            return details;
        }

        public List<BudgetDetails> GetBudgetDetailsbyProjectAmendment(int projectId, int budgetId, int amendmentId, string sessionId)
        {
            List<BudgetDetails> details = new List<BudgetDetails>();
            try
            {
                Utilities.ValidateSession(sessionId);
                CORE.DataNamesMapper<BudgetDetails> mapper = new CORE.DataNamesMapper<BudgetDetails>();
                string query = $@"SELECT *, C1.CategoryCode AS PACKAGE_NAME, C2.CategoryCode AS SUB_PACKAGE_NAME,
                                  (select top 1 po_number from POScheduleDetails 
                                  where package_id = B.PACKAGE_ID and SUB_PACKAGE_ID = B.SUB_PACKAGE_ID and PROJECT_ID = b.PROJECT_ID) as PO_NUMBER,
                                  (select top 1 isnull(total_amount,0) from POScheduleDetails 
                                  where project_id = B.PROJECT_ID and BUDGET_ID = B.BUDGET_ID and PACKAGE_ID = B.PACKAGE_ID and SUB_PACKAGE_ID = B.SUB_PACKAGE_ID) as VENDOR_PO_AMOUNT
                                  FROM projectAmendment B
                                  INNER JOIN cm_category C1 ON C1.CategoryId = B.PACKAGE_ID
                                  LEFT JOIN cm_category C2 ON C2.CategoryId = B.SUB_PACKAGE_ID
                                  WHERE PROJECT_ID = {projectId} and BUDGET_ID = {budgetId} AND PROJECT_AMENDMENT_ID = {amendmentId}";
                var dt = sqlHelper.SelectQuery(query);
                details = mapper.Map(dt).ToList();

                foreach (var budgetDetail in details)
                {
                    budgetDetail.IS_BUDGET_APPROVED = false;
                    if (budgetDetail.BUDGET_WF_ID > 0 && budgetDetail.BUDGET_WF_APPROVAL_STATUS == "APPROVED" && string.IsNullOrEmpty(budgetDetail.BUDGET_WF_PENDING_APPROVERS))
                    {
                        budgetDetail.IS_BUDGET_APPROVED = true;
                    }

                    if (!string.IsNullOrEmpty(budgetDetail.VENDOR_IDS))
                    {
                        DataSet ds = sqlHelper.ExecuteQuery($"select (SELECT U_ID,COMP_NAME,U_EMAIL,U_PHONE FROM vendors WHERE u_id in ({budgetDetail.VENDOR_IDS}) AND IS_PRIMARY = 1 FOR JSON PATH) as VENDORS_LIST");
                        if (ds != null && ds.Tables[0].Rows.Count > 0)
                        {
                            budgetDetail.Vendors = Convert.ToString(ds.Tables[0].Rows[0]["VENDORS_LIST"]);
                        }
                    }

                    if (!string.IsNullOrEmpty(budgetDetail.PO_VENDOR_IDS))
                    {
                        DataSet ds = sqlHelper.ExecuteQuery($"select (SELECT U_ID,COMP_NAME,U_EMAIL,U_PHONE FROM vendors WHERE u_id in ({budgetDetail.PO_VENDOR_IDS}) AND IS_PRIMARY = 1 FOR JSON PATH) as VENDORS_LIST");
                        if (ds != null && ds.Tables[0].Rows.Count > 0)
                        {
                            budgetDetail.POVendors = Convert.ToString(ds.Tables[0].Rows[0]["VENDORS_LIST"]);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Error("Error in GetBudgetDetailsbyProjectAmendment()>>>", ex.Message);
            }

            return details;
        }

        public Response DeleteProject(int PROJECT_ID, string sessionID)
        {
            Response details = new Response();
            try
            {
                Utilities.ValidateSession(sessionID);
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                sd.Add("P_PROJECT_ID", PROJECT_ID);
                CORE.DataNamesMapper<Response> mapper = new CORE.DataNamesMapper<Response>();
                var dataset = sqlHelper.SelectList("proj_deleteProject", sd);
                details = mapper.Map(dataset.Tables[0]).FirstOrDefault();
            }
            catch (Exception ex)
            {
                logger.Error("Error in GetProjectDetails()>>>", ex.Message);
            }

            return details;
        }
        public Response UpdateBudgetWorkflow(int wID, int moduleID, int projectID, int user, string sessionid) {

            Response response = new Response();

            Utilities.ValidateSession(sessionid);
            try
            {
                if (wID > 0 && moduleID > 0 && projectID > 0)
                {
                    sqlHelper.ExecuteNonQuery_IUD($@"update budgets set BUDGET_WF_ID = {wID} WHERE BUDGET_ID = {moduleID} and PROJECT_ID = {projectID}");
                    PRMWFService pRMWF = new PRMWFService();
                    Response res2 = pRMWF.AssignWorkflow(wID, Convert.ToInt32(moduleID), user, sessionid);
                }
                else {
                    logger.Error("error in UpdateBudgetWorkflow--->" + "wID:"+ wID + "moduleID:" + moduleID + "projectID:" + projectID);
                }
            }
            catch (Exception ex)
            {
                logger.Error("Error in UpdateBudgetWorkflow()>>>", ex.Message);
            }


            return response;
        }
        public Response UpdateVendorWorkflow(List<BudgetDetails> budgetDetailsList, int projectId, int budgetId, string sessionid)
        {

            Response response = new Response();

            Utilities.ValidateSession(sessionid);
            try
            {

                if (budgetDetailsList !=null && budgetDetailsList.Count > 0) 
                {
                    string query = string.Empty;
                    foreach (BudgetDetails budgetdetails in  budgetDetailsList) 
                    {
                        query = $@"update budgets set SUB_PACKAGE_WF_ID = {budgetdetails.SUB_PACKAGE_WF_ID},VENDOR_IDS = '{budgetdetails.VENDOR_IDS}',
                                    DATE_MODIFIED = GETUTCDATE(),MODIFIED_BY = {budgetdetails.MODIFIED_BY} WHERE ROW_ID = {budgetdetails.ROW_ID};";
                        sqlHelper.ExecuteNonQuery_IUD(query);
                        if (budgetdetails.SUB_PACKAGE_WF_ID > 0 && budgetdetails.ROW_ID > 0)
                        {
                            PRMWFService pRMWF = new PRMWFService();
                            Response res2 = pRMWF.AssignWorkflow(budgetdetails.SUB_PACKAGE_WF_ID, Convert.ToInt32(budgetdetails.ROW_ID), budgetdetails.U_ID, sessionid);
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                logger.Error("Error in UpdateBudgetWorkflow()>>>", ex.Message);
            }


            return response;
        }
        public Response UpdateBudgetDetails(List<BudgetDetails> details, string sessionid)
        {
            Response response = new Response();
            Utilities.ValidateSession(sessionid);

            try
            {
                foreach(var detail in details)
                {
                    SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                    sd.Add("P_COMP_ID", detail.COMP_ID);
                    sd.Add("P_QCS_ID", detail.QCS_ID);
                    sd.Add("P_REQ_ID", detail.REQ_ID);
                    sd.Add("P_PACKAGE_NAME", detail.PACKAGE_NAME);
                    sd.Add("P_SUB_PACKAGE_NAME", detail.SUB_PACKAGE_NAME);
                    sd.Add("P_QCS_PO_VALUE", detail.QCS_PO_VALUE);
                    sd.Add("P_PO_VENDOR_IDS", detail.PO_VENDOR_IDS);
                    DataSet ds = sqlHelper.SelectList("cp_UpdateBudgetPODetails", sd);
                }                
            }
            catch (Exception ex)
            {
                response.ErrorMessage = ex.Message;
            }

            return response;
        }
        //public List<BudgetDetails> GetPackAndSubPackNames(string poNumber,int PROJECT_ID, int PACKAGE_ID, int SUB_PACKAGE_ID, string sessionid) {

        //    List<BudgetDetails> budgets = new List<BudgetDetails>();
        //    try
        //    {
        //        Utilities.ValidateSession(sessionid);
        //        CORE.DataNamesMapper<BudgetDetails> mapper = new CORE.DataNamesMapper<BudgetDetails>();
        //        string query = $@"SELECT *, C1.CategoryCode AS PACKAGE_NAME, C2.CategoryCode AS SUB_PACKAGE_NAME FROM budgets B
        //                          INNER JOIN cm_category C1 ON C1.CategoryId = B.PACKAGE_ID
        //                          LEFT JOIN cm_category C2 ON C2.CategoryId = B.SUB_PACKAGE_ID
        //                          WHERE PO_NUMBER = {poNumber} AND PROJECT_ID = {PROJECT_ID}";

        //        if (PROJECT_ID > 0  && PACKAGE_ID > 0 && SUB_PACKAGE_ID > 0) {
        //            query += $@"AND PACKAGE_ID = {PACKAGE_ID} AND SUB_PACKAGE_ID = {SUB_PACKAGE_ID}";
        //        }

        //        var dt = sqlHelper.SelectQuery(query);
        //        budgets = mapper.Map(dt).ToList();
        //    }
        //    catch (Exception ex) 
        //    {
        //        logger.Error("Error in GetPackAndSubPackNames()>>>", ex.Message);
        //    }


        //    return budgets;
        //}
        public bool AreValidToPostRFQS(int PROJECT_ID, string SUB_PACKAGE_IDS, string sessionid)
        {
            Utilities.ValidateSession(sessionid);
            bool isValid = true;
            //ssqlHelper.ExecuteNonQuery_IUD($@"select ");

            string query = $@"select *  from budgets where PROJECT_ID = {PROJECT_ID} and SUB_PACKAGE_ID IN ({SUB_PACKAGE_IDS}) and  REQ_ID > 0;";

            var tempTable = sqlHelper.ExecuteQuery(query);
            if (tempTable != null && tempTable.Tables.Count > 0 && tempTable.Tables[0].Rows.Count > 0)
            {
                 isValid = false;
            }

            return isValid;
        }
        public Response AssignPackageApproval(List<BudgetDetails> details, string sessionid) 
        {
            Response response = new Response();
            if (details!=null && details.Count >  0) 
            {
                Utilities.ValidateSession(sessionid);
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                int PACKAGE_APPROVAL_ID = 0;
                var subpackageDetails = details.OrderBy(y => y.PACKAGE_APPROVAL_ID == 0);
                foreach (BudgetDetails budgetdetails in subpackageDetails)
                {
                    sd = new SortedDictionary<object, object>() { };
                    sd.Add("P_QCS_ID", budgetdetails.QCS_ID);
                    sd.Add("P_PACKAGE_APPROVAL_WF_ID", budgetdetails.PACKAGE_APPROVAL_WF_ID);
                    sd.Add("P_PACKAGE_APPROVAL_ID", budgetdetails.PACKAGE_APPROVAL_ID);
                    var dataset = sqlHelper.SelectList("proj_UpdatePackageApproval", sd);

                    if (dataset.Tables.Count > 0 && dataset.Tables[0].Rows.Count > 0)
                    {
                        //if (budgetdetails.PACKAGE_APPROVAL_ID == 0)
                        //{
                            PACKAGE_APPROVAL_ID = Convert.ToInt32(dataset.Tables[0].Rows[0]["PACKAGE_APPROVAL_ID"]);
                        //}
                    }
                }

                if (PACKAGE_APPROVAL_ID > 0) 
                {
                    PRMWFService pRMWF = new PRMWFService();
                    Response res2 = pRMWF.AssignWorkflow(details[0].PACKAGE_APPROVAL_WF_ID, Convert.ToInt32(PACKAGE_APPROVAL_ID), details[0].U_ID, sessionid);
                }
                response.ObjectID = PACKAGE_APPROVAL_ID;
            }
            return response;
        }
        public List<BudgetDetails> GetPackageApprovalList(int COMP_ID,int USER_ID, string PROJECT_ID, string COST_CENTER,
            string LOCATION, string STATUS, string sessionid, int PageSize = 0, int NumberOfRecords = 0) 
        {
            List<BudgetDetails> budgetList = new List<BudgetDetails>();
            try
            {
                Utilities.ValidateSession(sessionid);
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                sd.Add("P_COMP_ID", COMP_ID);
                sd.Add("P_USER_ID", USER_ID);
                sd.Add("P_PROJ_ID", PROJECT_ID);
                sd.Add("P_COST_CENTER", COST_CENTER);
                sd.Add("P_LOCATION", LOCATION);
                sd.Add("P_SEARCH", STATUS);
                sd.Add("P_PAGE", PageSize);
                sd.Add("P_PAGE_SIZE", NumberOfRecords);
                CORE.DataNamesMapper<BudgetDetails> mapper = new CORE.DataNamesMapper<BudgetDetails>();
                var dataset = sqlHelper.SelectList("proj_GetPackageApprovalsList", sd);
                budgetList = mapper.Map(dataset.Tables[0]).ToList();
            }
            catch (Exception ex) {
                logger.Error(ex, ex.Message);
            }
            
            return budgetList;
        }
        public List<BudgetDetails> GetApprovedPackages(string subpackageIds, int projectId) 
        {
            
            List<BudgetDetails> details = new List<BudgetDetails>();
            try
            {
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                sd.Add("P_SUB_PACKAGE_IDS", subpackageIds);
                sd.Add("P_PROJECT_ID", projectId);
                CORE.DataNamesMapper<BudgetDetails> mapper = new CORE.DataNamesMapper<BudgetDetails>();
                var dataset = sqlHelper.SelectList("proj_GetApprovedPackages", sd);
                details = mapper.Map(dataset.Tables[0]).ToList();
            }
            catch (Exception ex) 
            {
                logger.Error(ex, ex.Message);
            }
            
            return details;
        }
        public string GetProjectAudit(int COMP_ID, int PROJECT_ID, string sessionid) 
        {
            string json = null;
            try
            {
                Utilities.ValidateSession(sessionid);
                CORE.DataNamesMapper<BillRequests> mapper = new CORE.DataNamesMapper<BillRequests>();
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                sd.Add("P_COMP_ID", COMP_ID);
                sd.Add("P_PROJECT_ID", PROJECT_ID);
                DataSet dataSet = sqlHelper.SelectList("proj_GetProjectAudit", sd);
                if (dataSet != null && dataSet.Tables.Count > 0 && dataSet.Tables[0].Rows.Count > 0)
                {
                    json = JsonConvert.SerializeObject(dataSet, Formatting.Indented);
                }

            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error in retrieving Data");
            }

            return json;

        }

        public string GetBudgetAudit(int COMP_ID, int BUDGET_ID, string sessionid)
        {
            string json = null;
            try
            {
                Utilities.ValidateSession(sessionid);
                CORE.DataNamesMapper<BillRequests> mapper = new CORE.DataNamesMapper<BillRequests>();
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                sd.Add("P_COMP_ID", COMP_ID);
                //sd.Add("P_PROJECT_ID", PROJECT_ID);
                sd.Add("P_BUDGET_ID", BUDGET_ID);
                DataSet dataSet = sqlHelper.SelectList("proj_GetBudgetAudit", sd);
                if (dataSet != null && dataSet.Tables.Count > 0 && dataSet.Tables[0].Rows.Count > 0)
                {
                    json = JsonConvert.SerializeObject(dataSet, Formatting.Indented);
                }

            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error in retrieving Data");
            }

            return json;

        }

        public string GetProjectBillToDetails(int U_ID, int COMP_ID, string searchString, string sessionID)
        {
            string json = null;
            try
            {
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                Utilities.ValidateSession(sessionID);
                sd.Add("P_U_ID", U_ID);
                sd.Add("P_COMP_ID", COMP_ID);
                sd.Add("P_SEARCH_STRING", searchString);
                DataSet dataSet = sqlHelper.SelectList("proj_GetProjectBillToDetails", sd);
                if (dataSet != null && dataSet.Tables.Count > 0 && dataSet.Tables[0].Rows.Count > 0)
                {
                    json = JsonConvert.SerializeObject(dataSet, Formatting.Indented);
                }
            }
            catch (Exception ex)
            {
                logger.Error("error in GetProjectBillToDetails() is>>>" + ex.Message);
            }

            return json;
        }

        public Response SaveProjectBillToDetails(BillTo billTo, string sessionid)
        {

            Response details = new Response();

            try
            {
                Utilities.ValidateSession(sessionid);
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };

                sd.Add("P_BILL_TO_ID", billTo.BILL_TO_ID);
                sd.Add("P_COMP_ID", billTo.COMP_ID);
                sd.Add("P_LEGAL_ENTITY_NAME", billTo.LEGAL_ENTITY_NAME);
                sd.Add("P_ADDRESS", billTo.ADDRESS);
                sd.Add("P_SPOC_EMAIL_ADDRESS", billTo.SPOC_EMAIL_ADDRESS);
                sd.Add("P_GSTIN", billTo.GSTIN);
                sd.Add("P_PAN", billTo.PAN);
                sd.Add("P_U_ID", billTo.U_ID);

                CORE.DataNamesMapper<Response> mapper = new CORE.DataNamesMapper<Response>();
                var dataset = sqlHelper.SelectList("proj_SaveProjectBillToDetails", sd);
                details = mapper.Map(dataset.Tables[0]).FirstOrDefault();

                if (dataset != null && dataset.Tables.Count > 0 && dataset.Tables[0].Rows.Count > 0 && dataset.Tables[0].Rows[0][0] != null)
                {
                    details.ObjectID = dataset.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToInt32(dataset.Tables[0].Rows[0][0].ToString()) : -1;
                    details.ErrorMessage = dataset.Tables[0].Rows[0][1] != DBNull.Value ? Convert.ToString(dataset.Tables[0].Rows[0][1].ToString()) : string.Empty;
                }

            }
            catch (Exception ex)
            {
                logger.Error("Error in SaveProjectBillToDetails()>>>" + ex.Message);
            }
            return details;
        }

        public List<ProjectDashboard> GetProjectDashboardFilters(int COMP_ID, int USER_ID, string FROM_DATE, string TO_DATE, string DEPT_ID, string DESIG_ID, string sessionid)
        {
            List<ProjectDashboard> details = new List<ProjectDashboard>();
            try
            {
                Utilities.ValidateSession(sessionid);
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                sd.Add("P_COMP_ID", COMP_ID);
                sd.Add("P_U_ID", USER_ID);
                sd.Add("P_FROM_DATE", FROM_DATE);
                sd.Add("P_TO_DATE", TO_DATE);
                sd.Add("P_DEPT_ID", DEPT_ID);
                sd.Add("P_DESIG_ID", DESIG_ID);
                CORE.DataNamesMapper<ProjectDashboard> mapper = new CORE.DataNamesMapper<ProjectDashboard>();
                var dataset = sqlHelper.SelectList("proj_GetDashboardFilters", sd);
                details = mapper.Map(dataset.Tables[0]).ToList();
            }
            catch (Exception ex) 
            {
                logger.Error("error in GetProjectDashboardFilters()>>>" + ex.Message);
            }

            return details;
        }

        public ProjectDashboard GetProjectDashboardStats(int COMP_ID, int USER_ID, string costCenter, string location, string clientName, string projectCode, string projectId, string fromDate, string toDate, string sessionid,int type, string DEPT_ID, string DESIG_ID)
        {
            ProjectDashboard details = new ProjectDashboard();
            try
            {
                Utilities.ValidateSession(sessionid);
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                sd.Add("P_COMP_ID", COMP_ID);
                sd.Add("P_U_ID", USER_ID);
                sd.Add("P_FROM_DATE", fromDate);
                sd.Add("P_TO_DATE", toDate);
                sd.Add("P_COST_CENTER", costCenter);
                sd.Add("P_LOCATION", location);
                sd.Add("P_CLIENT_NAME", clientName);
                sd.Add("P_PROJECT_CODE", projectCode);
                sd.Add("P_PROJECT_ID", projectId);
                if (type == 0)
                {
                    CORE.DataNamesMapper<ProjectDashboard> mapper = new CORE.DataNamesMapper<ProjectDashboard>();
                    CORE.DataNamesMapper<BuyerVolume> mapper1 = new CORE.DataNamesMapper<BuyerVolume>();
                    CORE.DataNamesMapper<PackageVolume> mapper2 = new CORE.DataNamesMapper<PackageVolume>();
                    CORE.DataNamesMapper<RevenueDetails> mapper3 = new CORE.DataNamesMapper<RevenueDetails>();
                    CORE.DataNamesMapper<PackageVolume> mapper4 = new CORE.DataNamesMapper<PackageVolume>();
                    CORE.DataNamesMapper<RevenueStats> mapper5 = new CORE.DataNamesMapper<RevenueStats>();
                    var dataset = sqlHelper.SelectList("proj_GetProjectDashboardStats_1", sd);
                    details = mapper.Map(dataset.Tables[0]).FirstOrDefault();
                    details.PURCHASE_VOL_BY_PROCUREMENT = mapper1.Map(dataset.Tables[1]).ToList();
                    details.PACKAGES_VOLUMES = mapper2.Map(dataset.Tables[2]).ToList();
                    details.REVENUE_DETAILS = mapper3.Map(dataset.Tables[3]).ToList();
                    details.PACKAGES_SPENT = mapper4.Map(dataset.Tables[4]).ToList();
                    details.REVENUE_STATS = mapper5.Map(dataset.Tables[5]).ToList();
                }
                else {
                    sd.Add("P_DEPT_ID", DEPT_ID);
                    sd.Add("P_DESIG_ID", DESIG_ID);
                    CORE.DataNamesMapper<ProjectDashboard> mapper4 = new CORE.DataNamesMapper<ProjectDashboard>();
                    CORE.DataNamesMapper<MonthlyAvgSavings> mapper5 = new CORE.DataNamesMapper<MonthlyAvgSavings>();
                    CORE.DataNamesMapper<TopSupplier> mapper6 = new CORE.DataNamesMapper<TopSupplier>();
                    CORE.DataNamesMapper<TransactionalTrend> mapper7 = new CORE.DataNamesMapper<TransactionalTrend>();
                    CORE.DataNamesMapper<TopPerformer> mapper8 = new CORE.DataNamesMapper<TopPerformer>();
                    CORE.DataNamesMapper<LiveRequirements> mapper9 = new CORE.DataNamesMapper<LiveRequirements>();
                    CORE.DataNamesMapper<ProjDetails> mapper10 = new CORE.DataNamesMapper<ProjDetails>();
                    CORE.DataNamesMapper<ReleasedPOCount> mapper11 = new CORE.DataNamesMapper<ReleasedPOCount>();
                    CORE.DataNamesMapper<PendingBills> mapper12 = new CORE.DataNamesMapper<PendingBills>();
                    CORE.DataNamesMapper<MonthlyAvgUtilizaion> mapper13 = new CORE.DataNamesMapper<MonthlyAvgUtilizaion>();
                    CORE.DataNamesMapper<PendingInvoiceApproval> mapper14 = new CORE.DataNamesMapper<PendingInvoiceApproval>();
                    var dataset = sqlHelper.SelectList("proj_GetUserDashboardStats_development", sd);
                    details = mapper4.Map(dataset.Tables[0]).FirstOrDefault();
                    //List<PArrayKeyValue> stats = new List<PArrayKeyValue>();

                    //if (dataset != null && dataset.Tables.Count > 0 && dataset.Tables[2].Rows.Count > 0)
                    //{
                    //    details.TREND_MONTHS = new List<string>();
                    //    details.MY_REQ_COUNT = new List<int>();
                    //    details.TOTAL_REQ_COUNT = new List<int>();
                    //    foreach (var row in dataset.Tables[0].AsEnumerable())
                    //    {
                    //        string month = row["MONTH_NAME"] != DBNull.Value ? Convert.ToString(row["MONTH_NAME"]) : string.Empty;
                    //        int myReqCount = row["MY_REQ_COUNT"] != DBNull.Value ? Convert.ToInt32(row["MY_REQ_COUNT"]) : 0;
                    //        int totalReqCount = row["TOTAL_REQ_COUNT"] != DBNull.Value ? Convert.ToInt32(row["TOTAL_REQ_COUNT"]) : 0;
                    //        details.TREND_MONTHS.Add(month);
                    //        details.MY_REQ_COUNT.Add(myReqCount);
                    //        details.TOTAL_REQ_COUNT.Add(totalReqCount);
                    //    }
                    //}
                    if (dataset.Tables[1].Rows.Count > 0)
                    {
                        details.TOP_SUPPLIER_BY_COST_CENTRE = mapper6.Map(dataset.Tables[1]).ToList();
                    }
                    if (dataset.Tables[2].Rows.Count > 0)
                    {
                        details.TRANSACTIONAL_TREND = mapper7.Map(dataset.Tables[2]).ToList();
                    }
                    if (dataset.Tables[3].Rows.Count > 0)
                    {
                        details.MONTHLY_AVG_SAVINGS = mapper5.Map(dataset.Tables[3]).ToList();
                    }
                    if (dataset.Tables[4].Rows.Count > 0)
                    {
                        details.TOP_PERFORMER = mapper8.Map(dataset.Tables[4]).ToList();
                    }
                    if (dataset.Tables[5].Rows.Count > 0)
                    {
                        details.LIVE_REQUIREMENTS = mapper9.Map(dataset.Tables[5]).ToList();
                    }
                    if (dataset.Tables[6].Rows.Count > 0)
                    {
                        details.PROJECT_DETAILS = mapper10.Map(dataset.Tables[6]).ToList();
                    }
                    if (dataset.Tables[7].Rows.Count > 0)
                    {
                        details.RELEASEDPO_COUNT = mapper11.Map(dataset.Tables[7]).ToList();
                    }
                    if (dataset.Tables[8].Rows.Count > 0)
                    {
                        details.PENDING_BILLS_COUNT = mapper12.Map(dataset.Tables[8]).ToList();
                    }
                    if (dataset.Tables[9].Rows.Count > 0)
                    {
                        //details.MONTHLY_AVG_UTILIZATION = mapper13.Map(dataset.Tables[9]).ToList();
                        List<string> months = new List<string>();
                        List<ChartData> chartSeries = new List<ChartData>();
                        foreach (var row in dataset.Tables[9].AsEnumerable())
                        {
                            string month = row["MONTH_YEAR"] != DBNull.Value ? Convert.ToString(row["MONTH_YEAR"]) : string.Empty;
                            if (!months.Contains(month))
                            {
                                months.Add(month);
                            }

                            string status = row["REQ_STATUS"] != DBNull.Value ? Convert.ToString(row["REQ_STATUS"]) : string.Empty;
                            int reqCount = row["REQ_COUNT"] != DBNull.Value ? Convert.ToInt32(row["REQ_COUNT"]) : 0;
                            if (!chartSeries.Any(c => c.Name == status))
                            {
                                ChartData data = new ChartData();
                                data.Name = status;
                                data.Data = new List<int>();
                                data.Data.Add(reqCount);
                                chartSeries.Add(data);
                            }
                            else
                            {
                                chartSeries.First(c => c.Name == status).Data.Add(reqCount);
                            }
                        }

                        details.ChartSeries = chartSeries.ToArray();
                        details.Months = months.ToArray();
                    }
                    if (dataset.Tables[10].Rows.Count > 0)
                    {
                        details.PENDING_INVOICE_APPROVAL = mapper14.Map(dataset.Tables[10]).ToList();
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Error("error in GetProjectDashboardStats()>>>" + ex.Message);
            }

            return details;
        }

        #endregion Services

    }

}