prmApp
    .controller('saveProjectDetailsCtrl', ["$scope", "$stateParams", "$log", "$state", "$window", "userService", "auctionsService",
        "storeService", "growlService", "poService", "$rootScope", "catalogService", "fileReader", "PRMProjectServices","$filter",
        function ($scope, $stateParams, $log, $state, $window, userService, auctionsService,
            storeService, growlService,  poService, $rootScope, catalogService,
            fileReader, PRMProjectServices, $filter) {

            $scope.userID = userService.getUserId();
            $scope.sessionID = userService.getUserToken();
            $scope.COMP_ID = userService.getUserCompanyId();
            $scope.isSuperUser = userService.getUserObj().isSuperUser;
            $scope.PROJECT_ID = $stateParams.Id;
            $scope.ProjectObject = $stateParams.detailsObj ? $stateParams.detailsObj : "";


            $scope.expand = false;
            $scope.expand1 = false;
            $scope.expand2 = false;

            $scope.PROJECT_DET = {
                COMP_ID: +$scope.COMP_ID,
                U_ID: +$scope.userID,
                PROJ_ID: 0,
                PROJECT_NAME: '',
                COST_CENTER: '',
                LOCATION: '',
                PO_VALUE: '',
                PO_NUMBER: '',
                NET_REVENUE_MARGIN: '',
                SHIP_TO_ADDRESS: '',
                BILL_TO_ADDRESS: '',
                TARGET_BUDGET: '',
                TARGETED_REVENUE_BUDGET: 0,
                IS_VALID: 1
               

            };


            $scope.ProjectObject = $stateParams.detailsObj ? $stateParams.detailsObj : "";

            $scope.editConfiguration = function (Id)
            {
                if (Id == 0)
                {
                    $scope.addnewconfigView = true;
                }
            }

            $scope.GetCompanyDepartments = function () {
                auctionsService.GetCompanyDepartments($scope.userID, $scope.sessionID)
                    .then(function (response) {
                        $scope.companyDepartments = response;

                        if ($scope.PROJECT_DET.DEPT_IDS_STR && $scope.PROJECT_DET.DEPT_IDS.length <= 0) {
                            let tempDeptIds = $scope.PROJECT_DET.DEPT_IDS_STR.split(',');
                            tempDeptIds.forEach(function (deptId, index) {
                                $scope.PROJECT_DET.DEPT_IDS.push(+deptId);
                            });
                        }

                        $scope.companyDepartments.forEach(function (compDept, userIndex) {
                            if ($scope.PROJECT_DET.DEPT_IDS && $scope.PROJECT_DET.DEPT_IDS.length > 0 && $scope.PROJECT_DET.DEPT_IDS.includes(compDept.deptID)) {
                                $scope.PROJECT_DET.SELECTED_DEPTS.push(' ' + compDept.deptCode);
                            }
                        });

                        $scope.getSubUserData();
                    });
            };

            $scope.closeEditConfiguration = function () {
               // $scope.addnewconfigView = false;
                $scope.ProjectObject = {
                    COMP_ID: +$scope.COMP_ID,
                    PROJECT_ID: '',
                    PROJECT_NAME: '',
                    COST_CENTER: '',
                    LOCATION: '',
                    COLLIEAR_PO_VALUE: '',
                    PO_NUMBER: '',
                    NET_REVENUE_MARGIN: '',
                    SHIP_TO_ADDRESS: '',
                    BILL_TO_ADDRESS: '',
                    TARGETED_REVENUE_BUDGET: '',
                    CREATED_BY: +$scope.USER_ID

                }
                $state.go("projectDetails");

            };
            $scope.showPackage = function (Id) {
                $scope.expand = false;
                if (Id == 0) {
                    $scope.expand = true;
                } else if (Id == 1) {
                    $scope.expand = false;
                }
            }

            $scope.showPackage1 = function (Id) {
                $scope.expand1 = false;
                if (Id == 0) {
                    $scope.expand1 = true;
                } else if (Id == 1) {
                    $scope.expand1 = false;
                }
            }

            $scope.showPackage2 = function (Id) {
                $scope.expand2 = false;
                if (Id == 0) {
                    $scope.expand2 = true;
                } else if (Id == 1) {
                    $scope.expand2 = false;
                }
            }

            $scope.goToProjectEdit = function () {             
                    var url = $state.href("projectDetails");
                    window.open(url, '_blank');                
            };

            $scope.showallitems = false;
            $scope.showitem = false;

            $scope.checkAllitems = function (value) {
                if (value) {
                    $scope.showitem = true;
                } else {
                    $scope.showitem = false;
                }
          

            }
            $scope.showallitems1 = false;
            $scope.showitem1 = false;

            $scope.checkAllitems1 = function (value) {
                if (value) {
                    $scope.showitem1 = true;
                } else {
                    $scope.showitem1 = false;
                }


            }

            $scope.showSetupBudget = function (budgetID) {
                $state.go("setupBudget", { "projectId": +$scope.PROJECT_DET.PROJECT_ID, "budgetId": budgetID })
            };


            $scope.goToProjectEdit1 = function (Id) {
                if (Id == 0) {
                    $state.go("projectDetails", { "Id": Id });
                   
                }
            }



            $scope.saveProjectDetails1 = function () {

                var params = {                   
                    "projectdetails": $scope.PROJECT_DET,
                    "sessionid": userService.getUserToken()
                };
                
                PRMProjectServices.SaveProjectDetails(params)
                    .then(function (response) {
                        if (response.errorMessage) {
                            growlService.growl(response.errorMessage, "inverse");
                        } else {
                            growlService.growl('Project Created Successfully.', "success");
                        };
                    });
              
                $scope.ProjectObject = {};               
            };


            $scope.GetProjectDetails = function (id) {
                var params =
                {
                    "PROJECT_ID": id,
                    "sessionid": userService.getUserToken()
                };

                PRMProjectServices.GetProjectDetails(params)
                    .then(function (response) {
                        $scope.PROJECT_DET = response;
                        $scope.PROJECT_DET.SELECTED_DEPTS = [];
                        $scope.PROJECT_DET.ASSIGNED_USERS = [];
                        //$scope.getSubUserData();
                        $scope.GetCompanyDepartments();
                        if ($scope.PROJECT_DET.BILL_TO_ADDRESS) {
                            $scope.PROJECT_DET.BILL_TO_ADDRESS_JSON = $scope.PROJECT_DET.BILL_TO_ADDRESS;
                            $scope.PROJECT_DET.BILL_TO_ADDRESS = JSON.parse($scope.PROJECT_DET.BILL_TO_ADDRESS).ADDRESS;
                        }

                        $scope.PROJECT_DET.TARGETED_REVENUE_BUDGET_TEMP = $scope.PROJECT_DET.TARGETED_REVENUE_BUDGET;
                    });
            };

            $scope.getSubUserData = function () {
                userService.getSubUsersData({ "userid": userService.getUserId(), "sessionid": userService.getUserToken() })
                    .then(function (response) {

                        if ($scope.PROJECT_DET.ASSIGNED_U_IDS_STR && $scope.PROJECT_DET.ASSIGNED_U_IDS.length <= 0) {
                            let tempUids = $scope.PROJECT_DET.ASSIGNED_U_IDS_STR.split(',');
                            tempUids.forEach(function (userId, index) {
                                $scope.PROJECT_DET.ASSIGNED_U_IDS.push(+userId);
                            });
                        }

                        $scope.subUsers = $filter('filter')(response, { isValid: true });
                        $scope.inactiveSubUsers = $filter('filter')(response, { isValid: false });

                        $scope.subUsers.forEach(function (user, userIndex) {
                            user.userID = +user.userID;
                            user.dateFrom = userService.toLocalDate(user.dateFrom);
                            user.dateTo = userService.toLocalDate(user.dateTo);

                            if ($scope.PROJECT_DET.ASSIGNED_U_IDS && $scope.PROJECT_DET.ASSIGNED_U_IDS.length > 0 && $scope.PROJECT_DET.ASSIGNED_U_IDS.includes(user.userID)) {
                                $scope.PROJECT_DET.ASSIGNED_USERS.push(' ' + user.firstName + ' ' + user.lastName + ' ');
                            }
                        });

                        $scope.subUsers1 = $filter('filter')(response, { isValid: true });

                        $scope.deptChange();
                    });
            };


            if (+$scope.PROJECT_ID > 0) {
                $scope.GetProjectDetails(+$scope.PROJECT_ID);
            } else {
                $scope.GetCompanyDepartments();
            }

            $scope.getStyles = function () {
                return 'width:325px;height: ' + angular.element('#productTable')[0].offsetHeight + 'px;max-height: 400px; overflow-y: auto;';
            };

            $scope.deptChange = function () {
                $scope.PROJECT_DET.DEPT_IDS = [];
                if ($scope.PROJECT_DET.SELECTED_DEPTS && $scope.PROJECT_DET.SELECTED_DEPTS.length > 0) {
                    $scope.PROJECT_DET.SELECTED_DEPTS.forEach(function (selectedDept, index) {
                        if (!$scope.PROJECT_DET.DEPT_IDS.includes(selectedDept.deptID)) {
                            $scope.PROJECT_DET.DEPT_IDS.push(selectedDept.deptID)
                        }
                    });
                }

                $scope.filteredSubUsers = [];
                if ($scope.subUsers && $scope.subUsers.length > 0) {
                    $scope.filteredSubUsers = $scope.subUsers.filter(function (subUser) {
                        return subUser.DEPARTMENT_ID_LIST && $scope.PROJECT_DET.DEPT_IDS.some(r => subUser.DEPARTMENT_ID_LIST.includes(r));
                    });
                }

                console.log($scope.filteredSubUsers);
            };

        }]);
