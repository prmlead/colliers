prmApp
    .controller('approvedVendorProjectCtrl', ["$scope", "$stateParams", "$log", "$state", "$window", "userService", "auctionsService",
        "storeService", "growlService", "poService", "$rootScope", "catalogService", "fileReader","PRMProjectUploadServices",
        function ($scope, $stateParams, $log, $state, $window, userService, auctionsService,
            storeService, growlService,  poService, $rootScope, catalogService,
            fileReader, PRMProjectUploadServices) {

            $scope.userID = userService.getUserId();
            $scope.sessionID = userService.getUserToken();
            $scope.COMP_ID = userService.getUserCompanyId();
            $scope.isSuperUser = userService.getUserObj().isSuperUser;
            $scope.BC_ID = $stateParams.Id;
            $scope.ProjectObject = $stateParams.detailsObj ? $stateParams.detailsObj : "";

            $scope.addnewconfigView = false;
            $scope.expand = false;
            $scope.expand1 = false;
            $scope.expand2 = false;

            $scope.ProjectObject = {
                COMP_ID: $scope.COMP_ID,
                PROJECT_ID: '',
                PROJECT_NAME: '',
                COST_CENTER: '',
                LOCATION: '',
                COLLIEAR_PO_VALUE: '',
                PO_NUMBER: '',
                NET_REVENUE_MARGIN: '',
                SHIP_TO_ADDRESS: '',
                BILL_TO_ADDRESS: '',
                TARGETED_REVENUE_BUDGET: '',
                CREATED_BY: $scope.USER_ID

            };

            $scope.BudgetCodes = [
                {
                    PACKAGE: [
                        { inner_package: "LOW CURRENT WORKS" },
                        { inner_package: "CIVIL INTERIORS" },
                        { inner_package: "HVAC" },
                        { inner_package: "CONSULTANT" },
                        { inner_package: "FIRE FIGHTIMG WORK" }
                    ],
                    COLLIEAR_PO_VALUE: 3000,
                    NET_REVENUE_MARGIN: 20,
                    BUDGET_AMOUNT: 5000,
                    Sub_package: [
                        {
                            SUB_PACKAGE: "Earthing",
                            ALLOCATION: 60,
                            EST_BUDGET_AMOUNT: 1800,
                            PO_VALUE: '',
                            VARIANCE: ''
                        },
                        {
                            SUB_PACKAGE: "RaceWay",
                            ALLOCATION: 40,
                            EST_BUDGET_AMOUNT: 1200,
                            PO_VALUE: '',
                            VARIANCE: ''
                        },

                    ]
                },
                {
                    PACKAGE: [
                        { inner_package: "CIVIL INTERIORS" },
                        { inner_package: "HVAC" },
                        { inner_package: "CONSULTANT" },
                        { inner_package: "FIRE FIGHTIMG WORK" },
                        { inner_package: "LOW CURRENT WORKS " }
                    ],
                    COLLIEAR_PO_VALUE: 5000,
                    NET_REVENUE_MARGIN: 30,
                    BUDGET_AMOUNT: 5000,
                    Sub_package: [
                        {
                            SUB_PACKAGE: "Earthing",
                            ALLOCATION: 60,
                            EST_BUDGET_AMOUNT: 1800,
                            PO_VALUE: '',
                            VARIANCE: ''
                        },
                        {
                            SUB_PACKAGE: "RaceWay",
                            ALLOCATION: 40,
                            EST_BUDGET_AMOUNT: 1200,
                            PO_VALUE: '',
                            VARIANCE: ''
                        },

                    ]
                },
                {
                    PACKAGE: [
                        { inner_package: "Contigency" }
                    ],
                    COLLIEAR_PO_VALUE: 1000,
                }

            ];




            $scope.ProjectObject = $stateParams.detailsObj ? $stateParams.detailsObj : "";

            $scope.vendors = [{ id: 1, name: 'Automation Vendor 1' }, { id: 2, name: 'Automation Vendor 2' }, { id: 3, name: 'Automation Vendor 3' }];
            $scope.selectedVendor = [];

            $scope.editConfiguration = function (Id)
            {
                if (Id == 0)
                {
                    $scope.addnewconfigView = true;
                }
            }

            $scope.closeEditConfiguration = function () {
               // $scope.addnewconfigView = false;
                $scope.ProjectObject = {
                    COMP_ID: $scope.COMP_ID,
                    PROJECT_ID: '',
                    PROJECT_NAME: '',
                    COST_CENTER: '',
                    LOCATION: '',
                    COLLIEAR_PO_VALUE: '',
                    PO_NUMBER: '',
                    NET_REVENUE_MARGIN: '',
                    SHIP_TO_ADDRESS: '',
                    BILL_TO_ADDRESS: '',
                    TARGETED_REVENUE_BUDGET: '',
                    CREATED_BY: $scope.USER_ID

                };
                $state.go("projectDetails");

            };
            $scope.showPackage = function (Id) {
                $scope.expand = false;
                if (Id == 0) {
                    $scope.expand = true;
                } else if (Id == 1) {
                    $scope.expand = false;
                }
            }

            $scope.showPackage1 = function (Id) {
                $scope.expand1 = false;
                if (Id == 0) {
                    $scope.expand1 = true;
                } else if (Id == 1) {
                    $scope.expand1 = false;
                }
            }

            $scope.showPackage2 = function (Id) {
                $scope.expand2 = false;
                if (Id == 0) {
                    $scope.expand2 = true;
                } else if (Id == 1) {
                    $scope.expand2 = false;
                }
            }

            $scope.goToProjectEdit = function () {             
                    var url = $state.href("projectDetails");
                    window.open(url, '_blank');                
            };

            $scope.showallitems = false;
            $scope.showitem = false;

            $scope.checkAllitems = function (value) {
                if (value) {
                    $scope.showitem = true;
                } else {
                    $scope.showitem = false;
                }
          

            }
            $scope.showallitems1 = false;
            $scope.showitem1 = false;

            $scope.checkAllitems1 = function (value) {
                if (value) {
                    $scope.showitem1 = true;
                } else {
                    $scope.showitem1 = false;
                }


            }
            $scope.goToProjectEdit1 = function (Id) {
                if (Id == 0) {
                    $state.go("projectDetails", { "Id": Id });
                   
                }
            }

            $scope.saveProjectDetails = function () {
                var params = {                   
                    "projectdetails": $scope.ProjectObject,
                    "sessionid": userService.getUserToken()
                };
            };


            $scope.getStyles = function () {
                return 'width:325px;height: ' + angular.element('#productTable')[0].offsetHeight + 'px;max-height: 400px; overflow-y: auto;';
            };            
            $scope.rejectBudget = function () {
              // swal("Warning!", " Access Denined!", "warning");

            };
            $scope.routeToRfq = function () {
                var url = $state.href("save-requirementAdv");
                window.open(url, '_self');
            }


        }]);
