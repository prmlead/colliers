
prmApp.controller('packageApprovalListCtrl', ["$scope", "$stateParams", "$log", "$state", "$window", "userService", "auctionsService", "storeService", "growlService","PRMProjectServices", "$filter", "fileReader", "$rootScope",
    function ($scope, $stateParams, $log, $state, $window, userService, auctionsService, storeService, growlService, PRMProjectServices, $filter, fileReader, $rootScope) {
        $scope.USER_ID = userService.getUserId();
        $scope.COMP_ID = userService.getUserCompanyId();
        $scope.sessionID = userService.getUserToken();
        $scope.isSuperUser = userService.getUserObj().isSuperUser;
        $scope.BC_ID = $stateParams.Id;
        $scope.pdfTableShow = false;
        $scope.scStatus = 'ALL';

        /*PAGINATION CODE*/
        $scope.totalItems = 0;
        $scope.totalItems1 = 0;
        $scope.currentPage = 1;
        $scope.currentPage1 = 1;
        $scope.itemsPerPage = 10;
        $scope.itemsPerPage1 = 10;
        $scope.maxSize = 5;
        $scope.maxSize1 = 5;
        $scope.userObj = {};
        userService.getUserDataNoCache()
            .then(function (response) {
                $scope.userObj = response;
            });

        $scope.projectlist = [];
        $scope.projectlist1 = [];
        $scope.projectIdsArr = [];
        $scope.projectCostcentersArr = [];
        $scope.projectLocationsArr = [];
        $scope.showSetupBudgetBtn = false;


        $scope.filters =
        {
            project: {},
            costCentre: {},
            location: {}
        };

        $scope.filtersList = {
            project: [],
            costCentre: [],
            location: []
        };

        $scope.setPage = function (pageNo) {
            $scope.currentPage = pageNo;
            //$scope.GetProjectList(($scope.currentPage - 1), 10);
        };

        $scope.setFilters = function (currentPage) {

            if ($scope.loadServices) {
                $scope.GetPackageApprovalList(0, 10);
            }

        };

        $scope.initialProjectPageArray = [];

        $scope.GetProjectList = function (recordsFetchFrom, pageSize) {

            var projectIds, costCentres, location;

            if (_.isEmpty($scope.filters.project)) {
                projectIds = '';
            } else if ($scope.filters.project && $scope.filters.project.length > 0) {
                var projects = _($scope.filters.project)
                    .filter(item => item.id)
                    .map('id')
                    .value();
                projectIds = projects.join(',');
            }


            if (_.isEmpty($scope.filters.costCentre)) {
                costCentres = '';
            } else if ($scope.filters.costCentre && $scope.filters.costCentre.length > 0) {
                var ccs = _($scope.filters.costCentre)
                    .filter(item => item.id)
                    .map('id')
                    .value();
                costCentres = ccs.join(',');
            }

            if (_.isEmpty($scope.filters.location)) {
                location = '';
            } else if ($scope.filters.location && $scope.filters.location.length > 0) {
                var loc = _($scope.filters.location)
                    .filter(item => item.id)
                    .map('id')
                    .value();
                location = loc.join(',');
            }

            var params = {
                "COMP_ID": $scope.COMP_ID,
                "USER_ID": $scope.USER_ID,
                "pendingApproval": 0,
                "PROJECT_ID": projectIds,
                "COST_CENTER": costCentres,
                "LOCATION": location,
                "STATUS": '',
                "PageSize": recordsFetchFrom * pageSize,
                "NumberOfRecords": pageSize,
                "sessionid": userService.getUserToken()
            };
            $scope.pageSizeTemp = (params.PageSize + 1);
            $scope.NumberOfRecords = ((recordsFetchFrom + 1) * pageSize);

            PRMProjectServices.GetProjectList(params)
                .then(function (response) {
                    $scope.projectlist = [];
                    $scope.filteredprojectList = [];
                    response.forEach(function (item, index) {
                        $scope.projectlist.push(item);
                        if ($scope.initialProjectPageArray.length <= 9) { // Push Initial 10 Records When Page is Loaded because needed in SetFilters function it's getting called every time (need to modify directive code)  #Crap Code need to remove(should think of another solution)
                            $scope.initialProjectPageArray.push(item);
                        }
                    });

                    if ($scope.projectlist && $scope.projectlist.length > 0) {
                        $scope.totalItems = $scope.projectlist[0].TOTAL_ROWS;
                        $scope.filteredprojectList = $scope.projectlist;
                    }

                });
        };

        //$scope.GetProjectList(0, 10);

        $scope.searchById = '';
        $scope.searchByLocation = '';
        $scope.searchByCostcenter = '';

        $scope.filters = {
            project: {},
            costCentre: {},
            location: {}
        };

        $scope.filtersList = {
            project: [],
            costCentre: [],
            location: []
        };

        $scope.GetProjectListByFilter = function () {
            let projectsIDsTemp = [];
            let costCentresTemp = [];
            let locationsTemp = [];
            var params = {
                "COMP_ID": $scope.COMP_ID,
                "USER_ID": $scope.USER_ID,
                "TYPE": 'PACKAGE',
                "sessionid": userService.getUserToken()
            };
            $scope.loadServices = false;
            PRMProjectServices.GetProjectListByFilter(params)
                .then(function (response) {
                    $scope.projectlistFilteredTemp = response;

                    $scope.projectlistFilteredTemp.forEach(function (item, index) {
                        if (item.TYPE === 'PROJID') {
                            projectsIDsTemp.push({ name: item.VALUE, id: item.ID });
                        } else if (item.TYPE === 'COST_CENTER') {
                            costCentresTemp.push({ id: item.VALUE });
                        } else if (item.TYPE === 'LOCATION') {
                            locationsTemp.push({ id: item.VALUE });
                        }
                    });

                    $scope.filtersList.project = projectsIDsTemp;
                    $scope.filtersList.costCentre = costCentresTemp;
                    $scope.filtersList.location = locationsTemp;

                });
        };

        $scope.GetProjectListByFilter();


        $scope.goToPackageApproval = function (project) {
            var url = $state.href("packageApproval", {
                "projectId": +project.PROJECT_ID, "budgetId": project.BUDGET_ID, "qcsId": project.QCS_ID });
            window.open(url, '_blank');
        };

        $scope.packageApprovalsList = [];

        $scope.GetPackageApprovalList = function (recordsFetchFrom, pageSize)
        {
            var projectIds, costCentres, location;

            if (_.isEmpty($scope.filters.project)) {
                projectIds = '';
            } else if ($scope.filters.project && $scope.filters.project.length > 0) {
                var projects = _($scope.filters.project)
                    .filter(item => item.id)
                    .map('id')
                    .value();
                projectIds = projects.join(',');
            }


            if (_.isEmpty($scope.filters.costCentre)) {
                costCentres = '';
            } else if ($scope.filters.costCentre && $scope.filters.costCentre.length > 0) {
                var ccs = _($scope.filters.costCentre)
                    .filter(item => item.id)
                    .map('id')
                    .value();
                costCentres = ccs.join(',');
            }

            if (_.isEmpty($scope.filters.location)) {
                location = '';
            } else if ($scope.filters.location && $scope.filters.location.length > 0) {
                var loc = _($scope.filters.location)
                    .filter(item => item.id)
                    .map('id')
                    .value();
                location = loc.join(',');
            }


            $scope.packageApprovalsListFiltered = [];
            var params = {
                "COMP_ID": $scope.COMP_ID,
                "USER_ID": $scope.USER_ID,
                "PROJECT_ID": projectIds,
                "COST_CENTER": costCentres,
                "LOCATION": location,
                "SEARCH": $scope.filters.searchKeyword ? $scope.filters.searchKeyword : '',
                "sessionid": userService.getUserToken()
            };

            params.PageSize = pageSize * recordsFetchFrom;
            params.NumberOfRecords = pageSize;

            PRMProjectServices.GetPackageApprovalList(params)
                .then(function (response) {
                    $scope.loadServices = true;
                    if (response) {
                        $scope.packageApprovalsList = response;
                        $scope.packageApprovalsListTemp = angular.copy(response);

                        var filteredApprovalArr = _.filter($scope.packageApprovalsList, function (val) { return val.PACKAGE_APPROVAL_ID > 0; });

                        if (filteredApprovalArr && filteredApprovalArr.length > 0) {
                            filteredApprovalArr.forEach(function (item,index) {
                                var isFound = _.findIndex($scope.packageApprovalsListFiltered, function (list) { return list.PACKAGE_APPROVAL_ID === item.PACKAGE_APPROVAL_ID });
                                if (isFound <= -1) {
                                    $scope.packageApprovalsListFiltered.push(item);
                                }
                            });
                        }

                        if ($scope.packageApprovalsListFiltered && $scope.packageApprovalsListFiltered.length > 0) {
                            $scope.packageApprovalsListFiltered = _.orderBy($scope.packageApprovalsListFiltered, ['PACKAGE_APPROVAL_ID'], ['desc']);
                            $scope.packageApprovalsListFiltered.forEach(function (item, index) {
                                var filteredPackages = _.filter($scope.packageApprovalsList, function (val) { return val.PACKAGE_APPROVAL_ID === item.PACKAGE_APPROVAL_ID; });
                                var filteredPackagesBasedOnProjectAndBudget = _.filter($scope.packageApprovalsList, function (val) { return val.BUDGET_ID === item.BUDGET_ID && val.PROJECT_ID === item.PROJECT_ID; });
                                item.QCS_PO_VALUE = _.sumBy(filteredPackages, 'QCS_PO_VALUE');
                                item.EST_BUDGET_AMOUNT = _.sumBy(filteredPackagesBasedOnProjectAndBudget, 'EST_BUDGET_AMOUNT');
                                item.VARIANCE = (item.EST_BUDGET_AMOUNT - item.QCS_PO_VALUE);
                            });
                        }


                        $scope.totalItems = $scope.packageApprovalsListFiltered.length;

                    }

                });
        };


        $scope.GetPackageApprovalList(0,0);


        $scope.goToPackageApproval = function (projectID,budgetID,packageApprovalID,obj) {
            var url = $state.href("packageApproval", { "projectId": projectID, "budgetId": budgetID, "packageApprovalId": packageApprovalID, "qcsId": obj.QCS_ID });
            window.open(url, '_blank');
        };

        $scope.displayAnticipatedBudget = function (project)
        {
            var filteredList = _.filter($scope.packageApprovalsListTemp, function (allPackages)
            {
                return allPackages.PROJECT_ID === project.PROJECT_ID && allPackages.BUDGET_ID === project.BUDGET_ID 
            });

            if (filteredList && filteredList.length > 0) {
                filteredList.forEach(function (item, index) {
                    item.ANTICIPATED_PO_VALUE = 0;
                    if (item.QCS_PO_VALUE <= 0) {
                        item.ANTICIPATED_PO_VALUE = item.EST_BUDGET_AMOUNT;
                    } else if (item.QCS_PO_VALUE > 0) {
                        item.ANTICIPATED_PO_VALUE = item.QCS_PO_VALUE;
                    }
                });
            }

            return _.sumBy(filteredList, 'ANTICIPATED_PO_VALUE');

        };

        $scope.goToSaveImportQCS = function (reqID, qcsID) {
            var url = $state.href("import-qcs", { "reqID": reqID, "qcsID": qcsID });
            window.open(url, '_blank');
        };

    }]);