
prmApp.controller('projectAuditCtrl', ["$scope", "$stateParams", "$log", "$state", "$window", "userService", "auctionsService", "storeService", "growlService","PRMProjectServices", "$filter", "fileReader", "$rootScope",
    function ($scope, $stateParams, $log, $state, $window, userService, auctionsService, storeService, growlService, PRMProjectServices, $filter, fileReader, $rootScope) {
        $scope.USER_ID = userService.getUserId();
        $scope.COMP_ID = userService.getUserCompanyId();
        $scope.sessionID = userService.getUserToken();
        $scope.PROJECT_ID = $stateParams.PROJECT_ID;


        $scope.tableColumns = [];
        $scope.tableValues = [];


        $scope.getProjectAudit = function () {
            $scope.params =
            {
                "COMP_ID": $scope.COMP_ID,
                "PROJECT_ID": $scope.PROJECT_ID,
                "sessionid": userService.getUserToken()
            };

            PRMProjectServices.GetProjectAudit($scope.params)
                .then(function (response) {
                    if (response) {

                        var arr = JSON.parse(response).Table;                        
                        if (arr && arr.length > 0) {
                            $scope.tableColumnsTemp = angular.copy(_.keys(arr[0]));
                            $scope.tableColumnsTemp.forEach(function (item, index) {
                                item = item.replaceAll("_", " ");
                                $scope.tableColumns.push(item);
                            });
                            $scope.rows = arr;
                            arr.forEach(function (item, index) {
                                var obj = angular.copy(_.values(item));
                                if (obj) {
                                    item.tableValues = [];
                                    obj.forEach(function (value, valueIndex) {
                                        item.tableValues.push(value);
                                    });
                                }
                            });
                        }
                    }
                });
        }
        $scope.getProjectAudit($scope.PROJECT_ID);

    }]);