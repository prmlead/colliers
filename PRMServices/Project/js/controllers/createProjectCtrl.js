﻿prmApp
    .controller('createProjectCtrl', ["$scope", "$stateParams", "$log", "$state", "$window", "userService", "auctionsService",
        "storeService", "growlService", "poService", "$rootScope", "catalogService", "fileReader", "PRMProjectServices", "$filter",
        function ($scope, $stateParams, $log, $state, $window, userService, auctionsService,
            storeService, growlService, poService, $rootScope, catalogService,
            fileReader, PRMProjectServices, $filter) {

            $scope.userID = userService.getUserId();
            $scope.sessionID = userService.getUserToken();
            $scope.COMP_ID = userService.getUserCompanyId();
            $scope.isSuperUser = userService.getUserObj().isSuperUser;
            $scope.customerType = userService.getUserType();
            $scope.isCustomer = $scope.customerType === "CUSTOMER" ? true : false;
            $scope.PROJECT_ID = $stateParams.Id;
            $scope.companyDepartments = [];
            $scope.subUsers = [];
            $scope.filteredSubUsers = [];
            $scope.filteredBillDeptUsers = [];
            $scope.addnewconfigView = false;
            $scope.expand = false;
            $scope.expand1 = false;
            $scope.expand2 = false;
            $scope.absentArray = [];

            if (!$scope.isCustomer) {
                $state.go("home");
            }

            $scope.PROJECT_DET = {
                COMP_ID: $scope.COMP_ID,
                PROJ_ID: '',
                PROJECT_NAME: '',
                COST_CENTER: '',
                LOCATION: '',
                PO_VALUE: 0,
                PO_NUMBER: '',
                NET_REVENUE_MARGIN: 0,
                SHIP_TO_ADDRESS: '',
                BILL_TO_ADDRESS: '',
                TARGETED_REVENUE_BUDGET: 0,
                IS_VALID : 1,
                U_ID: $scope.userID,
                CLIENT_NAME : '',
                ADDRESS: '',
                SPOC_EMAIL: '',
                GST_NUMBER: '',
                PAN_NUMBER: ''               
            };

            $scope.PROJECT_BILL_DEPT = [
                {
                    DEPARTMENT: 'Project_Manager',
                    IS_DEPARTMENT_SELECTED: false
                },

                {
                    DEPARTMENT: 'Regional_Head',
                    IS_DEPARTMENT_SELECTED: false
                },

                {
                    DEPARTMENT: 'Project_Control',
                    IS_DEPARTMENT_SELECTED: false
                },

                {
                    DEPARTMENT: 'Procurement',
                    IS_DEPARTMENT_SELECTED: false
                }
            ],

            $scope.editConfiguration = function (Id) {
                if (Id == 0) {
                    $scope.addnewconfigView = true;
                }

            }

            $scope.GetCompanyDepartments = function () {
                auctionsService.GetCompanyDepartments($scope.userID, $scope.sessionID)
                    .then(function (response) {
                        $scope.companyDepartments = response;

                        if ($scope.PROJECT_DET.DEPT_IDS_STR && $scope.PROJECT_DET.DEPT_IDS.length <= 0) {
                            let tempDeptIds = $scope.PROJECT_DET.DEPT_IDS_STR.split(',');
                            tempDeptIds.forEach(function (deptId, index) {
                                $scope.PROJECT_DET.DEPT_IDS.push(+deptId);
                            });
                        }

                        $scope.companyDepartments.forEach(function (compDept, userIndex) {
                            if ($scope.PROJECT_DET.DEPT_IDS && $scope.PROJECT_DET.DEPT_IDS.length > 0 && $scope.PROJECT_DET.DEPT_IDS.includes(compDept.deptID)) {
                                $scope.PROJECT_DET.SELECTED_DEPTS.push(compDept);
                            }
                        });

                        $scope.getSubUserData();
                    });
            };

            $scope.closeEditConfiguration = function () {
                $scope.ProjectObject = {
                    COMP_ID: $scope.COMP_ID,
                    PROJECT_ID: '',
                    PROJECT_NAME: '',
                    COST_CENTER: '',
                    LOCATION: '',
                    COLLIEAR_PO_VALUE: '',
                    PO_NUMBER: '',
                    NET_REVENUE_MARGIN: '',
                    SHIP_TO_ADDRESS: '',
                    BILL_TO_ADDRESS: '',
                    TARGETED_REVENUE_BUDGET: '',
                    CREATED_BY: $scope.USER_ID,
                    CLIENT_NAME: '',
                    ADDRESS: '',
                    SPOC_EMAIL: '',
                    GST_NUMBER: '',
                    PAN_NUMBER: ''
                }
                $state.go("projectDetails");

            };

            $scope.filters = {
                project: {},
                costCentre: {},
                location: {}
            };

            $scope.filtersList = {
                location: []
            };

            $scope.projectlistTemp = [];
            $scope.projectlistFilteredTemp = [];
            $scope.GetProjectListByFilter = function () {
                let locationsTemp = [];
                var params = {
                    "COMP_ID": $scope.COMP_ID,
                    "USER_ID": $scope.userID,
                    "TYPE" :'PROJECT',
                    "sessionid": userService.getUserToken()
                };

                PRMProjectServices.GetProjectListByFilter(params)
                    .then(function (response) {
                        $scope.projectlistFilteredTemp = response;

                        $scope.projectlistFilteredTemp.forEach(function (item, index) {
                            if (item.TYPE === 'LOCATION') {
                                locationsTemp.push({ id: item.VALUE });
                            }
                        });
                        $scope.filtersList.location = locationsTemp;

                    });
            };
            $scope.GetProjectListByFilter();

            $scope.locationDisplay = [];
            $scope.locationSearch = function (value, type) {
                if (type == 'LOCATION' && value != undefined) {
                    if (value != '') {
                        $scope.showlocations = true;
                        $scope.locationDisplay = $scope.filtersList.location.filter(function (item) {
                            return (String(item.id.toLowerCase()).includes(value.toLowerCase()) == true);
                        })
                    } else {
                        $scope.showlocations = false;
                    }
                }
            }
            $scope.fillValueLocation = function (value, type) {
                if (type == 'LOCATION') {
                    $scope.showlocations = false;
                    $scope.PROJECT_DET.LOCATION = value;
                }
            }


            $scope.updateTargetRevBudg = function (value) {
                if ((parseFloat(value.NET_REVENUE_MARGIN) > 100) || (parseFloat(value.NET_REVENUE_MARGIN) < 0)) {
                    swal("Error!", 'Please Enter Valid Net Revenue Margin %');
                    value.NET_REVENUE_MARGIN = 0;
                };
                if (!value.PO_VALUE || !value.NET_REVENUE_MARGIN) {
                    value.TARGETED_REVENUE_BUDGET = 0;
                } else {
                    value.TARGETED_REVENUE_BUDGET = (value.PO_VALUE * (1 - (value.NET_REVENUE_MARGIN / 100))).toFixed(2);
                }
            };


            $scope.validateAssignedUserDept = function () {
                $scope.depts = [];
                $scope.users = [];
                $scope.PROJECT_DET.SELECTED_DEPTS.forEach(function (item) {
                    $scope.depts.push(item.deptCode);
                })
                //$scope.subUsers.forEach(function (subuser) {
                //    $scope.PROJECT_BILL_DEPT.forEach(function (user) {
                //        if (user.DEPARTEMENT == item1.DEPARTMENT) {
                //            $scope.users.push(subuser.userID);
                //        }
                //    })                    
                //})

                if ($scope.PROJECT_DET.SELECTED_DEPTS) {
                    $scope.PROJECT_BILL_DEPT.forEach(function (item1) {
                        if (item1) {

                            if ($scope.tableValidation) {
                                return false;
                            }

                            if ($scope.depts.indexOf(item1.DEPARTMENT) != -1) {
                                item1.IS_DEPARTMENT_SELECTED = true;
                            } else {
                                $scope.tableValidation = true;
                                growlService.growl("Please Select" + ' ' + item1.DEPARTMENT + ' ' + "Department", 'inverse');
                                return
                            }


                            //if ($scope.users.indexOf(item1.DEPARTMENT) != -1) {
                            //    assignedUser.IS_SELECTED = true
                            //} else {
                            //    $scope.tableValidation = true;
                            //    growlService.growl("Please Select" + ' ' + assignedUser.fullName + ' ' + "User", 'inverse');
                            //    return
                            //}
                          
                        }
                    });
                }

                //if ($scope.PROJECT_DET.ASSIGNED_USERS) {
                //    $scope.PROJECT_DET.ASSIGNED_USERS.forEach(function (assignedUser) {
                //        if (assignedUser) {
                //            if ($scope.tableValidation) {
                //                return false;
                //            }
                //            if ($scope.users.indexOf(assignedUser.DEPARTMENT) != -1) {
                //                assignedUser.IS_SELECTED = true
                //            } else {
                //                $scope.tableValidation = true;
                //                growlService.growl("Please Select" + ' ' + assignedUser.fullName + ' ' + "User", 'inverse');
                //                return
                //            }
                //        }

                //    })
                //} 

            }


            $scope.saveProjectDetails = function () {
                if ((+$scope.PROJECT_DET.PO_VALUE) && (+$scope.PROJECT_DET.NET_REVENUE_MARGIN) && $scope.PROJECT_DET.ASSIGNED_USERS
                    && $scope.PROJECT_DET.ASSIGNED_USERS.length > 0) {
                    $scope.tableValidation = false;
                    $scope.absentArray = [];
                    var params = {

                       "projectdetails": $scope.PROJECT_DET,
                        "sessionid": userService.getUserToken()
                    };

                    if ($scope.PROJECT_DET.PO_VALUE <= 0) {
                        growlService.growl("Please Enter PO Value.", 'inverse');
                        return;
                    }


                    $scope.validateAssignedUserDept();
                    if (!$scope.tableValidation)
                    {
                        $scope.validateAssignedAllUserDept();
                    }

                    if ($scope.tableValidation) {
                        return;
                    }



                    PRMProjectServices.SaveProjectDetails(params)
                        .then(function (response) {
                            if (response.errorMessage) {
                                if (response.objectID === -2) {
                                    $scope.PROJECT_DET.PO_VALUE = $scope.PROJECT_DET.PO_VALUE_TEMP;
                                }
                                swal("Error!", response.errorMessage, "error");
                            } else {
                                growlService.growl('Project Created Successfully.', "success");
                                $state.go("projectDetails");
                            };
                        });
                } else {
                    swal("Warning!", 'Please fill all the mandatory fields to save the project.', "warning");
                }
            };


            function executeSaveProjectDetails(params,closePopup) {
                PRMProjectServices.SaveProjectDetails(params)
                    .then(function (response) {
                        if (response.errorMessage) {
                            if (response.objectID === -2) {
                                $scope.PROJECT_DET.PO_VALUE = $scope.PROJECT_DET.PO_VALUE_TEMP;
                                $scope.PROJECT_DET.TARGETED_REVENUE_BUDGET = $scope.PROJECT_DET.TARGETED_REVENUE_BUDGET_TEMP;
                            }
                            swal("Error!", response.errorMessage, "error");
                        } else {
                            growlService.growl('Project Created Successfully.', "success");
                            $state.go("projectDetails");
                        };
                    });
            }

            $scope.getStyles = function () {
                return 'width:325px;height: ' + angular.element('#productTable')[0].offsetHeight + 'px;max-height: 400px; overflow-y: auto;';
            };

            $scope.GetProjectDetails = function (id) {
                var params =
                {
                    "PROJECT_ID": id,
                    "sessionid": userService.getUserToken()
                };

                PRMProjectServices.GetProjectDetails(params)
                    .then(function (response) {
                        $scope.PROJECT_DET = response;
                        $scope.PROJECT_DET.SELECTED_DEPTS = [];
                        $scope.PROJECT_DET.ASSIGNED_USERS = [];

                        //$scope.getSubUserData();
                        $scope.GetCompanyDepartments();
                        if ($scope.PROJECT_DET.BILL_TO_ADDRESS) {
                            $scope.PROJECT_DET.BILL_TO_ADDRESS_JSON = $scope.PROJECT_DET.BILL_TO_ADDRESS;
                            $scope.PROJECT_DET.BILL_TO_ADDRESS = JSON.parse($scope.PROJECT_DET.BILL_TO_ADDRESS).ADDRESS;
                        }
                        
                        $scope.PROJECT_DET.TARGETED_REVENUE_BUDGET_TEMP = $scope.PROJECT_DET.TARGETED_REVENUE_BUDGET;
                    });

            };

            $scope.getSubUserData = function () {
                userService.getSubUsersData({ "userid": userService.getUserId(), "sessionid": userService.getUserToken() })
                    .then(function (response) {
                        if ($scope.PROJECT_DET.ASSIGNED_U_IDS_STR && $scope.PROJECT_DET.ASSIGNED_U_IDS.length <= 0) {
                            let tempUids = $scope.PROJECT_DET.ASSIGNED_U_IDS_STR.split(',');
                            tempUids.forEach(function (userId, index) {
                                $scope.PROJECT_DET.ASSIGNED_U_IDS.push(+userId);
                            });
                        }

                        $scope.subUsers = $filter('filter')(response, { isValid: true });

                        $scope.filteredSubUsers = [];
                        $scope.subUsers.forEach(function (user, index) {
                            user.userID = +user.userID;
                            user.dateFrom = userService.toLocalDate(user.dateFrom);
                            user.dateTo = userService.toLocalDate(user.dateTo);
                            user.IS_SELECTED = false;
                            if ($scope.PROJECT_DET.ASSIGNED_U_IDS && $scope.PROJECT_DET.ASSIGNED_U_IDS.length > 0 && $scope.PROJECT_DET.ASSIGNED_U_IDS.includes(user.userID)) {
                                $scope.PROJECT_DET.ASSIGNED_USERS.push(user);
                            }
                        });

                        console.log($scope.PROJECT_DET.ASSIGNED_USERS);

                        $scope.deptChange();
                    });
            };


            if (+$scope.PROJECT_ID > 0) {
                $scope.GetProjectDetails(+$scope.PROJECT_ID);
            } else {
                //$scope.getSubUserData();
                $scope.GetCompanyDepartments();
            }

            $scope.GetProjectBillToDetails = function (searchString) {
                $scope.ProjectBillToDetailsList = [];
                var params = {
                    "U_ID": $scope.userID,
                    "COMP_ID": $scope.COMP_ID,
                    "searchString": searchString,
                    "sessionID": $scope.sessionID
                };
                PRMProjectServices.GetProjectBillToDetails(params)
                    .then(function (response) {
                        if (response) {
                            $scope.ProjectBillToDetailsList = JSON.parse(response).Table;
                        }
                    });
            };

            $scope.fillValue = function (billToDetail)
            {
                if (billToDetail) {
                    $scope.PROJECT_DET.BILL_TO_ADDRESS = billToDetail.ADDRESS;
                    $scope.PROJECT_DET.BILL_TO_ADDRESS_JSON = JSON.stringify(billToDetail);
                    $scope.ProjectBillToDetailsList = [];
                }
            };


            $scope.showEmailValid = false;
            $scope.showEmailValidMessage = '';
            $scope.EmailValidate1 = function () {
                var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                var result = re.test($scope.PROJECT_DET.SPOC_EMAIL);
                if (!result) {
                    $scope.showEmailValid = true;
                    $scope.showEmailValidMessage = "Please enter a valid Email Address";
                    return false;
                } else {
                    $scope.showEmailValid = false;
                    $scope.showEmailValidMessage = '';
                }
            };

            $scope.deptChange = function () {
                $scope.PROJECT_DET.DEPT_IDS = [];
                if ($scope.PROJECT_DET.SELECTED_DEPTS && $scope.PROJECT_DET.SELECTED_DEPTS.length > 0) {
                    $scope.PROJECT_DET.SELECTED_DEPTS.forEach(function (selectedDept, index) {
                        if (!$scope.PROJECT_DET.DEPT_IDS.includes(selectedDept.deptID)) {
                            $scope.PROJECT_DET.DEPT_IDS.push(selectedDept.deptID)
                        }
                    });
                }

                //$scope.filteredSubUsers1 = [];
                $scope.filteredSubUsers = [];
                $scope.filteredBillDeptUsers = [];
                if ($scope.subUsers && $scope.subUsers.length > 0) {

                    
                  
                        $scope.filteredSubUsers = $scope.subUsers.filter(function (subUser) {
                            //if (subUser.IS_SELECTED == false) {
                                return subUser.DEPARTMENT_ID_LIST && $scope.PROJECT_DET.DEPT_IDS.some(r => subUser.DEPARTMENT_ID_LIST.includes(r));
                        //    }
                        });
                        //$scope.filteredSubUsers = angular.copy($scope.filteredSubUsers1);
                    }

                             
                $scope.filteredSubUsers.forEach(function (billDeptUser) {
                    if (billDeptUser.DEPARTMENT == 'Project_Manager' || billDeptUser.DEPARTMENT == 'Regional_Head' ||
                        billDeptUser.DEPARTMENT == 'Project_Control' || billDeptUser.DEPARTMENT == 'Procurement') {
                        $scope.filteredBillDeptUsers.push(billDeptUser.userID)
                        //$scope.PROJECT_DET.BILL_U_IDS_TEMP = $scope.filteredBillDeptUsers.join();
                    }
                })

                // $scope.filteredSubUsers.forEach(function (user) {
                //    if (user.IS_SELECTED == true) {
                //        $scope.PROJECT_DET.ASSIGNED_U_IDS.push(user.userID);
                //        $scope.handleAssignedTo();
                //    }
                //})

                console.log($scope.filteredSubUsers);
            };

            if (window.location.href.toLowerCase().contains("web"))
            {
                if (+$scope.COMP_ID === 1) {
                    $scope.mandatoryDeptIds = ['1', '3', '6', '7'];
                    $scope.mandatoryDeptIds2 = [1, 3, 6, 7];
                } else {
                    $scope.mandatoryDeptIds = ['13', '18', '19', '20'];
                    $scope.mandatoryDeptIds2 = [13, 18, 19, 20];
                }
            } else
            {
                $scope.mandatoryDeptIds = ['25', '31', '32', '33'];
                $scope.mandatoryDeptIds2 = [25, 31, 32, 33];
            }
            $scope.handleAssignedTo = function () {
                $scope.PROJECT_DET.ASSIGNED_U_IDS = [];
                $scope.PROJECT_DET.PROCUREMENT_U_IDS = [];
                $scope.PROJECT_DET.BILL_U_IDS = [];
                $scope.tempUserDeptArray = [];
                if ($scope.PROJECT_DET.ASSIGNED_USERS && $scope.PROJECT_DET.ASSIGNED_USERS.length > 0) {
                    $scope.PROJECT_DET.ASSIGNED_USERS.forEach(function (selectedUser, index) {
                        if (!$scope.PROJECT_DET.ASSIGNED_U_IDS.includes(selectedUser.userID)) {
                            //selectedUser.IS_SELECTED = true;
                            $scope.PROJECT_DET.ASSIGNED_U_IDS.push(selectedUser.userID);
                        }
                        //else {
                        //    selectedUser.IS_SELECTED = false;

                        //}
                        //$scope.subUsers.forEach(function (item) {
                        //    $scope.PROJECT_DET.ASSIGNED_USERS.forEach(function (subitem) {
                        //        if (subitem.IS_SELECTED == true && subitem.userID == item.userID) {
                        //            item.IS_SELECTED = true;
                        //        }
                        //    })
                        //})
                        
                        if ($scope.filteredBillDeptUsers.length > 0) {
                            if ($scope.filteredBillDeptUsers.indexOf(selectedUser.userID) != -1)
                                {
                                    $scope.PROJECT_DET.BILL_U_IDS.push(selectedUser.userID)
                            }

                        }
                        if (selectedUser.DEPARTMENT_CODES_LIST && selectedUser.DEPARTMENT_CODES_LIST.length > 0)
                        {
                            if (selectedUser.DEPARTMENT_CODES_LIST.some(x => x.toLowerCase() == 'procurement')) {
                                if (!$scope.PROJECT_DET.PROCUREMENT_U_IDS.includes(selectedUser.userID)) {
                                    $scope.PROJECT_DET.PROCUREMENT_U_IDS.push(selectedUser.userID);
                                }
                            }
                        }
                    });
                }
            };


            $scope.validateAssignedAllUserDept = function () {
                if ($scope.PROJECT_DET.ASSIGNED_USERS.length > 0) {
                    $scope.PROJECT_DET.ASSIGNED_USERS.forEach(function (selectedUser) {
                        if (selectedUser.DEPARTMENT_IDS)
                        {
                            var array = selectedUser.DEPARTMENT_IDS.split(',');

                            array.forEach(function (a) {
                                var ifExists = _.findIndex($scope.tempUserDeptArray, function (b) { return a === b });
                                if (ifExists < 0)
                                {
                                    $scope.tempUserDeptArray.push(a);
                                }
                            });
                        }
                    });


                    $scope.mandatoryDeptIds.forEach(function (id) {
                        var ifExists = _.findIndex($scope.tempUserDeptArray, function (e) { return e === id });
                        if (ifExists < 0) {
                            var ifExists1 = _.findIndex($scope.absentArray, function (a) { return a === id });
                            if (ifExists1 < 0) {
                                $scope.absentArray.push(id);
                            }
                        }
                    });


                    if ($scope.absentArray.length == 0) {
                        $scope.tableValidation = false;
                    }
                    else {
                        console.log($scope.absentArray);
                        var companyDeptTemp = $scope.companyDepartments.filter(e => $scope.mandatoryDeptIds2.includes(e.deptID));
                        companyDeptTemp.forEach(function (cItem) {
                            $scope.absentArray.forEach(function (aItem) {
                                if (cItem.deptID == aItem) {
                                    $scope.tableValidation = true;
                                    growlService.growl("Please Select" + ' ' + cItem.deptCode + ' ' + "Department Users", 'inverse');
                                    return;
                                }
                            })
                        })
                    };
                }
            }

        }]);
