prmApp
    .controller('setupBudgetCtrl', ["$scope", "$stateParams", "$log", "$state", "$window", "userService", "auctionsService",
        "storeService", "growlService", "poService", "$rootScope", "catalogService", "fileReader", "PRMProjectServices","workflowService",
        function ($scope, $stateParams, $log, $state, $window, userService, auctionsService,
            storeService, growlService,  poService, $rootScope, catalogService,
            fileReader, PRMProjectServices, workflowService) {

            $scope.userID = userService.getUserId();
            $scope.sessionID = userService.getUserToken();
            $scope.CompId = userService.getUserCompanyId();
            $scope.catalogCompId = userService.getUserCatalogCompanyId();
            $scope.isSuperUser = userService.getUserObj().isSuperUser;
            $scope.PROJECT_ID = $stateParams.projectId;
            $scope.BUDGET_ID = $stateParams.budgetId;
            $scope.isAllWidgetsCollapsed = true;
            $scope.ProjectObject = $stateParams.detailsObj ? $stateParams.detailsObj : "";
            $scope.BudgetList = [];
            $scope.selectedSubPackagesForReq = [];
            $scope.PushDeletedPackages = [];
            // Pagination //
            $scope.loaderMore = false;
            $scope.scrollended = false;
            $scope.page = 0;
            var page = 0;
            $scope.PageSize = 200;
            var totalData = 0;
            $scope.fetchRecordsFrom = $scope.page * $scope.PageSize;
            $scope.totalCount = 0;
            // Pagination //

            $scope.addnewconfigView = false;
            $scope.expand = false;
            $scope.expand1 = false;
            $scope.expand2 = false;
            $scope.deptIDs = [];
            $scope.desigIDs = [];

            /*region start WORKFLOW*/
            $scope.workflowList = [];
            $scope.itemWorkflow = [];
            $scope.workflowObj = {};
            $scope.workflowObj.workflowID = 0;
            $scope.currentStep = 0;
            $scope.orderInfo = 0;
            $scope.assignToShow = '';
            $scope.isWorkflowCompleted = false;
            $scope.WorkflowModule = 'BUDGET';
            $scope.disableWFSelection = false;
            /*region end WORKFLOW*/
            $scope.ListUserDepartmentDesignations = userService.getListUserDepartmentDesignations();
            if ($scope.ListUserDepartmentDesignations && $scope.ListUserDepartmentDesignations.length > 0) {
                $scope.ListUserDepartmentDesignations.forEach(function (item, index) {
                    $scope.deptIDs.push(item.deptID);
                    item.listDesignation.forEach(function (item1, index1) {
                        if (item1.isAssignedToUser && item1.isValid) {
                            $scope.desigIDs.push(item1.desigID);
                        }
                    });
                });
            }
            $scope.showDisclaimer = false;         
            $scope.GetProjectDetails = function () {
                var params =
                {
                    "PROJECT_ID": $scope.PROJECT_ID,
                    "sessionid": userService.getUserToken()
                };

                PRMProjectServices.GetProjectDetails(params)
                    .then(function (response) {
                        $scope.PROJECT_DET = response;

                        $scope.showDisclaimer = validateDisclaimers();

                    });

            };


            if (+$scope.PROJECT_ID > 0) {
                $scope.GetProjectDetails();
            }


            function validateDisclaimers()
            {
                var isValid = false;

                if ($scope.PROJECT_DET.COST_CENTER === 'PMC_North' || $scope.PROJECT_DET.COST_CENTER === 'PMC_Mid' || $scope.PROJECT_DET.COST_CENTER === 'PMC_South') {
                    isValid = true;
                }

                return isValid;
            }

            $scope.ProjectObject = $stateParams.detailsObj ? $stateParams.detailsObj : '';

            $scope.editConfiguration = function (Id)
            {
                if (Id === 0)
                {
                    $scope.addnewconfigView = true;
                }
            }

            $scope.closeEditConfiguration = function () {
               // $scope.addnewconfigView = false;
                $scope.ProjectObject = {
                    COMP_ID: $scope.COMP_ID,
                    PROJECT_ID: '',
                    PROJECT_NAME: '',
                    COST_CENTER: '',
                    LOCATION: '',
                    COLLIEAR_PO_VALUE: '',
                    PO_NUMBER: '',
                    NET_REVENUE_MARGIN: '',
                    SHIP_TO_ADDRESS: '',
                    BILL_TO_ADDRESS: '',
                    TARGETED_REVENUE_BUDGET: '',
                    CREATED_BY: $scope.USER_ID

                }
                $state.go("projectDetails");

            };
            $scope.showPackage = function (Id) {
                $scope.expand = false;
                if (Id == 0) {
                    $scope.expand = true;
                } else if (Id == 1) {
                    $scope.expand = false;
                }
            }

            $scope.showPackage1 = function (Id) {
                $scope.expand1 = false;
                if (Id == 0) {
                    $scope.expand1 = true;
                } else if (Id == 1) {
                    $scope.expand1 = false;
                }
            }

            $scope.showPackage2 = function (Id) {
                $scope.expand2 = false;
                if (Id == 0) {
                    $scope.expand2 = true;
                } else if (Id == 1) {
                    $scope.expand2 = false;
                }
            }

            $scope.showallitems = false;
            $scope.showitem = false;

            $scope.checkAllitems = function (value) {
                if (value) {
                    $scope.showitem = true;
                } else {
                    $scope.showitem = false;
                }
          

            }
            $scope.showallitems1 = false;
            $scope.showitem1 = false;

            $scope.checkAllitems1 = function (value) {
                if (value) {
                    $scope.showitem1 = true;
                } else {
                    $scope.showitem1 = false;
                }


            }

            $scope.SaveBudgetDetails = function () {
                $scope.BudgetList = [];
                $scope.BudgetArrValidations = [];
                var keepGoing = false;
                $scope.BudgetArr.forEach(function (budgetItem, budgetItemIndex) {
                    if (budgetItem && budgetItem.PACKAGE_ID && !keepGoing && budgetItem.IS_VALID) {
                        if (!budgetItem.PO_VALUE || +budgetItem.PO_VALUE <= 0)
                        {
                            growlService.growl("Please enter Package Collier PO Value greater than zero for package s.no " + (budgetItem.S_NO) + " with package name " + budgetItem.PACKAGE_NAME, "inverse");
                            keepGoing = true;
                            return false;
                        }

                        if (!budgetItem.NET_REVENUE_MARGIN || +budgetItem.NET_REVENUE_MARGIN <= 0)//
                        {
                            growlService.growl("Please enter Package Net Revenue Margin(%) greater than zero for package s.no " + (budgetItem.S_NO) + " with package name " + budgetItem.PACKAGE_NAME, "inverse");
                            keepGoing = true;
                            return false;
                        }

                        if (!budgetItem.EST_BUDGET_AMOUNT || +budgetItem.EST_BUDGET_AMOUNT <= 0)
                        {
                            growlService.growl("Please enter Package Budget Amount greater than zero for package s.no " + (budgetItem.S_NO) + " with package name " + budgetItem.PACKAGE_NAME, "inverse");
                            keepGoing = true;
                            return false;
                        }
                        budgetItem.U_ID = +$scope.userID;
                        $scope.BudgetList.push(budgetItem);
                        if (budgetItem && budgetItem.NODES && budgetItem.NODES.length > 0) {
                            $scope.validateSubPackages(budgetItem);
                            budgetItem.NODES.forEach(function (subBudgetItem, subPackageItemIndex) {
                                subBudgetItem.COMP_ID = budgetItem.COMP_ID;
                                subBudgetItem.U_ID = (budgetItem.U_ID ? budgetItem.U_ID : +$scope.userID);
                                if (subBudgetItem.SUB_PACKAGE_ID && !keepGoing && subBudgetItem.IS_VALID && subBudgetItem.PACKAGE_STATUS === 'ACTIVE')
                                {
                                    if (!subBudgetItem.PO_VALUE || subBudgetItem.PO_VALUE <= 0) {
                                        growlService.growl("Please enter Sub Package PO Value greater than zero for Sub Package s.no " + (subPackageItemIndex + 1) + " with package name " + budgetItem.PACKAGE_NAME, "inverse");
                                        keepGoing = true;
                                        return false;
                                    }

                                    if (!subBudgetItem.EST_BUDGET_AMOUNT || subBudgetItem.EST_BUDGET_AMOUNT <= 0) {
                                        growlService.growl("Please enter Sub Package Est Budget Amount greater than zero for Sub Package s.no  " + (subPackageItemIndex + 1) + " with package name " + budgetItem.PACKAGE_NAME, "inverse");
                                        keepGoing = true;
                                        return false;
                                    }
                                    $scope.BudgetList.push(subBudgetItem);
                                }

                            });
                        }
                    }
                    if (!budgetItem.MULTIPLE_ATTACHMENTS && validateDisclaimers())
                    {
                        growlService.growl("Please attach the client approval.", "inverse");
                        keepGoing = true;
                        return false;
                    }

                });

                if (keepGoing) {
                    return false;
                }

                if ($scope.PushDeletedPackages && $scope.PushDeletedPackages.length > 0) {
                    $scope.PushDeletedPackages.forEach(function (item, index) {
                        $scope.BudgetList.push(item);
                    });
                }


                var params =
                {
                    "budgetDetailsList": $scope.BudgetList,
                    "projectId": $scope.PROJECT_ID,
                    "budgetId": $scope.BUDGET_ID,
                    "sessionid": userService.getUserToken()
                };



                PRMProjectServices.SaveBudgetDetails(params)
                    .then(function (response) {
                        if (response.objectID) {
                            growlService.growl('Saved Successfully.', "success");
                            $state.go('.', { budgetId: response.objectID }, { notify: false });
                            $scope.BUDGET_ID = response.objectID;
                            $scope.GetBudgetDetailsbyProject();
                            $scope.PushDeletedPackages = [];
                        } else {
                            growlService.growl(response.errorMessage, "inverse");
                        };
                    });
            };

            $scope.BudgetArr = [];
            $scope.BudgetObj = 
                {
                    PROJ_ID: 0,
                    COMP_ID: +$scope.CompId,
                    U_ID: +$scope.userID,
                    PACKAGE_ID: 0,
                    PO_VALUE: 0,
                    NET_REVENUE_MARGIN: 0,
                    TARGETED_REVENUE_BUDGET: 0,
                    SUB_PACKAGE_ID: 0,
                    SUB_PACKAGE_PO_VALUE: 0,
                    ALLOCATION_PERCENTAGE: 0,
                    EST_BUDGET_AMOUNT: 0,
                    PACKAGE_STATUS: '',
                    BUDGET_WF_ID: 0,
                    PACKAGE_NAME : '',
                    BUDGET_AMOUNT :0
                };

            $scope.getcategories = function (IsPaging) {
                var catNodes = [];
                    catalogService.getcategories($scope.catalogCompId, $scope.fetchRecordsFrom, $scope.PageSize)
                        .then(function (response) {
                            response.forEach(function (catItem, catIndex) {
                                catItem.catNameTemp = catItem.catName.toLowerCase();
                            });
                            response = response.filter(function (item, index) {
                                return item.IS_CORE === 1 && item.nodes.length > 0;
                            });
                            $scope.companyCatalogTemp = [];
                            $scope.companyCatalog = response;
                            $scope.companyCatalogTemp = response;
                            $scope.companyCatalog = _.orderBy($scope.companyCatalog, ['isDefault'], ['desc']);

                            $scope.companyCatalog.forEach(function (item, index) {
                                let BudgetObj =
                                {
                                    PROJ_ID: 0,
                                    COMP_ID: +$scope.CompId,
                                    U_ID: +$scope.userID,
                                    PACKAGE_ID: item.isDefault ? item.catId : 0,
                                    PO_VALUE: 0,
                                    NET_REVENUE_MARGIN: 0,
                                    TARGETED_REVENUE_BUDGET: 0,
                                    SUB_PACKAGE_ID: 0,
                                    SUB_PACKAGE_PO_VALUE: 0,
                                    ALLOCATION_PERCENTAGE: 0,
                                    EST_BUDGET_AMOUNT: 0,
                                    PACKAGE_STATUS: '',
                                    BUDGET_WF_ID: 0,
                                    PACKAGE_NAME: item.catCode,
                                    BUDGET_AMOUNT: 0,
                                    IS_VALID: 1
                                };
                                if (item.isDefault)
                                {
                                    $scope.BudgetArr.push(BudgetObj);
                                    $scope.showSubPackages($scope.BudgetArr[index]);
                                }
                            });

                            $scope.BudgetArr.forEach(function (item, index1) {
                                item.S_NO = index1 + 1;
                                item.expanded = false;
                            });

                            if ($scope.companyCatalog && $scope.companyCatalog.length > 0) {
                                $scope.totalCount = $scope.companyCatalog[0].totalCategories;
                            }

                            $scope.data = [{
                                'compId': 0,
                                'catId': 0,
                                'catName': 'Package Index',
                                'catDesc': '',
                                'nodes': $scope.companyCatalog,
                                'catParentId': 0
                            }];

                            if (+$scope.BUDGET_ID > 0 && +$scope.PROJECT_ID > 0) {
                                $scope.GetCompanyVendors();
                                //$scope.GetBudgetDetailsbyProject();
                            }
                        });
            };

            $scope.getcategories();

            $scope.GetBudgetDetailsbyProject = function (isExpanded) {
                var params =
                {
                    "projectId": $scope.PROJECT_ID,
                    "budgetId": $scope.BUDGET_ID,
                    "onlyApproved": false,
                    "sessionId": $scope.sessionID
                };

                PRMProjectServices.GetBudgetDetailsbyProject(params)
                    .then(function (response) {
                        $scope.BudgetList = response;
                        if ($scope.BudgetList && $scope.BudgetList.length > 0)
                        {
                            var validList = $scope.BudgetList.filter(function (item) {
                                if (item.ATTACHMENTS)
                                {
                                    item.MULTIPLE_ATTACHMENTS = angular.fromJson(item.ATTACHMENTS);
                                }
                                if (item.BUDGET_WF_ID > 0) {
                                    item.IS_ACK = true;
                                }
                                item.EST_BUDGET_AMOUNT_TEMP = item.EST_BUDGET_AMOUNT;
                                item.NET_REVENUE_MARGIN_TEMP = item.NET_REVENUE_MARGIN;
                                item.PO_VALUE_TEMP = item.PO_VALUE;
                                if (item.IS_VALID === 1 && item.PACKAGE_STATUS != 'NA') {
                                    return item;
                                }
                            })
                            $scope.BudgetList = validList;//_.filter($scope.BudgetList, { IS_VALID: 1 , PACKAGE_STATUS : !'NA'});
                            if ($scope.BudgetList[0].BUDGET_WF_ID > 0) {
                                $scope.getItemWorkflow($scope.BUDGET_ID, $scope.WorkflowModule, isExpanded);
                            } else {
                                fillBudgets();
                            }
                        }
                    });
            };



            $scope.getStyles = function () {
                return 'width:325px;height: ' + angular.element('#productTable')[0].offsetHeight + 'px;max-height: 400px; overflow-y: auto;';
            };


            function fillBudgets(isExpanded,validateWorkflowDisability)
            {
                $scope.BudgetArr = [];
                $scope.vendorWorkflowArr = [];
                let counter = 0;
                $scope.BudgetList.forEach(function (budget, index) {

                    if (validateWorkflowDisability && $scope.isFormdisabled)
                    {
                        budget.IS_BUDGET_APPROVED = $scope.isFormdisabled;
                    }

                    if (budget.VENDOR_IDS)
                    {
                        $scope.vendorWorkflowArr.push(budget.ROW_ID);
                    }

                    if (budget.PACKAGE_ID && !budget.SUB_PACKAGE_ID) {
                        $scope.BudgetArr[counter] = budget;
                        $scope.showSubPackages($scope.BudgetArr[counter]);
                        counter++
                    }

                    if (budget.PACKAGE_ID && budget.SUB_PACKAGE_ID) {
                        let currentPackage = $scope.BudgetArr.filter(function (temp) {
                            return temp.PACKAGE_ID === budget.PACKAGE_ID;
                        });
                        if (currentPackage && currentPackage.length > 0) {
                            let currentSubPackage = currentPackage[0].NODES.filter(function (temp1) {
                                return temp1.PACKAGE_ID === budget.PACKAGE_ID && temp1.catId === budget.SUB_PACKAGE_ID;
                            });

                            if (currentSubPackage && currentSubPackage.length > 0) {
                                budget.catCode = currentSubPackage[0].catCode;
                                budget.catId = budget.SUB_PACKAGE_ID;
                                //budget.selectedVendors = JSON.parse(budget.Vendors);
                                var vendorsList = JSON.parse(budget.Vendors);
                                if (vendorsList !== null) {
                                    budget.selectedVendors = $scope.vendors.filter(function (obj) {
                                        return vendorsList.some(function (obj2) {
                                            return obj2.U_ID == obj.id;
                                        });
                                    });
                                    // when no vendors are selected. List of vendors are shown from companyvendors
                                    if (budget.selectedVendors.length === 0) {
                                        budget.selectedVendors = $scope.vendors;
                                    }
                                }

                                if (budget.selectedVendors && budget.selectedVendors.length > 0) {
                                    $scope.selectVendors(budget);
                                }
                                currentPackage[0].NODES = currentPackage[0].NODES.filter(function (temp1) {
                                    return temp1.catId !== budget.SUB_PACKAGE_ID;
                                });

                                currentPackage[0].NODES.unshift(budget);
                            }
                        }
                    }
                });

                $scope.BudgetArr.forEach(function (budgItem, budgIndex) {
                    budgItem.S_NO = budgIndex + 1;
                    if (isExpanded)
                    {
                        $scope.validateSubPackages(budgItem);
                    }

                    budgItem.ENABLE_DELETE_FOR_PACKAGE = false;

                    if (budgItem.PACKAGE_ID > 0 && budgItem.BUDGET_WF_ID <= 0) {
                        budgItem.ENABLE_DELETE_FOR_PACKAGE = true;
                    }

                    budgItem.NODES = _.orderBy(budgItem.NODES, ['ROW_ID']);

                });

                $scope.BudgetArr = _.orderBy($scope.BudgetArr, ['ROW_ID']);

                if ($scope.vendorWorkflowArr && $scope.vendorWorkflowArr.length > 0)
                {
                    $scope.getItemWorkflowsMultiple();
                }

                return;
            }

            $scope.getItemWorkflowsMultiple = function () {
                workflowService.getItemWorkflow(0, $scope.vendorWorkflowArr.join(','), 'VENDOR_SELECTION')
                    .then(function (response) {
                        $scope.itemWorkflow1 = response;
                        $scope.checkIsFormDisable();
                        if ($scope.itemWorkflow1 && $scope.itemWorkflow1.length > 0 && $scope.itemWorkflow1[0].WorkflowTracks.length > 0) {
                            $scope.currentStep = 0;
                            var count = 0;
                            $scope.itemWorkflow1[0].WorkflowTracks.forEach(function (track) {
                                if (!track.multipleAttachments) {
                                    track.multipleAttachments = [];
                                }

                                if (track.status === 'APPROVED') { track.statusNew = 'Approved'; }
                                if (track.status === 'HOLD') { track.statusNew = 'Hold'; }
                                if (track.status === 'PENDING') { track.statusNew = 'Pending'; }
                                if (track.status === 'REJECTED') { track.statusNew = 'Rejected'; }

                                if (track.status === 'APPROVED' || track.status === 'HOLD') {
                                    $scope.isFormdisabled = true;
                                }

                                if (track.status === 'APPROVED') {
                                    $scope.isWorkflowCompleted = true;
                                    $scope.orderInfo = track.order;
                                    $scope.assignToShow = track.status;

                                }
                                else {
                                    $scope.isWorkflowCompleted = false;
                                }

                                if (track.status === 'REJECTED' && count == 0) {
                                    count = count + 1;
                                }

                                if ((track.status === 'PENDING' || track.status === 'HOLD') && count == 0) {
                                    count = count + 1;
                                    //$scope.IsUserApproverForStage(track.approverID);
                                    $scope.currentAccess = track.order;
                                }

                                if ((track.status === 'PENDING' || track.status === 'HOLD' || track.status === 'REJECTED') && $scope.currentStep === 0) {
                                    $scope.currentStep = track.order;
                                    return false;
                                }
                            });
                        }
                    });
            };

            /*region start WORKFLOW*/

            $scope.getWorkflows = function () {
                workflowService.getWorkflowList()
                    .then(function (response) {
                        $scope.workflowList = [];
                        $scope.workflowListDeptWise = [];
                        $scope.workflowListSubPackage = [];
                        $scope.workflowListTemp = response;

                        $scope.workflowListTemp.forEach(function (item, index) {
                            if (item.WorkflowModule === $scope.WorkflowModule) {
                                $scope.workflowList.push(item);
                                $scope.workflowListDeptWise.push(item);
                            }
                        });

                        if ($scope.isSuperUser) {
                            $scope.workflowList = $scope.workflowList;
                        }
                        else {
                            $scope.workflowList = [];
                            $scope.workflowListDeptWise.forEach(function (wf, idx) {
                                $scope.deptIDs.forEach(function (dep) {
                                    if (dep == wf.deptID) {
                                        $scope.workflowList.push(wf);
                                    }
                                });
                            });

                        }
                    });
            };

            $scope.getWorkflows();

            

            $scope.getItemWorkflow = function (budgetID, workflowModule,isExpanded) {
                workflowService.getItemWorkflow(0, budgetID, workflowModule)
                    .then(function (response) {
                        $scope.itemWorkflow = response;
                        $scope.checkIsFormDisable();
                        if ($scope.itemWorkflow && $scope.itemWorkflow.length > 0 && $scope.itemWorkflow[0].WorkflowTracks.length > 0) {
                            $scope.currentStep = 0;
                            var count = 0;
                            $scope.itemWorkflow[0].WorkflowTracks.forEach(function (track) {
                                if (!track.multipleAttachments) {
                                    track.multipleAttachments = [];
                                }

                                if (track.status === 'APPROVED') { track.statusNew = 'Approved'; }
                                if (track.status === 'HOLD') { track.statusNew = 'Hold'; }
                                if (track.status === 'PENDING') { track.statusNew = 'Pending'; }
                                if (track.status === 'REJECTED') { track.statusNew = 'Rejected'; }

                                if (track.status === 'APPROVED' || track.status === 'HOLD') {
                                    $scope.isFormdisabled = true;
                                }

                                if (track.status === 'APPROVED') {
                                    $scope.isWorkflowCompleted = true;
                                    $scope.orderInfo = track.order;
                                    $scope.assignToShow = track.status;

                                }
                                else {
                                    $scope.isWorkflowCompleted = false;
                                }

                                if (track.status === 'REJECTED' && count == 0) {
                                    count = count + 1;
                                }

                                if ((track.status === 'PENDING' || track.status === 'HOLD') && count == 0) {
                                    count = count + 1;
                                    //$scope.IsUserApproverForStage(track.approverID);
                                    $scope.currentAccess = track.order;
                                }

                                if ((track.status === 'PENDING' || track.status === 'HOLD' || track.status === 'REJECTED') && $scope.currentStep === 0) {
                                    $scope.currentStep = track.order;
                                    return false;
                                }
                            });

                            fillBudgets(isExpanded,true);

                        }
                    });
            };


            $scope.isApproverDisable1 = function (index,moduleId) {

                var disable = true;

                var previousStep = {};

                $scope.itemWorkflow1[0].WorkflowTracks.forEach(function (step, stepIndex) {

                    if (moduleId === step.moduleID)
                    {
                        if (index == stepIndex) {
                            if (stepIndex == 0) {
                                if ($scope.isUserBelongsToDeptandDesig(step.department.deptID, step.approver.desigID) &&
                                    (step.status === 'PENDING' || step.status === 'HOLD')) {
                                    disable = false;
                                }
                                else {
                                    disable = true;
                                }
                            }
                            else if (stepIndex > 0) {
                                if (previousStep.status === 'PENDING' || previousStep.status === 'HOLD' || previousStep.status === 'REJECTED') {
                                    disable = true;
                                }
                                else if ($scope.isUserBelongsToDeptandDesig(step.department.deptID, step.approver.desigID) &&
                                    (step.status === 'PENDING' || step.status === 'HOLD')) {
                                    disable = false;
                                }
                                else {
                                    disable = true;
                                }
                            }
                        }
                        previousStep = step;
                    }
                });

                return disable;
            };
           
            $scope.isApproverDisable = function (index) {

                var disable = true;

                var previousStep = {};

                $scope.itemWorkflow[0].WorkflowTracks.forEach(function (step, stepIndex) {

                    if (index == stepIndex) {
                        if (stepIndex == 0) {
                            if ($scope.isUserBelongsToDeptandDesig(step.department.deptID, step.approver.desigID) &&
                                (step.status === 'PENDING' || step.status === 'HOLD')) {
                                disable = false;
                            }
                            else {
                                disable = true;
                            }
                        }
                        else if (stepIndex > 0) {
                            if (previousStep.status === 'PENDING' || previousStep.status === 'HOLD' || previousStep.status === 'REJECTED') {
                                disable = true;
                            }
                            else if ($scope.isUserBelongsToDeptandDesig(step.department.deptID, step.approver.desigID) &&
                                (step.status === 'PENDING' || step.status === 'HOLD')) {
                                disable = false;
                            }
                            else {
                                disable = true;
                            }
                        }
                    }
                    previousStep = step;
                });

                return disable;
            };

            $scope.isUserBelongsToDeptandDesig = function (deptID, desigID) {
                var isEligible = true;

                if ($scope.deptIDs.indexOf(deptID) != -1 && $scope.desigIDs.indexOf(desigID) != -1) {
                    isEligible = true;
                } else {
                    isEligible = false;
                }

                return isEligible;
            };

            $scope.updateTrack = function (step, status, type, isExpanded) {
                $scope.disableAssignPR = true;
                $scope.commentsError = '';
                step.isLastApprover = false;

                if (step.comments != null || step.comments != "" || step.comments != undefined) {
                    step.comments = validateStringWithoutSpecialCharacters(step.comments);
                }
                var tempArray = $scope.itemWorkflow[0].WorkflowTracks[$scope.itemWorkflow[0].WorkflowTracks.length - 1];
                if (step.order == tempArray.order && status === 'APPROVED') {
                    $scope.disableAssignPR = false;
                    step.isLastApprover = true;
                } else {
                    $scope.disableAssignPR = true;
                }

                if ($scope.isReject) {
                    $scope.commentsError = 'Please Save Rejected Items/Qty';
                    return false;
                }

                if (status === 'REJECTED' && (step.comments == null || step.comments == "")) {
                    $scope.commentsError = 'Please enter comments';
                    return false;
                }

                step.status = status;
                step.sessionID = $scope.sessionID;
                step.modifiedBy = userService.getUserId();

                step.moduleName = type ? type : $scope.WorkflowModule;

                step.subModuleName = '';
                step.subModuleID = 0;

                workflowService.SaveWorkflowTrack(step)
                    .then(function (response) {
                        if (response.errorMessage) {
                            growlService.growl(response.errorMessage, "inverse");
                        }
                        else {
                            $scope.GetBudgetDetailsbyProject(true);
                        }
                    });
            };

            $scope.showSubPackages = function (budgetObj)
            {
                let subPackages = _.find($scope.companyCatalog, { catId: budgetObj.PACKAGE_ID }).nodes;
                let subPackagesTemp = angular.copy(_.find($scope.companyCatalog, { catId: budgetObj.PACKAGE_ID }).nodes);
                if (subPackages && subPackages.length > 0)
                {
                    budgetObj.subPackageErrorMessage = '';
                    if ($scope.BUDGET_ID > 0 && $scope.BudgetList)
                    {
                        if ($scope.BudgetList.length > 0)
                        {
                            var validSubPackages = _($scope.BudgetList)
                                .filter(item => item.SUB_PACKAGE_ID > 0 && item.PACKAGE_ID === budgetObj.PACKAGE_ID)
                                .map('SUB_PACKAGE_ID')
                                .value();
            
                            if (validSubPackages && validSubPackages.length > 0)
                            {
                                subPackages = subPackages.filter(function (catItem, catIndex) {
                                    var itemFound = _.findIndex(validSubPackages, function (id) { return id === catItem.catId  });
                                    if (itemFound >= 0) {
                                        return catItem;
                                    }
                                });
                            }
                        }
                    }
                }
                budgetObj.NODES = subPackages;

                if (budgetObj.NODES && budgetObj.NODES.length > 0) {
                    budgetObj.NODES.forEach(function (node, index1) {
                        node.PACKAGE_ID = budgetObj.PACKAGE_ID;
                        node.PACKAGE_STATUS = 'ACTIVE';
                        node.SUB_PACKAGE_ID = node.catId;
                        node.SUB_PACKAGE_NAME = node.catCode;
                        node.IS_VALID = 1;
                        node.BUDGET_WF_ID = budgetObj.BUDGET_WF_ID;
                        node.IS_BUDGET_APPROVED = budgetObj.IS_BUDGET_APPROVED;
                        node.SUB_PACKAGE_ARR = [];

                        subPackagesTemp.forEach(function (catItem, catIndex) {
                            let catObj =
                            {
                                "catId": catItem.catId,
                                "catCode": catItem.catCode
                            };
                            node.SUB_PACKAGE_ARR.push(catObj);
                        });
                    });
                }

                //if (budgetObj.NODES && budgetObj.NODES.length > 0) {
                //    //$scope.allSubPackages = subPackagesTemp.map(function (value) { return value });
                //    budgetObj.NODES.forEach(function (nodeItems, nodeItemsIndex) {
                        
                //    });
                //}
            };

            $scope.validateSubPackages = function (budgetObj) {
                budgetObj.expanded = true;
                budgetObj.subPackageErrorMessage = '';

                if (!budgetObj.PACKAGE_ID) {
                    //budgetObj.subPackageErrorMessage = 'Please select a package to display sub packages.';
                    budgetObj.expanded = false;
                    alert('Please select a package to display sub packages.');
                }

                if (budgetObj.PACKAGE_ID) {
                    let subPackages = _.find($scope.companyCatalog, { catId: budgetObj.PACKAGE_ID }).nodes;
                    let subPackagesTemp = angular.copy(_.find($scope.companyCatalog, { catId: budgetObj.PACKAGE_ID }).nodes);
                    if (subPackages.length <= 0) {
                        //budgetObj.subPackageErrorMessage = 'Sub packages are not available for the selected package.';
                        budgetObj.expanded = false;
                        alert('Sub packages are not available for the selected package.');
                    }

                    if (subPackages.length > 0)
                    {
                        subPackages = _.filter(subPackages, function (item) {
                            return item.IS_VALID > 0;
                        });

                        budgetObj.NODES = $scope.BUDGET_ID > 0 ? budgetObj.NODES : budgetObj.NODES;
                        if (budgetObj.NODES && budgetObj.NODES.length > 0) {
                            budgetObj.NODES.forEach(function (node, index1) {
                                node.SUB_PACKAGE_ID = node.EDITED_SUB_PACKAGE_ID  ? node.EDITED_SUB_PACKAGE_ID : node.catId;
                                node.SUB_PACKAGE_ARR = [];
                                //node.PO_VALUE = subPackages.PO_VALUE;
                                //node.EST_BUDGET_AMOUNT = subPackages.EST_BUDGET_AMOUNT;

                                subPackagesTemp.forEach(function (catItem, catIndex) {
                                    let catObj =
                                    {
                                        "catId": catItem.catId,
                                        "catCode": catItem.catCode
                                    };
                                    node.SUB_PACKAGE_ARR.push(catObj);
                                });

                            });
                        }

                        //if (budgetObj.NODES && budgetObj.NODES.length > 0) {
                        //    //$scope.allSubPackages = subPackagesTemp.map(function (value) { return value });
                        //    budgetObj.NODES.forEach(function (nodeItems, nodeItemsIndex) {
                                
                        //    });
                        //}
                    }
                }
            };

            $scope.AssignWorkflowToBudget = function () {

                var acknowledgementNotDone = !$scope.BudgetArr[0].IS_ACK && validateDisclaimers();

                if (acknowledgementNotDone)
                {
                    swal("Error!", "Please Acknowledge as client approval is secured for this project and attached.", "error");
                    return;
                }

                var workflowID = (_.find($scope.workflowListTemp, { location: $scope.PROJECT_DET.COST_CENTER, WorkflowModule: 'BUDGET' }) ? _.find($scope.workflowListTemp, { location: $scope.PROJECT_DET.COST_CENTER, WorkflowModule: 'BUDGET' }).workflowID : 0);

                if (workflowID <= 0)
                {
                    swal("Error!", "Please create a workflow with " + $scope.PROJECT_DET.COST_CENTER + " cost centre for the budget module.", "error");
                    return;
                }


                var params =
                {
                    "wID": workflowID,
                    "moduleID": +$scope.BUDGET_ID,
                    "projectID": +$scope.PROJECT_ID,
                    "user": +$scope.userID,
                    "sessionid": $scope.sessionID
                };

                PRMProjectServices.UpdateBudgetWorkflow(params)
                    .then(function (response) {
                        if (response.errorMessage) {
                            growlService.growl(response.errorMessage, "inverse");
                        } else {
                            $state.go("projectDetails");
                            growlService.growl('Budget Worfklow Assigned Successfully.', "success");
                        };
                    });
            };

            $scope.isFormdisabled = false;
            $scope.isBudgetApproved = false;

            $scope.checkIsFormDisable = function () {
                $scope.isFormdisabled = true;
                $scope.isBudgetApproved = true;
                //validateBudgetApproved(true);
                if ($scope.itemWorkflow.length == 0) {
                    $scope.isFormdisabled = true;
                } else {
                    if ($scope.BudgetList && $scope.BudgetList.length > 0) {
                        if (($scope.BudgetList[0].CREATED_BY == +userService.getUserId() || $scope.BudgetList[0].MODIFIED_BY == +userService.getUserId() || $scope.isSuperUser) && $scope.itemWorkflow[0].WorkflowTracks.length > 0 &&
                            $scope.itemWorkflow[0].WorkflowTracks[0].status !== "APPROVED" && $scope.itemWorkflow[0].WorkflowTracks[0].order == 1 && $scope.itemWorkflow[0].workflowID > 0) {
                            $scope.isFormdisabled = false;
                            $scope.isBudgetApproved = false;
                            //validateBudgetApproved(false);
                        }
                    }
                }

            };

            function validateBudgetApproved(isApproved)
            {
                if ($scope.BUDGET_ID > 0) {
                    $scope.BudgetArr.forEach(function (packageItem, packageItemIndex) {
                        packageItem.IS_BUDGET_APPROVED = isApproved;
                        if (packageItem.NODES && packageItem.NODES.length > 0)
                        {
                            packageItem.NODES.forEach(function (subPackageItem, subPackageItemIndex) {
                                subPackageItem.IS_BUDGET_APPROVED = isApproved;
                            });
                        }
                    })
                }
            }

            function validateStringWithoutSpecialCharacters(string) {
                if (string) {
                    string = string.replace(/\'/gi, "");
                    string = string.replace(/\"/gi, "");
                    string = string.replace(/[`^_|\?;:'",<>\{\}\[\]\\\/]/gi, "");
                    string = string.replace(/(\r\n|\n|\r)/gm, "");
                    string = string.replace(/\t/g, '');
                    return string;
                }
            }

            $scope.showApprovedDate = function (date) {
                return userService.toLocalDate(date);
            };

            $scope.GetCompanyVendors = function () {
                $scope.vendors = [];
                let vendors = [];

                $scope.params = { "userID": +$scope.userID, "sessionID": userService.getUserToken(), "PageSize": 0, "NumberOfRecords": 10000, "searchString": '' };

                userService.GetCompanyVendors($scope.params)
                    .then(function (response) {
                        response = _.filter(response, function (item) {
                            return item.regStatus == 1;
                        });

                        response.forEach(function (item) {
                            item.companyVendorCodes = _.uniq(item.companyVendorCodes.split(',').map(function (item) { return item.trim() })).join(',');
                        })
                        $scope.vendorsList = response;
                        $scope.vendorsList.forEach(function (item,index) {
                            vendors.push({ id: item.userID, name: item.companyName });
                        });

                        $scope.vendors = vendors;

                        $scope.GetBudgetDetailsbyProject();
                    });
            };


            //$scope.GetCompanyVendors();


            $scope.selectVendors = function (subpackage) {
                if (subpackage && subpackage.selectedVendors) {
                    if (!_.isEmpty(subpackage.selectedVendors)) {
                        let vendorIds = _(subpackage.selectedVendors)
                            .filter(item => item.id)
                            .map('id')
                            .value();
                        subpackage.VENDOR_IDS = vendorIds.join(',');
                    }
                    else {
                        subpackage.VENDOR_IDS = '';
                    }
                }
            };

            $scope.showBudgetApprovalPopUp = true;

            $scope.showBudgetApproval = function ()
            {
                $scope.showBudgetApprovalPopUp = false;
            };

            $scope.validateNetRevenueMargin = function (package) {
                let isValidValue = true;
                let totalBudgetValue = 0;
                if (package && package.NET_REVENUE_MARGIN && +package.NET_REVENUE_MARGIN > 100) {
                    package.NET_REVENUE_MARGIN = package.NET_REVENUE_MARGIN_TEMP;      
                }
                if (package && package.PO_VALUE == 0) {
                    package.NET_REVENUE_MARGIN = package.NET_REVENUE_MARGIN_TEMP;
                    growlService.growl("Please Enter Collier PO Value", "inverse");
                }


                if (isValidValue) {
                    if (package.NET_REVENUE_MARGIN > 0 && package.PO_VALUE > 0) {
                        package.EST_BUDGET_AMOUNT = (package.PO_VALUE - (package.PO_VALUE * (package.NET_REVENUE_MARGIN / 100)));
                        let isAmountValid = false;
                        isAmountValid = validateSubPackageAmounts(package, 'EST_BUDGET');
                        if (isAmountValid) {
                            isValidValue = false;
                            growlService.growl("Sum Of Sub Package Est Budget Amount is less than Package Budget Amount - " + package.EST_BUDGET_AMOUNT_TEMP, "inverse");
                            package.EST_BUDGET_AMOUNT = package.EST_BUDGET_AMOUNT_TEMP;
                            package.NET_REVENUE_MARGIN = package.NET_REVENUE_MARGIN_TEMP;
                        }
                    }
                    else
                    {
                        if (+package.NET_REVENUE_MARGIN <= 0) {
                            package.EST_BUDGET_AMOUNT = 0;
                            //package.EST_BUDGET_AMOUNT_TEMP = 0;
                        } else
                        {
                            package.EST_BUDGET_AMOUNT = package.EST_BUDGET_AMOUNT_TEMP;
                        }
                    }
                }
                $scope.BudgetArr.forEach(function (item, index1) {
                    totalBudgetValue += (+item.EST_BUDGET_AMOUNT);
                });
                if (totalBudgetValue > $scope.PROJECT_DET.TARGETED_REVENUE_BUDGET) {
                    isValidValue = false;                    
                    package.EST_BUDGET_AMOUNT = package.EST_BUDGET_AMOUNT_TEMP;
                    package.NET_REVENUE_MARGIN = package.NET_REVENUE_MARGIN_TEMP;
                    growlService.growl("Package Budget Amount is greater than Project target revenue budget - " + $scope.PROJECT_DET.TARGETED_REVENUE_BUDGET, "inverse");
                }
            };

            $scope.validatePackageValue = function (package, field) {
                
                if (field === 'PO_VALUE') {
                    let isValidValue = true;
                    let totalPOVal = 0;
                    $scope.BudgetArr.forEach(function (item, index1) {
                            totalPOVal += (+item.PO_VALUE); 
                    });

                    if (package.NODES && package.NODES.length > 0)
                    {
                        let isAmountValid = false;
                        isAmountValid = validateSubPackageAmounts(package, field);
                        if (isAmountValid)
                        {
                            isValidValue = false;
                            growlService.growl("Sum Of Sub Package PO Values is less than Package PO Value - " + package.PO_VALUE_TEMP, "inverse");
                            package.PO_VALUE = package.PO_VALUE_TEMP;
                        }
                    }

                    if (totalPOVal > +$scope.PROJECT_DET.PO_VALUE) {
                        isValidValue = false;
                        package.PO_VALUE = package.PO_VALUE_TEMP;
                        growlService.growl("Package PO values is greater than Project PO value - " + $scope.PROJECT_DET.PO_VALUE, "inverse");
                    }

                    if (isValidValue) {
                        if (+package.PO_VALUE > 0 && +package.EST_BUDGET_AMOUNT > 0) {//&& +package.PO_VALUE > +package.EST_BUDGET_AMOUNT
                            package.NET_REVENUE_MARGIN = ((package.PO_VALUE - package.EST_BUDGET_AMOUNT) / package.PO_VALUE) * 100;
                        } else {
                            package.NET_REVENUE_MARGIN = 0;
                            package.EST_BUDGET_AMOUNT = 0;
                        }
                    }
                }

                if (field === 'EST_BUDGET') {

                    let isValidValue = true;
                    //if (package.EST_BUDGET_AMOUNT && (+package.EST_BUDGET_AMOUNT) > (+package.PO_VALUE)) {
                    //    isValidValue = false;
                    //    package.EST_BUDGET_AMOUNT = package.EST_BUDGET_AMOUNT_TEMP;                     
                    //    growlService.growl("Package Budget Amount cannot be more than Package PO value - " + package.PO_VALUE, "inverse");
                    //}
                    //else {
                        
                    //}

                    let totalEstBudgetVal = 0;
                    $scope.BudgetArr.forEach(function (item, index1) {
                        totalEstBudgetVal += (+item.EST_BUDGET_AMOUNT);
                    });

                    if (totalEstBudgetVal > +$scope.PROJECT_DET.TARGETED_REVENUE_BUDGET) {
                        isValidValue = false;
                        package.EST_BUDGET_AMOUNT = package.EST_BUDGET_AMOUNT_TEMP;
                        growlService.growl("Package Budget Amount is greater than Project target revenue budget - " + $scope.PROJECT_DET.TARGETED_REVENUE_BUDGET, "inverse");
                    }


                    if (package.NODES && package.NODES.length > 0)
                    {
                        let isAmountValid = false;
                        isAmountValid = validateSubPackageAmounts(package, field);
                        if (isAmountValid) {
                            isValidValue = false;
                            growlService.growl("Sum Of Sub Package Est Budget Amount is less than Package Budget Amount - " + package.EST_BUDGET_AMOUNT_TEMP, "inverse");
                            package.EST_BUDGET_AMOUNT = package.EST_BUDGET_AMOUNT_TEMP;
                        }
                    }

                    if (isValidValue) {
                        if (+package.PO_VALUE > 0 ) {
                            package.NET_REVENUE_MARGIN = ((package.PO_VALUE - package.EST_BUDGET_AMOUNT) / package.PO_VALUE) * 100;
                        }
                        else {
                            package.NET_REVENUE_MARGIN = package.NET_REVENUE_MARGIN_TEMP;
                        }
                    }
                }
            };


            function validateSubPackageAmounts(package,field) {
                let isValid = false;
                var newSubPackArr = angular.copy(package.NODES);
                if (newSubPackArr && newSubPackArr.length > 0)
                {
                    newSubPackArr.forEach(function (packItem, packIndex) {
                        if (field === 'PO_VALUE') {
                            packItem.PO_VALUE = +packItem.PO_VALUE;
                        }

                        if (field === 'EST_BUDGET') {
                            packItem.EST_BUDGET_AMOUNT = +packItem.EST_BUDGET_AMOUNT;
                        }
                    });
                }
                if (field === 'PO_VALUE')
                {
                    var subPackagePOValue = _.sumBy(newSubPackArr, 'PO_VALUE');
                    if (subPackagePOValue > +package.PO_VALUE) {
                        isValid = true;
                    }
                }

                if (field === 'EST_BUDGET')
                {
                    var subPackageValue = _.sumBy(newSubPackArr, 'EST_BUDGET_AMOUNT');
                    if (subPackageValue > +package.EST_BUDGET_AMOUNT) {
                        isValid = true;
                    }
                }

                return isValid;
            }

            $scope.validateSubPackageValue = function (package, subPackage, field) {
                if (field === 'PO_VALUE') {
                    let totalPOVal = 0;
                    package.NODES.forEach(function (item, index1) {
                        if (item.PO_VALUE > 0 && item.IS_VALID) {
                            totalPOVal += (+item.PO_VALUE);
                        }
                    });

                    if (totalPOVal > +package.PO_VALUE) {
                        subPackage.PO_VALUE = subPackage.PO_VALUE_TEMP;
                        growlService.growl("Sub-Package PO value is greater than Package PO value : " + package.PO_VALUE, "inverse");
                    }
                }

                if (field === 'EST_BUDGET') {
                    let totalBudgetVal = 0;
                    package.NODES.forEach(function (item, index1) {
                        if (item.EST_BUDGET_AMOUNT > 0 && item.IS_VALID) {
                            totalBudgetVal += (+item.EST_BUDGET_AMOUNT);
                        }
                        //if (item.EST_BUDGET_AMOUNT > +  item.PO_VALUE) {
                        //    item.EST_BUDGET_AMOUNT = 0;
                        //    growlService.growl("Sub-Package Est Budget Amount cannot exceed Sub-Package PO value : " + item.PO_VALUE, "inverse");
                        //}
                    });

                    if (totalBudgetVal > +package.EST_BUDGET_AMOUNT) {
                        subPackage.EST_BUDGET_AMOUNT = subPackage.EST_BUDGET_AMOUNT_TEMP;
                        subPackage.ALLOCATION_PERCENTAGE = 0;
                        growlService.growl("Sub-Packages Budget Amount cannot exceed Package Budget Amount : " + package.EST_BUDGET_AMOUNT, "inverse");
                    }
                    else {
                        subPackage.ALLOCATION_PERCENTAGE = $scope.precisionRound(((subPackage.EST_BUDGET_AMOUNT / package.EST_BUDGET_AMOUNT) * 100), $rootScope.companyRoundingDecimalSetting);
                    }
                }
            };

            $scope.validatePOValue = function (obj,type,fieldName) {
                $scope.poValueErrorMessage = '';

                if (type === 'SUB_PACKAGE') {
                    if (obj.SUB_PACKAGE_ID)
                    {
                        if (fieldName === 'PO_VALUE') {
                            let value = 0;
                            value = _.find($scope.BudgetArr, { PACKAGE_ID: obj.PACKAGE_ID }).PO_VALUE;
                            
                            var subPackages = [];
                            $scope.BudgetArr.forEach(function (item, index) {
                                if (item.NODES && item.NODES.length > 0) {
                                    item.NODES.forEach(function (item, index) {
                                        if (item.PO_VALUE && +item.PO_VALUE > 0) {
                                            subPackages.push(+item.PO_VALUE);
                                        }
                                    });
                                }
                            });
                            var totalSubPackages = 0;
                            totalSubPackages = _.sumBy(subPackages);
                            if (totalSubPackages > 0) {
                                if (totalSubPackages > $scope.PROJECT_DET.PO_VALUE) {
                                    $scope.poValueErrorMessage = 'Given Value is Exceeding Project Collier PO Value.'
                                    obj.PO_VALUE = obj.PO_VALUE_TEMP;
                                }
                                if (totalSubPackages > value) {
                                    $scope.poValueErrorMessage = 'Given Value is Exceeding Overall Package Collier PO Value.'
                                    obj.PO_VALUE = obj.PO_VALUE_TEMP;
                                    return;
                                }
                            }
                        } else if (fieldName === 'EST_BUDGET') {
                            let value = 0;
                            value = _.find($scope.BudgetArr, { PACKAGE_ID: obj.PACKAGE_ID }).EST_BUDGET_AMOUNT;
                            
                            var subPackages = [];
                            $scope.BudgetArr.forEach(function (item, index) {
                                if (item.NODES && item.NODES.length > 0) {
                                    item.NODES.forEach(function (item, index) {
                                        if (item.EST_BUDGET_AMOUNT && +item.EST_BUDGET_AMOUNT > 0) {
                                            subPackages.push(+item.EST_BUDGET_AMOUNT);
                                        }
                                    });
                                }
                            });
                            var totalSubPackages = 0;
                            totalSubPackages = _.sumBy(subPackages);
                            if (totalSubPackages > 0) {
                                if (totalSubPackages > $scope.PROJECT_DET.TARGETED_REVENUE_BUDGET) {
                                    $scope.poValueErrorMessage = 'Given Value is Exceeding Targeted Revenue Budget.'
                                    obj.EST_BUDGET_AMOUNT = obj.EST_BUDGET_AMOUNT_TEMP;
                                }

                                if (totalSubPackages > value) {
                                    $scope.poValueErrorMessage = 'Given Value is Exceeding Package Level Budget Amount Value.'
                                    obj.EST_BUDGET_AMOUNT = obj.EST_BUDGET_AMOUNT_TEMP;
                                    return;
                                }

                            }

                        }
                    }
                } else {
                    if (obj.PACKAGE_ID)
                    {
                        let selectedPackage = _.find($scope.BudgetArr, { PACKAGE_ID: obj.PACKAGE_ID }).NODES;
                        if (selectedPackage && selectedPackage.length > 0) {
                            selectedPackage.forEach(function (item, index) {
                                item.PO_VALUE = 0;
                            });
                        }

                        let allPackages = _.sumBy(_.map($scope.BudgetArr, 'PO_VALUE'));
                        if (+allPackages > +$scope.PROJECT_DET.PO_VALUE) {
                            $scope.poValueErrorMessage = 'Given Value is Exceeding Overall Package Collier PO Value.'
                            obj.PO_VALUE = obj.PO_VALUE_TEMP;
                            return;
                        }

                    }
                }
            };

            $scope.calculatePackageValues = function (obj, type)
            {
                $scope.calculationErrorMessage = '';
                if (obj.PACKAGE_ID) {
                    let packagePOValue = _.find($scope.BudgetArr, { PACKAGE_ID: obj.PACKAGE_ID }).PO_VALUE;
                    let packageBudgetValue = _.find($scope.BudgetArr, { PACKAGE_ID: obj.PACKAGE_ID }).EST_BUDGET_AMOUNT;
                    if (type === 'NET_MARGIN' || type === 'PO_VALUE') {
                        if (packagePOValue <= 0) {
                            obj.NET_REVENUE_MARGIN = 0;
                        } else {
                            obj.NET_REVENUE_MARGIN = ((packagePOValue - packageBudgetValue) / packagePOValue) * 100;
                        }

                        if (obj.NET_REVENUE_MARGIN < 0 || obj.NET_REVENUE_MARGIN > 100) {
                            obj.NET_REVENUE_MARGIN = obj.NET_REVENUE_MARGIN_TEMP;
                            $scope.calculationErrorMessage = 'Net Revenue Margin Cannot be more than 100';
                        }
                        obj.EST_BUDGET_AMOUNT = packagePOValue * (100 - obj.NET_REVENUE_MARGIN) / 100;

                        let allBudgets = _.sumBy(_.map($scope.BudgetArr, 'EST_BUDGET_AMOUNT'));

                        if (allBudgets > $scope.PROJECT_DET.TARGETED_REVENUE_BUDGET) {
                            obj.EST_BUDGET_AMOUNT = obj.EST_BUDGET_AMOUNT_TEMP;
                        }
                    }
                    if (type === 'EST_BUDGET') {
                        if (packagePOValue <= 0) {
                            obj.NET_REVENUE_MARGIN = 0;
                        } else {
                            obj.NET_REVENUE_MARGIN = ((packagePOValue - packageBudgetValue) / packagePOValue) * 100;
                        }
                        let allBudgets = _.sumBy(_.map($scope.BudgetArr, 'EST_BUDGET_AMOUNT').map(i => +i));

                        if (allBudgets > $scope.PROJECT_DET.TARGETED_REVENUE_BUDGET) {
                            obj.EST_BUDGET_AMOUNT = obj.EST_BUDGET_AMOUNT_TEMP;
                            $scope.calculationErrorMessage = 'Budget Amount Cannot be more than Targeted Revenue Budget.';
                        }
                    }
                }
            };


            $scope.showRequirementButton = function () {
                let showButton = false;
                let mainArray = [];
                if ($scope.BudgetList && $scope.BudgetList.length > 0) {
                    $scope.BudgetList.forEach(function (item, index) {
                        if (item.NODES && item.NODES.length > 0) {
                            item.NODES.forEach(function (nodeItem, nodeItemIndex) {
                                var obj = {
                                    IS_ITEM_APPROVED: false
                                };
                                if (nodeItem.SUB_PACKAGE_WF_APPROVAL_STATUS === 'APPROVED' && !nodeItem.SUB_PACKAGE_WF_PENDING_APPROVERS) {
                                    obj = { IS_ITEM_APPROVED: true }
                                }
                                mainArray.push(obj);
                            });
                        } else {
                            var obj = {
                                IS_ITEM_APPROVED: true
                            };

                            mainArray.push(obj);
                        }
                    });
                }

                if (mainArray && mainArray.length > 0) {
                    showButton = _.some(mainArray, function (items) {
                        return (items.IS_ITEM_APPROVED === false);
                    });
                } else {
                    showButton = true;
                }

                return !showButton;
            };

            $scope.RouteToRFQ = function ()
            {
                var firstVal = $scope.similarVendors[0].ids.toString();//21,22
                $scope.similarVendors.forEach(function (item, index) {
                    if (item.ids.toString() != firstVal) {
                        item.isSame = false;
                    }
                });

                var isSame = validateVendors();
                if (isSame)
                {
                    swal("Error!", "Please select same vendors.", "error");
                    return;
                }

                var params =
                {
                    "PROJECT_ID": +$scope.PROJECT_ID,
                    "SUB_PACKAGE_IDS": $scope.selectedSubPackagesForReq.join(','),
                    "sessionid": $scope.sessionID
                };

                PRMProjectServices.AreValidToPostRFQS(params)
                    .then(function (response) {
                        if (response) {

                            userService.setProjectID(+$scope.PROJECT_ID);
                            userService.setSelectedSubPackages($scope.selectedSubPackagesForReq);
                            $state.go('save-requirementAdv');
                        } else {
                            swal("Error!", "RFQ is already created for the selected sub packages.", "error");
                            return;
                        }
                    });


            };

            function validateVendors() {
                let isValid = false;
                isValid = _.some($scope.similarVendors, function (item) {
                    return !item.isSame
                });
                return isValid;
            }

            $scope.AssignWorkflowToVendors = function (subpackage) {
                var workflowID = (_.find($scope.workflowListTemp, { location: $scope.PROJECT_DET.COST_CENTER, WorkflowModule: 'VENDOR_SELECTION' }) ? _.find($scope.workflowListTemp, { location: $scope.PROJECT_DET.COST_CENTER, WorkflowModule: 'VENDOR_SELECTION' }).workflowID : 0);

                if (workflowID <= 0) {
                    swal("Error!", "Please create a workflow with " + $scope.PROJECT_DET.COST_CENTER + " cost centre for the vendor selection module.", "error");
                    return;
                }

                $scope.vendorsWorkflows = [];

                $scope.BudgetList.forEach(function (item, index) {
                    if (subpackage.ROW_ID === item.ROW_ID) {
                        item.SUB_PACKAGE_WF_ID = workflowID;
                        $scope.vendorsWorkflows.push(item);
                    }
                });


                var params =
                {
                    "budgetDetailsList": $scope.vendorsWorkflows,
                    "projectId": $scope.PROJECT_ID,
                    "budgetId": $scope.BUDGET_ID,
                    "sessionid": userService.getUserToken()
                };

                PRMProjectServices.UpdateVendorWorkflow(params)
                    .then(function (response) {
                        if (response.errorMessage) {
                            growlService.growl(response.errorMessage, "inverse");
                        } else {
                            growlService.growl('Saved Successfully.', "success");
                            $scope.GetBudgetDetailsbyProject(true);
                        };
                    });
            };

            $scope.similarVendors = [];
            $scope.handlePackageSelect = function (budgetObj) {
                $scope.similarVendors = [];
                budgetObj.NODES.forEach(function (item, index1) {
                    if (item.SUB_PACKAGE_WF_APPROVAL_STATUS === 'APPROVED' && !item.REQ_ID) {
                        item.subPackageSelected = budgetObj.packageSelected;
                        if (item.subPackageSelected) {
                            if (!$scope.selectedSubPackagesForReq.includes(item.SUB_PACKAGE_ID)) {
                                $scope.selectedSubPackagesForReq.push(item.SUB_PACKAGE_ID);

                            }                            
                        } else {
                            $scope.selectedSubPackagesForReq = $scope.selectedSubPackagesForReq.filter(function (item1) {
                                return item1 !== item.SUB_PACKAGE_ID
                            });
                        }
                    }
                });

                if ($scope.BudgetArr && $scope.BudgetArr.length > 0) {
                    $scope.BudgetArr.forEach(function (item, index) {
                        if (item.NODES && item.NODES.length > 0) {
                            var values = _(item.NODES)
                                .filter(item => item.subPackageSelected)
                                .map('VENDOR_IDS')
                                .value();
                            if (values && values.length > 0) {
                                values.forEach(function (item) {
                                    var obj = {
                                        ids: item,
                                        isSame: true
                                    }
                                    $scope.similarVendors.push(obj);
                                });
                            }
                        }
                    });
                }

            };

            $scope.validatePackageSelection = function (budgetObj) {
                let isDisabled = true;
                if (budgetObj && budgetObj.NODES && budgetObj.NODES.length > 0) {
                    let validApprovedItems = budgetObj.NODES.filter(function (item1) {
                        return item1.SUB_PACKAGE_WF_APPROVAL_STATUS === 'APPROVED' && !item1.SUB_PACKAGE_WF_PENDING_APPROVERS && $scope.PROJECT_DET.ASSIGNED_U_IDS.includes(+$scope.userID)
                    });

                    if (validApprovedItems && validApprovedItems.length > 0) {
                        isDisabled = false;
                    }

                    let reqAssignedItems = budgetObj.NODES.filter(function (item1) {
                        return !item1.REQ_ID
                    });

                    if (!reqAssignedItems || reqAssignedItems.length <= 0) {
                        isDisabled = true;
                    }
                } else {
                    isDisabled = true;
                }
                
                return isDisabled;
            };
            $scope.similarVendors = [];
            $scope.handleSubPackageSelect = function (subpackage) {
                $scope.similarVendors = [];
                if (subpackage.SUB_PACKAGE_WF_APPROVAL_STATUS === 'APPROVED') {
                    if (subpackage.subPackageSelected) {
                        if (!$scope.selectedSubPackagesForReq.includes(subpackage.SUB_PACKAGE_ID)) {
                            $scope.selectedSubPackagesForReq.push(subpackage.SUB_PACKAGE_ID);
                        }                        
                    } else {
                        $scope.selectedSubPackagesForReq = $scope.selectedSubPackagesForReq.filter(function (item1) {
                            return item1 !== subpackage.SUB_PACKAGE_ID
                        });
                    }

                    if ($scope.BudgetArr && $scope.BudgetArr.length > 0) {
                        $scope.BudgetArr.forEach(function (item,index) {
                            if (item.NODES && item.NODES.length > 0)
                            {
                                var values = _(item.NODES)
                                    .filter(item => item.subPackageSelected)
                                    .map('VENDOR_IDS')
                                    .value();
                                if (values && values.length > 0) {
                                    values.forEach(function (item) {
                                        var obj = {
                                            ids: item,
                                            isSame : true
                                        }
                                        $scope.similarVendors.push(obj);
                                    });
                                }
                            }
                        });

                    }

                }
            };

            $scope.handleSubPackageChange = function (budgetObj, subpackage) {
                let initCount = budgetObj.NODES.length;
                if (subpackage) {
                    subpackage.DEFAULT_SUB_PACKAGE_ID = subpackage.catId;
                    subpackage.EDITED_SUB_PACKAGE_ID = subpackage.SUB_PACKAGE_ID;


                    subpackage.EDITED_SUB_PACKAGE_NAME = subpackage.SUB_PACKAGE_ARR.filter(function (item, index) {
                        return item.catId === subpackage.SUB_PACKAGE_ID;
                    })[0].catCode;

                    
                }
                var uniqueList = _.uniqWith(
                    budgetObj.NODES,
                    (itemA, itemB) =>
                        itemA.SUB_PACKAGE_ID === itemB.SUB_PACKAGE_ID
                );

                if (initCount > uniqueList.length) {
                    let temp = budgetObj.NODES.filter(function (item, index) {
                        return item.SUB_PACKAGE_ID === subpackage.SUB_PACKAGE_ID;
                    });

                    let temp1 = $scope.companyCatalog.filter(function (item, index) {
                        return item.catId === budgetObj.PACKAGE_ID;
                    });
                    
                    swal("Warning!", "sub-packages '" + temp[0].EDITED_SUB_PACKAGE_NAME + "' cannot be duplicate in a package - " + temp1[0].catCode, "error");
                    subpackage.catId = subpackage.DEFAULT_SUB_PACKAGE_ID
                    subpackage.SUB_PACKAGE_ID = subpackage.DEFAULT_SUB_PACKAGE_ID
                }
            };

            $scope.handleWidgetsState = function () {
                $scope.isAllWidgetsCollapsed = !$scope.isAllWidgetsCollapsed;
                $scope.BudgetArr.forEach(function (item, index1) {
                    var subpackagesTemp = _.find($scope.companyCatalog, { catId: item.PACKAGE_ID }).nodes;
                    item.expanded = !$scope.isAllWidgetsCollapsed;
                    item.NODES.forEach(function (nodeItems, nodeItemsIndex) {
                        nodeItems.SUB_PACKAGE_ARR = [];
                        subpackagesTemp.forEach(function (catItem, catIndex) {
                            let catObj =
                            {
                                "catId": catItem.catId,
                                "catCode": catItem.catCode
                            };
                            nodeItems.SUB_PACKAGE_ARR.push(catObj);
                        });
                    });
                });
            };

            $scope.precisionRound = function (number, precision) {
                var factor = Math.pow(10, precision);
                return Math.round(number * factor) / factor;
            };

            $scope.handleSubPackageStatus = function (subpackage,allSubpackages,package) {
                if (subpackage.PACKAGE_STATUS === 'NA')
                {
                    let validSubPackages = _.filter(allSubpackages, function (item) {
                        return item.IS_VALID > 0;
                    });

                    if (validSubPackages.length === 1)
                    {
                        subpackage.PACKAGE_STATUS = 'ACTIVE';
                        growlService.growl("Cannot change the status to NA as the package contains only one sub-package");
                        return;
                    }

                    var getDeletedPackageObj = package[index];
                    if ($scope.BUDGET_ID > 0)
                    {
                        //getDeletedPackageObj.U_ID = +$scope.userID;
                        subpackage.U_ID = +$scope.userID;
                        $scope.PushDeletedPackages.push(subpackage);
                    }

                    subpackage.EST_BUDGET_AMOUNT = 0;
                    subpackage.PO_VALUE = 0;
                    subpackage.ALLOCATION_PERCENTAGE = 0;
                }
            };

            $scope.deletePackage = function (index)
            {
                $scope.BudgetArr[index].IS_VALID = 0;
                let validBudgets = _.filter($scope.BudgetArr, function (item) {
                    return item.IS_VALID > 0;
                });

                if (validBudgets.length <= 0) {
                    $scope.BudgetArr[index].IS_VALID = 1;
                    growlService.growl("Cannot delete all the packages .", "inverse");
                    return;
                }

                var getDeletedPackageObj = $scope.BudgetArr[index];
                $scope.BudgetArr = validBudgets;

                if ($scope.BUDGET_ID > 0)
                {
                    if (!getDeletedPackageObj.IS_NEW_PACKAGE)
                    {
                        getDeletedPackageObj.U_ID = +$scope.userID;
                        $scope.PushDeletedPackages.push(getDeletedPackageObj);
                        if (getDeletedPackageObj.NODES && getDeletedPackageObj.NODES.length > 0) {
                            getDeletedPackageObj.NODES.forEach(function (subPack, subPackIndex) {
                                subPack.IS_VALID = 0;
                                subPack.U_ID = +$scope.userID;
                                $scope.PushDeletedPackages.push(subPack);
                            });
                        }
                    }
                }
                $scope.BudgetArr.forEach(function (budgItem,budgIndex) {
                    budgItem.S_NO = budgIndex + 1;
                });
            };


            $scope.ValidatePackages = function (budgetObj,index)
            {
                if ($scope.BudgetArr && $scope.BudgetArr.length > 0)
                {
                    var newArr = angular.copy($scope.BudgetArr);
                    var newArr1 = [];
                    newArr = newArr.forEach(function (budgItem,budgIndex) {
                        if (budgIndex !== index) {
                            newArr1.push(budgItem);
                        }
                    });

                    var isPackageFound = _.findIndex(newArr1, function (budgetItem) { return budgetItem.PACKAGE_ID === budgetObj.PACKAGE_ID});
                    if (isPackageFound >= 0) {
                        var sno = _.find(newArr1, function (budgetItem) { return budgetItem.PACKAGE_ID === budgetObj.PACKAGE_ID }).S_NO;
                        growlService.growl("Selected package is already added in s.no : " + (sno), "inverse");//
                        $scope.BudgetArr[index].PACKAGE_ID = budgetObj.PACKAGE_ID_TEMP;
                        $scope.showSubPackages(budgetObj);
                    }
                }
            };

            $scope.AddPackage = function (isbudgetApproved,budgetWfId)
            {
                $scope.newPackagesArr = [];

                var alreadyAddedpackageIDS = _($scope.BudgetArr)
                    .filter(item => item.PACKAGE_ID)
                    .map('PACKAGE_ID')
                    .value();

                var addedOnePackage = false;

                $scope.companyCatalog.forEach(function (categoryItem, categoryItemIndex) {
                    if (!addedOnePackage) {
                        var itemFound = _.findIndex(alreadyAddedpackageIDS, function (id) { return id === categoryItem.catId });
                        if (itemFound <= -1) {
                            let BudgetObj =
                            {
                                PROJ_ID: 0,
                                COMP_ID: +$scope.CompId,
                                U_ID: +$scope.userID,
                                PACKAGE_ID: categoryItem.catId,
                                PO_VALUE: 0,
                                NET_REVENUE_MARGIN: 0,
                                TARGETED_REVENUE_BUDGET: 0,
                                SUB_PACKAGE_ID: 0,
                                SUB_PACKAGE_PO_VALUE: 0,
                                ALLOCATION_PERCENTAGE: 0,
                                EST_BUDGET_AMOUNT: 0,
                                PACKAGE_STATUS: '',
                                BUDGET_WF_ID: $scope.BUDGET_ID > 0 && !isbudgetApproved ? budgetWfId : 0,
                                PACKAGE_NAME: categoryItem.catCode,
                                BUDGET_AMOUNT: 0,
                                IS_VALID: 1,
                                IS_NEW_PACKAGE: true,
                                IS_BUDGET_APPROVED: ($scope.BUDGET_ID > 0 && !isbudgetApproved && budgetWfId <= 0 ? false : isbudgetApproved)
                            };
                            $scope.BudgetArr.push(BudgetObj);
                            var getLastValueIndex = ($scope.BudgetArr.length - 1);
                            $scope.showSubPackages($scope.BudgetArr[getLastValueIndex]);
                            addedOnePackage = true;
                        }
                    }
                });

                $scope.BudgetArr.forEach(function (item, index1) {
                    item.S_NO = index1 + 1;
                    item.expanded = false;
                    if (item.NODES && item.NODES.length > 0) {
                        item.NODES.forEach(function (subPackage, subPackageIndex) {
                            subPackage.IS_BUDGET_APPROVED = false;
                            if (subPackage.REQ_ID > 0) {
                                subPackage.IS_BUDGET_APPROVED = true;
                            }
                        });
                    }
                    item.IS_BUDGET_APPROVED = checkRFQPostedForSubPackages(item.NODES,'ALL');
                    if (item.IS_NEW_PACKAGE)
                    {
                        item.ENABLE_DELETE_FOR_PACKAGE = true;
                    } else {
                        if (item.BUDGET_WF_ID > 0 && item.BUDGET_WF_APPROVAL_STATUS === 'APPROVED' && item.BUDGET_WF_PENDING_APPROVERS == null) {
                            var isValid = checkRFQPostedForSubPackages(item.NODES);
                            item.ENABLE_DELETE_FOR_PACKAGE = !isValid;
                        }
                    }

                });

                $scope.isFormdisabled = false;
            };

            //function isRFQPostedForAllSubPackages(subPackages)
            //{
            //    let isValid = false;
            //    isValid = _.every(subPackages, function (item) { return item.REQ_ID > 0; }); // check whether all the subpackages are used in the RFQ's then disable the package if all are used.
            //    return isValid;
            //}

            function checkRFQPostedForSubPackages(subPackages,type) {
                let isValid = false;
                if (type === 'ALL') {
                    isValid = _.every(subPackages, function (item) { return item.REQ_ID > 0; }); // check whether all the subpackages are used in the RFQ's then disable the package if all are used.
                } else {
                    isValid = _.some(subPackages, function (item) { return item.REQ_ID > 0; }); // check whether any of the subpackage has RFQ.
                }
                return isValid;
            }


            $scope.AddSubPackage = function (budgetObj, index) {
                $scope.newSubPackagesArr = [];
                var alreadyAddedsubpackageIDS = _(budgetObj.NODES)
                    .filter(item => item.SUB_PACKAGE_ID && item.IS_VALID)
                    .map('SUB_PACKAGE_ID')
                    .value();

                var subpackages = _.find($scope.companyCatalog, { catId: budgetObj.PACKAGE_ID }).nodes;
                var subpackagesTemp = _.find($scope.companyCatalog, { catId: budgetObj.PACKAGE_ID }).nodes;
                if (subpackages && subpackages.length > 0)
                {
                    var addedOneSubPackage = false;
                    subpackages.forEach(function (subPackageItem, subPackageItemIndex) {
                        var ExistitemFound = _.findIndex(alreadyAddedsubpackageIDS, function (id) { return id === subPackageItem.catId });
                        if (ExistitemFound >= 0) {
                            subPackageItem.IS_VALID = 1;
                        }
                        if (!addedOneSubPackage)
                        {
                            var itemFound = _.findIndex(alreadyAddedsubpackageIDS, function (id) { return id === subPackageItem.catId });
                            if (itemFound <= -1) {
                                let subpackageObj = {
                                    "CreatedBy": +$scope.userID,
                                    "EST_BUDGET_AMOUNT_TEMP": 0,
                                    "ALLOCATION_PERCENTAGE": 0,
                                    "BUDGET_WF_ID": $scope.BUDGET_ID > 0 && !budgetObj.IS_BUDGET_APPROVED ? budgetObj.BUDGET_WF_ID : 0,
                                    "COMP_ID": +$scope.CompId,
                                    "EST_BUDGET_AMOUNT": 0,
                                    "IS_VALID": 1,
                                    "IS_CORE": 1,
                                    "NET_REVENUE_MARGIN": 0,
                                    "PACKAGE_ID": subPackageItem.PACKAGE_ID,
                                    "PACKAGE_STATUS": "ACTIVE",
                                    "PO_VALUE_TEMP": 0,
                                    "SUB_PACKAGE_ID": subPackageItem.catId,
                                    "SUB_PACKAGE_NAME": subPackageItem.catName,
                                    "catName": subPackageItem.catName,
                                    "catId": subPackageItem.catId,
                                    "catCode": subPackageItem.catCode,
                                    "PO_VALUE": 0,
                                    "TARGETED_REVENUE_BUDGET": 0,
                                    "U_ID": +$scope.userID,
                                    "IS_BUDGET_APPROVED": budgetObj.IS_BUDGET_APPROVED,
                                    "SUB_PACKAGE_ARR":[]
                                };
                                subpackagesTemp.forEach(function (catItem, catIndex) {
                                    let catObj =
                                    {
                                        "catId": catItem.catId,
                                        "catCode": catItem.catCode
                                    };
                                    subpackageObj.SUB_PACKAGE_ARR.push(catObj);
                                });
                                budgetObj.NODES.push(subpackageObj);
                                //categoryItem.IS_VALID = 1;
                                subPackageItem.IS_VALID = 1;
                                addedOneSubPackage = true;
                            }
                        }
                    })
                }
                
            };

            $scope.getSetupBudgetAudit = function () {
                $state.go('setupBudgetAudit', { "PROJECT_ID": $scope.PROJECT_ID, "BUDGET_ID": $scope.BUDGET_ID });
            }


            $scope.DeleteSubPackage = function (budgetObj,index)
            {
                budgetObj.NODES[index].IS_VALID = 0;
                var subPackageId = budgetObj.NODES[index].PACKAGE_ID;

                let validSubPackages = _.filter(budgetObj.NODES, function (item) {
                    return item.IS_VALID > 0;
                });

                if (validSubPackages.length <= 0) {
                    budgetObj.NODES[index].IS_VALID = 1;
                    growlService.growl("Cannot delete all the sub packages in a package.", "inverse");
                    return;
                }

                var getDeletedPackageObj = budgetObj.NODES[index];
                if ($scope.BUDGET_ID > 0 && !budgetObj.IS_NEW_PACKAGE)
                {
                    getDeletedPackageObj.U_ID = +$scope.userID;
                    $scope.PushDeletedPackages.push(getDeletedPackageObj);
                }
                budgetObj.NODES = validSubPackages;
                //$scope.companyCatalog.forEach(function (item,index) {
                //    item.IS_VALID = 1;
                //    if (item.catId === subPackageId)
                //    {
                //        item.IS_VALID = 0;
                //    }
                //});

                //$scope.companyCatalogTemp.forEach(function (item, index) {
                //    item.VALID = 1;
                //    if (item.catId === subPackageId) {
                //        item.IS_VALID = 0;
                //    }
                //});
            };

            $scope.totalAttachmentMaxSize = 6291456;
            $scope.totalRequirementSize = 0;
            $scope.totalRequirementItemSize = 0;
            $scope.multipleAttachments = [];



            $scope.getFile = function () {
                $scope.progress = 0;
                $scope.totalRequirementSize = 0;
                $scope.multipleAttachments = $("#attachement")[0].files;
                $scope.multipleAttachments = Object.values($scope.multipleAttachments);
                $scope.BudgetArr.forEach(function (budgetItem, budgetIndex) {
                    budgetItem.MULTIPLE_ATTACHMENTS = [];
                    if (budgetItem.NODES && budgetItem.NODES.length > 0) {
                        budgetItem.NODES.forEach(function (nodeItem, nodeIndex) {
                            nodeItem.MULTIPLE_ATTACHMENTS = [];
                        });
                    }
                });
                if ($scope.multipleAttachments && $scope.multipleAttachments.length > 0) {
                    $scope.multipleAttachments.forEach(function (item, index) {
                        $scope.totalRequirementSize = $scope.totalRequirementSize + item.size;
                    });
                }

                if (($scope.totalRequirementSize + $scope.totalRequirementItemSize) > $scope.totalAttachmentMaxSize) {
                    swal({
                        title: "Attachment size!",
                        text: "Total Attachments size cannot exceed 6MB",
                        type: "warning",
                        showCancelButton: false,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Ok",
                        closeOnConfirm: true
                    },
                        function () {
                            return;
                        });
                    return;
                }

                $scope.multipleAttachments.forEach(function (item, index) {
                    fileReader.readAsDataUrl(item, $scope)
                        .then(function (result) {

                            var fileUpload = {
                                fileStream: [],
                                fileName: '',
                                fileID: 0
                            };
                            var bytearray = new Uint8Array(result);
                            fileUpload.fileStream = $.makeArray(bytearray);
                            fileUpload.fileName = $scope.file.name;
                            $scope.BudgetArr.forEach(function (budgItem, budgIndex) {
                                budgItem.MULTIPLE_ATTACHMENTS.push(fileUpload);
                                if (budgItem.NODES && budgItem.NODES.length > 0) {
                                    budgItem.NODES.forEach(function (nodeItem, nodeIndex) {
                                        nodeItem.MULTIPLE_ATTACHMENTS.push(fileUpload);
                                    });
                                }
                            });
                            
                        });
                });

            };


            $scope.removeAttach = function () {
                $scope.BudgetArr.forEach(function (budgetItem, budgetIndex) {
                    budgetItem.MULTIPLE_ATTACHMENTS = [];
                    if (budgetItem.NODES && budgetItem.NODES.length > 0) {
                        budgetItem.NODES.forEach(function (nodeItem, nodeIndex) {
                            nodeItem.MULTIPLE_ATTACHMENTS = [];
                        });
                    }
                });
            };

            $scope.getMaxOrderIndexForTheModule = function (index,workflowObj)
            {
                let foundIndex = -1;
                workflowObj.NEW_STATUS = 'Pending';
                var filteredArr = _.filter($scope.itemWorkflow1[0].WorkflowTracks, { moduleID: workflowObj.moduleID });
                var firstValueWF = _.orderBy(filteredArr, ['order'], ['desc'])[0];
                var getMaxOrderIndex = _.findIndex($scope.itemWorkflow1[0].WorkflowTracks, function (item) { return item.moduleID === workflowObj.moduleID && item.order === firstValueWF.order });
                if (index === getMaxOrderIndex) {
                    foundIndex = getMaxOrderIndex;
                    if (workflowObj.statusNew === 'Approved')
                    {
                        workflowObj.NEW_STATUS = 'Approved';
                    }
                }
                if (foundIndex >= 1) {
                    $scope.getStyles();
                }
                return foundIndex;
            };

            $scope.getStyles = function () {
                return 'display:none;';
            };

        }]);
