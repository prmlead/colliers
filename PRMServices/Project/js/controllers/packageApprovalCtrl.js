prmApp
    .controller('packageApprovalCtrl', ["$scope", "$stateParams", "$log", "$state", "$window", "userService", "auctionsService",
        "storeService", "growlService", "poService", "$rootScope", "catalogService", "fileReader", "PRMProjectServices", "workflowService", "PRMPOService",
        "reportingService","$timeout",
        function ($scope, $stateParams, $log, $state, $window, userService, auctionsService,
            storeService, growlService, poService, $rootScope, catalogService,
            fileReader, PRMProjectServices, workflowService, PRMPOService, reportingService, $timeout) {

            $scope.userId = userService.getUserId();
            $scope.highlatedQCS = $stateParams.qcsId;
            $scope.sessionId = userService.getUserToken();
            $scope.CompId = userService.getUserCompanyId();
            $scope.catalogCompId = userService.getUserCatalogCompanyId();
            $scope.isSuperUser = userService.getUserObj().isSuperUser;
            $scope.PROJECT_ID = $stateParams.projectId;
            $scope.BUDGET_ID = $stateParams.budgetId;
            $scope.PACKAGE_APPROVAL_ID = +$stateParams.packageApprovalId;
            $scope.ProjectObject = $stateParams.detailsObj ? $stateParams.detailsObj : "";
            $scope.BudgetList = [];
            $scope.selectedPOSubPackages = [];
            $scope.requirementDetails = {};
            $scope.qcsRequirementDetails = {};
            $scope.isAllWidgetsCollapsed = true;
            // Pagination //
            $scope.loaderMore = false;
            $scope.scrollended = false;
            $scope.page = 0;
            var page = 0;
            $scope.PageSize = 200;
            var totalData = 0;
            $scope.fetchRecordsFrom = $scope.page * $scope.PageSize;
            $scope.totalCount = 0;
            // Pagination //

            $scope.addnewconfigView = false;
            $scope.expand = false;
            $scope.expand1 = false;
            $scope.expand2 = false;
            $scope.deptIDs = [];
            $scope.desigIDs = [];

            /*region start WORKFLOW*/
            $scope.workflowList = [];
            $scope.itemWorkflow = [];
            $scope.workflowObj = {};
            $scope.workflowObj.workflowID = 0;
            $scope.currentStep = 0;
            $scope.orderInfo = 0;
            $scope.assignToShow = '';
            $scope.isWorkflowCompleted = false;
            $scope.WorkflowModule = 'PACKAGE_APPROVAL';
            $scope.disableWFSelection = false;
            $scope.isProcurementUser = false;


            $scope.workflowStatusArr =
                [
                    {
                        "id": 'All'
                    },{
                        "id":'Pending for submission'
                    },
                    {
                        "id": 'Approved'
                    },
                    {
                        "id": 'Rejected'
                    }
                ];


            /*region end WORKFLOW*/


            $scope.ListUserDepartmentDesignations = userService.getListUserDepartmentDesignations();
            if ($scope.ListUserDepartmentDesignations && $scope.ListUserDepartmentDesignations.length > 0) {
                $scope.ListUserDepartmentDesignations.forEach(function (item, index) {
                    $scope.deptIDs.push(item.deptID);
                    item.listDesignation.forEach(function (item1, index1) {
                        if (item1.isAssignedToUser && item1.isValid) {
                            $scope.desigIDs.push(item1.desigID);
                        }
                    });
                });
            }

            $scope.GetProjectDetails = function () {
                var params =
                {
                    "PROJECT_ID": $scope.PROJECT_ID,
                    "sessionid": userService.getUserToken()
                };

                PRMProjectServices.GetProjectDetails(params)
                    .then(function (response) {
                        $scope.PROJECT_DET = response;

                        if ($scope.PROJECT_DET) {
                            if (!$scope.isProcurementUser && ($scope.PROJECT_DET.PROCUREMENT_U_IDS_STR.includes($scope.userId) || $scope.isSuperUser)) {
                                $scope.isProcurementUser = true;
                            }
                        }
                    });

            };


            if (+$scope.PROJECT_ID > 0) {
                $scope.GetProjectDetails();
            }

            $scope.ProjectObject = $stateParams.detailsObj ? $stateParams.detailsObj : '';

            $scope.editConfiguration = function (Id) {
                if (Id === 0) {
                    $scope.addnewconfigView = true;
                }
            }

            $scope.closeEditConfiguration = function () {
                // $scope.addnewconfigView = false;
                $scope.ProjectObject = {
                    COMP_ID: $scope.COMP_ID,
                    PROJECT_ID: '',
                    PROJECT_NAME: '',
                    COST_CENTER: '',
                    LOCATION: '',
                    COLLIEAR_PO_VALUE: '',
                    PO_NUMBER: '',
                    NET_REVENUE_MARGIN: '',
                    SHIP_TO_ADDRESS: '',
                    BILL_TO_ADDRESS: '',
                    TARGETED_REVENUE_BUDGET: '',
                    CREATED_BY: $scope.USER_ID

                }
                $state.go("projectDetails");

            };
            $scope.showPackage = function (Id) {
                $scope.expand = false;
                if (Id == 0) {
                    $scope.expand = true;
                } else if (Id == 1) {
                    $scope.expand = false;
                }
            }

            $scope.showPackage1 = function (Id) {
                $scope.expand1 = false;
                if (Id == 0) {
                    $scope.expand1 = true;
                } else if (Id == 1) {
                    $scope.expand1 = false;
                }
            }

            $scope.showPackage2 = function (Id) {
                $scope.expand2 = false;
                if (Id == 0) {
                    $scope.expand2 = true;
                } else if (Id == 1) {
                    $scope.expand2 = false;
                }
            }

            $scope.showallitems = false;
            $scope.showitem = false;

            $scope.checkAllitems = function (value) {
                if (value) {
                    $scope.showitem = true;
                } else {
                    $scope.showitem = false;
                }


            }
            $scope.showallitems1 = false;
            $scope.showitem1 = false;

            $scope.checkAllitems1 = function (value) {
                if (value) {
                    $scope.showitem1 = true;
                } else {
                    $scope.showitem1 = false;
                }


            }

            $scope.SaveBudgetDetails = function () {
                $scope.BudgetList = [];
                $scope.BudgetArr.forEach(function (budgetItem, budgetItemIndex) {
                    if (budgetItem && budgetItem.PACKAGE_ID) {
                        budgetItem.U_ID = +$scope.userId;
                        $scope.BudgetList.push(budgetItem);
                        if (budgetItem && budgetItem.NODES && budgetItem.NODES.length > 0) {
                            budgetItem.NODES.forEach(function (subBudgetItem, budgetItemIndex) {
                                if (subBudgetItem.SUB_PACKAGE_ID) {
                                    subBudgetItem.COMP_ID = budgetItem.COMP_ID;
                                    subBudgetItem.U_ID = (budgetItem.U_ID ? budgetItem.U_ID : +$scope.userId);
                                    $scope.BudgetList.push(subBudgetItem);
                                }
                            });
                        }
                    }
                });



                var params =
                {
                    "budgetDetailsList": $scope.BudgetList,
                    "projectId": $scope.PROJECT_ID,
                    "budgetId": $scope.BUDGET_ID,
                    "sessionid": userService.getUserToken()
                };



                PRMProjectServices.SaveBudgetDetails(params)
                    .then(function (response) {
                        if (response.objectID) {
                            growlService.growl('Saved Successfully.', "success");
                            $state.go('.', { budgetId: response.objectID }, { notify: false });
                            $scope.BUDGET_ID = response.objectID;
                            $scope.GetBudgetDetailsbyProject();
                        } else {
                            growlService.growl(response.errorMessage, "inverse");
                        };
                    });
            };

            $scope.BudgetArr = [];
            $scope.BudgetObj =
            {
                PROJ_ID: 0,
                COMP_ID: +$scope.CompId,
                U_ID: +$scope.userId,
                PACKAGE_ID: 0,
                PO_VALUE: 0,
                NET_REVENUE_MARGIN: 0,
                TARGETED_REVENUE_BUDGET: 0,
                SUB_PACKAGE_ID: 0,
                SUB_PACKAGE_PO_VALUE: 0,
                ALLOCATION_PERCENTAGE: 0,
                EST_BUDGET_AMOUNT: 0,
                PACKAGE_STATUS: '',
                BUDGET_WF_ID: 0,
                PACKAGE_NAME: '',
                BUDGET_AMOUNT: 0
            };

            $scope.getcategories = function (IsPaging) {
                var catNodes = [];
                catalogService.getcategories($scope.catalogCompId, $scope.fetchRecordsFrom, $scope.PageSize)
                    .then(function (response) {
                        response.forEach(function (catItem, catIndex) {
                            catItem.catNameTemp = catItem.catName.toLowerCase();
                        });
                        response = response.filter(function (item, index) {
                            return item.IS_CORE === 1;
                        });
                        $scope.companyCatalogTemp = [];
                        $scope.companyCatalog = response;
                        $scope.companyCatalogTemp = response;
                        $scope.companyCatalog = _.orderBy($scope.companyCatalog, ['isDefault'], ['desc']);

                        $scope.companyCatalog.forEach(function (item, index) {
                            let BudgetObj =
                            {
                                PROJ_ID: 0,
                                COMP_ID: +$scope.CompId,
                                U_ID: +$scope.userId,
                                PACKAGE_ID: item.isDefault ? item.catId : 0,
                                PO_VALUE: 0,
                                NET_REVENUE_MARGIN: 0,
                                TARGETED_REVENUE_BUDGET: 0,
                                SUB_PACKAGE_ID: 0,
                                SUB_PACKAGE_PO_VALUE: 0,
                                ALLOCATION_PERCENTAGE: 0,
                                EST_BUDGET_AMOUNT: 0,
                                PACKAGE_STATUS: '',
                                BUDGET_WF_ID: 0,
                                PACKAGE_NAME: '',
                                BUDGET_AMOUNT: 0,
                                ANTICIPATED_PO_VALUE: 0
                            };
                            $scope.BudgetArr.push(BudgetObj);
                            if (item.isDefault) {
                                $scope.showSubPackages($scope.BudgetArr[index]);
                            }
                        });

                        $scope.BudgetArr.forEach(function (item, index1) {
                            item.expanded = false;
                        });

                        if ($scope.companyCatalog && $scope.companyCatalog.length > 0) {
                            $scope.totalCount = $scope.companyCatalog[0].totalCategories;
                        }

                        $scope.data = [{
                            'compId': 0,
                            'catId': 0,
                            'catName': 'Package Index',
                            'catDesc': '',
                            'nodes': $scope.companyCatalog,
                            'catParentId': 0
                        }];

                        if (+$scope.BUDGET_ID > 0 && +$scope.PROJECT_ID > 0) {
                            $scope.GetCompanyVendors();
                            //$scope.GetBudgetDetailsbyProject();
                        }
                    });
            };

            $scope.getcategories();

            $scope.GetBudgetDetailsbyProject = function () {
                var params =
                {
                    "projectId": $scope.PROJECT_ID,
                    "budgetId": $scope.BUDGET_ID,
                    "onlyApproved": false,
                    "sessionId": $scope.sessionId
                };

                PRMProjectServices.GetBudgetDetailsbyProject(params)
                    .then(function (response) {
                        $scope.BudgetList = response;

                        if ($scope.BudgetList && $scope.BudgetList.length > 0) {
                            var validList = $scope.BudgetList.filter(function (item) {
                                item.EST_BUDGET_AMOUNT_TEMP = item.EST_BUDGET_AMOUNT;
                                item.NET_REVENUE_MARGIN_TEMP = item.NET_REVENUE_MARGIN;
                                item.PO_VALUE_TEMP = item.PO_VALUE;
                                if (item.IS_VALID === 1 && item.PACKAGE_STATUS != 'NA') {
                                    return item;
                                }
                            })
                            $scope.BudgetList = validList;
                        }

                        if ($scope.PACKAGE_APPROVAL_ID > 0)
                        {
                            $scope.getItemWorkflow($scope.PACKAGE_APPROVAL_ID, $scope.WorkflowModule);
                        }

                        fillValues();

                    });
            };

            $scope.getStyles = function () {
                return 'width:325px;height: ' + angular.element('#productTable')[0].offsetHeight + 'px;max-height: 400px; overflow-y: auto;';
            };

            $scope.BudgetArrTemp = [];

            function fillValues() {
                $scope.BudgetArr = [];
                let counter = 0;
                $scope.BudgetList.forEach(function (budget, index) {
                    if (budget.PACKAGE_ID && !budget.SUB_PACKAGE_ID) {
                        $scope.BudgetArr[counter] = budget;
                        $scope.showSubPackages($scope.BudgetArr[counter]);
                        counter++
                    }

                    if (budget.PACKAGE_ID && budget.SUB_PACKAGE_ID) {
                        let currentPackage = $scope.BudgetArr.filter(function (temp) {
                            return temp.PACKAGE_ID === budget.PACKAGE_ID;
                        });
                        if (currentPackage && currentPackage.length > 0) {
                            let currentSubPackage = currentPackage[0].NODES.filter(function (temp1) {
                                return temp1.PACKAGE_ID === budget.PACKAGE_ID && temp1.catId === budget.SUB_PACKAGE_ID;
                            });

                            if (currentSubPackage && currentSubPackage.length > 0) {
                                budget.catCode = currentSubPackage[0].catCode;
                                budget.catId = budget.SUB_PACKAGE_ID;
                                if (budget.POVendors) {
                                    var vendorsList = JSON.parse(budget.POVendors);
                                    if (vendorsList) {
                                        budget.selectedVendors = $scope.vendors.filter(function (obj) {
                                            return vendorsList.some(function (obj2) {
                                                return obj2.U_ID == obj.id;
                                            });
                                        });

                                        // when no vendors are selected. List of vendors are shown from companyvendors
                                        if (budget.selectedVendors.length === 0) {
                                            budget.selectedVendors = $scope.vendors;
                                        }
                                    }

                                    if (budget.selectedVendors && budget.selectedVendors.length > 0) {
                                        $scope.selectVendors('', budget);
                                    }
                                }

                                currentPackage[0].NODES = currentPackage[0].NODES.filter(function (temp1) {
                                    return temp1.catId !== budget.SUB_PACKAGE_ID;
                                });

                                currentPackage[0].NODES.unshift(budget);
                            }
                        }
                    }
                });

                $scope.BudgetArrTemp = $scope.BudgetArr;


                $scope.IS_PO_GENERATED_FOR_ALL_THE_ITEMS = false;
                var rfqItems = -1;
                var poItems = 0;
                var getOnlyRFQCreatedItems = _.filter($scope.BudgetList, function (budget) { return budget.REQ_ID > 0; });
                if (getOnlyRFQCreatedItems && getOnlyRFQCreatedItems.length > 0)
                {
                    rfqItems = getOnlyRFQCreatedItems.length;
                }
                var getOnlyPOCreatedItems = _.filter($scope.BudgetList, function (budget) { return budget.PO_NUMBER; });
                if (getOnlyPOCreatedItems && getOnlyPOCreatedItems.length > 0)
                {
                    poItems = getOnlyPOCreatedItems.length;
                }
                if (rfqItems === poItems) 
                {
                    $scope.IS_PO_GENERATED_FOR_ALL_THE_ITEMS = true;
                }

                if ($scope.PACKAGE_APPROVAL_ID > 0) {
                    if ($scope.itemWorkflow[0].WorkflowTracks && $scope.itemWorkflow[0].WorkflowTracks.length > 0) {
                        var workflowsTracks = _.orderBy($scope.itemWorkflow[0].WorkflowTracks, ['order'], ['desc']);
                        if (workflowsTracks && workflowsTracks.length > 0) {
                            var getFirstWorkflow = _.first(workflowsTracks);
                            var lastApproverStatus = getFirstWorkflow.status;
                        }
                        var count = 0;
                        $scope.itemWorkflow[0].WorkflowTracks.forEach(function (trackItem, trackItemIndex) {
                            $scope.BudgetArr.forEach(function (budg, budgIndex) {
                                if (budg.NODES && budg.NODES.length > 0) {
                                    budg.NODES.forEach(function (node, nodeIndex) {
                                        if (node.PACKAGE_APPROVAL_ID > 0 && node.PACKAGE_APPROVAL_WF_APPROVAL_STATUS === 'PENDING') {
                                            if (trackItem.status === 'PENDING') {
                                                budg.PACKAGE_APPROVAL_STATUS = 'Pending for ' + trackItem.approver.desigCode;
                                                node.PACKAGE_APPROVAL_STATUS = 'Pending for ' + trackItem.approver.desigCode;
                                                return;
                                            }
                                        } else {
                                            if (trackItem.status === 'PENDING') {
                                                budg.PACKAGE_APPROVAL_STATUS = 'Pending for ' + trackItem.approver.desigCode;
                                                node.PACKAGE_APPROVAL_STATUS = 'Pending for ' + trackItem.approver.desigCode;
                                                return;
                                            } else if (node.PACKAGE_APPROVAL_WF_APPROVAL_STATUS === 'APPROVED') {
                                                budg.PACKAGE_APPROVAL_STATUS = 'Approved';
                                                node.PACKAGE_APPROVAL_STATUS = 'Approved';
                                            }
                                            //else if (lastApproverStatus === 'APPROVED') {
                                            //    budg.PACKAGE_APPROVAL_STATUS = 'Approved';
                                            //    node.PACKAGE_APPROVAL_STATUS = 'Approved';
                                            //}
                                        }
                                    });
                                }
                            });
                            count++;
                        })
                    }
                } else {
                    $scope.BudgetArr.forEach(function (budg, budgIndex) {
                        if (budg.NODES && budg.NODES.length > 0) {
                            budg.NODES.forEach(function (node, nodeIndex) {
                                budg.PACKAGE_APPROVAL_STATUS = 'Pending for submission';
                                node.PACKAGE_APPROVAL_STATUS = 'Pending for submission';
                            });
                        }
                    });
                }
            }

            /*region start WORKFLOW*/

            $scope.getWorkflows = function () {
                workflowService.getWorkflowList()
                    .then(function (response) {
                        $scope.workflowList = [];
                        $scope.workflowListDeptWise = [];
                        $scope.workflowListSubPackage = [];
                        $scope.workflowListTemp = response;

                        $scope.workflowListTemp.forEach(function (item, index) {
                            if (item.WorkflowModule === $scope.WorkflowModule) {
                                $scope.workflowList.push(item);
                                $scope.workflowListDeptWise.push(item);
                            }
                        });

                        if ($scope.isSuperUser) {
                            $scope.workflowList = $scope.workflowList;
                        }
                        else {
                            $scope.workflowList = [];
                            $scope.workflowListDeptWise.forEach(function (wf, idx) {
                                $scope.deptIDs.forEach(function (dep) {
                                    if (dep == wf.deptID) {
                                        $scope.workflowList.push(wf);
                                    }
                                });
                            });

                        }
                    });
            };

            $scope.getWorkflows();

            $scope.IS_PACKAGE_APPROVED = false;

            $scope.getItemWorkflow = function (budgetID, workflowModule) {
                workflowService.getItemWorkflow(0, budgetID, workflowModule)
                    .then(function (response) {
                        $scope.itemWorkflow = response;
                        $scope.checkIsFormDisable();
                        if ($scope.itemWorkflow && $scope.itemWorkflow.length > 0 && $scope.itemWorkflow[0].WorkflowTracks.length > 0) {
                            $scope.currentStep = 0;

                            var count = 0;

                            $scope.itemWorkflow[0].WorkflowTracks.forEach(function (track) {
                                if (!track.multipleAttachments) {
                                    track.multipleAttachments = [];
                                }

                                if (track.status === 'APPROVED') { track.statusNew = 'Approved'; }
                                if (track.status === 'HOLD') { track.statusNew = 'Hold'; }
                                if (track.status === 'PENDING') { track.statusNew = 'Pending'; }
                                if (track.status === 'REJECTED') { track.statusNew = 'Rejected'; }

                                if (track.status === 'APPROVED' || track.status === 'HOLD') {
                                    $scope.isFormdisabled = true;
                                }

                                if (track.status === 'APPROVED') {
                                    $scope.isWorkflowCompleted = true;
                                    $scope.orderInfo = track.order;
                                    $scope.assignToShow = track.status;

                                }
                                else {
                                    $scope.isWorkflowCompleted = false;
                                }

                                if (track.status === 'REJECTED' && count == 0) {
                                    count = count + 1;
                                }

                                if ((track.status === 'PENDING' || track.status === 'HOLD') && count == 0) {
                                    count = count + 1;
                                    //$scope.IsUserApproverForStage(track.approverID);
                                    $scope.currentAccess = track.order;
                                }

                                if ((track.status === 'PENDING' || track.status === 'HOLD' || track.status === 'REJECTED') && $scope.currentStep === 0) {
                                    $scope.currentStep = track.order;
                                    return false;
                                }
                            });

                            fillValues();

                        }


                        var workflowsTracks = _.orderBy($scope.itemWorkflow[0].WorkflowTracks, ['order'], ['desc']);
                        if (workflowsTracks && workflowsTracks.length > 0) {
                            var getFirstWorkflow = _.first(workflowsTracks);
                            if (getFirstWorkflow.status === 'APPROVED') {
                                $scope.IS_PACKAGE_APPROVED = true;
                            }
                        }

                        if ($scope.itemWorkflow[0].WorkflowTracks && $scope.itemWorkflow[0].WorkflowTracks.length > 0) {
                            $scope.itemWorkflow[0].WorkflowTracks.forEach(function (track,trackIndex) {
                                $scope.workflowStatusArr.push({ "id": 'Pending for ' + track.approver.desigCode});
                            });
                        }

                    });
            };


            $scope.isApproverDisable = function (index) {

                var disable = true;

                var previousStep = {};

                $scope.itemWorkflow[0].WorkflowTracks.forEach(function (step, stepIndex) {

                    if (index == stepIndex) {
                        if (stepIndex == 0) {
                            if ($scope.isUserBelongsToDeptandDesig(step.department.deptID, step.approver.desigID) &&
                                (step.status === 'PENDING' || step.status === 'HOLD')) {
                                disable = false;
                            }
                            else {
                                disable = true;
                            }
                        }
                        else if (stepIndex > 0) {
                            if (previousStep.status === 'PENDING' || previousStep.status === 'HOLD' || previousStep.status === 'REJECTED') {
                                disable = true;
                            }
                            else if ($scope.isUserBelongsToDeptandDesig(step.department.deptID, step.approver.desigID) &&
                                (step.status === 'PENDING' || step.status === 'HOLD')) {
                                disable = false;
                            }
                            else {
                                disable = true;
                            }
                        }
                    }
                    previousStep = step;
                });

                return disable;
            };

            $scope.isUserBelongsToDeptandDesig = function (deptID, desigID) {
                var isEligible = true;

                if ($scope.deptIDs.indexOf(deptID) != -1 && $scope.desigIDs.indexOf(desigID) != -1) {
                    isEligible = true;
                } else {
                    isEligible = false;
                }

                return isEligible;
            };

            $scope.updateTrack = function (step, status, type) {
                $scope.disableAssignPR = true;
                $scope.commentsError = '';
                step.isLastApprover = false;

                if (step.comments != null || step.comments != "" || step.comments != undefined) {
                    step.comments = validateStringWithoutSpecialCharacters(step.comments);
                }
                var tempArray = $scope.itemWorkflow[0].WorkflowTracks[$scope.itemWorkflow[0].WorkflowTracks.length - 1];
                if (step.order == tempArray.order && status === 'APPROVED') {
                    $scope.disableAssignPR = false;
                    step.isLastApprover = true;
                } else {
                    $scope.disableAssignPR = true;
                }

                if ($scope.isReject) {
                    $scope.commentsError = 'Please Save Rejected Items/Qty';
                    return false;
                }

                if (status === 'REJECTED' && (step.comments == null || step.comments == "")) {
                    $scope.commentsError = 'Please enter comments';
                    return false;
                }

                step.status = status;
                step.sessionID = $scope.sessionId;
                step.modifiedBy = $scope.userId;

                step.moduleName = type ? type : $scope.WorkflowModule;

                step.subModuleName = '';
                step.subModuleID = 0;

                workflowService.SaveWorkflowTrack(step)
                    .then(function (response) {
                        if (response.errorMessage) {
                            growlService.growl(response.errorMessage, "inverse");
                        }
                        else {
                            $scope.getItemWorkflow($scope.PACKAGE_APPROVAL_ID, $scope.WorkflowModule);
                            location.reload();
                        }
                    });
            };

            $scope.calculateAllocationPerc = function (package, subPackage) {
                if (package && package.PO_VALUE && !subPackage) {
                    package.PO_VALUE = +package.PO_VALUE;
                    package.NODES.forEach(function (node, index1) {
                        if (node.SUB_PACKAGE_ID && node.EST_BUDGET_AMOUNT) {
                            node.EST_BUDGET_AMOUNT = +node.EST_BUDGET_AMOUNT;
                            node.ALLOCATION_PERCENTAGE = (node.EST_BUDGET_AMOUNT / package.PO_VALUE) * 100;
                        }
                    });
                }

                if (package && package.PO_VALUE && subPackage && subPackage.EST_BUDGET_AMOUNT) {
                    package.PO_VALUE = +package.PO_VALUE;
                    subPackage.EST_BUDGET_AMOUNT = +subPackage.EST_BUDGET_AMOUNT;
                    subPackage.ALLOCATION_PERCENTAGE = (subPackage.EST_BUDGET_AMOUNT / package.PO_VALUE) * 100;
                }
            };


            $scope.showSubPackages = function (budgetObj) {
                let subPackages = _.find($scope.companyCatalog, { catId: budgetObj.PACKAGE_ID }).nodes;
                if (subPackages && subPackages.length > 0) {
                    budgetObj.subPackageErrorMessage = '';
                    if ($scope.BUDGET_ID > 0 && $scope.BudgetList) {
                        if ($scope.BudgetList.length > 0) {
                            var validSubPackages = _($scope.BudgetList)
                                .filter(item => item.SUB_PACKAGE_ID > 0 && item.PACKAGE_ID === budgetObj.PACKAGE_ID)
                                .map('SUB_PACKAGE_ID')
                                .value();

                            if (validSubPackages && validSubPackages.length > 0) {
                                subPackages = subPackages.filter(function (catItem, catIndex) {
                                    var itemFound = _.findIndex(validSubPackages, function (id) { return id === catItem.catId });
                                    if (itemFound >= 0) {
                                        return catItem;
                                    }
                                });
                            }
                        }
                    }
                }

                budgetObj.NODES = subPackages;
                if (budgetObj.NODES && budgetObj.NODES.length > 0) {
                    budgetObj.NODES.forEach(function (node, index1) {
                        node.PACKAGE_ID = budgetObj.PACKAGE_ID;
                        node.PACKAGE_STATUS = 'ACTIVE';
                        node.SUB_PACKAGE_ID = node.catId;
                        node.SUB_PACKAGE_NAME = node.catCode;
                    });
                }
            };

            $scope.validateSubPackages = function (budgetObj) {
                budgetObj.expanded = !budgetObj.expanded;
                budgetObj.subPackageErrorMessage = '';

                if (!budgetObj.PACKAGE_ID) {
                    //budgetObj.subPackageErrorMessage = 'Please select a package to display sub packages.';
                    budgetObj.expanded = false;
                    alert('Please select a package to display sub packages.');
                }

                if (budgetObj.PACKAGE_ID) {
                    let subPackages = _.find($scope.companyCatalog, { catId: budgetObj.PACKAGE_ID }).nodes;
                    if (subPackages.length <= 0) {
                        //budgetObj.subPackageErrorMessage = 'Sub packages are not available for the selected package.';
                        budgetObj.expanded = false;
                        alert('Sub packages are not available for the selected package.');
                    }

                    if (subPackages.length > 0) {

                        subPackages = _.filter(subPackages, function (item) {
                            return item.IS_VALID > 0;
                        });

                        budgetObj.NODES = $scope.BUDGET_ID > 0 ? budgetObj.NODES : subPackages;
                        if (budgetObj.NODES && budgetObj.NODES.length > 0) {
                            budgetObj.NODES.forEach(function (node, index1) {
                                node.SUB_PACKAGE_ID = node.catId;
                                if (node.PO_NUMBER_TEMP) {
                                    node.PO_NUMBER_TEMP1 = node.PO_NUMBER_TEMP;
                                }
                            });
                        }
                    }
                }
            };

            $scope.AssignPackageApproval = function () {

                var workflowID = (_.find($scope.workflowListTemp, { location: $scope.PROJECT_DET.COST_CENTER, WorkflowModule: $scope.WorkflowModule }) ? _.find($scope.workflowListTemp, { location: $scope.PROJECT_DET.COST_CENTER, WorkflowModule: $scope.WorkflowModule }).workflowID : 0);

                if (workflowID <= 0) {
                    swal("Error!", "Please create a workflow with ( " + $scope.PROJECT_DET.COST_CENTER +" ) cost center for the Package Approval module.", "error");
                    return;
                }


                $scope.BudgetArr.forEach(function (budg, index1) {
                    if (budg.NODES && budg.NODES.length > 0) {
                        budg.NODES.forEach(function (node, index1) {
                            if (node.subPackageSelected && node.QCS_ID == $stateParams.qcsId) {
                                var obj = {
                                    "PACKAGE_ID": budg.PACKAGE_ID,
                                    "SUB_PACKAGE_ID": node.PACKAGE_ID,
                                    "QCS_ID": node.QCS_ID,
                                    "U_ID": +$scope.userId,
                                    "PACKAGE_APPROVAL_ID": node.PACKAGE_APPROVAL_ID > 0 ? node.PACKAGE_APPROVAL_ID : 0
                                };
                                $scope.submitForPackageApproval.push(obj);
                            }
                        });
                    }
                });


                $scope.submitForPackageApproval.forEach(function (item,index) {
                    item.PACKAGE_APPROVAL_WF_ID = workflowID;
                });

                var params =
                {
                    "details": $scope.submitForPackageApproval,
                    "sessionid": $scope.sessionId
                };

                PRMProjectServices.AssignPackageApproval(params)
                    .then(function (response) {
                        if (response.errorMessage) {
                            growlService.growl(response.errorMessage, "inverse");
                        } else {
                            $state.go('.', { packageApprovalId: response.objectID }, { notify: false });
                            $scope.PACKAGE_APPROVAL_ID = response.objectID;
                            growlService.growl('Package Approval Assigned Successfully.', "success");
                            $scope.getItemWorkflow($scope.PACKAGE_APPROVAL_ID, $scope.WorkflowModule);
                        };
                    });
            };

            $scope.isFormdisabled = true;

            $scope.checkIsFormDisable = function () {
                $scope.isFormdisabled = false;
                if ($scope.itemWorkflow.length == 0) {
                    $scope.isFormdisabled = true;
                } else {
                    if (($scope.BudgetList.CREATED_BY == +$scope.userId || $scope.BudgetList.MODIFIED_BY == +$scope.userId) && $scope.itemWorkflow[0].WorkflowTracks.length > 0 &&
                        $scope.itemWorkflow[0].WorkflowTracks[0].status !== "APPROVED" && $scope.itemWorkflow[0].WorkflowTracks[0].order == 1 && $scope.itemWorkflow[0].workflowID > 0) {
                        $scope.isFormdisabled = false;
                    }
                }

            };

            function validateStringWithoutSpecialCharacters(string) {
                if (string) {
                    string = string.replace(/\'/gi, "");
                    string = string.replace(/\"/gi, "");
                    string = string.replace(/[`^_|\?;:'",<>\{\}\[\]\\\/]/gi, "");
                    string = string.replace(/(\r\n|\n|\r)/gm, "");
                    string = string.replace(/\t/g, '');
                    return string;
                }
            }

            $scope.showApprovedDate = function (date) {
                return userService.toLocalDate(date);
            };

            $scope.GetCompanyVendors = function () {
                $scope.vendors = [];
                let vendors = [];

                $scope.params = { "userID": +$scope.userId, "sessionID": userService.getUserToken(), "PageSize": 0, "NumberOfRecords": 100, "searchString": '' };

                userService.GetCompanyVendors($scope.params)
                    .then(function (response) {
                        response.forEach(function (item) {
                            item.companyVendorCodes = _.uniq(item.companyVendorCodes.split(',').map(function (item) { return item.trim() })).join(',');
                        })
                        $scope.vendorsList = response;
                        $scope.vendorsList.forEach(function (item, index) {
                            vendors.push({ id: item.userID, name: item.companyName });
                        });

                        $scope.vendors = vendors;

                        $scope.GetBudgetDetailsbyProject();
                    });
            };


            //$scope.GetCompanyVendors();


            $scope.selectVendors = function (budgetcode, subpackage) {
                if (subpackage && subpackage.selectedVendors) {
                    if (!_.isEmpty(subpackage.selectedVendors)) {
                        let vendorIds = _(subpackage.selectedVendors)
                            .filter(item => item.id)
                            .map('id')
                            .value();
                        subpackage.VENDOR_IDS = vendorIds.join(',');
                    }
                    else {
                        subpackage.VENDOR_IDS = '';
                    }
                }
            };

            $scope.showBudgetApprovalPopUp = true;

            $scope.showBudgetApproval = function () {
                $scope.showBudgetApprovalPopUp = false;
            };

            $scope.validatePOValue = function (obj, type, fieldName) {
                $scope.poValueErrorMessage = '';

                if (type === 'SUB_PACKAGE') {
                    if (obj.SUB_PACKAGE_ID) {
                        if (fieldName === 'PO_VALUE') {
                            let value = 0;
                            value = _.find($scope.BudgetArr, { PACKAGE_ID: obj.PACKAGE_ID }).PO_VALUE;

                            var subPackages = [];
                            $scope.BudgetArr.forEach(function (item, index) {
                                if (item.NODES && item.NODES.length > 0) {
                                    item.NODES.forEach(function (item, index) {
                                        if (item.PO_VALUE && +item.PO_VALUE > 0) {
                                            subPackages.push(+item.PO_VALUE);
                                        }
                                    });
                                }
                            });
                            var totalSubPackages = 0;
                            totalSubPackages = _.sumBy(subPackages);
                            if (totalSubPackages > 0) {
                                if (totalSubPackages > $scope.PROJECT_DET.PO_VALUE) {
                                    $scope.poValueErrorMessage = 'Given Value is Exceeding Project Collier PO Value.'
                                    obj.PO_VALUE = obj.PO_VALUE_TEMP;
                                }
                                if (totalSubPackages > value) {
                                    $scope.poValueErrorMessage = 'Given Value is Exceeding Overall Package Collier PO Value.'
                                    obj.PO_VALUE = obj.PO_VALUE_TEMP;
                                    return;
                                }
                            }
                        } else if (fieldName === 'EST_BUDGET') {
                            let value = 0;
                            value = _.find($scope.BudgetArr, { PACKAGE_ID: obj.PACKAGE_ID }).EST_BUDGET_AMOUNT;

                            var subPackages = [];
                            $scope.BudgetArr.forEach(function (item, index) {
                                if (item.NODES && item.NODES.length > 0) {
                                    item.NODES.forEach(function (item, index) {
                                        if (item.EST_BUDGET_AMOUNT && +item.EST_BUDGET_AMOUNT > 0) {
                                            subPackages.push(+item.EST_BUDGET_AMOUNT);
                                        }
                                    });
                                }
                            });
                            var totalSubPackages = 0;
                            totalSubPackages = _.sumBy(subPackages);
                            if (totalSubPackages > 0) {
                                if (totalSubPackages > $scope.PROJECT_DET.TARGETED_REVENUE_BUDGET) {
                                    $scope.poValueErrorMessage = 'Given Value is Exceeding Targeted Revenue Budget.'
                                    obj.EST_BUDGET_AMOUNT = obj.EST_BUDGET_AMOUNT_TEMP;
                                }

                                if (totalSubPackages > value) {
                                    $scope.poValueErrorMessage = 'Given Value is Exceeding Package Level Budget Amount Value.'
                                    obj.EST_BUDGET_AMOUNT = obj.EST_BUDGET_AMOUNT_TEMP;
                                    return;
                                }

                            }

                        }
                    }
                } else {
                    if (obj.PACKAGE_ID) {
                        let selectedPackage = _.find($scope.BudgetArr, { PACKAGE_ID: obj.PACKAGE_ID }).NODES;
                        if (selectedPackage && selectedPackage.length > 0) {
                            selectedPackage.forEach(function (item, index) {
                                item.PO_VALUE = 0;
                            });
                        }

                        let allPackages = _.sumBy(_.map($scope.BudgetArr, 'PO_VALUE'));
                        if (+allPackages > +$scope.PROJECT_DET.PO_VALUE) {
                            $scope.poValueErrorMessage = 'Given Value is Exceeding Overall Package Collier PO Value.'
                            obj.PO_VALUE = obj.PO_VALUE_TEMP;
                            return;
                        }

                    }
                }
            };

            $scope.calculatePackageValues = function (obj, type) {
                $scope.calculationErrorMessage = '';
                if (obj.PACKAGE_ID) {
                    let packagePOValue = _.find($scope.BudgetArr, { PACKAGE_ID: obj.PACKAGE_ID }).PO_VALUE;
                    let packageBudgetValue = _.find($scope.BudgetArr, { PACKAGE_ID: obj.PACKAGE_ID }).EST_BUDGET_AMOUNT;
                    if (type === 'NET_MARGIN' || type === 'PO_VALUE') {
                        if (packagePOValue <= 0) {
                            obj.NET_REVENUE_MARGIN = 0;
                        } else {
                            obj.NET_REVENUE_MARGIN = ((packagePOValue - packageBudgetValue) / packagePOValue) * 100;
                        }

                        if (obj.NET_REVENUE_MARGIN < 0 || obj.NET_REVENUE_MARGIN > 100) {
                            obj.NET_REVENUE_MARGIN = obj.NET_REVENUE_MARGIN_TEMP;
                            $scope.calculationErrorMessage = 'Net Revenue Margin Cannot be more than 100';
                        }
                        obj.EST_BUDGET_AMOUNT = packagePOValue * (100 - obj.NET_REVENUE_MARGIN) / 100;

                        let allBudgets = _.sumBy(_.map($scope.BudgetArr, 'EST_BUDGET_AMOUNT'));

                        if (allBudgets > $scope.PROJECT_DET.TARGETED_REVENUE_BUDGET) {
                            obj.EST_BUDGET_AMOUNT = obj.EST_BUDGET_AMOUNT_TEMP;
                        }
                    }
                    if (type === 'EST_BUDGET') {
                        if (packagePOValue <= 0) {
                            obj.NET_REVENUE_MARGIN = 0;
                        } else {
                            obj.NET_REVENUE_MARGIN = ((packagePOValue - packageBudgetValue) / packagePOValue) * 100;
                        }
                        let allBudgets = _.sumBy(_.map($scope.BudgetArr, 'EST_BUDGET_AMOUNT').map(i => +i));

                        if (allBudgets > $scope.PROJECT_DET.TARGETED_REVENUE_BUDGET) {
                            obj.EST_BUDGET_AMOUNT = obj.EST_BUDGET_AMOUNT_TEMP;
                            $scope.calculationErrorMessage = 'Budget Amount Cannot be more than Targeted Revenue Budget.';
                        }
                    }

                }
            };


            $scope.showRequirementButton = function () {
                let showButton = false;
                let mainArray = [];
                if ($scope.BudgetList && $scope.BudgetList.length > 0) {
                    $scope.BudgetList.forEach(function (item, index) {
                        if (item.NODES && item.NODES.length > 0) {
                            item.NODES.forEach(function (nodeItem, nodeItemIndex) {
                                var obj = {
                                    IS_ITEM_APPROVED: false
                                };
                                if (nodeItem.SUB_PACKAGE_WF_APPROVAL_STATUS === 'APPROVED' && !nodeItem.SUB_PACKAGE_WF_PENDING_APPROVERS) {
                                    obj = { IS_ITEM_APPROVED: true }
                                }
                                mainArray.push(obj);
                            });
                        } else {
                            var obj = {
                                IS_ITEM_APPROVED: true
                            };

                            mainArray.push(obj);
                        }
                    });
                }

                if (mainArray && mainArray.length > 0) {
                    showButton = _.some(mainArray, function (items) {
                        return (items.IS_ITEM_APPROVED === false);
                    });
                } else {
                    showButton = true;
                }

                return !showButton;
            };

            $scope.RouteToRFQ = function () {
                userService.setProjectID(+$scope.PROJECT_ID);
                $state.go('save-requirementAdv');
            };

            $scope.AssignWorkflowToVendors = function (subpackage) {
                var workflowID = (_.find($scope.workflowListTemp, { location: $scope.PROJECT_DET.COST_CENTER, WorkflowModule: 'VENDOR_SELECTION' }) ? _.find($scope.workflowListTemp, { location: $scope.PROJECT_DET.COST_CENTER, WorkflowModule: 'VENDOR_SELECTION' }).workflowID : 0);

                if (workflowID <= 0) {
                    swal("Error!", "Please create a workflow with " + $scope.PROJECT_DET.COST_CENTER + " cost centre for the vendor selection module.", "error");
                    return;
                }

                $scope.vendorsWorkflows = [];

                $scope.BudgetList.forEach(function (item, index) {
                    if (subpackage.ROW_ID === item.ROW_ID) {
                        item.SUB_PACKAGE_WF_ID = workflowID;
                        $scope.vendorsWorkflows.push(item);
                    }
                });


                var params =
                {
                    "budgetDetailsList": $scope.vendorsWorkflows,
                    "projectId": $scope.PROJECT_ID,
                    "budgetId": $scope.BUDGET_ID,
                    "sessionid": userService.getUserToken()
                };

                PRMProjectServices.UpdateVendorWorkflow(params)
                    .then(function (response) {
                        if (response.errorMessage) {
                            growlService.growl(response.errorMessage, "inverse");
                        } else {
                            growlService.growl('Saved Successfully.', "success");
                            $scope.GetBudgetDetailsbyProject();
                        };
                    });
            };

            $scope.goToSaveImportQCS = function (reqID, qcsID) {
                var url = $state.href("import-qcs", { "reqID": reqID, "qcsID": qcsID });
                window.open(url, '_blank');
            };

            $scope.validatePackageSelection = function (budgetObj) {
                let isDisabled = true;
                if (budgetObj && budgetObj.NODES && budgetObj.NODES.length > 0) {
                    let reqAssignedItems = budgetObj.NODES.filter(function (item1) {
                        return item1.QCS_ID
                    });

                    if (reqAssignedItems && reqAssignedItems.length > 0) {
                        isDisabled = false;
                    }
                } else {
                    isDisabled = true;
                }

                return isDisabled;
            };

            $scope.handlePackageSelect = function (budgetObj) {
                if (budgetObj.packageSelected) {
                    let subPackageIds = [];
                    if ($scope.selectedPOSubPackages && $scope.selectedPOSubPackages.length > 0) {
                        $scope.selectedPOSubPackages.forEach(function (item, index1) {
                            if (!subPackageIds.includes(item.QCS_ID)) {
                                subPackageIds.push(item.QCS_ID);
                            }
                        });
                    }

                    budgetObj.NODES.forEach(function (item, index1) {
                        if (item.QCS_ID) {
                            item.subPackageSelected = budgetObj.packageSelected;
                            if (item.subPackageSelected) {
                                if (!subPackageIds.includes(item.QCS_ID)) {
                                    subPackageIds.push(item.QCS_ID);
                                }
                            }
                        }
                    });

                    if (subPackageIds.length > 1) {
                        swal({
                            title: "Error!",
                            text: "Selected Package contains Sub-Packages related different QCS.",
                            type: "error",
                            showCancelButton: false,
                            confirmButtonColor: "#DD6B55",
                            confirmButtonText: "Ok",
                            closeOnConfirm: true
                        },
                            function () {

                            });

                        budgetObj.packageSelected = false;
                        budgetObj.NODES.forEach(function (item, index1) {
                            if (item.QCS_ID) {
                                item.subPackageSelected = budgetObj.packageSelected;
                            }
                        });
                    } else {
                        budgetObj.NODES.forEach(function (item, index1) {
                            if (item.QCS_ID) {
                                item.subPackageSelected = budgetObj.packageSelected;
                                if (item.subPackageSelected) {
                                    let temp = $scope.selectedPOSubPackages.filter(function (item1) {
                                        return item1.SUB_PACKAGE_ID === item.SUB_PACKAGE_ID
                                    });

                                    if (!temp || temp.length <= 0) {
                                        $scope.selectedPOSubPackages.push(item);
                                    }
                                }
                            }
                        });
                    }
                } else {
                    budgetObj.NODES.forEach(function (item, index1) {
                        if (item.QCS_ID) {
                            item.subPackageSelected = budgetObj.packageSelected;
                            if (!item.subPackageSelected) {
                                $scope.selectedPOSubPackages = $scope.selectedPOSubPackages.filter(function (item1) {
                                    return item1.SUB_PACKAGE_ID !== item.SUB_PACKAGE_ID
                                });
                            }
                        }
                    });
                }

            };

            $scope.submitForPackageApproval = [];

            $scope.handleSubPackageSelect = function (subpackage) {

                if (!$scope.IS_PACKAGE_APPROVED) {
                    let currentQCSId = subpackage.QCS_ID;
                    $scope.BudgetArr.forEach(function (budg, index1) {
                        if (budg.NODES && budg.NODES.length > 0) {
                            budg.NODES.forEach(function (node, index1) {
                                if (node.QCS_ID && node.QCS_ID === currentQCSId) {
                                    node.subPackageSelected = subpackage.subPackageSelected;
                                    var obj = {
                                        "PACKAGE_ID": budg.PACKAGE_ID,
                                        "SUB_PACKAGE_ID": node.PACKAGE_ID,
                                        "QCS_ID": node.QCS_ID,
                                        "U_ID": +$scope.userId
                                    };
                                    $scope.submitForPackageApproval.push(obj);
                                }
                            });
                        }
                    });
                }

                if ($scope.IS_PACKAGE_APPROVED) {
                    if (subpackage.subPackageSelectedForPO) {
                        let subPackageIds = [];
                        if ($scope.selectedPOSubPackages && $scope.selectedPOSubPackages.length > 0) {
                            $scope.selectedPOSubPackages.forEach(function (item, index1) {
                                if (!subPackageIds.includes(item.QCS_ID)) {
                                    subPackageIds.push(item.QCS_ID);
                                }
                            });
                        }

                        if (subpackage.QCS_ID) {
                            if (!subPackageIds.includes(subpackage.QCS_ID)) {
                                subPackageIds.push(subpackage.QCS_ID);
                            }
                        }

                        if (subPackageIds.length > 1) {
                            swal({
                                title: "Error!",
                                text: "Selected Package contains Sub-Packages related different QCS.",
                                type: "error",
                                showCancelButton: false,
                                confirmButtonColor: "#DD6B55",
                                confirmButtonText: "Ok",
                                closeOnConfirm: true
                            },
                                function () {

                                });

                            subpackage.subPackageSelectedForPO = false;
                        } else {
                            let temp = $scope.selectedPOSubPackages.filter(function (item1) {
                                return item1.SUB_PACKAGE_ID === subpackage.SUB_PACKAGE_ID
                            });

                            if (!temp || temp.length <= 0) {
                                $scope.selectedPOSubPackages.push(subpackage);
                            }
                        }

                    } else {
                        $scope.selectedPOSubPackages = $scope.selectedPOSubPackages.filter(function (item1) {
                            return item1.SUB_PACKAGE_ID !== subpackage.SUB_PACKAGE_ID
                        });
                    }
                }
            };

            $scope.generatePOStart = function () {
                if ($scope.selectedPOSubPackages && $scope.selectedPOSubPackages.length > 0) {
                    let params = {
                        "qcsid": $scope.selectedPOSubPackages[0].QCS_ID,
                        "sessionid": $scope.sessionId
                    };

                    reportingService.GetQCSDetails(params)
                        .then(function (response) {
                            let QCSDetails = response;

                            if (QCSDetails.REQ_JSON) {
                                $scope.qcsRequirementDetails = JSON.parse(QCSDetails.REQ_JSON);
                            }

                            getRequirementData($scope.selectedPOSubPackages[0].REQ_ID, $scope.selectedPOSubPackages[0].QCS_ID);
                        });

                } else {
                    swalMessage('Error!', 'Please select sub package to proceed.', 'error');
                }
            };

            function getRequirementData(reqId, qcsId) {
                auctionsService.getReportrequirementdata({ "reqid": reqId, "sessionid": $scope.sessionId, 'userid': $scope.userId, 'excludePriceCap': 1 })
                    .then(function (response) {
                        $scope.qcsItems = [];
                        $scope.qcsCoreItems = [];
                        response.listRequirementItems.forEach(function (reqItem, index) {
                            if (qcsId > 0 && $scope.qcsRequirementDetails) {
                                var tempItem = _.filter($scope.qcsRequirementDetails.listRequirementItems, function (qcsReqItem) { return qcsReqItem.itemID === reqItem.itemID; });
                                if (tempItem && tempItem.length > 0) {
                                    reqItem.LPPValue = tempItem[0].LPPValue;
                                    reqItem.DATE_CREATED = tempItem[0].DATE_CREATED ? tempItem[0].DATE_CREATED : '';
                                    reqItem.RFQ_UNITS = tempItem[0].RFQ_UNITS;
                                    reqItem.LPP_COMENTS = tempItem[0].LPP_COMENTS;
                                    reqItem.QUANTITY = tempItem[0].QUANTITY;
                                }
                            }
                        });

                        response.listRequirementItems.forEach(function (reqItem, index) {
                            reqItem.isVisible = true;
                            var qcsItem = {
                                productId: reqItem.catalogueItemID,
                                itemID: reqItem.itemID,
                                itemName: reqItem.productIDorName,
                                productQuantity: reqItem.productQuantity,
                                isCoreProductCategory: reqItem.isCoreProductCategory,
                                prQuantity: reqItem.ITEM_PR_QUANTITY,
                                prNumber: reqItem.ITEM_PR_NUMBER,
                                isSelected: true
                            };

                            $scope.qcsItems.push(qcsItem);
                        });

                        $scope.qcsCoreItems = $scope.qcsItems.filter(function (qcsitem) {
                            return qcsitem.isCoreProductCategory > 0;
                        });

                        if (response) {
                            $scope.requirementDetails = response;
                            $scope.requirementDetails.listRequirementItems.sort(function (a, b) {
                                return b.isCoreProductCategory - a.isCoreProductCategory;
                            });

                            $scope.requirementDetails.auctionVendors = _.filter($scope.requirementDetails.auctionVendors, function (vendor) { return vendor.companyName !== 'PRICE_CAP'; });
                            if (qcsId > 0 && $scope.qcsRequirementDetails) {
                                $scope.requirementDetails.customerComment = $scope.qcsRequirementDetails.customerComment;
                                $scope.requirementDetails.includeGstInCal = $scope.qcsRequirementDetails.includeGstInCal;
                                $scope.requirementDetails.listRequirementItems.forEach(function (item, index) {
                                    item.maxHeight = '';
                                    var tempItem = _.filter($scope.qcsRequirementDetails.listRequirementItems, function (qcsReqItem) { return qcsReqItem.itemID === item.itemID; });
                                    if (tempItem && tempItem.length > 0) {
                                        item.qtyDistributed = tempItem[0].qtyDistributed;
                                        item.budget = tempItem[0].budget;
                                    }
                                });

                                $scope.requirementDetails.auctionVendors.forEach(function (vendor, index) {
                                    vendor.isVisible = true;
                                    vendor.currencyRate = 1;
                                    var qcsVendor = {
                                        vendorID: vendor.vendorID,
                                        vendorCompany: vendor.companyName,
                                        vendorCurrency: vendor.selectedVendorCurrency,
                                        isSelected: true
                                    };

                                    vendor.listRequirementItems.forEach(function (vendorItem, index) {
                                        if (vendorItem.productQuotationTemplateJson) {
                                            vendorItem.productQuotationTemplateJson = JSON.parse(vendorItem.productQuotationTemplateJson);
                                        }
                                    });

                                    var tempQCSVendor = _.filter($scope.qcsRequirementDetails.auctionVendors, function (qcsVendor) { return qcsVendor.vendorID === vendor.vendorID; });
                                    if (tempQCSVendor && tempQCSVendor.length > 0) {
                                        vendor.currencyRate = tempQCSVendor[0].currencyRate;
                                        //vendor.currencyRate = tempQCSVendor[0].vendorCurrencyFactor;
                                        vendor.customerComment = tempQCSVendor[0].customerComment;
                                        vendor.revChargeAny = tempQCSVendor[0].revChargeAny;
                                        vendor.seaFreight = tempQCSVendor[0].seaFreight;
                                        vendor.insurance = tempQCSVendor[0].insurance;
                                        vendor.basicCustomDuty = tempQCSVendor[0].basicCustomDuty;
                                        vendor.antiDumping = tempQCSVendor[0].antiDumping;
                                        vendor.socialWelfareCharge = tempQCSVendor[0].socialWelfareCharge;
                                        vendor.clearingCharges = tempQCSVendor[0].clearingCharges;
                                        vendor.paymentLoadingFactor = tempQCSVendor[0].paymentLoadingFactor;
                                        vendor.paymentLoadingFactorPercent = tempQCSVendor[0].paymentLoadingFactorPercent;
                                        vendor.socialWelfareCharge = tempQCSVendor[0].socialWelfareCharge;
                                        vendor.localTransportCharges = tempQCSVendor[0].localTransportCharges;
                                        vendor.iGst = tempQCSVendor[0].iGst;

                                        vendor.listRequirementItems.forEach(function (vendorItem, index) {
                                            var tempQCSVendorItem = _.filter(tempQCSVendor[0].listRequirementItems, function (qcsVendorItem) { return qcsVendorItem.itemID === vendorItem.itemID; });
                                            if (tempQCSVendorItem && tempQCSVendorItem.length > 0) {
                                                vendorItem.vendorID = vendor.vendorID;
                                                vendorItem.unitPrice = tempQCSVendorItem[0].unitPrice;
                                                vendorItem.revUnitPrice = tempQCSVendorItem[0].revUnitPrice;
                                                vendorItem.qtyDistributed = tempQCSVendorItem[0].qtyDistributed;
                                            }
                                        });
                                    }
                                });
                            }
                        }

                        generatePO(reqId, qcsId);
                    });
            };

            $scope.handleWidgetsState = function () {
                $scope.isAllWidgetsCollapsed = !$scope.isAllWidgetsCollapsed;
                $scope.BudgetArr.forEach(function (item, index1) {
                    item.expanded = !$scope.isAllWidgetsCollapsed;
                    item.NODES.forEach(function (node, index1) {
                        if (node.PO_NUMBER_TEMP) {
                            node.PO_NUMBER_TEMP1 = node.PO_NUMBER_TEMP;
                        }
                    });
                });
            };

            $scope.showOnlyQCSCreated = true;

            $scope.checkValidforPO = function (budgetObj) {
                let isValid = true;
                if (budgetObj && budgetObj.NODES && budgetObj.NODES.length > 0) {
                    let filteredItemsWithReq = budgetObj.NODES.filter(function (item, index) {
                        if (item.REQ_ID > 0) {
                            item.subPackageSelected = true;
                            if (item.PO_NUMBER) {
                                item.PO_NUMBER_TEMP = _.uniqBy(item.PO_NUMBER.split('@$'));
                            }
                        }
                        return item;
                    });

                    if (!filteredItemsWithReq || filteredItemsWithReq.length <= 0) {
                        isValid = false;
                    }
                } else {
                    isValid = false;
                }

                if (isValid) {
                    if (budgetObj && budgetObj.NODES && budgetObj.NODES.length > 0) {
                        budgetObj.ANTICIPATED_PO_VALUE = 0;
                        budgetObj.NODES.forEach(function (nodeItem, nodeItemIndex) {
                            if (nodeItem.QCS_PO_VALUE <= 0) {
                                budgetObj.ANTICIPATED_PO_VALUE += nodeItem.EST_BUDGET_AMOUNT;
                            } else if (nodeItem.QCS_PO_VALUE > 0) {
                                budgetObj.ANTICIPATED_PO_VALUE += nodeItem.QCS_PO_VALUE;
                            }

                            if (nodeItem.PACKAGE_APPROVAL_ID > 0) {
                                nodeItem.subPackageSelected = true;
                            }
                        });
                        budgetObj.VARIANCE = (budgetObj.EST_BUDGET_AMOUNT - budgetObj.ANTICIPATED_PO_VALUE);
                    }


                    let filteredItemsWithReq = budgetObj.NODES.filter(function (item, index) {
                        if ($scope.showOnlyQCSCreated) {
                            return item.subPackageSelected
                        } else {
                            return item;
                        }
                    });

                    if (!filteredItemsWithReq || filteredItemsWithReq.length <= 0) {
                        isValid = false;
                    }

                }

                return isValid;
            };


            $scope.showAllPackages = function () {
                $scope.showOnlyQCSCreated = !$scope.showOnlyQCSCreated;
            };

            $scope.displayAnticipatedBudgetAmount = function ()
            {
                if ($scope.BudgetArr && $scope.BudgetArr.length > 0)
                {
                    $scope.PROJECT_DET.ANTICIPATED_BUDGET =   _.sumBy($scope.BudgetArr, 'ANTICIPATED_PO_VALUE');
                }
            };

            function generatePO(reqId, qcsId) {
                let calculatedVendorSubPackages = [];
                let calculatedVendorSubPackagesByItem = [];
                let calculatedVendorSubPackagesByItemTemp = [];
                $scope.selectedPOSubPackages.forEach(function (subPackage, index1) {
                    if (subPackage.PO_VENDOR_IDS) {
                        let vendorIds = subPackage.PO_VENDOR_IDS.split(",");
                        vendorIds.forEach(function (id, index1) {
                            let tempSubPackage = JSON.stringify(subPackage);
                            let tempSubPackageObj = JSON.parse(tempSubPackage);
                            tempSubPackageObj.PO_VENDOR_IDS = id;
                            calculatedVendorSubPackages.push(tempSubPackageObj);
                        });
                    }
                });

                console.log(calculatedVendorSubPackages);
                //$scope.nonCoreItems = [];

                if (calculatedVendorSubPackages && calculatedVendorSubPackages.length > 0) {
                    calculatedVendorSubPackages.forEach(function (subPackage, index1) {
                        let currentVendor = $scope.requirementDetails.auctionVendors.filter(function (vendor, index) {
                            return vendor.vendorID === +subPackage.PO_VENDOR_IDS;
                        });

                        if (currentVendor && currentVendor.length > 0) {
                            var currentVendorItems = _.filter(currentVendor[0].listRequirementItems, function (vendorItem) { return vendorItem.productNo === subPackage.PACKAGE_NAME && vendorItem.productIDorName === subPackage.SUB_PACKAGE_NAME; });
                            //$scope.nonCoreItems = _.filter(currentVendor[0].listRequirementItems, function (vendorItem) { return vendorItem.isCoreProductCategory <= 0;  });
                            if (currentVendorItems && currentVendorItems.length > 0) {
                                currentVendorItems.forEach(function (item, index1) {
                                    let tempObj = {};

                                    var taxAmount = 0;
                                    taxAmount = (item.revUnitPrice * (item.qtyDistributed ? item.qtyDistributed : item.productQuantity) * ((item.cGst + item.iGst + item.sGst) / 100));
                                    
                                    var price = 0;
                                    price = (item.revUnitPrice * (item.qtyDistributed ? item.qtyDistributed : item.productQuantity));
                                    
                                    tempObj.CUSTOMER_COMP_ID = +$scope.CompId;
                                    tempObj.CUSTOMER_USER_ID = +$scope.userId;
                                    tempObj.PROJECT_ID = +$scope.PROJECT_ID;
                                    tempObj.PACKAGE_NAME = subPackage.PACKAGE_NAME;
                                    tempObj.SUB_PACKAGE_NAME = subPackage.SUB_PACKAGE_NAME;
                                    tempObj.PACKAGE_ID = subPackage.PACKAGE_ID;
                                    tempObj.SUB_PACKAGE_ID = subPackage.SUB_PACKAGE_ID;
                                    tempObj.DESCRIPTION = item.productCode;
                                    tempObj.VENDOR_ID = currentVendor[0].vendorID;
                                    tempObj.VENDOR_CODE = '';
                                    tempObj.ORDER_QTY = item.qtyDistributed;
                                    tempObj.UOM = item.productQuantityIn;
                                    tempObj.UNIT_PRICE = item.revUnitPrice;
                                    tempObj.CGST = item.cGst;
                                    tempObj.SGST = item.sGst;
                                    tempObj.IGST = item.iGst;
                                    //tempObj.TAX_AMOUNT = taxAmount;
                                    //tempObj.OVERALL_AMOUNT = price;
                                    tempObj.PO_DESCRIPTION = subPackage.PO_DESCRIPTION ? subPackage.PO_DESCRIPTION : '';
                                    tempObj.PAYMENT_TERMS = currentVendor[0].payment;

                                    calculatedVendorSubPackagesByItem.push(tempObj);
                                });

                                //calculatedVendorSubPackagesByItemTemp.forEach(function (subPackageItemTemp, subPackageItemTempIndex) {
                                //    var foundItem = _.findIndex(calculatedVendorSubPackagesByItem, function (subPackageItem) {
                                //        return subPackageItem.PACKAGE_ID === subPackageItemTemp.PACKAGE_ID && subPackageItem.SUB_PACKAGE_ID === subPackageItemTemp.SUB_PACKAGE_ID && subPackageItem.VENDOR_ID === subPackageItemTemp.VENDOR_ID;
                                //    });
                                //    if (foundItem >= 0) {
                                //        var groupItems = _.filter(calculatedVendorSubPackagesByItemTemp, function (item) { return item.PACKAGE_ID === subPackageItemTemp.PACKAGE_ID && item.SUB_PACKAGE_ID === subPackageItemTemp.SUB_PACKAGE_ID && item.VENDOR_ID === subPackageItemTemp.VENDOR_ID; });
                                //        calculatedVendorSubPackagesByItem[foundItem].TAX_AMOUNT = _.sumBy(groupItems, 'TAX_AMOUNT');
                                //        calculatedVendorSubPackagesByItem[foundItem].TOTAL_AMOUNT = _.sumBy(groupItems, 'OVERALL_AMOUNT');
                                //    } else {
                                //        var newObj = Object.assign({}, subPackageItemTemp);
                                //        calculatedVendorSubPackagesByItem.push(newObj);
                                //    }
                                //});
                            }
                        }
                    });
                }


                if (calculatedVendorSubPackagesByItem && calculatedVendorSubPackagesByItem.length > 0) {
                    // POST HERE
                    let poParams = {
                        'details': calculatedVendorSubPackagesByItem,
                        'sessionId': $scope.sessionId
                    };
                    PRMPOService.savePODetails(poParams)
                        .then(function (response) {
                            if (response.errorMessage) {
                                swalMessage("Error!", "Error occured, please contact support team.", "error");//growlService.growl(response.errorMessage, "inverse");
                            } else {
                                swal({
                                    title: 'Success!',
                                    text: 'Successfully saved.',
                                    type: 'success',
                                    showCancelButton: false,
                                    confirmButtonColor: "#DD6B55",
                                    confirmButtonText: "Ok",
                                    closeOnConfirm: true
                                },
                                    function () {
                                        $state.go('list-po-new');
                                    });
                                //swalMessage("Success!", "Successfully saved.", "success");
                            };
                        });
                }
            }

            function swalMessage(title, text, type) {
                swal({
                    title: title,
                    text: text,
                    type: type,
                    showCancelButton: false,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Ok",
                    closeOnConfirm: true
                },
                    function () {

                    });
            }


            $scope.filterBasedOnStatus = function ()
            {
                if ($scope.workflowStatus === 'All') {
                    $scope.BudgetArr = $scope.BudgetArrTemp;
                } else {
                    $scope.BudgetArr = _.filter($scope.BudgetArrTemp, function (x) { return x.PACKAGE_APPROVAL_STATUS === $scope.workflowStatus; });
                }
            };


            $scope.displayPONumber = function (po) {
                if (po) {
                    return po.split('$@')[0];
                } else {
                    return '-';
                }

            };

            $scope.displayPOVendorCompName = function (po) {
                if (po) {
                    return po.split('$@')[1];
                } else {
                    return '-';
                }

            };
        }]);
