
prmApp.controller('projectDetailsCtrl', ["$scope", "$stateParams", "$log", "$state", "$window", "userService", "auctionsService", "storeService", "growlService","PRMProjectServices", "$filter", "fileReader", "$rootScope",
    function ($scope, $stateParams, $log, $state, $window, userService, auctionsService, storeService, growlService, PRMProjectServices, $filter, fileReader, $rootScope) {
        $scope.USER_ID = userService.getUserId();
        $scope.COMP_ID = userService.getUserCompanyId();
        $scope.sessionID = userService.getUserToken();
        $scope.customerType = userService.getUserType();
        $scope.isCustomer = $scope.customerType === "CUSTOMER" ? true : false;
        // $scope.deptID = userService.getSelectedUserDepartmentDesignation().deptID;
        $scope.isSuperUser = userService.getUserObj().isSuperUser;
        $scope.BC_ID = $stateParams.Id;
        $scope.pdfTableShow = false;
        $scope.scStatus = 'ALL';

        /*PAGINATION CODE*/
        $scope.totalItems = 0;
        $scope.pageSize = 10;
        $scope.recordsFetchFrom = 0;
        $scope.totalItems1 = 0;
        $scope.currentPage = 1;
        $scope.currentPage1 = 1;
        $scope.itemsPerPage = 10;
        $scope.itemsPerPage1 = 10;
        $scope.maxSize = 5;
        $scope.maxSize1 = 5;
        $scope.userObj = {};
        userService.getUserDataNoCache()
            .then(function (response) {
                $scope.userObj = response;
            });

        $scope.projectlist = [];
        $scope.projectlist1 = [];
        $scope.projectIdsArr = [];
        $scope.projectCostcentersArr = [];
        $scope.projectLocationsArr = [];
        $scope.projectlistPage = [];
        $scope.projectlist = [];

        $scope.searchById = '';
        $scope.searchByLocation = '';
        $scope.searchByCostcenter = '';
        $scope.filters =
        {
            project: {},
            costCentre: {},
            location: {}
        };

        $scope.filtersList = {
            project: [],
            costCentre: [],
            location: []
        };

        $scope.projectlistTemp = [];
        $scope.projectlistFilteredTemp = [];
        $scope.projectlist = [];
        $scope.projectlist1 = [];
        $scope.projectIdsArr = [];
        $scope.projectCostcentersArr = [];
        $scope.projectLocationsArr = [];

        if (!$scope.isCustomer) {
            $state.go("home");
        }

        $scope.deptIDs = [];
        $scope.desigIDs = [];
        $scope.deptIDStr = '';
        $scope.desigIDStr = '';

        $scope.ListUserDepartmentDesignations = userService.getListUserDepartmentDesignations();
        if ($scope.ListUserDepartmentDesignations && $scope.ListUserDepartmentDesignations.length > 0) {
            $scope.ListUserDepartmentDesignations.forEach(function (item, index) {
                $scope.deptIDs.push(item.deptID);
                item.listDesignation.forEach(function (item1, index1) {
                    if (item1.isAssignedToUser && item1.isValid) {
                        $scope.desigIDs.push(item1.desigID);
                        $scope.desigIDs = _.union($scope.desigIDs, [item1.desigID]);
                    }
                });
            });
            $scope.deptIDStr = $scope.deptIDs.join(',');
            $scope.desigIDStr = $scope.desigIDs.join(',');
        }


        $scope.setPage = function (pageNo) {
            $scope.currentPage = pageNo;
            $scope.GetProjectList(($scope.currentPage - 1), 10);
        };

        $scope.setFilters = function (currentPage) {

            if ($scope.filters.searchKeyword || !_.isEmpty($scope.filters.project) || !_.isEmpty($scope.filters.costCentre) || !_.isEmpty($scope.filters.location)) {
                $scope.GetProjectList(0, 10);
            } else {
                if ($scope.initialProjectPageArray && $scope.initialProjectPageArray.length > 0) {
                    $scope.projectlist = $scope.initialProjectPageArray;
                    if ($scope.projectlist && $scope.projectlist.length > 0) {
                        $scope.totalItems = $scope.projectlist[0].TOTAL_ROWS;
                        $scope.filteredprojectList = $scope.projectlist;
                    }
                }
            }

        };

        $scope.initialProjectPageArray = [];
        $scope.loadFields = true;
        $scope.GetProjectList = function (recordsFetchFrom, pageSize) {

            var projectIds, costCentres, location;

            if (_.isEmpty($scope.filters.project)) {
                projectIds = '';
            } else if ($scope.filters.project && $scope.filters.project.length > 0) {
                var projects = _($scope.filters.project)
                    .filter(item => item.id)
                    .map('id')
                    .value();
                projectIds = projects.join(',');
            }


            if (_.isEmpty($scope.filters.costCentre)) {
                costCentres = '';
            } else if ($scope.filters.costCentre && $scope.filters.costCentre.length > 0) {
                var ccs = _($scope.filters.costCentre)
                    .filter(item => item.id)
                    .map('id')
                    .value();
                costCentres = ccs.join(',');
            }

            if (_.isEmpty($scope.filters.location)) {
                location = '';
            } else if ($scope.filters.location && $scope.filters.location.length > 0) {
                var loc = _($scope.filters.location)
                    .filter(item => item.id)
                    .map('id')
                    .value();
                location = loc.join(',');
            }

            var params = {
                "COMP_ID": $scope.COMP_ID,
                "USER_ID": $scope.USER_ID,
                "DEPT_ID": $scope.deptIDStr,
                "DESIG_ID": $scope.desigIDStr,
                "pendingApproval": 0,
                "PROJECT_ID": projectIds,
                "COST_CENTER": costCentres,
                "LOCATION": location,
                "SEARCH": $scope.filters.searchKeyword ? $scope.filters.searchKeyword : '',
                "PageSize": recordsFetchFrom * pageSize,
                "NumberOfRecords": pageSize,
                "sessionid": userService.getUserToken()
            };
            $scope.pageSizeTemp = (params.PageSize + 1);
            $scope.NumberOfRecords = ((recordsFetchFrom + 1) * pageSize);

            PRMProjectServices.GetProjectList(params)
                .then(function (response) {
                    $scope.projectlist = [];
                    $scope.filteredprojectList = [];
                    $scope.totalItems = 0;
                    response.forEach(function (item, index) {
                        $scope.projectlist.push(item);
                        if ($scope.initialProjectPageArray.length <= 9) { // Push Initial 10 Records When Page is Loaded because needed in SetFilters function it's getting called every time (need to modify directive code)  #Crap Code need to remove(should think of another solution)
                            $scope.initialProjectPageArray.push(item);
                        }
                    });

                    if ($scope.projectlist && $scope.projectlist.length > 0)
                    {
                        $scope.totalItems = $scope.projectlist[0].TOTAL_ROWS;
                        $scope.filteredprojectList = $scope.projectlist;
                        hideFieldsForProcurementUsers();
                    }

                });
        };


        function hideFieldsForProcurementUsers()
        {
            let isValid = false;
            isValid = (!$scope.isSuperUser && $scope.ListUserDepartmentDesignations.some(x => x.deptCode.toLowerCase() == 'procurement'));
            if (isValid)
            {
                $scope.loadFields = false;
            } 
        }

        $scope.GetProjectList(0, 10);

        $scope.GetProjectListByFilter = function () {
            let projectsIDsTemp = [];
            let costCentresTemp = [];
            let locationsTemp = [];
            var params = {
                "COMP_ID": $scope.COMP_ID,
                "USER_ID": $scope.USER_ID,
                "TYPE": 'PROJECT',
                "sessionid": userService.getUserToken()
            };

            PRMProjectServices.GetProjectListByFilter(params)
                .then(function (response) {
                    $scope.projectlistFilteredTemp = response;

                    $scope.projectlistFilteredTemp.forEach(function (item, index) {
                        if (item.TYPE === 'PROJID') {
                            projectsIDsTemp.push({ name: item.VALUE, id: item.ID });
                        } else if (item.TYPE === 'COST_CENTER') {
                            costCentresTemp.push({ id: item.VALUE });
                        } else if (item.TYPE === 'LOCATION') {
                            locationsTemp.push({ id: item.VALUE });
                        }
                    });

                    $scope.filtersList.project = projectsIDsTemp;
                    $scope.filtersList.costCentre = costCentresTemp;
                    $scope.filtersList.location = locationsTemp;

                });
        };

        $scope.GetProjectListByFilter();

        $scope.PROJECT_AUDIT = [];


        $scope.getProjectAudit = function (obj) {
            //$scope.params =
            //{
            //    "COMP_ID": $scope.COMP_ID,
            //    "PROJECT_ID": obj.PROJECT_ID,
            //    "sessionid": userService.getUserToken()
            //};

            //PRMProjectServices.GetProjectAudit($scope.params)
            //    .then(function (response) {
            //        if (response) {

            //            $scope.PROJECT_AUDIT = JSON.parse(response).Table;
            //            angular.element('#audit').modal('show');
            //            console.log($scope.PROJECT_AUDIT);

                        
            //        }
            //    });

            $state.go('projectAudit', { "PROJECT_ID": obj.PROJECT_ID});
        }



        $scope.selectIds = function (obj, TYPE) {
            $scope.selectedIds = obj;
            if (TYPE === "PROJID") {
                if ($scope.selectedIds && $scope.selectedIds.length > 0) {
                    let projectIds = _($scope.selectedIds)
                        .filter(item => item.id)
                        .map('name')
                        .value();
                    PROJECT_IDS = projectIds.join(',');
                }
                else {
                    PROJECT_IDS = '';
                }
            } else if (TYPE === "COST_CENTER") {
                $scope.selectedCostCenters = obj;
                if ($scope.selectedCostCenters && $scope.selectedCostCenters.length > 0) {
                    let projectCostcenters = _($scope.selectedCostCenters)
                        .filter(item => item.id)
                        .map('id')
                        .value();
                    PROJECT_COST_CENTER = projectCostcenters.join(',');
                }
                else {
                    PROJECT_COST_CENTER = '';
                }
                console.log(PROJECT_COST_CENTER);
            } else if (TYPE === "LOCATION") {
                $scope.selectedLocations = obj;
                if ($scope.selectedLocations && $scope.selectedLocations.length > 0) {
                    let projectLocations = _($scope.selectedLocations)
                        .filter(item => item.id)
                        .map('id')
                        .value();
                    PROJECT_LOCATION = projectLocations.join(',');
                }
                else {
                    PROJECT_LOCATION = '';
                }
            }
        };


        $scope.index = [];
        $scope.isConfirm = false;
        $scope.goToCreateProject = function (Id) {
            if (Id == 0) {
                var url = $state.href("createProject", { "Id": Id });
                window.open(url, '_self');
            }
        };

        $scope.deleteProject = function (obj) {
            if (obj.STATUS && obj.STATUS === 'IN-PROGRESS') {
                swal({
                    title: 'Warning!',
                    text: 'Package award recommendation is in-progress.',
                    type: 'warning',
                    showCancelButton: false,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Ok",
                    closeOnConfirm: true
                },
                    function () {
                    });
            } else {
                var params = {
                    PROJECT_ID: obj.PROJECT_ID,
                    sessionID: userService.getUserToken(),
                };
                PRMProjectServices.DeleteProject(params)
                    .then(function (response) {
                        if (response.errorMessage) {
                            growlService.growl(response.errorMessage, "inverse");
                        }
                        else {
                            growlService.growl("Project Deleted Successfully.", "success");
                            $state.reload();
                            $scope.GetProjectList();
                            //$window.history.back();
                        }
                    });
            }
        };
     
        $scope.goToProjectView = function (obj) {
            $state.go("saveProjectDetails", { "Id": obj.PROJECT_ID });
        };

        $scope.goToProjectEdit = function (obj) {
            //if (obj.STATUS && obj.STATUS === 'IN-PROGRESS') {
            //    swal({
            //        title: 'Warning!',
            //        text: 'Package award recommendation is in-progress.',
            //        type: 'warning',
            //        showCancelButton: false,
            //        confirmButtonColor: "#DD6B55",
            //        confirmButtonText: "Ok",
            //        closeOnConfirm: true
            //    },
            //        function () {                        
            //        });
            //} else {
            //    $state.go("createProject", { "Id": obj.PROJECT_ID });
            //}
            $state.go("createProject", { "Id": obj.PROJECT_ID });
        };

        $scope.goToBudgetSetUp = function (project) {
            projid = project.PROJECT_ID;
            budgid = project.BUDGET_ID;
            $state.go("setupBudget", { "projectId": +projid, "budgetId": budgid })
        };

        $scope.setUpProjectAmendment = function (project)
        {
            $state.go("projectAmendment", { "projectId": +project.PROJECT_ID, "budgetId": +project.BUDGET_ID, "amendmentId": +project.PROJECT_AMENDMENT_ID })
        };

    }]);