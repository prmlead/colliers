prmApp.constant('PRMProjectServicesDomain', 'project/svc/PRMProjectServices.svc/REST/');
prmApp.service('PRMProjectServices', ["PRMProjectServicesDomain", "userService", "httpServices",
    function (PRMProjectServicesDomain, userService, httpServices) {

        var PRMProjectServices = this;

        PRMProjectServices.SaveProjectDetails = function (params) {
            let url = PRMProjectServicesDomain + 'saveProjectDetails';
             return httpServices.post(url, params);
        };

        PRMProjectServices.GetProjectList = function (params) {
            let url = PRMProjectServicesDomain + 'getProjectList?COMP_ID=' + params.COMP_ID + '&USER_ID=' + params.USER_ID + '&DEPT_ID=' + params.DEPT_ID + '&DESIG_ID=' + params.DESIG_ID + '&pendingApproval=' + params.pendingApproval + '&PROJECT_ID=' + params.PROJECT_ID
                + '&COST_CENTER=' + params.COST_CENTER + '&LOCATION=' + params.LOCATION + '&SEARCH=' + params.SEARCH + '&PageSize=' + params.PageSize + '&NumberOfRecords=' + params.NumberOfRecords + '&sessionid=' + params.sessionid;
            return httpServices.get(url);
        };

        PRMProjectServices.GetProjectListByFilter = function (params) {
            let url = PRMProjectServicesDomain + 'GetProjectListByFilter?COMP_ID=' + params.COMP_ID + '&USER_ID=' + params.USER_ID + '&TYPE=' + params.TYPE +'&sessionid=' + params.sessionid;
            return httpServices.get(url);
        };

        PRMProjectServices.GetProjectDetails = function (params) {
            let url = PRMProjectServicesDomain + 'getProjectDetails?PROJECT_ID=' + params.PROJECT_ID + '&sessionid=' + params.sessionid;
            return httpServices.get(url);
        };

        PRMProjectServices.GetProjectAmendmentDetails = function (params) {
            let url = PRMProjectServicesDomain + 'getProjectAmendmentDetails?PROJECT_ID=' + params.PROJECT_ID + '&sessionid=' + params.sessionid;
            return httpServices.get(url);
        };

        PRMProjectServices.GetBudgetDetails = function (params) {
            let url = PRMProjectServicesDomain + 'getBudgetDetails?BUDGET_ID=' + params.BUDGET_ID + '&sessionid=' + params.sessionid;
            return httpServices.get(url);
        };
        PRMProjectServices.GetBudgetDetailsbyProject = function (params) {
            if (!params.subPackageIds) {
                params.subPackageIds = '';
            }

            let url = PRMProjectServicesDomain + 'getBudgetDetailsByProject?projectId=' + params.projectId + '&budgetId=' + params.budgetId + '&onlyApproved=' + params.onlyApproved + '&subPackageIds=' + params.subPackageIds + '&sessionId=' + params.sessionId;
            return httpServices.get(url);
        };

        PRMProjectServices.GetBudgetDetailsbyProjectAmendment = function (params) {
            let url = PRMProjectServicesDomain + 'getBudgetDetailsbyProjectAmendment?projectId=' + params.projectId + '&budgetId=' + params.budgetId + '&amendmentId=' + params.amendmentId  + '&sessionId=' + params.sessionId;
            return httpServices.get(url);
        };
        
        PRMProjectServices.GetProjectAudit = function (params) {
            let url = PRMProjectServicesDomain + 'GetProjectAudit?COMP_ID=' + params.COMP_ID + '&PROJECT_ID=' + params.PROJECT_ID + '&sessionid=' + params.sessionid;
            return httpServices.get(url);
        };

        PRMProjectServices.GetBudgetAudit = function (params) {
            let url = PRMProjectServicesDomain + 'GetBudgetAudit?COMP_ID=' + params.COMP_ID + '&BUDGET_ID=' + params.BUDGET_ID + '&sessionid=' + params.sessionid;
            return httpServices.get(url);
        };
        PRMProjectServices.DeleteProject = function (params) {
            let url = PRMProjectServicesDomain + 'deleteProject';
            return httpServices.post(url, params);
        };

        PRMProjectServices.SaveBudgetDetails = function (params) {
            let url = PRMProjectServicesDomain + 'saveBudgetDetails';
            return httpServices.post(url, params);
        };

        PRMProjectServices.SaveProjectAmendmentDetails = function (params) {
            let url = PRMProjectServicesDomain + 'saveProjectAmendmentDetails';
            return httpServices.post(url, params);
        };
        
        PRMProjectServices.UpdateBudgetWorkflow = function (params) {
            let url = PRMProjectServicesDomain + 'updateBudgetWorkflow';
            return httpServices.post(url, params);
        };

        PRMProjectServices.UpdateVendorWorkflow = function (params) {
            let url = PRMProjectServicesDomain + 'updateVendorWorkflow';
            return httpServices.post(url, params);
        };

        PRMProjectServices.UpdateBudgetDetails = function (params) {
            let url = PRMProjectServicesDomain + 'updateBudgetDetails';
            return httpServices.post(url, params);
        };

        PRMProjectServices.GetPackAndSubPackNames = function (params) {
            let url = PRMProjectServicesDomain + 'GetPackAndSubPackNames?PROJECT_ID=' + params.PROJECT_ID + '&PACKAGE_ID=' + params.PACKAGE_ID + '&SUB_PACKAGE_ID=' + params.SUB_PACKAGE_ID + '&sessionid=' + params.sessionid;
            return httpServices.get(url);
        };

        PRMProjectServices.AreValidToPostRFQS = function (params) {
            let url = PRMProjectServicesDomain + 'AreValidToPostRFQS?PROJECT_ID=' + params.PROJECT_ID + '&SUB_PACKAGE_IDS=' + params.SUB_PACKAGE_IDS + '&sessionid=' + params.sessionid;
            return httpServices.get(url);
        };

        PRMProjectServices.AssignPackageApproval = function (params) {
            let url = PRMProjectServicesDomain + 'assignPackageApproval';
            return httpServices.post(url, params);
        };

        PRMProjectServices.GetPackageApprovalList = function (params) {
            let url = PRMProjectServicesDomain + 'GetPackageApprovalList?COMP_ID=' + params.COMP_ID + '&USER_ID=' + params.USER_ID
                + '&PROJECT_ID=' + params.PROJECT_ID
                + '&COST_CENTER=' + params.COST_CENTER + '&LOCATION=' + params.LOCATION + '&SEARCH=' + params.SEARCH + '&PageSize=' + params.PageSize + '&NumberOfRecords=' + params.NumberOfRecords + '&sessionid=' + params.sessionid;
            return httpServices.get(url);
        };

        PRMProjectServices.saveProjectBillToDetails = function (params) {
            let url = PRMProjectServicesDomain + 'SaveProjectBillToDetails';
            return httpServices.post(url, params);
        };

        PRMProjectServices.GetProjectBillToDetails = function (params) {//int U_ID, int COMP_ID, string sessionID
            let url = PRMProjectServicesDomain + 'GetProjectBillToDetails?U_ID=' + params.U_ID + '&COMP_ID=' + params.COMP_ID + '&searchString=' + params.searchString + '&sessionID=' + params.sessionID;
            return httpServices.get(url);
        };

        PRMProjectServices.getDashboardFilters = function (params) {
            let url = PRMProjectServicesDomain + 'getDashboardFilters?COMP_ID=' + params.COMP_ID + '&USER_ID=' + params.USER_ID + '&FROM_DATE=' + params.FROM_DATE + '&TO_DATE=' + params.TO_DATE +
                '&DEPT_ID=' + params.DEPT_ID + '&DESIG_ID=' + params.DESIG_ID +'&sessionid=' + params.sessionid;
            return httpServices.get(url);
        };

        PRMProjectServices.getProjectDashboardStats = function (params) {
            let url = PRMProjectServicesDomain + 'getProjectDashboardStats?COMP_ID=' + params.COMP_ID + '&USER_ID=' + params.USER_ID + '&costCenter=' + params.costCenter + '&location=' + params.location
                + '&clientName=' + params.clientName + '&projectCode=' + params.projectCode + '&projectId=' + params.projectId + '&fromDate=' + params.fromDate + '&toDate=' + params.toDate + '&sessionid=' + params.sessionid + '&type=' + params.type +
                '&DEPT_ID=' + params.DEPT_ID + '&DESIG_ID=' + params.DESIG_ID;
            return httpServices.get(url);
        };

        return PRMProjectServices;

}]);