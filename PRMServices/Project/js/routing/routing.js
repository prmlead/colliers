﻿prmApp
    .config(["$stateProvider", "$urlRouterProvider", "$httpProvider", "$provide", "domain", "version",
        function ($stateProvider, $urlRouterProvider, $httpProvider, $provide, domain, version) {
            $stateProvider
                .state('projectDetails', {
                    url: '/projectDetails',
                    templateUrl: 'Project/views/projectDetails.html',
                    params: {
                        detailsObj: null
                    }
                    
                })

                .state('createProject', {
                    url: '/createProject/:Id',
                    templateUrl: 'Project/views/createProject.html',
                    params: {
                        detailsObj: null
                    }

                })

                .state('saveProjectDetails', {
                    url: '/viewProjectDetails/:Id',
                    templateUrl: 'Project/views/saveProjectDetails.html',
                    params: {
                        detailsObj: null
                    }
                })

                .state('budgetApproval', {
                    url: '/budgetApproval/:Id',
                    templateUrl: 'Project/views/projectApproval.html',
                    params: {
                        detailsObj: null
                    }
                })
                .state('packageApproval', {
                    url: '/packageApproval/:projectId/:budgetId/:packageApprovalId/:qcsId',
                    templateUrl: 'Project/views/packageApproval.html',
                    params: {
                        detailsObj: null
                    }
                })

                .state('projectApproved', {
                    url: '/projectApproved/:Id',
                    templateUrl: 'Project/views/approvedProject.html',
                    params: {
                        detailsObj: null
                    }
                })
                .state('setupBudget', {
                    url: '/setupBudget/:projectId/:budgetId',
                    templateUrl: 'Project/views/setupBudget.html',
                    params: {
                        detailsObj: null
                    }
                })
                .state('projectAmendment', {
                    url: '/projectAmendment/:projectId/:budgetId/:amendmentId',
                    templateUrl: 'Project/views/projectAmendment.html',
                    params: {
                        detailsObj: null
                    }
                })
                .state('vendorApproval', {
                    url: '/vendorApproval/:Id',
                    templateUrl: 'Project/views/vendorApproval.html',
                    params: {
                        detailsObj: null
                    }
                })
                .state('approvedVendorProject', {
                    url: '/approvedVendorProject/:Id',
                    templateUrl: 'Project/views/approvedVendorProject.html',
                    params: {
                        detailsObj: null
                    }
                })
                .state('packageApprovals', {
                    url: '/packageApprovals',
                    templateUrl: 'Project/views/packageApprovalList.html',
                    params: {
                        detailsObj: null
                    }
                })
                .state('projectAudit', {
                    url: '/projectAudit/:PROJECT_ID',
                    templateUrl: 'Project/views/projectAudit.html',
                    params: {
                        detailsObj: null
                    }
                })

                .state('setupBudgetAudit', {
                    url: '/setupBudgetAudit/:PROJECT_ID/:BUDGET_ID',
                    templateUrl: 'Project/views/setupBudgetAudit.html',
                    params: {
                        detailsObj: null
                    }
                })
                
        }]);