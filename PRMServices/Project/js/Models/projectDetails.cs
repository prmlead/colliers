﻿using PRM.Core.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace PRMServices.Models
{
    [DataContract]
    public class ProjectDetails : Entity
    {
        [DataMember] [DataNames("COMP_ID")] public int COMP_ID { get; set; }
        [DataMember] [DataNames("PROJ_ID")] public string PROJ_ID { get; set; }
        [DataMember] [DataNames("PROJECT_ID")] public int PROJECT_ID { get; set; }
        [DataMember] [DataNames("REQ_ID")] public int REQ_ID { get; set; }
        [DataMember] [DataNames("STATUS")] public string STATUS { get; set; }
        [DataMember] [DataNames("U_ID")] public int U_ID { get; set; }
        [DataMember] [DataNames("BUDGET_ID")] public int BUDGET_ID { get; set; }
        [DataMember] [DataNames("PROJECT_NAME")] public string PROJECT_NAME { get; set; }
        [DataMember] [DataNames("BUDGET_WF_APPROVAL_STATUS")] public string BUDGET_WF_APPROVAL_STATUS { get; set; }
        [DataMember] [DataNames("BUDGET_WF_PENDING_APPROVERS")] public string BUDGET_WF_PENDING_APPROVERS { get; set; }
        [DataMember] [DataNames("BUDGET_WF_ID")] public int BUDGET_WF_ID { get; set; }
        [DataMember] [DataNames("COST_CENTER")] public string COST_CENTER { get; set; }
        [DataMember] [DataNames("LOCATION")] public string LOCATION { get; set; }
        [DataMember] [DataNames("PO_VALUE")] public decimal PO_VALUE { get; set; }
        [DataMember] [DataNames("PO_NUMBER")] public string PO_NUMBER { get; set; }
        [DataMember] [DataNames("SHIP_TO_ADDRESS")] public string SHIP_TO_ADDRESS { get; set; }
        [DataMember] [DataNames("BILL_TO_ADDRESS")] public string BILL_TO_ADDRESS { get; set; }
        [DataMember] [DataNames("BILL_TO_ADDRESS_JSON")] public string BILL_TO_ADDRESS_JSON { get; set; }
        [DataMember] [DataNames("TARGETED_REVENUE_BUDGET")] public decimal TARGETED_REVENUE_BUDGET { get; set; }
        [DataMember] [DataNames("IS_VALID")] public int IS_VALID { get; set; }
        [DataMember] [DataNames("NET_REVENUE_MARGIN")] public decimal NET_REVENUE_MARGIN { get; set; }
        [DataMember] [DataNames("DATE_CREATED")] public DateTime? DATE_CREATED { get; set; }
        [DataMember] [DataNames("DATE_MODIFIED")] public DateTime? DATE_MODIFIED { get; set; }
        [DataMember] [DataNames("CREATED_BY")] public int CREATED_BY { get; set; }
        [DataMember] [DataNames("MODIFIED_BY")] public int MODIFIED_BY { get; set; }
        [DataMember] [DataNames("TYPE")] public string TYPE { get; set; }
        [DataMember] [DataNames("VALUE")] public string VALUE { get; set; }
        [DataMember] [DataNames("TOTAL_ROWS")] public int TOTAL_ROWS { get; set; }
        [DataMember] [DataNames("ANTICIPATED_BUDGET_AMOUNT")] public decimal ANTICIPATED_BUDGET_AMOUNT { get; set; }
        [DataMember] [DataNames("ID")] public string ID { get; set; }
        [DataMember] [DataNames("PROJECT_AMENDMENT_ID")] public int PROJECT_AMENDMENT_ID { get; set; }

        [DataMember] [DataNames("CLIENT_NAME")] public string CLIENT_NAME { get; set; }
        [DataMember] [DataNames("ADDRESS")] public string ADDRESS { get; set; }
        [DataMember] [DataNames("SPOC_EMAIL")] public string SPOC_EMAIL { get; set; }
        [DataMember] [DataNames("GST_NUMBER")] public string GST_NUMBER { get; set; }
        [DataMember] [DataNames("PAN_NUMBER")] public string PAN_NUMBER { get; set; }
        [DataMember] [DataNames("ASSIGNED_U_ID")] public int ASSIGNED_U_ID { get; set; }
        [DataMember] public int[] ASSIGNED_U_IDS { get; set; }
        [DataMember] public int[] DEPT_IDS { get; set; }
        [DataMember] public int[] PROCUREMENT_U_IDS { get; set; }
        [DataMember] public int[] BILL_U_IDS { get; set; }
        [DataMember] [DataNames("ASSIGNED_U_IDS_STR")] public string ASSIGNED_U_IDS_STR { get; set; }
        [DataMember] [DataNames("DEPT_IDS_STR")] public string DEPT_IDS_STR { get; set; }
        [DataMember] [DataNames("PROCUREMENT_U_IDS_STR")] public string PROCUREMENT_U_IDS_STR { get; set; }
    }


}