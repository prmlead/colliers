﻿using PRM.Core.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace PRMServices.Models
{
    [DataContract]
    public class ProjectDashboard
    {
        [DataMember] [DataNames("COMP_ID")] public int COMP_ID { get; set; }
        [DataMember] [DataNames("U_ID")] public int U_ID { get; set; }
        [DataMember] [DataNames("TYPE")] public string TYPE { get; set; }
        [DataMember] [DataNames("VALUE")] public string VALUE { get; set; }
        [DataMember] [DataNames("ID")] public int ID { get; set; }
        [DataMember] [DataNames("TOTAL_BUDGET_APPROVED_PROJECTS")] public int TOTAL_BUDGET_APPROVED_PROJECTS { get; set; }
        [DataMember] [DataNames("TOTAL_RFQS_RELEASED")] public int TOTAL_RFQS_RELEASED { get; set; }
        [DataMember] [DataNames("TOTAL_RFQS")] public int TOTAL_RFQS { get; set; }
        [DataMember] [DataNames("PENDING_RFQS")] public int PENDING_RFQS { get; set; }
        [DataMember] [DataNames("PENDING_POS_CNT")] public int PENDING_POS_CNT { get; set; }
        [DataMember] [DataNames("SAVINGS")] public decimal SAVINGS { get; set; }
        [DataMember] [DataNames("SAVINGS_PERCENTAGE")] public decimal SAVINGS_PERCENTAGE { get; set; }
        [DataMember] [DataNames("PENDING_RFQS_AWARD_RECOMMEND")] public int PENDING_RFQS_AWARD_RECOMMEND { get; set; }
        [DataMember] [DataNames("SUB_PACKAGES_AWARDED")] public int SUB_PACKAGES_AWARDED { get; set; }
        [DataMember] [DataNames("TOTAL_SUB_PACKAGES_AWARDED")] public int TOTAL_SUB_PACKAGES_AWARDED { get; set; }
        [DataMember] [DataNames("RELEASED_PO_COUNT")] public int RELEASED_PO_COUNT { get; set; }
        [DataMember] [DataNames("PROJECT_LEVEL_PO_VALUE")] public decimal PROJECT_LEVEL_PO_VALUE { get; set; }
        [DataMember] [DataNames("TARGETED_REVENUE_BUDGET")] public decimal TARGETED_REVENUE_BUDGET { get; set; }
        [DataMember] [DataNames("SUB_PACKAGES_PENDING_FOR_AWARD_RECOMMENDATION")] public int SUB_PACKAGES_PENDING_FOR_AWARD_RECOMMENDATION { get; set; }
        [DataMember] [DataNames("SUB_PACKAGES_PENDING_FOR_RFQ")] public int SUB_PACKAGES_PENDING_FOR_RFQ { get; set; }
        [DataMember] [DataNames("PROJECT_ID")] public int PROJECT_ID { get; set; }
        [DataMember] [DataNames("BUDGET_ID")] public int BUDGET_ID { get; set; }
        [DataMember] [DataNames("PACKAGE_ID")] public int PACKAGE_ID { get; set; }
        [DataMember] [DataNames("SUB_PACKAGE_ID")] public int SUB_PACKAGE_ID { get; set; }
        [DataMember] [DataNames("BUDGET_WF_APPROVAL_STATUS")] public string BUDGET_WF_APPROVAL_STATUS { get; set; }
        [DataMember] [DataNames("BUDGET_WF_PENDING_APPROVERS")] public string BUDGET_WF_PENDING_APPROVERS { get; set; }
        [DataMember] [DataNames("SUB_PACKAGE_WF_APPROVAL_STATUS")] public string SUB_PACKAGE_WF_APPROVAL_STATUS { get; set; }
        [DataMember] [DataNames("REQ_ID")] public int REQ_ID { get; set; }
        [DataMember] [DataNames("PACKAGE_APPROVAL_ID")] public int PACKAGE_APPROVAL_ID { get; set; }
        [DataMember] [DataNames("PO_NUMBER")] public string PO_NUMBER { get; set; }
        [DataMember] [DataNames("TURN_AROUND_TIME")] public double TURN_AROUND_TIME { get; set; }
        [DataMember] [DataNames("PO_VALUE")] public decimal PO_VALUE { get; set; }
        [DataMember] [DataNames("PACKAGE_APPROVAL_WF_APPROVAL_STATUS")] public string PACKAGE_APPROVAL_WF_APPROVAL_STATUS { get; set; }
        [DataMember] [DataNames("PACKAGE_APPROVAL_WF_PENDING_APPROVERS")] public string PACKAGE_APPROVAL_WF_PENDING_APPROVERS { get; set; }
        [DataMember] [DataNames("SUB_PACKAGE_WF_PENDING_APPROVERS")] public string SUB_PACKAGE_WF_PENDING_APPROVERS { get; set; }
        [DataMember] [DataNames("QCS_ID")] public int QCS_ID { get; set; }
        [DataMember] [DataNames("ASSIGNED_U_IDS")] public string ASSIGNED_U_IDS { get; set; }
        [DataMember] [DataNames("OVERALL_AMOUNT")] public decimal OVERALL_AMOUNT { get; set; }
        [DataMember] [DataNames("EST_BUDGET_AMOUNT")] public decimal EST_BUDGET_AMOUNT { get; set; }
        [DataMember] [DataNames("QCS_PO_VALUE")] public decimal QCS_PO_VALUE { get; set; }
        [DataMember] [DataNames("PURCHASE_VOL_BY_PROCUREMENT")] public List<BuyerVolume> PURCHASE_VOL_BY_PROCUREMENT { get; set; }
        [DataMember] [DataNames("PACKAGES_VOLUMES")] public List<PackageVolume> PACKAGES_VOLUMES { get; set; }
        [DataMember] [DataNames("REVENUE_DETAILS")] public List<RevenueDetails> REVENUE_DETAILS { get; set; }
        [DataMember] [DataNames("REVENUE_STATS")] public List<RevenueStats> REVENUE_STATS { get; set; }
        [DataMember] [DataNames("PACKAGES_SPENT")] public List<PackageVolume> PACKAGES_SPENT { get; set; }
        [DataMember] [DataNames("PENDING_BILLS")] public int PENDING_BILLS { get; set; }
        [DataMember] [DataNames("APPROVED_BILLS")] public int APPROVED_BILLS { get; set; }
        [DataMember] [DataNames("BILL_CERTIFIED_AMOUNT")] public decimal BILL_CERTIFIED_AMOUNT { get; set; }
        [DataMember] [DataNames("BILL_PO_VALUE")] public decimal BILL_PO_VALUE { get; set; }
        [DataMember] [DataNames("REVENUE_RECOGNISED")] public decimal REVENUE_RECOGNISED { get; set; }
        [DataMember] [DataNames("PENDING_PO_OVERALL_AMOUNT")] public decimal PENDING_PO_OVERALL_AMOUNT { get; set; }
        [DataMember] [DataNames("TOTAL_CASH_FLOW")] public decimal TOTAL_CASH_FLOW { get; set; }
        [DataMember] [DataNames("BALANCE_CASH_FLOW")] public decimal BALANCE_CASH_FLOW { get; set; }

        [DataMember] [DataNames("APPROVED_INVOICES_COUNT")] public int APPROVED_INVOICES_COUNT { get; set; }
        [DataMember] [DataNames("PENDING_INVOICES_COUNT")] public int PENDING_INVOICES_COUNT { get; set; }
        [DataMember(Name = "TREND_MONTHS")] public List<string> TREND_MONTHS { get; set; }

        //[DataMember(Name = "MY_REQ_COUNT")] public List<int> MY_REQ_COUNT { get; set; }
        //[DataMember(Name = "TOTAL_REQ_COUNT")] public List<int> TOTAL_REQ_COUNT { get; set; }
        [DataMember] [DataNames("MONTHLY_AVG_SAVINGS")] public List<MonthlyAvgSavings> MONTHLY_AVG_SAVINGS { get; set; }
        //[DataMember(Name = "monthlyAvgSavings")] public List<KeyValuePair> MonthlyAvgSavings { get; set; }
        //[DataMember(Name = "stats")] public List<PArrayKeyValue> PArrayKeyValue { get; set; }
        [DataMember] [DataNames("TOP_SUPPLIER_BY_COST_CENTRE")] public List<TopSupplier> TOP_SUPPLIER_BY_COST_CENTRE { get; set; }
        [DataMember] [DataNames("MONTHLY_AVG_UTILIZATION")] public List<MonthlyAvgUtilizaion> MONTHLY_AVG_UTILIZATION { get; set; }
        [DataMember] [DataNames("TOP_PERFORMER")] public List<TopPerformer> TOP_PERFORMER { get; set; }
        [DataMember] [DataNames("LIVE_REQUIREMENTS")] public List<LiveRequirements> LIVE_REQUIREMENTS { get; set; }
        [DataMember] [DataNames("PROJECT_DETAILS")] public List<ProjDetails> PROJECT_DETAILS { get; set; }
        [DataMember] [DataNames("RELEASEDPO_COUNT")] public List<ReleasedPOCount> RELEASEDPO_COUNT { get; set; }
        [DataMember] [DataNames("PENDING_BILLS_COUNT")] public List<PendingBills> PENDING_BILLS_COUNT { get; set; }
        [DataMember] [DataNames("TRANSACTIONAL_TREND")] public List<TransactionalTrend> TRANSACTIONAL_TREND { get; set; }
        [DataMember] [DataNames("PENDING_INVOICE_APPROVAL")] public List<PendingInvoiceApproval> PENDING_INVOICE_APPROVAL { get; set; }
        [DataMember(Name = "chartSeries")]  public ChartData[] ChartSeries { get; set; }
        [DataMember(Name = "months")]  public string[] Months { get; set; }


    }

    public class BuyerVolume
    {
        [DataMember] [DataNames("U_ID")] public int U_ID { get; set; }
        [DataMember] [DataNames("BUYER_NAME")] public string BUYER_NAME { get; set; }
        [DataMember] [DataNames("PURCHASE_VOLUME")] public decimal PURCHASE_VOLUME { get; set; }
    }

    public class PackageVolume
    {
        [DataMember] [DataNames("PACKAGE_NAME")] public string PACKAGE_NAME { get; set; }
        [DataMember] [DataNames("PACKAGE_EST_BUDGET_AMOUNT")] public decimal PACKAGE_EST_BUDGET_AMOUNT { get; set; }
        [DataMember] [DataNames("ANTICIPATED_PO_VALUE")] public decimal ANTICIPATED_PO_VALUE { get; set; }
        [DataMember] [DataNames("PLANNED")] public decimal PLANNED { get; set; }
        [DataMember] [DataNames("SPENT")] public decimal SPENT { get; set; }
    }

    public class RevenueDetails
    {
        [DataMember] [DataNames("MONTH_YEAR")] public string MONTH_YEAR { get; set; }
        [DataMember] [DataNames("INVOICE_RAISED")] public decimal INVOICE_RAISED { get; set; }
        [DataMember] [DataNames("INVOICE_PAID")] public decimal INVOICE_PAID { get; set; }
        [DataMember] [DataNames("REVENUE_BOOKED")] public decimal REVENUE_BOOKED { get; set; }
    }

    public class RevenueStats
    {
        [DataMember] [DataNames("MONTH_YEAR")] public string MONTH_YEAR { get; set; }
        [DataMember] [DataNames("REVENUE_BOOKED")] public decimal REVENUE_BOOKED { get; set; }
    }

    public class MonthlyAvgSavings
    {
        [DataMember] [DataNames("MONTH")] public string MONTH { get; set; }
        [DataMember] [DataNames("YEAR")] public string YEAR { get; set; }
        [DataMember] [DataNames("QUOTATION_VALUE")] public decimal QUOTATION_VALUE { get; set; }
        [DataMember] [DataNames("SAVINGS")] public decimal SAVINGS { get; set; }
    }
    public class TopSupplier
    {
        [DataMember] [DataNames("COMP_NAME")] public string COMP_NAME { get; set; }
        [DataMember] [DataNames("LEAST_PRICES")] public decimal LEAST_PRICES { get; set; }
    }

    public class MonthlyAvgUtilizaion
    {
        [DataMember] [DataNames("MONTH_NAME")] public string MONTH_NAME { get; set; }
        [DataMember] [DataNames("MONTH_NUM")] public int MONTH_NUM { get; set; }
        [DataMember] [DataNames("REQ_COUNT")] public int REQ_COUNT { get; set; }
        [DataMember] [DataNames("REQ_STATUS")] public string REQ_STATUS { get; set; }
        [DataMember] [DataNames("MONTH_YEAR")] public string MONTH_YEAR { get; set; }
    }


    public class TransactionalTrend
    {
        [DataMember] [DataNames("MONTH_NAME")] public string MONTH_NAME { get; set; }
        [DataMember] [DataNames("MONTH_NUM")] public int MONTH_NUM { get; set; }
        [DataMember] [DataNames("MY_REQ_COUNT")] public int MY_REQ_COUNT { get; set; }
        [DataMember] [DataNames("TOTAL_REQ_COUNT")] public int TOTAL_REQ_COUNT { get; set; }
    }
    public class TopPerformer
    {
        [DataMember] [DataNames("PERFORMER_NAME")] public string PERFORMER_NAME { get; set; }
        [DataMember] [DataNames("PERFORMER_AMOUNT")] public decimal PERFORMER_AMOUNT { get; set; }
    }

    public class LiveRequirements
    {
        [DataMember] [DataNames("QUOTES_TO_BE_RECEIVED")] public int QUOTES_TO_BE_RECEIVED { get; set; }
        [DataMember] [DataNames("PROJ_ID")] public string PROJ_ID { get; set; }
        [DataMember] [DataNames("REQ_NUMBER")] public string REQ_NUMBER { get; set; }
        [DataMember] [DataNames("REQ_ID")] public int REQ_ID { get; set; }
    }
    public class ProjDetails
    {
        [DataMember] [DataNames("PROJECT_ID")] public int PROJECT_ID { get; set; }
        [DataMember] [DataNames("PROJECT_NAME")] public string PROJECT_NAME { get; set; }
        [DataMember] [DataNames("TARGETED_REVENUE_BUDGET")] public decimal TARGETED_REVENUE_BUDGET { get; set; }
        [DataMember] [DataNames("PACKAGE_AWARDED")] public int PACKAGE_AWARDED { get; set; }
        [DataMember] [DataNames("PACKAGES_PENDING_FOR_AWARD_RECOMMENDATION")] public int PACKAGES_PENDING_FOR_AWARD_RECOMMENDATION { get; set; }
        [DataMember] [DataNames("PACKAGES_PENDING_FOR_RFQ")] public int PACKAGES_PENDING_FOR_RFQ { get; set; }
    }

    public class ReleasedPOCount
    {
        [DataMember] [DataNames("PROJECT_ID")] public int PROJECT_ID { get; set; }
        [DataMember] [DataNames("POS_RELEASED_COUNT")] public int POS_RELEASED_COUNT { get; set; }

    }
    public class PendingBills
    {
        [DataMember] [DataNames("PROJECT_ID")] public int PROJECT_ID { get; set; }
        [DataMember] [DataNames("PENDING_BILLS")] public int PENDING_BILLS { get; set; }

    }


    public class PendingInvoiceApproval
    {
        [DataMember] [DataNames("PROJECT_ID")] public int PROJECT_ID { get; set; }
        [DataMember] [DataNames("PROJECT_NAME")] public string PROJECT_NAME { get; set; }
        [DataMember] [DataNames("PENDING_INVOICE_FOR_APPROVAL")] public int PENDING_INVOICE_FOR_APPROVAL { get; set; }
    }

    [DataContract]
    public class PArrayKeyValue
    {
        [DataMember(Name = "name")]
        public string Name { get; set; }

        [DataMember(Name = "arrayPair")]
        public List<KeyValuePair> ArrayPair { get; set; }
    }
}