﻿using PRM.Core.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace PRMServices.Models
{
    [DataContract]
    public class BudgetDetails : Entity
    {
        [DataMember] [DataNames("ROW_ID")] public int ROW_ID { get; set; }
        [DataMember] [DataNames("COMP_ID")] public int COMP_ID { get; set; }
        [DataMember] [DataNames("PROJ_ID")] public string PROJ_ID { get; set; }
        [DataMember] [DataNames("PROJECT_ID")] public int PROJECT_ID { get; set; }
        [DataMember] [DataNames("IS_VALID")] public int IS_VALID { get; set; }

        [DataMember] [DataNames("BUDGET_ID")] public int BUDGET_ID { get; set; }

        [DataMember] [DataNames("PACKAGE_ID")] public int PACKAGE_ID { get; set; }
        [DataMember] [DataNames("PACKAGE_NAME")] public string PACKAGE_NAME { get; set; }

        [DataMember] [DataNames("PO_VALUE")] public decimal PO_VALUE { get; set; }

        [DataMember] [DataNames("U_ID")] public int U_ID { get; set; }

        [DataMember] [DataNames("PROJECT_NAME")] public string PROJECT_NAME { get; set; }

        [DataMember] [DataNames("SUB_PACKAGE_ID")] public int SUB_PACKAGE_ID { get; set; }
        [DataMember] [DataNames("SUB_PACKAGE_NAME")] public string SUB_PACKAGE_NAME { get; set; }

        [DataMember] [DataNames("ALLOCATION_PERCENTAGE")] public decimal ALLOCATION_PERCENTAGE { get; set; }

        [DataMember] [DataNames("EST_BUDGET_AMOUNT")] public decimal EST_BUDGET_AMOUNT { get; set; }

        [DataMember] [DataNames("PACKAGE_STATUS")] public string PACKAGE_STATUS { get; set; }

        [DataMember] [DataNames("BUDGET_WF_ID")] public int BUDGET_WF_ID { get; set; }

        [DataMember] [DataNames("BUDGET_WF_APPROVAL_STATUS")] public string BUDGET_WF_APPROVAL_STATUS { get; set; }

        [DataMember] [DataNames("BUDGET_WF_PENDING_APPROVERS")] public string BUDGET_WF_PENDING_APPROVERS { get; set; }

        [DataMember] [DataNames("SUB_PACKAGE_WF_ID")] public int SUB_PACKAGE_WF_ID { get; set; }

        [DataMember] [DataNames("SUB_PACKAGE_WF_APPROVAL_STATUS")] public string SUB_PACKAGE_WF_APPROVAL_STATUS { get; set; }

        [DataMember] [DataNames("SUB_PACKAGE_WF_PENDING_APPROVERS")] public string SUB_PACKAGE_WF_PENDING_APPROVERS { get; set; }

        [DataMember] [DataNames("COST_CENTER")] public string COST_CENTER { get; set; }
        [DataMember] [DataNames("LOCATION")] public string LOCATION { get; set; }

        [DataMember] [DataNames("VENDOR_IDS")] public string VENDOR_IDS { get; set; }
        [DataMember] [DataNames("Vendors")] public string Vendors { get; set; }
        [DataMember] [DataNames("poVendors")] public string POVendors { get; set; }

        [DataMember] [DataNames("TARGETED_REVENUE_BUDGET")] public decimal TARGETED_REVENUE_BUDGET { get; set; }

        [DataMember] [DataNames("NET_REVENUE_MARGIN")] public decimal NET_REVENUE_MARGIN { get; set; }

        [DataMember] [DataNames("DATE_CREATED")] public DateTime? DATE_CREATED { get; set; }

        [DataMember] [DataNames("DATE_MODIFIED")] public DateTime? DATE_MODIFIED { get; set; }

        [DataMember] [DataNames("CREATED_BY")] public int CREATED_BY { get; set; }

        [DataMember] [DataNames("MODIFIED_BY")] public int MODIFIED_BY { get; set; }
        [DataMember] [DataNames("REQ_ID")] public int REQ_ID { get; set; }
        [DataMember] [DataNames("QCS_ID")] public int QCS_ID { get; set; }
        [DataMember] [DataNames("QCS_PO_VALUE")] public decimal QCS_PO_VALUE { get; set; }
        [DataMember] [DataNames("PO_VENDOR_IDS")] public string PO_VENDOR_IDS { get; set; }
        [DataMember] [DataNames("PACKAGE_APPROVAL_ID")] public int PACKAGE_APPROVAL_ID { get; set; }
        [DataMember] [DataNames("PACKAGE_APPROVAL_WF_ID")] public int PACKAGE_APPROVAL_WF_ID { get; set; }
        [DataMember] [DataNames("PACKAGE_APPROVAL_WF_APPROVAL_STATUS")] public string PACKAGE_APPROVAL_WF_APPROVAL_STATUS { get; set; }
        [DataMember] [DataNames("PACKAGE_APPROVAL_WF_PENDING_APPROVERS")] public string PACKAGE_APPROVAL_WF_PENDING_APPROVERS { get; set; }
        [DataMember] [DataNames("ANTICIPATED_BUDGET_AMOUNT")] public decimal ANTICIPATED_BUDGET_AMOUNT { get; set; }
        [DataMember] [DataNames("PO_NUMBER")] public string PO_NUMBER { get; set; }
        [DataMember] [DataNames("IS_BUDGET_APPROVED")] public bool IS_BUDGET_APPROVED { get; set; }
        [DataMember] [DataNames("IS_FROM_PROJECT_AMENDMENT")] public bool IS_FROM_PROJECT_AMENDMENT { get; set; }
        [DataMember] [DataNames("PROJECT_PO_VALUE")] public decimal PROJECT_PO_VALUE { get; set; }
        [DataMember] [DataNames("PROJECT_NET_REVENUE_MARGIN")] public decimal PROJECT_NET_REVENUE_MARGIN { get; set; }
        [DataMember] [DataNames("PROJECT_TARGETED_REVENUE_BUDGET")] public decimal PROJECT_TARGETED_REVENUE_BUDGET { get; set; }
        [DataMember] [DataNames("VENDOR_PO_AMOUNT")] public decimal VENDOR_PO_AMOUNT { get; set; }
        [DataMember] [DataNames("BUDGET_ROW_ID")] public int BUDGET_ROW_ID { get; set; }
        [DataMember] [DataNames("MULTIPLE_ATTACHMENTS")] public List<FileUpload> MULTIPLE_ATTACHMENTS { get; set; }
        [DataMember] [DataNames("ATTACHMENTS")] public string ATTACHMENTS { get; set; }
    }


}