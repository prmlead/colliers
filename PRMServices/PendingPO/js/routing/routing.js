﻿prmApp
    .config(["$stateProvider", "$urlRouterProvider", "$httpProvider", "$provide", "domain", "version",
        function ($stateProvider, $urlRouterProvider, $httpProvider, $provide, domain, version) {
            $stateProvider
                .state('list-pendingPO', {
                    url: '/list-pendingPO',
                    templateUrl: 'PendingPO/views/list-pendingPO.html',
                    params: {
                        excludeContracts: true,
                        onlyContracts: false,
                        detailsObj: null
                    }
                })
                .state('list-po-new', {
                    url: '/list-po-new',
                    templateUrl: 'PendingPO/views/list-po.html',
                    params: {
                        projectId: 0,
                        poNumber: ''
                    }
                })
                .state('view-po-details', {
                    url: '/view-po-details',
                    templateUrl: 'PendingPO/views/view-po-details.html',
                    params: {
                        poNumber: ''
                    }
                })
                .state('view-pendingPO', {
                    url: '/view-pendingPO/:poID',
                    templateUrl: 'PendingPO/views/view-pendingPO.html',
                    params: {
                        detailsObj: null
                    }
                })

                .state('poAmendment', {
                    url: '/poAmendment/:poID/:PO_AMENDMENT_ID',
                    templateUrl: 'PendingPO/views/poAmendment.html',
                    params: {
                        detailsObj: null
                    }
                })

                .state('uploadvendorinvoice', {
                    url: '/uploadvendorinvoice/:ID',
                    templateUrl: 'PendingPO/views/uploadvendorinvoice.html'
                })

                //.state('billRequests', {
                //    url: '/billRequests/:poNumber',
                //    templateUrl: 'PendingPO/views/billRequests.html',
                //    params: {
                //        detailsObj: null
                //    }
                //})

                //.state('billRequests', {
                //    url: '/billRequests',
                //    templateUrl: 'PendingPO/views/billRequests.html',
                //    params: {
                //        detailsObj: null
                //    }
                //})

                .state('billRequestsList', {
                    url: '/billRequestsList',
                    templateUrl: 'PendingPO/views/billRequestsList.html',
                    params: {
                        detailsObj: null
                    }
                })

                .state('createBillRequest', {
                    url: '/createBillRequest/:poNumber/:ID',
                    templateUrl: 'PendingPO/views/createBillRequest.html',
                    params: {
                        poItemsForBillRequest: null
                    }
                })

                .state('editBillRequest', {
                    url: '/editBillRequest/:poNumber/:ID',
                    templateUrl: 'PendingPO/views/createBillRequest.html',
                    params: {
                        detailsObj: null
                    }
                })

                .state('viewBillRequest', {
                    url: '/viewBillRequest/:poNumber/:ID',
                    templateUrl: 'PendingPO/views/createBillRequest.html',
                    params: {
                        detailsObj: null
                    }
                })
                .state('createInvoices', {
                    url: '/createInvoices/:PO_NUMBER/:BILL_ID/:INVOICE_ID',//:PROJECT_ID/:PACKAGE_ID/:SUB_PACKAGE_ID/:BILL_ID,
                    templateUrl: 'PendingPO/views/createInvoice.html',
                    params: {
                        detailsObj: null
                    }
                })
                .state('viewInvoices', {
                    url: '/viewInvoices/:PO_NUMBER/:BILL_ID/:INVOICE_ID',//:PROJECT_ID/:PACKAGE_ID/:SUB_PACKAGE_ID/:BILL_ID,
                    templateUrl: 'PendingPO/views/createInvoice.html',
                    params: {
                        detailsObj: null
                    }
                })
                .state('editInvoices', {
                    url: '/editInvoices/:PO_NUMBER/:BILL_ID/:INVOICE_ID',//:PROJECT_ID/:PACKAGE_ID/:SUB_PACKAGE_ID/:BILL_ID,
                    templateUrl: 'PendingPO/views/createInvoice.html',
                    params: {
                        detailsObj: null
                    }
                })
                .state('listInvoices', {
                    url: '/listInvoices',
                    templateUrl: 'PendingPO/views/listInvoices.html',
                    params: {
                        detailsObj: null
                    }
                }).state('pociPDF', {
                    url: '/pociPDF',
                    templateUrl: 'PendingPO/views/pociPDF.html',
                    params: {
                        poNumber: ''
                    }
                })
                .state('poAudit', {
                    url: '/poAudit/:PO_NUMBER',
                    templateUrl: 'PendingPO/views/poAudit.html',
                    params: {
                        detailsObj: null
                    }
                });

        }]);