﻿prmApp.constant('PRMPOServiceDomain', 'svc/PRMPOService.svc/REST/');
prmApp.service('PRMPOService', ["PRMPOServiceDomain", "SAPIntegrationServicesDomain", "userService", "httpServices", "$window",
    function (PRMPOServiceDomain, SAPIntegrationServicesDomain, userService, httpServices, $window) {

        var PRMPOService = this;

        PRMPOService.getPOScheduleList = function (params) {

            params.onlycontracts = params.onlycontracts ? params.onlycontracts : 0;
            params.excludecontracts = params.excludecontracts ? params.excludecontracts : 0;

            let url = PRMPOServiceDomain + 'getposchedulelist?compid=' + params.compid + '&uid=' + params.uid + '&search=' + params.search + '&categoryid=' + params.categoryid
                + '&productid=' + params.productid + '&supplier=' + params.supplier + '&postatus=' + params.postatus
                + '&deliverystatus=' + params.deliverystatus
                + '&plant=' + params.plant
                + '&fromdate=' + params.fromdate + '&todate=' + params.todate
                + '&onlycontracts=' + params.onlycontracts + '&excludecontracts=' + params.excludecontracts + '&ackStatus=' + params.ackStatus + '&buyer=' + params.buyer + '&purchaseGroup=' + params.purchaseGroup
                + '&page=' + params.page
                + '&pagesize=' + params.pagesize + '&sessionid=' + userService.getUserToken();
            return httpServices.get(url);
        };

        PRMPOService.getPOScheduleItems = function (params) {
            let url = PRMPOServiceDomain + 'getposcheduleitems?ponumber=' + params.ponumber + '&moredetails=' + params.moredetails + '&forasn=' + params.forasn + '&sessionid=' + userService.getUserToken();

            return httpServices.get(url);
        };

        PRMPOService.getPOList = function (params) {
            if (!params.poNumber) {
                params.poNumber = '';
            }

            if (!params.vendorId) {
                params.vendorId = 0;
            }

            if (!params.projectId) {
                params.projectId = 0;
            }

            let url = PRMPOServiceDomain + 'getPOList';
            return httpServices.post(url, params);
        };

        PRMPOService.getPODetails = function (params) {
            let url = PRMPOServiceDomain + 'getPODetails';
            return httpServices.post(url, params);
        };

        PRMPOService.getPOAmendmentDetails = function (params) {
            let url = PRMPOServiceDomain + 'getPOAmendmentDetails';
            return httpServices.post(url, params);
        };
        

        PRMPOService.AmendPODetails = function (params) {
            let url = PRMPOServiceDomain + 'amendPODetails';
            return httpServices.post(url, params);
        };

        PRMPOService.UpdatePORevisionNumber = function (params) {
            let url = PRMPOServiceDomain + 'UpdatePORevisionNumber';
            return httpServices.post(url, params);
        };
        
        PRMPOService.savePOtermsandconditions = function (params) {
            let url = PRMPOServiceDomain + 'savePOtermsandconditions';
            return httpServices.post(url, params);
        };

        PRMPOService.savePODetails = function (params) {
            let url = PRMPOServiceDomain + 'savePODetails';
            return httpServices.post(url, params);
        };

        PRMPOService.editPODetails = function (params) {
            let url = PRMPOServiceDomain + 'editPODetails';
            return httpServices.post(url, params);
        };

        PRMPOService.editPOItemDetails = function (params) {
            let url = PRMPOServiceDomain + 'editPOItemDetails';
            return httpServices.post(url, params);
        };

        PRMPOService.getPOFilterValues = function (params) {
            let url = PRMPOServiceDomain + 'getPOFilters?compid=' + params.compid + '&sessionid=' + userService.getUserToken();
            return httpServices.get(url);
        };

        PRMPOService.editPOInvoice = function (params) {
            let url = PRMPOServiceDomain + 'editPOInvoice';
            return httpServices.post(url, params);
        };


        PRMPOService.getPOInvoiceDetails = function (params) {
            if (!params.vendorID) {
                params.vendorID = 0;
            }
            if (!params.invoiceID) {
                params.invoiceID = 0;
            }
            let url = PRMPOServiceDomain + 'getpoinvoicedetails?ponumber=' + params.ponumber + '&sessionid=' + userService.getUserToken() + '&vendorID=' + params.vendorID + '&invoiceID=' + params.invoiceID;
            return httpServices.get(url);
        };

        PRMPOService.getASNDetails = function (params) {
            let url = PRMPOServiceDomain + 'getasndetails?compid=' + params.compid + '&asnid=' + params.asnid + '&ponumber=' + params.ponumber + '&grncode=' + params.grncode + '&asncode=' + params.asncode + '&sessionid=' + userService.getUserToken() + '&vendorid=' + params.vendorid;
            return httpServices.get(url);
        };

        PRMPOService.getASNDetailsList = function (params) {
            let url = PRMPOServiceDomain + 'getasndetailslist?ponumber=' + params.ponumber + '&sessionid=' + userService.getUserToken() + '&vendorid=' + params.vendorid;
            return httpServices.get(url);
        };

        PRMPOService.saveASNdetails = function (params) {
            let url = PRMPOServiceDomain + 'saveasndetails';
            return httpServices.post(url, params);
        };

        PRMPOService.savePOInvoice = function (params) {
            let url = PRMPOServiceDomain + 'savepoinvoice';
            return httpServices.post(url, params);
        };

        PRMPOService.savepoattachments = function (params) {
            let url = PRMPOServiceDomain + 'savepoattachments';
            return httpServices.post(url, params);
        };

        PRMPOService.SavePODetails = function (params) {
            let url = PRMPOServiceDomain + 'savePODetails';
            return httpServices.post(url, params);
        };

        PRMPOService.poApproval = function (params) {
            let url = PRMPOServiceDomain + 'poapproval';
            return httpServices.post(url, params);
        };

        PRMPOService.deletePOInvoice = function (params) {
            if (!params.invoiceId) {
                params.invoiceId = 0;
            }
            if (!params.wfId) {
                params.wfId = 0;
            }
            let url = PRMPOServiceDomain + 'deletepoinvoice?ponumber=' + params.ponumber + '&invoicenumber=' + params.invoicenumber + '&invoiceId=' + params.invoiceId + '&wfId=' + params.wfId + '&sessionid=' + params.sessionid;
            return httpServices.get(url, params);
        };

        PRMPOService.GeneratePOPDF = function (params) {
            let url = PRMPOServiceDomain + 'generatepopdf?ponumber=' + params + '&sessionid=' + userService.getUserToken();
            let data = httpServices.get(url).then(function (response) {
                if (response) {
                    response = b64toBlob(response, 'application/octet-stream');
                    var linkElement = document.createElement('a');
                    try {
                        var url = $window.URL.createObjectURL(response);
                        linkElement.setAttribute('href', url);
                        linkElement.setAttribute("download", params + ".pdf");

                        var clickEvent = new MouseEvent("click", {
                            "view": window,
                            "bubbles": true,
                            "cancelable": false
                        });
                        linkElement.dispatchEvent(clickEvent);
                    }
                    catch (ex) { }
                    return response;
                }
            }, function (result) {
                //console.log("date error");
            });
        };

        function b64toBlob(b64Data, contentType, sliceSize) {
            contentType = contentType || '';
            sliceSize = sliceSize || 512;

            var byteCharacters = atob(b64Data);
            var byteArrays = [];

            for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
                var slice = byteCharacters.slice(offset, offset + sliceSize);

                var byteNumbers = new Array(slice.length);
                for (var i = 0; i < slice.length; i++) {
                    byteNumbers[i] = slice.charCodeAt(i);
                }

                var byteArray = new Uint8Array(byteNumbers);

                byteArrays.push(byteArray);
            }

            var blob = new Blob(byteArrays, { type: contentType });
            return blob;
        }


        PRMPOService.getBillRequests = function (params) {
            let url = PRMPOServiceDomain + 'getBillRequests?COMP_ID=' + params.COMP_ID + '&U_ID=' + params.U_ID + '&IS_CUSTOMER=' + params.IS_CUSTOMER + '&poNumber=' + params.poNumber +
                '&projectIds=' + params.projectIds + '&billsStatus=' + params.billsStatus + '&packageIds=' + params.packageIds + '&subPackageIds=' + params.subPackageIds + '&vendorIds=' + params.vendorIds
                + '&fromDate=' + params.fromDate + '&toDate=' + params.toDate + '&PageSize=' + params.PageSize +
                '&NumberOfRecords=' + params.NumberOfRecords + '&sessionid=' + params.sessionid;
            return httpServices.get(url);
        };

        PRMPOService.getInvoicedetails = function (params) {
            let url = PRMPOServiceDomain + 'getInvoicedetails?COMP_ID=' + params.COMP_ID + '&INVOICE_ID=' + params.INVOICE_ID + '&sessionid=' + params.sessionid;
            return httpServices.get(url);
        };

        PRMPOService.GetInvoiceAmountForPO = function (params) {
            let url = PRMPOServiceDomain + 'GetInvoiceAmountForPO?U_ID=' + params.U_ID + '&COMP_ID=' + params.COMP_ID + '&PO_NUMBER=' + params.PO_NUMBER + '&sessionid=' + params.sessionid;
            return httpServices.get(url);
        };

        PRMPOService.getPOScheduleDetails = function (params) {
            let url = PRMPOServiceDomain + 'getPOScheduleDetails?COMP_ID=' + params.COMP_ID + '&PO_NUMBER=' + params.PO_NUMBER + '&VALIDATE=' + params.VALIDATE + '&sessionid=' + params.sessionid;
            return httpServices.get(url);
        };

        PRMPOService.createBillSubmission = function (params) {
            let url = PRMPOServiceDomain + 'createBillSubmission';
            return httpServices.post(url, params);
        };

        PRMPOService.acknowledgeOrRejectBillRequest = function (params) {
            let url = PRMPOServiceDomain + 'acknowledgeOrRejectBillRequest';
            return httpServices.post(url, params);
        };

        PRMPOService.submitForBillCertification = function (params) {
            let url = PRMPOServiceDomain + 'submitForBillCertification';
            return httpServices.post(url, params);
        };

        PRMPOService.uploadBillCertification = function (params) {
            let url = PRMPOServiceDomain + 'uploadBillCertification';
            return httpServices.post(url, params);
        };

        PRMPOService.getBillRequestsFilterValues = function (params) {
            let url = PRMPOServiceDomain + 'getBillRequestsFilterValues?COMP_ID=' + params.COMP_ID + '&U_ID=' + params.U_ID + '&IS_CUSTOMER=' + params.IS_CUSTOMER + '&FROM_DATE=' + params.FROM_DATE + '&TO_DATE=' + params.TO_DATE +'&sessionid=' + params.sessionid;
            return httpServices.get(url);
        };

        PRMPOService.getInvoiceFilterValues = function (params) {
            let url = PRMPOServiceDomain + 'getInvoiceFilterValues?COMP_ID=' + params.COMP_ID + '&U_ID=' + params.U_ID + '&IS_CUSTOMER=' + params.IS_CUSTOMER
                + '&fromDate=' + params.fromDate + '&toDate=' + params.toDate +'&sessionid=' + params.sessionid;
            return httpServices.get(url);
        }

        PRMPOService.getBillsBasedOnID = function (params) {
            let url = PRMPOServiceDomain + 'getBillsBasedOnID?PO_NUMBER=' + params.PO_NUMBER + '&BILL_ID=' + params.BILL_ID + '&sessionid=' + params.sessionid;
            return httpServices.get(url);
        };

        PRMPOService.getPODetailsBasedOnID = function (params) {
            let url = PRMPOServiceDomain + 'getPODetailsBasedOnID?PO_NUMBER=' + params.PO_NUMBER + '&sessionid=' + params.sessionid;
            return httpServices.get(url);
        };

        PRMPOService.saveInvoiceDetails = function (params) {
            let url = PRMPOServiceDomain + 'saveInvoiceDetails';
            return httpServices.post(url, params);
        };

        PRMPOService.getInvoicesList = function (params) {//&U_ID={U_ID}&IS_CUSTOMER={IS_CUSTOMER}
            let url = PRMPOServiceDomain + 'getInvoicesList?COMP_ID=' + params.COMP_ID + '&U_ID=' + params.U_ID + '&IS_CUSTOMER=' + params.IS_CUSTOMER + '&fromDate=' + params.fromDate + '&toDate=' + params.toDate +
                '&poNumber=' + params.poNumber +
                '&projectIds=' + params.projectIds + '&invoiceNumbers=' + params.invoiceNumbers + '&invoiceStatus=' + params.invoiceStatus + '&sessionid=' + params.sessionid + '&PageSize=' + params.PageSize
                + '&NumberOfRecords=' + params.NumberOfRecords;
            return httpServices.get(url);
        };

        PRMPOService.savePOPDFDetails = function (params) {
            let url = PRMPOServiceDomain + 'savePOPDFDetails';
            return httpServices.post(url, params);
        };

        PRMPOService.GetPoAudit = function (params) {
            let url = PRMPOServiceDomain + 'GetPoAudit?COMP_ID=' + params.COMP_ID + '&PO_NUMBER=' + params.PO_NUMBER + '&sessionid=' + params.sessionid;
            return httpServices.get(url);
        };

        PRMPOService.deactiveBillRequest = function (params) {
            let url = PRMPOServiceDomain + 'deactiveBillRequest';
            return httpServices.post(url, params);
        };

        PRMPOService.PDFSave = function (params) {
            let url = PRMPOServiceDomain + 'PDFSave?tcData=' + params.tcData
                + '&poNumber=' + params.poNumber
                + '&compId=' + params.compId
                + '&revision=' + params.revision
                + '&sessionId=' + params.sessionId
                + '&saveToServer=' + params.saveToServer
                + '&validateSession=' + params.validateSession;

            let data1 = httpServices.get(url).then(function (response) {
                if (response) {
                    var base64String = "data:application/pdf;base64," + response.Message;
                    const linkSource = base64String;
                    const downloadLink = document.createElement("a");
                    const fileName = params.poNumber + ".pdf";
                    downloadLink.href = linkSource;
                    downloadLink.download = fileName;
                    downloadLink.click();
                }
            }, function (result) {
                //console.log("date error");
            });

            return data1
        };

        return PRMPOService;

    }]);