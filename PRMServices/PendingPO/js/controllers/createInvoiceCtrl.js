
prmApp.controller('createInvoiceCtrl', ["$scope", "$stateParams", "$log", "$state", "$window", "userService", "auctionsService", "storeService", "growlService", "PRMProjectServices", "$filter", "fileReader", "$rootScope", "PRMPOService", "workflowService",
    function ($scope, $stateParams, $log, $state, $window, userService, auctionsService, storeService, growlService, PRMProjectServices, $filter, fileReader, $rootScope, PRMPOService, workflowService) {

        $scope.USER_ID = +userService.getUserId();
        $scope.COMP_ID = +userService.getUserCompanyId();
        $scope.customerCompanyId = +userService.getCustomerCompanyId();
        $scope.sessionID = userService.getUserToken();
        $scope.isSuperUser = userService.getUserObj().isSuperUser;
        $scope.isVendor = (userService.getUserType() == "VENDOR") ? true : false;
        $scope.PO_NUMBER = $stateParams.PO_NUMBER;
        //$scope.PROJECT_ID = $stateParams.PROJECT_ID;
        //$scope.PACKAGE_ID = $stateParams.PACKAGE_ID;
        //$scope.SUB_PACKAGE_ID = $stateParams.SUB_PACKAGE_ID;
        $scope.BILL_ID = +$stateParams.BILL_ID;
        $scope.INVOICE_ID = +$stateParams.INVOICE_ID;
        $scope.editAccess = 0;
        if ($state.current.name === 'editInvoices' || $state.current.name === 'createInvoices' ) {
            $scope.editAccess = 0;
        } else {
            $scope.editAccess = 1;
        }


        $scope.deptIDs = [];
        $scope.desigIDs = [];

        /*region start WORKFLOW*/
        $scope.workflowList = [];
        $scope.itemWorkflow = [];
        $scope.workflowObj = {};
        $scope.workflowObj.workflowID = 0;
        $scope.currentStep = 0;
        $scope.orderInfo = 0;
        $scope.assignToShow = '';
        $scope.isWorkflowCompleted = false;
        $scope.WorkflowModule = 'VENDOR_INVOICE';
        $scope.disableWFSelection = false;
        /*region end WORKFLOW*/
        $scope.maxDate = moment();



        $scope.ListUserDepartmentDesignations = userService.getListUserDepartmentDesignations();
        if ($scope.ListUserDepartmentDesignations && $scope.ListUserDepartmentDesignations.length > 0) {
            $scope.ListUserDepartmentDesignations.forEach(function (item, index) {
                $scope.deptIDs.push(item.deptID);
                item.listDesignation.forEach(function (item1, index1) {
                    if (item1.isAssignedToUser && item1.isValid) {
                        $scope.desigIDs.push(item1.desigID);
                    }
                });
            });
        }


        $scope.getPODetailsBasedOnID = function () {
            $scope.params =
            {
                "PO_NUMBER": $scope.PO_NUMBER,
                "sessionid": $scope.sessionID
            };

            PRMPOService.getPODetailsBasedOnID($scope.params)
                .then(function (response) {

                    if (response) {
                        var resp = JSON.parse(response).Table;
                        $scope.invoicesList = JSON.parse(response).Table;
                        $scope.selectedPODetails.VENDOR_ID = $scope.invoicesList[0].VENDOR_ID;
                        $scope.selectedPODetails.VENDOR_CODE = $scope.invoicesList[0].VENDOR_CODE;

                    }
                });
        };



        $scope.checkListObj =
        {
            checkListArr:
                [
                    {
                        isChecked: true,
                        value: "PO Acceptance copy submission / Variation order copy.",
                        MULTIPLE_ATTACHMENTS: []
                    },
                    {
                        isChecked: true,
                        value: "Proforma Invoice.",
                        MULTIPLE_ATTACHMENTS: []
                    },
                    {
                        isChecked: true,
                        value: "MAR's.",
                        MULTIPLE_ATTACHMENTS: []
                    },
                    {
                        isChecked: true,
                        value: "DC' copies.",
                        MULTIPLE_ATTACHMENTS: []
                    },
                    {
                        isChecked: true,
                        value: "Joint Measurement Sheets.",
                        MULTIPLE_ATTACHMENTS: []
                    },
                    {
                        isChecked: true,
                        value: "Reference drawings supporting measurements.",
                        MULTIPLE_ATTACHMENTS: []
                    },
                    {
                        isChecked: true,
                        value: "Backup documents including invoices related to Basic rate variation.",
                        MULTIPLE_ATTACHMENTS: []
                    },
                    {
                        isChecked: true,
                        value: "Non Tendered item backups.",
                        MULTIPLE_ATTACHMENTS: []
                    },
                    {
                        isChecked: true,
                        value: "Other documents.",
                        MULTIPLE_ATTACHMENTS: []
                    }
                ]
        };

        $scope.selectedPODetails = {
            "ROW_ID": 0,
            "INVOICE_ID": 0,
            "BILL_ID": $scope.BILL_ID,
            "PO_NUMBER": '',
            "PROJECT_ID": 0,
            "VENDOR_ID": 0,
            "VENDOR_CODE": '',
            "INVOICE_NUMBER": '',
            "INVOICE_DESCRIPTION": '',
            "INVOICE_DATE1": '',
            "BILL_CERTIFIED_AMOUNT": 0,
            "INVOICE_AMOUNT": 0,
             "TOTAL_AMOUNT" : 0,
            "CREATED_BY": $scope.USER_ID,
            "V_COMP_ID": $scope.COMP_ID,
            "C_COMP_ID": $scope.customerCompanyId,
            "TAX_AMOUNT": 0,
            "MULTIPLE_ATTACHMENTS": '',
            "INVOICE_WF_ID": 0
        };
        $scope.ApproverDate = {
            "LAST_APPROVER_MODIFIED_DATE": ''
        }

        $scope.TotalPO = {
            "PO_VALUE": ''
        }

        $scope.getBillsBasedOnID = function () {
            $scope.billRequestsList = [];
            $scope.billAttachmentsList = [];
            $scope.params =
            {
                "PO_NUMBER": $scope.PO_NUMBER,
                "BILL_ID": $scope.BILL_ID,
                "sessionid": $scope.sessionID
            };

            PRMPOService.getBillsBasedOnID($scope.params)
                .then(function (response) {
                    if (response) {
                        $scope.PO_ITEMS_FOR_BILL_REQUEST = JSON.parse(response).Table;
                        if ($scope.PO_ITEMS_FOR_BILL_REQUEST && $scope.PO_ITEMS_FOR_BILL_REQUEST.length > 0) {
                            $scope.PO_ITEMS_FOR_BILL_REQUEST.forEach(function (billItem, billIndex) {
                                billItem.TOTAL_AMOUNT = billItem.OVERALL_AMOUNT;
                                if ($scope.BILL_ID > 0) {
                                    $scope.checkListObj.checkListArr = angular.fromJson(billItem.CHECK_LIST_JSON);
                                    $scope.billSubmissionObj = {
                                        BILL_TYPE: $scope.PO_ITEMS_FOR_BILL_REQUEST[0].BILL_TYPE
                                    };
                                }
                                billItem.DISPLAY = false;
                            });
                        }
                        $scope.selectedPODetails.PO_NUMBER = $scope.PO_ITEMS_FOR_BILL_REQUEST[0].PO_NUMBER;
                        $scope.selectedPODetails.PROJECT_ID = $scope.PO_ITEMS_FOR_BILL_REQUEST[0].PROJECT_ID;
                        $scope.selectedPODetails.VENDOR_ID = $scope.USER_ID;
                        $scope.selectedPODetails.VENDOR_CODE = '';
                        $scope.selectedPODetails.BILL_CERTIFIED_AMOUNT = $scope.PO_ITEMS_FOR_BILL_REQUEST[0].BILL_CERTIFIED_AMOUNT;
                        $scope.ApproverDate.LAST_APPROVER_MODIFIED_DATE = moment($scope.PO_ITEMS_FOR_BILL_REQUEST[0].LAST_APPROVER_MODIFIED_DATE.split('T')[0]).format('DD-MM-YYYY');//$scope.getDate();
                        $scope.TotalPO.PO_VALUE = $scope.PO_ITEMS_FOR_BILL_REQUEST[0].PO_VALUE;
                    }
                });
        };


        if ($scope.PO_NUMBER) {
            $scope.getBillsBasedOnID($scope.PO_NUMBER);
        }

        $scope.getInvoicedetails = function () {
            var params = {
                "COMP_ID": $scope.COMP_ID,
                "INVOICE_ID": $scope.INVOICE_ID,
                "sessionid": $scope.sessionID
            }
            PRMPOService.getInvoicedetails(params)
                .then(function (response) {
                    $scope.Invoice = JSON.parse(response).Table;
                    $scope.selectedPODetails = {
                        "ROW_ID": $scope.Invoice[0].ROW_ID,
                        "INVOICE_NUMBER": $scope.Invoice[0].INVOICE_NUMBER,
                        "INVOICE_DESCRIPTION": $scope.Invoice[0].INVOICE_DESCRIPTION,
                        "INVOICE_DATE": $scope.Invoice[0].INVOICE_DATE,
                        "BILL_CERTIFIED_AMOUNT": $scope.Invoice[0].BILL_CERTIFIED_AMOUNT,
                        "INVOICE_AMOUNT": $scope.Invoice[0].INVOICE_AMOUNT ,
                        "TOTAL_AMOUNT": $scope.Invoice[0].TOTAL_AMOUNT,                        
                        "TAX_AMOUNT": $scope.Invoice[0].TAX_AMOUNT,
                        "PO_NUMBER": $scope.Invoice[0].PO_NUMBER,
                        "PROJECT_ID": $scope.Invoice[0].PROJECT_ID,
                        //"MULTIPLE_ATTACHMENTS": JSON.parse($scope.selectedPODetails.MULTIPLE_ATTACHMENTS),
                        "INVOICE_WF_ID": $scope.Invoice[0].INVOICE_WF_ID,
                        "INVOICE_STATUS": $scope.Invoice[0].INVOICE_STATUS,
                        "VENDOR_ID": $scope.Invoice[0].VENDOR_ID,
                        ATTACHMENTS_ARRAY: [],
                        MULTIPLE_ATTACHMENTS : [],


                    };
                    if ($scope.Invoice[0].MULTIPLE_ATTACHMENTS) {
                        var list = angular.fromJson($scope.Invoice[0].MULTIPLE_ATTACHMENTS);//billItem.checkListArr
                        if (list && list.length > 0) {
                            list.forEach(function (attachItem, attachIndex) {
                                $scope.selectedPODetails.MULTIPLE_ATTACHMENTS.push(attachItem);
                            });
                        }
                    }
                    
                    if ($scope.selectedPODetails.INVOICE_WF_ID > 0) {
                        $scope.getItemWorkflow($scope.INVOICE_ID, $scope.WorkflowModule)
                    }

                    
                })
        }

        if ($scope.INVOICE_ID > 0) {
            $scope.getInvoicedetails();
        };



        //if ($scope.PO_NUMBER) {
        //    $scope.getPODetailsBasedOnID();
        //};

        $scope.selectedIndex = 0;
        $scope.getFile1 = function (id, itemid, ext) {
            $scope.filesTemp = $("#" + id)[0].files;

            $scope.filesTemp = Object.values($scope.filesTemp);
            $scope.filesTemp.forEach(function (attach, attachIndex) {
                $scope.file = $("#" + id)[0].files[attachIndex];

                $scope.filesTemp.forEach(function (attach, attachIndex) {
                    $scope.file = $("#" + id)[0].files[attachIndex];
                    fileReader.readAsDataUrl($scope.file, $scope)
                        .then(function (result) {
                            var fileUpload = {
                                fileStream: [],
                                fileName: '',
                                fileID: 0
                            };
                            var bytearray = new Uint8Array(result);
                            fileUpload.fileStream = $.makeArray(bytearray);
                            fileUpload.fileName = attach.name;
                            if (!$scope.selectedPODetails.MULTIPLE_ATTACHMENTS) {
                                $scope.selectedPODetails.MULTIPLE_ATTACHMENTS = [];
                            }

                            var ifExists = _.findIndex($scope.selectedPODetails.MULTIPLE_ATTACHMENTS, function (attach) { return attach.fileName.toLowerCase() === fileUpload.fileName.toLowerCase() });

                            if (ifExists <= -1) {
                                $scope.selectedPODetails.MULTIPLE_ATTACHMENTS.push(fileUpload);
                            }
                        });
                });

            });
            $("#").val('');
        };
        $scope.removeAttach = function (parentIndex, index) {
            $scope.selectedPODetails.MULTIPLE_ATTACHMENTS.splice(index, 1);
        };

        $scope.saveInvoiceDetails = function () {

            $scope.errorMessage = '';
            $scope.invNumberErrorMessage = '';
            $scope.invDescErrorMessage = '';
            $scope.billAmountErrorMessage = '';
            $scope.invAmountErrorMessage = '';
            $scope.invAmountwithErrorMessage = '';
            $scope.invDateErrorMessage = '';
            $scope.attachmentsErrorMessage = '';
            $scope.taxAmountErrorMessage = '';

            if (!$scope.selectedPODetails.INVOICE_NUMBER) {
                $scope.invNumberErrorMessage = 'Please Enter Invoice Number.';
                $scope.errorMessage = $scope.invNumberErrorMessage;
            };

            if (!$scope.selectedPODetails.INVOICE_DESCRIPTION) {
                $scope.invDescErrorMessage = 'Please Enter Invoice Description.';
                $scope.errorMessage = $scope.invDescErrorMessage;
            };

            if (!($scope.selectedPODetails.BILL_CERTIFIED_AMOUNT > 0)) {
                $scope.billAmountErrorMessage = 'Please Enter Bill Certified Amount.';
                $scope.errorMessage = $scope.billAmountErrorMessage;
            };

            if (!$scope.selectedPODetails.INVOICE_DATE) {
                $scope.invDateErrorMessage = 'Please Enter Invoice Date.';
                $scope.errorMessage = $scope.invDateErrorMessage;
            };

            if (!($scope.selectedPODetails.INVOICE_AMOUNT > 0)) {
                $scope.invAmountErrorMessage = 'Please Enter Invoice Amount(Without TAX).';
                $scope.errorMessage = $scope.invAmountErrorMessage;
            };

            if (!($scope.selectedPODetails.TOTAL_AMOUNT > 0)) {
                $scope.invAmountwithErrorMessage = 'Please Enter the Invoice Amount(With TAX).';
                $scope.errorMessage = $scope.invAmountwithErrorMessage;
            };


            if ($scope.selectedPODetails.TAX_AMOUNT < 0) {
                $scope.taxAmountErrorMessage = 'Please Enter the GST Tax Amount';
                $scope.errorMessage = $scope.taxAmountErrorMessage;
            };

            if ($scope.selectedPODetails.MULTIPLE_ATTACHMENTS.length <= 0) {
                $scope.attachmentsErrorMessage = 'Please Upload Invoice Attachments.';
                $scope.errorMessage = $scope.attachmentsErrorMessage;
            };
           
            if ($scope.selectedPODetails.INVOICE_AMOUNT) {
                if ((parseInt($scope.selectedPODetails.INVOICE_AMOUNT)) > parseInt($scope.selectedPODetails.TOTAL_AMOUNT)) {
                    growlService.growl("Invoice Amount(without Tax) Shouldn't be greater than Invoice Amount(with Tax)", 'inverse')
                    return;
                }
            }
            if ($scope.selectedPODetails.TAX_AMOUNT) {
                if (parseInt($scope.selectedPODetails.TAX_AMOUNT) > parseInt($scope.selectedPODetails.TOTAL_AMOUNT)) {
                    growlService.growl("Tax Amount Shouldn't be greater than Invoice Amount(with Tax)", 'inverse')
                    return;
                }

                if (parseInt($scope.selectedPODetails.TAX_AMOUNT) > parseInt($scope.selectedPODetails.INVOICE_AMOUNT)) {
                    growlService.growl("Tax Amount Shouldn't be greater than Invoice Amount(without Tax)", 'inverse')
                    return;
                }
              
            }

            if ($scope.selectedPODetails.INVOICE_DATE) {
                $scope.selectedPODetails.INVOICE_DATE_TEMP = $scope.selectedPODetails.INVOICE_DATE;
                var ts = userService.toUTCTicks($scope.selectedPODetails.INVOICE_DATE_TEMP);
                var m = moment(ts);
                var quotationDate = new Date(m);
                var milliseconds = parseInt(quotationDate.getTime() / 1000.0);
                $scope.selectedPODetails.INVOICE_DATE_TEMP = "/Date(" + milliseconds + "000+0530)/";
            }

            if ($scope.errorMessage) {
                return;
            }

            $scope.invoiceDetArr = [];
           
            $scope.invoiceDetObj = {
                "ROW_ID": $scope.selectedPODetails.ROW_ID,
                "INVOICE_ID": $scope.INVOICE_ID <= 0 ? 0 : $scope.INVOICE_ID,
                "BILL_ID": $scope.BILL_ID,
                "PO_NUMBER": $scope.selectedPODetails.PO_NUMBER,
                "PROJECT_ID": $scope.selectedPODetails.PROJECT_ID,
                "VENDOR_ID": $scope.selectedPODetails.VENDOR_ID,
                "VENDOR_CODE": $scope.selectedPODetails.VENDOR_CODE,
                "INVOICE_NUMBER": $scope.selectedPODetails.INVOICE_NUMBER,
                "INVOICE_DESCRIPTION": $scope.selectedPODetails.INVOICE_DESCRIPTION,
                "INVOICE_DATE": $scope.selectedPODetails.INVOICE_DATE_TEMP,
                "BILL_CERTIFIED_AMOUNT": $scope.selectedPODetails.BILL_CERTIFIED_AMOUNT,
                "INVOICE_AMOUNT": $scope.selectedPODetails.INVOICE_AMOUNT,
                "TOTAL_AMOUNT": $scope.selectedPODetails.TOTAL_AMOUNT,
                "CREATED_BY": $scope.USER_ID,
                "V_COMP_ID": $scope.COMP_ID,
                "C_COMP_ID": $scope.customerCompanyId,
                "TAX_AMOUNT": $scope.selectedPODetails.TAX_AMOUNT,
                "MULTIPLE_ATTACHMENTS": JSON.stringify($scope.selectedPODetails.MULTIPLE_ATTACHMENTS)
            }

            $scope.invoiceDetArr.push($scope.invoiceDetObj);

            $scope.params =
            {
                "invoiceDetailsList": $scope.invoiceDetArr,
                "invocieId": $scope.INVOICE_ID <= 0 ? 0 : $scope.INVOICE_ID,
                "sessionid": $scope.sessionID
            };

            PRMPOService.saveInvoiceDetails($scope.params)
                .then(function (response) {

                    if (response.errorMessage != '') {
                        growlService.growl(response.errorMessage, "inverse");
                    } else {
                        growlService.growl("Invoice Created Successfully.", "success");
                        $state.go('listInvoices');
                    }
                });

        };


        /*region start WORKFLOW*/

        $scope.getWorkflows = function () {
            workflowService.getWorkflowList($scope.customerCompanyId)
                .then(function (response) {
                    $scope.workflowList = [];
                    $scope.workflowListDeptWise = [];
                    $scope.workflowListSubPackage = [];
                    $scope.workflowListTemp = response;

                    $scope.workflowListTemp.forEach(function (item, index) {
                        if (item.WorkflowModule === $scope.WorkflowModule) {
                            $scope.workflowList.push(item);
                            $scope.workflowListDeptWise.push(item);
                        }
                    });

                    if ($scope.isSuperUser) {
                        $scope.workflowList = $scope.workflowList;
                    }
                    else {
                        $scope.workflowList = [];
                        $scope.workflowListDeptWise.forEach(function (wf, idx) {
                            $scope.deptIDs.forEach(function (dep) {
                                if (dep == wf.deptID) {
                                    $scope.workflowList.push(wf);
                                }
                            });
                        });

                    }
                });
        };

        $scope.getWorkflows();

        $scope.getItemWorkflow = function (ID, workflowModule) {
            workflowService.getItemWorkflow(0, ID, workflowModule)
                .then(function (response) {
                    $scope.itemWorkflow = response;
                    $scope.checkIsFormDisable();
                    if ($scope.itemWorkflow && $scope.itemWorkflow.length > 0 && $scope.itemWorkflow[0].WorkflowTracks.length > 0) {
                        $scope.currentStep = 0;

                        var count = 0;

                        $scope.itemWorkflow[0].WorkflowTracks.forEach(function (track) {
                            if (!track.multipleAttachments) {
                                track.multipleAttachments = [];
                            }

                            if (track.status === 'APPROVED') { track.statusNew = 'Approved'; }
                            if (track.status === 'HOLD') { track.statusNew = 'Hold'; }
                            if (track.status === 'PENDING') { track.statusNew = 'Pending'; }
                            if (track.status === 'REJECTED') { track.statusNew = 'Rejected'; }

                            if (track.status === 'APPROVED' || track.status === 'HOLD') {
                                $scope.isFormdisabled = true;
                            }

                            if (track.status === 'APPROVED') {
                                $scope.isWorkflowCompleted = true;
                                $scope.orderInfo = track.order;
                                $scope.assignToShow = track.status;

                            }
                            else {
                                $scope.isWorkflowCompleted = false;
                            }

                            if (track.status === 'REJECTED' && count == 0) {
                                count = count + 1;
                            }

                            if ((track.status === 'PENDING' || track.status === 'HOLD') && count == 0) {
                                count = count + 1;
                                //$scope.IsUserApproverForStage(track.approverID);
                                $scope.currentAccess = track.order;
                            }

                            if ((track.status === 'PENDING' || track.status === 'HOLD' || track.status === 'REJECTED') && $scope.currentStep === 0) {
                                $scope.currentStep = track.order;
                                return false;
                            }
                        });
                    }
                });
        };


        $scope.isApproverDisable = function (index) {

            var disable = true;

            var previousStep = {};

            $scope.itemWorkflow[0].WorkflowTracks.forEach(function (step, stepIndex) {

                if (index == stepIndex) {
                    if (stepIndex == 0) {
                        if ($scope.isUserBelongsToDeptandDesig(step.department.deptID, step.approver.desigID) &&
                            (step.status === 'PENDING' || step.status === 'HOLD')) {
                            disable = false;
                        }
                        else {
                            disable = true;
                        }
                    }
                    else if (stepIndex > 0) {
                        if (previousStep.status === 'PENDING' || previousStep.status === 'HOLD' || previousStep.status === 'REJECTED') {
                            disable = true;
                        }
                        else if ($scope.isUserBelongsToDeptandDesig(step.department.deptID, step.approver.desigID) &&
                            (step.status === 'PENDING' || step.status === 'HOLD')) {
                            disable = false;
                        }
                        else {
                            disable = true;
                        }
                    }
                }
                previousStep = step;
            });

            return disable;
        };

        $scope.isUserBelongsToDeptandDesig = function (deptID, desigID) {
            var isEligible = true;

            if ($scope.deptIDs.indexOf(deptID) != -1 && $scope.desigIDs.indexOf(desigID) != -1) {
                isEligible = true;
            } else {
                isEligible = false;
            }

            return isEligible;
        };

        $scope.updateTrack = function (step, status, type) {
            $scope.disableAssignPR = true;
            $scope.commentsError = '';
            if (step.comments != null || step.comments != "" || step.comments != undefined) {
                step.comments = validateStringWithoutSpecialCharacters(step.comments);
            }
            var tempArray = $scope.itemWorkflow[0].WorkflowTracks[$scope.itemWorkflow[0].WorkflowTracks.length - 1];
            if (step.order == tempArray.order && status === 'APPROVED') {
                $scope.disableAssignPR = false;
            } else {
                $scope.disableAssignPR = true;
            }

            if ($scope.isReject) {
                $scope.commentsError = 'Please Save Rejected Items/Qty';
                return false;
            }

            if (status === 'REJECTED' && (step.comments == null || step.comments == "")) {
                $scope.commentsError = 'Please enter comments';
                return false;
            }

            step.status = status;
            step.sessionID = $scope.sessionID;
            step.modifiedBy = +userService.getUserId();

            step.moduleName = $scope.WorkflowModule;

            step.subModuleName = '';
            step.subModuleID = 0;

            workflowService.SaveWorkflowTrack(step)
                .then(function (response) {
                    if (response.errorMessage) {
                        growlService.growl(response.errorMessage, "inverse");
                    }
                    else {
                        $scope.getItemWorkflow();
                        location.reload();
                    }
                });
        };

        $scope.showBillRequestApprovalPopUp = true;

        $scope.showBillRequestApproval = function () {
            $scope.showBillRequestApprovalPopUp = false;
        };

        $scope.isFormdisabled = true;

        $scope.checkIsFormDisable = function () {
            $scope.isFormdisabled = false;
            //if ($scope.itemWorkflow.length == 0) {
            //    $scope.isFormdisabled = true;
            //} else {
            //    if (($scope.BudgetList.CREATED_BY == +userService.getUserId() || $scope.BudgetList.MODIFIED_BY == +userService.getUserId()) && $scope.itemWorkflow[0].WorkflowTracks.length > 0 &&
            //        $scope.itemWorkflow[0].WorkflowTracks[0].status !== "APPROVED" && $scope.itemWorkflow[0].WorkflowTracks[0].order == 1 && $scope.itemWorkflow[0].workflowID > 0) {
            //        $scope.isFormdisabled = true;
            //    }
            //}
        };

        function validateStringWithoutSpecialCharacters(string) {
            if (string) {
                string = string.replace(/\'/gi, "");
                string = string.replace(/\"/gi, "");
                string = string.replace(/[`^_|\?;:'",<>\{\}\[\]\\\/]/gi, "");
                string = string.replace(/(\r\n|\n|\r)/gm, "");
                string = string.replace(/\t/g, '');
                return string;
            }
        }

        $scope.showApprovedDate = function (date) {
            return userService.toLocalDate(date);
        };

        $scope.cancel = function () {
            if ($scope.INVOICE_ID > 0) {
                $state.go('listInvoices');
            } else {
                $state.go('billRequestsList');
            }

        };
        $scope.getDate = function (value) {
            if (value) {
                return userService.toLocalDateOnly(value);
            }
        };

        $scope.updateTotalAmount = function (val1, val2) {
            val1 = +val1;
            val2 = +val2;
            if (val1) {
                $scope.selectedPODetails.TOTAL_AMOUNT = val1 + val2;
            }
        };


        //$scope.GetInvoiceAmountForPO = function () {


        //    var params = {
        //        "U_ID": $scope.USER_ID,
        //        "COMP_ID": $scope.COMP_ID,
        //        "PO_NUMBER": $scope.PO_NUMBER,
        //        "sessionid": $scope.sessionID
                
        //    }
        //    PRMPOService.GetInvoiceAmountForPO(params)
        //        .then(function (response) {
        //            $scope.TotalInvoice = JSON.parse(response).Table;
        //            if ($scope.TotalInvoice[0].INVOICE_AMOUNT == $scope.TotalPO.PO_VALUE) {
        //                growlService.growl("Invoice Amount Exceeds the PO Value for This PO Number", "inverse")
        //                $scope.go("billRequestsList");
        //            }
        //        });

        //}
        //$scope.GetInvoiceAmountForPO();

    }]);