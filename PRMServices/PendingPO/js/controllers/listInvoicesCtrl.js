
prmApp.controller('listInvoicesCtrl', ["$scope", "$stateParams", "$log", "$state", "$window", "userService", "auctionsService", "storeService", "growlService", "PRMProjectServices", "$filter", "fileReader", "$rootScope", "PRMPOService", "workflowService",
    function ($scope, $stateParams, $log, $state, $window, userService, auctionsService, storeService, growlService, PRMProjectServices, $filter, fileReader, $rootScope, PRMPOService, workflowService) {

        $scope.USER_ID = +userService.getUserId();
        $scope.COMP_ID = +userService.getUserCompanyId();
        $scope.customerCompanyId = +userService.getCustomerCompanyId();
        $scope.sessionID = userService.getUserToken();
        $scope.isSuperUser = userService.getUserObj().isSuperUser;
        $scope.isVendor = (userService.getUserType() == "VENDOR") ? true : false;
        /*PAGINATION CODE*/
        $scope.totalItems = 0;
        $scope.currentPage = 1;
        $scope.itemsPerPage = 10;
        $scope.maxSize = 5;
        $scope.setPage = function (pageNo) {
            $scope.currentPage = pageNo;
            $scope.getInvoicesList(($scope.currentPage - 1), 10, '');
        };

        $scope.filters =
        {
            poNumbers: {},
            projects: {},
            invoiceNumbers: {},
            invoiceStatus: {},
            
        };
        $scope.filters = {
            poNumbers: [],
            projects: [],
            invoiceNumbers: [],
            invoiceStatus: [],           
        };

        $scope.filters.invoiceToDate = moment().format('YYYY-MM-DD');
        $scope.filters.invoiceFromDate = moment().subtract(30, "days").format("YYYY-MM-DD");
        $scope.filtersList = [];

        $scope.setFilters = function () {
            if ($scope.loadServices) {
                $scope.getInvoicesList(0, 10);
            }
        };

        $scope.pageChanged = function () {
            ////console.log('Page changed to: ' + $scope.currentPage);
        };

        $scope.filterByDate = function () {
            $scope.getInvoicesList(0, 10);
            $scope.getInvoiceFilterValues();
        };
        
        $scope.getInvoicesList = function (recordsFetchFrom, pageSize) {

            var invoiceFromDate, invoiceToDate, poNumbers, projectIds, invoiceNumbers, invoiceStatus;


            if (_.isEmpty($scope.filters.invoiceFromDate)) {
                invoiceFromDate = '';
            } else {
                invoiceFromDate = $scope.filters.invoiceFromDate;
            }

            if (_.isEmpty($scope.filters.invoiceToDate)) {
                invoiceToDate = '';
            } else {
                invoiceToDate = $scope.filters.invoiceToDate;
            }

            if (_.isEmpty($scope.filters.poNumbers)) {
                poNumbers = '';
            } else if ($scope.filters.poNumbers && $scope.filters.poNumbers.length > 0) {
                let temp = _($scope.filters.poNumbers)
                    .filter(item => item.id)
                    .map('id')
                    .value();
                poNumbers = temp.join(',');
            }

            if (_.isEmpty($scope.filters.projects)) {
                projectIds = '';
            } else if ($scope.filters.projects && $scope.filters.projects.length > 0) {
                let temp = _($scope.filters.projects)
                    .filter(item => item.id)
                    .map('id')
                    .value();
                projectIds = temp.join(',');
            }

            if (_.isEmpty($scope.filters.invoiceNumbers)) {
                invoiceNumbers = '';
            } else if ($scope.filters.invoiceNumbers && $scope.filters.invoiceNumbers.length > 0) {
                let temp = _($scope.filters.invoiceNumbers)
                    .filter(item => item.name)
                    .map('name')
                    .value();
                invoiceNumbers = temp.join(',');
            }

            if (_.isEmpty($scope.filters.invoiceStatus)) {
                invoiceStatus = '';
            } else if ($scope.filters.invoiceStatus && $scope.filters.invoiceStatus.length > 0) {
                let temp = _($scope.filters.invoiceStatus)
                    .filter(item => item.name)
                    .map('name')
                    .value();
                invoiceStatus = temp.join(',');
            }

            $scope.params =
            {
                "COMP_ID": $scope.COMP_ID,
                "U_ID": $scope.USER_ID,
                "IS_CUSTOMER": ($scope.isVendor ? 0 : 1),
                "fromDate": invoiceFromDate,
                "toDate": invoiceToDate,
                "poNumber": poNumbers,
                "projectIds": projectIds,
                "invoiceNumbers": invoiceNumbers,
                "invoiceStatus": invoiceStatus,
                "sessionid": $scope.sessionID,
                "PageSize": recordsFetchFrom * pageSize,
                "NumberOfRecords": pageSize
            };

            PRMPOService.getInvoicesList($scope.params)
                .then(function (response) {
                    $scope.loadServices = true;

                    if (response) {
                        $scope.invoicesList = JSON.parse(response).Table;
                        $scope.totalItems = $scope.invoicesList[0].TOTAL_COUNT;
                        $scope.invoicesList.forEach(a => delete a.TOTAL_COUNT);

                        $scope.invoicesList.forEach(function (item, index) {

                            item.DESCRIPTION = item.INVOICE_DESCRIPTION;
                            item.INVOICE_DATE = userService.toLocalDate(item.INVOICE_DATE).split(' ')[0];
                            item.DATE_CREATED = moment(item.DATE_CREATED).format("YYYY-MM-DD");

                        });
                    } else {
                        $scope.invoicesList = [];
                    }
                });
        };


        $scope.getInvoiceFilterValues = function () {
            $scope.params =
            {
                "COMP_ID": $scope.COMP_ID,
                "U_ID": $scope.USER_ID,
                "IS_CUSTOMER": ($scope.isVendor ? 0 : 1),
                "fromDate": $scope.filters.invoiceFromDate,
                "toDate": $scope.filters.invoiceToDate,
                "sessionid": $scope.sessionID
            };

            let poNumbersTemp = [];
            let projectsTemp = [];
            let invoicesTemp = [];
            let invoiceStatusTemp = [];
            

            PRMPOService.getInvoiceFilterValues($scope.params)
                .then(function (response) {
                    $scope.loadServices = false;
                    $scope.getInvoicesList(0, 10);
                    if (response) {
                        $scope.filterValues = JSON.parse(response).Table;

                        $scope.filterValues.forEach(function (item, index) {
                            if (item.TYPE === 'PO_NUMBER') {
                                poNumbersTemp.push({ id: item.ID, name: item.NAME });
                            } else if (item.TYPE === 'PROJECT_NAME') {
                                projectsTemp.push({ id: item.ID, name: item.NAME });
                            } else if (item.TYPE === 'INVOICE_NUMBER') {
                                invoicesTemp.push({ id: item.ID, name: item.NAME });
                            } else if (item.TYPE === 'INVOICE_STATUS') {
                                invoiceStatusTemp.push({ id: item.ID, name: item.NAME });
                            } 
                        });

                        $scope.filtersList.poNumberList = poNumbersTemp;
                        $scope.filtersList.projectsList = projectsTemp;
                        $scope.filtersList.invoiceList = invoicesTemp;
                        $scope.filtersList.invoiceStatusList = invoiceStatusTemp;
                       
                    }

                });
        };

        //$scope.getBillRequests(0,10);
        $scope.getInvoiceFilterValues();


        $scope.goToCreateInvoice = function (detailsObj, flag) {

            if (flag === 'EDIT') {
                var url = $state.href("editInvoices", { "PO_NUMBER": detailsObj.PO_NUMBER, "BILL_ID": detailsObj.BILL_ID, "INVOICE_ID": detailsObj.INVOICE_ID });
                window.open(url, '_blank');
            } else if (flag === 'VIEW') {
                var url = $state.href("viewInvoices", { "PO_NUMBER": detailsObj.PO_NUMBER, "BILL_ID": detailsObj.BILL_ID, "INVOICE_ID": detailsObj.INVOICE_ID });
                window.open(url, '_blank');
            }
        };

       
    }]);