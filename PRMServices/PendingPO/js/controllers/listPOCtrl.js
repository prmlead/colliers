﻿prmApp
    .controller('listPOCtrl', ["$rootScope", "$scope", "$stateParams", "$log", "$state", "$window", "userService", "auctionsService", "growlService", "PRMPOService",
        "fileReader", "$uibModal", "$filter", "PRMProjectServices", 
        function ($rootScope, $scope, $stateParams, $log, $state, $window, userService, auctionsService, growlService, PRMPOService, fileReader, $uibModal, $filter, PRMProjectServices) {

            $scope.userId = userService.getUserId();
            $scope.sessionId = userService.getUserToken();
            $scope.compId = userService.getUserCompanyId();
            $scope.isCustomer = userService.getUserType() === "CUSTOMER" ? true : false;
            $scope.isSuperUser = userService.getUserObj().isSuperUser;
            $scope.selectedPODetails = null;
            $scope.selectedPOItemDetails = null;

            /*PAGINATION CODE*/
            $scope.totalItems = 0;
            $scope.currentPage = 1;
            $scope.itemsPerPage = 10;
            $scope.maxSize = 5;
            $scope.filters = {
                projects: [],
                packages: [],
                costCenters: [],
                vendors: []
            };
            $scope.filters = {
                searchKeyword: ''
            }

            $scope.filtersList = [];
            $scope.setPage = function (pageNo) {
                $scope.currentPage = pageNo;
                $scope.getpendingPOlist(($scope.currentPage - 1), 10, $scope.filters.searchKeyword);
            };

            $scope.pageChanged = function () {
                ////console.log('Page changed to: ' + $scope.currentPage);
            };

            /*PAGINATION CODE*/


            $scope.filters.pendingPOToDate = moment().format('YYYY-MM-DD');
            $scope.filters.pendingPOFromDate = moment().subtract(30, "days").format("YYYY-MM-DD");

            $scope.headerRows = [];
            $scope.pendingPOList = [];
            $scope.filteredPendingPOsList = [];
            
            $('.selected-items-box').bind('click', function (e) {
                $('.multiple-selection-dropdown .list').slideToggle('fast');
            });

            $scope.filterByDate = function () {
                $scope.getpendingPOlist(0, 10, $scope.filters.searchKeyword);
            };


            $scope.formdetails = {
                attachmentLists: [],
            };


            $scope.totalAttachmentMaxSize = 6291456;
            $scope.totalRequirementSize = 0;

            $scope.getFile1 = function (id, itemid, ext) {
                $scope.attachmentError = undefined;
                $scope.totalRequirementItemSize = 0;
                $scope.totalRequirementSize = 0;
                $scope.filesTemp = $("#" + id)[0].files;
                $scope.filesTemp = Object.values($scope.filesTemp);

                if ($scope.filesTemp && $scope.filesTemp.length > 5) {
                    swal({
                        title: "Attachment size!",
                        text: "You can attach maximum 5 files",
                        type: "warning",
                        showCancelButton: false,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Ok",
                        closeOnConfirm: true
                    },
                        function () {
                            $scope.filesTemp = [];
                            return;
                        });
                    return;
                }

                if ($scope.filesTemp && $scope.filesTemp.length > 0) {
                    $scope.filesTemp.forEach(function (item, index) {
                        $scope.totalRequirementSize = $scope.totalRequirementSize + item.size;
                    });
                }

                if (($scope.totalRequirementSize + $scope.totalRequirementItemSize) > $scope.totalAttachmentMaxSize) {
                    swal({
                        title: "Attachment size!",
                        text: "Total Attachments size cannot exceed 6MB",
                        type: "warning",
                        showCancelButton: false,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Ok",
                        closeOnConfirm: true
                    },
                        function () {

                            return;
                        });
                    return;
                }
                if (id == "attachement") {
                    $scope.filesTemp.forEach(function (attach, attachIndex) {
                        $scope.file = $("#" + id)[0].files[attachIndex];
                        $scope.progress = 0;
                        $scope.filesTemp.forEach(function (attach, attachIndex) {
                            $scope.file = $("#" + id)[0].files[attachIndex];
                            fileReader.readAsDataUrl($scope.file, $scope)
                                .then(function (result) {
                                    var fileUpload = {
                                        fileStream: [],
                                        fileName: '',
                                        fileID: 0
                                    };
                                    var bytearray = new Uint8Array(result);
                                    fileUpload.fileStream = $.makeArray(bytearray);
                                    fileUpload.fileName = attach.name;
                                    if (!$scope.formdetails.attachmentLists) {
                                        $scope.formdetails.attachmentLists = [];
                                    }
                                    var ifExists = _.findIndex($scope.formdetails.attachmentLists, function (attach) { return attach.fileName.toLowerCase() === fileUpload.fileName.toLowerCase() });
                                    if (ifExists < 0) {
                                        $scope.formdetails.attachmentLists.push(fileUpload);
                                    }
                                });
                        });

                    });
                }
            };


            $scope.removeAttach = function (index, type) {
                if (type == 'Attach') {
                    $scope.formdetails.attachmentLists.splice(index, 1);
                } 
                angular.forEach(
                    angular.element("input[type='file']"),
                    function (inputElem) {
                        angular.element(inputElem).val(null);
                    });
            };


            $scope.getpendingPOlist = function (recordsFetchFrom, pageSize, searchKeyword) {
                let pendingPOFromDate, pendingPOToDate, projectIds, packageIds, costCenters, vendorIds;

                if (_.isEmpty($scope.filters.pendingPOFromDate)) {
                    pendingPOFromDate = '';
                } else {
                    pendingPOFromDate = $scope.filters.pendingPOFromDate;
                }

                if (_.isEmpty($scope.filters.pendingPOToDate)) {
                    pendingPOToDate = '';
                } else {
                    pendingPOToDate = $scope.filters.pendingPOToDate;
                }

                if (_.isEmpty($scope.filters.projects)) {
                    projectIds = '';
                } else if ($scope.filters.projects && $scope.filters.projects.length > 0) {
                    let temp = _($scope.filters.projects)
                        .filter(item => item.id)
                        .map('id')
                        .value();
                    projectIds = temp.join(',');
                }

                if (_.isEmpty($scope.filters.packages)) {
                    packageIds = '';
                } else if ($scope.filters.packages && $scope.filters.packages.length > 0) {
                    let temp = _($scope.filters.packages)
                        .filter(item => item.id)
                        .map('id')
                        .value();
                    packageIds = temp.join(',');
                }

                if (_.isEmpty($scope.filters.costCenters)) {
                    costCenters = '';
                } else if ($scope.filters.costCenters && $scope.filters.costCenters.length > 0) {
                    let temp = _($scope.filters.costCenters)
                        .filter(item => item.id)
                        .map('id')
                        .value();
                    costCenters = temp.join(',');
                }

                if (_.isEmpty($scope.filters.vendors)) {
                    vendorIds = '';
                } else if ($scope.filters.vendors && $scope.filters.vendors.length > 0) {
                    let temp = _($scope.filters.vendors)
                        .filter(item => item.id)
                        .map('id')
                        .value();
                    vendorIds = temp.join(',');
                }


                var params = {
                    "compId": $scope.isCustomer ? $scope.compId : 0,
                    "vendorId": $scope.isCustomer ? 0 : $scope.userId,
                    "fromdate": $scope.filters.pendingPOFromDate,
                    "todate": $scope.filters.pendingPOToDate,
                    "projectId": 0,
                    "poNumber": '',
                    "projectIds": projectIds,
                    "packageIds": packageIds,
                    "costCenters": costCenters,
                    "vendorIds": vendorIds,
                    "searchKeyword": searchKeyword,
                    "page": recordsFetchFrom * pageSize,
                    "size": pageSize,
                    "sessionId": $scope.sessionId
                };

                $scope.pageSizeTemp = (params.page + 1);
                $scope.NumberOfRecords = ((recordsFetchFrom + 1) * pageSize);

                PRMPOService.getPOList(params)
                    .then(function (response) {
                       
                        if (response && response.length > 0) {
                            $scope.headerRows = [];
                            response = _.orderBy(response, ['DELIVERY_DATE'], ['desc']);
                            response.forEach(function (item, index) {
                                item.isDisabled = false;
                                item.packageSelected = false;
                                item.DELIVERY_DATE = item.DELIVERY_DATE ? moment(item.DELIVERY_DATE).format("DD-MM-YYYY") : '-';
                                item.TOTAL_AMOUNT_INCLUSIVE_TAX = item.TOTAL_AMOUNT + item.TAX_AMOUNT;
                                item.PO_STATUS_TEMP = item.PO_STATUS;
                            });

                            $scope.pendingPOList = [];
                            $scope.filteredPendingPOsList = [];
                            if (response && response.length > 0) {
                                response.forEach(function (item, index) {
                                    $scope.pendingPOList.push(item);
                                });
                            }

                            let poValues = [];
                            poValues = _($scope.pendingPOList).groupBy('PO_NUMBER')
                                .map((objs, key) => ({
                                    'PO_NUMBER': key,
                                    'TOTAL_VALUE': _.sumBy(objs, 'TOTAL_AMOUNT_INCLUSIVE_TAX')
                                })).value();


                            $scope.pendingPOList.forEach(function (poItem, poItemIndex) {
                                poValues.forEach(function (groupedItem, groupedItemIndex) {
                                    if (groupedItem.PO_NUMBER === poItem.PO_NUMBER) {
                                        poItem.ALL_ITEMS_AMOUNT = groupedItem.TOTAL_VALUE;
                                    }
                                })
                            });


                            if ($scope.pendingPOList && $scope.pendingPOList.length > 0) {
                                $scope.totalItems = $scope.pendingPOList[0].TOTAL_COUNT;
                                $scope.filteredPendingPOsList = $scope.pendingPOList;

                                $scope.headerRows = JSON.stringify(_.uniqWith(
                                    $scope.filteredPendingPOsList,
                                    (itemA, itemB) =>
                                        itemA.PO_NUMBER === itemB.PO_NUMBER
                                ));

                                $scope.headerRows = JSON.parse($scope.headerRows);
                            }
                        } else {
                            $scope.headerRows = [];
                        }
                    });
            };

            $scope.getpendingPOlist(0, 10, $scope.filters.searchKeyword);
            $scope.amendTemp = [];

            $scope.getPendingPOItems = function (poDetails) {
                if (poDetails) {
                    let poItems = $scope.filteredPendingPOsList.filter(function (item) {
                        return item.PO_NUMBER === poDetails.PO_NUMBER;
                    });

                    poDetails.poItems = poItems;
                    $scope.amendTemp = poDetails.poItems;
                }
            };

            $scope.goToPoAmendment = function (poObj) {

                if (poObj)
                {
                    if (poObj.PO_STATUS === 'COMPLETE')
                    {
                        poObj.PO_AMENDMENT_ID = 0;
                    }
                    $state.go("poAmendment", { "poID": poObj.PO_NUMBER, "PO_AMENDMENT_ID": poObj.PO_AMENDMENT_ID });
                }
            }

            $scope.getFilterValues = function () {
                var params =
                {
                    "compid": $scope.isCustomer ? $scope.compId : 0
                };

                let projectsTemp = [];
                let packagesTemp = [];
                let costCentersTemp = [];
                let vendorsTemp = [];

                PRMPOService.getPOFilterValues(params)
                    .then(function (response) {
                        if (response && response.length > 0) {
                            $scope.filterValues = response;
                            $scope.filterValues.forEach(function (item, index) {
                                if (item.name === 'PACKAGE') {
                                    item.arrayPair.forEach(function (item) {
                                        packagesTemp.push({ id: item.key, name: item.value });
                                    });
                                } else if (item.name === 'PROJECT') {
                                    item.arrayPair.forEach(function (item) {
                                        projectsTemp.push({ id: item.key, name: item.value });
                                    });
                                } else if (item.name === 'COST_CENTER') {
                                    item.arrayPair.forEach(function (item) {
                                        costCentersTemp.push({ id: item.key1, name: item.value });
                                    });
                                } else if (item.name === 'VENDORS') {
                                    item.arrayPair.forEach(function (item) {
                                        vendorsTemp.push({ id: item.key, name: item.value });
                                    });
                                }
                            });

                            $scope.filtersList.packagesList = packagesTemp;
                            $scope.filtersList.projectsList = projectsTemp;
                            $scope.filtersList.costCenterList = costCentersTemp;
                            $scope.filtersList.vendorList = vendorsTemp;
                        }
                    });
            };

            $scope.getFilterValues();

            $scope.viewPODetails = function (poObj) {
                if (poObj) {
                    $state.go('view-po-details', { 'poNumber': poObj.PO_NUMBER });
                }
            };

            $scope.generatePOPDF = function (poObj) {
                if (poObj) {
                    $state.go('pociPDF', { 'poNumber': poObj.PO_NUMBER });
                }
            };

            $scope.selectPOObject = function (poObj) {
                if (poObj) {
                    $scope.selectedPODetails = poObj;
                }
            };

            $scope.selectPOItemObject = function (poItemObj) {
                if (poItemObj) {
                    $scope.selectedPOItemDetails = poItemObj;
                }
            };

            $scope.resetPOStatus = function () {
                $scope.selectedPODetails.PO_STATUS_TEMP = $scope.selectedPODetails.PO_STATUS
                angular.element('#poEditSelection').modal('hide');
            };
            $scope.savePODetails = function () {
                if ($scope.selectedPODetails &&
                    $scope.selectedPODetails.DELIVERY_DATE && $scope.selectedPODetails.PO_TYPE
                    && $scope.selectedPODetails.PO_STATUS_TEMP) {
                    $scope.attachmentError = '';
                    if ($scope.formdetails.attachmentLists.length < 1 && $scope.selectedPODetails.NOTIFY_VENDOR==1) {
                        $scope.attachmentError = 'Please attach signed PO copy & other supporting documents.';
                        return;
                    }

                    let ts = moment($scope.selectedPODetails.DELIVERY_DATE, "DD-MM-YYYY").valueOf();
                    let m = moment(ts);
                    let DELIVERY_DATE = new Date(m);
                    var milliseconds = parseInt(DELIVERY_DATE.getTime() / 1000.0);
                    let finalDate = "/Date(" + milliseconds + "000+0530)/";

                    let params = {
                        item: {
                            'PO_TYPE': $scope.selectedPODetails.PO_TYPE,
                            'PO_STATUS': $scope.selectedPODetails.PO_STATUS_TEMP,
                            'PO_NUMBER': $scope.selectedPODetails.PO_NUMBER,
                            'NOTIFY_VENDOR': $scope.selectedPODetails.NOTIFY_VENDOR ? 1 : 0,
                            'PROJECT_ID': $scope.selectedPODetails.PROJECT_ID,
                            'VENDOR_ID': $scope.selectedPODetails.VENDOR_ID,
                            'DELIVERY_DATE': finalDate,
                            'CUSTOMER_USER_ID': $scope.userId,
                            'sessionID': $scope.sessionId,
                            'vendorAttachment': $scope.formdetails.attachmentLists,

                        },
                        sessionId: $scope.sessionId,
                        //'vendorAttachment': $scope.formdetails.attachmentLists,

                    };

                    PRMPOService.editPODetails(params)
                        .then(function (response) {
                            angular.element('#poEditSelection').modal('hide');
                            if (response.objectID) {
                                let notifyVendor = $scope.selectedPODetails.NOTIFY_VENDOR ? 1 : 0;
                                if (notifyVendor) {
                                    swal("Success!", "Successfully saved & notified the vendor: " + $scope.selectedPODetails.VENDOR_COMPANY, "success");
                                    location.reload();
                                } else {
                                    swal("Success!", "Successfully saved.", "success");
                                    location.reload();
                                }
                            } else {
                                swal("Error!", "Error saving details, please contact support.", "error");
                            };

                            $scope.selectedPODetails = null;
                        });
                } else {
                    swal("Warning!", "Please enter all required fields to continue.", "warning");
                }
            };

            $scope.savePOItemDetails = function () {
                if ($scope.selectedPOItemDetails &&
                    $scope.selectedPOItemDetails.PO_DESCRIPTION) {

                    let params = {
                        item: {
                            'PO_NUMBER': $scope.selectedPOItemDetails.PO_NUMBER,
                            'PACKAGE_ID': $scope.selectedPOItemDetails.PACKAGE_ID,
                            'SUB_PACKAGE_ID': $scope.selectedPOItemDetails.SUB_PACKAGE_ID,
                            'PO_DESCRIPTION': $scope.selectedPOItemDetails.PO_DESCRIPTION,
                            'CUSTOMER_USER_ID': $scope.userId
                        },
                        sessionId: $scope.sessionId
                    };

                    PRMPOService.editPOItemDetails(params)
                        .then(function (response) {
                            $scope.selectedPOItemDetails
                            angular.element('#poItemEditSelection').modal('hide');
                            if (response.objectID) {
                                swal("Success!", "Successfully saved.", "success");
                            } else {
                                swal("Error!", "Error saving details, please contact support.", "error");
                            };
                        });
                } else {
                    swal("Warning!", "Please enter all required fields to continue.", "warning");
                }
            };



            $scope.AmendPODetails = function ()
            {
                $scope.AMEND_ARR = [];
                $scope.amendTemp.forEach(function (item, index) {
                    let newObj =
                    {
                        "PO_NUMBER": item.PO_NUMBER,
                        "PROJECT_ID": item.PROJECT_ID,
                        "PACKAGE_ID": item.PACKAGE_ID,
                        "SUB_PACKAGE_ID": item.SUB_PACKAGE_ID,
                        "UNIT_PRICE": item.UNIT_PRICE,
                        "CGST": item.CGST,
                        "SGST": item.SGST,
                        "IGST": item.IGST,
                        "ORDER_QTY": item.ORDER_QTY,
                    };
                    $scope.AMEND_ARR.push(newObj);
                });
                let params =
                {
                    "poAmendItemsDetails": $scope.AMEND_ARR,
                    sessionId: $scope.sessionId
                };

                PRMPOService.AmendPODetails(params)
                    .then(function (response) {
                        swal("Success!", "Successfully saved.", "success");
                    });

            };

            $scope.scrollWin = function (id, scrollPosition) {

                var elmnt = document.getElementById(id);
                elmnt.scrollIntoView();
                //document.getElementById(id).scrollIntoView();
                if (scrollPosition == 'BOTTOM') {
                    window.scroll({ bottom: elmnt.offsetBottom });
                }
                else {
                    window.scroll({ top: elmnt.offsetTop });
                }

            };


            $scope.createBillRequest = function (poDetails)
            {
                $state.go("createBillRequest", { "poNumber": poDetails.PO_NUMBER, "ID": 0});
            };

            $scope.isPackageSelectedFromSamePO = [];

            $scope.handlePackageSelect = function ()
            {
                $scope.filteredPendingPOsList.forEach(function (item, index) {
                    if (item.packageSelected) {
                        if ($scope.isPackageSelectedFromSamePO && $scope.isPackageSelectedFromSamePO <= 0) {
                            $scope.isPackageSelectedFromSamePO.push(item.PO_NUMBER);
                        }
                        if ($scope.isPackageSelectedFromSamePO.length > 0) {
                            var firstSelectedPONumber = $scope.isPackageSelectedFromSamePO[0];
                            if (firstSelectedPONumber !== item.PO_NUMBER) {
                                item.isDisabled = true;
                            }
                        }
                    } else {
                        if ($scope.isPackageSelectedFromSamePO && $scope.isPackageSelectedFromSamePO <= 0) {
                            $scope.isPackageSelectedFromSamePO.push(item.PO_NUMBER);
                        }
                        if ($scope.isPackageSelectedFromSamePO.length > 0) {
                            var firstSelectedPONumber = $scope.isPackageSelectedFromSamePO[0];
                            if (firstSelectedPONumber !== item.PO_NUMBER) {
                                item.isDisabled = true;
                            }
                        }
                    }
                });
            };

            $scope.getPoAudit = function (obj)
            {
                $state.go('poAudit', { "PO_NUMBER": obj.PO_NUMBER });
            }

        }]);