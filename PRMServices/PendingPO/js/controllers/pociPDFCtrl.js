﻿prmApp
    .controller('pociPDFCtrl', ["$rootScope", "$scope", "$stateParams", "$log", "$state", "$window", "userService", "auctionsService", "growlService",
        "PRMPOService", "$uibModal", "fileReader", "PRMProjectServices",
        function ($rootScope, $scope, $stateParams, $log, $state, $window, userService, auctionsService, growlService, PRMPOService, $uibModal, fileReader, PRMProjectServices) {

            $scope.userId = userService.getUserId();
            $scope.sessionId = userService.getUserToken();
            $scope.compId = userService.getUserCompanyId();
            $scope.isCustomer = userService.getUserType() === "CUSTOMER" ? true : false;
            $scope.PO_NUMBER = $stateParams.poNumber;
            $scope.termsConditions = [];
            $scope.poDetails = {};
            $scope.projectDetails = {};
            $scope.poDetailItems = [];
            $scope.isPrintView = false;
            $scope.PO_PDF_JSON = '';
            if (!$scope.PO_NUMBER) {
                $state.go('list-po-new');
            }

            $scope.getPODetails = function () {
                var params = {
                    "poNumber": $scope.PO_NUMBER,
                    "sessionId": $scope.sessionId
                };

                PRMPOService.getPODetails(params)
                    .then(function (response) {
                        $scope.PO_PDF_JSON = '';
                        $scope.poDetails = {};
                        $scope.poDetailItems = response;
                        $scope.poDetails.PO_VALUE = 0;
                        $scope.poDetails.PO_VALUE_WITHOUTTAX = 0;
                        $scope.poDetailItems.forEach(function (item, index) {
                            item.DELIVERY_DATE = item.DELIVERY_DATE ? moment(item.DELIVERY_DATE).format("DD-MM-YYYY") : '-';
                            $scope.poDetails.PO_VALUE += item.TOTAL_VALUE;
                            $scope.poDetails.PO_VALUE_WITHOUTTAX += item.TOTAL_AMOUNT;
                        });

                        let poValue = $scope.poDetails.PO_VALUE;

                        if (('' + $scope.poDetails.PO_VALUE).includes('.')) {
                            poValue = +('' + $scope.poDetails.PO_VALUE).split('.')[0];
                        }

                        $scope.PO_PDF_JSON = $scope.poDetailItems[0].PO_PDF_JSON;
                        $scope.poDetails.PO_STATUS = $scope.poDetailItems[0].PO_STATUS;
                        $scope.poDetails.PO_VALUE_WORDS = price_in_words(poValue);
                        $scope.poDetails.PO_NUMBER = $scope.poDetailItems[0].PO_NUMBER;
                        $scope.poDetails.VENDOR_COMPANY = $scope.poDetailItems[0].VENDOR_COMPANY;
                        $scope.poDetails.VENDOR_CODE = $scope.poDetailItems[0].VENDOR_CODE;
                        $scope.poDetails.VENDOR_PRIMARY_EMAIL = $scope.poDetailItems[0].VENDOR_PRIMARY_EMAIL;
                        $scope.poDetails.VENDOR_ADDRESS = $scope.poDetailItems[0].VENDOR_ADDRESS;
                        $scope.poDetails.GST_NUMBER = $scope.poDetailItems[0].VENDOR_GST_NUMBER;
                        $scope.poDetails.PROJECT_ID = $scope.poDetailItems[0].PROJECT_ID;
                        $scope.poDetails.PROJ_ID = $scope.poDetailItems[0].PROJ_ID;
                        $scope.poDetails.PROJECT_NAME = $scope.poDetailItems[0].PROJECT_NAME;
                        $scope.poDetails.CLIENT_NAME = $scope.poDetailItems[0].CLIENT_NAME;
                        $scope.poDetails.PO_TYPE = $scope.poDetailItems[0].PO_TYPE;
                        $scope.poDetails.PO_GST_PERC = $scope.poDetailItems[0].CGST + $scope.poDetailItems[0].SGST + $scope.poDetailItems[0].IGST;
                        $scope.poDetails.PO_GST_VALUE = _.sumBy($scope.poDetailItems, 'TAX_AMOUNT');//$scope.poDetails.PO_VALUE_WITHOUTTAX * (($scope.poDetailItems[0].CGST + $scope.poDetailItems[0].SGST + $scope.poDetailItems[0].IGST) / 100);
                        $scope.poDetails.PO_DATE = userService.toLocalDate($scope.poDetailItems[0].PO_DATE).split(' ')[0];
                        $scope.poDetails.INVOICE_TO_ADDRESS = 'COLLIERS INTERNATIONAL (INDIA) PROPERTY SERVICES PVT. LTD. Indiabulls Finance Centre, 17th Floor, unit No: 1701, Tower - 3, Senapati Bapat Marg, Elphinstone(W), Mumbai - 400013';
                        //$scope.poDetails.PO_TYPE_DESC = '';
                        //$scope.poDetails.CONTRACT_LEGAL_ENTITY = 'Colliers International (India) Property Services Private Limited';
                        //$scope.poDetails.CONTRACT_ADDR = 'First Floor, Sunning Dale, Embassy Golf Link Business Park, Bangalore - 560071.';

                        $scope.poDetails.CLIENT_ADDRESS = $scope.poDetailItems[0].ADDRESS;
                        $scope.poDetails.CLIENT_GST = $scope.poDetailItems[0].GST_NUMBER;
                        $scope.poDetails.CLIENT_PAN = $scope.poDetailItems[0].PAN_NUMBER;

                        $scope.getProjectDetails($scope.poDetails.PROJECT_ID);
                        $scope.poDetails.MAP_TERM_ID = $scope.poDetailItems[0].MAP_TERM_ID;
                        $scope.GetTermsandConditions();
                        if ($scope.poDetails.PO_TYPE === 'WORK_ORDER') {
                            $scope.poDetails.PO_TYPE_DESC = 'Work Order (Item Rate Contract)';
                        }

                        if ($scope.poDetails.PO_TYPE === 'LOCAL_SALES') {
                            $scope.poDetails.PO_TYPE_DESC = 'Purchase Order (Local Sales)';
                        }

                        if ($scope.poDetails.PO_TYPE === 'SERVICE_AGREEMENT') {
                            $scope.poDetails.PO_TYPE_DESC = 'Service Agreeement';
                        }

                        if ($scope.poDetails.PO_TYPE === 'IMPORT') {
                            $scope.poDetails.PO_TYPE_DESC = 'Purchase Order (Import)';
                        }
                    });
            };

            if ($scope.PO_NUMBER) {
                $scope.getPODetails();
            }            

           $scope.getProjectDetails = function (projectId) {
                var params =
                {
                    "PROJECT_ID": projectId,
                    "sessionid": $scope.sessionId
                };

                PRMProjectServices.GetProjectDetails(params)
                    .then(function (response) {
                        $scope.projectDetails = response;
                        var parsedAddress = JSON.parse($scope.projectDetails.BILL_TO_ADDRESS);
                        if (parsedAddress != null) {
                            $scope.projectDetails.BILL_TO_ADDRESS = parsedAddress.LEGAL_ENTITY_NAME + "\n" + parsedAddress.ADDRESS;
                            $scope.poDetails.CONTRACT_LEGAL_ENTITY = parsedAddress.LEGAL_ENTITY_NAME;
                            $scope.poDetails.CONTRACT_ADDR = parsedAddress.ADDRESS;
                            $scope.poDetails.CONTRACT_SPOC_EMAIL = parsedAddress.SPOC_EMAIL_ADDRESS;
                            $scope.poDetails.CONTRACT_GST = parsedAddress.GSTIN;
                            $scope.poDetails.CONTRACT_PAN = parsedAddress.PAN;
                        }
                                              
                    });
            };

            $scope.printPO = function () {

                var element = document.getElementById('container');
                html2pdf().set({
                    margin: 0,
                    image: { type: 'jpeg', quality: 0.98 },
                    html2canvas: { scale: 2 },
                    jsPDF: { unit: 'mm', format: 'a4', orientation: 'landscape' }
                }).from(element).toPdf().get('pdf').output('datauristring').then(function (pdfAsString) {
                    var arr = pdfAsString.split(',');
                    pdfAsString = arr[1];
                    let params =
                    {
                        base64: pdfAsString,
                        poNumber: $scope.poDetails.PO_NUMBER,
                        poType: $scope.poDetails.PO_TYPE,
                        compId: +$scope.compId,
                        sessionid: userService.getUserToken()
                    };
                    PRMPOService.updatePOPDF(params)
                        .then(function (response) {
                            if (response.errorMessage) {
                                swal("Error!", response.errorMessage, "warning");
                            } else {
                                var base64String = "data:application/pdf;base64," + response.message;
                                const linkSource = base64String;
                                const downloadLink = document.createElement("a");
                                const fileName = $scope.poDetails.PO_NUMBER + ".pdf";
                                downloadLink.href = linkSource;
                                downloadLink.download = fileName;
                                downloadLink.click();
                            }
                        });
                });

            };
            $scope.poPDFDetails = {}

            $scope.savePOPDF = function () {

                if ($scope.poDetails.PO_STATUS) {
                    $scope.PO_STATUS_TEMP = ''
                    $scope.PO_STATUS_TEMP = $scope.poDetails.PO_STATUS;
                    $scope.poDetails.PO_STATUS = 'APPROVED'
                }

                /*$scope.poPDFDetails.SPOC_NAME = $scope.poDetails.SPOC_NAME;*/
                $scope.poDetails.CONTRACT_SPOC_EMAIL = $scope.poDetails.CONTRACT_SPOC_EMAIL;
                $scope.disablebutton = true;


                // Function to remove HTML tags from a string
                function stripHtmlTags(input) {
                    var doc = new DOMParser().parseFromString(input, 'text/html');
                    return doc.body.textContent || "";
                }

                // Clean each value in poDetails to remove HTML tags
                function cleanPoDetails(poDetails) {
                    for (let key in poDetails) {
                        if (poDetails.hasOwnProperty(key)) {
                            // If the value is a string, clean it
                            if (typeof poDetails[key] === 'string') {
                                poDetails[key] = stripHtmlTags(poDetails[key]);
                            }
                            // If the value is an object, recursively clean it
                            if (typeof poDetails[key] === 'object' && poDetails[key] !== null) {
                                cleanPoDetails(poDetails[key]);
                            }
                        }
                    }
                }

                // Clean the poDetails object before stringifying
                cleanPoDetails($scope.poDetails);

                let params = {
                    detail: {
                        'PO_NUMBER': $scope.PO_NUMBER,
                        'PO_PDF_JSON': JSON.stringify($scope.poDetails),
                        'U_ID': $scope.userId,
                        'PO_PDF_ATTACHMENTS': "",
                        'PO_TYPE': $scope.poDetails.PO_TYPE,
                        'COMP_ID': $scope.compId,
                        'PO_AMENDMENT_REV': $scope.poDetails.PO_AMENDMENT_REV
                    },
                    sessionId: $scope.sessionId
                };

                PRMPOService.savePOPDFDetails(params)
                    .then(function (response) {
                        $scope.disablebutton = false;
                        if (response.errorMessage) {
                            if ($scope.PO_STATUS_TEMP) {
                                $scope.poDetails.PO_STATUS = $scope.PO_STATUS_TEMP;
                            }
                            let errorMsg = response.errorMessage ? response.errorMessage : 'Error saving details, please contact support.';
                            swal("Error!", errorMsg, "error");
                        } else {
                            if ($scope.PO_STATUS_TEMP) {
                                $scope.poDetails.PO_STATUS = $scope.PO_STATUS_TEMP;
                            }
                            swal("Success!", "Saved Successfully.", "success");
                            //$scope.submitPo(1)

                        };
                    });

            };

            $scope.isRequired = function (itemNo) {
                if (itemNo === 3) {
                    if ($scope.poDetails.PO_TYPE === 'WORK_ORDER' || $scope.poDetails.PO_TYPE === 'SERVICE_AGREEMENT') {
                        return 'N';
                    } else {
                        return 'Y';
                    }
                }

                if (itemNo === 5) {
                    if ($scope.poDetails.PO_TYPE === 'WORK_ORDER' || $scope.poDetails.PO_TYPE === 'SERVICE_AGREEMENT') {
                        return 'N';
                    } else {
                        return 'Y';
                    }
                }

                if (itemNo === 8) {
                    if ($scope.poDetails.PO_TYPE === 'SERVICE_AGREEMENT') {
                        return 'N';
                    } else {
                        return 'Y';
                    }
                }

                if (itemNo === 14) {
                    if ($scope.poDetails.PO_TYPE === 'LOCAL_SALES' || $scope.poDetails.PO_TYPE === 'SERVICE_AGREEMENT') {
                        return 'N';
                    } else {
                        return 'Y';
                    }
                }
            };            

            $scope.GetTermsandConditions = function () {
                auctionsService.GetTermsandConditions(0, $scope.compId, $scope.sessionId, $scope.poDetails.PO_TYPE,'', $scope.userId)
                    .then(function (response) {
                        $scope.termsConditions = response;
                        $scope.fillTermsConditionsDesc();
                        console.log($scope.termsConditions);
                        if ($scope.PO_PDF_JSON) {

                            $scope.poDetailsTest = angular.copy($scope.poDetails);
                            $scope.poDetails = JSON.parse($scope.PO_PDF_JSON);
                            $scope.poDetails.PO_TYPE = $scope.poDetailsTest.PO_TYPE;
                            if ($scope.poDetails.PO_TYPE === 'WORK_ORDER') {
                                $scope.poDetails.PO_TYPE_DESC = 'Work Order (Item Rate Contract)';
                            }

                            if ($scope.poDetails.PO_TYPE === 'LOCAL_SALES') {
                                $scope.poDetails.PO_TYPE_DESC = 'Purchase Order (Local Sales)';
                            }

                            if ($scope.poDetails.PO_TYPE === 'SERVICE_AGREEMENT') {
                                $scope.poDetails.PO_TYPE_DESC = 'Service Agreeement';
                            }

                            if ($scope.poDetails.PO_TYPE === 'IMPORT') {
                                $scope.poDetails.PO_TYPE_DESC = 'Purchase Order (Import)';
                            }
                        }

                        

                        if (typeof $scope.poDetails === "object") {
                            Object.keys($scope.poDetails).forEach(key => {
                                if (typeof $scope.poDetails[key] === "string" && $scope.poDetails[key].includes("{{poDetails."+key+"}}")) {
                                    $scope.poDetails[key] = "";
                                }
                            });
                        }
                    })
            };

            $scope.fillTermsConditionsDesc = function () {
                if ($scope.termsConditions && $scope.termsConditions.length > 0) {
                    $scope.termsConditions.forEach(function (item, index) {
                        if (item.TITLE === 'Project Milestone') {
                            $scope.poDetails.RESPONSE1 = item.DESCRIPTION;
                        }

                        if (item.TITLE === 'Form of Contract (e.g. purchase order, contract etc)') {
                            $scope.poDetails.PO_TYPE_DESC = item.DESCRIPTION;
                        }

                        if (item.TITLE === 'Liquidated and Ascertained Damages') {
                            $scope.poDetails.RESPONSE2 = item.DESCRIPTION;
                        }

                        if (item.TITLE === 'Defects Liability Period / Warranty') {
                            $scope.poDetails.RESPONSE3 = item.DESCRIPTION;
                        }

                        if (item.TITLE === 'Retention') {
                            $scope.poDetails.RESPONSE4 = item.DESCRIPTION;
                        }

                        if (item.TITLE === 'Performance Bond') {
                            $scope.poDetails.RESPONSE5 = item.DESCRIPTION;
                        }

                        if (item.TITLE === 'Bank Guarantee (for advance payment)') {
                            $scope.poDetails.RESPONSE6 = item.DESCRIPTION;
                        }

                        if (item.TITLE === 'Payment terms (down payments, milestones, % completion etc)') {
                            $scope.poDetails.RESPONSE7 = item.DESCRIPTION;
                        }

                        if (item.TITLE === 'Project Specific Terms and Conditions') {
                            $scope.poDetails.RESPONSE8 = item.DESCRIPTION;
                        }

                        if (item.TITLE === 'Termination') {
                            $scope.poDetails.RESPONSE9 = item.DESCRIPTION;
                        }

                        if (item.TITLE === 'Insurance of the Works') {
                            $scope.poDetails.RESPONSE10 = item.DESCRIPTION;
                        }

                        if (item.TITLE === 'Insurance of the Works (All risks Insurance to cover the works)') {
                            $scope.poDetails.RESPONSE10 = item.DESCRIPTION;
                        }

                        if (item.TITLE === 'Labour Complainces') {
                            $scope.poDetails.RESPONSE11 = item.DESCRIPTION;
                        }

                        if (item.TITLE === 'Compliances') {
                            $scope.poDetails.RESPONSE12 = item.DESCRIPTION;
                        }

                        if (item.TITLE === 'Professional Indemnity Liability Insurance (Insurance for any loss due to alleged neglect, error or omission of professional indemnity, for consultant or contractor’s designed element only)') {
                            $scope.poDetails.RESPONSE13 = item.DESCRIPTION;
                        }

                        if (item.TITLE === 'Professional Indemnity Liability Insurance') {
                            $scope.poDetails.RESPONSE13 = item.DESCRIPTION;
                        }
                    });
                } 
            };

            function price_in_words(price) {
                var sglDigit = ["Zero", "One", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine"],
                    dblDigit = ["Ten", "Eleven", "Twelve", "Thirteen", "Fourteen", "Fifteen", "Sixteen", "Seventeen", "Eighteen", "Nineteen"],
                    tensPlace = ["", "Ten", "Twenty", "Thirty", "Forty", "Fifty", "Sixty", "Seventy", "Eighty", "Ninety"],
                    handle_tens = function (dgt, prevDgt) {
                        return 0 == dgt ? "" : " " + (1 == dgt ? dblDigit[prevDgt] : tensPlace[dgt])
                    },
                    handle_utlc = function (dgt, nxtDgt, denom) {
                        return (0 != dgt && 1 != nxtDgt ? " " + sglDigit[dgt] : "") + (0 != nxtDgt || dgt > 0 ? " " + denom : "")
                    };

                var str = "",
                    digitIdx = 0,
                    digit = 0,
                    nxtDigit = 0,
                    words = [];
                if (price += "", isNaN(parseInt(price))) str = "";
                else if (parseInt(price) > 0 && price.length <= 10) {
                    for (digitIdx = price.length - 1; digitIdx >= 0; digitIdx--) switch (digit = price[digitIdx] - 0, nxtDigit = digitIdx > 0 ? price[digitIdx - 1] - 0 : 0, price.length - digitIdx - 1) {
                        case 0:
                            words.push(handle_utlc(digit, nxtDigit, ""));
                            break;
                        case 1:
                            words.push(handle_tens(digit, price[digitIdx + 1]));
                            break;
                        case 2:
                            words.push(0 != digit ? " " + sglDigit[digit] + " Hundred" + (0 != price[digitIdx + 1] && 0 != price[digitIdx + 2] ? " and" : "") : "");
                            break;
                        case 3:
                            words.push(handle_utlc(digit, nxtDigit, "Thousand"));
                            break;
                        case 4:
                            words.push(handle_tens(digit, price[digitIdx + 1]));
                            break;
                        case 5:
                            words.push(handle_utlc(digit, nxtDigit, "Lakh"));
                            break;
                        case 6:
                            words.push(handle_tens(digit, price[digitIdx + 1]));
                            break;
                        case 7:
                            words.push(handle_utlc(digit, nxtDigit, "Crore"));
                            break;
                        case 8:
                            words.push(handle_tens(digit, price[digitIdx + 1]));
                            break;
                        case 9:
                            words.push(0 != digit ? " " + sglDigit[digit] + " Hundred" + (0 != price[digitIdx + 1] || 0 != price[digitIdx + 2] ? " and" : " Crore") : "")
                    }
                    str = words.reverse().join("")
                } else str = "";
                return str

            }


            $scope.PDFSave = function () {
                var params =
                {
                    "tcData": JSON.stringify($scope.poPDFDetails),
                    "poNumber": $scope.PO_NUMBER,
                    "compId": +$scope.compId,
                    "revision": $scope.pdfTemplate == 'viewpoAmendmentPDF' ? $scope.REVISION : -1,
                    "sessionId": userService.getUserToken(),
                    "saveToServer": false,
                    "validateSession": true,
                };

                PRMPOService.PDFSave(params)
                    .then(function (response) {


                    });
            };


           

        }]);