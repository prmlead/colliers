﻿prmApp
    .controller('viewPendingPOCtrl', ["$rootScope", "$scope", "$stateParams", "$log", "$state", "$window", "userService", "auctionsService", "storeService", "growlService", "PRMPRServices", "poService",
        "PRMCustomFieldService", "PRMPOService", "$uibModal", "fileReader",
        function ($rootScope, $scope, $stateParams, $log, $state, $window, userService, auctionsService, storeService, growlService, PRMPRServices, poService, PRMCustomFieldService, PRMPOService, $uibModal, fileReader) {
            $scope.poNumber = $stateParams.poID;
            $scope.userID = userService.getUserId();
            $scope.sessionID = userService.getUserToken();
            $scope.poOrderId = $stateParams.poOrderId;
            $scope.isCustomer = userService.getUserType() === "CUSTOMER" ? true : false;
            $scope.compID = userService.getUserCompanyId();
            $scope.poDetails = {};

            $scope.getPODetails = function () {
                var params = {
                    "ponumber": $scope.poNumber
                };

                PRMPOService.getPODetails(params)
                    .then(function (response) {
                        $scope.poDetails = response;
                        //$scope.pendingPOItems.forEach(function (item, index) {
                        //    item.DELIVERY_DATE = item.DELIVERY_DATE ? moment(item.DELIVERY_DATE).format("DD-MM-YYYY") : '-';
                        //});
                    });
            };

            $scope.getPODetails();

        }]);