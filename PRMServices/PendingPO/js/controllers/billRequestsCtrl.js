
prmApp.controller('billRequestsCtrl', ["$scope", "$stateParams", "$log", "$state", "$window", "userService", "auctionsService", "storeService", "growlService", "PRMProjectServices", "$filter", "fileReader", "$rootScope", "PRMPOService", "workflowService",
    function ($scope, $stateParams, $log, $state, $window, userService, auctionsService, storeService, growlService, PRMProjectServices, $filter, fileReader, $rootScope, PRMPOService, workflowService) {

        $scope.USER_ID = +userService.getUserId();
        $scope.COMP_ID = +userService.getUserCompanyId();
        $scope.customerCompanyId = +userService.getCustomerCompanyId();
        $scope.sessionID = userService.getUserToken();
        $scope.isSuperUser = userService.getUserObj().isSuperUser;
        $scope.isVendor = (userService.getUserType() == "VENDOR") ? true : false;
        $scope.poNumber = $stateParams.poNumber;

        $scope.totalAttachmentMaxSize = 6291456;
        $scope.totalRequirementSize = 0;
        $scope.totalRequirementItemSize = 0;

        $scope.billRequestsList = [];



        $scope.deptIDs = [];
        $scope.desigIDs = [];

        /*region start WORKFLOW*/
        $scope.workflowList = [];
        $scope.itemWorkflow = [];
        $scope.workflowObj = {};
        $scope.workflowObj.workflowID = 0;
        $scope.currentStep = 0;
        $scope.orderInfo = 0;
        $scope.assignToShow = '';
        $scope.isWorkflowCompleted = false;
        $scope.WorkflowModule = 'BILL_CERTIFICATION';
        $scope.disableWFSelection = false;
        /*region end WORKFLOW*/


        $scope.ListUserDepartmentDesignations = userService.getListUserDepartmentDesignations();
        if ($scope.ListUserDepartmentDesignations && $scope.ListUserDepartmentDesignations.length > 0) {
            $scope.ListUserDepartmentDesignations.forEach(function (item, index) {
                $scope.deptIDs.push(item.deptID);
                item.listDesignation.forEach(function (item1, index1) {
                    if (item1.isAssignedToUser && item1.isValid) {
                        $scope.desigIDs.push(item1.desigID);
                    }
                });
            });
        }

        $scope.getBillRequests = function () {
            $scope.billRequestsList = [];
            $scope.billAttachmentsList = [];
            $scope.params =
            {
                "COMP_ID": $scope.COMP_ID,
                "U_ID": $scope.USER_ID,
                "IS_CUSTOMER": ($scope.isVendor ? 0 : 1),
                "sessionid": $scope.sessionID
            };

            PRMPOService.getBillRequests($scope.params)
                .then(function (response) {


                    if (response)
                    {

                        $scope.billRequestsList = JSON.parse(response).Table;

                        if ($scope.billRequestsList && $scope.billRequestsList.length > 0) {
                            $scope.billRequestsList.forEach(function (billItem, billIndex) {
                                billItem.DISPLAY = false;
                                //if (JSON.parse(response).Table1) {
                                //    billItem.ATTACHMENTS_ARRAY = [];
                                //    billItem.BILL_CERTIFICATES = [];
                                //    $scope.billAttachmentsList = JSON.parse(response).Table1;
                                //    if ($scope.billAttachmentsList && $scope.billAttachmentsList.length > 0) {
                                //        $scope.billAttachmentsList.forEach(function (billAttachItem, billAttachIndex) {
                                //            if (billItem.BILL_ID === billAttachItem.BILL_ID && billAttachItem.ATT_TYPE === 'BILL_ATTACHMENTS') {
                                //                var fileUpload = {
                                //                    fileName: billAttachItem.ATT_PATH,
                                //                    fileID: billAttachItem.ATT_ID
                                //                };
                                //                billItem.ATTACHMENTS_ARRAY.push(fileUpload);
                                //            } else if (billItem.BILL_ID === billAttachItem.BILL_ID && billAttachItem.ATT_TYPE === 'BILL_CERTIFICATES') {
                                //                var fileUpload = {
                                //                    fileName: billAttachItem.ATT_PATH,
                                //                    fileID: billAttachItem.ATT_ID
                                //                };
                                //                billItem.BILL_CERTIFICATES.push(fileUpload);
                                //            }
                                //        });
                                //    }
                                //}
                            });
                        }
                    }
                });
        };

        $scope.getBillRequests();


        $scope.createBillRequest = function ()
        {

            $scope.getPOScheduleDetails();
        };

        $scope.billSubmissionObj =
        {
            poArray: [],
            MULTIPLE_ATTACHMENTS : []
        };

        $scope.getPOScheduleDetails = function ()
        {
            $scope.ERROR_MESSAGE = '';
            $scope.params =
            {
                "COMP_ID": $scope.COMP_ID,
                "PO_NUMBER": $scope.poNumber,
                "VALIDATE": 1,
                "sessionid": $scope.sessionID
            };

            PRMPOService.getPOScheduleDetails($scope.params)
                .then(function (response) {

                    if (response)
                    {
                        var resp = JSON.parse(response).Table;

                        if (resp[0].ERROR_MESSAGE)
                        {
                            $scope.ERROR_MESSAGE = resp[0].ERROR_MESSAGE;
                        } else {
                            $scope.billSubmissionObj.poArray = JSON.parse(response).Table;
                            $scope.billSubmissionObj.OVERALL_AMOUNT = (_.sumBy($scope.billSubmissionObj.poArray, 'OVERALL_AMOUNT') - $scope.billSubmissionObj.poArray[0].TOTAL_CERTIFIED_AMOUNT);

                            
                        }
                    }

                    angular.element('#billSubmissionPopup').modal('show');


                });
        };

        
        $scope.multipleAttachments = [];
        $scope.multipleAttachments1 = [];
        $scope.selectedIndex = 0;
        $scope.getFile1 = function (id, itemid, ext) {
            $scope.filesTemp = (itemid === 'BILL_CERTIFICATION_ATTACHMENTS' ? $("#" + id)[0].files : $("#fileInput")[0].files);

            if (itemid != 'BILL_CERTIFICATION_ATTACHMENTS') {
                id = "0";
            }
            $scope.filesTemp = Object.values($scope.filesTemp);
            $scope.filesTemp.forEach(function (attach, attachIndex) {
                $scope.file = (itemid === 'BILL_CERTIFICATION_ATTACHMENTS' ? $("#" + id)[0].files[attachIndex] : $("#fileInput")[0].files[attachIndex]);

                if (itemid === 'BILL_CERTIFICATION_ATTACHMENTS') {
                    $scope.progress = 0;
                    $scope.totalRequirementSize = 0;
                    $scope.billValues.MULTIPLE_ATTACHMENTS_BILL_CERTIFICATION = $scope.file;
                    $scope.multipleAttachments1 = Object.values($scope.billValues.MULTIPLE_ATTACHMENTS_BILL_CERTIFICATION);
                    if ($scope.multipleAttachments1 && $scope.multipleAttachments1.length > 0) {
                        $scope.multipleAttachments1.forEach(function (item, index) {
                            $scope.totalRequirementSize = $scope.totalRequirementSize + item.size;
                        });
                    }
                    $scope.billValues.MULTIPLE_ATTACHMENTS_BILL_CERTIFICATION = [];
                    if (($scope.totalRequirementSize + $scope.totalRequirementItemSize) > $scope.totalAttachmentMaxSize) {
                        swal({
                            title: "Attachment size!",
                            text: "Total Attachments size cannot exceed 6MB",
                            type: "warning",
                            showCancelButton: false,
                            confirmButtonColor: "#DD6B55",
                            confirmButtonText: "Ok",
                            closeOnConfirm: true
                        },
                            function () {
                                return;
                            });
                        return;
                    }
                } else {
                    $scope.progress = 0;
                    $scope.totalRequirementSize = 0;
                    $scope.billSubmissionObj.MULTIPLE_ATTACHMENTS = $scope.file;
                    $scope.multipleAttachments = Object.values($scope.billSubmissionObj.MULTIPLE_ATTACHMENTS);
                    if ($scope.multipleAttachments && $scope.multipleAttachments.length > 0) {
                        $scope.multipleAttachments.forEach(function (item, index) {
                            $scope.totalRequirementSize = $scope.totalRequirementSize + item.size;
                        });
                    }
                    $scope.billSubmissionObj.MULTIPLE_ATTACHMENTS = [];
                    if (($scope.totalRequirementSize + $scope.totalRequirementItemSize) > $scope.totalAttachmentMaxSize) {
                        swal({
                            title: "Attachment size!",
                            text: "Total Attachments size cannot exceed 6MB",
                            type: "warning",
                            showCancelButton: false,
                            confirmButtonColor: "#DD6B55",
                            confirmButtonText: "Ok",
                            closeOnConfirm: true
                        },
                            function () {
                                return;
                            });
                        return;
                    }
                }
 

                $scope.filesTemp.forEach(function (attach, attachIndex) {
                    $scope.file = (itemid === 'BILL_CERTIFICATION_ATTACHMENTS' ? $("#" + id)[0].files[attachIndex] : $("#fileInput")[0].files[attachIndex]);
                    fileReader.readAsDataUrl($scope.file, $scope)
                        .then(function (result) {
                            var fileUpload = {
                                fileStream: [],
                                fileName: '',
                                fileID: 0
                            };
                            var bytearray = new Uint8Array(result);
                            fileUpload.fileStream = $.makeArray(bytearray);
                            fileUpload.fileName = attach.name;
                            if (itemid === 'BILL_CERTIFICATION_ATTACHMENTS') {
                                if (!$scope.billValues.MULTIPLE_ATTACHMENTS_BILL_CERTIFICATION) {
                                    $scope.billValues.MULTIPLE_ATTACHMENTS_BILL_CERTIFICATION = [];
                                }

                                var ifExists = _.findIndex($scope.billValues.MULTIPLE_ATTACHMENTS_BILL_CERTIFICATION, function (attach) { return attach.fileName.toLowerCase() === fileUpload.fileName.toLowerCase() });
                                if (ifExists <= -1) {
                                    $scope.billValues.MULTIPLE_ATTACHMENTS_BILL_CERTIFICATION.push(fileUpload);
                                }
                            } else if (itemid === 'BILL_SUBMISSION_ATTACHMENTS') {
                                if (!$scope.billSubmissionObj.MULTIPLE_ATTACHMENTS) {
                                    $scope.billSubmissionObj.MULTIPLE_ATTACHMENTS = [];
                                }

                                var ifExists = _.findIndex($scope.billSubmissionObj.MULTIPLE_ATTACHMENTS, function (attach) { return attach.fileName.toLowerCase() === fileUpload.fileName.toLowerCase() });
                                if (ifExists <= -1) {
                                    $scope.billSubmissionObj.MULTIPLE_ATTACHMENTS.push(fileUpload);
                                }
                            }
                        });
                });
            });
        };



        $scope.createBillSubmission = function () {

            $scope.billSubmissionObj.V_COMP_ID = $scope.COMP_ID;
            $scope.billSubmissionObj.C_COMP_ID = $scope.customerCompanyId;
            $scope.billSubmissionObj.CREATED_BY = $scope.USER_ID;
            $scope.billSubmissionObj.PROJECT_ID = $scope.billSubmissionObj.poArray[0].PROJECT_ID;
            $scope.billSubmissionObj.PO_NUMBER = $scope.billSubmissionObj.poArray[0].PO_NUMBER;
            $scope.billSubmissionObj.PO_VALUE = $scope.billSubmissionObj.OVERALL_AMOUNT;
            
            $scope.params =
            {
                "billRequest": $scope.billSubmissionObj,
                "sessionid": $scope.sessionID
            };

            PRMPOService.createBillSubmission($scope.params)
                .then(function (response) {

                    if (response.errorMessage != '') {
                        growlService.growl(response.errorMessage, "error");
                    } else {
                        growlService.growl("Bill Submission Done", "success");
                        location.reload();
                        angular.element('#billSubmissionPopup').modal('hide');
                    }

                });

        };

        $scope.uploadBillCertification = function () {

            $scope.billValues.MODIFIED_BY = $scope.USER_ID;

            var workflows = _.filter($scope.workflowListTemp, { WorkflowModule: 'BILL_CERTIFICATION' });

            if (workflows && workflows.length <= 0) {
                swal("Error!", "Cannot submit bill certificates/bill certified amount until there is a bill submission workflow.", "error");
            }

            $scope.billValues.BILL_CERTIFICATE_WF_ID = _.orderBy(workflows, ['workflowID'], ['desc'])[0].workflowID;

            $scope.billCertificates =
            {
                BILL_ID: $scope.billValues.BILL_ID,
                MODIFIED_BY: $scope.billValues.MODIFIED_BY,
                BILL_CERTIFIED_AMOUNT: +$scope.billValues.BILL_CERTIFIED_AMOUNT,
                BILL_CERTIFICATE_WF_ID: $scope.billValues.BILL_CERTIFICATE_WF_ID,
                MULTIPLE_ATTACHMENTS_BILL_CERTIFICATION: $scope.billValues.MULTIPLE_ATTACHMENTS_BILL_CERTIFICATION
            };



            $scope.params =
            {
                "billCertificates": $scope.billCertificates,
                "sessionid": $scope.sessionID
            };

            PRMPOService.uploadBillCertification($scope.params)
                .then(function (response) {

                    if (response.errorMessage != '') {
                        growlService.growl(response.errorMessage, "error");
                    } else {
                        growlService.growl("Bill Certification Done", "success");
                        location.reload();
                        angular.element('#billCertificationPopup').modal('hide');
                    }

                });
        };



        


        $scope.submitForBillCertification = function ()
        {

            var params =
            {
                "billSubmission": $scope.billSubmissionObj,
                "sessionid": $scope.sessionID
            };

            PRMPOService.submitForBillCertification(params)
                .then(function (response) {

                    if (response.errorMessage != '') {
                        growlService.growl(response.errorMessage, "error");
                    } else {
                        growlService.growl("Bill Submission " + $scope.billSubmissionObj.BILL_REQUEST_STATUS, "success");
                    }

                });
        };

        $scope.showBillCertificationBox = function (billRequest, value) {
            if (value) {
                billRequest.DISPLAY = true;
                $scope.billValues = billRequest;
                $scope.billValues.MULTIPLE_ATTACHMENTS_BILL_CERTIFICATION = [];

                angular.element('#billCertificationPopup').modal('show');
            } else {
                billRequest.DISPLAY = false;
            }
        };


        /*region start WORKFLOW*/

        $scope.getWorkflows = function () {
            workflowService.getWorkflowList($scope.customerCompanyId)
                .then(function (response) {
                    $scope.workflowList = [];
                    $scope.workflowListDeptWise = [];
                    $scope.workflowListSubPackage = [];
                    $scope.workflowListTemp = response;

                    $scope.workflowListTemp.forEach(function (item, index) {
                        if (item.WorkflowModule === $scope.WorkflowModule) {
                            $scope.workflowList.push(item);
                            $scope.workflowListDeptWise.push(item);
                        }
                    });

                    if ($scope.isSuperUser) {
                        $scope.workflowList = $scope.workflowList;
                    }
                    else {
                        $scope.workflowList = [];
                        $scope.workflowListDeptWise.forEach(function (wf, idx) {
                            $scope.deptIDs.forEach(function (dep) {
                                if (dep == wf.deptID) {
                                    $scope.workflowList.push(wf);
                                }
                            });
                        });

                    }
                });
        };

        $scope.getWorkflows();

        $scope.getItemWorkflow = function (ID, workflowModule) {
            workflowService.getItemWorkflow(0, ID, workflowModule)
                .then(function (response) {
                    $scope.itemWorkflow = response;
                    $scope.checkIsFormDisable();
                    if ($scope.itemWorkflow && $scope.itemWorkflow.length > 0 && $scope.itemWorkflow[0].WorkflowTracks.length > 0) {
                        $scope.currentStep = 0;

                        var count = 0;

                        $scope.itemWorkflow[0].WorkflowTracks.forEach(function (track) {
                            if (!track.multipleAttachments) {
                                track.multipleAttachments = [];
                            }

                            if (track.status === 'APPROVED') { track.statusNew = 'Approved'; }
                            if (track.status === 'HOLD') { track.statusNew = 'Hold'; }
                            if (track.status === 'PENDING') { track.statusNew = 'Pending'; }
                            if (track.status === 'REJECTED') { track.statusNew = 'Rejected'; }

                            if (track.status === 'APPROVED' || track.status === 'HOLD') {
                                $scope.isFormdisabled = true;
                            }

                            if (track.status === 'APPROVED') {
                                $scope.isWorkflowCompleted = true;
                                $scope.orderInfo = track.order;
                                $scope.assignToShow = track.status;

                            }
                            else {
                                $scope.isWorkflowCompleted = false;
                            }

                            if (track.status === 'REJECTED' && count == 0) {
                                count = count + 1;
                            }

                            if ((track.status === 'PENDING' || track.status === 'HOLD') && count == 0) {
                                count = count + 1;
                                //$scope.IsUserApproverForStage(track.approverID);
                                $scope.currentAccess = track.order;
                            }

                            if ((track.status === 'PENDING' || track.status === 'HOLD' || track.status === 'REJECTED') && $scope.currentStep === 0) {
                                $scope.currentStep = track.order;
                                return false;
                            }
                        });
                    }
                });
        };


        $scope.isApproverDisable = function (index) {

            var disable = true;

            var previousStep = {};

            $scope.itemWorkflow[0].WorkflowTracks.forEach(function (step, stepIndex) {

                if (index == stepIndex) {
                    if (stepIndex == 0) {
                        if ($scope.isUserBelongsToDeptandDesig(step.department.deptID, step.approver.desigID) &&
                            (step.status === 'PENDING' || step.status === 'HOLD')) {
                            disable = false;
                        }
                        else {
                            disable = true;
                        }
                    }
                    else if (stepIndex > 0) {
                        if (previousStep.status === 'PENDING' || previousStep.status === 'HOLD' || previousStep.status === 'REJECTED') {
                            disable = true;
                        }
                        else if ($scope.isUserBelongsToDeptandDesig(step.department.deptID, step.approver.desigID) &&
                            (step.status === 'PENDING' || step.status === 'HOLD')) {
                            disable = false;
                        }
                        else {
                            disable = true;
                        }
                    }
                }
                previousStep = step;
            });

            return disable;
        };

        $scope.isUserBelongsToDeptandDesig = function (deptID, desigID) {
            var isEligible = true;

            if ($scope.deptIDs.indexOf(deptID) != -1 && $scope.desigIDs.indexOf(desigID) != -1) {
                isEligible = true;
            } else {
                isEligible = false;
            }

            return isEligible;
        };

        $scope.updateTrack = function (step, status, type) {
            $scope.disableAssignPR = true;
            $scope.commentsError = '';
            if (step.comments != null || step.comments != "" || step.comments != undefined) {
                step.comments = validateStringWithoutSpecialCharacters(step.comments);
            }
            var tempArray = $scope.itemWorkflow[0].WorkflowTracks[$scope.itemWorkflow[0].WorkflowTracks.length - 1];
            if (step.order == tempArray.order && status === 'APPROVED') {
                $scope.disableAssignPR = false;
            } else {
                $scope.disableAssignPR = true;
            }

            if ($scope.isReject) {
                $scope.commentsError = 'Please Save Rejected Items/Qty';
                return false;
            }

            if (status === 'REJECTED' && (step.comments == null || step.comments == "")) {
                $scope.commentsError = 'Please enter comments';
                return false;
            }

            step.status = status;
            step.sessionID = $scope.sessionID;
            step.modifiedBy = +userService.getUserId();

            step.moduleName = $scope.WorkflowModule;

            step.subModuleName = '';
            step.subModuleID = 0;

            workflowService.SaveWorkflowTrack(step)
                .then(function (response) {
                    if (response.errorMessage) {
                        growlService.growl(response.errorMessage, "inverse");
                    }
                    else {
                        $scope.getItemWorkflow();
                        location.reload();
                    }
                });
        };

        $scope.showBillRequestApprovalPopUp = true;

        $scope.showBillRequestApproval = function () {
            $scope.showBillRequestApprovalPopUp = false;
        };

        $scope.isFormdisabled = true;

        $scope.checkIsFormDisable = function () {
            $scope.isFormdisabled = false;
            //if ($scope.itemWorkflow.length == 0) {
            //    $scope.isFormdisabled = true;
            //} else {
            //    if (($scope.BudgetList.CREATED_BY == +userService.getUserId() || $scope.BudgetList.MODIFIED_BY == +userService.getUserId()) && $scope.itemWorkflow[0].WorkflowTracks.length > 0 &&
            //        $scope.itemWorkflow[0].WorkflowTracks[0].status !== "APPROVED" && $scope.itemWorkflow[0].WorkflowTracks[0].order == 1 && $scope.itemWorkflow[0].workflowID > 0) {
            //        $scope.isFormdisabled = true;
            //    }
            //}
        };

        function validateStringWithoutSpecialCharacters(string) {
            if (string) {
                string = string.replace(/\'/gi, "");
                string = string.replace(/\"/gi, "");
                string = string.replace(/[`^_|\?;:'",<>\{\}\[\]\\\/]/gi, "");
                string = string.replace(/(\r\n|\n|\r)/gm, "");
                string = string.replace(/\t/g, '');
                return string;
            }
        }

        $scope.showApprovedDate = function (date) {
            return userService.toLocalDate(date);
        };


        $scope.routeToInvoices = function (billRequest) {
            $state.go("listInvoices", {"PO_ID": billRequest.PO_ID,"ID":0});
        };

        $scope.cancelBillSubmissionPopup = function () {
            angular.element('#billSubmissionPopup').modal('hide');
        };

        $scope.cancelBillCertificationPopup = function () {
            angular.element('#billCertificationPopup').modal('hide');
        };

    }]);