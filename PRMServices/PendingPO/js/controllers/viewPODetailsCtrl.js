﻿prmApp
    .controller('viewPODetailsCtrl', ["$rootScope", "$scope", "$stateParams", "$log", "$state", "$window", "userService", "auctionsService", "growlService",
        "PRMPOService", "$uibModal", "fileReader", "PRMProjectServices",
        function ($rootScope, $scope, $stateParams, $log, $state, $window, userService, auctionsService, growlService, PRMPOService, $uibModal, fileReader, PRMProjectServices) {
            $scope.PO_NUMBER = $stateParams.poNumber;
            $scope.userId = userService.getUserId();
            $scope.sessionId = userService.getUserToken();
            $scope.isCustomer = userService.getUserType() === "CUSTOMER" ? true : false;
            $scope.compId = userService.getUserCompanyId();
            $scope.poDetails = {};
            $scope.projectDetails = {};
            $scope.poDetailItems = [];

            if (!$scope.PO_NUMBER) {
                $state.go('list-po-new');
            }

            $scope.getPODetails = function () {
                var params = {
                    "poNumber": $scope.PO_NUMBER,
                    "sessionId": $scope.sessionId
                };

                PRMPOService.getPODetails(params)
                    .then(function (response) {
                        $scope.poDetailItems = response;
                        $scope.poDetails.PO_VALUE = 0;
                        $scope.poDetailItems.forEach(function (item, index) {
                            item.DELIVERY_DATE = item.DELIVERY_DATE ? moment(item.DELIVERY_DATE).format("DD-MM-YYYY") : '-';
                            $scope.poDetails.PO_VALUE += item.TOTAL_VALUE;
                        });

                        $scope.poDetails.PO_NUMBER = $scope.poDetailItems[0].PO_NUMBER;
                        $scope.poDetails.VENDOR_COMPANY = $scope.poDetailItems[0].VENDOR_COMPANY;
                        $scope.poDetails.VENDOR_CODE = $scope.poDetailItems[0].VENDOR_CODE;
                        $scope.poDetails.PROJECT_ID = $scope.poDetailItems[0].PROJECT_ID;
                        $scope.poDetails.PROJECT_NAME = $scope.poDetailItems[0].PROJECT_NAME;
                        $scope.poDetails.PO_TYPE = $scope.poDetailItems[0].PO_TYPE;
                        $scope.poDetails.PAYMENT_TERMS = $scope.poDetailItems[0].PAYMENT_TERMS;
                        $scope.getProjectDetails($scope.poDetails.PROJECT_ID);

                        $scope.poDetails.MAP_TERM_ID = $scope.poDetailItems[0].MAP_TERM_ID;

                    });
            };

            $scope.getPODetails();

            $scope.termsConditions = [];

            $scope.getTermsandConditions = function ()
            {
                auctionsService.GetTermsandConditions(0, $scope.compId, $scope.sessionId, $scope.poDetails.PO_TYPE, ($scope.poDetails.MAP_TERM_ID > 0 ? $scope.PO_NUMBER : ''))
                    .then(function (response) {
                        $scope.termsConditions = response;
                        angular.element('#termsCondition').modal('show');
                    });
            }
            

            $scope.getProjectDetails = function (projectId) {
                var params =
                {
                    "PROJECT_ID": projectId,
                    "sessionid": $scope.sessionId
                };

                PRMProjectServices.GetProjectDetails(params)
                    .then(function (response) {
                        $scope.projectDetails = response;
                        var parsedAddress = JSON.parse($scope.projectDetails.BILL_TO_ADDRESS).ADDRESS;
                        $scope.projectDetails.BILL_TO_ADDRESS = parsedAddress;
                    });
            };

            $scope.EditTermsAndConditions = function ()
            {
                $scope.getTermsandConditions();
            };


            $scope.savePOtermsandconditions = function ()
            {

                var params = {
                    "MAP_ID" : 0,
                    "PO_NUMBER": $scope.poDetails.PO_NUMBER,
                    //"PO_TYPE": $scope.poDetails.PO_TYPE,
                    "TERMS_CONDITIONS": $scope.termsConditions,
                    "COMP_ID": $scope.compId,
                    "U_ID": $scope.userId,
                    "sessionId": $scope.sessionId
                };

                PRMPOService.savePOtermsandconditions(params)
                    .then(function (response) {
                        if (response.errorMessage != '') {
                            growlService.growl(response.errorMessage, "inverse");
                        }
                        else {
                            growlService.growl("Terms and Conditions Saved Successfully.", "success");
                            angular.element('#termsCondition').modal('hide');
                            $state.reload();
                        }
                        


                    });
            };
            $scope.goToState = function () {
                $state.go('list-po-new');
            };
        }]);