
prmApp.controller('createBillRequestCtrl', ["$scope", "$stateParams", "$log", "$state", "$window", "userService", "auctionsService", "storeService",
    "growlService", "PRMProjectServices", "$filter", "fileReader", "$rootScope", "PRMPOService", "workflowService","PRMProjectServices",
    function ($scope, $stateParams, $log, $state, $window, userService, auctionsService, storeService,
        growlService, PRMProjectServices, $filter, fileReader, $rootScope, PRMPOService, workflowService, PRMProjectServices) {


        $scope.PO_ITEMS_FOR_BILL_REQUEST = [];
        $scope.USER_ID = +userService.getUserId();
        $scope.COMP_ID = +userService.getUserCompanyId();
        $scope.customerCompanyId = +userService.getCustomerCompanyId();
        $scope.sessionID = userService.getUserToken();
        $scope.isSuperUser = userService.getUserObj().isSuperUser;
        $scope.isVendor = (userService.getUserType() == "VENDOR") ? true : false;
        $scope.userName = userService.getUsername();
        $scope.BILL_ID = $stateParams.ID;
        $scope.PO_NUMBER = $stateParams.poNumber;
        $scope.isFormSubmitted = false;


        if ($scope.BILL_ID == 0)
        {
            if (!$scope.PO_NUMBER)
            {
                $state.go('list-po-new');
            }

            $scope.billSubmissionObj =
            {
                BILL_TYPE: 'Advance'
            };
        }

        $scope.checkListObj =
        {
            checkListArr:
                [
                    {
                        isChecked : true,
                        value: "PO Acceptance copy submission / Variation order copy.",
                        MULTIPLE_ATTACHMENTS : []
                    },
                    {
                        isChecked: true,
                        value: "Proforma Invoice.",
                        MULTIPLE_ATTACHMENTS: []
                    },
                    {
                        isChecked: true,
                        value: "MAR's.",
                        MULTIPLE_ATTACHMENTS: []
                    },
                    {
                        isChecked: true,
                        value: "DC' copies.",
                        MULTIPLE_ATTACHMENTS: []
                    },
                    {
                        isChecked: true,
                        value: "Joint Measurement Sheets.",
                        MULTIPLE_ATTACHMENTS: []
                    },
                    {
                        isChecked: true,
                        value: "Reference drawings supporting measurements.",
                        MULTIPLE_ATTACHMENTS: []
                    },
                    {
                        isChecked: true,
                        value: "Backup documents including invoices related to Basic rate variation.",
                        MULTIPLE_ATTACHMENTS: []
                    },
                    {
                        isChecked: true,
                        value: "Non Tendered item backups.",
                        MULTIPLE_ATTACHMENTS: []
                    },
                    {
                        isChecked: true,
                        value: "Other documents.",
                        MULTIPLE_ATTACHMENTS: []
                    }
                ]
        };


        if ($scope.PO_NUMBER && $state.current.name === 'viewBillRequest')
        {
            $scope.isFormSubmitted = true;
        }

        if ($scope.PO_ITEMS_FOR_BILL_REQUEST && $scope.PO_ITEMS_FOR_BILL_REQUEST.length > 0)
        {
            $scope.PO_ITEMS_FOR_BILL_REQUEST.forEach(function (item,index) {
                item.MULTIPLE_ATTACHMENTS =  [];
                item.BILL_REQUEST_AMOUNT = 0;
                item.billRequestValidation = false;
                item.billRequestAmountValidation = false;
                item.uploadBillSubmissionValidation = false;
            });
        }


        $scope.totalAttachmentMaxSize = 6291456;
        $scope.totalRequirementSize = 0;
        $scope.totalRequirementItemSize = 0;

        $scope.billRequestsList = [];



        $scope.deptIDs = [];
        $scope.desigIDs = [];

        /*region start WORKFLOW*/
        $scope.workflowList = [];
        $scope.itemWorkflow = [];
        $scope.workflowObj = {};
        $scope.workflowObj.workflowID = 0;
        $scope.currentStep = 0;
        $scope.orderInfo = 0;
        $scope.assignToShow = '';
        $scope.isWorkflowCompleted = false;
        $scope.WorkflowModule = 'BILL_CERTIFICATION';
        $scope.disableWFSelection = false;
        /*region end WORKFLOW*/


        $scope.ListUserDepartmentDesignations = userService.getListUserDepartmentDesignations();
        if ($scope.ListUserDepartmentDesignations && $scope.ListUserDepartmentDesignations.length > 0) {
            $scope.ListUserDepartmentDesignations.forEach(function (item, index) {
                $scope.deptIDs.push(item.deptID);
                item.listDesignation.forEach(function (item1, index1) {
                    if (item1.isAssignedToUser && item1.isValid) {
                        $scope.desigIDs.push(item1.desigID);
                    }
                });
            });
        }

        $scope.getBillsBasedOnID = function () {
            $scope.billRequestsList = [];
            $scope.billAttachmentsList = [];
            $scope.params =
            {
                "PO_NUMBER": $scope.PO_NUMBER,
                "BILL_ID": $scope.BILL_ID,
                "sessionid": $scope.sessionID
            };

            PRMPOService.getBillsBasedOnID($scope.params)
                .then(function (response) {


                    if (response)
                    {

                        $scope.PO_ITEMS_FOR_BILL_REQUEST = JSON.parse(response).Table;

                        if ($scope.PO_ITEMS_FOR_BILL_REQUEST && $scope.PO_ITEMS_FOR_BILL_REQUEST.length > 0) {
                            $scope.PO_ITEMS_FOR_BILL_REQUEST.forEach(function (billItem, billIndex) {
                                billItem.TOTAL_AMOUNT = billItem.OVERALL_AMOUNT;
                                if ($scope.BILL_ID > 0)
                                {
                                    $scope.checkListObj.checkListArr = angular.fromJson(billItem.CHECK_LIST_JSON);
                                    $scope.billSubmissionObj = {
                                        BILL_TYPE: $scope.PO_ITEMS_FOR_BILL_REQUEST[0].BILL_TYPE
                                    };
                                }
                                billItem.DISPLAY = false;
                                //if (JSON.parse(response).Table1) {
                                //    billItem.MULTIPLE_ATTACHMENTS = [];
                                //    billItem.BILL_CERTIFICATES = [];
                                //    $scope.billAttachmentsList = JSON.parse(response).Table1;
                                //    if ($scope.billAttachmentsList && $scope.billAttachmentsList.length > 0) {
                                //        $scope.billAttachmentsList.forEach(function (billAttachItem, billAttachIndex) {
                                //            if (billItem.BILL_ID === billAttachItem.BILL_ID && billAttachItem.ATT_TYPE === 'BILL_ATTACHMENTS') {
                                //                var fileUpload = {
                                //                    fileName: billAttachItem.ATT_PATH,
                                //                    fileID: billAttachItem.ATT_ID,
                                //                    fileStream: []
                                //                };
                                //                billItem.MULTIPLE_ATTACHMENTS.push(fileUpload);
                                //            } else if (billItem.BILL_ID === billAttachItem.BILL_ID && billAttachItem.ATT_TYPE === 'BILL_CERTIFICATES') {
                                //                var fileUpload = {
                                //                    fileName: billAttachItem.ATT_PATH,
                                //                    fileID: billAttachItem.ATT_ID
                                //                };
                                //                billItem.BILL_CERTIFICATES.push(fileUpload);
                                //            }
                                //        });
                                //    }
                                //}
                            });
                        }
                    }
                });
        };


        if ($scope.PO_NUMBER)
        {
            $scope.getBillsBasedOnID($scope.PO_NUMBER);
        }
        
        $scope.multipleAttachments = [];
        $scope.multipleAttachments1 = [];
        $scope.selectedIndex = 0;
        $scope.getFile1 = function (id, itemid, ext) {
            $scope.filesTemp = $("#" + id)[0].files;

            $scope.filesTemp = Object.values($scope.filesTemp);
            $scope.filesTemp.forEach(function (attach, attachIndex) {
                $scope.file = $("#" + id)[0].files[attachIndex];

                    $scope.progress = 0;
                    $scope.totalRequirementSize = 0;
                    //$scope.checkListObj.checkListArr[id].MULTIPLE_ATTACHMENTS.push($scope.file);
                    //$scope.multipleAttachments = Object.values($scope.checkListObj.checkListArr[id].MULTIPLE_ATTACHMENTS);
                    //if ($scope.multipleAttachments && $scope.multipleAttachments.length > 0) {
                    //    $scope.multipleAttachments.forEach(function (item, index) {
                    //        $scope.totalRequirementSize = $scope.totalRequirementSize + item.size;
                    //    });
                    //}
                    //$scope.checkListObj.checkListArr[id].MULTIPLE_ATTACHMENTS = [];

                    //if (($scope.totalRequirementSize + $scope.totalRequirementItemSize) > $scope.totalAttachmentMaxSize) {
                    //    swal({
                    //        title: "Attachment size!",
                    //        text: "Total Attachments size cannot exceed 6MB",
                    //        type: "warning",
                    //        showCancelButton: false,
                    //        confirmButtonColor: "#DD6B55",
                    //        confirmButtonText: "Ok",
                    //        closeOnConfirm: true
                    //    },
                    //        function () {
                    //            return;
                    //        });
                    //    return;
                    //}
 

                $scope.filesTemp.forEach(function (attach, attachIndex) {
                    $scope.file = $("#" + id)[0].files[attachIndex];
                    fileReader.readAsDataUrl($scope.file, $scope)
                        .then(function (result) {
                            var fileUpload = {
                                fileStream: [],
                                fileName: '',
                                fileID: 0
                            };
                            var bytearray = new Uint8Array(result);
                            fileUpload.fileStream = $.makeArray(bytearray);
                            fileUpload.fileName = attach.name;
                            if (!$scope.checkListObj.checkListArr[id].MULTIPLE_ATTACHMENTS) {
                                $scope.checkListObj.checkListArr[id].MULTIPLE_ATTACHMENTS = [];
                            }

                            var ifExists = _.findIndex($scope.checkListObj.checkListArr[id].MULTIPLE_ATTACHMENTS, function (attach) { return attach.fileName.toLowerCase() === fileUpload.fileName.toLowerCase() });

                            if (ifExists <= -1) {
                                $scope.checkListObj.checkListArr[id].MULTIPLE_ATTACHMENTS.push(fileUpload);
                            }
                        });
                });

            });
            $("#").val('');
        };
        $scope.checkListObj.checkListArrTemp = [];
        $scope.createBillSubmission = function ()
        {
            //if ($scope.PO_ITEMS_FOR_BILL_REQUEST && $scope.PO_ITEMS_FOR_BILL_REQUEST.length > 0)
            //{
            //    $scope.areAllMandateFieldsFilled = false;

            //    $scope.PO_ITEMS_FOR_BILL_REQUEST.forEach(function (item, index) {
            //        item.uploadBillSubmissionValidation = false;
            //        item.billRequestAmountValidation = false;
            //        if (item.MULTIPLE_ATTACHMENTS && item.MULTIPLE_ATTACHMENTS.length <= 0)
            //        {
            //            item.uploadBillSubmissionValidation = true;
            //        }
            //        if (item.BILL_REQUEST_AMOUNT <= 0) {
            //            item.billRequestAmountValidation = true;
            //        }
            //    });
            //} else {
            //    swal("Error!", "Please select PO items to create a bill request.", "error");
            //}

            //let isAnyMandateFieldNotFilled = $scope.validateMandateFields();
            //if (isAnyMandateFieldNotFilled) {
            //    return;
            //}

            $scope.billRequestArr = [];
            
            $scope.PO_ITEMS_FOR_BILL_REQUEST.forEach(function (poItem, poIndex) {
                $scope.checkListObj.checkListArrTemp = $scope.checkListObj.checkListArr
                if (poItem.PO_NUMBER)
                {
                    var obj =
                    {
                        "V_COMP_ID": $scope.COMP_ID,
                        "C_COMP_ID": $scope.customerCompanyId,
                        "CREATED_BY": $scope.USER_ID,
                        "PROJECT_ID": poItem.PROJECT_ID,
                        "PACKAGE_ID": poItem.PACKAGE_ID,
                        "SUB_PACKAGE_ID": poItem.SUB_PACKAGE_ID,
                        "PO_VALUE": poItem.TOTAL_AMOUNT,
                        "MULTIPLE_ATTACHMENTS": poItem.MULTIPLE_ATTACHMENTS,
                        "BILL_REQUEST_AMOUNT": poItem.BILL_REQUEST_AMOUNT,
                        "PO_NUMBER": poItem.PO_NUMBER,
                        "BILL_ID": poItem.BILL_ID,
                        "ROW_ID": +$scope.BILL_ID === 0 ? 0 : poItem.ROW_ID,
                        "PO_ID": poItem.ROW_ID,
                        "CHECK_LIST_JSON": JSON.stringify($scope.checkListObj.checkListArrTemp),
                        "BILL_TYPE": $scope.billSubmissionObj.BILL_TYPE,
                        "IS_VALID": 1,
                    };
                    $scope.billRequestArr.push(obj);
                }
            });

            $scope.params =
            {
                "billRequestList": $scope.billRequestArr,
                "BILL_ID": $scope.BILL_ID,
                "sessionid": $scope.sessionID
            };
            $scope.isFormSubmitted = true;
            PRMPOService.createBillSubmission($scope.params)
                .then(function (response) {

                    if (response.errorMessage != '') {
                        growlService.growl(response.errorMessage, "inverse");
                    } else {
                        $scope.isFormSubmitted = false;
                        growlService.growl("Bill Submission Done", "success");
                        $state.go('billRequestsList');
                    }

                });
        };


        $scope.validateMandateFields = function ()
        {
            let isValid = false;
            isValid = _.some($scope.PO_ITEMS_FOR_BILL_REQUEST, function (item) {
                return (item.billRequestValidation);
            });
            return isValid;
        };


        $scope.validateAmount = function (obj)
        {
            obj.billRequestValidation = false;
            if (+obj.BILL_REQUEST_AMOUNT > +obj.TOTAL_AMOUNT)
            {
                obj.billRequestValidation = true;
                obj.BILL_REQUEST_AMOUNT = 0;
            }
        };

        $scope.removeAttach = function (parentIndex,index) {
            $scope.checkListObj.checkListArr[parentIndex].MULTIPLE_ATTACHMENTS.splice(index, 1);
        };

        $scope.cancelBillSubmissionPopup = function () {
            $state.go('billRequestsList');
        };

        $scope.cancelBillCertificationPopup = function () {
            angular.element('#billCertificationPopup').modal('hide');
        };


        $scope.validateCheckLists = function () {
            if ($scope.billSubmissionObj.BILL_TYPE != 'Advance') {
                $scope.checkListObj.checkListArr.splice(1, 1);
            } else {
                var obj =
                {
                    isChecked: true,
                    value: "Proforma Invoice."
                };
                $scope.checkListObj.checkListArr.splice(1, 0, obj);
            }
        };
        
    }]);