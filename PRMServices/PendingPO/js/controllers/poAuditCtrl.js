
prmApp.controller('poAuditCtrl', ["$scope", "$stateParams", "$log", "$state", "$window", "userService", "auctionsService", "storeService", "growlService", "PRMProjectServices", "$filter", "fileReader", "$rootScope","PRMPOService",
    function ($scope, $stateParams, $log, $state, $window, userService, auctionsService, storeService, growlService, PRMProjectServices, $filter, fileReader, $rootScope, PRMPOService) {
        $scope.USER_ID = userService.getUserId();
        $scope.COMP_ID = userService.getUserCompanyId();
        $scope.sessionID = userService.getUserToken();
        $scope.PO_NUMBER = decodeURIComponent($stateParams.PO_NUMBER);


        $scope.tableColumns = [];
        $scope.tableValues = [];


        $scope.getPoAudit = function () {
            $scope.params =
            {
                "COMP_ID": $scope.COMP_ID,
                "PO_NUMBER": $scope.PO_NUMBER,
                "sessionid": userService.getUserToken()
            };

            PRMPOService.GetPoAudit($scope.params)
                .then(function (response) {
                    if (response) {

                        var arr = JSON.parse(response).Table;                        
                        if (arr && arr.length > 0) {
                            $scope.tableColumnsTemp = angular.copy(_.keys(arr[0]));
                            $scope.tableColumnsTemp.forEach(function (item, index) {
                                item = item.replaceAll("_", " ");
                                $scope.tableColumns.push(item);
                            });
                            $scope.rows = arr;
                            arr.forEach(function (item, index) {
                                var obj = angular.copy(_.values(item));
                                if (obj) {
                                    item.tableValues = [];
                                    obj.forEach(function (value, valueIndex) {
                                        item.tableValues.push(value);
                                    });
                                }
                            });
                        }
                    }
                });
        }
        $scope.getPoAudit($scope.PO_NUMBER);

    }]);