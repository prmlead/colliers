prmApp.controller('billRequestsListCtrl', ["$scope", "$stateParams", "$log", "$state", "$window", "userService", "auctionsService", "storeService", "growlService", "PRMProjectServices", "$filter", "fileReader", "$rootScope", "PRMPOService", "workflowService",
    function ($scope, $stateParams, $log, $state, $window, userService, auctionsService, storeService, growlService, PRMProjectServices, $filter, fileReader, $rootScope, PRMPOService, workflowService) {

        $scope.USER_ID = +userService.getUserId();
        $scope.COMP_ID = +userService.getUserCompanyId();
        $scope.customerCompanyId = +userService.getCustomerCompanyId();
        $scope.sessionID = userService.getUserToken();
        $scope.isSuperUser = userService.getUserObj().isSuperUser;
        $scope.isVendor = (userService.getUserType() == "VENDOR") ? true : false;
        $scope.billRequestsList = [];



        $scope.checkListObj =
        {
            checkListArr:
                [
                    {
                        isChecked: true,
                        value: "PO Acceptance copy submission / Variation order copy.",
                        MULTIPLE_ATTACHMENTS: []
                    },
                    {
                        isChecked: true,
                        value: "Proforma Invoice.",
                        MULTIPLE_ATTACHMENTS: []
                    },
                    {
                        isChecked: true,
                        value: "MAR's.",
                        MULTIPLE_ATTACHMENTS: []
                    },
                    {
                        isChecked: true,
                        value: "DC' copies.",
                        MULTIPLE_ATTACHMENTS: []
                    },
                    {
                        isChecked: true,
                        value: "Joint Measurement Sheets.",
                        MULTIPLE_ATTACHMENTS: []
                    },
                    {
                        isChecked: true,
                        value: "Reference drawings supporting measurements.",
                        MULTIPLE_ATTACHMENTS: []
                    },
                    {
                        isChecked: true,
                        value: "Backup documents including invoices related to Basic rate variation.",
                        MULTIPLE_ATTACHMENTS: []
                    },
                    {
                        isChecked: true,
                        value: "Non Tendered item backups.",
                        MULTIPLE_ATTACHMENTS: []
                    },
                    {
                        isChecked: true,
                        value: "Other documents.",
                        MULTIPLE_ATTACHMENTS: []
                    }
                ]
        };

        //Pagination
        $scope.totalItems = 0;
        $scope.currentPage = 1;
        $scope.itemsPerPage = 10;
        $scope.maxSize = 10;


        $scope.setPage = function (pageNo) {
            $scope.currentPage = pageNo;
            $scope.getBillRequests(($scope.currentPage - 1), 10);
        };

            //^Pagination
        $scope.filters =
        {
            poNumbers: {},
            projects: {},
            packages: {},
            subPackages: {},
            vendors: {},
            billStatus: {}
        };
        $scope.filters = {
            poNumbers: [],
            projects: [],
            packages: [],
            subPackages: [],
            vendors: [],
            billStatus: []

        };
        $scope.filtersList = [];
        $scope.filters.billToDate = moment().format('YYYY-MM-DD');
        $scope.filters.billFromDate = moment().subtract(30, "days").format("YYYY-MM-DD");

        $scope.setFilters = function ()
        {
            if ($scope.loadServices) {
                $scope.getBillRequests(0, 10);
            }
        };
        $scope.filterByDate = function () {
            $scope.getBillRequests(0, 10);
            $scope.getBillRequestsFilterValues();
        };

        $scope.deptIDs = [];
        $scope.desigIDs = [];

        /*region start WORKFLOW*/
        $scope.workflowList = [];
        $scope.itemWorkflow = [];
        $scope.workflowObj = {};
        $scope.workflowObj.workflowID = 0;
        $scope.currentStep = 0;
        $scope.orderInfo = 0;
        $scope.assignToShow = '';
        $scope.isWorkflowCompleted = false;
        $scope.WorkflowModule = 'BILL_CERTIFICATION';
        $scope.disableWFSelection = false;
        /*region end WORKFLOW*/


        $scope.ListUserDepartmentDesignations = userService.getListUserDepartmentDesignations();
        if ($scope.ListUserDepartmentDesignations && $scope.ListUserDepartmentDesignations.length > 0) {
            $scope.ListUserDepartmentDesignations.forEach(function (item, index) {
                $scope.deptIDs.push(item.deptID);
                item.listDesignation.forEach(function (item1, index1) {
                    if (item1.isAssignedToUser && item1.isValid) {
                        $scope.desigIDs.push(item1.desigID);
                    }
                });
            });
        }

        $scope.filters.billToDate = moment().format('YYYY-MM-DD');
        $scope.filters.billFromDate = moment().subtract(30, "days").format("YYYY-MM-DD");


        $scope.getBillRequests = function (recordsFetchFrom, pageSize) {
            var billFromDate, billToDate, poNumbers, projectIds, billsStatus, packageIds, subPackageIds, vendorIds;
           

            if (_.isEmpty($scope.filters.billFromDate)) {
                billFromDate = '';
            } else {
                billFromDate = $scope.filters.billFromDate;
            }
            if (_.isEmpty($scope.filters.billToDate)) {
                billToDate = '';
            } else {
                billToDate = $scope.filters.billToDate;
            }

            if (_.isEmpty($scope.filters.poNumbers)) {
                poNumbers = '';
            } else if ($scope.filters.poNumbers && $scope.filters.poNumbers.length > 0) {
                let temp = _($scope.filters.poNumbers)
                    .filter(item => item.id)
                    .map('id')
                    .value();
                poNumbers = temp.join(',');
            }

            if (_.isEmpty($scope.filters.projects)) {
                projectIds = '';
            } else if ($scope.filters.projects && $scope.filters.projects.length > 0) {
                let temp = _($scope.filters.projects)
                    .filter(item => item.id)
                    .map('id')
                    .value();
                projectIds = temp.join(',');
            }

            if (_.isEmpty($scope.filters.billStatus)) {
                billsStatus = '';
            } else if ($scope.filters.billStatus && $scope.filters.billStatus.length > 0) {
                let temp = _($scope.filters.billStatus)
                    .filter(item => item.name)
                    .map('name')
                    .value();
                billsStatus = temp.join(',');
            }

            if (_.isEmpty($scope.filters.packages)) {
                packageIds = '';
            } else if ($scope.filters.packages && $scope.filters.packages.length > 0) {
                let temp = _($scope.filters.packages)
                    .filter(item => item.id)
                    .map('id')
                    .value();
                packageIds = temp.join(',');
            }

            if (_.isEmpty($scope.filters.subPackages)) {
                subPackageIds = '';
            } else if ($scope.filters.subPackages && $scope.filters.subPackages.length > 0) {
                let temp = _($scope.filters.subPackages)
                    .filter(item => item.id)
                    .map('id')
                    .value();
                subPackageIds = temp.join(',');
            }

            if (_.isEmpty($scope.filters.vendors)) {
                vendorIds = '';
            } else if ($scope.filters.vendors && $scope.filters.vendors.length > 0) {
                let temp = _($scope.filters.vendors)
                    .filter(item => item.id)
                    .map('id')
                    .value();
                vendorIds = temp.join(',');
            }


            $scope.billRequestsList = [];
            $scope.billAttachmentsList = [];
            $scope.initialBillArray = [];
            $scope.params =
            {
                "COMP_ID": $scope.COMP_ID,
                "U_ID": $scope.USER_ID,
                "IS_CUSTOMER": ($scope.isVendor ? 0 : 1),
                "poNumber": poNumbers,
                "projectIds": projectIds,
                "billsStatus": billsStatus,
                "packageIds": packageIds,
                "subPackageIds": subPackageIds,
                "vendorIds": vendorIds,
                "fromDate": billFromDate,
                "toDate": billToDate,
                "PageSize": recordsFetchFrom * pageSize,
                "NumberOfRecords": pageSize,
                "sessionid": $scope.sessionID,
            };

            PRMPOService.getBillRequests($scope.params)
                .then(function (response) {

                    $scope.loadServices = true;
                    if (response)
                    {
                        $scope.billRequestsList = JSON.parse(response).Table;
                        $scope.totalItems = 0;

                        if ($scope.billRequestsList && $scope.billRequestsList.length > 0) {
                            $scope.billRequestsList.forEach(function (billItem, billIndex) {
                                billItem.DISPLAY = false;
                                billItem.checkListArr = [];
                                if (billItem.CHECK_LIST_JSON)
                                {
                                    var checkLists = angular.fromJson(billItem.CHECK_LIST_JSON);//billItem.checkListArr
                                    if (checkLists && checkLists.length > 0)
                                    {
                                        checkLists.forEach(function (checkItem,index) {
                                            billItem.checkListArr.push(checkItem);
                                        });
                                    }
                                }
                                $scope.initialBillArray.push(billItem);
                                if (JSON.parse(response).Table1) {
                                    billItem.ATTACHMENTS_ARRAY = [];
                                    billItem.BILL_CERTIFICATES = [];
                                    $scope.billAttachmentsList = JSON.parse(response).Table1;
                                    if ($scope.billAttachmentsList && $scope.billAttachmentsList.length > 0) {
                                        $scope.billAttachmentsList.forEach(function (billAttachItem, billAttachIndex) {

                                            if (billItem.BILL_ID === billAttachItem.BILL_ID && billAttachItem.ATT_TYPE === 'BILL_CERTIFICATES') {
                                                var fileUpload = {
                                                    fileName: billAttachItem.ATT_PATH,
                                                    fileID: billAttachItem.ATT_ID
                                                };
                                                billItem.BILL_CERTIFICATES.push(fileUpload);
                                            }
                                        });
                                        
                                    }
                                }
                            });


                            $scope.billRequestsList = _.orderBy($scope.billRequestsList, ['BILL_ID'], ['desc']);
                            //$scope.totalItems = $scope.billRequestsList.length;
                        }

                        if ($scope.billRequestsList && $scope.billRequestsList.length > 0) {
                            $scope.totalItems = $scope.billRequestsList[0].TOTAL_ROWS;
                            $scope.filteredbillRequestsList = $scope.billRequestsList;
                        }
                    } else {
                        $scope.filteredbillRequestsList = [];
                    }
                });
        };
        $scope.loadServices = false;
        $scope.getBillRequestsFilterValues = function ()
        {
            $scope.params =
            {
                "COMP_ID": $scope.COMP_ID,
                "U_ID": $scope.USER_ID,
                "IS_CUSTOMER": ($scope.isVendor ? 0 : 1),
                "FROM_DATE": $scope.filters.billFromDate,
                "TO_DATE": $scope.filters.billToDate,
                "sessionid": $scope.sessionID
            };

            let projectsTemp = [];
            let packagesTemp = [];
            let subPackagesTemp = [];
            let vendorsTemp = [];
            let poNumbersTemp = [];
            let billStatusTemp = [];

            PRMPOService.getBillRequestsFilterValues($scope.params)
                .then(function (response) {
                    $scope.loadServices = false;
                    $scope.getBillRequests(0, 10);
                    if (response)
                    {
                        $scope.filterValues = JSON.parse(response).Table;

                        $scope.filterValues.forEach(function (item, index) {
                            if (item.TYPE === 'PACKAGE') {
                                packagesTemp.push({ id: item.ID, name: item.NAME });
                            } else if (item.TYPE === 'PROJECT') {
                                projectsTemp.push({ id: item.ID, name: item.NAME });
                            } else if (item.TYPE === 'SUB_PACKAGE') {
                                subPackagesTemp.push({ id: item.ID, name: item.NAME });
                            } else if (item.TYPE === 'VENDORS') {
                                vendorsTemp.push({ id: item.ID, name: item.NAME });
                            } else if (item.TYPE === 'PO_NUMBER') {
                                poNumbersTemp.push({ id: item.ID, name: item.NAME });
                            }
                            else if (item.TYPE === 'BILL_CERTIFICATION_STATUS') {
                                if (item.NAME) {
                                    billStatusTemp.push({ id: item.ID, name: item.NAME });
                                }
                            }
                        });

                        $scope.filtersList.packagesList = packagesTemp;
                        $scope.filtersList.projectsList = projectsTemp;
                        $scope.filtersList.subPackagesList = subPackagesTemp;
                        $scope.filtersList.vendorList = vendorsTemp;
                        $scope.filtersList.poNumbersList = poNumbersTemp;
                        $scope.filtersList.billStatusList = billStatusTemp
                    }

                });
        };

        //$scope.getBillRequests(0,10);
        $scope.getBillRequestsFilterValues();


        $scope.createBillRequest = function (id,poNumber,request)
        {
            if (id <= 0) {
                $state.go("createBillRequest", { "poNumber": poNumber, "ID": id });
            } else if (id > 0 && request === 'EDIT') {
                $state.go("editBillRequest", { "poNumber": poNumber, "ID": id });
            } else {
                $state.go("viewBillRequest", { "poNumber": poNumber, "ID": id });
            }
        };

        $scope.billSubmissionObj = {};

        $scope.acknowledgeOrRejectBillRequest = function (billObject, isAck) {
            $scope.billSubmissionObj.MODIFIED_BY = $scope.USER_ID;
            $scope.billSubmissionObj.BILL_REQUEST_STATUS = isAck === 1 ? 'ACKNOWLEDGED' : 'REJECTED';
            $scope.billSubmissionObj.PO_NUMBER = billObject.PO_NUMBER
            $scope.billSubmissionObj.BILL_ID = billObject.BILL_ID

            var params =
            {
                "billSubmission": $scope.billSubmissionObj,
                "sessionid": $scope.sessionID
            };

            PRMPOService.acknowledgeOrRejectBillRequest(params)
                .then(function (response) {

                    if (response.errorMessage != '') {
                        growlService.growl(response.errorMessage, "error");
                    } else {
                        growlService.growl("Bill Submission " + $scope.billSubmissionObj.BILL_REQUEST_STATUS, "success");
                        $scope.getBillRequests(0,10);
                    }


                });

        };


        $scope.showBillCertificationBox = function (billRequest, value) {
            if (value) {
                billRequest.DISPLAY = true;
                $scope.billValues = billRequest;
                $scope.billValues.MULTIPLE_ATTACHMENTS_BILL_CERTIFICATION = [];

                angular.element('#billCertificationPopup').modal('show');
            } else {
                billRequest.DISPLAY = false;
            }
        };


        $scope.multipleAttachments = [];
        $scope.multipleAttachments1 = [];
        $scope.selectedIndex = 0;
        $scope.getFile1 = function (id, itemid, ext) {
            $scope.filesTemp = $("#" + id)[0].files;

            $scope.filesTemp = Object.values($scope.filesTemp);
            $scope.filesTemp.forEach(function (attach, attachIndex) {
                $scope.file = $("#" + id)[0].files[attachIndex];

                $scope.progress = 0;
                $scope.totalRequirementSize = 0;
                //$scope.checkListObj.checkListArr[id].MULTIPLE_ATTACHMENTS.push($scope.file);
                //$scope.multipleAttachments = Object.values($scope.checkListObj.checkListArr[id].MULTIPLE_ATTACHMENTS);
                //if ($scope.multipleAttachments && $scope.multipleAttachments.length > 0) {
                //    $scope.multipleAttachments.forEach(function (item, index) {
                //        $scope.totalRequirementSize = $scope.totalRequirementSize + item.size;
                //    });
                //}
                //$scope.checkListObj.checkListArr[id].MULTIPLE_ATTACHMENTS = [];

                //if (($scope.totalRequirementSize + $scope.totalRequirementItemSize) > $scope.totalAttachmentMaxSize) {
                //    swal({
                //        title: "Attachment size!",
                //        text: "Total Attachments size cannot exceed 6MB",
                //        type: "warning",
                //        showCancelButton: false,
                //        confirmButtonColor: "#DD6B55",
                //        confirmButtonText: "Ok",
                //        closeOnConfirm: true
                //    },
                //        function () {
                //            return;
                //        });
                //    return;
                //}


                $scope.filesTemp.forEach(function (attach, attachIndex) {
                    $scope.file = $("#" + id)[0].files[attachIndex];
                    fileReader.readAsDataUrl($scope.file, $scope)
                        .then(function (result) {
                            var fileUpload = {
                                fileStream: [],
                                fileName: '',
                                fileID: 0
                            };
                            var bytearray = new Uint8Array(result);
                            fileUpload.fileStream = $.makeArray(bytearray);
                            fileUpload.fileName = attach.name;
                            if (!$scope.billValues.checkListArr[id].MULTIPLE_ATTACHMENTS) {
                                $scope.billValues.checkListArr[id].MULTIPLE_ATTACHMENTS = [];
                            }

                            var ifExists = _.findIndex($scope.billValues.checkListArr[id].MULTIPLE_ATTACHMENTS, function (attach) { return attach.fileName.toLowerCase() === fileUpload.fileName.toLowerCase() });

                            if (ifExists <= -1) {
                                $scope.billValues.checkListArr[id].MULTIPLE_ATTACHMENTS.push(fileUpload);
                            }
                        });
                });

            });
            $("#").val('');
        };

        $scope.removeAttach = function (parentIndex,index)
        {
            $scope.billValues.checkListArr[parentIndex].MULTIPLE_ATTACHMENTS.splice(index, 1);
            //$scope.billValues.MULTIPLE_ATTACHMENTS_BILL_CERTIFICATION.splice(index, 1);
        };

        $scope.checkListObj.checkListArrTemp = [];
        $scope.ERROR_MESSAGE = '';
        $scope.uploadBillCertification = function () {
            $scope.ERROR_MESSAGE = '';
            $scope.billValues.MODIFIED_BY = $scope.USER_ID;

            var workflows = _.filter($scope.workflowListTemp, { WorkflowModule: 'BILL_CERTIFICATION' });

            //if (workflows && workflows.length <= 0) {
            //    $scope.ERROR_MESSAGE = 'Cannot submit bill certificates/bill certified amount until there is a bill submission workflow.';
            //    //swal("Error!", "", "error");
            //    return;
            //}

            if ($scope.billValues.BILL_CERTIFIED_AMOUNT <= 0) {
                $scope.ERROR_MESSAGE = 'Please enter Bill Certification Amount Greater Than Zero.';
                //swal("Error!", "Please enter Bill Certification Amount.", "error");
                return;
            }

            $scope.billValues.BILL_CERTIFICATE_WF_ID = 0;
            $scope.checkListObj.checkListArrTemp = $scope.billValues.checkListArr
            $scope.billCertificates =
            {
                BILL_ID: $scope.billValues.BILL_ID,
                PO_NUMBER: $scope.billValues.PO_NUMBER,
                MODIFIED_BY: $scope.billValues.MODIFIED_BY,
                BILL_CERTIFIED_AMOUNT: +$scope.billValues.BILL_CERTIFIED_AMOUNT,
                BILL_CERTIFICATE_WF_ID: $scope.billValues.BILL_CERTIFICATE_WF_ID,
                CHECK_LIST_JSON: JSON.stringify($scope.checkListObj.checkListArrTemp),
            };

            $scope.params =
            {
                "billCertificates": $scope.billCertificates,
                "sessionid": $scope.sessionID
            };

            PRMPOService.uploadBillCertification($scope.params)
                .then(function (response) {

                    if (response.errorMessage != '') {
                        $scope.ERROR_MESSAGE = response.errorMessage;
                        $scope.billValues.BILL_CERTIFIED_AMOUNT = 0;
                    } else {
                        growlService.growl("Bill Certification Done", "success");
                        location.reload();
                        angular.element('#billCertificationPopup').modal('hide');
                    }

                });
        };


        $scope.cancelBillCertificationPopup = function () {
            angular.element('#billCertificationPopup').modal('hide');
            location.reload();
        };

        /*region start Delete Bill Request*/
        $scope.deleteBillRequest = function (obj,value) {
            var params = {
                "BILL_ID": obj.BILL_ID,
                "IS_VALID": value,
                "MODIFIED_BY": $scope.USER_ID,
                "sessionid": userService.getUserToken()
            };
            PRMPOService.deactiveBillRequest(params)
                .then(function (response) {

                    if (response.errorMessage != '') {
                        growlService.growl("Error Occured", "inverse");
                    } else {
                        growlService.growl("Bill Deactivated Sucessfully.", "success");
                        $scope.getBillRequests(0, 1000);
                    }

                });
        };

        /*region End Delete Bill Request*/


        /*region start WORKFLOW*/

        $scope.getWorkflows = function () {
            workflowService.getWorkflowList($scope.customerCompanyId)
                .then(function (response) {
                    $scope.workflowList = [];
                    $scope.workflowListDeptWise = [];
                    $scope.workflowListSubPackage = [];
                    $scope.workflowListTemp = response;

                    $scope.workflowListTemp.forEach(function (item, index) {
                        if (item.WorkflowModule === $scope.WorkflowModule) {
                            $scope.workflowList.push(item);
                            $scope.workflowListDeptWise.push(item);
                        }
                    });

                    if ($scope.isSuperUser) {
                        $scope.workflowList = $scope.workflowList;
                    }
                    else {
                        $scope.workflowList = [];
                        $scope.workflowListDeptWise.forEach(function (wf, idx) {
                            $scope.deptIDs.forEach(function (dep) {
                                if (dep == wf.deptID) {
                                    $scope.workflowList.push(wf);
                                }
                            });
                        });

                    }
                });
        };

        $scope.getWorkflows();

        $scope.getItemWorkflow = function (ID, workflowModule) {
            workflowService.getItemWorkflow(0, ID, workflowModule)
                .then(function (response) {
                    $scope.itemWorkflow = response;
                    $scope.checkIsFormDisable();
                    if ($scope.itemWorkflow && $scope.itemWorkflow.length > 0 && $scope.itemWorkflow[0].WorkflowTracks.length > 0) {
                        $scope.currentStep = 0;

                        var count = 0;

                        $scope.itemWorkflow[0].WorkflowTracks.forEach(function (track) {
                            if (!track.multipleAttachments) {
                                track.multipleAttachments = [];
                            }

                            if (track.status === 'APPROVED') { track.statusNew = 'Approved'; }
                            if (track.status === 'HOLD') { track.statusNew = 'Hold'; }
                            if (track.status === 'PENDING') { track.statusNew = 'Pending'; }
                            if (track.status === 'REJECTED') { track.statusNew = 'Rejected'; }

                            if (track.status === 'APPROVED' || track.status === 'HOLD') {
                                $scope.isFormdisabled = true;
                            }

                            if (track.status === 'APPROVED') {
                                $scope.isWorkflowCompleted = true;
                                $scope.orderInfo = track.order;
                                $scope.assignToShow = track.status;

                            }
                            else {
                                $scope.isWorkflowCompleted = false;
                            }

                            if (track.status === 'REJECTED' && count == 0) {
                                count = count + 1;
                            }

                            if ((track.status === 'PENDING' || track.status === 'HOLD') && count == 0) {
                                count = count + 1;
                                //$scope.IsUserApproverForStage(track.approverID);
                                $scope.currentAccess = track.order;
                            }

                            if ((track.status === 'PENDING' || track.status === 'HOLD' || track.status === 'REJECTED') && $scope.currentStep === 0) {
                                $scope.currentStep = track.order;
                                return false;
                            }
                        });
                    }
                });
        };


        $scope.isApproverDisable = function (index) {

            var disable = true;

            var previousStep = {};

            $scope.itemWorkflow[0].WorkflowTracks.forEach(function (step, stepIndex) {

                if (index == stepIndex) {
                    if (stepIndex == 0) {
                        if ($scope.isUserBelongsToDeptandDesig(step.department.deptID, step.approver.desigID) &&
                            (step.status === 'PENDING' || step.status === 'HOLD')) {
                            disable = false;
                        }
                        else {
                            disable = true;
                        }
                    }
                    else if (stepIndex > 0) {
                        if (previousStep.status === 'PENDING' || previousStep.status === 'HOLD' || previousStep.status === 'REJECTED') {
                            disable = true;
                        }
                        else if ($scope.isUserBelongsToDeptandDesig(step.department.deptID, step.approver.desigID) &&
                            (step.status === 'PENDING' || step.status === 'HOLD')) {
                            disable = false;
                        }
                        else {
                            disable = true;
                        }
                    }
                }
                previousStep = step;
            });

            return disable;
        };

        $scope.isUserBelongsToDeptandDesig = function (deptID, desigID) {
            var isEligible = true;

            if ($scope.deptIDs.indexOf(deptID) != -1 && $scope.desigIDs.indexOf(desigID) != -1) {
                isEligible = true;
            } else {
                isEligible = false;
            }

            return isEligible;
        };

        $scope.updateTrack = function (step, status, type) {
            $scope.disableAssignPR = true;
            $scope.commentsError = '';
            if (step.comments != null || step.comments != "" || step.comments != undefined) {
                step.comments = validateStringWithoutSpecialCharacters(step.comments);
            }
            var tempArray = $scope.itemWorkflow[0].WorkflowTracks[$scope.itemWorkflow[0].WorkflowTracks.length - 1];
            if (step.order == tempArray.order && status === 'APPROVED') {
                $scope.disableAssignPR = false;
            } else {
                $scope.disableAssignPR = true;
            }

            if ($scope.isReject) {
                $scope.commentsError = 'Please Save Rejected Items/Qty';
                return false;
            }

            if (status === 'REJECTED' && (step.comments == null || step.comments == "")) {
                $scope.commentsError = 'Please enter comments';
                return false;
            }

            step.status = status;
            step.sessionID = $scope.sessionID;
            step.modifiedBy = +userService.getUserId();

            step.moduleName = $scope.WorkflowModule;

            step.subModuleName = '';
            step.subModuleID = 0;

            workflowService.SaveWorkflowTrack(step)
                .then(function (response) {
                    if (response.errorMessage) {
                        growlService.growl(response.errorMessage, "inverse");
                    }
                    else {
                        $scope.getItemWorkflow();
                        location.reload();
                    }
                });
        };


        $scope.isFormdisabled = true;

        $scope.checkIsFormDisable = function () {
            $scope.isFormdisabled = false;
            //if ($scope.itemWorkflow.length == 0) {
            //    $scope.isFormdisabled = true;
            //} else {
            //    if (($scope.BudgetList.CREATED_BY == +userService.getUserId() || $scope.BudgetList.MODIFIED_BY == +userService.getUserId()) && $scope.itemWorkflow[0].WorkflowTracks.length > 0 &&
            //        $scope.itemWorkflow[0].WorkflowTracks[0].status !== "APPROVED" && $scope.itemWorkflow[0].WorkflowTracks[0].order == 1 && $scope.itemWorkflow[0].workflowID > 0) {
            //        $scope.isFormdisabled = true;
            //    }
            //}
        };

        function validateStringWithoutSpecialCharacters(string) {
            if (string) {
                string = string.replace(/\'/gi, "");
                string = string.replace(/\"/gi, "");
                string = string.replace(/[`^_|\?;:'",<>\{\}\[\]\\\/]/gi, "");
                string = string.replace(/(\r\n|\n|\r)/gm, "");
                string = string.replace(/\t/g, '');
                return string;
            }
        }

        $scope.showApprovedDate = function (date) {
            return userService.toLocalDate(date);
        };

        $scope.showBillRequestApprovalPopUp = true;

        $scope.showBillRequestApproval = function () {
            $scope.showBillRequestApprovalPopUp = false;
        };

        $scope.routeToInvoices = function (billRequest) {
            // $state.go("createInvoices", { "PO_NUMBER": billRequest.PO_NUMBER, "PROJECT_ID": billRequest.PROJECT_ID, "PACKAGE_ID": billRequest.PACKAGE_ID, "SUB_PACKAGE_ID": billRequest.SUB_PACKAGE_ID, "BILL_ID": billRequest.BILL_ID});
            if (billRequest.INVOICE_ID == 0) {
                $state.go("createInvoices", { "PO_NUMBER": billRequest.PO_NUMBER, "BILL_ID": billRequest.BILL_ID, "INVOICE_ID": billRequest.INVOICE_ID });
            } else {
                growlService.growl("Invoice Has Been Created Against this Bill Request ID" + ' ' + billRequest.BILL_ID + ' ', 'inverse');
                $state.go("listInvoices")
            }
        };

        $scope.showCheckLists = function (billRequest,type) {
            $scope.checkListsPopup = billRequest;
            $scope.showAttachments = type;
            angular.element('#checkListPopup').modal('show');
        };

        $scope.showIfAttachments = function (eachBillRequest)
        {
            let isValid = false;
            isValid = _.some(eachBillRequest.checkListArr, function (item) {
                return (item.MULTIPLE_ATTACHMENTS && item.MULTIPLE_ATTACHMENTS.length > 0);
            });
            return isValid;
        };

        $scope.isProjectMgmtUser = false;
        $scope.deptIDs = [];
        $scope.ListUserDepartmentDesignations = userService.getListUserDepartmentDesignations();
        if ($scope.ListUserDepartmentDesignations && $scope.ListUserDepartmentDesignations.length > 0) {
            $scope.ListUserDepartmentDesignations.forEach(function (item, index) {

                if (!$scope.isProjectMgmtUser && item.deptCode.toUpperCase().includes("PROJECT_MANAGER")) {
                    $scope.isProjectMgmtUser = true;
                }
                $scope.deptIDs.push(item.deptID);
            });
        }

    }]);