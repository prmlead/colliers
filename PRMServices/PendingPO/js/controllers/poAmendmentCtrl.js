﻿prmApp
    .controller('poAmendmentCtrl', ["$rootScope", "$scope", "$stateParams", "$log", "$state", "$window", "userService", "auctionsService", "growlService", "PRMPOService",
        "fileReader", "$uibModal", "$filter", "PRMProjectServices", "catalogService", "workflowService",
        function ($rootScope, $scope, $stateParams, $log, $state, $window, userService, auctionsService, growlService, PRMPOService, fileReader, $uibModal, $filter, PRMProjectServices, catalogService, workflowService) {

            $scope.userId = userService.getUserId();
            $scope.sessionId = userService.getUserToken();
            $scope.compId = userService.getUserCompanyId();
            $scope.isCustomer = userService.getUserType() === "CUSTOMER" ? true : false;
            $scope.isSuperUser = userService.getUserObj().isSuperUser;
            $scope.selectedPODetails = null;
            $scope.selectedPOItemDetails = null;
            $scope.poDetails = [];
            $scope.poNumber = decodeURIComponent($stateParams.poID);
            $scope.PO_AMENDMENT_ID = $stateParams.PO_AMENDMENT_ID;
            $scope.catalogCompId = userService.getUserCatalogCompanyId();
            $scope.page = 0;
            $scope.PageSize = 200;
            $scope.fetchRecordsFrom = $scope.page * $scope.PageSize;
            $scope.WorkflowModule = 'PO_AMENDMENT';

            /*PAGINATION CODE*/
            $scope.totalItems = 0;
            $scope.currentPage = 1;
            $scope.itemsPerPage = 10;
            $scope.maxSize = 5;
            $scope.filters = {
                projects: [],
                packages: [],
                costCenters: [],
                vendors: []
            };

            $scope.filtersList = [];
            $scope.setPage = function (pageNo) {
                $scope.currentPage = pageNo;
                $scope.getpendingPOlist(($scope.currentPage - 1), 10);
            };

            $scope.pageChanged = function () {
                ////console.log('Page changed to: ' + $scope.currentPage);
            };

            /*PAGINATION CODE*/
            $scope.BudgetArr = [];


            $scope.deptIDs = [];
            $scope.desigIDs = [];

            /*region start WORKFLOW*/
            $scope.workflowList = [];
            $scope.itemWorkflow = [];
            $scope.workflowObj = {};
            $scope.workflowObj.workflowID = 0;
            $scope.currentStep = 0;
            $scope.orderInfo = 0;
            $scope.assignToShow = '';
            $scope.isWorkflowCompleted = false;
            $scope.WorkflowModule = 'PO_AMENDMENT';
            $scope.disableWFSelection = false;
            /*region end WORKFLOW*/

            $scope.ListUserDepartmentDesignations = userService.getListUserDepartmentDesignations();
            if ($scope.ListUserDepartmentDesignations && $scope.ListUserDepartmentDesignations.length > 0) {
                $scope.ListUserDepartmentDesignations.forEach(function (item, index) {
                    $scope.deptIDs.push(item.deptID);
                    item.listDesignation.forEach(function (item1, index1) {
                        if (item1.isAssignedToUser && item1.isValid) {
                            $scope.desigIDs.push(item1.desigID);
                        }
                    });
                });
            }

            $scope.getcategories = function (IsPaging) {
                var catNodes = [];
                catalogService.getcategories($scope.catalogCompId, $scope.fetchRecordsFrom, $scope.PageSize)
                    .then(function (response) {
                        response.forEach(function (catItem, catIndex) {
                            catItem.catNameTemp = catItem.catName.toLowerCase();
                        });
                        response = response.filter(function (item, index) {
                            return item.IS_CORE === 1 && item.nodes.length > 0;
                        });
                        $scope.companyCatalogTemp = [];
                        $scope.companyCatalog = response;
                        $scope.companyCatalogTemp = response;
                        $scope.companyCatalog = _.orderBy($scope.companyCatalog, ['isDefault'], ['desc']);

                        $scope.companyCatalog.forEach(function (item, index) {
                            let BudgetObj =
                            {
                                PROJ_ID: 0,
                                COMP_ID: +$scope.CompId,
                                U_ID: +$scope.userID,
                                PACKAGE_ID: item.isDefault ? item.catId : 0,
                                PO_VALUE: 0,
                                NET_REVENUE_MARGIN: 0,
                                TARGETED_REVENUE_BUDGET: 0,
                                SUB_PACKAGE_ID: 0,
                                SUB_PACKAGE_PO_VALUE: 0,
                                ALLOCATION_PERCENTAGE: 0,
                                EST_BUDGET_AMOUNT: 0,
                                PACKAGE_STATUS: '',
                                BUDGET_WF_ID: 0,
                                PACKAGE_NAME: item.catCode,
                                BUDGET_AMOUNT: 0,
                                IS_VALID: 1
                            };
                            if (item.isDefault) {
                                $scope.BudgetArr.push(BudgetObj);
                                // $scope.showSubPackages($scope.BudgetArr[index]);
                            }
                        });

                        if ($scope.companyCatalog && $scope.companyCatalog.length > 0) {
                            $scope.totalCount = $scope.companyCatalog[0].totalCategories;
                        }

                        $scope.data = [{
                            'compId': 0,
                            'catId': 0,
                            'catName': 'Package Index',
                            'catDesc': '',
                            'nodes': $scope.companyCatalog,
                            'catParentId': 0
                        }];


                        $scope.getPODetails();


                    });
            };

            $scope.getcategories();

            $scope.getPODetails = function () {
                if ($scope.PO_AMENDMENT_ID > 0) {
                    var params = {
                        "poNumber": $scope.poNumber,
                        "PO_AMENDMENT_ID": $scope.PO_AMENDMENT_ID,
                        "sessionId": userService.getUserToken()
                    };
                    PRMPOService.getPOAmendmentDetails(params)
                        .then(function (response) {
                            $scope.poDetails = response;
                            $scope.poDetails.forEach(function (item, index) {
                                item.IS_DISABLE = true;
                                item.PACKAGE_ID = +item.PACKAGE_ID;
                                //item.TAX_AMOUNT = (item.CGST + item.SGST + item.IGST);
                                item.TAX = item.TAX_AMOUNT;
                                item.TAX_TEMP = item.TAX_AMOUNT;
                                item.DELIVERY_DATE = item.DELIVERY_DATE ? userService.toLocalDate(item.DELIVERY_DATE).split(' ')[0] : '-';
                                item.PO_DESCRIPTION = item.PO_DESCRIPTION ? item.PO_DESCRIPTION : item.DESCRIPTION;
                                item.MULTIPLE_ATTACHMENTS = JSON.parse(item.MULTIPLE_ATTACHMENTS);
                                item.IS_NEW_PACKAGE = false;
                                if (item.PACKAGE_ID > 0) {
                                    $scope.showSubPackages(item);
                                }
                                //item.OVERALL_AMOUNT = item.TOTAL_VALUE;
                            });
                            $scope.poDetailsTemp = angular.copy($scope.poDetails);
                            $scope.getItemWorkflow($scope.PO_AMENDMENT_ID, $scope.WorkflowModule);
                        });
                } else {
                    var params = {
                        "poNumber": $scope.poNumber,
                        "sessionId": userService.getUserToken()
                    };
                    PRMPOService.getPODetails(params)
                        .then(function (response) {
                            $scope.poDetails = response;
                            $scope.poDetails.forEach(function (item, index) {
                                item.IS_DISABLE = true;
                                item.PACKAGE_ID = +item.PACKAGE_ID;
                                //item.TAX = (item.CGST + item.SGST + item.IGST);
                                item.TAX = item.TAX_AMOUNT;
                                item.TAX_TEMP = item.TAX_AMOUNT;
                                item.DELIVERY_DATE = item.DELIVERY_DATE ? moment(item.DELIVERY_DATE).format("DD-MM-YYYY") : '-';
                                item.DESCRIPTION = item.PO_DESCRIPTION ? item.PO_DESCRIPTION : item.DESCRIPTION;
                                item.MULTIPLE_ATTACHMENTS = JSON.parse(item.MULTIPLE_ATTACHMENTS);
                                item.IS_NEW_PACKAGE = false;
                                if (item.PACKAGE_ID > 0) {
                                    $scope.showSubPackages(item);
                                }
                            });
                            $scope.poDetailsTemp = angular.copy($scope.poDetails);
                        });
                }
            };

            $scope.AmendPODetails = function (isLastApprover) {
                $scope.AMEND_ARR = [];
                $scope.poDetails.forEach(function (item, index) {

                    if (item.WF_ID <= 0) {
                        var workflowID = (_.find($scope.workflowListTemp, { location: item.COST_CENTER, WorkflowModule: $scope.WorkflowModule }) ? _.find($scope.workflowListTemp, { location: item.COST_CENTER, WorkflowModule: $scope.WorkflowModule }).workflowID : 0);
                    }

                    let newObj =
                    {
                        "ROW_ID": $scope.PO_AMENDMENT_ID > 0 ? item.ROW_ID : 0,
                        "PO_AMENDMENT_ID": $scope.PO_AMENDMENT_ID,
                        "PO_SCH_ROW_ID": item.PO_SCH_ROW_ID,
                        "PO_NUMBER": item.PO_NUMBER,
                        "PROJECT_ID": item.PROJECT_ID,
                        "PACKAGE_ID": item.PACKAGE_ID,
                        "SUB_PACKAGE_ID": item.SUB_PACKAGE_ID,
                        "UNIT_PRICE": item.UNIT_PRICE,
                        "CGST": 0,
                        "SGST": 0,
                        "IGST": 0,
                        "ORDER_QTY": item.ORDER_QTY,
                        "DESCRIPTION": item.PO_DESCRIPTION,
                        "VENDOR_ID": item.VENDOR_ID,
                        "VENDOR_CODE": item.VENDOR_CODE,
                        "UOM": item.UOM,
                        "PO_STATUS": item.PO_STATUS,
                        "PO_TYPE": item.PO_TYPE,
                        "PAYMENT_TERMS": item.PAYMENT_TERMS,
                        "IS_PO_AMENDMENT_DONE": isLastApprover ? true : false,
                        "CUSTOMER_COMP_ID": +$scope.compId,
                        "CUSTOMER_USER_ID": +$scope.userId,
                        "WF_ID": workflowID,
                        "MULTIPLE_ATTACHMENTS": JSON.stringify(item.MULTIPLE_ATTACHMENTS),
                        "TOTAL_AMOUNT": item.TOTAL_AMOUNT,
                        "TAX_AMOUNT": item.TAX,
                        "IS_NEW_PACKAGE": item.IS_NEW_PACKAGE
                    };
                    $scope.AMEND_ARR.push(newObj);
                });
                var keepGoing = false;
                $scope.AMEND_ARR.forEach(function (item,index) {
                    if (!keepGoing) {
                        if (!item.PACKAGE_ID) {
                            growlService.growl("Please select a package for s.no " + (index + 1), "inverse");
                            keepGoing = true;
                            return false;
                        }

                        if (!item.SUB_PACKAGE_ID) {
                            growlService.growl("Please select a sub package for s.no " + (index + 1), "inverse");
                            keepGoing = true;
                            return false;
                        }

                        if (!item.DESCRIPTION) {
                            growlService.growl("Please enter description for s.no " + (index + 1), "inverse");
                            keepGoing = true;
                            return false;
                        }

                        if (!item.TAX_AMOUNT) {
                            growlService.growl("Please enter GST Amount for s.no " + (index + 1), "inverse");
                            keepGoing = true;
                            return false;
                        }

                        if (item.TAX_AMOUNT && item.TAX_AMOUNT <= 0) {
                            growlService.growl("Please enter GST Amount greater than zero for s.no " + (index + 1), "inverse");
                            keepGoing = true;
                            return false;
                        }

                        if (!item.TOTAL_AMOUNT) {
                            growlService.growl("Please enter Total Amount for s.no " + (index + 1), "inverse");
                            keepGoing = true;
                            return false;
                        }

                        if (item.TOTAL_AMOUNT && item.TOTAL_AMOUNT <= 0) {
                            growlService.growl("Please enter Total Amount for s.no " + (index + 1), "inverse");
                            keepGoing = true;
                            return false;
                        }

                        if (item.IS_NEW_PACKAGE) {
                            if (item.MULTIPLE_ATTACHMENTS == '[]') {
                                growlService.growl("Please attach the attachments for s.no " + (index + 1), "inverse");
                                keepGoing = true;
                                return false;
                            }
                        }
                    }
                });

                if (keepGoing) {
                    return false;
                }

                let params =
                {
                    "poAmendItemsDetails": $scope.AMEND_ARR,
                    "poAmendmentID": $scope.PO_AMENDMENT_ID,
                    sessionId: $scope.sessionId
                };


                params.poAmendItemsDetails.forEach(function (poAmendItem, poAmendIndex) {
                    let ts = userService.toUTCTicks(poAmendItem.DELIVERY_DATE);
                    let m = moment(ts);
                    let DELIVERY_DATE = new Date(m);
                    var milliseconds = parseInt(DELIVERY_DATE.getTime() / 1000.0);
                    let finalDate = "/Date(" + milliseconds + "000+0530)/";
                    poAmendItem.DELIVERY_DATE = finalDate;
                });
                
                PRMPOService.AmendPODetails(params)
                    .then(function (response) {
                        //$state.go('.', { poID: $scope.poNumber, amendmentId: response.objectID }, { notify: false });
                        $state.go("poAmendment", { "poID": $scope.poNumber, "PO_AMENDMENT_ID": response.objectID });
                        $scope.PO_AMENDMENT_ID = response.objectID;
                        swal("Success!", "Successfully Submitted For Approval.", "success");
                        $scope.getPODetails();
                        //$scope.getItemWorkflow($scope.PO_AMENDMENT_ID, $scope.WorkflowModule);
                    });

            };

            $scope.AddPackage = function () {
                let newPackageObj =
                {
                    "ROW_ID": 0,
                    "PO_SCH_ROW_ID": 0,
                    "PO_NUMBER": $scope.poDetails[0].PO_NUMBER,
                    "PROJECT_ID": $scope.poDetails[0].PROJECT_ID,
                    "SUB_PACKAGE_ID": 0,
                    "UNIT_PRICE": 0,
                    "TAX": 0,
                    "DESCRIPTION": '',
                    "DELIVERY_DATE": $scope.poDetails[0].DELIVERY_DATE,
                    "ORDER_QTY": 1,
                    "VENDOR_ID": $scope.poDetails[0].VENDOR_ID,
                    "VENDOR_CODE": $scope.poDetails[0].VENDOR_CODE,
                    "UOM": $scope.poDetails[0].UNIT,
                    "PO_STATUS": $scope.poDetails[0].PO_STATUS,
                    "PO_TYPE": $scope.poDetails[0].PO_TYPE,
                    "PAYMENT_TERMS": $scope.poDetails[0].PAYMENT_TERMS,
                    "IS_PO_AMENDMENT_DONE": false,
                    "MULTIPLE_ATTACHMENTS": [],
                    "IS_NEW_PACKAGE": true,
                    "COST_CENTER": $scope.poDetails[0].COST_CENTER
                };
                $scope.poDetails.push(newPackageObj);
            };


            $scope.showSubPackages = function (poObj) {
                if (poObj) {
                    poObj.SUB_PACKAGE_ARR = [];
                    let subPackages = _.find($scope.companyCatalog, { catId: poObj.PACKAGE_ID }).nodes;
                    subPackages.forEach(function (catItem, catIndex) {
                        let catObj =
                        {
                            "catId": catItem.catId,
                            "catCode": catItem.catCode
                        };
                        poObj.SUB_PACKAGE_ARR.push(catObj);
                    });
                }
            };

            $scope.calculateAmounts = function (poItem) {
                //poItem.OVERALL_AMOUNT = (+poItem.TOTAL_AMOUNT + +poItem.TAX);
                //poItem.TOTAL_AMOUNT = (+poItem.TOTAL_AMOUNT + +poItem.TAX);
                poItem.TOTAL_VALUE = (+poItem.TOTAL_AMOUNT + +poItem.TAX);
            };
            $scope.isFormdisabled = false;

            $scope.getWorkflows = function () {
                workflowService.getWorkflowList()
                    .then(function (response) {
                        $scope.workflowList = [];
                        $scope.workflowListDeptWise = [];
                        $scope.workflowListSubPackage = [];
                        $scope.workflowListTemp = response;

                        $scope.workflowListTemp.forEach(function (item, index) {
                            if (item.WorkflowModule === $scope.WorkflowModule) {
                                $scope.workflowList.push(item);
                                $scope.workflowListDeptWise.push(item);
                            }
                        });

                        if ($scope.isSup) {
                            $scope.workflowList = $scope.workflowList;
                        }
                        else {
                            $scope.workflowList = [];
                            $scope.workflowListDeptWise.forEach(function (wf, idx) {
                                $scope.deptIDs.forEach(function (dep) {
                                    if (dep == wf.deptID) {
                                        $scope.workflowList.push(wf);
                                    }
                                });
                            });

                        }
                    });
            };

            $scope.getWorkflows();


            $scope.getItemWorkflow = function (poID, workflowModule, isExpanded) {
                workflowService.getItemWorkflow(0, poID, workflowModule)
                    .then(function (response) {
                        $scope.itemWorkflow = response;
                        $scope.checkIsFormDisable();
                        if ($scope.itemWorkflow && $scope.itemWorkflow.length > 0 && $scope.itemWorkflow[0].WorkflowTracks.length > 0) {
                            $scope.currentStep = 0;
                            var count = 0;
                            $scope.itemWorkflow[0].WorkflowTracks.forEach(function (track) {
                                if (!track.multipleAttachments) {
                                    track.multipleAttachments = [];
                                }

                                if (track.status === 'APPROVED') { track.statusNew = 'Approved'; }
                                if (track.status === 'HOLD') { track.statusNew = 'Hold'; }
                                if (track.status === 'PENDING') { track.statusNew = 'Pending'; }
                                if (track.status === 'REJECTED') { track.statusNew = 'Rejected'; }

                                if (track.status === 'APPROVED' || track.status === 'HOLD') {
                                    $scope.isFormdisabled = true;
                                }

                                if (track.status === 'APPROVED') {
                                    $scope.isWorkflowCompleted = true;
                                    $scope.orderInfo = track.order;
                                    $scope.assignToShow = track.status;

                                }
                                else {
                                    $scope.isWorkflowCompleted = false;
                                }

                                if (track.status === 'REJECTED' && count == 0) {
                                    count = count + 1;
                                }

                                if ((track.status === 'PENDING' || track.status === 'HOLD') && count == 0) {
                                    count = count + 1;
                                    //$scope.IsUserApproverForStage(track.approverID);
                                    $scope.currentAccess = track.order;
                                }

                                if ((track.status === 'PENDING' || track.status === 'HOLD' || track.status === 'REJECTED') && $scope.currentStep === 0) {
                                    $scope.currentStep = track.order;
                                    return false;
                                }
                            });
                        }

                        //$scope.getcategories();
                    });
            };


            $scope.isApproverDisable = function (index) {

                var disable = true;

                var previousStep = {};

                $scope.itemWorkflow[0].WorkflowTracks.forEach(function (step, stepIndex) {

                    if (index == stepIndex) {
                        if (stepIndex == 0) {
                            if ($scope.isUserBelongsToDeptandDesig(step.department.deptID, step.approver.desigID) &&
                                (step.status === 'PENDING' || step.status === 'HOLD')) {
                                disable = false;
                            }
                            else {
                                disable = true;
                            }
                        }
                        else if (stepIndex > 0) {
                            if (previousStep.status === 'PENDING' || previousStep.status === 'HOLD' || previousStep.status === 'REJECTED') {
                                disable = true;
                            }
                            else if ($scope.isUserBelongsToDeptandDesig(step.department.deptID, step.approver.desigID) &&
                                (step.status === 'PENDING' || step.status === 'HOLD')) {
                                disable = false;
                            }
                            else {
                                disable = true;
                            }
                        }
                    }
                    previousStep = step;
                });

                return disable;
            };

            $scope.isUserBelongsToDeptandDesig = function (deptID, desigID) {
                var isEligible = true;

                if ($scope.deptIDs.indexOf(deptID) != -1 && $scope.desigIDs.indexOf(desigID) != -1) {
                    isEligible = true;
                } else {
                    isEligible = false;
                }

                return isEligible;
            };


            $scope.updateTrack = function (step, status, type, isExpanded) {
                $scope.UPDATE_PO = false;
                $scope.commentsError = '';
                if (step.comments != null || step.comments != "" || step.comments != undefined) {
                    step.comments = validateStringWithoutSpecialCharacters(step.comments);
                }
                var tempArray = $scope.itemWorkflow[0].WorkflowTracks[$scope.itemWorkflow[0].WorkflowTracks.length - 1];
                if (step.order == tempArray.order && status === 'APPROVED') {
                    step.isLastApprover = true;
                } else {
                    step.isLastApprover = false;
                }

                if ($scope.isReject) {
                    $scope.commentsError = 'Please Save Rejected Items/Qty';
                    return false;
                }

                if (status === 'REJECTED' && (step.comments == null || step.comments == "")) {
                    $scope.commentsError = 'Please enter comments';
                    return false;
                }

                step.status = status;
                step.sessionID = $scope.sessionID;
                step.modifiedBy = userService.getUserId();

                step.moduleName = "PO_AMENDMENT";

                step.subModuleName = '';
                step.subModuleID = 0;

                step.revisionPODetails = [];
                if (step.isLastApprover)
                {
                    /** PO Details to be sent in workflow tracks **/
                    var newPODetails = [];
                    $scope.poDetailsTemp.forEach(function (subPackageItemTemp, subPackageItemTempIndex) {
                        //subPackageItemTemp.ROW_ID = subPackageItemTemp.PO_SCH_ROW_ID;
                        subPackageItemTemp.IS_FROM_PO_AMENDMENT = true;
                        subPackageItemTemp.CUSTOMER_USER_ID = +$scope.userId;
                        subPackageItemTemp.CUSTOMER_COMP_ID = +$scope.compId;
                        var ts = moment(subPackageItemTemp.DELIVERY_DATE, "DD-MM-YYYY HH:mm").valueOf();
                        let m = moment(ts);
                        let DELIVERY_DATE = new Date(m);
                        var milliseconds = parseInt(DELIVERY_DATE.getTime() / 1000.0);
                        let finalDate = "/Date(" + milliseconds + "000+0530)/";
                        subPackageItemTemp.DELIVERY_DATE = finalDate;

                        var newObj =
                        {
                            "PROJECT_ID": subPackageItemTemp.PROJECT_ID,
                            "PACKAGE_ID": subPackageItemTemp.PACKAGE_ID,
                            "SUB_PACKAGE_ID": subPackageItemTemp.SUB_PACKAGE_ID,
                            "DESCRIPTION": subPackageItemTemp.DESCRIPTION,
                            "VENDOR_ID": subPackageItemTemp.VENDOR_ID,
                            "PO_NUMBER": subPackageItemTemp.PO_NUMBER,
                            "SESSION_ID": userService.getUserToken()
                            //,"PO_AMEND_TAX_AMOUNT": subPackageItemTemp.TAX,
                            //"PO_AMEND_TOTAL_AMOUNT": subPackageItemTemp.TOTAL_AMOUNT,
                            //"PO_AMEND_OVERALL_AMOUNT": subPackageItemTemp.TOTAL_VALUE

                        };
                        newPODetails.push(newObj);
                    });
                    step.revisionPODetails = newPODetails;

                    /** PO Details to be sent in workflow tracks **/
                }

                workflowService.SaveWorkflowTrack(step)
                    .then(function (response) {
                        if (response.errorMessage) {
                            growlService.growl(response.errorMessage, "inverse");
                        }
                        else {
                            $state.go('list-po-new');
                            //if ($scope.UPDATE_PO) {
                            //    $scope.updatePORevisionNumber();
                            //} else {
                            //    $state.go('list-po-new');
                            //}
                            //$scope.GetBudgetDetailsbyProject(true);
                        }
                    });
            };

            $scope.updatePORevisionNumber = function ()
            {
                var newPODetails = [];
                $scope.poDetailsTemp.forEach(function (subPackageItemTemp, subPackageItemTempIndex) {
                    //subPackageItemTemp.ROW_ID = subPackageItemTemp.PO_SCH_ROW_ID;
                    subPackageItemTemp.IS_FROM_PO_AMENDMENT = true;
                    subPackageItemTemp.CUSTOMER_USER_ID = +$scope.userId;
                    subPackageItemTemp.CUSTOMER_COMP_ID = +$scope.compId;
                    var ts = moment(subPackageItemTemp.DELIVERY_DATE, "DD-MM-YYYY HH:mm").valueOf();
                    let m = moment(ts);
                    let DELIVERY_DATE = new Date(m);
                    var milliseconds = parseInt(DELIVERY_DATE.getTime() / 1000.0);
                    let finalDate = "/Date(" + milliseconds + "000+0530)/";
                    subPackageItemTemp.DELIVERY_DATE = finalDate;
                    
                    var newObj =
                    {
                        "PROJECT_ID": subPackageItemTemp.PROJECT_ID,
                        "PACKAGE_ID": subPackageItemTemp.PACKAGE_ID,
                        "SUB_PACKAGE_ID": subPackageItemTemp.SUB_PACKAGE_ID,
                        "DESCRIPTION": subPackageItemTemp.DESCRIPTION,
                        "VENDOR_ID": subPackageItemTemp.VENDOR_ID,
                        "PO_NUMBER": subPackageItemTemp.PO_NUMBER
                        //,"PO_AMEND_TAX_AMOUNT": subPackageItemTemp.TAX,
                        //"PO_AMEND_TOTAL_AMOUNT": subPackageItemTemp.TOTAL_AMOUNT,
                        //"PO_AMEND_OVERALL_AMOUNT": subPackageItemTemp.TOTAL_VALUE

                    };
                    newPODetails.push(newObj);
                });

                var params = {
                    "poNumber": $scope.poNumber,
                    "poDetails": newPODetails,
                    "sessionId": userService.getUserToken()
                };

                PRMPOService.UpdatePORevisionNumber(params)
                    .then(function (response) {
                        if (response.errorMessage)
                        {
                            $state.go("poAmendment", { "poID": response.errorMessage, "PO_AMENDMENT_ID": $scope.PO_AMENDMENT_ID });
                            //$state.go('.', { poID: response.errorMessage, amendmentId: $scope.PO_AMENDMENT_ID }, { notify: false });
                            $scope.poNumber = response.errorMessage;
                        }
                        //location.reload();
                        //$state.go('list-po-new');
                    });
            };


            $scope.totalAttachmentMaxSize = 6291456;
            $scope.totalRequirementSize = 0;
            $scope.totalRequirementItemSize = 0;

            $scope.multipleAttachments = [];
            $scope.getFile1 = function (id, itemid, ext) {
                $scope.file = $("#" + id)[0].files[0];
                $scope.totalRequirementItemSize = 0;
                if (id != "itemsAttachment" && $scope.file) {
                    $scope.poDetails[+id].MULTIPLE_ATTACHMENTS = [];
                    var listItem = $scope.poDetails[+id];
                    listItem.fileSize = $scope.file.size;

                    $scope.poDetails.forEach(function (item, index) {
                        if (item.fileSize) {
                            $scope.totalRequirementItemSize = $scope.totalRequirementItemSize + item.fileSize;
                        }
                    });
                }

                if (($scope.totalRequirementSize + $scope.totalRequirementItemSize) > $scope.totalAttachmentMaxSize) {
                    swal({
                        title: "Attachment size!",
                        text: "Total Attachments size cannot exceed 6MB",
                        type: "warning",
                        showCancelButton: false,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Ok",
                        closeOnConfirm: true
                    },
                        function () {
                            return;
                        });
                    return;
                }

                fileReader.readAsDataUrl($scope.file, $scope)
                    .then(function (result) {
                        if (id != "itemsAttachment") {
                            var fileUpload = {
                                fileStream: [],
                                fileName: '',
                                fileID: 0
                            };
                            var bytearray = new Uint8Array(result);
                            fileUpload.fileStream = $.makeArray(bytearray);
                            fileUpload.fileName = $scope.file.name;
                            $scope.poDetails[+id].MULTIPLE_ATTACHMENTS.push(fileUpload);
                        }
                    });
            }

            $scope.removeAttach = function (index) {
                $scope.poDetails[+index].MULTIPLE_ATTACHMENTS = [];
            };

            function validateStringWithoutSpecialCharacters(string) {
                if (string) {
                    string = string.replace(/\'/gi, "");
                    string = string.replace(/\"/gi, "");
                    string = string.replace(/[`^_|\?;:'",<>\{\}\[\]\\\/]/gi, "");
                    string = string.replace(/(\r\n|\n|\r)/gm, "");
                    string = string.replace(/\t/g, '');
                    return string;
                }
            };

            $scope.showApprovedDate = function (date) {
                return userService.toLocalDate(date);
            };

            $scope.isFormdisabled = false;

            $scope.checkIsFormDisable = function () {
                $scope.isFormdisabled = true;
                if ($scope.itemWorkflow.length == 0) {
                    $scope.isFormdisabled = true;
                } else {
                    if ($scope.poDetails && $scope.poDetails.length > 0) {
                        if (($scope.poDetails[0].CREATED_BY == +userService.getUserId() || $scope.poDetails[0].MODIFIED_BY == +userService.getUserId()) &&
                            $scope.itemWorkflow[0].WorkflowTracks.length > 0 &&
                            $scope.itemWorkflow[0].WorkflowTracks[0].status == "REJECTED" &&
                            $scope.itemWorkflow[0].WorkflowTracks[0].order == 1 &&
                            $scope.itemWorkflow[0].workflowID > 0)
                        {
                            $scope.isFormdisabled = false;
                        }
                    }
                }

            };

        }]);