﻿prmApp
    .config(["$stateProvider", "$urlRouterProvider", "$httpProvider", "$provide", "domain", "version",
        function ($stateProvider, $urlRouterProvider, $httpProvider, $provide, domain, version) {
            $stateProvider
                .state('vendorRegistration1', {
                    url: '/vendorRegistration1/:vendorId',
                    templateUrl: 'Registration/views/vendorRegistration1.html',
                    params: {
                        detailsObj: null
                    }
                });
        }]);