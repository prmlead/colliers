
prmApp.controller('vendorRegistration1Ctrl', ["$scope", "$stateParams", "$log", "$state", "$window", "userService", "auctionsService", "growlService", "PRMRegistrationService", "$filter", "fileReader",
    "$rootScope", "$sce","workflowService",
    function ($scope, $stateParams, $log, $state, $window, userService, auctionsService, storeService, PRMRegistrationService, $filter, fileReader, $rootScope, $sce, workflowService) {

        $scope.userId = userService.getUserId();
        $scope.sessionId = userService.getUserToken();
        $scope.compId = userService.getUserCompanyId();
        $scope.isCustomer = userService.getUserType() === "CUSTOMER" ? true : false;
        $scope.vendorId = $stateParams.vendorId;
        $scope.isSuperUser = userService.getUserObj().isSuperUser;

        $scope.formDefinition = [];
        $scope.formDefinitionGroups = [];
        $scope.formDefinitionObj = {};

        /*region start WORKFLOW*/
        $scope.workflowList = [];
        $scope.itemWorkflow = [];
        $scope.workflowObj = {};
        $scope.workflowObj.workflowID = 0;
        $scope.currentStep = 0;
        $scope.orderInfo = 0;
        $scope.assignToShow = '';
        $scope.isWorkflowCompleted = false;
        $scope.disableWFSelection = false;
        $scope.WorkflowModule = 'VENDOR_FORM_WORKFLOW';
        $scope.deptIDs = [];
        $scope.desigIDs = [];
        $scope.ListUserDepartmentDesignations = userService.getListUserDepartmentDesignations();
        if ($scope.ListUserDepartmentDesignations && $scope.ListUserDepartmentDesignations.length > 0) {
            $scope.ListUserDepartmentDesignations.forEach(function (item, index) {
                $scope.deptIDs.push(item.deptID);
                item.listDesignation.forEach(function (item1, index1) {
                    if (item1.isAssignedToUser && item1.isValid) {
                        $scope.desigIDs.push(item1.desigID);
                    }
                });
            });
        }
        /*region end WORKFLOW*/
        
        if (!$scope.isCustomer && +$scope.userId !== +$scope.vendorId) {
            $state.go('pages.profile.profile-about');
        }

        $scope.getFormControls = function () {
            var params =
            {
                "formCode": 'VENDOR_REGISTRATION'
            };

            PRMRegistrationService.getFormControls(params)
                .then(function (response) {
                    $scope.formDefinition = response;
                    if ($scope.formDefinition && $scope.formDefinition.length > 0) {
                        $scope.formDefinitionGroups = $scope.formDefinition.filter(function (control) {
                            return control.CONTROL_TYPE === 'GROUP';
                        });

                        $scope.formDefinitionGroups.forEach(function (group, index) {
                            let subGroups = $scope.formDefinition.filter(function (control) {
                                return control.CONTROL_TYPE === 'SUB-GROUP' && control.PARENT_CONTROL_ID === group.CONTROL_ID;
                            });

                            group.SUB_GROUPS = [];
                            group.SUB_GROUPS = subGroups;

                            group.SUB_GROUPS.forEach(function (subGroup, index) {
                                let controls = $scope.formDefinition.filter(function (control) {
                                    return control.CONTROL_TYPE === 'CONTROL' && control.PARENT_CONTROL_ID === subGroup.CONTROL_ID;
                                });

                                subGroup.CONTROLS = [];
                                subGroup.CONTROLS = controls;

                                if (subGroup.CONTROLS && subGroup.CONTROLS.length > 0) {
                                    subGroup.CONTROLS.forEach(function (control, index) {
                                        if (control.CONTROL_DEFAULT_VALUE) {
                                            control.CONTROL_VALUE = control.CONTROL_DEFAULT_VALUE;
                                        }
                                        if (control.CONTROL_OPTIONS) {
                                            let options = control.CONTROL_OPTIONS.split(';');
                                            control.CONTROL_OPTIONS_ARR = [];
                                            control.CONTROL_VALUE_TEMP = [];
                                            options.forEach(function (op, index) {
                                                let temp = { key: op.replace(/[^a-zA-Z]/g, "").toUpperCase(), value: op, checked: false };
                                                control.CONTROL_OPTIONS_ARR.push(temp);
                                            });
                                        }
                                    });

                                    subGroup.CONTROLMatrix = listToMatrix(subGroup.CONTROLS, 2);
                                }
                            });
                        });

                        if ($scope.vendorId && +$scope.vendorId > 0) {
                            $scope.getVendorFormDetails();
                        }
                    }
                });
        };

        $scope.getFormControls();

        $scope.getVendorFormDetails = function () {
            $scope.isVendorApproved = false;
            $scope.hasWorkflowAssigned = false;
            var params = {
                "vendorId": $scope.vendorId,
                "sessionId": $scope.sessionId
            };

            PRMRegistrationService.getVendorFormDetails(params)
                .then(function (response) {
                    if (response && response.message) {
                        if (response.objectID === 1)
                        {
                            $scope.isVendorApproved = true;
                        }
                        if (response.entityID === 1) {
                            $scope.hasWorkflowAssigned = true;
                        }

                        $scope.formDefinitionGroups = JSON.parse(response.message);
                        $scope.formDefinitionGroups.forEach(function (group, index) {
                            if (group.SUB_GROUPS && group.SUB_GROUPS.length > 0) {
                                group.SUB_GROUPS.forEach(function (subGroup, index) {
                                    if (subGroup.CONTROLS && subGroup.CONTROLS.length > 0) {
                                        subGroup.CONTROLS.forEach(function (control, index) {
                                            if (control.CONTROL_OPTIONS && control.CONTROL_SUB_TYPE === 'CHECKBOX' && control.CONTROL_VALUE) {
                                                let selectedOptions = control.CONTROL_VALUE.split(';');
                                                selectedOptions.forEach(function (selectedOp, index) {
                                                    let temp1 = control.CONTROL_OPTIONS_ARR.filter(function (op) {
                                                        return op.key === selectedOp;
                                                    });

                                                    if (temp1 && temp1.length > 0) {
                                                        temp1[0].checked = true;
                                                    }
                                                });
                                            }
                                        });

                                        subGroup.CONTROLMatrix = listToMatrix(subGroup.CONTROLS, 2);
                                    }
                                });
                            }
                        });

                        $scope.getItemWorkflow($scope.vendorId, $scope.WorkflowModule);
                    }
                });
        };

        $scope.saveVendorFormDetails = function (type) {
            let isValidForm = true;
            if (type) {
                $scope.formDefinitionGroups.forEach(function (group, index) {
                    if (group.SUB_GROUPS && group.SUB_GROUPS.length > 0) {
                        group.SUB_GROUPS.forEach(function (subGroup, index) {
                            if (subGroup.CONTROLS && subGroup.CONTROLS.length > 0) {
                                subGroup.CONTROLS.forEach(function (control, index) {
                                    control.SHOW_ERROR = false;
                                    if (control.IS_REQUIRED && control.IS_REQUIRED === 1) {
                                        if (!control.CONTROL_VALUE) {
                                            control.SHOW_ERROR = true;
                                            isValidForm = false;
                                        }
                                    }

                                    if (control.CONTROL_ID === 233) {
                                        console.log(control.CONTROL_ID);
                                        console.log(control.CONTROL_VALIDATION);
                                        console.log(control.CONTROL_VALIDATION_EXP);
                                    }
                                    
                                    if (control.CONTROL_VALUE && control.CONTROL_VALIDATION && control.CONTROL_VALIDATION_EXP) {
                                        let regExStr = new RegExp(control.CONTROL_VALIDATION_EXP.substring(1, control.CONTROL_VALIDATION_EXP.length - 1));
                                        if (!regExStr.test(control.CONTROL_VALUE)) {
                                            control.SHOW_ERROR = true;
                                            isValidForm = false;
                                        }
                                    }
                                });
                            }
                        });
                    }
                });
            }

            if (isValidForm) {
                let params = {
                    'vendorId': $scope.vendorId,
                    'json': JSON.stringify($scope.formDefinitionGroups),
                    'user': $scope.userId,
                    'sessionId': $scope.sessionId,
                    'sendForWorkflow': type ? true : false,
                    'vendorGSTNumber': getVendorGSTNumber($scope.formDefinitionGroups)
                };

                PRMRegistrationService.saveVendorFormDetails(params)
                    .then(function (response) {
                        if (response.errorMessage) {
                            swal("Error!", response.errorMessage, "error");
                        } else {
                            swal("Success!", "Successfully saved.", "success");
                            location.reload();
                        };
                    });
            } else {
                swal("Warning!", "Please fill all the required fields.", "warning");
            }
        };

        $scope.setFilters = function (selectedControl) {
            if (!_.isEmpty(selectedControl.CONTROL_VALUE_TEMP)) {
                var temp = _(selectedControl.CONTROL_VALUE_TEMP)
                    .filter(item => item.key)
                    .map('key')
                    .value();
                selectedControl.CONTROL_VALUE = temp.join(';');
            } else {
                selectedControl.CONTROL_VALUE = '';
            }
        };

        $scope.calculateVisibility = function (control) {
            let visibility = true;
            if (control.CONTROL_VISIBILITY) {
                console.log(control.CONTROL_VISIBILITY);
                let condition = JSON.parse(control.CONTROL_VISIBILITY);
                if (condition) {
                    let controlId = condition.id;
                    let controlValue = condition.value;

                    if (controlId && controlValue) {
                        $scope.formDefinitionGroups.forEach(function (group, index) {
                            if (group.SUB_GROUPS && group.SUB_GROUPS.length > 0) {
                                group.SUB_GROUPS.forEach(function (subGroup, index) {
                                    if (subGroup.CONTROLS && subGroup.CONTROLS.length > 0) {
                                        subGroup.CONTROLS.forEach(function (control, index) {
                                            if (control.CONTROL_ID === controlId) {
                                                if (!(control.CONTROL_VALUE && control.CONTROL_VALUE === controlValue)) {
                                                    visibility = false;
                                                }
                                            }
                                        });
                                    }
                                });
                            }
                        });
                    }
                }
            }

            return visibility;
            
        };

        $scope.getFile1 = function (id, itemid, ext) {
            let file = $("#" + id)[0].files[0];
            file.CONTROL_HTML_ID = id;
          
            fileReader.readAsDataUrl(file, $scope)
                .then(function (result) {
                    let fileUpload = {
                        fileStream: [],
                        fileName: '',
                        fileType: 'qualification',
                        fileID: 0
                    };

                    var bytearray = new Uint8Array(result);
                    fileUpload.fileStream = $.makeArray(bytearray);
                    fileUpload.fileName = file.name;
                    if (fileUpload && fileUpload.fileStream && fileUpload.fileStream.length > 0 && !fileUpload.fileID) {
                        let param = {
                            file: fileUpload,
                            user: $scope.userId,
                            sessionid: $scope.sessionId
                        };

                        auctionsService.saveAttachment(param).then(function (response) {
                            if (response && response.fileID) {
                                $scope.formDefinitionGroups.forEach(function (group, index) {
                                    if (group.SUB_GROUPS && group.SUB_GROUPS.length > 0) {
                                        group.SUB_GROUPS.forEach(function (subGroup, index) {
                                            if (subGroup.CONTROLS && subGroup.CONTROLS.length > 0) {
                                                let currentFileControl = subGroup.CONTROLS.filter(function (control) {
                                                    return control.CONTROL_HTML_ID === file.CONTROL_HTML_ID;
                                                });

                                                if (currentFileControl && currentFileControl.length > 0) {
                                                    currentFileControl[0].CONTROL_VALUE = response.fileID;
                                                    currentFileControl[0].CONTROL_OPTIONS = file.name;
                                                }
                                            }
                                        });
                                    }
                                });
                            }
                        });
                    }

                });
        }


        /*region start WORKFLOW*/

        $scope.getWorkflows = function () {
            workflowService.getWorkflowList()
                .then(function (response) {
                    $scope.workflowList = [];
                    $scope.workflowListDeptWise = [];
                    $scope.workflowListSubPackage = [];
                    $scope.workflowListTemp = response;

                    $scope.workflowListTemp.forEach(function (item, index) {
                        if (item.WorkflowModule === $scope.WorkflowModule) {
                            $scope.workflowList.push(item);
                            $scope.workflowListDeptWise.push(item);
                        }
                    });

                    if ($scope.isSuperUser) {
                        $scope.workflowList = $scope.workflowList;
                    }
                    else {
                        $scope.workflowList = [];
                        $scope.workflowListDeptWise.forEach(function (wf, idx) {
                            $scope.deptIDs.forEach(function (dep) {
                                if (dep == wf.deptID) {
                                    $scope.workflowList.push(wf);
                                }
                            });
                        });

                    }
                });
        };

        $scope.getWorkflows();

        $scope.getItemWorkflow = function (budgetID, workflowModule) {
            workflowService.getItemWorkflow(0, budgetID, workflowModule)
                .then(function (response) {
                    $scope.itemWorkflow = response;
                    $scope.checkIsFormDisable();
                    if ($scope.itemWorkflow && $scope.itemWorkflow.length > 0 && $scope.itemWorkflow[0].WorkflowTracks.length > 0) {
                        $scope.currentStep = 0;
                        var count = 0;
                        $scope.itemWorkflow[0].WorkflowTracks.forEach(function (track) {
                            if (!track.multipleAttachments) {
                                track.multipleAttachments = [];
                            }

                            if (track.status === 'APPROVED') { track.statusNew = 'Approved'; }
                            if (track.status === 'HOLD') { track.statusNew = 'Hold'; }
                            if (track.status === 'PENDING') { track.statusNew = 'Pending'; }
                            if (track.status === 'REJECTED') { track.statusNew = 'Rejected'; }

                            if (track.status === 'APPROVED' || track.status === 'HOLD') {
                                $scope.isFormdisabled = true;
                            }

                            if (track.status === 'APPROVED') {
                                $scope.isWorkflowCompleted = true;
                                $scope.orderInfo = track.order;
                                $scope.assignToShow = track.status;

                            }
                            else {
                                $scope.isWorkflowCompleted = false;
                            }

                            if (track.status === 'REJECTED' && count == 0) {
                                count = count + 1;
                            }

                            if ((track.status === 'PENDING' || track.status === 'HOLD') && count == 0) {
                                count = count + 1;
                                //$scope.IsUserApproverForStage(track.approverID);
                                $scope.currentAccess = track.order;
                            }

                            if ((track.status === 'PENDING' || track.status === 'HOLD' || track.status === 'REJECTED') && $scope.currentStep === 0) {
                                $scope.currentStep = track.order;
                                return false;
                            }
                        });
                    }
                });
        };
        $scope.isFormdisabled = false;
        $scope.checkIsFormDisable = function () {
            if ($scope.isCustomer) {
                $scope.isFormdisabled = true;
            } else {
                $scope.isFormdisabled = $scope.isVendorApproved || $scope.hasWorkflowAssigned;
            }
        };

        $scope.isApproverDisable = function (index) {

            var disable = true;

            var previousStep = {};

            $scope.itemWorkflow[0].WorkflowTracks.forEach(function (step, stepIndex) {

                if (index == stepIndex) {
                    if (stepIndex == 0) {
                        if ($scope.isUserBelongsToDeptandDesig(step.department.deptID, step.approver.desigID) &&
                            (step.status === 'PENDING' || step.status === 'HOLD')) {
                            disable = false;
                        }
                        else {
                            disable = true;
                        }
                    }
                    else if (stepIndex > 0) {
                        if (previousStep.status === 'PENDING' || previousStep.status === 'HOLD' || previousStep.status === 'REJECTED') {
                            disable = true;
                        }
                        else if ($scope.isUserBelongsToDeptandDesig(step.department.deptID, step.approver.desigID) &&
                            (step.status === 'PENDING' || step.status === 'HOLD')) {
                            disable = false;
                        }
                        else {
                            disable = true;
                        }
                    }
                }
                previousStep = step;
            });

            return disable;
        };

        $scope.isUserBelongsToDeptandDesig = function (deptID, desigID) {
            var isEligible = true;

            if ($scope.isSuperUser) {
                isEligible = true;
            } else {
                if ($scope.deptIDs.indexOf(deptID) != -1 && $scope.desigIDs.indexOf(desigID) != -1) {
                    isEligible = true;
                } else {
                    isEligible = false;
                }
            }


            return isEligible;
        };

        $scope.updateTrack = function (step, status, type, isExpanded) {
            $scope.APPROVE_VENDOR = false;
            $scope.commentsError = '';
            step.isLastApprover = false;

            if (step.comments != null || step.comments != "" || step.comments != undefined) {
                step.comments = validateStringWithoutSpecialCharacters(step.comments);
            }
            var tempArray = $scope.itemWorkflow[0].WorkflowTracks[$scope.itemWorkflow[0].WorkflowTracks.length - 1];
            if (step.order == tempArray.order && status === 'APPROVED') {
                $scope.APPROVE_VENDOR = true;
                step.isLastApprover = true;

            } else {
                $scope.APPROVE_VENDOR = false;
            }

            if ($scope.isReject) {
                $scope.commentsError = 'Please Save Rejected Items/Qty';
                return false;
            }

            if (status === 'REJECTED' && (step.comments == null || step.comments == "")) {
                $scope.commentsError = 'Please enter comments';
                return false;
            }

            step.status = status;
            step.sessionID = $scope.sessionId;
            step.modifiedBy = userService.getUserId();

            step.moduleName = $scope.WorkflowModule;

            step.subModuleName = '';
            step.subModuleID = 0;

            workflowService.SaveWorkflowTrack(step)
                .then(function (response) {
                    $scope.getFormControls();

                    if ($scope.APPROVE_VENDOR) {
                        $scope.ApproveVendor($scope.vendorId);
                    }

                });
        };

        $scope.ApproveVendor = function (vendorID) {

            userService.approveVendorThroughWorkflow({ "customerID": userService.getUserId(), "vendorID": vendorID, "isValid": 1 })
                .then(function (response) {
                    if (response.errorMessage != "") {
                        growlService.growl(response.errorMessage, "inverse");
                    }
                    else if (response.errorMessage == "") {
                        growlService.growl("Vendor Activated Successfully", "success");
                    }
                });

        };

        function validateStringWithoutSpecialCharacters(string) {
            if (string) {
                string = string.replace(/\'/gi, "");
                string = string.replace(/\"/gi, "");
                string = string.replace(/[`^_|\?;:'",<>\{\}\[\]\\\/]/gi, "");
                string = string.replace(/(\r\n|\n|\r)/gm, "");
                string = string.replace(/\t/g, '');
                return string;
            }
        }

        $scope.showApprovedDate = function (date) {
            return userService.toLocalDate(date);
        };

        $scope.validateControl = function (control) {
            control.CONTROL_VALIDATION_ERROR_MSG = '';
            if (control.CONTROL_VALUE && control.CONTROL_ID && control.CONTROL_VALIDATION && control.CONTROL_VALIDATION_EXP) {
                control.SHOW_ERROR = true;
                let regExStr = new RegExp(control.CONTROL_VALIDATION_EXP.substring(1, control.CONTROL_VALIDATION_EXP.length - 1));
                if (!regExStr.test(control.CONTROL_VALUE)) {
                    control.SHOW_ERROR = true;
                    control.CONTROL_VALIDATION_ERROR_MSG = 'Please enter valid value, which is ' + control.CONTROL_VALIDATION;
                } else {
                    control.SHOW_ERROR = false;
                }
            }
        };

        function listToMatrix(list, n) {
            var grid = [], i = 0, x = list.length, col, row = -1;
            for (var i = 0; i < x; i++) {
                col = i % n;
                if (col === 0) {
                    grid[++row] = [];
                }
                grid[row][col] = list[i];
            }
            return grid;
        }

        function getVendorGSTNumber()
        {
            var V_GST_NUMBER = '';

            var gstArray = [];

            if ($scope.formDefinitionGroups && $scope.formDefinitionGroups.length > 0)
            {
                gstArray = $scope.formDefinitionGroups.filter(function (item) {
                    return item.CONTROL_ID === 179 && item.FORM_CODE === 'VENDOR_REGISTRATION';
                });

                if (gstArray[0].SUB_GROUPS && gstArray[0].SUB_GROUPS.length > 0) {
                    if (gstArray[0].SUB_GROUPS[0].CONTROLMatrix && gstArray[0].SUB_GROUPS[0].CONTROLMatrix.length > 0) {
                        gstArray[0].SUB_GROUPS[0].CONTROLMatrix.forEach(function (controlElement, controlElementIndex) {
                            if (controlElement && controlElement.length > 0) {
                                controlElement.forEach(function (innerElement, innerElementIndex) {
                                    if (innerElement.CONTROL_ID === 181) {
                                        V_GST_NUMBER = innerElement.CONTROL_VALUE;
                                    }
                                });
                            }
                        });
                    }
                }
            }

            return V_GST_NUMBER;
        }

    }]);