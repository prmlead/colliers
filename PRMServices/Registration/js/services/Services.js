prmApp.constant('PRMRegistrationServiceDomain', 'Registration/svc/PRMRegistrationService.svc/REST/');
prmApp.service('PRMRegistrationService', ["PRMRegistrationServiceDomain", "userService", "httpServices",
    function (PRMRegistrationServiceDomain, userService, httpServices) {

        var PRMRegistrationService = this;

        PRMRegistrationService.getFormControls = function (params) {
            let url = PRMRegistrationServiceDomain + 'getFormControls?formCode=' + params.formCode;
            return httpServices.get(url, params);
        };

        PRMRegistrationService.getVendorFormDetails = function (params) {
            let url = PRMRegistrationServiceDomain + 'getVendorFormDetails?vendorId=' + params.vendorId + '&sessionId=' + params.sessionId;
            return httpServices.get(url, params);
        };

        PRMRegistrationService.saveVendorFormDetails = function (params) {
            let url = PRMRegistrationServiceDomain + 'saveVendorFormDetails';
            return httpServices.post(url, params);
        };

        return PRMRegistrationService;

}]);