﻿using System;
using System.IO;
using System.Data;
using System.Linq;
using System.Web;
using System.Configuration;
using System.Collections.Generic;
using System.ServiceModel.Activation;

using OfficeOpenXml;
using PRMServices.Common;
using PRMServices.Models;
using MySql.Data.MySqlClient;
using PRMServices;
using PdfSharp.Pdf;
using TheArtOfDev.HtmlRenderer.PdfSharp;
using PRMServices.SQLHelper;
//using PRM.Core.Models.Reports;
using CORE = PRM.Core.Common;
using Newtonsoft.Json;
using System.Dynamic;

namespace PRMServices
{
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class PRMRegistrationService : IPRMRegistrationService
    {
        PRMServices prm = new PRMServices();
        PRMWFService prmWF = new PRMWFService();

        private static NLog.Logger logger = NLog.LogManager.GetCurrentClassLogger();
        private IDatabaseHelper sqlHelper = DatabaseProvider.GetDatabaseProvider();
        public List<ControlDefinition> GetFormControls(string formCode)
        {
            List<ControlDefinition> details = new List<ControlDefinition>();
            try
            {
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                sd.Add("P_FORM_CODE", "VENDOR_REGISTRATION");
                DataSet ds = sqlHelper.SelectList("cp_FormDefinition", sd);
                CORE.DataNamesMapper<ControlDefinition> mapper = new CORE.DataNamesMapper<ControlDefinition>();
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    details = mapper.Map(ds.Tables[0]).ToList();                    
                }

                if (ds != null && ds.Tables.Count > 1 && ds.Tables[1].Rows.Count > 0)
                {
                    details.AddRange(mapper.Map(ds.Tables[1]).ToList());
                }

                if (ds != null && ds.Tables.Count > 2 && ds.Tables[2].Rows.Count > 0)
                {
                    details.AddRange(mapper.Map(ds.Tables[2]).ToList());
                }

                foreach (var detail in details)
                {
                    if (string.IsNullOrWhiteSpace(detail.CONTROL_UI_SIZE))
                    {
                        detail.CONTROL_UI_SIZE = "6";
                    }

                    if (string.IsNullOrWhiteSpace(detail.CONTROL_HTML_ID))
                    {
                        detail.CONTROL_HTML_ID = "CONTROLID" + detail.CONTROL_ID.ToString();
                    }

                    if (string.IsNullOrWhiteSpace(detail.CONTROL_HTML_NAME))
                    {
                        detail.CONTROL_HTML_NAME = "CONTROLNAME" + detail.CONTROL_ID.ToString();
                    }

                    //if (detail.CONTROL_TYPE.Equals("CONTROL"))
                    //{

                    //}
                }
            }
            catch (Exception ex)
            {
                ControlDefinition detail = new ControlDefinition();
                detail.ErrorMessage = ex.Message;
                details.Add(detail);
            }
            finally
            {
            }

            return details;
        }



        public Response GetVendorFormDetails(int vendorId, string sessionId)
        {
            Response details = new Response();
            try
            {
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                sd.Add("P_VENDOR_ID", vendorId);
                DataSet ds1 = sqlHelper.SelectList("cp_GetVendorFormDetails", sd);

                if (ds1 != null && ds1.Tables.Count > 0 && ds1.Tables[0].Rows.Count > 0)
                {
                    var row = ds1.Tables[0].Rows[0];
                    details.Message = row["FORM_JSON"] != DBNull.Value ? Convert.ToString(row["FORM_JSON"]) : string.Empty;
                    details.ObjectID = row["REG_STATUS"] != DBNull.Value ? Convert.ToInt32(row["REG_STATUS"]) : 0;
                    details.EntityID = row["HAS_WORKFLOW"] != DBNull.Value ? Convert.ToInt32(row["HAS_WORKFLOW"]) : 0;
                }
            }
            catch (Exception ex)
            {
                details.ErrorMessage = ex.Message;
            }
            finally
            {
            }

            return details;
        }

        public Response SaveVendorFormDetails(int vendorId, string json, int user, string sessionId, bool sendForWorkflow, string vendorGSTNumber)
        {
            Response response = new Response();
            List<EmailTemplate> emailTemplates = new List<EmailTemplate>();

            try
            {
                int wfID = 0;
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                sd.Add("P_VENDOR_ID", vendorId);
                sd.Add("P_FORM_JSON", json);
                sd.Add("P_USER", user);
                sd.Add("P_VENDOR_GST_NUMBER", vendorGSTNumber);
                DataSet dataSet = sqlHelper.SelectList("cp_SaveVendorFormDetails", sd);

                if (sendForWorkflow) 
                {
                    sd = new SortedDictionary<object, object>() { };
                    sd.Add("P_U_ID", ConfigurationManager.AppSettings["COMPANY_ID"]);
                    sd.Add("P_VENDOR_ID", vendorId);
                    DataSet ds1 = sqlHelper.SelectList("cp_SaveVendorFormWorkflow", sd);
                    
                    if (ds1 != null && ds1.Tables.Count > 0 && ds1.Tables[0].Rows.Count > 0)
                    {
                        var row = ds1.Tables[0].Rows[0];
                        response.ErrorMessage = row["ERR_MESSAGE"] != DBNull.Value ? Convert.ToString(row["ERR_MESSAGE"]) : string.Empty;
                        response.ObjectID = row["TRACK_ID"] != DBNull.Value ? Convert.ToInt32(row["TRACK_ID"]) : 0;
                        wfID = row["WF_ID"] != DBNull.Value ? Convert.ToInt32(row["WF_ID"]) : 0;
                    }

                    if (response.ObjectID > 0 && wfID > 0)
                    {
                        string body = string.Empty;
                        string bodySMS = string.Empty;
                        System.Xml.XmlDocument doc = new System.Xml.XmlDocument();
                        doc.Load(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "bin") + "/EmailTemplates/EmailFormats.xml");
                        System.Xml.XmlNode footernode = doc.DocumentElement.SelectSingleNode("EmailFooter");

                        DataSet ds2 = prm.GetEmailTemplatesData(vendorId,"VENDOR_FORM_WORKFLOW", wfID, 1, "CREATED", 0);
                        string content = string.Empty;
                        string useremails = string.Empty;
                        string cc = string.Empty;

                        if (ds2 != null && ds2.Tables.Count > 0 && ds2.Tables[0].Rows.Count > 0)
                        {

                            foreach (DataRow row in ds2.Tables[0].Rows)
                            {
                                EmailTemplate email = new EmailTemplate();
                                email.Subject = row["SUBJECT"] != DBNull.Value ? Convert.ToString(row["SUBJECT"]) : string.Empty;
                                email.Content = row["CONTENTS"] != DBNull.Value ? Convert.ToString(row["CONTENTS"]) : string.Empty;
                                email.Type = row["TYPE"] != DBNull.Value ? Convert.ToString(row["TYPE"]) : string.Empty;
                                email.Action = row["ACTION"] != DBNull.Value ? Convert.ToString(row["ACTION"]) : string.Empty;
                                emailTemplates.Add(email);
                            }
                        }


                        if (ds2 != null && ds2.Tables.Count > 0 && ds2.Tables[1].Rows.Count > 0)
                        {


                            foreach (DataRow row in ds2.Tables[1].Rows)
                            {

                                var templateContent = emailTemplates.FirstOrDefault(v => v.Action == row["PARAM_7"].ToString() && v.Type == row["PARAM_8"].ToString());

                                DataRow dataRow = row;
                                string jsonContent = prmWF.DataRowToJson(dataRow);
                                var parameters = Newtonsoft.Json.JsonConvert.DeserializeObject<Dictionary<string, object>>(jsonContent);
                                string replacedsubject = prmWF.ReplacePlaceholders(templateContent.Subject, parameters);
                                string replacedTemplate = prmWF.ReplacePlaceholders(templateContent.Content, parameters);
                                string subject = string.Format(replacedsubject, "");

                                int Totalcount = 0;
                                if (parameters.TryGetValue("Param", out object paramValue))
                                {
                                    Totalcount = Convert.ToInt32(paramValue);
                                }
                                useremails = JsonConvert.DeserializeObject<Dictionary<string, string>>(jsonContent)?["Param9"];
                                cc = JsonConvert.DeserializeObject<Dictionary<string, string>>(jsonContent)?["Param10"];

                                if (ds2.Tables[1].Columns.Count == Totalcount)
                                {
                                    body = String.Format(replacedTemplate, "", "");
                                    body += footernode.InnerText;
                                    prm.SendEmail(useremails, subject, body, 0, 0, "CREATED", sessionId, null, null, null, 0, "CREATED_TRACK", cc).ConfigureAwait(false);
                                }
                                else
                                {
                                    logger.Info("parameters not matched with Total Columns>>" + parameters);

                                }
                            }
                        }

                    }

                }

            }
            catch (Exception ex)
            {
                response.ErrorMessage = ex.Message;
            }

            return response;
        }
    }
}