﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using System.Configuration;
using System.Data.SqlClient;
using PRMServices.Models;

namespace PRMServices
{
    [ServiceContract]
    public interface IPRMRegistrationService
    {
        [OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getFormControls?formCode={formCode}")]
        List<ControlDefinition> GetFormControls(string formCode);

        [OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getVendorFormDetails?vendorId={vendorId}&sessionId={sessionId}")]
        Response GetVendorFormDetails(int vendorId, string sessionId);

        [WebInvoke(Method = "POST",
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        ResponseFormat = WebMessageFormat.Json,
        RequestFormat = WebMessageFormat.Json,
        UriTemplate = "saveVendorFormDetails")]
        Response SaveVendorFormDetails(int vendorId, string json, int user, string sessionId, bool sendForWorkflow, string vendorGSTNumber);
    }
}
