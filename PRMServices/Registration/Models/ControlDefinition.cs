﻿using PRM.Core.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace PRMServices.Models
{
    [DataContract]
    public class ControlDefinition : ResponseAudit
    {
        [DataMember] [DataNames("CONTROL_ID")] public int CONTROL_ID { get; set; }
        [DataMember] [DataNames("CONTROL_HTML_ID")] public string CONTROL_HTML_ID { get; set; }
        [DataMember] [DataNames("CONTROL_HTML_NAME")] public string CONTROL_HTML_NAME { get; set; }
        [DataMember] [DataNames("FORM_CODE")] public string FORM_CODE { get; set; }
        [DataMember] [DataNames("PARENT_CONTROL_ID")] public int PARENT_CONTROL_ID { get; set; }
        [DataMember] [DataNames("CONTROL_TYPE")] public string CONTROL_TYPE { get; set; }
        [DataMember] [DataNames("CONTROL_SUB_TYPE")] public string CONTROL_SUB_TYPE { get; set; }
        [DataMember] [DataNames("CONTROL_LABEL")] public string CONTROL_LABEL { get; set; }
        [DataMember] [DataNames("CONTROL_DESCRIPTION")] public string CONTROL_DESCRIPTION { get; set; }
        [DataMember] [DataNames("CONTROL_PLACE_HOLDER")] public string CONTROL_PLACE_HOLDER { get; set; }
        [DataMember] [DataNames("CONTROL_INFO")] public string CONTROL_INFO { get; set; }
        [DataMember] [DataNames("CONTROL_DEFAULT_VALUE")] public string CONTROL_DEFAULT_VALUE { get; set; }
        [DataMember] [DataNames("CONTROL_VALUE")] public string CONTROL_VALUE { get; set; }
        [DataMember] [DataNames("IS_REQUIRED")] public int IS_REQUIRED { get; set; }
        [DataMember] [DataNames("ORDER")] public int ORDER { get; set; }
        [DataMember] [DataNames("CONTROL_OPTIONS")] public string CONTROL_OPTIONS { get; set; }
        [DataMember] [DataNames("CONTROL_MODEL")] public string CONTROL_MODEL { get; set; }

        [DataMember] [DataNames("CONTROL_UI_SIZE")] public string CONTROL_UI_SIZE { get; set; }
        [DataMember] [DataNames("CONTROL_VISIBILITY")] public string CONTROL_VISIBILITY { get; set; }
        [DataMember] [DataNames("CONTROL_VALIDATION")] public string CONTROL_VALIDATION { get; set; }
        [DataMember] [DataNames("CONTROL_VALIDATION_EXP")] public string CONTROL_VALIDATION_EXP { get; set; }
    }
}