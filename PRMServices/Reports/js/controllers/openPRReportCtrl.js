﻿prmApp

    .controller('openPRReportCtrl', ["$scope", "$http", "$state", "domain", "$filter", "$log", "$stateParams", "$timeout", "auctionsService",
        "userService", "SignalRFactory", "fileReader", "growlService", "PRMAnalysisServices","$uibModal",
        function ($scope, $http, $state, domain, $filter, $log, $stateParams, $timeout, auctionsService,
            userService, SignalRFactory, fileReader, growlService, PRMAnalysisServices, $uibModal) {

            $scope.sessionId = userService.getUserToken();
            $scope.companyId = userService.getUserCompanyId();
            /*PAGINATION CODE*/
            $scope.totalItems = 0;
            $scope.currentPage = 1;
            $scope.itemsPerPage = 5;
            $scope.maxSize = 5; //Number of pager buttons to show

            /*PAGINATION CODE*/

            $scope.openPRArr = [];
            $scope.openPRArrInitial = [];
            $scope.openPRArrExport = [];
            $scope.eportOpenPRData = false;
            $scope.openPRPivot = [];
            $scope.openPRPivotData = [];
            $scope.UIFilters = {
                filterModelPurchaseUser: 'ALL',
                filterModelCategory: 'ALL',
                yearmonth: ''
            };           

            $scope.getOpenPRPivot = function () {
                PRMAnalysisServices.GetOpenPRStatsbyMonth({ 'compid': $scope.companyId, 'sessionid': $scope.sessionId})
                    .then(function (response) {
                        $scope.openPRPivot = response;
                        if ($scope.openPRPivot && $scope.openPRPivot.length > 0) {
                            $scope.openPRPivot.forEach(function (obj, key) {
                                var temp = {
                                    "name": obj.key1,
                                    "y": obj.value1
                                };

                                $scope.openPRPivotData.push(temp);
                            });

                            loadHighCharts();
                        }
                    });
            };

            $scope.filterGraph = function () {
                $scope.getOpenPRPivot();
            };

            $scope.filterGraph();

            $scope.getOPenPRData = function (yearmonth, page, pagesize) {
                PRMAnalysisServices.GetOpenPRData({ 'compid': $scope.companyId, 'yearmonth': yearmonth, 'page': page, 'pagesize': pagesize,  'sessionid': $scope.sessionId })
                    .then(function (response) {
                        if (response && response.length > 0) {
                            if ($scope.eportOpenPRData) {
                                $scope.openPRArrExport = response;
                                $scope.openPRArrExport.forEach(function (obj, key) {
                                    obj.PR_CHANGE_DATE1 = userService.toLocalDate(obj.PR_CHANGE_DATE);
                                    obj.DELIVERY_DATE1 = userService.toLocalDate(obj.DELIVERY_DATE);
                                });
                                $scope.exportData();
                            } else {
                                $scope.openPRArrInitial = [];
                                $scope.openPRArrInitial = response;
                                $scope.openPRArrInitial.forEach(function (obj, key) {
                                    obj.PR_CHANGE_DATE1 = userService.toLocalDate(obj.PR_CHANGE_DATE);
                                    obj.DELIVERY_DATE1 = userService.toLocalDate(obj.DELIVERY_DATE);
                                });
                            }
                        }
                    });
            };

            $scope.filterTableData = function (yearmonth, page, pagesize) {
                $scope.getOPenPRData($scope.UIFilters.yearmonth, page, pagesize);
            };

            $scope.filterTableData('', 0, $scope.itemsPerPage);


            $scope.pageChanged = function (pageNo) {
                $scope.currentPage = pageNo;
                $scope.filterTableData($scope.UIFilters.yearmonth, +($scope.currentPage - 1) * $scope.itemsPerPage, $scope.itemsPerPage);
            };

            function loadHighCharts() {
                Highcharts.chart('container', {
                    chart: {
                        type: 'column'
                    },
                    title: {
                        text: 'Open PR'
                    },
                    subtitle: {
                        text: ''
                    },
                    xAxis: {
                        type: 'category'
                    },
                    yAxis: {
                        title: {
                            text: 'Total No.of PR\'s'
                        }

                    },
                    legend: {
                        enabled: false
                    },
                    plotOptions: {
                        series: {
                            borderWidth: 0,
                            dataLabels: {
                                enabled: true
                            },
                            point: {
                                events: {
                                    click: function () {
                                        $scope.UIFilters.yearmonth = this.name;
                                        $scope.filterTableData(this.name, 0, $scope.itemsPerPage);
                                    }
                                }
                            }
                        }
                    },

                    tooltip: {
                        headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
                        pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.0f}</b><br/>'
                    },

                    "series": [
                        {
                            "name": "PR COUNT",
                            "colorByPoint": true,
                            "data": $scope.openPRPivotData
                        }
                    ],
                    "drilldown": {

                    }
                });
            }

            $scope.exportItemsToExcel = function () {
                $scope.eportOpenPRData = true;
                $scope.filterTableData('', 0, 0);                
            };

            $scope.exportData = function () {
                $scope.eportOpenPRData = false;
                var mystyle = {
                    sheetid: 'OpenPR',
                    headers: true,
                    column: {
                        style: 'font-size:15px;background:#233646;color:#FFF;'
                    }
                };

                alasql.fn.decimalRound = function (x) { return Math.round(x); };
                alasql(' SELECT PLANT as [Plant Name], REQUISITIONER_NAME as [Requisitioner Name], PR_NUMBER as [PR Number], ' +
                    ' ITEM_NUM as [PR Item Number], PR_CHANGE_DATE1 as [PR Creat. Date], ' +
                    ' MATERIAL_DESCRIPTION as [Material Desc.], UOM as [UOM], decimalRound(REQUIRED_QUANTITY) as [PR Qty.], ' +
                    ' LEAD_TIME as [Lead Time], DELIVERY_DATE1 as [Delivery Date], ' +
                    ' CategoryCode as [Category] INTO XLSX(?, { ' +
                    ' headers: true, sheetid: "PRDetails", ' +
                    ' style: "font-size:15px;background:#233646;color:#FFF;"}) FROM ? ', ["OpenPR.xlsx", $scope.openPRArrExport]);
            };

        }]);