﻿prmApp

    .controller('averageProcureCtrl', ["$scope", "$rootScope", "$filter", "$stateParams", "$http", "domain", "fileReader", "$state",
        "$timeout", "auctionsService", "userService", "SignalRFactory", "growlService", "$log", "signalRHubName", "ngDialog",
        "reportingService", "$window", "priceCapServices", "PRMLotReqService", "PRMCustomFieldService", "workflowService",
        function ($scope, $rootScope, $filter, $stateParams, $http, domain, fileReader, $state, $timeout, auctionsService,
            userService, SignalRFactory, growlService, $log, signalRHubName, ngDialog, reportingService, $window, priceCapServices,
            PRMLotReqService, PRMCustomFieldService, workflowService) {

            $scope.averageContainer = function () {
                Highcharts.setOptions({
                    colors: ['#01BAF2', '#71BF45', '#FAA74B']
                });
                Highcharts.chart('container', {
                    credits: {
                        enabled: false
                    },
                    chart: {
                        plotBackgroundColor: null,
                        plotBorderWidth: null,
                        plotShadow: false,
                        type: 'pie'
                    },
                    title: {
                        text: 'On-Time Delivery,Commit Date,Supplier'
                    },
                    legend: {
                        enabled: false
                    },
                    tooltip: {
                        pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                    },
                    plotOptions: {
                        pie: {
                            allowPointSelect: true,
                            cursor: 'pointer',
                            dataLabels: {
                                enabled: true,
                                formatter: function () {
                                    return this.key + ': ' + this.y + '%';
                                }
                            },
                            showInLegend: true
                        }
                    },
                    series: [{
                        name: 'Percentage',
                        colorByPoint: true,
                        innerSize: '70%',
                        data: [{
                            name: 'On Time',
                            // color: '#01BAF2',
                            y: 14,
                        }, {
                            name: 'Early',
                            // color: '#71BF45',
                            y: 18
                        }, {
                            name: 'Late',
                            //color: '#FAA74B',
                            y: 68
                        }]
                    }]
                });

            }
            $scope.averageContainer();

        }
    ]);