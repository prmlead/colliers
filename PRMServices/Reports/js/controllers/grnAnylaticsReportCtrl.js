﻿prmApp
    // =========================================================================
    // Auction Tiles
    // =========================================================================
    .controller('grnAnylaticsReportCtrl', ["$timeout", "$state", "$scope", "$log", "growlService", "userService", "auctionsService", "$http", "$rootScope",
        "SignalRFactory", "signalRHubName", "logisticServices", "$filter", "store", "poService", "workflowService", "PRMAnalysisServices",
        function ($timeout, $state, $scope, $log, growlService, userService, auctionsService, $http, $rootScope, $filter, SignalRFactory, signalRHubName, logisticServices, store, poService,
            workflowService, PRMAnalysisServices) {

            $scope.dateobj = {
                anylaticsFromDate: moment().subtract(15, "days").format("YYYY-MM-DD"),
                anylaticsToDate: moment().format('YYYY-MM-DD')
            };

            $scope.currentUserCompId = userService.getUserCompanyId();
            $scope.currentUserId = +userService.getUserId();
            $scope.isCustomer = userService.getUserType();
            $scope.showL1Data = false;
            $scope.showL2Data = false;
            $scope.analyticsTotalData = {};
            $scope.analyticsDataInitial = [];
            $scope.analyticsDataLevel1 = [];
            $scope.pivotColumn = '';
            $scope.pivotColumnDisplayName = '';

            $scope.totalItems = 0;
            $scope.currentPage = 1;
            $scope.itemsPerPage = 10;
            $scope.maxSize = 5;

            $scope.setPage = function (pageNo) {
                $scope.currentPage = pageNo;
            };

            $scope.getAnalyticsTotalData = function () {
                var params = {
                    "fromDate": $scope.dateobj.anylaticsFromDate,
                    "toDate": $scope.dateobj.anylaticsToDate,
                    "compid": $scope.currentUserCompId
                };

                PRMAnalysisServices.GetPRMGRNAnalyticsTotals(params)
                    .then(function (response) {
                        $scope.analyticsTotalData = response;
                    });
            };

            $scope.getAnalyticsTotalData();


            $scope.getAnylaticsReportsData = function (datasettype) {
                var params = {
                    "fromDate": $scope.dateobj.anylaticsFromDate,
                    "toDate": $scope.dateobj.anylaticsToDate,
                    "compid": $scope.currentUserCompId,
                    'dataSetType': datasettype
                };

                PRMAnalysisServices.getAnylaticsReportsData(params)
                    .then(function (response) {
                        $scope.analyticsDataInitial = response;

                        if ($scope.analyticsDataInitial && $scope.analyticsDataInitial.length > 0) {
                            $scope.analyticsDataInitial.forEach(function (item, itemIndexs) {
                                item.GRN_DATE1 = userService.toLocalDate(item.GRN_DATE);
                            });

                            pivotGRNData(datasettype);
                        }                        
                    });
            };

            $scope.getAnylaticsReportsData('TOTAL_GRN');

            $scope.getAnylaticsReportsDataFilterByCard = function (datasettype) {
                if (!$scope.analyticsDataInitial || $scope.analyticsDataInitial.length <= 0) {
                    $scope.getAnylaticsReportsData(datasettype);
                } else {
                    pivotGRNData(datasettype);
                }
            };

            function pivotGRNData(datasettype) {
                $scope.pivotColumn = '';
                $scope.pivotColumnDisplayName = '';
                if (datasettype === 'TOTAL_GRN') {
                    $scope.showL1Data = true;
                    $scope.showL2Data = false;
                }
                if (datasettype === 'MATERIAL_GRN') {
                    $scope.pivotColumn = 'ProductCode';
                    $scope.pivotColumnDisplayName = 'Material Name';
                    $scope.showL1Data = false;
                    $scope.showL2Data = true;
                }
                if (datasettype === 'VENDOR_GRN') {
                    $scope.pivotColumn = 'SUPPLIER_NAME';
                    $scope.pivotColumnDisplayName = 'Vendor Company';
                    $scope.showL1Data = false;
                    $scope.showL2Data = true;
                }
                if (datasettype === 'PO_GRN') {
                    $scope.pivotColumn = 'PO_NUMBER';
                    $scope.pivotColumnDisplayName = 'PO Number';
                    $scope.showL1Data = false;
                    $scope.showL2Data = true;
                }
                if (datasettype === 'CATEGORY_GRN') {
                    $scope.pivotColumn = 'CategoryCode';
                    $scope.pivotColumnDisplayName = 'Material Group';
                    $scope.showL1Data = false;
                    $scope.showL2Data = true;
                }
                if (datasettype === 'COUNTRY_GRN') {
                    $scope.pivotColumn = 'VENDOR_COUNTRY';
                    $scope.pivotColumnDisplayName = 'Vendor Country';
                    $scope.showL1Data = false;
                    $scope.showL2Data = true;
                }
                if (datasettype === 'PAYMENT_TERM_GRN') {
                    $scope.pivotColumn = 'PAYMENT_TERM_CODE';
                    $scope.pivotColumnDisplayName = 'Payment Term Code';
                    $scope.showL1Data = false;
                    $scope.showL2Data = true;
                }

                if ($scope.showL2Data) {
                    $scope.analyticsDataLevel1 = [];
                    let uniqPivotValues = _.uniq(_.map($scope.analyticsDataInitial, $scope.pivotColumn));
                    if (uniqPivotValues && uniqPivotValues.length > 0) {
                        uniqPivotValues.forEach(function (item, itemIndexs) {
                            let filteredPivotDataList = $scope.analyticsDataInitial.filter(function (itemObj) {
                                return itemObj[$scope.pivotColumn] === item;
                            });

                            $scope.analyticsDataLevel1.push({ pivotValue: item, analyticsDataLevel2: filteredPivotDataList });
                        });
                    }
                } else {
                    $scope.analyticsDataLevel1 = $scope.analyticsDataInitial;
                    $scope.totalItems = $scope.analyticsDataInitial.length;
                }
                
            }

        }]);