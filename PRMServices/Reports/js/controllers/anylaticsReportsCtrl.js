﻿prmApp
    // =========================================================================
    // Auction Tiles
    // =========================================================================
    .controller('anylaticsReportsCtrl', ["$timeout", "$state", "$scope", "$log", "growlService", "userService", "auctionsService", "$http", "$rootScope",
        "SignalRFactory", "signalRHubName", "logisticServices", "$filter", "store", "poService", "workflowService", "PRMAnalysisServices",
        function ($timeout, $state, $scope, $log, growlService, userService, auctionsService, $http, $rootScope, $filter, SignalRFactory, signalRHubName, logisticServices, store, poService,
            workflowService, PRMAnalysisServices) {

            $scope.dateobj = {
                anylaticsFromDate: moment().subtract(15, "days").format("YYYY-MM-DD"),
                anylaticsToDate: moment().format('YYYY-MM-DD')
            };

            $scope.currentUserCompID = userService.getUserCompanyId();
            $scope.rfqexpanded = false;
            $scope.departmentWiseGraphShow = false;
            $scope.categoryWiseGraphShow = false;
            $scope.departmentWiseLevelGraphShow = false;
            $scope.categoryWiseLevelGraphShow = false;
            $scope.TitleText = '';
            $scope.deptTitleText = '';
            $scope.catTileText = '';
            $scope.UserSpendTitleText = '';
            $scope.ContributeSpendTitleText = '';
            $scope.UserSpentCatTitleText = '';
            $scope.DeptWiseCatTitleText = '';
            $scope.type = '';
            $scope.SelectedPlantCode = 0;
            $scope.currentUserId = +userService.getUserId();
            $scope.isCustomer = userService.getUserType();

            $scope.prmStatus = function (type, status) {
                return userService.NegotiationStatus(type, status);
            };

            /*pagination code*/

            $scope.totalVendors = 0;
            $scope.totalSubuser = 0;
            $scope.totalInactiveVendors = 0;
            $scope.totalLeads = 0;
            $scope.totalItems = 0;
            $scope.currentPage = 1;
            $scope.currentPage2 = 1;
            $scope.itemsPerPage = 10;
            $scope.itemsPerPage2 = 10;
            $scope.maxSize = 10;

            $scope.setPage = function (type, selectedIds, search, prnum, pageNo) {
                $scope.currentPage = pageNo;
                $scope.getAnylaticsReportsData(type, selectedIds, search, prnum, ($scope.currentPage - 1), 10);

            };

            $scope.searchKeyword = '';
            $scope.filtersList = {
                plantCodeList1: [],
                plantCodeListFilter: [],
                plantCodeError: ""
            };
            $scope.plantCodeListTemp = [];
            $scope.selectedCodesArray = [];
            $scope.downloadExcel = false;
            $scope.getAnalyticsTotalData = function () {
                var params = {
                    "fromDate": $scope.dateobj.anylaticsFromDate,
                    "toDate": $scope.dateobj.anylaticsToDate,
                    "compid": userService.getUserCompanyId()
                };

                PRMAnalysisServices.GetPRMAnalyticsTotals(params)
                    .then(function (response) {
                        $scope.analyticsTotalData = response;
                        $scope.selectedCodesArray = [];
                        //$scope.analyticsTotalData.PLANTS = [];
                        var allPlant = { PLANT_NAME: 'ALL', PLANT_CODE: 'ALL', isChecked: true };
                        $scope.analyticsTotalData.PLANTS.push(allPlant);
                        $scope.filtersList.plantCodeList1 = $scope.analyticsTotalData.PLANTS;
                        $scope.plantCodeListTemp = angular.copy($scope.analyticsTotalData.PLANTS);
                        $scope.filtersList.plantCodeListFilter = $scope.analyticsTotalData.PLANTS;
                        $scope.selectedCodesArray.push(allPlant);
                        $scope.departmentWiseLevelGraphShow = false;

                    });
            };
            $scope.getAnalyticsTotalData();
            $scope.qcsApprovalHistoryData = [];
            $scope.vendorResponseData = [];
            $scope.analyticsTotalReqData = [];
            $scope.analyticsTotalReqDataTemp = [];
            $scope.AllqcsData = [];
            $scope.AllqcsDataTemp = [];
            $scope.getAnylaticsReportsData = function (type, selectedIds, search, prnum, recordsFetchFrom) {
                if ($scope.downloadExcel) {
                    $scope.pageSize = 0;
                } else if (!$scope.downloadExcel) {
                    $scope.pageSize = 10;
                }

                if (type !== 'QCS_APPROVAL_HISTORY') {
                    $scope.qcsData = [];
                    $scope.qcsDataTemp = [];
                }

                $scope.type = type;
                if (type === 'totalReq') {
                    var params = {
                        "from": $scope.dateobj.anylaticsFromDate,
                        "to": $scope.dateobj.anylaticsToDate,
                        "userID": $scope.currentUserId,
                        "type": type,
                        "plantCode": 0,
                        "compID": $scope.currentUserCompID,
                        "reqIDs": '',
                        "qcsID": 0,
                        "search": search ? search : '',
                        "page": recordsFetchFrom * $scope.pageSize,
                        "pagesize": $scope.pageSize
                    };
                    $scope.pageSizeTemp = (params.page + 1);
                    PRMAnalysisServices.GetAnylaticsReports(params)
                        .then(function (response) {
                            if (response.reqTransactions && response.reqTransactions.length > 0) {
                                response.reqTransactions.forEach(function (item) {
                                    if (item.closed === "UNCONFIRMED" && (item.closed !== "NOTSTARTED" || item.closed !== "STARTED" || item.closed !== "Negotiation Ended")) {
                                        if (item.vendorsInvited === 0) {
                                            item.closed = "Saved As Draft";
                                        } else {
                                            if (item.vendorsInvited == item.vendorsParticipated) {
                                                item.closed = "Quotations Received";
                                            }
                                            else if (item.vendorsInvited != item.vendorsParticipated) {
                                                item.closed = "UNCONFIRMED";
                                            }
                                        }
                                    }
                                })
                                var DataToExcel = [];
                                if ($scope.downloadExcel) {
                                    DataToExcel = angular.copy(response);
                                    $scope.GetReport(type, DataToExcel.reqTransactions);
                                } else if (!$scope.downloadExcel) {
                                    $scope.analyticsTotalReqDataTemp = angular.copy(response);
                                    $scope.analyticsTotalReqData = response;
                                }
                                $scope.totalItems = response.reqTransactions.length > 0 ? response.reqTransactions[0].totalCount : 0;
                            }
                        });
                }
                else if (type === 'totalPlants') {

                    if ($scope.selectedCodesArray.length == 0) {
                        growlService.growl("Please Select Atleat One Plant", 'inverse');
                        return;
                    }
                    $scope.PlantCodesToGet = [];
                    $scope.selectedCodesArray.forEach(function (i) {
                        if (i.isChecked) {
                            $scope.PlantCodesToGet.push(i.PLANT_CODE);
                        }
                    });
                    $scope.SelectedPlantCode = $scope.PlantCodesToGet.join(',');

                    var params1 = {
                        "from": $scope.dateobj.anylaticsFromDate,
                        "to": $scope.dateobj.anylaticsToDate,
                        "userID": $scope.currentUserId,
                        "type": type,
                        "plantCode": $scope.SelectedPlantCode.length > 0 ? $scope.SelectedPlantCode : 0,
                        "compID": $scope.currentUserCompID,
                        "reqIDs": '',
                        "qcsID": 0,
                        "search": search ? search : '',
                        "page": recordsFetchFrom * $scope.pageSize,
                        "pagesize": $scope.pageSize
                    };
                    $scope.pageSizeTemp = (params1.page + 1);
                    PRMAnalysisServices.GetAnylaticsReports(params1)
                        .then(function (response) {
                            if (response.reqTransactions && response.reqTransactions.length > 0) {
                                response.reqTransactions.forEach(function (item) {
                                    if (item.closed == "UNCONFIRMED" && (item.closed !== "NOTSTARTED" || item.closed !== "STARTED" || item.closed !== "Negotiation Ended")) {
                                        if (item.vendorsInvited === 0) {
                                            item.closed = "Saved As Draft";
                                        } else {
                                            if (item.vendorsInvited == item.vendorsParticipated) {
                                                item.closed = "Quotations Received";
                                            }
                                            else if (item.vendorsInvited != item.vendorsParticipated) {
                                                item.closed = "UNCONFIRMED";
                                            }
                                        }
                                    }
                                })
                                var DataToExcel = [];
                                if ($scope.downloadExcel) {
                                    DataToExcel = angular.copy(response);
                                    $scope.GetReport(type, DataToExcel.reqTransactions);
                                } else if (!$scope.downloadExcel) {
                                    $scope.analyticsTotalPlantDataTemp = angular.copy(response);
                                    $scope.analyticsTotalPlantData = response;
                                }
                                $scope.totalItems = response.reqTransactions.length > 0 ? response.reqTransactions[0].totalCount : 0;
                            }
                        });
                } else if (type === 'PR_RFQ') {
                    var params2 = {
                        "from": $scope.dateobj.anylaticsFromDate,
                        "to": $scope.dateobj.anylaticsToDate,
                        "userID": $scope.currentUserId,
                        "type": type,
                        "plantCode": $scope.SelectedPlantCode > 0 ? $scope.SelectedPlantCode : 0,
                        "compID": $scope.currentUserCompID,
                        "reqIDs": '',
                        "qcsID": 0,
                        "search": search ? search : '',
                        "page": recordsFetchFrom * $scope.pageSize,
                        "pagesize": $scope.pageSize
                    };
                    $scope.pageSizeTemp = (params2.page + 1);
                    PRMAnalysisServices.GetAnylaticsReports(params2)
                        .then(function (response) {
                            $scope.analyticsPRRFQData = response;
                            if ($scope.analyticsPRRFQData.PRRFQData && $scope.analyticsPRRFQData.PRRFQData.length > 0) {
                                $scope.analyticsPRRFQData.PRRFQData.forEach(function (item) {
                                    item.RELEASE_DATE = new moment(item.RELEASE_DATE).format("DD-MM-YYYY HH:mm");
                                    item.CREATED_DATE = new moment(item.CREATED_DATE).format("DD-MM-YYYY HH:mm");
                                    item.MODIFIED_DATE = new moment(item.MODIFIED_DATE).format("DD-MM-YYYY HH:mm");
                                });
                                $scope.totalItems = $scope.analyticsPRRFQData.PRRFQData.length > 0 ? $scope.analyticsPRRFQData.PRRFQData[0].TotalCount : 0;
                            }
                        });
                } else if (type === 'RFQ') {
                    var params6 = {
                        "from": $scope.dateobj.anylaticsFromDate,
                        "to": $scope.dateobj.anylaticsToDate,
                        "userID": $scope.currentUserId,
                        "type": type,
                        "plantCode": $scope.SelectedPlantCode > 0 ? $scope.SelectedPlantCode : 0,
                        "compID": $scope.currentUserCompID,
                        "reqIDs": selectedIds,
                        "qcsID": 0,
                        "search": search ? search : '',
                        "page": 0,
                        "pagesize": 0
                    };
                    PRMAnalysisServices.GetAnylaticsReports(params6)
                        .then(function (response) {
                            // $scope.SubPRRFQData = response;
                            if ($scope.analyticsPRRFQData.PRRFQData && $scope.analyticsPRRFQData.PRRFQData.length > 0) {
                                $scope.analyticsPRRFQData.PRRFQData.forEach(function (item) {
                                    if (prnum == item.PR_NUMBER) {
                                        item.SubPRRFQData = [];
                                        item.SubPRRFQData = response;
                                    }
                                    if (item.SubPRRFQData) {
                                        item.SubPRRFQData.RFQDataWithPR.forEach(function (subItem) {
                                            subItem.startTime = userService.toLocalDate(subItem.startTime);
                                            subItem.endTime = userService.toLocalDate(subItem.endTime);
                                            if (subItem.status === "UNCONFIRMED" && (subItem.status !== "NOTSTARTED" || subItem.status !== "STARTED" || subItem.status !== "Negotiation Ended")) {
                                                if (subItem.noOfVendorsInvited === 0) {
                                                    subItem.status = "Saved As Draft";
                                                } else {
                                                    if (subItem.noOfVendorsInvited == subItem.noOfVendorsParticipated) {
                                                        subItem.status = "Quotations Received";
                                                    }
                                                    else if (subItem.noOfVendorsInvited != subItem.noOfVendorsParticipated) {
                                                        subItem.status = "UNCONFIRMED";
                                                    }
                                                }
                                            }
                                            if (subItem.startTime.includes('9999') || subItem.startTime.includes('10000')) {
                                                subItem.startTime = '--';
                                            }
                                            if (subItem.endTime.includes('9999') || subItem.endTime.includes('10000')) {
                                                subItem.endTime = '--';
                                            }
                                        });
                                    }

                                });
                            }
                        });
                } else if (type === 'QCS_CURRENT_APPROVERS') {
                    var params3 = {
                        "from": $scope.dateobj.anylaticsFromDate,
                        "to": $scope.dateobj.anylaticsToDate,
                        "userID": $scope.currentUserId,
                        "type": type,
                        "plantCode": 0,
                        "compID": $scope.currentUserCompID,
                        "reqIDs": '',
                        "qcsID": 0,
                        "search": search ? search : '',
                        "page": recordsFetchFrom * $scope.pageSize,
                        "pagesize": $scope.pageSize
                    };
                    $scope.pageSizeTemp = (params3.page + 1);
                    PRMAnalysisServices.GetAnylaticsReports(params3)
                        .then(function (response) {
                            var DataToExcel = [];
                            if ($scope.downloadExcel) {
                                DataToExcel = angular.copy(response);
                                $scope.GetReport(type, DataToExcel.QCS_CURRENT_APPROVERS);
                                $scope.qcsData = $scope.AllqcsData;
                                $scope.qcsDataTemp = $scope.AllqcsDataTemp;
                            } else if (!$scope.downloadExcel) {
                                $scope.qcsData = response.QCS_CURRENT_APPROVERS;
                                $scope.qcsDataTemp = angular.copy(response.QCS_CURRENT_APPROVERS);
                                $scope.AllqcsData = angular.copy(response.QCS_CURRENT_APPROVERS);
                                $scope.AllqcsDataTemp = angular.copy(response.QCS_CURRENT_APPROVERS);
                            }
                            $scope.totalItems = $scope.qcsData.length > 0 ? $scope.qcsData[0].TotalCount : 0;
                        });
                } else if (type === 'QCS_APPROVAL_HISTORY') {
                    var params4 = {
                        "from": $scope.dateobj.anylaticsFromDate,
                        "to": $scope.dateobj.anylaticsToDate,
                        "userID": $scope.currentUserId,
                        "type": type,
                        "plantCode": 0,
                        "compID": $scope.currentUserCompID,
                        "reqIDs": '',
                        "qcsID": selectedIds,
                        "search": search ? search : '',
                        "page": 0,
                        "pagesize": 0
                    };
                    PRMAnalysisServices.GetAnylaticsReports(params4)
                        .then(function (response) {
                            $scope.qcsApprovalHistoryData = response.QCS_APPROVAL_HISTORY;
                            $scope.totalItems = $scope.AllqcsData.length > 0 ? $scope.AllqcsData[0].TotalCount : 0;
                        });
                } else if (type === 'VENDOR_RESPONSE') {
                    var params5 = {
                        "from": $scope.dateobj.anylaticsFromDate,
                        "to": $scope.dateobj.anylaticsToDate,
                        "userID": 0,
                        "type": type,
                        "plantCode": 0,
                        "compID": $scope.currentUserCompID,
                        "reqIDs": '',
                        "qcsID": 0,
                        "search": search ? search : '',
                        "page": recordsFetchFrom * $scope.pageSize,
                        "pagesize": $scope.pageSize
                    };
                    $scope.pageSizeTemp = (params5.page + 1);
                    PRMAnalysisServices.GetAnylaticsReports(params5)
                        .then(function (response) {
                            $scope.vendorResponseData = response.VENDOR_RESPONSE_TIME;
                            if ($scope.vendorResponseData && $scope.vendorResponseData.length > 0) {
                                $scope.vendorResponseData.forEach(function (item) {
                                    item.RESPONSE_TIME_ALL = convertHMS(item.RESPONSE_TIME_ALL);
                                });
                                $scope.totalItems = $scope.vendorResponseData.length > 0 ? $scope.vendorResponseData[0].TotalCount : 0;
                            }
                        });
                } else if (type === 'PR_PO') {
                    var params7 = {
                        "from": $scope.dateobj.anylaticsFromDate,
                        "to": $scope.dateobj.anylaticsToDate,
                        "userID": 0,
                        "type": type,
                        "plantCode": 0,
                        "compID": $scope.currentUserCompID,
                        "reqIDs": '',
                        "qcsID": 0,
                        "search": search ? search : '',
                        "page": recordsFetchFrom * $scope.pageSize,
                        "pagesize": $scope.pageSize
                    };
                    $scope.pageSizeTemp = (params7.page + 1);
                    PRMAnalysisServices.GetAnylaticsReports(params7)
                        .then(function (response) {
                            $scope.PRTOPODATARR = response.PR_PO_DATA;
                            if ($scope.PRTOPODATARR && $scope.PRTOPODATARR.length > 0) {
                                $scope.PRTOPODATARR.forEach(function (item, index) {
                                    item.PR_RELEASE_DATE = item.PR_RELEASE_DATE ? moment(item.PR_RELEASE_DATE).format("DD-MM-YYYY") : '-';

                                    if (item.PR_PO_VENDOR_LIST && item.PR_PO_VENDOR_LIST.length > 0) {
                                        var value = 75;
                                        value = (value * item.PR_PO_VENDOR_LIST.length);
                                        item.style = "height: " + value + "px;max-height:" + value + "px;overflow-y: auto";
                                    }
                                });
                                $scope.totalItems = $scope.PRTOPODATARR.length > 0 ? $scope.PRTOPODATARR[0].TotalCount : 0;
                            }
                        });
                }
            };

            $scope.NotParticipatedList = [];
            $scope.getNotParticipatedList = function (vendorID) {
                var params = {
                    "vendorID": vendorID
                };
                PRMAnalysisServices.GetVendorNotParticipatedList(params)
                    .then(function (response) {
                        $scope.NotParticipatedList = response;
                    });
            };
            $scope.qcsDetailsRfq = [];
            $scope.getrfqQcsDetails = function (qcsDetails) {
                $scope.qcsDetailsRfq = qcsDetails;
            };


            function convertHMS(value) {
                const sec = parseInt(value, 10); // convert value to number if it's string
                let days = Math.floor(sec / (3600 * 24)); // get days
                let hours = Math.floor(sec / 3600); // get hours
                let minutes = Math.floor((sec - (hours * 3600)) / 60); // get minutes
                let seconds = sec - (hours * 3600) - (minutes * 60); //  get seconds
                // add 0 if value < 10; Example: 2 => 02
                if (days < 10) { days = "0" + days; }
                if (hours < 10) { hours = "0" + hours; }
                if (minutes < 10) { minutes = "0" + minutes; }
                if (seconds < 10) { seconds = "0" + seconds; }
                //return hours + ':' + minutes + ':' + seconds; // Return is HH : MM : SS
                return days + ':' + hours + ':' + minutes; // Return is DD : HH : MM 
            }

            $scope.getAnalyticsData = function (type, text, value) {
                $scope.type1 = type
                $scope.departmentWiseUserDataGraph.series = [];
                $scope.departmentWiseCategoryDataGraph.series = [];
                $scope.departmentWiseContributeGraph.series = [];
                $scope.departmentWiseUserDataGraph.xAxis.categories = [];
                $scope.departmentWiseCategorySpend = [];
                $scope.categoryWiseDepartmentSpend = [];
                if (type === 'totalSpendDept') {
                    $scope.analyticsTotalSpendData.DepartmentUserSpend.forEach(function (item) {
                        if (item.key1 === text) {
                            $scope.departmentWiseUserDataGraph.series.push({
                                'name': item.value,
                                'data': [item.decimalVal]
                            });
                            $scope.departmentWiseUserDataGraph.xAxis.categories.push(item.value);
                        }
                    });

                    $scope.departmentWiseCategorySpend = $scope.analyticsTotalSpendData.DepartmentCategorySpend.filter(function (item) {
                        return item.key1 === text;
                    });

                    $scope.departmentWiseContributeGraph.series.push({
                        'name': 'Total',
                        'y': $scope.analyticsTotalData.PARAM_TOTAL_SPEND
                    }, {
                        'name': text,
                        'y': value
                    });

                    $scope.TitleText = 'Department Wise Total Spend';
                    $scope.UserSpendTitleText = 'Department Wise User Spend';
                    $scope.DeptWiseCatTitleText = 'Department Wise Category Total Spend';
                    $scope.ContributeSpendTitleText = 'Department Wise Contribute Spend';
                    $scope.UserSpentCatTitleText = 'User Wise Category in department Spend';

                } else if (type === 'totalSpendCat') {
                    $scope.analyticsTotalSpendData.CategoryUserSpend.forEach(function (item) {
                        if (item.key1 === text) {
                            $scope.departmentWiseUserDataGraph.series.push({
                                'name': item.value,
                                'data': [item.decimalVal]
                            });
                            $scope.departmentWiseUserDataGraph.xAxis.categories.push(item.value);
                        }

                    })

                    $scope.departmentWiseCategorySpend = $scope.analyticsTotalSpendData.DepartmentCategorySpend.filter(function (item) {
                        return item.value === text;
                    });

                    $scope.departmentWiseContributeGraph.series.push({
                        'name': 'Total',
                        'y': $scope.analyticsTotalData.PARAM_TOTAL_SPEND
                    }, {
                        'name': text,
                        'y': value
                    });

                    $scope.TitleText = 'Category Wise Total Spend';
                    $scope.UserSpendTitleText = 'Category Wise User Spend';
                    $scope.DeptWiseCatTitleText = 'Category Wise Department Total Spend'
                    $scope.ContributeSpendTitleText = 'Category Wise Contribute Spend';
                    $scope.UserSpentCatTitleText = 'User Wise Department in Category Spend';


                } else if (type === 'totalSavingsDept') {
                    $scope.analyticsTotalSavingsData.DepartmentUserSavings.forEach(function (item) {
                        if (item.key1 === text) {
                            $scope.departmentWiseUserDataGraph.series.push({
                                'name': item.value,
                                'data': [item.decimalVal]
                            });
                            $scope.departmentWiseUserDataGraph.xAxis.categories.push(item.value);
                        }
                    });

                    $scope.departmentWiseCategorySpend = $scope.analyticsTotalSavingsData.DepartmentCategorySavings.filter(function (item) {
                        return item.key1 === text;
                    });

                    $scope.departmentWiseContributeGraph.series.push({
                        'name': 'Total',
                        'y': $scope.analyticsTotalData.PARAM_TOTAL_SAVINGS
                    }, {
                        'name': text,
                        'y': value
                    });

                    $scope.TitleText = 'Department Wise Total Savings';
                    $scope.UserSpendTitleText = 'Department Wise User Savings';
                    $scope.DeptWiseCatTitleText = 'Department Wise category Total Savings';
                    $scope.ContributeSpendTitleText = 'Department Wise Contribute Savings';
                    $scope.UserSpentCatTitleText = 'User Wise Category in department Savings';
                } else if (type === 'totalSavingsCat') {
                    $scope.analyticsTotalSavingsData.CategoryUserSavings.forEach(function (item) {
                        if (item.key1 === text) {
                            $scope.departmentWiseUserDataGraph.series.push({
                                'name': item.value,
                                'data': [item.decimalVal]
                            });
                            $scope.departmentWiseUserDataGraph.xAxis.categories.push(item.value);
                        }

                    });

                    $scope.departmentWiseCategorySpend = $scope.analyticsTotalSavingsData.DepartmentCategorySavings.filter(function (item) {
                        return item.value === text;
                    });


                    $scope.departmentWiseContributeGraph.series.push({
                        'name': 'Total',
                        'y': $scope.analyticsTotalData.PARAM_TOTAL_SAVINGS
                    }, {
                        'name': text,
                        'y': value
                    });
                    $scope.TitleText = 'Category Wise Total Savings';
                    $scope.UserSpendTitleText = 'Category Wise User Savings';
                    $scope.DeptWiseCatTitleText = 'Category Wise Department Total Savings'
                    $scope.ContributeSpendTitleText = 'Category Wise Contribute Savings';
                    $scope.UserSpentCatTitleText = 'User Wise Department in Category Savings';


                } else if (type === 'totalPlantsDept') {
                    $scope.analyticsTotalPlantsData.DepartmentUserPlants.forEach(function (item) {
                        if (item.key1 === text) {
                            $scope.departmentWiseUserDataGraph.series.push({
                                'name': item.value,
                                'data': [item.value1]
                            });
                            $scope.departmentWiseUserDataGraph.xAxis.categories.push(item.value);
                        }

                    });
                    $scope.departmentWiseCategorySpend = $scope.analyticsTotalPlantsData.DepartmentCategoryPlants.filter(function (item) {
                        return item.key1 === text;
                    });

                    $scope.departmentWiseContributeGraph.series.push({
                        'name': 'Total',
                        'y': $scope.analyticsTotalData.PARAM_TOTAL_PLANTS
                    }, {
                        'name': text,
                        'y': value
                    });
                    $scope.TitleText = 'Plant-Department Wise';
                    $scope.UserSpendTitleText = 'Plant-Department Wise User';
                    $scope.DeptWiseCatTitleText = 'Department Wise Category Total Plants';
                    $scope.ContributeSpendTitleText = 'Plant-Department Wise Contribute';
                    $scope.UserSpentCatTitleText = 'Plant-User Wise Category in department';

                } else {
                    $scope.analyticsTotalPlantsData.CategoryUserPlants.forEach(function (item) {
                        if (item.key1 === text) {
                            $scope.departmentWiseUserDataGraph.series.push({
                                'name': item.value,
                                'data': [item.value1]
                            });
                            $scope.departmentWiseUserDataGraph.xAxis.categories.push(item.value);
                        }
                    });

                    $scope.departmentWiseCategorySpend = $scope.analyticsTotalPlantsData.DepartmentCategoryPlants.filter(function (item) {
                        return item.value === text;
                    });

                    $scope.departmentWiseContributeGraph.series.push({
                        'name': 'Total',
                        'y': $scope.analyticsTotalData.PARAM_TOTAL_PLANTS
                    }, {
                        'name': text,
                        'y': value
                    });

                    $scope.TitleText = 'Plant-Category Wise ';
                    $scope.UserSpendTitleText = 'Plant-Category Wise User';
                    $scope.DeptWiseCatTitleText = 'Category Wise Department Total Plants'
                    $scope.ContributeSpendTitleText = 'Plant-Category Wise Contribute';
                    $scope.UserSpentCatTitleText = 'Plant-User Wise Department in Category';

                }

                $('#upateChartButton2').click();

                $scope.departmentWiseLevelGraphShow = true;
                $scope.categoryWiseLevelGraphShow = false;


            };

            $scope.searchTable = function (str, type, prnum, recordsFetchFrom) {
                str = str ? str.toLowerCase() : '';
                $scope.getAnylaticsReportsData(type, '', str, prnum, recordsFetchFrom);
            };

            $scope.departmentWiseGraph = {
                credits: {
                    enabled: false
                },
                chart: {
                    plotBackgroundColor: null,
                    plotBorderWidth: null,
                    plotShadow: false,
                    type: 'pie',
                    events: {
                        load: function () {

                            var chart = this,
                                series = []
                            $('#upateChartButton').click(function () {
                                series = $scope.departmentWiseGraph.series
                                chart.update({
                                    series: [{ data: series }],
                                    title: {
                                        text: $scope.deptTitleText
                                    }
                                }, true, true);


                            });


                        }
                    }
                },
                title: {
                    text: 'Department Wise'
                },
                accessibility: {
                    point: {
                        valueSuffix: '%'
                    }
                },
                plotOptions: {
                    pie: {
                        allowPointSelect: true,
                        cursor: 'pointer'
                    },
                    series: {
                        cursor: 'pointer',
                        point: {
                            events: {
                                click: function () {
                                    //$scope.departmentWiseData(this.y);
                                    $scope.getAnalyticsData($scope.type + 'Dept', this.name, this.y)
                                    //$scope.departmentWiseLevelGraphShow = true;
                                }
                            }
                        }
                    }
                },

                series: []
            };

            $scope.categoryWiseGraph = {
                credits: {
                    enabled: false
                },
                chart: {
                    plotBackgroundColor: null,
                    plotBorderWidth: null,
                    plotShadow: false,
                    type: 'pie',
                    events: {
                        load: function () {

                            var chart = this,
                                series = [];

                            $('#upateChartButton').click(function () {
                                series = $scope.categoryWiseGraph.series
                                chart.update({
                                    series: [{ data: series }],
                                    title: {
                                        text: $scope.catTileText
                                    }
                                }, true, true);


                            });

                        }
                    }
                },
                title: {
                    text: 'Category Wise'
                },
                accessibility: {
                    point: {
                        valueSuffix: '%'
                    }
                },
                plotOptions: {
                    pie: {
                        allowPointSelect: true,
                        cursor: 'pointer'
                    },
                    series: {
                        cursor: 'pointer'
                    }
                },
                series: []
            };

            $scope.departmentWiseUserDataGraph = {
                chart: {
                    type: 'column',
                    width: 450,

                    height: 400,
                    events: {
                        load: function () {

                            var chart = this,
                                series = [];
                            $('#upateChartButton2').click(function () {
                                series = $scope.departmentWiseUserDataGraph.series
                                categories = $scope.departmentWiseUserDataGraph.xAxis.categories
                                chart.update({
                                    series: series,
                                    xAxis: { categories: categories },
                                    title: {
                                        text: $scope.UserSpendTitleText
                                    }
                                }, true, true);
                            });
                        }
                    }

                },
                title: {
                    text: $scope.UserSpendTitleText
                },

                xAxis: {
                    categories: []
                },
                yAxis: {
                    title: {
                        text: 'Users'
                    }

                },
                plotOptions: {
                    bar: {
                        dataLabels: {
                            enabled: true
                        }
                    }
                },
                credits: {
                    enabled: false
                },
                series: []
            };

            $scope.departmentWiseCategoryDataGraph = {
                credits: {
                    enabled: false
                },
                chart: {
                    plotBackgroundColor: null,
                    plotBorderWidth: null,
                    plotShadow: false,
                    type: 'pie',
                    width: 1200,
                    height: 400,
                    events: {
                        load: function () {
                            var chart = this,
                                series = [];
                            $('#upateChartButton2').click(function () {
                                series = $scope.departmentWiseCategoryDataGraph.series

                                chart.update({
                                    series: [{ data: series }],

                                    title: {
                                        text: $scope.DeptWiseCatTitleText
                                    }
                                }, true, true);
                            });
                        }
                    }

                },
                title: {
                    text: $scope.DeptWiseCatTitleText
                },
                accessibility: {
                    point: {
                        valueSuffix: '%'
                    }
                },
                plotOptions: {
                    pie: {
                        allowPointSelect: true,
                        cursor: 'pointer'
                    },
                    series: {
                        cursor: 'pointer'
                    }
                },
                series: []
            };

            $scope.departmentWiseContributeGraph = {
                credits: {
                    enabled: false
                },
                chart: {
                    plotBackgroundColor: null,
                    plotBorderWidth: null,
                    plotShadow: false,
                    type: 'pie',
                    width: 450,

                    height: 400,
                    events: {
                        load: function () {


                            var chart = this,
                                series = [];
                            $('#upateChartButton2').click(function () {
                                series = $scope.departmentWiseContributeGraph.series

                                chart.update({
                                    series: [{ data: series }],

                                    title: {
                                        text: $scope.ContributeSpendTitleText
                                    }
                                }, true, true);
                            });
                        }
                    }

                },
                title: {
                    text: $scope.ContributeSpendTitleText
                },
                accessibility: {
                    point: {
                        valueSuffix: '%'
                    }
                },
                plotOptions: {
                    pie: {
                        allowPointSelect: true,
                        cursor: 'pointer',
                        dataLabels: {
                            enabled: false
                        },
                        showInLegend: true
                    }

                },
                series: []
            };

            $scope.categoryWiseUserGraph = {
                chart: {
                    type: 'bar',
                    events: {
                        load: function () {

                            var chart = this,
                                series = [];
                            $('#upateChartButton').click(function () {
                                chart.update({
                                    title: {
                                        text: $scope.UserSpentCatTitleText
                                    }

                                }, true, true);
                            });
                        }
                    }

                },
                title: {
                    text: $scope.UserSpentCatTitleText
                },

                xAxis: {
                    categories: ['user1', 'user2', 'user3', 'user4', 'user5'],
                    title: {
                        text: null
                    }
                },
                yAxis: {
                    min: 0,
                    title: {
                        align: 'high'
                    },
                    labels: {
                        overflow: 'justify'
                    }
                },
                tooltip: {
                    valueSuffix: ' millions'
                },
                plotOptions: {
                    bar: {
                        dataLabels: {
                            enabled: true
                        }
                    },
                    series: {
                        stacking: 'normal'
                    }
                },
                legend: {
                    reversed: true
                },
                credits: {
                    enabled: false
                },
                series: [{
                    name: 'John',
                    data: [5, 3, 4, 7, 2]
                }, {
                    name: 'Jane',
                    data: [2, 2, 3, 2, 1]
                }, {
                    name: 'Joe',
                    data: [3, 4, 4, 2, 5]
                }]
            };

            $scope.changetype = function (val) {
                $scope.type = val;
                $scope.searchKeyword = '';
                $scope.searchKeyword1 = '';
                $scope.searchKeyword2 = '';

                $scope.totalItems = 0;
                $scope.currentPage = 0;
            };

            $scope.getDateConverted = function (date) {
                if (date) {
                    return userService.toLocalDate(date);
                }
            };

            $scope.displayNames = function (qcs) {
                $scope.rfqTitle = '';
                $scope.workflowTitle = '';
                $scope.REQ_NUMBER = '';

                $scope.rfqTitle = qcs.REQ_TITLE;
                $scope.workflowTitle = qcs.WF_TITLE;
                $scope.REQ_NUMBER = qcs.REQ_NUMBER;

            };

            $scope.calculateTime = function (date1, date2) {
                var string = '';
                let dateValue1 = moment(date1).add(330, 'minutes');
                let dateValue2 = moment(date2).add(330, 'minutes');
                let value = moment.duration(moment(dateValue1).diff(moment(dateValue2)));

                string += value._data.years > 0 ? value._data.years + ' years ' : '';
                string += value._data.days ? value._data.days + ' days ' : '';
                string += value._data.months ? value._data.months + ' months ' : '';
                string += value._data.hours ? value._data.hours + ' hours ' : '';
                string += value._data.minutes ? value._data.minutes + ' minutes ' : '';
                string += value._data.seconds ? value._data.seconds + ' seconds ' : '';

                return string;
            };


            $scope.searchTablePlants = function (str, plantCodes) {
                str1 = str.toUpperCase();
                $scope.filtersList.plantCodeList1 = $scope.filtersList.plantCodeListFilter.filter(function (code) {
                    if (String(code.PLANT_NAME).toUpperCase().includes(str1)) {
                        $scope.filtersList.plantCodeError = '';
                        return String(code.PLANT_NAME).toUpperCase().includes(str1);
                    } else {
                        $scope.filtersList.plantCodeError = 'Please select plants from dropdown only';
                        $scope.searchKeyword = '';
                    }
                });
            };

            $scope.selectPlantCode = function (obj, isChecked, PlantCodesArray) {
                if (isChecked) {
                    $scope.selectedCodesArray.push(obj);
                } else if (!isChecked) {
                    var tempindex = $scope.selectedCodesArray.indexOf(obj);
                    if (tempindex > -1) {
                        $scope.selectedCodesArray.splice(tempindex, 1);
                    }
                }

                $scope.TempAll = [];
                $scope.TempCodes = [];
                $scope.PlantCodesToGet = [];
                $scope.PlantCodesToGetCodes = [];
                $scope.selectedCodesArray = _.uniqBy($scope.selectedCodesArray, 'PLANT_CODE');
                $scope.selectedCodesArray = [];
                if (obj.PLANT_CODE === 'ALL') {
                    $scope.TempAll = PlantCodesArray.filter(function (T) {
                        return T.isChecked && T.PLANT_CODE === "ALL";
                    });

                    PlantCodesArray.forEach(function (code) {
                        if (code.isChecked && code.PLANT_CODE === 'ALL') {
                            $scope.PlantCodesToGet.push(code.PLANT_CODE);
                            $scope.selectedCodesArray.push(code);
                        } else {
                            code.isChecked = false;
                        }
                    });

                } else {
                    $scope.TempCodes = PlantCodesArray.filter(function (T) {
                        return T.isChecked && T.PLANT_CODE !== "ALL";
                    });

                    PlantCodesArray.forEach(function (code) {
                        if (code.isChecked && code.PLANT_CODE !== 'ALL') {
                            $scope.PlantCodesToGet.push(code.PLANT_CODE);
                            $scope.selectedCodesArray.push(code);
                        } else {
                            code.isChecked = false;
                        }
                    });
                }
                if ($scope.selectedCodesArray.length === 0) {
                    var allPlant1 = { PLANT_NAME: 'ALL', PLANT_CODE: 'ALL', isChecked: true };
                    PlantCodesArray.forEach(function (code) {
                        if (code.PLANT_CODE === 'ALL') {
                            code.isChecked = true;
                        }
                    });

                    $scope.selectedCodesArray.push(allPlant1);
                }
            };

            $scope.unCheckAll = function (PlantCodesArray) {
                var tempArray = [];
                var tempAllArray = [];
                tempArray = PlantCodesArray.filter(function (code) {
                    if (code.PLANT_NAME !== 'ALL' && code.isChecked) {
                        return code;
                    }
                });

                tempAllArray = PlantCodesArray.filter(function (code) {
                    if (tempArray.length > 0 && code.PLANT_NAME === 'ALL' && code.isChecked) {
                        code.isChecked = false;
                    }
                    if (code.PLANT_NAME === 'ALL' && code.isChecked) {
                        return code;
                    }
                });

                if (tempAllArray.length > 0) {
                    $scope.filtersList.plantCodeList1.forEach(function (item) {
                        if (item.PLANT_NAME !== 'ALL' && item.isChecked) {
                            item.isChecked = false;
                        }
                    });

                    $scope.selectedCodesArray.forEach(function (I) {
                        if (I.PLANT_NAME !== 'ALL' && I.isChecked) {
                            I.isChecked = false;
                        }
                    });
                }

                if (tempArray.length > 0) {
                    $scope.filtersList.plantCodeList1.forEach(function (item) {
                        if (item.PLANT_NAME === 'ALL' && item.isChecked) {
                            item.isChecked = false;
                        }
                    });

                    $scope.selectedCodesArray.forEach(function (I) {
                        if (I.PLANT_NAME === 'ALL' && I.isChecked) {
                            I.isChecked = false;
                        }
                    });
                } else {
                    $scope.filtersList.plantCodeList1.forEach(function (item) {
                        if (item.PLANT_NAME === 'ALL') {
                            item.isChecked = true;
                        }
                    });

                    $scope.selectedCodesArray.forEach(function (I) {
                        if (I.PLANT_NAME === 'ALL') {
                            I.isChecked = true;
                        }
                    });
                }
            };

            $scope.goToSaveDomesticQCS = function (reqID, qcsID) {
                var url = $state.href("cost-comparisions-qcs", { "reqID": reqID, "qcsID": qcsID });
                window.open(url, '_blank');
            };

            $scope.gotoViewReq = function (reqID) {
                var url = $state.href("view-requirement", { "Id": reqID });
                window.open(url, '_blank');
            };

            $scope.goToSaveImportQCS = function (reqID, qcsID) {
                var url = $state.href("import-qcs", { "reqID": reqID, "qcsID": qcsID });
                window.open(url, '_blank');
            };

            $scope.GetReportWithOutPagination = function (type, searchString) {
                $scope.downloadExcel = true;
                $scope.getAnylaticsReportsData(type, '', searchString, '', 0);
            };

            $scope.GetReport = function (type, data) {
                $scope.downloadExcel = false;
                $scope.ToExcelData = [];
                $scope.ToExcelData = data;
                if (type === 'totalReq') {
                    $scope.ToExcelData.forEach(function (item) {
                        item.req_POSTED_ON = $scope.getDateConverted(item.req_POSTED_ON);
                        item.closed = $scope.prmStatus($scope.isCustomer, item.closed);
                    });
                    alasql('SELECT REQ_NUMBER as [Requirement Number],PR_NUMBERS as [PR Number],title as [Title], ' +
                        'req_POSTED_ON as [Created Date], ' +
                        'reqPostedBy as [Created By], closed as [Req Status], vendorsInvited as [Vendors Invited],vendorsParticipated as [Vendors Participated], ' +
                        'SURROGATED_VENDOR_COUNT as [Surrogated Vendors],  savings as [Savings] ' +
                        'INTO XLSX(?, { headers: true, sheetid: "RequirementsReport", style: "font-size:15px;background:#233646;color:#FFF;" }) FROM ? ',
                        ["RequirementsReport.xlsx", $scope.ToExcelData]);

                }
                if (type === 'totalPlants') {
                    $scope.ToExcelData.forEach(function (item) {
                        item.req_POSTED_ON = $scope.getDateConverted(item.req_POSTED_ON);
                        item.closed = $scope.prmStatus($scope.isCustomer, item.closed);
                    });

                    alasql('SELECT REQ_NUMBER as [Requirement Number],PR_NUMBERS as [PR Number],title as [Title],PLANT_CODES as [Plant Code],PLANT_NAME as [Plant Name], ' +
                        'req_POSTED_ON as [Created Date], ' +
                        'reqPostedBy as [Created By], closed as [Req Status], vendorsInvited as [Vendors Invited],vendorsParticipated as [Vendors Participated], ' +
                        'SURROGATED_VENDOR_COUNT as [Surrogated Vendors],  savings as [Savings] ' +
                        'INTO XLSX(?, { headers: true, sheetid: "PlantsRequirementData", style: "font-size:15px;background:#233646;color:#FFF;" }) FROM ? ',
                        ["PlantsRequirementData.xlsx", $scope.ToExcelData]);
                }
                if (type === 'QCS_CURRENT_APPROVERS') {
                    $scope.ToExcelData.forEach(function (item) {
                        item.CREATED_DATE = $scope.getDateConverted(item.CREATED_DATE);
                        item.LATEST_ACTION_ON = $scope.getDateConverted(item.LATEST_ACTION_ON);
                        item.IS_PRIMARY_ID = item.IS_PRIMARY_ID > 1 ? 'SECONDARY' : 'PRIMARY';
                    });

                    alasql('SELECT QCS_CODE as [Qcs Code],REQ_NUMBER as [Req Number],PR_NUMBER as [PR Number],PR_LINE_ITEM as [PR Item Num], ' +
                        ' REQ_TITLE as [Req Title],IS_PRIMARY_ID as [Qcs Priority],QCS_TYPE as [Qcs Type],APPROVAL_STATUS as [Qcs Approval Status], ' +
                        ' CREATED_BY_NAME as [Created By],MODIFIED_BY_NAME as [Modified By],CREATED_DATE as [Created on],LATEST_ACTION_ON as [Latest Action],WF_TITLE as [Workflow Title] ' +
                        'INTO XLSX(?, { headers: true, sheetid: "QCSDetails", style: "font-size:15px;background:#233646;color:#FFF;" }) FROM ? ',
                        ["QCSDetails.xlsx", $scope.ToExcelData]);
                }
            };

        }]);