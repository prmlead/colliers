﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace PRMServices.Models
{
    [DataContract]
    public class rp_VendorAnalysis : Entity
    {
        [DataMember(Name = "RATIO_OF_WINNING")]
        public double RATIO_OF_WINNING { get; set; }

        [DataMember(Name = "MATERIALS_PROCURED")]
        public int MATERIALS_PROCURED { get; set; }

        [DataMember(Name = "SHARE_IN_MATERIAL")]
        public double SHARE_IN_MATERIAL { get; set; }

        [DataMember(Name = "SHARE_IN_BUYING")]
        public double SHARE_IN_BUYING { get; set; }

        
    }

   
}