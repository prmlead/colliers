﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace PRMServices.Models
{
    [DataContract]
    public class QCSReportDetails : Entity
    {
        [DataMember(Name = "REQ_ID")]
        public int REQ_ID { get; set; }

        [DataMember(Name = "QCS_ID")]
        public int QCS_ID { get; set; }

        [DataMember(Name = "QCS_CODE")]
        public string QCS_CODE { get; set; }

        [DataMember(Name = "CREATED_DATE")]
        public DateTime? CREATED_DATE { get; set; }

        [DataMember(Name = "QCS_TYPE")]
        public string QCS_TYPE { get; set; }

        [DataMember(Name = "IS_PRIMARY")]
        public int IS_PRIMARY { get; set; }

    }
}