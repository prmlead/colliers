﻿prmApp
    .config(["$stateProvider", "$urlRouterProvider", "$httpProvider", "$provide", "domain", "version",
        function ($stateProvider, $urlRouterProvider, $httpProvider, $provide, domain, version) {
            $stateProvider
                .state('supplier', {
                    url: '/supplier',
                    templateUrl: 'Reports/views/supplier.html'
                })
                .state('savingsDashBoard', {
                    url: '/savingsDashBoard',
                    templateUrl: 'Reports/views/savingsDashBoard.html'
                })
                .state('contractSpend', {
                    url: '/contractSpend',
                    templateUrl: 'Reports/views/contractSpend.html'
                })
                .state('averageProcure', {
                    url: '/averageProcure',
                    templateUrl: 'Reports/views/averageProcure.html'
                })
                .state('purchaseTurnOver', {
                    url: '/purchaseTurnOver',
                    templateUrl: 'Reports/views/purchaseTurnOver.html'
                })
                .state('purchaserAnalysis', {
                    url: '/purchaserAnalysis',
                    templateUrl: 'Reports/views/purchaserAnalysis.html'
                })
                .state('anylaticsReports', {
                    url: '/anylaticsReports',
                    templateUrl: 'Reports/views/anylaticsReports.html'

                }).state('vendorViewProfile', {
                    url: '/vendorViewProfile/:Id/:callID',
                    templateUrl: 'Reports/views/vendorViewProfile.html'
                }).state('openPRReport', {
                    url: '/openPRReport',
                    templateUrl: 'Reports/views/openPRReport.html'
                })
                .state('openPOReport', {
                    url: '/openPOReport',
                    templateUrl: 'Reports/views/openPOReport.html'
                }).state('grnAnylaticsReport', {
                    url: '/grnAnylaticsReport',
                    templateUrl: 'Reports/views/grnAnylaticsReport.html'
                });
        }]);