﻿using System;
using System.IO;
using System.Data;
using System.Linq;
using System.Web;
using System.Configuration;
using System.Collections.Generic;
using System.ServiceModel.Activation;

using OfficeOpenXml;
using PRMServices.Common;
using PRMServices.Models;
using MySql.Data.MySqlClient;
using PRMServices;
using PdfSharp.Pdf;
using TheArtOfDev.HtmlRenderer.PdfSharp;
using PRMServices.SQLHelper;
//using PRM.Core.Models.Reports;
using CORE = PRM.Core.Common;

namespace PRMServices
{
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class PRMAnalysisService : IPRMAnalysisService
    {

        private static NLog.Logger logger = NLog.LogManager.GetCurrentClassLogger();
        private MSSQLBizClass sqlHelper = new MSSQLBizClass();
        public PRMServices prm = new PRMServices();

        #region Services

        public PurchaserAnalysis GetPurchaserAnalysis(int userID, DateTime fromDate, DateTime toDate, string sessionid)
        {
            PurchaserAnalysis details = new PurchaserAnalysis();
            try
            {
                Utilities.ValidateSession(sessionid);
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                sd.Add("P_U_ID", userID);
                sd.Add("P_FROM_DATE", fromDate);
                sd.Add("P_TO_DATE", toDate);
                
                var dataset = sqlHelper.SelectList("cp_GetPurchaserAnalysis", sd);
                if (dataset != null && dataset.Tables.Count > 0)
                {
                    if (dataset.Tables[0].Rows.Count > 0 && dataset.Tables[0].Rows[0][0] != null)
                    {
                        DataRow row = dataset.Tables[0].Rows[0];
                        details.USER_SAVINGS = row["PARAM_USER_SAVINGS"] != DBNull.Value ? Convert.ToDouble(row["PARAM_USER_SAVINGS"]) : 0;
                        details.TOTAL_VOLUME = row["PARAM_TOTAL_VOLUME"] != DBNull.Value ? Convert.ToDouble(row["PARAM_TOTAL_VOLUME"]) : 0;
                        details.TOTAL_MATERIAL = row["PARAM_TOTAL_MATERIAL"] != DBNull.Value ? Convert.ToInt32(row["PARAM_TOTAL_MATERIAL"]) : 0;
                        details.TOTAL_RFQ = row["PARAM_TOTAL_RFQ"] != DBNull.Value ? Convert.ToInt32(row["PARAM_TOTAL_RFQ"]) : 0;
                        details.TOTAL_VENDORS = row["PARAM_TOTAL_VENDORS"] != DBNull.Value ? Convert.ToInt32(row["PARAM_TOTAL_VENDORS"]) : 0;
                        details.ADDED_VENDORS = row["PARAM_ADDED_VENDORS"] != DBNull.Value ? Convert.ToInt32(row["PARAM_ADDED_VENDORS"]) : 0;
                        details.RFQ_TAT = row["RFQTURNAROUNDTIME"] != DBNull.Value ? Convert.ToDouble(row["RFQTURNAROUNDTIME"]) : 0;
                        details.QCS_TAT = row["QCSTURNAROUNDTIME"] != DBNull.Value ? Convert.ToDouble(row["QCSTURNAROUNDTIME"]) : 0;
                        details.TOTAL_ITEM_SUM = row["PARAM_TOTAL_ITEM_SUM"] != DBNull.Value ? Convert.ToDouble(row["PARAM_TOTAL_ITEM_SUM"]) : 0;
                    }

                }

            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error in retrieving Data");
            }

            return details;
        }

        public PRMAnalyticsTotals GetPRMAnalyticsTotals(int compid, DateTime fromdate, DateTime todate, string sessionid)
        {
            PRMAnalyticsTotals detail = new PRMAnalyticsTotals();

            try
            {
                Utilities.ValidateSession(sessionid);
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                sd.Add("P_COMP_ID", compid);
                sd.Add("P_FROM_DATE", fromdate);
                sd.Add("P_TO_DATE", todate);
                var dataset = sqlHelper.SelectList("rp_PRMAnalyticsTotals", sd);
                CORE.DataNamesMapper<PRMAnalyticsTotals> mapper = new CORE.DataNamesMapper<PRMAnalyticsTotals>();
                detail = mapper.Map(dataset.Tables[0]).ToList().FirstOrDefault();

                detail.PLANTS = new List<PlantValues>();

                foreach (DataRow row in dataset.Tables[1].Rows)
                {
                    PlantValues val = new PlantValues();
                    val.PLANT_CODE = row["FIELD_NAME"] != DBNull.Value ? Convert.ToInt32(row["FIELD_NAME"]) : 0;
                    val.PLANT_NAME = row["FIELD_VALUE"] != DBNull.Value ? Convert.ToString(row["FIELD_VALUE"]) : string.Empty;
                    detail.PLANTS.Add(val);
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error in retrieving Data");
            }

            return detail;

        }

        public OpsReports GetAnylaticsReports(int userID, string from, string to, string type, string plantCode, int compID, string reqIDs, int qcsID, string sessionID, string search, int page, int pagesize)
        {
            OpsReports reports = new OpsReports();
            PRMServices prm = new PRMServices();
            List<QCSReportDetails> qcsDetails = new List<QCSReportDetails>();
            try
            {
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                sd.Add("P_USER_ID", userID);
                sd.Add("P_FROM_DATE", from);
                sd.Add("P_TO_DATE", to);
                sd.Add("P_TYPE", type);
                sd.Add("P_PLANT_CODE", plantCode);
                sd.Add("P_COMP_ID", compID);
                sd.Add("P_REQ_IDS", reqIDs);
                sd.Add("P_QCS_ID", qcsID);
                sd.Add("P_SEARCH", search);
                sd.Add("P_PAGE", page);
                sd.Add("P_PAGE_SIZE", pagesize);
                var ds = sqlHelper.SelectList("rp_PRMAnalyticsReports", sd);

                if (ds != null && ds.Tables.Count > 0)
                {
                    List<OpsRequirement> reqList = new List<OpsRequirement>();
                    reports.PRRFQData = new List<PRRFQData>();
                    reports.RFQDataWithPR = new List<Requirement>();
                    reports.QCS_CURRENT_APPROVERS = new List<QCSCURRENTAPPROVERS>();
                    reports.QCS_APPROVAL_HISTORY = new List<QCSAPPROVALHISTORY>();
                    reports.VENDOR_RESPONSE_TIME = new List<VENDORRESPONSETIME>();
                    reports.PR_PO_DATA = new List<PRPOData>();
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        if (type == "totalReq" || type == "totalPlants")
                        {
                            OpsRequirement reportsReq = new OpsRequirement();
                            reportsReq.RequirementID = row["REQ_ID"] != DBNull.Value ? Convert.ToInt32(row["REQ_ID"]) : 0;
                            reportsReq.Title = row["REQ_TITLE"] != DBNull.Value ? Convert.ToString(row["REQ_TITLE"]) : string.Empty;
                            reportsReq.PR_NUMBERS = row["PR_NUMBERS"] != DBNull.Value ? Convert.ToString(row["PR_NUMBERS"]) : string.Empty;
                            reportsReq.REQ_POSTED_ON = row["DATE_CREATED"] != DBNull.Value ? Convert.ToDateTime(row["DATE_CREATED"]) : DateTime.Now;
                            reportsReq.StartTime = row["START_TIME"] != DBNull.Value ? Convert.ToDateTime(row["START_TIME"]) : DateTime.MaxValue;
                            reportsReq.EndTime = row["END_TIME"] != DBNull.Value ? Convert.ToDateTime(row["END_TIME"]) : DateTime.MaxValue;
                            reportsReq.ReqPostedBy = row["Posted_By"] != DBNull.Value ? Convert.ToString(row["Posted_By"]) : string.Empty;
                            reportsReq.CLOSED = row["CLOSED"] != DBNull.Value ? Convert.ToString(row["CLOSED"]) : string.Empty;
                            string Status = row["CLOSED"] != DBNull.Value ? Convert.ToString(row["CLOSED"]) : string.Empty;
                            reportsReq.VendorsInvited = row["VENDORS_INVITED"] != DBNull.Value ? Convert.ToInt32(row["VENDORS_INVITED"]) : 0;
                            reportsReq.VendorsParticipated = row["VENDORS_PARTICIPATED"] != DBNull.Value ? Convert.ToInt32(row["VENDORS_PARTICIPATED"]) : 0;
                            reportsReq.SURROGATED_VENDOR_COUNT = row["SURROGATED_VENDOR_COUNT"] != DBNull.Value ? Convert.ToInt32(row["SURROGATED_VENDOR_COUNT"]) : 0;
                            reportsReq.MinIbitialQuotationPrice = row["MIN_INITIAL_QUOTATION_PRICE"] != DBNull.Value ? Convert.ToDouble(row["MIN_INITIAL_QUOTATION_PRICE"]) : 0;
                            reportsReq.SAVINGS = row["SAVINGS"] != DBNull.Value ? Convert.ToDouble(row["SAVINGS"]) : 0;
                            reportsReq.PLANT_CODES = row["PLANT_CODES"] != DBNull.Value ? Convert.ToString(row["PLANT_CODES"]) : string.Empty;
                            reportsReq.PLANT_NAME = row["PLANT_NAME"] != DBNull.Value ? Convert.ToString(row["PLANT_NAME"]) : string.Empty;
                            reportsReq.REQ_NUMBER = row["REQ_NUMBER"] != DBNull.Value ? Convert.ToString(row["REQ_NUMBER"]) : string.Empty;
                            var prStatus = row["REQ_PR_STATUS"] != DBNull.Value ? Convert.ToString(row["REQ_PR_STATUS"]) : string.Empty;
                            reportsReq.TotalCount = row["TOTAL_ROWS"] != DBNull.Value ? Convert.ToInt32(row["TOTAL_ROWS"]) : 0;
                            DateTime now = DateTime.UtcNow;
                            if (reportsReq.EndTime != DateTime.MaxValue && reportsReq.EndTime > now && reportsReq.StartTime < now)
                            {
                                DateTime date = Convert.ToDateTime(reportsReq.EndTime);
                                long diff = Convert.ToInt64((date - now).TotalSeconds);
                                reportsReq.TimeLeft = diff;
                                reportsReq.CLOSED = prm.GetEnumDesc<PRMStatus>(PRMStatus.STARTED.ToString());
                            }
                            else if (reportsReq.StartTime != DateTime.MaxValue && reportsReq.StartTime > now)
                            {
                                DateTime date = Convert.ToDateTime(reportsReq.StartTime);
                                long diff = Convert.ToInt64((date - now).TotalSeconds);
                                reportsReq.TimeLeft = diff;
                                reportsReq.CLOSED = prm.GetEnumDesc<PRMStatus>(PRMStatus.NOTSTARTED.ToString());
                            }
                            else if (reportsReq.EndTime < now)
                            {
                                reportsReq.TimeLeft = -1;
                                if ((Status == prm.GetEnumDesc<PRMStatus>(PRMStatus.NOTSTARTED.ToString()) || Status == prm.GetEnumDesc<PRMStatus>(PRMStatus.STARTED.ToString())) && reportsReq.EndTime < now)
                                {
                                    reportsReq.CLOSED = prm.GetEnumDesc<PRMStatus>(PRMStatus.NegotiationEnded.ToString());
                                    //prm.EndNegotiation(reportsReq.RequirementID, reportsReq.CustomerID, sessionID);
                                }
                                else
                                {
                                    reportsReq.CLOSED = Status;
                                }
                            }
                            else if (reportsReq.StartTime == DateTime.MaxValue)
                            {
                                reportsReq.TimeLeft = -1;
                                reportsReq.CLOSED = prm.GetEnumDesc<PRMStatus>(PRMStatus.UNCONFIRMED.ToString());
                            }
                            if (Status == prm.GetEnumDesc<PRMStatus>(PRMStatus.DELETED.ToString()))
                            {
                                reportsReq.TimeLeft = -1;
                                reportsReq.CLOSED = Status;
                            }

                            if (reportsReq.CLOSED == "Negotiation Ended")
                            {
                                reportsReq.CLOSED = "Qcs Pending";
                                //requirement.Status = tempStatus;
                                if (ds.Tables.Count > 0 && ds.Tables[1].Rows.Count > 0)
                                {
                                    var requirements = ds.Tables[1].Select("REQ_ID = " + reportsReq.RequirementID);
                                    if (requirements != null && requirements.Length > 0)
                                    {
                                        int wfId = Convert.ToInt32(requirements[0]["WF_ID"]);
                                        if (wfId == 0)
                                        {
                                            reportsReq.CLOSED = "Qcs Created";
                                        }
                                    }
                                }

                                if (!reportsReq.CLOSED.Equals("Qcs Created", StringComparison.InvariantCultureIgnoreCase) && ds.Tables.Count > 0 && ds.Tables[2].Rows.Count > 0)
                                {
                                    var wfStatusRequirementsLastApprover = ds.Tables[2].Select("REQ_ID = " + reportsReq.RequirementID).FirstOrDefault();
                                    if (wfStatusRequirementsLastApprover != null)
                                    {
                                        var latestQCSId = wfStatusRequirementsLastApprover["QCS_ID"] != DBNull.Value ? Convert.ToInt32(wfStatusRequirementsLastApprover["QCS_ID"]) : 0;
                                        var wfStatusRequirements = ds.Tables[2].Select("REQ_ID = " + reportsReq.RequirementID + " AND QCS_ID = " + latestQCSId).ToList();
                                        if (wfStatusRequirements != null && wfStatusRequirementsLastApprover != null)
                                        {
                                            var LastApproverstatus = wfStatusRequirementsLastApprover["WF_STATUS"] != DBNull.Value ? Convert.ToString(wfStatusRequirementsLastApprover["WF_STATUS"]) : string.Empty;
                                            var LastApproverOrder = wfStatusRequirementsLastApprover["WF_ORDER"] != DBNull.Value ? Convert.ToInt32(wfStatusRequirementsLastApprover["WF_ORDER"]) : 0;

                                            foreach (var track in wfStatusRequirements)
                                            {
                                                var status2 = track["WF_STATUS"] != DBNull.Value ? Convert.ToString(track["WF_STATUS"]) : string.Empty;
                                                var order = track["WF_ORDER"] != DBNull.Value ? Convert.ToInt32(track["WF_ORDER"]) : 0;

                                                if (order == 1 && status2 == "REJECTED")
                                                {
                                                    if (status2 == "PENDING" || status2 == "HOLD")
                                                    {
                                                        reportsReq.CLOSED = "Qcs Approval Pending";
                                                    }
                                                    else if (status2 == "REJECTED")
                                                    {
                                                        reportsReq.CLOSED = "Qcs Rejected";
                                                    }
                                                    else
                                                    {
                                                        reportsReq.CLOSED = "Qcs Approved";
                                                    }
                                                }
                                                else if (LastApproverOrder == order)
                                                {
                                                    if (status2 == "PENDING" || status2 == "HOLD")
                                                    {
                                                        reportsReq.CLOSED = "Qcs Approval Pending";
                                                    }
                                                    else if (status2 == "REJECTED")
                                                    {
                                                        reportsReq.CLOSED = "Qcs Rejected";
                                                    }
                                                    else
                                                    {
                                                        reportsReq.CLOSED = "Qcs Approved";
                                                    }
                                                }
                                            }
                                        }
                                    }

                                }

                                if (!string.IsNullOrEmpty(reportsReq.PR_NUMBERS))
                                {
                                    if (!string.IsNullOrEmpty(prStatus) && prStatus.Equals("CLOSED", StringComparison.InvariantCultureIgnoreCase))
                                    {
                                        reportsReq.CLOSED = "Closed";
                                    }
                                }
                            }

                            reqList.Add(reportsReq);
                        }
                        if (type == "PR_RFQ")
                        {
                            PRRFQData data = new PRRFQData();
                            data.PR_NUMBER = row["PR_NUMBER"] != DBNull.Value ? Convert.ToString(row["PR_NUMBER"]) : string.Empty;
                            data.REQ_IDS = row["REQ_IDS"] != DBNull.Value ? Convert.ToString(row["REQ_IDS"]) : string.Empty;
                            data.PR_ITEM_IDS = row["PR_ITEM_IDS"] != DBNull.Value ? Convert.ToString(row["PR_ITEM_IDS"]) : string.Empty;
                            data.REQ_COUNT = row["REQ_COUNT"] != DBNull.Value ? Convert.ToInt32(row["REQ_COUNT"]) : 0;
                            data.CREATED_BY = row["CREATED_BY"] != DBNull.Value ? Convert.ToString(row["CREATED_BY"]) : string.Empty;
                            data.RELEASE_DATE = row["RELEASE_DATE"] != DBNull.Value ? Convert.ToDateTime(row["RELEASE_DATE"]) : DateTime.Now;
                            data.CREATED_DATE = row["CREATED_DATE"] != DBNull.Value ? Convert.ToDateTime(row["CREATED_DATE"]) : DateTime.Now;
                            data.MODIFIED_DATE = row["MODIFIED_DATE"] != DBNull.Value ? Convert.ToDateTime(row["MODIFIED_DATE"]) : DateTime.Now;
                            data.TotalCount = row["TOTAL_ROWS"] != DBNull.Value ? Convert.ToInt32(row["TOTAL_ROWS"]) : 0;
                            reports.PRRFQData.Add(data);
                        }

                        if (type == "RFQ")
                        {
                            Requirement data = new Requirement();
                            data.RequirementNumber = row["REQ_NUMBER"] != DBNull.Value ? Convert.ToString(row["REQ_NUMBER"]) : string.Empty;
                            data.Title = row["REQ_TITLE"] != DBNull.Value ? Convert.ToString(row["REQ_TITLE"]) : string.Empty;
                            data.RequirementID = row["REQ_ID"] != DBNull.Value ? Convert.ToInt32(row["REQ_ID"]) : 0;
                            data.Status = row["CLOSED"] != DBNull.Value ? Convert.ToString(row["CLOSED"]) : string.Empty;
                            string Status = row["CLOSED"] != DBNull.Value ? Convert.ToString(row["CLOSED"]) : string.Empty;
                            data.StartTime = row["START_TIME"] != DBNull.Value ? Convert.ToDateTime(row["START_TIME"]) : DateTime.MaxValue;
                            data.EndTime = row["END_TIME"] != DBNull.Value ? Convert.ToDateTime(row["END_TIME"]) : DateTime.MaxValue;
                            data.CB_END_TIME = row["CB_END_TIME"] != DBNull.Value ? Convert.ToDateTime(row["CB_END_TIME"]) : DateTime.Now;
                            data.PRNumbers = row["PR_NUMBER"] != DBNull.Value ? Convert.ToString(row["PR_NUMBER"]) : string.Empty;
                            data.NoOfVendorsInvited = row["VENDORS_INVITED"] != DBNull.Value ? Convert.ToInt16(row["VENDORS_INVITED"]) : 0;
                            data.NoOfVendorsParticipated = row["VENDORS_PARTICIPATED"] != DBNull.Value ? Convert.ToInt16(row["VENDORS_PARTICIPATED"]) : 0;
                            var prStatus = row["REQ_PR_STATUS"] != DBNull.Value ? Convert.ToString(row["REQ_PR_STATUS"]) : string.Empty;
                            DateTime now = DateTime.UtcNow;
                            if (data.EndTime != DateTime.MaxValue && data.EndTime > now && data.StartTime < now)
                            {
                                DateTime date = Convert.ToDateTime(data.EndTime);
                                long diff = Convert.ToInt64((date - now).TotalSeconds);
                                data.TimeLeft = diff;
                                data.Status = prm.GetEnumDesc<PRMStatus>(PRMStatus.STARTED.ToString());
                            }
                            else if (data.StartTime != DateTime.MaxValue && data.StartTime > now)
                            {
                                DateTime date = Convert.ToDateTime(data.StartTime);
                                long diff = Convert.ToInt64((date - now).TotalSeconds);
                                data.TimeLeft = diff;
                                data.Status = prm.GetEnumDesc<PRMStatus>(PRMStatus.NOTSTARTED.ToString());
                            }
                            else if (data.EndTime < now)
                            {
                                data.TimeLeft = -1;
                                if ((Status == prm.GetEnumDesc<PRMStatus>(PRMStatus.NOTSTARTED.ToString()) || Status == prm.GetEnumDesc<PRMStatus>(PRMStatus.STARTED.ToString())) && data.EndTime < now)
                                {
                                    data.Status = prm.GetEnumDesc<PRMStatus>(PRMStatus.NegotiationEnded.ToString());
                                    // prm.EndNegotiation(data.RequirementID, data.CustomerID, sessionID);
                                }
                                else
                                {
                                    data.Status = Status;
                                }
                            }
                            else if (data.StartTime == DateTime.MaxValue)
                            {
                                data.TimeLeft = -1;
                                data.Status = prm.GetEnumDesc<PRMStatus>(PRMStatus.UNCONFIRMED.ToString());
                            }
                            if (Status == prm.GetEnumDesc<PRMStatus>(PRMStatus.DELETED.ToString()))
                            {
                                data.TimeLeft = -1;
                                data.Status = Status;
                            }

                            if (data.Status == "Negotiation Ended")
                            {
                                data.Status = "Qcs Pending";
                                //requirement.Status = tempStatus;
                                if (ds.Tables.Count > 0 && ds.Tables[1].Rows.Count > 0)
                                {
                                    var requirements = ds.Tables[1].Select("REQ_ID = " + data.RequirementID);
                                    if (requirements != null && requirements.Length > 0)
                                    {
                                        int wfId = Convert.ToInt32(requirements[0]["WF_ID"]);
                                        if (wfId == 0)
                                        {
                                            data.Status = "Qcs Created";
                                        }
                                    }
                                }

                                if (!data.Status.Equals("Qcs Created", StringComparison.InvariantCultureIgnoreCase) && ds.Tables.Count > 0 && ds.Tables[2].Rows.Count > 0)
                                {
                                    var wfStatusRequirementsLastApprover = ds.Tables[2].Select("REQ_ID = " + data.RequirementID).FirstOrDefault();
                                    if (wfStatusRequirementsLastApprover != null)
                                    {
                                        var latestQCSId = wfStatusRequirementsLastApprover["QCS_ID"] != DBNull.Value ? Convert.ToInt32(wfStatusRequirementsLastApprover["QCS_ID"]) : 0;
                                        var wfStatusRequirements = ds.Tables[2].Select("REQ_ID = " + data.RequirementID + " AND QCS_ID = " + latestQCSId).ToList();
                                        if (wfStatusRequirements != null && wfStatusRequirementsLastApprover != null)
                                        {
                                            var LastApproverstatus = wfStatusRequirementsLastApprover["WF_STATUS"] != DBNull.Value ? Convert.ToString(wfStatusRequirementsLastApprover["WF_STATUS"]) : string.Empty;
                                            var LastApproverOrder = wfStatusRequirementsLastApprover["WF_ORDER"] != DBNull.Value ? Convert.ToInt32(wfStatusRequirementsLastApprover["WF_ORDER"]) : 0;

                                            foreach (var track in wfStatusRequirements)
                                            {
                                                var status2 = track["WF_STATUS"] != DBNull.Value ? Convert.ToString(track["WF_STATUS"]) : string.Empty;
                                                var order = track["WF_ORDER"] != DBNull.Value ? Convert.ToInt32(track["WF_ORDER"]) : 0;

                                                if (order == 1 && status2 == "REJECTED")
                                                {
                                                    if (status2 == "PENDING" || status2 == "HOLD")
                                                    {
                                                        data.Status = "Qcs Approval Pending";
                                                    }
                                                    else if (status2 == "REJECTED")
                                                    {
                                                        data.Status = "Qcs Rejected";
                                                    }
                                                    else
                                                    {
                                                        data.Status = "Qcs Approved";
                                                    }
                                                }
                                                else if (LastApproverOrder == order)
                                                {
                                                    if (status2 == "PENDING" || status2 == "HOLD")
                                                    {
                                                        data.Status = "Qcs Approval Pending";
                                                    }
                                                    else if (status2 == "REJECTED")
                                                    {
                                                        data.Status = "Qcs Rejected";
                                                    }
                                                    else
                                                    {
                                                        data.Status = "Qcs Approved";
                                                    }
                                                }
                                            }
                                        }
                                    }

                                }

                                if (!string.IsNullOrEmpty(data.PRNumbers))
                                {
                                    if (!string.IsNullOrEmpty(prStatus) && prStatus.Equals("CLOSED", StringComparison.InvariantCultureIgnoreCase))
                                    {
                                        data.Status = "Closed";
                                    }
                                }
                            }

                            reports.RFQDataWithPR.Add(data);
                        }

                        if (type == "QCS_CURRENT_APPROVERS")
                        {
                            QCSCURRENTAPPROVERS data = new QCSCURRENTAPPROVERS();
                            data.REQ_NUMBER = row["REQ_NUMBER"] != DBNull.Value ? Convert.ToString(row["REQ_NUMBER"]) : string.Empty;
                            data.QCS_CODE = row["QCS_CODE"] != DBNull.Value ? Convert.ToString(row["QCS_CODE"]) : string.Empty;
                            data.WF_TITLE = row["WF_TITLE"] != DBNull.Value ? Convert.ToString(row["WF_TITLE"]) : string.Empty;
                            data.REQ_ID = row["REQ_ID"] != DBNull.Value ? Convert.ToInt32(row["REQ_ID"]) : 0;
                            data.QCS_ID = row["QCS_ID"] != DBNull.Value ? Convert.ToInt32(row["QCS_ID"]) : 0;
                            data.IS_PRIMARY_ID = row["IS_PRIMARY_ID"] != DBNull.Value ? Convert.ToInt32(row["IS_PRIMARY_ID"]) : 0;
                            data.REQ_TITLE = row["REQ_TITLE"] != DBNull.Value ? Convert.ToString(row["REQ_TITLE"]) : string.Empty;
                            data.QCS_TYPE = row["QCS_TYPE"] != DBNull.Value ? Convert.ToString(row["QCS_TYPE"]) : string.Empty;
                            data.APPROVAL_STATUS = row["APPROVAL_STATUS"] != DBNull.Value ? Convert.ToString(row["APPROVAL_STATUS"]) : string.Empty;
                            data.MODIFIED_BY_NAME = row["MODIFIED_BY_NAME"] != DBNull.Value ? Convert.ToString(row["MODIFIED_BY_NAME"]) : string.Empty;
                            data.CREATED_BY_NAME = row["CREATED_BY"] != DBNull.Value ? Convert.ToString(row["CREATED_BY"]) : string.Empty;
                            data.LATEST_ACTION_ON = row["LATEST_ACTION_ON"] != DBNull.Value ? Convert.ToDateTime(row["LATEST_ACTION_ON"]) : DateTime.Now;
                            data.CREATED_DATE = row["CREATED_DATE"] != DBNull.Value ? Convert.ToDateTime(row["CREATED_DATE"]) : DateTime.Now;
                            data.PR_NUMBER = row["PR_NUMBER"] != DBNull.Value ? Convert.ToString(row["PR_NUMBER"]) : string.Empty;
                            data.PR_LINE_ITEM = row["PR_LINE_ITEM"] != DBNull.Value ? Convert.ToString(row["PR_LINE_ITEM"]) : string.Empty;
                            data.TotalCount = row["TOTAL_ROWS"] != DBNull.Value ? Convert.ToInt32(row["TOTAL_ROWS"]) : 0;
                            reports.QCS_CURRENT_APPROVERS.Add(data);
                        }

                        if (type == "QCS_APPROVAL_HISTORY")
                        {
                            QCSAPPROVALHISTORY data = new QCSAPPROVALHISTORY();
                            data.WF_STATUS = row["WF_STATUS"] != DBNull.Value ? Convert.ToString(row["WF_STATUS"]) : string.Empty;
                            data.WF_COMMENTS = row["WF_COMMENTS"] != DBNull.Value ? Convert.ToString(row["WF_COMMENTS"]) : string.Empty;
                            data.MODIFIED_BY = row["MODIFIED_BY"] != DBNull.Value ? Convert.ToString(row["MODIFIED_BY"]) : string.Empty;
                            data.CREATED = row["CREATED"] != DBNull.Value ? Convert.ToDateTime(row["CREATED"]) : DateTime.Now;
                            reports.QCS_APPROVAL_HISTORY.Add(data);
                        }

                        if (type == "VENDOR_RESPONSE")
                        {
                            VENDORRESPONSETIME data = new VENDORRESPONSETIME();
                            data.VENDOR_NAME = row["VENDOR_NAME"] != DBNull.Value ? Convert.ToString(row["VENDOR_NAME"]) : string.Empty;
                            data.VENDOR_EMAIL = row["U_EMAIL"] != DBNull.Value ? Convert.ToString(row["U_EMAIL"]) : string.Empty;
                            data.COMP_NAME = row["COMP_NAME"] != DBNull.Value ? Convert.ToString(row["COMP_NAME"]) : string.Empty;
                            data.VENDOR_ID = row["VENDOR_ID"] != DBNull.Value ? Convert.ToInt32(row["VENDOR_ID"]) : 0;
                            data.VENDOR_INVITED = row["VENDOR_INVITED"] != DBNull.Value ? Convert.ToInt32(row["VENDOR_INVITED"]) : 0;
                            data.VENDOR_PARTICIPATED = row["VENDOR_PARTICIPATED"] != DBNull.Value ? Convert.ToInt32(row["VENDOR_PARTICIPATED"]) : 0;
                            data.VENDOR_NOT_PARTICIPATED = row["VENDOR_NOT_PARTICIPATED"] != DBNull.Value ? Convert.ToInt32(row["VENDOR_NOT_PARTICIPATED"]) : 0;
                            data.RESPONSE_TIME_ALL = row["RESPONSE_TIME_ALL"] != DBNull.Value ? Convert.ToDouble(row["RESPONSE_TIME_ALL"]) : 0;
                            data.TotalCount = row["TOTAL_ROWS"] != DBNull.Value ? Convert.ToInt32(row["TOTAL_ROWS"]) : 0;
                            reports.VENDOR_RESPONSE_TIME.Add(data);
                        }

                        if (type == "PR_PO")
                        {

                            PRPOData data = new PRPOData();
                            data.PO_CREATED_DATE = row["PO_CREATED_DATE"] != DBNull.Value ? Convert.ToDateTime(row["PO_CREATED_DATE"]) : DateTime.Now;
                            data.REQ_POSTED_ON = row["REQ_POSTED_ON"] != DBNull.Value ? Convert.ToDateTime(row["REQ_POSTED_ON"]) : DateTime.Now;
                            data.PR_NUMBER = row["PR_NUMBER"] != DBNull.Value ? Convert.ToString(row["PR_NUMBER"]) : string.Empty;
                            data.PR_RELEASE_DATE = row["PR_RELEASE_DATE"] != DBNull.Value ? Convert.ToDateTime(row["PR_RELEASE_DATE"]) : DateTime.Now;
                            data.PO_NUMBER = row["PO_NUMBER"] != DBNull.Value ? Convert.ToString(row["PO_NUMBER"]) : string.Empty;
                            data.REQ_ID = row["REQ_ID"] != DBNull.Value ? Convert.ToInt32(row["REQ_ID"]) : 0;
                            data.REQ_NUMBER = row["REQ_NUMBER"] != DBNull.Value ? Convert.ToString(row["REQ_NUMBER"]) : string.Empty;
                            data.VENDORS_INVITED = row["VENDORS_INVITED"] != DBNull.Value ? Convert.ToInt32(row["VENDORS_INVITED"]) : 0;
                            data.VENDORS_PARTICIPATED = row["VENDORS_PARTICIPATED"] != DBNull.Value ? Convert.ToInt32(row["VENDORS_PARTICIPATED"]) : 0;
                            data.SURROGATED_VENDOR = row["SURROGATED_VENDOR"] != DBNull.Value ? Convert.ToInt32(row["SURROGATED_VENDOR"]) : 0;
                            data.ALL_REQ_VENDORS = row["ALL_REQ_VENDORS"] != DBNull.Value ? Convert.ToString(row["ALL_REQ_VENDORS"]) : string.Empty;
                            data.REQ_TITLE = row["REQ_TITLE"] != DBNull.Value ? Convert.ToString(row["REQ_TITLE"]) : string.Empty;
                            data.PR_ID = row["PR_ID"] != DBNull.Value ? Convert.ToInt32(row["PR_ID"]) : 0;
                            data.TotalCount = row["TOTAL_ROWS"] != DBNull.Value ? Convert.ToInt32(row["TOTAL_ROWS"]) : 0;
                            data.PR_PO_VENDOR_LIST = new List<PRPOVENDORDATA>();

                            var rfqVendors = ds.Tables[1].Select("REQ_ID = " + data.REQ_ID + " and PR_ID = " + data.PR_ID);
                            if (rfqVendors != null && rfqVendors.Length > 0)
                            {
                                foreach (DataRow row1 in rfqVendors)
                                {
                                    PRPOVENDORDATA vendorObj = new PRPOVENDORDATA();
                                    vendorObj.REQ_ID = row1["REQ_ID"] != DBNull.Value ? Convert.ToInt32(row1["REQ_ID"]) : 0;
                                    vendorObj.VENDOR_FULL_NAME = row1["VENDOR_FULL_NAME"] != DBNull.Value ? Convert.ToString(row1["VENDOR_FULL_NAME"]) : string.Empty;
                                    vendorObj.VENDOR_COMP_NAME = row1["VENDOR_COMP_NAME"] != DBNull.Value ? Convert.ToString(row1["VENDOR_COMP_NAME"]) : string.Empty;
                                    vendorObj.PR_ID = row1["PR_ID"] != DBNull.Value ? Convert.ToInt32(row1["PR_ID"]) : 0;
                                    vendorObj.QUOTED_ON = row1["QUOTED_ON"] != DBNull.Value ? Convert.ToDateTime(row1["QUOTED_ON"]) : DateTime.Now;
                                    data.PR_PO_VENDOR_LIST.Add(vendorObj);
                                }
                            }
                            reports.PR_PO_DATA.Add(data);
                        }
                    }
                    if (type == "RFQ")
                    {
                        foreach (DataRow row1 in ds.Tables[3].Rows)
                        {

                            QCSReportDetails qcs = new QCSReportDetails();
                            qcs.REQ_ID = row1["REQ_ID"] != DBNull.Value ? Convert.ToInt32(row1["REQ_ID"]) : 0;
                            qcs.QCS_ID = row1["QCS_ID"] != DBNull.Value ? Convert.ToInt32(row1["QCS_ID"]) : 0;
                            qcs.IS_PRIMARY = row1["IS_PRIMARY_ID"] != DBNull.Value ? Convert.ToInt32(row1["IS_PRIMARY_ID"]) : 0;
                            qcs.QCS_CODE = row1["QCS_CODE"] != DBNull.Value ? Convert.ToString(row1["QCS_CODE"]) : string.Empty;
                            qcs.QCS_TYPE = row1["QCS_TYPE"] != DBNull.Value ? Convert.ToString(row1["QCS_TYPE"]) : string.Empty;
                            qcs.CREATED_DATE = row1["CREATED_DATE"] != DBNull.Value ? Convert.ToDateTime(row1["CREATED_DATE"]) : DateTime.Now;
                            qcsDetails.Add(qcs);

                        }
                    }

                    foreach (Requirement req in reports.RFQDataWithPR)
                    {
                        req.QCSDetails = qcsDetails.Where(a => a.REQ_ID == req.RequirementID).ToList();
                    }
                    reports.ReqTransactions = reqList;
                }
            }
            catch (Exception ex)
            {
                OpsReports recuirementerror = new OpsReports();
                recuirementerror.ErrorMessage = ex.Message;
            }
            finally
            {
                //cmd.Connection.Close();
            }
            return reports;
        }

        public List<Requirement> GetVendorNotParticipatedList(int vendorID)
        {
            List<Requirement> details = new List<Requirement>();
            List<RequirementItems> items = new List<RequirementItems>();
            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            sd.Add("P_VENDOR_ID", vendorID);
            DataSet ds = sqlHelper.SelectList("rp_GetVendorNotParticipatedReq", sd);

            if (ds != null && ds.Tables.Count >= 0)
            {
                foreach (DataRow row in ds.Tables[0].Rows)
                {
                    Requirement vd = new Requirement();
                    vd.RequirementID = row["REQ_ID"] != DBNull.Value ? Convert.ToInt32(row["REQ_ID"]) : 0;
                    details.Add(vd);
                }

                foreach (DataRow row1 in ds.Tables[1].Rows)
                {
                    RequirementItems item = new RequirementItems();
                    item.RequirementID = row1["REQ_ID"] != DBNull.Value ? Convert.ToInt32(row1["REQ_ID"]) : 0;
                    item.IsRegret = row1["IS_REGRET"] != DBNull.Value ? (Convert.ToInt32(row1["IS_REGRET"]) > 0 ? true : false) : false;
                    item.RegretComments = row1["REGRET_COMMENTS"] != DBNull.Value ? Convert.ToString(row1["REGRET_COMMENTS"]) : string.Empty;
                    item.ProductIDorName = row1["PROD_ID"] != DBNull.Value ? Convert.ToString(row1["PROD_ID"]) : string.Empty;
                    item.ProductNo = row1["PROD_NO"] != DBNull.Value ? Convert.ToString(row1["PROD_NO"]) : string.Empty;
                    items.Add(item);
                }

                foreach (Requirement req in details)
                {
                    req.ListRequirementItems = items.Where(a => a.RequirementID == req.RequirementID).ToList();

                }
            }

            return details;
        }

        public List<KeyValuePair> GetOpenPRStatsbyMonth(int compid, string sessionid)
        {
            List<KeyValuePair> details = new List<KeyValuePair>();
            try
            {
                Utilities.ValidateSession(sessionid);
                string query = $@"SELECT FORMAT(PR.RELEASE_DATE, 'yyyy') AS PR_YEAR, FORMAT(PR.RELEASE_DATE, 'MMMM') AS PR_MONTH, COUNT(PRI.PR_ID) AS PR_COUNT  FROM PRItems PRI INNER JOIN PRDetails PR ON PR.PR_ID = PRI.PR_ID
                                WHERE ISNULL(PRI.REQ_ID, 0) <= 0 AND PR.COMP_ID = {compid} AND PRI.IS_VALID = 1
                                GROUP BY FORMAT(PR.RELEASE_DATE, 'yyyy'), FORMAT(PR.RELEASE_DATE, 'MMMM'), FORMAT(PR.RELEASE_DATE, 'MM')
                                ORDER BY FORMAT(PR.RELEASE_DATE, 'yyyy'), FORMAT(PR.RELEASE_DATE, 'MM');";

                var dataset = sqlHelper.ExecuteQuery(query);
                if (dataset != null && dataset.Tables.Count > 0)
                {
                    if (dataset.Tables[0].Rows.Count > 0 && dataset.Tables[0].Rows.Count > 0)
                    {
                        foreach (var row in dataset.Tables[0].AsEnumerable())
                        {
                            var keyValue = new KeyValuePair();
                            string prYear = row["PR_YEAR"] != DBNull.Value ? Convert.ToString(row["PR_YEAR"]) : string.Empty;
                            string prMonth = row["PR_MONTH"] != DBNull.Value ? Convert.ToString(row["PR_MONTH"]) : string.Empty;
                            int prCount = row["PR_COUNT"] != DBNull.Value ? Convert.ToInt32(row["PR_COUNT"]) : 0;

                            keyValue.Key1 = prYear + "-" + prMonth;
                            keyValue.Value1 = prCount;

                            details.Add(keyValue);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error in retrieving Data");
            }

            return details;
        }

        public List<PRItems> GetOpenPRData(int compid, string yearmonth, int page, int pagesize, string sessionid)
        {
            List<PRItems> details = new List<PRItems>();
            try
            {
                Utilities.ValidateSession(sessionid);
                string month = "";
                string year = "";

                if (!string.IsNullOrEmpty(yearmonth))
                {
                    year = yearmonth.Split('-')[0];
                    month = yearmonth.Split('-')[1];
                }


                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                sd.Add("P_COMP_ID", compid);
                sd.Add("P_YEAR", year);
                sd.Add("P_MONTH", month);
                sd.Add("P_PAGE", page);
                sd.Add("P_PAGE_SIZE", pagesize);

                DataSet dataset = sqlHelper.SelectList("rp_GetOpenPRData", sd);

                if (dataset != null && dataset.Tables.Count > 0)
                {
                    if (dataset.Tables[0].Rows.Count > 0 && dataset.Tables[0].Rows.Count > 0)
                    {
                        CORE.DataNamesMapper<PRItems> mapper = new CORE.DataNamesMapper<PRItems>();
                        details = mapper.Map(dataset.Tables[0]).ToList();
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error in retrieving Data");
            }

            return details;
        }

        public List<KeyValuePair> GetOpenPOStatsbyMonth(int compid, string sessionid)
        {
            List<KeyValuePair> details = new List<KeyValuePair>();
            try
            {
                Utilities.ValidateSession(sessionid);
                string query = $@"SELECT FORMAT(PO.PO_RELEASE_DATE, 'yyyy') AS PO_YEAR, FORMAT(PO.PO_RELEASE_DATE, 'MMMM') AS PO_MONTH, COUNT(PO.PO_NUMBER) AS PO_COUNT  FROM POScheduleDetails PO
                                WHERE PO.COMP_ID = {compid} AND PO.PO_STATUS <> 'CLOSED' AND PO_RELEASE_DATE IS NOT NULL
                                GROUP BY FORMAT(PO.PO_RELEASE_DATE, 'yyyy'), FORMAT(PO.PO_RELEASE_DATE, 'MMMM'), FORMAT(PO.PO_RELEASE_DATE, 'MM')
                                ORDER BY FORMAT(PO.PO_RELEASE_DATE, 'yyyy'), FORMAT(PO.PO_RELEASE_DATE, 'MM');";

                var dataset = sqlHelper.ExecuteQuery(query);
                if (dataset != null && dataset.Tables.Count > 0)
                {
                    if (dataset.Tables[0].Rows.Count > 0 && dataset.Tables[0].Rows.Count > 0)
                    {
                        foreach (var row in dataset.Tables[0].AsEnumerable())
                        {
                            var keyValue = new KeyValuePair();
                            string prYear = row["PO_YEAR"] != DBNull.Value ? Convert.ToString(row["PO_YEAR"]) : string.Empty;
                            string prMonth = row["PO_MONTH"] != DBNull.Value ? Convert.ToString(row["PO_MONTH"]) : string.Empty;
                            int prCount = row["PO_COUNT"] != DBNull.Value ? Convert.ToInt32(row["PO_COUNT"]) : 0;

                            keyValue.Key1 = prYear + "-" + prMonth;
                            keyValue.Value1 = prCount;

                            details.Add(keyValue);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error in retrieving Data");
            }

            return details;
        }

        //public List<POReport> GetOpenPOData(int compid, string yearmonth, int page, int pagesize, string sessionid)
        //{
        //    List<POReport> details = new List<POReport>();
        //    try
        //    {
        //        Utilities.ValidateSession(sessionid);
        //        string month = "";
        //        string year = "";

        //        if (!string.IsNullOrEmpty(yearmonth))
        //        {
        //            year = yearmonth.Split('-')[0];
        //            month = yearmonth.Split('-')[1];
        //        }


        //        SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
        //        sd.Add("P_COMP_ID", compid);
        //        sd.Add("P_YEAR", year);
        //        sd.Add("P_MONTH", month);
        //        sd.Add("P_PAGE", page);
        //        sd.Add("P_PAGE_SIZE", pagesize);

        //        DataSet dataset = sqlHelper.SelectList("rp_GetOpenPOData", sd);

        //        if (dataset != null && dataset.Tables.Count > 0)
        //        {
        //            if (dataset.Tables[0].Rows.Count > 0 && dataset.Tables[0].Rows.Count > 0)
        //            {
        //                CORE.DataNamesMapper<POReport> mapper = new CORE.DataNamesMapper<POReport>();
        //                details = mapper.Map(dataset.Tables[0]).ToList();
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        logger.Error(ex, "Error in retrieving Data");
        //    }

        //    return details;
        //}

        public PRMAnalyticsTotals GetPRMGRNAnalyticsTotals(int compid, DateTime fromdate, DateTime todate, string sessionid)
        {
            PRMAnalyticsTotals detail = new PRMAnalyticsTotals();

            try
            {
                Utilities.ValidateSession(sessionid);
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                sd.Add("P_COMP_ID", compid);
                sd.Add("P_FROM_DATE", fromdate);
                sd.Add("P_TO_DATE", todate);
                var dataset = sqlHelper.SelectList("cp_GetGRNDashboard", sd);
                CORE.DataNamesMapper<PRMAnalyticsTotals> mapper = new CORE.DataNamesMapper<PRMAnalyticsTotals>();
                detail = mapper.Map(dataset.Tables[0]).ToList().FirstOrDefault();               
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error in retrieving Data");
            }

            return detail;

        }

        public List<GRNDetails> GetAnylaticsReportsData(int compid, DateTime fromdate, DateTime todate, string sessionid)
        {
            List<GRNDetails> details = new List<GRNDetails>();

            try
            {
                Utilities.ValidateSession(sessionid);
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                sd.Add("P_COMP_ID", compid);
                sd.Add("P_FROM_DATE", fromdate);
                sd.Add("P_TO_DATE", todate);
                var dataset = sqlHelper.SelectList("rp_GetGRNDashboardData", sd);
                CORE.DataNamesMapper<GRNDetails> mapper = new CORE.DataNamesMapper<GRNDetails>();
                details = mapper.Map(dataset.Tables[0]).ToList();
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error in retrieving Data");
            }

            return details;

        }

        #endregion Services
    }

}