prmApp
    // =========================================================================
    // COMMON FORMS
    // =========================================================================

    .controller('saveFwdReqCtrl', ["$state", "$stateParams", "$scope", "auctionsService", "userService",
        "$http", "$window", "domain", "fileReader", "growlService", "$log", "$filter", "ngDialog",
        "techevalService", "fwdauctionsService", "catalogService",
        "PRMFwdReqServiceService",
        function ($state, $stateParams, $scope, auctionsService, userService,
            $http, $window, domain, fileReader, growlService, $log, $filter, ngDialog,
            techevalService, fwdauctionsService, catalogService,
            PRMFwdReqServiceService) {
            $scope.showCatalogQuotationTemplate = true;
            if ($stateParams.Id) {
                $scope.stateParamsReqID = $stateParams.Id;
                $scope.postRequestLoding = true;
                $scope.showCatalogQuotationTemplate = false;

            } else {
                $scope.stateParamsReqID = 0;
                $scope.postRequestLoding = false;
                $scope.showCatalogQuotationTemplate = true;
            };
            $scope.itemPreviousPrice = {};
            $scope.itemPreviousPrice.lastPrice = -1;
            $scope.itemLastPrice = {};
            $scope.itemLastPrice.lastPrice = -1;
            $scope.bestPriceEnable = 0;
            $scope.bestLastPriceEnable = 0;
            $scope.companyItemUnits = [];
          //  $scope.isCloned = false;
            
            $scope.SelectedVendorsCount = 0;

            $scope.minDateMoment = moment();
            $scope.isTechEval = false;
            $scope.isForwardBidding = false;
            $scope.allCompanyVendors = [];
            $scope.selectedProducts = [];

            var curDate = new Date();
            var today = moment();
            var tomorrow = today.add('days', 1);
            $scope.subcategories = [];
            $scope.sub = {
                selectedSubcategories: [],
            }
            $scope.selectedCurrency = {};
            $scope.currencies = [];
            $scope.questionnaireList = [];
            
            $scope.selectedSubcategories = [];



            $scope.selectVendorShow = true;
            $scope.isEdit = false;
            //Input Slider
            this.nouisliderValue = 4;
            this.nouisliderFrom = 25;
            this.nouisliderTo = 80;
            this.nouisliderRed = 35;
            this.nouisliderBlue = 90;
            this.nouisliderCyan = 20;
            this.nouisliderAmber = 60;
            this.nouisliderGreen = 75;

            //Color Picker
            this.color = '#03A9F4';
            this.color2 = '#8BC34A';
            this.color3 = '#F44336';
            this.color4 = '#FFC107';

            $scope.Vendors = [];
            $scope.VendorsTemp = [];
            $scope.categories = [];
            $scope.selectedA = [];
            $scope.selectedB = [];
            $scope.showCategoryDropdown = false;
            $scope.checkVendorPhoneUniqueResult = false;
            $scope.checkVendorEmailUniqueResult = false;
            $scope.checkVendorPanUniqueResult = false;
            $scope.checkVendorTinUniqueResult = false;
            $scope.checkVendorStnUniqueResult = false;
            $scope.showFreeCreditsMsg = false;
            $scope.showNoFreeCreditsMsg = false;
            $scope.formRequest = {
                isTabular: true,
                auctionVendors: [],
                listFwdRequirementItems: [],
                isQuotationPriceLimit: false,
                quotationPriceLimit: 0,
                quotationFreezTime: '',
                deleteQuotations: false,
                expStartTimeName: '',
                isDiscountQuotation: 0,
                isRevUnitDiscountEnable: 0,
                multipleAttachments: [],
                listSlotDetails: [],
                contractStartTime: '',
                contractEndTime: '',
                isContract: false,
                isRFQ: 1
            };
            $scope.Vendors.city = "";
            $scope.Vendors.quotationUrl = "";
            $scope.vendorsLoaded = false;
            $scope.requirementAttachment = [];
            $scope.selectedProjectId = 0;
            $scope.selectedProject = {};
            $scope.branchProjects = [
                { PROJECT_ID: "RAW MATERIALS", PROJECT_CODE: "RAW MATERIALS"},
                { PROJECT_ID: "CAPEX", PROJECT_CODE: "CAPEX" },
                { PROJECT_ID: "SOLAR", PROJECT_CODE: "SOLAR" }
            ];

            $scope.changeProject = function () {
                console.log($scope.selectedProjectId);
                var filterdProjects = _.filter($scope.branchProjects, function (o) {
                    return o.PROJECT_ID == $scope.selectedProjectId;
                });
                if (filterdProjects && filterdProjects.length > 0) {
                    $scope.selectedProject = filterdProjects[0];
                }
            }

            $scope.sessionid = userService.getUserToken();

            $scope.selectedQuestionnaire == {}

            $scope.formRequest.indentID = 0;

            $scope.cijList = [];

            $scope.indentList = [];
            


            /*region start WORKFLOW*/
            $scope.workflowList = [];
            $scope.itemWorkflow = [];
            $scope.workflowObj = {};
            $scope.formRequest.workflowID = 0;
            $scope.currentStep = 0;
            $scope.orderInfo = 0;
            $scope.assignToShow = '';
            $scope.isWorkflowCompleted = false;
            $scope.WorkflowModule = 'QUOTATION';
            $scope.disableWFSelection = false;
            /*region end WORKFLOW*/


            
            $scope.compID = userService.getUserCompanyId();
            
            auctionsService.GetCompanyConfiguration($scope.compID, "ITEM_UNITS", userService.getUserToken())
                .then(function (unitResponse) {
                    $scope.companyItemUnits = unitResponse;
                });

            $scope.getPreviousItemPrice = function (itemDetails, index) {
                $scope.itemPreviousPrice = {};
                $scope.itemPreviousPrice.lastPrice = -1;
                $scope.bestPriceEnable = index;
                $log.info(itemDetails);
                itemDetails.sessionID = userService.getUserToken();
                itemDetails.compID = userService.getUserCompanyId();
                auctionsService.getPreviousItemPrice(itemDetails)
                .then(function (response) {
                    if (response && response.errorMessage == '') {
                        $scope.itemPreviousPrice.lastPrice = Number(response.initialPrice);
                        $scope.itemPreviousPrice.lastPriceDate = userService.toLocalDate(response.currentTime);
                        $scope.itemPreviousPrice.lastPriceVendor = response.companyName;
                        $log.info($scope.itemPreviousPrice);
                    }
                });
            };

            $scope.showTable = true;

            $scope.dispalyLastprices = function (val) {
                $scope.showTable = val;
            }

            $scope.GetLastPrice = function (itemDetails, index) {
                $scope.itemLastPrice = {};
                $scope.itemLastPrice.lastPrice = -1;
                $scope.bestLastPriceEnable = index;
                $log.info(itemDetails);
                itemDetails.sessionID = userService.getUserToken();
                itemDetails.compID = userService.getUserCompanyId();
                auctionsService.GetLastPrice(itemDetails)
                    .then(function (response) {
                        $scope.itemLastPrice = response;

                        $scope.itemLastPrice.forEach(function (item, index) {

                            item.currentTime = userService.toLocalDate(item.currentTime);
                        })
                    });
            };




            $scope.budgetValidate = function () {
                if ($scope.formRequest.budget != "" && (isNaN($scope.formRequest.budget) || $scope.formRequest.budget.indexOf('.') > -1)) {
                    $scope.postRequestLoding = false;
                    swal({
                        title: "Error!",
                        text: "Please enter valid budget, budget should be greater than 1,00,000.",
                        type: "error",
                        showCancelButton: false,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Ok",
                        closeOnConfirm: true
                    },
                        function () {

                        });

                    $scope.formRequest.budget = "";
                }
            };

            $scope.clickToOpen = function () {
                ngDialog.open({ template: 'login/termsAddNewReq.html', width: 1000, height: 500 });
            };

            $scope.changeCategory = function () {

                $scope.selectedSubCategoriesList = [];
                $scope.formRequest.auctionVendors = [];
                $scope.loadSubCategories();
                $scope.getvendors();
            }

            $scope.getCreditCount = function () {
                userService.getProfileDetails({ "userid": userService.getUserId(), "sessionid": userService.getUserToken() })
                    .then(function (response) {
                        $scope.userDetails = response;
                        $scope.selectedCurrency = $filter('filter')($scope.currencies, { value: response.currency });
                        $scope.selectedCurrency = $scope.selectedCurrency[0];
                        if (response.creditsLeft) {
                            $scope.showFreeCreditsMsg = true;
                        } else {
                            $scope.showNoFreeCreditsMsg = true;
                        }
                    });
            }



            $scope.getvendors = function () {
                $scope.vendorsLoaded = false;
                var category = [];

                category.push($scope.formRequest.category);
                //$scope.formRequest.category = category;
                if ($scope.formRequest.category != undefined) {
                    var params = { 'Categories': category, 'sessionID': userService.getUserToken(), 'uID': userService.getUserId(), evalID: $scope.isTechEval ? $scope.selectedQuestionnaire.evalID : 0 };
                    $http({
                        method: 'POST',
                        url: domain + 'getvendors',
                        encodeURI: true,
                        headers: { 'Content-Type': 'application/json' },
                        data: params
                    }).then(function (response) {
                        if (response && response.data) {
                            if (response.data.length > 0) {
                                $scope.Vendors = response.data;
                                // if (allVendors === 1) {
                                // $scope.allCompanyVendors = response.data;
                                // allVendors = 0;
                                // }  
                                $scope.vendorsLoaded = true;
                                for (var j in $scope.formRequest.auctionVendors) {
                                    for (var i in $scope.Vendors) {
                                        if ($scope.Vendors[i].vendorName == $scope.formRequest.auctionVendors[j].vendorName) {
                                            $scope.Vendors.splice(i, 1);
                                        }
                                    }
                                }

                                $scope.VendorsTemp = $scope.Vendors;
                                $scope.searchVendors('');

                            }
                            //$scope.formRequest.auctionVendors =[];
                        } else {
                        }
                    }, function (result) {
                    });
                }

            };

            $scope.getReqQuestionnaire = function () {


            }

            $scope.getQuestionnaireList = function () {
                techevalService.getquestionnairelist(0)
                    .then(function (response) {
                        $scope.questionnaireList = $filter('filter')(response, { reqID: 0 });
                        if ($stateParams.Id && $stateParams.Id > 0) {
                            techevalService.getreqquestionnaire($stateParams.Id, 1)
                                .then(function (response) {
                                    $scope.selectedQuestionnaire = response;

                                    if ($scope.selectedQuestionnaire && $scope.selectedQuestionnaire.evalID > 0) {
                                        $scope.isTechEval = true;
                                    }

                                    $scope.questionnaireList.push($scope.selectedQuestionnaire);
                                })
                        }
                    })
            };
           
            $scope.isRequirementPosted = 0;
            $scope.getCategories = function () {
                $http({
                    method: 'GET',
                    url: domain + 'getcategories?userid=' + userService.getUserId() + '&sessionid=' + userService.getUserToken(),
                    encodeURI: true,
                    headers: { 'Content-Type': 'application/json' }
                }).then(function (response) {
                    if (response && response.data) {

                        if (response.data.length > 0) {
                            $scope.categories = _.uniq(_.map(response.data, 'category'));
                            $scope.categoriesdata = response.data;
                            $scope.showCategoryDropdown = true;
                        }
                    } else {
                    }
                }, function (result) {
                });
                
            };


            $scope.getCurrencies = function () {
                $http({
                    method: 'GET',
                    url: domain + 'getkeyvaluepairs?parameter=CURRENCY',
                    encodeURI: true,
                    headers: { 'Content-Type': 'application/json' }
                }).then(function (response) {
                    if (response && response.data) {
                        if (response.data.length > 0) {
                            $scope.currencies = response.data;
                            $scope.getCreditCount();
                        }
                    } else {
                    }
                }, function (result) {
                });
            };

            


            $scope.getData = function () {
                if ($stateParams.Id) {
                    var id = $stateParams.Id;
                    $scope.isEdit = true;

                    PRMFwdReqServiceService.getrequirementdata({ "reqid": $stateParams.Id, "sessionid": userService.getUserToken(), "userid": userService.getUserId() })
                        .then(function (response) {
                            
                            $scope.selectedProjectId = response.projectId;
                            var category = response.category[0];
                            response.category = category;
                            response.taxes = parseInt(response.taxes);
                            $scope.formRequest = response;
                       //     $scope.getPRNumber($scope.formRequest.PR_ID);
                            $scope.itemSNo = $scope.formRequest.itemSNoCount;

                            $scope.formRequest.checkBoxEmail = true;
                            $scope.formRequest.checkBoxSms = true;
                            $scope.loadSubCategories();

                            $scope.showInspection($scope.formRequest.showInspectionValue);

                            if ($scope.formRequest.showInspectionValue && $scope.formRequest.listSlotDetails.length > 0)
                            {
                                $scope.formRequest.listSlotDetails.forEach(function (item, index) {
                                    if (item.slot_entity_type == 'REQ')
                                    {
                                        item.slot_start_time = userService.toLocalDate(item.slot_start_time);
                                        item.slot_end_time = userService.toLocalDate(item.slot_end_time);
                                    }
                                })
                            }

                            $scope.isRequirementPosted = $scope.formRequest.auctionVendors.length;



                            if (!$scope.formRequest.multipleAttachments) {
                                $scope.formRequest.multipleAttachments = [];
                            }
                            $scope.formRequest.attFile = response.attachmentName;
                            if ($scope.formRequest.attFile != '' && $scope.formRequest.attFile != null && $scope.formRequest.attFile != undefined) {


                                var attchArray = $scope.formRequest.attFile.split(',');

                                attchArray.forEach(function (att, index) {

                                    var fileUpload = {
                                        fileStream: [],
                                        fileName: '',
                                        fileID: att
                                    };

                                    $scope.formRequest.multipleAttachments.push(fileUpload);
                                })

                            }


                            $scope.selectedSubcategories = response.subcategories.split(",");
                            for (i = 0; i < $scope.selectedSubcategories.length; i++) {
                                for (j = 0; j < $scope.subcategories.length; j++) {
                                    if ($scope.selectedSubcategories[i] == $scope.subcategories[j].subcategory) {
                                        $scope.subcategories[j].ticked = true;
                                    }
                                }
                            }
                            //$scope.getvendors();
                            $scope.selectSubcat();
                            $scope.formRequest.attFile = response.attachmentName;
                            $scope.formRequest.quotationFreezTime = userService.toLocalDate($scope.formRequest.quotationFreezTime);
                            //$scope.formRequest.quotationFreezTimeNew = userService.toLocalDate($scope.formRequest.quotationFreezTime);
                            //$scope.formRequest.urgency.push(urgency);

                            $scope.formRequest.expStartTime = userService.toLocalDate($scope.formRequest.expStartTime);

                            $scope.SelectedVendors = $scope.formRequest.auctionVendors;

                            $scope.SelectedVendorsCount = $scope.formRequest.auctionVendors.length;

                            $scope.formRequest.listFwdRequirementItems.forEach(function (item, itemIndex){
                                if (item.productQuotationTemplateJson && item.productQuotationTemplateJson != '' && item.productQuotationTemplateJson != null && item.productQuotationTemplateJson != undefined) {
                                    item.productQuotationTemplateArray = JSON.parse(item.productQuotationTemplateJson);
                                }
                                else {
                                    item.productQuotationTemplateArray = [];
                                }
                                //$scope.GetProductQuotationTemplate(item.catalogueItemID, itemIndex);
                            })


                            $scope.postRequestLoding = false;
                        });
                }

            };

            $scope.showSimilarNegotiationsButton = function (value, searchstring) {
                $scope.showSimilarNegotiations = value;
                if (!value) {
                    $scope.CompanyLeads = {};
                }
                if (value) {
                    if (searchstring.length < 3) {
                        $scope.CompanyLeads = {};
                    }
                    if (searchstring.length > 2) {
                        $scope.searchstring = searchstring;
                        $scope.GetCompanyLeads(searchstring);
                    }
                }
                return $scope.showSimilarNegotiations;
            }

            $scope.showSimilarNegotiations = false;

            $scope.CompanyLeads = {};

            $scope.searchstring = '';

            $scope.GetCompanyLeads = function (searchstring) {
                if (searchstring.length < 3) {
                    $scope.CompanyLeads = {};
                }

                if ($scope.showSimilarNegotiations && searchstring.length > 2) {
                    $scope.searchstring = searchstring;
                    var params = { "userid": userService.getUserId(), "searchstring": $scope.searchstring, "searchtype": 'Title', "sessionid": userService.getUserToken() };
                    PRMFwdReqServiceService.GetCompanyLeads(params)
                        .then(function (response) {
                            $scope.CompanyLeads = response;
                            $scope.CompanyLeads.forEach(function (item, index) {
                                item.postedOn = userService.toLocalDate(item.postedOn);
                            })
                        });
                }
            }


            $scope.changeScheduledAuctionsLimit = function () {
                $scope.scheduledLimit = 8;
                $scope.getMiniItems();
            }

            $scope.loadSubCategories = function () {
                $scope.subcategories = _.filter($scope.categoriesdata, { category: $scope.formRequest.category });
            }

            $scope.selectedSubCategoriesList = [];
            $scope.selectSubcat = function (subcat) {
                $scope.selectedSubCategoriesList = [];
                if (!$scope.isEdit) {
                    $scope.formRequest.auctionVendors = [];
                }

                $scope.vendorsLoaded = false;
                var category = [];
                var count = 0;
                var succategory = "";
                $scope.sub.selectedSubcategories = $filter('filter')($scope.subcategories, { ticked: true });
                selectedcount = $scope.sub.selectedSubcategories.length;
                if (selectedcount > 0) {
                    succategory = _.map($scope.sub.selectedSubcategories, 'id');
                    category.push(succategory);

                    $scope.selectedSubCategoriesList = succategory;

                    if ($scope.formRequest.category != undefined) {
                        var params = { 'Categories': succategory, 'sessionID': userService.getUserToken(), 'count': selectedcount, 'uID': userService.getUserId(), evalID: $scope.selectedQuestionnaire ? $scope.selectedQuestionnaire.evalID : 0 };
                        $http({
                            method: 'POST',
                            url: domain + 'getvendorsbycatnsubcat',
                            encodeURI: true,
                            headers: { 'Content-Type': 'application/json' },
                            data: params
                        }).then(function (response) {
                            if (response && response.data) {
                                if (response.data.length > 0) {
                                    $scope.Vendors = response.data;
                                    $scope.vendorsLoaded = true;
                                    for (var j in $scope.formRequest.auctionVendors) {
                                        for (var i in $scope.Vendors) {
                                            if ($scope.Vendors[i].vendorName == $scope.formRequest.auctionVendors[j].vendorName) {
                                                $scope.Vendors.splice(i, 1);
                                            }
                                        }
                                    }

                                    $scope.VendorsTemp = $scope.Vendors;
                                    $scope.searchVendors('');

                                }
                                //$scope.formRequest.auctionVendors =[];
                            } else {
                            }
                        }, function (result) {
                        });
                    }
                } else {
                    $scope.getvendors();
                }

            }

            $scope.getData();

            $scope.checkVendorUniqueResult = function (idtype, inputvalue) {


                if (idtype == "PHONE") {
                    $scope.checkVendorPhoneUniqueResult = false;
                } else if (idtype == "EMAIL") {
                    $scope.checkVendorEmailUniqueResult = false;
                }
                else if (idtype == "PAN") {
                    $scope.checkVendorPanUniqueResult = false;
                }
                else if (idtype == "TIN") {
                    $scope.checkVendorTinUniqueResult = false;
                }
                else if (idtype == "STN") {
                    $scope.checkVendorStnUniqueResult = false;
                }

                if (inputvalue == "" || inputvalue == undefined) {
                    return false;
                }

                userService.checkUserUniqueResult(inputvalue, idtype).then(function (response) {
                    if (idtype == "PHONE") {
                        $scope.checkVendorPhoneUniqueResult = !response;
                    } else if (idtype == "EMAIL") {
                        $scope.checkVendorEmailUniqueResult = !response;
                    }
                    else if (idtype == "PAN") {
                        $scope.checkVendorPanUniqueResult = !response;
                    }
                    else if (idtype == "TIN") {
                        $scope.checkVendorTinUniqueResult = !response;
                    }
                    else if (idtype == "STN") {
                        $scope.checkVendorStnUniqueResult = !response;
                    }
                });
            };
            $scope.selectForA = function (item) {
                var index = $scope.selectedA.indexOf(item);
                if (index > -1) {
                    $scope.selectedA.splice(index, 1);
                } else {
                    $scope.selectedA.splice($scope.selectedA.length, 0, item);
                }
                for (i = 0; i < $scope.selectedA.length; i++) {
                    $scope.formRequest.auctionVendors.push($scope.selectedA[i]);
                    $scope.Vendors.splice($scope.Vendors.indexOf($scope.selectedA[i]), 1);
                    $scope.VendorsTemp.splice($scope.VendorsTemp.indexOf($scope.selectedA[i]), 1);
                }
                $scope.reset();
            }

            $scope.selectForB = function (item) {
                var index = $scope.selectedB.indexOf(item);
                if (index > -1) {
                    $scope.selectedB.splice(index, 1);
                } else {
                    $scope.selectedB.splice($scope.selectedA.length, 0, item);
                }
                for (i = 0; i < $scope.selectedB.length; i++) {
                    $scope.Vendors.push($scope.selectedB[i]);
                    $scope.VendorsTemp.push($scope.selectedB[i]);
                    $scope.formRequest.auctionVendors.splice($scope.formRequest.auctionVendors.indexOf($scope.selectedB[i]), 1);
                }
                $scope.reset();
            }

            $scope.AtoB = function () {

            }

            $scope.BtoA = function () {

            }

            $scope.reset = function () {
                $scope.selectedA = [];
                $scope.selectedB = [];
            }

            // $scope.getFile = function () {
            //     $scope.progress = 0;
            //     fileReader.readAsDataUrl($scope.file, $scope)
            //     .then(function(result) {
            //         $scope.formRequest.attachment = result;
            //     });
            // };

            $scope.multipleAttachments = [];

            $scope.getFile = function () {
                $scope.progress = 0;

                //$scope.file = $("#attachement")[0].files[0];
                $scope.multipleAttachments = $("#attachement")[0].files;

                $scope.multipleAttachments = Object.values($scope.multipleAttachments)


                $scope.multipleAttachments.forEach(function (item, index) {

                    fileReader.readAsDataUrl(item, $scope)
                        .then(function (result) {

                            var fileUpload = {
                                fileStream: [],
                                fileName: '',
                                fileID: 0
                            };

                            var bytearray = new Uint8Array(result);

                            fileUpload.fileStream = $.makeArray(bytearray);
                            fileUpload.fileName = item.name;

                            if (!$scope.formRequest.multipleAttachments) {
                                $scope.formRequest.multipleAttachments = [];
                            }

                            $scope.formRequest.multipleAttachments.push(fileUpload);

                        });

                })

            };


            $scope.newVendor = {};
            $scope.Attaachmentparams = {};
            $scope.deleteAttachment = function (reqid) {
                $scope.Attaachmentparams = {
                    reqID: reqid,
                    userID: userService.getUserId()
                }
                auctionsService.deleteAttachment($scope.Attaachmentparams)
                    .then(function (response) {
                        if (response.errorMessage != "") {
                            growlService.growl(response.errorMessage, "inverse");
                        } else {
                            growlService.growl("Attachment deleted Successfully", "inverse");
                            $scope.getData();
                        }
                    });
            }

            $scope.newVendor.panno = "";
            $scope.newVendor.vatNum = "";
            $scope.newVendor.serviceTaxNo = "";

            $scope.addVendor = function () {
                $scope.emailRegx = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                $scope.mobileRegx = /^\+?([0-9]{2})\)?[-. ]?([0-9]{4})[-. ]?([0-9]{4})$/;
                $scope.panregx = /^([a-zA-Z]{5})(\d{4})([a-zA-Z]{1})$/;
                //$scope.emailRegx = /^[a-z]+[a-z0-9._]+@[a-z]+\.[a-z.]{2,5}$/;
                var addVendorValidationStatus = false;
                $scope.firstvalidation = $scope.companyvalidation = $scope.lastvalidation = $scope.contactvalidation = $scope.emailvalidation = $scope.categoryvalidation = $scope.emailregxvalidation = $scope.contactvalidationlength = $scope.panregxvalidation = $scope.tinvalidation = $scope.stnvalidation = $scope.knownsincevalidation = $scope.vendorcurrencyvalidation = false;
                if ($scope.newVendor.companyName == "" || $scope.newVendor.companyName === undefined) {
                    $scope.companyvalidation = true;
                    addVendorValidationStatus = true;
                }
                if ($scope.newVendor.firstName == "" || $scope.newVendor.firstName === undefined) {
                    $scope.firstvalidation = true;
                    addVendorValidationStatus = true;
                }
                if ($scope.newVendor.lastName == "" || $scope.newVendor.lastName === undefined) {
                    $scope.lastvalidation = true;
                    addVendorValidationStatus = true;
                }
                if ($scope.newVendor.contactNum == "" || $scope.newVendor.contactNum === undefined || isNaN($scope.newVendor.contactNum)) {
                    $scope.contactvalidation = true;
                    addVendorValidationStatus = true;
                }
                else if ($scope.newVendor.contactNum.length != 10) {
                    $scope.contactvalidationlength = true;
                    addVendorValidationStatus = true;
                }
                if ($scope.newVendor.email == "" || $scope.newVendor.email === undefined) {
                    $scope.emailvalidation = true;
                    addVendorValidationStatus = true;
                }
                else if (!$scope.emailRegx.test($scope.newVendor.email)) {
                    $scope.emailregxvalidation = true;
                    addVendorValidationStatus = true;
                }
                if ($scope.newVendor.vendorcurrency == "" || $scope.newVendor.vendorcurrency === undefined) {
                    $scope.vendorcurrencyvalidation = true;
                    addVendorValidationStatus = true;
                }
                if ($scope.newVendor.panno != "" && $scope.newVendor.panno != undefined && !$scope.panregx.test($scope.newVendor.panno)) {
                    $scope.panregxvalidation = true;
                    addVendorValidationStatus = true;
                }
                if ($scope.newVendor.vatNum != "" && $scope.newVendor.vatNum != undefined && $scope.newVendor.vatNum.length != 11) {
                    $scope.tinvalidation = true;
                    addVendorValidationStatus = true;
                }

                if ($scope.newVendor.serviceTaxNo != "" && $scope.newVendor.serviceTaxNo != undefined && $scope.newVendor.serviceTaxNo.length != 15) {
                    $scope.stnvalidation = true;
                    addVendorValidationStatus = true;
                }
                if ($scope.formRequest.category == "" || $scope.formRequest.category === undefined) {
                    $scope.categoryvalidation = true;
                    addVendorValidationStatus = true;
                }
                if ($scope.checkVendorEmailUniqueResult || $scope.checkVendorEmailUniqueResult) {
                    addVendorValidationStatus = true;
                }
                if (addVendorValidationStatus) {
                    return false;
                }
                var vendCAtegories = [];
                $scope.newVendor.category = $scope.formRequest.category;
                vendCAtegories.push($scope.newVendor.category);
                var params = {
                    "register": {
                        "firstName": $scope.newVendor.firstName,
                        "lastName": $scope.newVendor.lastName,
                        "email": $scope.newVendor.email,
                        "phoneNum": $scope.newVendor.contactNum,
                        "username": $scope.newVendor.contactNum,
                        "password": $scope.newVendor.contactNum,
                        "companyName": $scope.newVendor.companyName ? $scope.newVendor.companyName : "",
                        "isOTPVerified": 0,
                        "category": $scope.newVendor.category,
                        "userType": "VENDOR",
                        "panNumber": ("panno" in $scope.newVendor) ? $scope.newVendor.panno : "",
                        "stnNumber": ("serviceTaxNo" in $scope.newVendor) ? $scope.newVendor.serviceTaxNo : "",
                        "vatNumber": ("vatNum" in $scope.newVendor) ? $scope.newVendor.vatNum : "",
                        "referringUserID": userService.getUserId(),
                        "knownSince": ("knownSince" in $scope.newVendor) ? $scope.newVendor.knownSince : "",
                        "errorMessage": "",
                        "sessionID": "",
                        "userID": 0,
                        "department": "",
                        "currency": $scope.newVendor.vendorcurrency.key,
                        "altPhoneNum": $scope.newVendor.altPhoneNum,
                        "altEmail": $scope.newVendor.altEmail,
                        "subcategories": $scope.selectedSubCategoriesList
                    }
                };
                $http({
                    method: 'POST',
                    url: domain + 'register',
                    encodeURI: true,
                    headers: { 'Content-Type': 'application/json' },
                    data: params
                }).then(function (response) {
                    if ((response && response.data && response.data.errorMessage == "") || response.data.errorMessage == 'User assigned to Company.') {
                        $scope.formRequest.auctionVendors.push({ vendorName: $scope.newVendor.firstName + " " + $scope.newVendor.lastName, companyName: $scope.newVendor.companyName, vendorID: response.data.objectID });
                        $scope.newVendor = null;
                        $scope.newVendor = {};
                        //$scope.addVendorForm.$setPristine();
                        $scope.addVendorShow = false;
                        $scope.selectVendorShow = true;
                        $scope.newVendor = {};
                        $scope.firstvalidation = $scope.companyvalidation = $scope.lastvalidation = $scope.contactvalidation = $scope.emailvalidation = $scope.categoryvalidation = $scope.emailregxvalidation = $scope.contactvalidationlength = $scope.panregxvalidation = $scope.tinvalidation = $scope.stnvalidation = $scope.knownsincevalidation = $scope.vendorcurrencyvalidation = false;
                        growlService.growl("Vendor Added Successfully.", 'inverse');
                    } else if (response && response.data && response.data.errorMessage) {
                        growlService.growl(response.data.errorMessage, 'inverse');
                    } else {
                        growlService.growl('Unexpected Error Occurred', 'inverse');
                    }
                });
            }

            //$scope.checkboxModel = {
            //    value1: true
            //};

            $scope.formRequest.checkBoxEmail = true;
            $scope.formRequest.checkBoxSms = true;
            //$scope.postRequestLoding = false;
            $scope.formRequest.urgency = '';
            $scope.formRequest.deliveryLocation = '';
            $scope.formRequest.paymentTerms = '';
            $scope.formRequest.isSubmit = 0;


            $scope.titleValidation = $scope.attachmentNameValidation = false;
            $scope.deliveryLocationValidation = $scope.paymentTermsValidation = $scope.deliveryTimeValidation = false;
            $scope.urgencyValidation = $scope.quotationFreezTimeValidation = $scope.selectedCurrencyValidation = $scope.quotationPriceLimitValidation = $scope.noOfQuotationRemindersValidation = $scope.questionnaireValidation = false;

            $scope.postRequest = function (isSubmit, pageNo, navigateToView, stage) {

                $scope.titleValidation = $scope.attachmentNameValidation = false;
                $scope.deliveryLocationValidation = $scope.paymentTermsValidation = $scope.deliveryTimeValidation = false;
                $scope.urgencyValidation = $scope.quotationFreezTimeValidation = $scope.selectedCurrencyValidation = $scope.quotationPriceLimitValidation = $scope.noOfQuotationRemindersValidation = $scope.questionnaireValidation = false;

                $scope.postRequestLoding = true;
                $scope.formRequest.isSubmit = isSubmit;

                if (isSubmit == 1 || pageNo == 1) {
                    if ($scope.formRequest.title == null || $scope.formRequest.title == '') {
                        $scope.titleValidation = true;
                        $scope.postRequestLoding = false;
                        return false;
                    }
                    if (isSubmit == 1) {
                        if ($scope.formRequest.isTabular) {
                            $scope.formRequest.listFwdRequirementItems.forEach(function (item, index) {
                                if (!item.attachmentName || item.attachmentName == '') {
                                    $scope.attachmentNameValidation = true;
                                    return false;
                                }
                            })

                        }
                        if (!$scope.formRequest.isTabular) {
                            if ($scope.formRequest.description == null || $scope.formRequest.description == '') {
                                $scope.descriptionValidation = true;
                                $scope.postRequestLoding = false;
                                return false;
                            }
                        }
                    }
                }

                if (isSubmit == 1) {
                    if ($scope.formRequest.deliveryTime == null || $scope.formRequest.deliveryTime == '') {
                        $scope.deliveryTimeValidation = true;
                        $scope.postRequestLoding = false;
                        return false;
                    }
                    if ($scope.formRequest.deliveryLocation == null || $scope.formRequest.deliveryLocation == '') {
                        $scope.deliveryLocationValidation = true;
                        $scope.postRequestLoding = false;
                        return false;
                    }
                    if ($scope.formRequest.paymentTerms == null || $scope.formRequest.paymentTerms == '') {
                        $scope.paymentTermsValidation = true;
                        $scope.postRequestLoding = false;
                        return false;
                    }
                    
                }

                if (isSubmit == 1) {
                    if ($scope.formRequest.urgency == null || $scope.formRequest.urgency == '') {
                        $scope.urgencyValidation = true;
                        $scope.postRequestLoding = false;
                        return false;
                    }
                    
                    if (($scope.formRequest.quotationPriceLimit == null || $scope.formRequest.quotationPriceLimit == '' || $scope.formRequest.quotationPriceLimit <= '') && $scope.formRequest.isQuotationPriceLimit == true) {
                        $scope.quotationPriceLimitValidation = true;
                        $scope.postRequestLoding = false;
                        return false;
                    }
                    if ($scope.formRequest.isQuotationPriceLimit == false) {
                        $scope.formRequest.quotationPriceLimit = 0;
                    }
                    if ($scope.formRequest.noOfQuotationReminders == null || $scope.formRequest.noOfQuotationReminders == '' || $scope.formRequest.noOfQuotationReminders <= 0 || $scope.formRequest.noOfQuotationReminders > 5) {
                        $scope.noOfQuotationRemindersValidation = true;
                        $scope.postRequestLoding = false;
                        return false;
                    }
                    if ($scope.formRequest.remindersTimeInterval == null || $scope.formRequest.remindersTimeInterval == '' || $scope.formRequest.remindersTimeInterval <= 0) {
                        $scope.remindersTimeIntervalValidation = true;
                        $scope.postRequestLoding = false;
                        return false;
                    }
                    if ($scope.isTechEval == true && (!$scope.selectedQuestionnaire || $scope.selectedQuestionnaire.evalID <= 0)) {
                        $scope.questionnaireValidation = true;
                        $scope.postRequestLoding = false;
                        return false;
                    }
                    if ($scope.formRequest.quotationFreezTime == null || $scope.formRequest.quotationFreezTime == '') {
                        $scope.quotationFreezTimeValidation = true;
                        $scope.postRequestLoding = false;
                        return false;
                    }
                }

                if (!$scope.postRequestLoding) {
                    return false;
                }


                if (pageNo != 4) {
                    $scope.textMessage = "Save as Draft.";
                }

                if ($scope.formRequest.checkBoxEmail == true && $scope.formRequest.checkBoxSms == true && pageNo == 4) {
                    $scope.textMessage = "This will send an email and sms invite to all the vendors selected above.";
                }
                else if ($scope.formRequest.checkBoxEmail == true && pageNo == 4) {
                    $scope.textMessage = "This will send an email invite to all the vendors selected above.";
                }
                else if ($scope.formRequest.checkBoxSms == true && pageNo == 4) {
                    $scope.textMessage = "This will send an SMS invite to all the vendors selected above.";
                }
                else if (pageNo == 4) {
                    $scope.textMessage = "This will not send an SMS or EMAIL the vendors selected above.";
                }
                $scope.formRequest.currency = $scope.selectedCurrency.value;
                $scope.formRequest.timeZoneID = 190;
                $scope.postRequestLoding = false;
                swal({
                    title: "Are you sure?",
                    text: $scope.textMessage,
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#F44336",
                    confirmButtonText: "OK",
                    closeOnConfirm: true
                }, function () {
                    $scope.postRequestLoding = true;
                    var ts = userService.toUTCTicks($scope.formRequest.quotationFreezTime);
                    var m = moment(ts);
                    var quotationDate = new Date(m);
                    var milliseconds = parseInt(quotationDate.getTime() / 1000.0);
                    $scope.formRequest.quotationFreezTime = "/Date(" + milliseconds + "000++530)/";
                    // this post request
                    


                    var ts = userService.toUTCTicks($scope.formRequest.expStartTime);
                    var m = moment(ts);
                    var quotationDate = new Date(m);
                    var milliseconds = parseInt(quotationDate.getTime() / 1000.0);
                    $scope.formRequest.expStartTime = "/Date(" + milliseconds + "000+0530)/";

                    $scope.formRequest.requirementID = $stateParams.Id ? $stateParams.Id : -1;
                    $scope.formRequest.customerID = userService.getUserId();
                    $scope.formRequest.customerFirstname = userService.getFirstname();
                    $scope.formRequest.customerLastname = userService.getLastname();
                    $scope.formRequest.isClosed = "NOTSTARTED";
                    $scope.formRequest.endTime = "";
                    $scope.formRequest.sessionID = userService.getUserToken();
                    $scope.formRequest.subcategories = "";
                    $scope.formRequest.budget = 100000;
                    $scope.formRequest.custCompID = userService.getUserCompanyId();
                    $scope.formRequest.customerCompanyName = userService.getUserCompanyId();

                    for (i = 0; i < $scope.sub.selectedSubcategories.length; i++) {
                        $scope.formRequest.subcategories += $scope.sub.selectedSubcategories[i].subcategory + ",";
                    }
                    var category = [];
                    category.push($scope.formRequest.category);
                    $scope.formRequest.category = category;

                        $scope.formRequest.listFwdRequirementItems.forEach(function (item, itemIndexs) {
                        if (item.productQuotationTemplateArray && item.productQuotationTemplateArray.length > 0) {
                            item.productQuotationTemplateArray.forEach(function (subItem, subItemIndex) {
                                subItem.dateCreated = "/Date(1561000200000+0530)/";
                                subItem.dateModified = "/Date(1561000200000+0530)/";
                            })
                        }
                    })

                    if ($scope.formRequest.listSlotDetails.length > 0) {
                        $scope.formRequest.listSlotDetails.forEach(function (item, index) {
                            //item.slotStartTime

                            var ts = userService.toUTCTicks(item.slot_start_time);
                            var m = moment(ts);
                            var slotStartDate = new Date(m);
                            var milliseconds = parseInt(slotStartDate.getTime() / 1000.0);
                            item.slot_start_time = "/Date(" + milliseconds + "000+0530)/";


                            var ts = userService.toUTCTicks(item.slot_end_time);
                            var m = moment(ts);
                            var slotEndDate = new Date(m);
                            var milliseconds = parseInt(slotEndDate.getTime() / 1000.0);
                            item.slot_end_time = "/Date(" + milliseconds + "000+0530)/";

                        })

                        } else {

                        }

                    if (!$scope.formRequest.isForwardBidding) {
                        $scope.formRequest.cloneID = 0;
                        if ($stateParams.reqObj && $stateParams.reqObj.cloneId && $stateParams.reqObj.cloneId !== "") {
                            $scope.formRequest.cloneID = $stateParams.reqObj.cloneId;
                            $scope.formRequest.auctionVendors = _.filter($scope.formRequest.auctionVendors, function (x) { return x.companyName !== 'PRICE_CAP'; });
                        }
                        
                        PRMFwdReqServiceService.postrequirementdata($scope.formRequest)
                            .then(function (response) {
                                if (response.objectID != 0) {
                                    if ($scope.selectedProject) {
                                        $scope.selectedProject.sessionID = userService.getUserToken();
                                        $scope.selectedProject.requirement = {};
                                        $scope.selectedProject.requirement.requirementID = response.objectID;
                                        var param = { item: $scope.selectedProject };
                                    }

                                    $scope.SaveReqDepartments(response.objectID);
                                    if ($scope.selectedQuestionnaire != null && $scope.selectedQuestionnaire != undefined && $scope.selectedQuestionnaire.evalID != undefined) {
                                        techevalService.getquestionnaire($scope.selectedQuestionnaire.evalID, 1)
                                            .then(function (response1) {
                                                $scope.selectedQuestionnaire = response1;

                                                if ($scope.isTechEval) {
                                                    $scope.selectedQuestionnaire.reqID = response.objectID;
                                                }
                                                else {
                                                    $scope.selectedQuestionnaire.reqID = 0;
                                                }
                                                $scope.selectedQuestionnaire.sessionID = userService.getUserToken();
                                                var params = {
                                                    questionnaire: $scope.selectedQuestionnaire
                                                }
                                                techevalService.assignquestionnaire(params)
                                                    .then(function (response) {
                                                    })
                                            })
                                    }
                                    if (stage) {
                                        $state.go('save-fwd-req', { Id: response.objectID });
                                    }
                                    swal({
                                        title: "Done!",
                                        text: "Requirement Saved Successfully",
                                        type: "success",
                                        showCancelButton: false,
                                        confirmButtonColor: "#DD6B55",
                                        confirmButtonText: "Ok",
                                        closeOnConfirm: true
                                    },
                                        function () {
                                            if (navigateToView && stage == undefined) {
                                                $state.go('view-fwd-req', { 'Id': response.objectID });
                                            } else {
                                                if ($scope.stateParamsReqID > 0 && stage == undefined) {
                                                    location.reload();
                                                }
                                                else {
                                                    if (stage == undefined) {
                                                        $state.go('save-fwd-req', { Id: response.objectID });
                                                    }
                                                }
                                            }
                                        });
                                }
                            });
                    }
                });
                //$scope.postRequestLoding = false;
            };


            $scope.ItemFile = '';
            $scope.itemSNo = 1;
            $scope.ItemFileName = '';

            $scope.itemnumber = $scope.formRequest.listFwdRequirementItems.length;

            $scope.requirementItems =
                {
                    productSNo: $scope.itemSNo++,
                    ItemID: 0,
                    productIDorName: '',
                    productNo: '',
                    productDescription: '',
                    productQuantity: 0,
                    productBrand: '',
                    othersBrands: '',
                    isDeleted: 0,
                    itemAttachment: '',
                    attachmentName: '',
                    itemMinReduction: 0,
                    splitenabled: 0,
                    fromrange: 0,
                    torange: 0,
                    requiredQuantity: 0,
                    productCode: ''
                }

            $scope.formRequest.listFwdRequirementItems.push($scope.requirementItems);
            $scope.AddItem = function () {
                $scope.itemnumber = $scope.formRequest.listFwdRequirementItems.length;
                $scope.requirementItems =
                    {
                        productSNo: $scope.itemSNo++,
                        ItemID: 0,
                        productIDorName: '',
                        productNo: '',
                        productDescription: '',
                        productQuantity: 0,
                        productBrand: '',
                        othersBrands: '',
                        isDeleted: 0,
                        itemAttachment: '',
                        attachmentName: '',
                        itemMinReduction: 0,
                        splitenabled: 0,
                        fromrange: 0,
                        torange: 0,
                        requiredQuantity: 0,
                        productCode: ''
                    }
                $scope.formRequest.listFwdRequirementItems.push($scope.requirementItems);
            };

            $scope.deleteItem = function (SNo) {
                //$scope.formRequest.listFwdRequirementItems[SNo].isDeleted = 0;
                //$scope.formRequest.listFwdRequirementItems.splice(SNo, 1);
                if ($scope.formRequest.listFwdRequirementItems && $scope.formRequest.listFwdRequirementItems.length > 1) {
                    $scope.formRequest.listFwdRequirementItems = _.filter($scope.formRequest.listFwdRequirementItems, function (x) { return x.productSNo !== SNo; });
                };
            };



            $scope.getFile1 = function (id, itemid, ext) {
                $scope.file = $("#" + id)[0].files[0];

                fileReader.readAsDataUrl($scope.file, $scope)
                    .then(function (result) {
                        if (id == "itemsAttachment") {
                            if (ext != "xlsx") {
                                swal("Error!", "File type should be XSLX. Please download the template and  fill values accordingly.", "error");
                                return;
                            }
                            var bytearray = new Uint8Array(result);
                            $scope.formRequest.itemsAttachment = $.makeArray(bytearray);
                            if (!$scope.isEdit) {
                                $scope.formRequest.listFwdRequirementItems = [];
                            }

                            $scope.postRequest(0, 1, false);
                            //$scope.formRequest.itemsAttachmentName = $scope.file.name;
                        }

                        if (id != "itemsAttachment") {
                            var bytearray = new Uint8Array(result);
                            var arrayByte = $.makeArray(bytearray);
                            var ItemFileName = $scope.file.name;
                            var index = _.indexOf($scope.formRequest.listFwdRequirementItems, _.find($scope.formRequest.listFwdRequirementItems, function (o) { return o.productSNo == id; }));
                            var obj = $scope.formRequest.listFwdRequirementItems[index];
                            obj.itemAttachment = arrayByte;
                            obj.attachmentName = ItemFileName;
                            $scope.formRequest.listFwdRequirementItems.splice(index, 1, obj);
                        }
                    });
            }

            $scope.pageNo = 1;

            $scope.nextpage = function (pageNo) {
                $scope.tableValidation = false;
                $scope.tableValidationMsg = '';
                $scope.deliveryLocationValidation = $scope.paymentTermsValidation = $scope.deliveryTimeValidation = departmentsValidation = false;
                $scope.urgencyValidation = $scope.quotationFreezTimeValidation = $scope.selectedCurrencyValidation = $scope.quotationPriceLimitValidation = $scope.noOfQuotationRemindersValidation = $scope.remindersTimeIntervalValidation = $scope.questionnaireValidation = false;

                $scope.titleValidation = $scope.attachmentNameValidation = false;
                $scope.deliveryLocationValidation = $scope.paymentTermsValidation = $scope.deliveryTimeValidation = false;
                $scope.urgencyValidation = $scope.quotationFreezTimeValidation = $scope.selectedCurrencyValidation = $scope.quotationPriceLimitValidation = $scope.noOfQuotationRemindersValidation = $scope.questionnaireValidation = $scope.expNegotiationTimeValidation = false;
                //$scope.slotDescriptionError = $scope.slotStartError = $scope.slotEndError = $scope.slotVendorsError = false;
                $scope.slotTableValidation = false;
                $scope.slotTableValidationMsg = '';
                $scope.showQuotationFreezeTimeLesserMessage = false;
                $scope.showSlotMessage = '';
                if (pageNo == 1 || pageNo == 4) {

                    if ($scope.reqDepartments == null || $scope.reqDepartments == undefined || $scope.reqDepartments.length == 0) {
                        $scope.GetReqDepartments();
                    }
                    
                    if ($scope.currencies == null || $scope.currencies == undefined || $scope.currencies.length == 0) {
                        //$scope.getCurrencies();
                    }

                    if ($scope.formRequest.title == null || $scope.formRequest.title == '') {
                        $scope.titleValidation = true;
                        $scope.postRequestLoding = false;
                        return false;
                    }

                    if ($scope.formRequest.isTabular) {
                        $scope.selectedProducts = [];
                        $scope.formRequest.listFwdRequirementItems.forEach(function (item, index) {

                            if ($scope.tableValidation) {
                                return false;
                            }

                            var sno = parseInt(index) + 1;

                            if (item.productIDorName == null || item.productIDorName == '') {
                                $scope.tableValidation = true;
                                $scope.tableValidationMsg = 'Please enter product ID for item: ' + sno;
                                return;
                            }
                            else {
                                $scope.selectedProducts.push(item.catalogueItemID);
                                var productQuotationTemplateFiltered = _.filter(item.productQuotationTemplate, function (o) {
                                    return o.IS_VALID == 1;
                                });
                                if (productQuotationTemplateFiltered != null && productQuotationTemplateFiltered != undefined && productQuotationTemplateFiltered.length > 0) {

                                    

                                    item.productQuotationTemplateJson = JSON.stringify(productQuotationTemplateFiltered);
                                } else {
                                    item.productQuotationTemplateJson = '';
                                }


                            }
                            //if (item.productNo == null || item.productNo == '') {
                            //    $scope.tableValidation = true;
                            //}
                            //if (item.productDescription == null || item.productDescription == '') {
                            //    $scope.tableValidation = true;
                            //}
                            if (item.productQuantity <= 0 || item.productQuantity == undefined || item.productQuantity == '') {
                                $scope.tableValidation = true;
                                $scope.tableValidationMsg = 'Please enter product Quantity for item: ' + sno;
                                return;
                            }
                            if (item.productQuantityIn == null || item.productQuantityIn == '') {
                                $scope.tableValidation = true;
                                $scope.tableValidationMsg = 'Please enter product Units for item: ' + sno;
                                return;
                            }
                            if ($scope.formRequest.isSplitEnabled == true) {
                                if (item.fromrange == null || item.fromrange == '' || item.fromrange <= 0) {
                                    $scope.tableValidation = true;
                                    $scope.tableValidationMsg = 'Please enter From Range for item: ' + sno;
                                    return;
                                }
                                if (item.torange == null || item.torange == '' || item.torange <= 0) {
                                    $scope.tableValidation = true;
                                    $scope.tableValidationMsg = 'Please enter To Range for item: ' + sno;
                                    return;
                                }

                                if (parseFloat(item.fromrange) > parseFloat(item.torange)) {
                                    $scope.tableValidation = true;
                                    $scope.tableValidationMsg = 'Please make sure that To Range is higher than From Range for item: ' + sno;
                                    return;
                                }

                                //if (item.requiredQuantity == null || item.requiredQuantity == '' || item.requiredQuantity <= 0) {
                                //    $scope.tableValidation = true;
                                //    $scope.tableValidationMsg = 'Please enter Required Qty for item: ' + sno;
                                //    return;
                                //}


                            }
                        });


                        if ($scope.tableValidation) {
                            return false;
                        }

                    }
                    if (!$scope.formRequest.isTabular) {
                        if ($scope.formRequest.description == null || $scope.formRequest.description == '') {
                            $scope.descriptionValidation = true;
                            $scope.postRequestLoding = false;
                            return false;
                        }
                    }
                     

                    var phn = '';
                    if ($scope.userObj && $scope.userObj.phoneNum)
                    {
                        phn = $scope.userObj.phoneNum;
                        $scope.formRequest.phoneNum = phn;
                        $scope.formRequest.contactDetails = phn;
                    }
                    else {
                        $scope.formRequest.phoneNum = '';
                        $scope.formRequest.contactDetails = '';
                    }

                                       

                }

                if (pageNo == 2 || pageNo == 4) {

                    

                    if ($scope.questionnaireList == null || $scope.questionnaireList == undefined || $scope.questionnaireList.length == 0) {
                        $scope.getQuestionnaireList();
                    }


                    if ($scope.showInspectionValue == true)
                    {
                        
                        $scope.formRequest.listSlotDetails.forEach(function (item, index) {

                            var tempArrayStartTime = moment(item.slot_start_time, "DD-MM-YYYY HH:mm").valueOf();
                            var tempArrayEndTime = moment(item.slot_end_time, "DD-MM-YYYY HH:mm").valueOf();
                            var d1 = new Date();
                            d1.setHours(18, 00, 00);
                            d1 = new moment(d1).valueOf();

                            if (tempArrayStartTime > d1) {
                                $scope.showQuotationFreezeTimeLesserMessage = true;
                                $scope.showSlotMessage = "Slot Times are greater. Please Change Your Quotation Freeze Time greater than Your Slot Time (OR) Reduce the slot Start Time"
                            } else {
                                $scope.showQuotationFreezeTimeLesserMessage = false;
                            }
                            //$scope.showQuotationFreezeTimeLesserMessage = true;
                        })

                    }

                    if ($scope.formRequest.quotationFreezTime == null || $scope.formRequest.quotationFreezTime == '') {
                        var d = new Date();
                        d.setHours(18, 00, 00);
                        d = new moment(d).format("DD-MM-YYYY HH:mm");
                        $scope.formRequest.quotationFreezTime = d;
                        
                    }

                    
                    if ($scope.formRequest.expStartTime == null || $scope.formRequest.expStartTime == '') {
                        var dt = new Date();
                        var tomorrowNoon = new Date(dt.getFullYear(), dt.getMonth(), dt.getDate() + 1, 12, 0, 0);
                        tomorrowNoon = new moment(tomorrowNoon).format("DD-MM-YYYY HH:mm");
                        $scope.formRequest.expStartTime = tomorrowNoon;

                    }

                    if ($scope.formRequest.noOfQuotationReminders == null || $scope.formRequest.noOfQuotationReminders == '' || $scope.formRequest.noOfQuotationReminders <= 0 || $scope.formRequest.noOfQuotationReminders > 5) {
                        $scope.formRequest.noOfQuotationReminders = 3;
                    }
                    if ($scope.formRequest.remindersTimeInterval == null || $scope.formRequest.remindersTimeInterval == '' || $scope.formRequest.remindersTimeInterval <= 0) {
                        $scope.formRequest.remindersTimeInterval = 2;
                    }
                    if ($scope.formRequest.deliveryTime == null || $scope.formRequest.deliveryTime == '') {
                        $scope.deliveryTimeValidation = true;
                        $scope.postRequestLoding = false;
                        return false;
                    }
                    if ($scope.formRequest.deliveryLocation == null || $scope.formRequest.deliveryLocation == '') {
                        $scope.deliveryLocationValidation = true;
                        $scope.postRequestLoding = false;
                        return false;
                    }
                    if ($scope.formRequest.paymentTerms == null || $scope.formRequest.paymentTerms == '') {
                        $scope.paymentTermsValidation = true;
                        $scope.postRequestLoding = false;
                        return false;
                    }
                    

                    if ($scope.reqDepartments.length > 0) {

                        $scope.departmentsValidation = true;

                        $scope.reqDepartments.forEach(function (item, index) {
                            if (item.isValid == true || $scope.noDepartments == true)
                                $scope.departmentsValidation = false;
                        });

                        if ($scope.departmentsValidation == true) {
                            return false;
                        };
                    }

                    if ($scope.showInspectionValue == true)
                    {
                        $scope.TempArray = [];
                        
                        $scope.formRequest.listSlotDetails.forEach(function (item, index) {

                            if ($scope.slotTableValidation) {
                                return false;
                            }

                            var sno = parseInt(index) + 1;


                            if (item.slot_description == '' || item.slot_description == null || item.slot_description == undefined)
                            {
                                $scope.slotTableValidation = true;
                                $scope.slotTableValidationMsg = 'Please Enter Slot Name For Slot Number :' + sno;
                                return false;
                            }
                            if (item.slot_start_time == '' || item.slot_start_time == undefined || item.slot_start_time == null) {
                                $scope.slotTableValidation = true;
                                $scope.slotTableValidationMsg = 'Please Enter Start Time For Slot Number :' + sno;
                                return false;
                            }
                            if (item.slot_end_time == '' || item.slot_end_time == undefined || item.slot_end_time == null)
                            {
                                $scope.slotTableValidation = true;
                                $scope.slotTableValidationMsg = 'Please Enter EndTime For Slot Number :' + sno;
                                return false;
                            }
                            if (item.slot_no_of_vendors == '' || item.slot_no_of_vendors == null || item.slot_no_of_vendors == undefined || item.slot_no_of_vendors <= 0) {
                                $scope.slotTableValidation = true;
                                $scope.slotTableValidationMsg = 'Please Enter No Of Vendors For Slot Number :' + sno;
                                return false;
                            }
                           
                            var slotStartTime = moment(item.slot_start_time, "DD-MM-YYYY HH:mm").valueOf();
                            var m = moment(slotStartTime);
                            var sltStartTime = new Date(m);

                            var slotEndTime = moment(item.slot_end_time, "DD-MM-YYYY HH:mm").valueOf();
                            var m = moment(slotEndTime);
                            var sltEndtTime = new Date(m);

                            if (slotStartTime == slotEndTime) {
                                $scope.slotTableValidation = true;
                                $scope.slotTableValidationMsg = 'Slot Start And End Time Cannot be Same For Slot Number :' + sno;
                                return false;
                            }

                            if (sltEndtTime < sltStartTime) {
                                $scope.slotTableValidation = true;
                                $scope.slotTableValidationMsg = 'Slot End Time Cannot be less than Slot Start Time for item: ' + sno;
                                return false;
                            }

                        })

                        $scope.TempArray = $scope.formRequest.listSlotDetails;

                        $scope.TempArray.forEach(function (item, index) {
                            $scope.formRequest.listSlotDetails.forEach(function (item1, index1) {

                                if (index == index1)
                                {

                                }
                                else
                                {
                                    var tempArrayStartTime = moment(item.slot_start_time, "DD-MM-YYYY HH:mm").valueOf();
                                    //var m = moment(tempStartTime);
                                    //var tempArrayStartTime = new Date(m);

                                    var tempArrayEndTime = moment(item.slot_end_time, "DD-MM-YYYY HH:mm").valueOf();
                                    //var m1 = moment(tempEndTime);
                                    //var tempArrayEndTime = new Date(m1);

                                    var slotStartTime = moment(item1.slot_start_time, "DD-MM-YYYY HH:mm").valueOf();
                                    //var m3 = moment(slotStartTime1);
                                    //var slotStartTime = new Date(m3);

                                    var slotEndTime = moment(item1.slot_end_time, "DD-MM-YYYY HH:mm").valueOf();
                                    //var m4 = moment(slotEndTime1);
                                    //var slotEndTime = new Date(m4);


                                    if (tempArrayStartTime == slotStartTime || tempArrayStartTime == slotEndTime || tempArrayEndTime == slotStartTime || tempArrayEndTime == slotEndTime)
                                    {
                                        $scope.slotTableValidation = true;
                                        $scope.slotTableValidationMsg = 'Slots Times are Same' ;
                                        return false;
                                    }
                                }


                            })
                        })



                        if ($scope.slotTableValidation) {
                            return false;
                        }
                    }

                }

                if (pageNo == 3 || pageNo == 4) {

                    if ($scope.categories == null || $scope.categories == undefined || $scope.categories.length == 0) {
                        $scope.getCategories();
                    }
                  //  $scope.getproductvendors();

                    if ($scope.formRequest.urgency == null || $scope.formRequest.urgency == '') {
                        $scope.urgencyValidation = true;
                        $scope.postRequestLoding = false;
                        return false;
                    }
                    

                    if (($scope.formRequest.quotationPriceLimit == null || $scope.formRequest.quotationPriceLimit == '' || $scope.formRequest.quotationPriceLimit <= 0) && $scope.formRequest.isQuotationPriceLimit == true) {
                        $scope.quotationPriceLimitValidation = true;
                        $scope.postRequestLoding = false;
                        return false;
                    }
                    if ($scope.formRequest.noOfQuotationReminders == null || $scope.formRequest.noOfQuotationReminders == '' || $scope.formRequest.noOfQuotationReminders <= 0 || $scope.formRequest.noOfQuotationReminders > 5) {
                        $scope.noOfQuotationRemindersValidation = true;
                        $scope.postRequestLoding = false;
                        return false;
                    }
                    if ($scope.formRequest.remindersTimeInterval == null || $scope.formRequest.remindersTimeInterval == '' || $scope.formRequest.remindersTimeInterval <= 0) {
                        $scope.remindersTimeIntervalValidation = true;
                        $scope.postRequestLoding = false;
                        return false;
                    }
                    if ($scope.isTechEval == true && (!$scope.selectedQuestionnaire || $scope.selectedQuestionnaire.evalID <= 0)) {
                        $scope.questionnaireValidation = true;
                        $scope.postRequestLoding = false;
                        return false;
                    }
                    if ($scope.formRequest.quotationFreezTime == null || $scope.formRequest.quotationFreezTime == '') {
                        $scope.quotationFreezTimeValidation = true;
                        $scope.postRequestLoding = false;
                        return false;
                    }

                    var ts = moment($scope.formRequest.quotationFreezTime, "DD-MM-YYYY HH:mm").valueOf();
                    var m = moment(ts);
                    var quotationFreezTime = new Date(m);

                    var ts = moment($scope.formRequest.expStartTime, "DD-MM-YYYY HH:mm").valueOf();
                    var m = moment(ts);
                    var expStartTime = new Date(m);

                    auctionsService.getdate1()
                        .then(function (GetDateResponse) {
                            //var CurrentDate = moment(new Date(parseInt(GetDateResponse.substr(6))));

                            var CurrentDateToLocal = userService.toLocalDate(GetDateResponse);

                            var ts = moment(CurrentDateToLocal, "DD-MM-YYYY HH:mm").valueOf();
                            var m = moment(ts);
                            var deliveryDate = new Date(m);
                            var milliseconds = parseInt(deliveryDate.getTime() / 1000.0);
                            var CurrentDateToTicks = "/Date(" + milliseconds + "000+0530)/";

                            var CurrentDate = moment(new Date(parseInt(CurrentDateToTicks.substr(6))));
                            //console.log("------------start-----------");
                            //console.log("quotationFreezTime>>>>>>>>>>" + quotationFreezTime);
                            //console.log("------------end-------------");
                            //console.log("CurrentDate>>>>>>>>>>>>>>>>>" + CurrentDate);
                            if (quotationFreezTime < CurrentDate) {
                                $scope.quotationFreezTimeValidation = true;
                                $scope.postRequestLoding = false;
                                return false;
                            }
                            else
                                if (quotationFreezTime >= expStartTime) {
                                    $scope.expNegotiationTimeValidation = true;
                                    $scope.postRequestLoding = false;
                                    return false;
                                }
                            $scope.pageNo = $scope.pageNo + 1;
                          //  console.log("page NO:::::::::::" + $scope.pageNo);

                        })

                }

                if (pageNo == 4) {
                    $scope.formRequest.subcategoriesView = '';
                    for (i = 0; i < $scope.sub.selectedSubcategories.length; i++) {
                        $scope.formRequest.subcategoriesView += $scope.sub.selectedSubcategories[i].subcategory + ",";
                    }
                    $scope.pageNo = $scope.pageNo + 1;
                }

                if (pageNo != 3) {
                    $scope.pageNo = $scope.pageNo + 1;
                }

                // return $scope.pageNo;
                // location.reload();
            }

            $scope.previouspage = function () {
                $scope.pageNo = $scope.pageNo - 1;
                // location.reload();
                //return $scope.pageNo;
            }

            $scope.gotopage = function (pageNo) {
                $scope.pageNo = pageNo;

                
                return $scope.pageNo;
            }


            $scope.reqDepartments = [];
            $scope.userDepartments = [];

            $scope.GetReqDepartments = function () {
                $scope.reqDepartments = [];
                auctionsService.GetReqDepartments(userService.getUserId(), $scope.stateParamsReqID, userService.getUserToken())
                    .then(function (response) {
                        if (response && response.length > 0) {
                            $scope.reqDepartments = response;

                            $scope.noDepartments = true;

                            $scope.reqDepartments.forEach(function (item, index) {
                                if (item.isValid == true) {
                                    $scope.noDepartments = false;
                                }
                            });

                        }
                    });
            }

            // $scope.GetReqDepartments();


            $scope.SaveReqDepartments = function (reqID) {

                $scope.reqDepartments.forEach(function (item, index) {
                    item.reqID = reqID;
                    item.userID = userService.getUserId();
                })

                var params = {
                    "listReqDepartments": $scope.reqDepartments,
                    sessionID: userService.getUserToken()
                };

                auctionsService.SaveReqDepartments(params)
                    .then(function (response) {
                        if (response.errorMessage != '') {
                            growlService.growl(response.errorMessage, "inverse");
                        }
                        else {

                        }
                    });
            };

            $scope.noDepartments = false;


            $scope.noDepartmentsFunction = function (value) {

                $scope.departmentsValidation = false;

                if (value == 'NA') {
                    $scope.reqDepartments.forEach(function (item, index) {
                        item.isValid = false;
                    });
                }
                else {
                    $scope.reqDepartments.forEach(function (item, index) {
                        if (item.isValid == true)
                            $scope.noDepartments = false;
                    });
                }
            };


            $scope.reqDeptDesig = [];

            $scope.GetReqDeptDesig = function () {
                $scope.reqDeptDesig = [];
                auctionsService.GetReqDeptDesig(userService.getUserId(), $scope.stateParamsReqID, userService.getUserToken())
                    .then(function (response) {
                        if (response && response.length > 0) {
                            $scope.reqDeptDesig = response;
                            $scope.noDepartments = true;
                            $scope.reqDeptDesig.forEach(function (item, index) {
                                if (item.isValid == true) {
                                    $scope.noDepartments = false;
                                }
                            });
                        }
                    });
            };

            // $scope.GetReqDeptDesig();


            $scope.SaveReqDeptDesig = function (reqID) {

                $scope.reqDeptDesig.forEach(function (item, index) {
                    item.reqID = reqID;
                    item.userID = userService.getUserId();
                })

                var params = {
                    "listReqDepartments": $scope.reqDeptDesig,
                    sessionID: userService.getUserToken()
                };

                auctionsService.SaveReqDeptDesig(params)
                    .then(function (response) {
                        if (response.errorMessage != '') {
                            growlService.growl(response.errorMessage, "inverse");
                        }
                    });


            };

            $scope.AssignVendorToCompany = function (vendorPhone, vendorEmail) {

                if ($scope.newVendor.altPhoneNum == undefined) {
                    $scope.newVendor.altPhoneNum = '';
                }
                if ($scope.newVendor.altEmail == undefined) {
                    $scope.newVendor.altEmail = '';
                }

                var params = {
                    userID: userService.getUserId(),
                    vendorPhone: vendorPhone,
                    vendorEmail: vendorEmail,
                    sessionID: userService.getUserToken(),
                    category: $scope.formRequest.category,
                    subCategory: $scope.selectedSubCategoriesList,
                    altPhoneNum: $scope.newVendor.altPhoneNum,
                    altEmail: $scope.newVendor.altEmail
                };

                auctionsService.AssignVendorToCompany(params)
                    .then(function (response) {

                        $scope.vendor = response;

                        if ($scope.vendor.errorMessage == '' || $scope.vendor.errorMessage == 'VENDOR ADDED SUCCESSFULLY') {
                            $scope.formRequest.auctionVendors.push({ vendorName: $scope.vendor.firstName + ' ' + $scope.vendor.lastName, companyName: $scope.vendor.email, vendorID: $scope.vendor.userID });
                            $scope.newVendor = {};
                            $scope.checkVendorPhoneUniqueResult = false;
                            $scope.checkVendorEmailUniqueResult = false;
                            $scope.checkVendorPanUniqueResult = false;
                            $scope.checkVendorTinUniqueResult = false;
                            $scope.checkVendorStnUniqueResult = false;
                            $scope.firstvalidation = $scope.companyvalidation = $scope.lastvalidation = $scope.contactvalidation = $scope.emailvalidation = $scope.categoryvalidation = $scope.emailregxvalidation = $scope.contactvalidationlength = $scope.panregxvalidation = $scope.tinvalidation = $scope.stnvalidation = $scope.knownsincevalidation = $scope.vendorcurrencyvalidation = false;
                            growlService.growl('Vendor Added Successfully', "success");
                        }
                        else {
                            $scope.newVendor = {};
                            $scope.checkVendorPhoneUniqueResult = false;
                            $scope.checkVendorEmailUniqueResult = false;
                            $scope.checkVendorPanUniqueResult = false;
                            $scope.checkVendorTinUniqueResult = false;
                            $scope.checkVendorStnUniqueResult = false;
                            $scope.firstvalidation = $scope.companyvalidation = $scope.lastvalidation = $scope.contactvalidation = $scope.emailvalidation = $scope.categoryvalidation = $scope.emailregxvalidation = $scope.contactvalidationlength = $scope.panregxvalidation = $scope.tinvalidation = $scope.stnvalidation = $scope.knownsincevalidation = $scope.vendorcurrencyvalidation = false;
                            growlService.growl($scope.vendor.errorMessage, "inverse");
                        }
                    });
            };



            $scope.searchvendorstring = '';

            $scope.searchVendors = function (value) {
                $scope.Vendors = _.filter($scope.VendorsTemp, function (item) { return item.companyName.toUpperCase().indexOf(value.toUpperCase()) > -1; });
                // $scope.vendorsLoaded = true;
                // $scope.Vendors = _.filter($scope.allCompanyVendors, function (item) { return item.companyName.toUpperCase().indexOf(value.toUpperCase()) > -1; });
                // var temp = $scope.formRequest.auctionVendors;
                // $scope.formRequest.auctionVendors.forEach(function (item, index) {
                // $scope.Vendors = _.filter($scope.Vendors, function (vendor) { return vendor.vendorID !== item.vendorID; });
                // });
            }


            $scope.exportItemsToExcel = function () {
                var mystyle = {
                    sheetid: 'RequirementDetails',
                    headers: true,
                    column: {
                        style: 'font-size:15px;background:#233646;color:#FFF;'
                    }
                };

                alasql('SELECT itemID as [ItemID], productIDorName as [ProductName], productNo as [ProductNumber], productDescription as [Description], productQuantity as [Quantity], productQuantityIn as [Units], productBrand as PreferredBrand, othersBrands as OtherBrands INTO XLSX(?,{headers:true,sheetid: "RequirementDetails", style: "font-size:15px;background:#233646;color:#FFF;"}) FROM ?', ["RequirementDetails.xlsx", $scope.formRequest.listFwdRequirementItems]);
            }


            $scope.cancelClick = function () {
                $window.history.back();
            }




            $scope.paymentRadio = false;
            $scope.paymentlist = [];

            $scope.addpaymentvalue = function () {
                var listpaymet =
                    {
                        reqTermsID: 0,
                        reqID: $scope.stateParamsReqID,
                        userID: userService.getUserId(),
                        reqTermsDays: 0,
                        reqTermsPercent: 0,
                        reqTermsType: 'PAYMENT',
                        paymentType: '+',
                        isRevised: 0
                    };
                $scope.paymentlist.push(listpaymet);
            };

            $scope.delpaymentvalue = function (val, id) {

                if (id > 0) {
                    $scope.listTerms.push(id);
                }

                $scope.paymentlist.splice(val, 1);
            };

            $scope.resetpayment = function () {
                $scope.paymentlist = [];
            };




            $scope.deliveryRadio = false;
            $scope.deliveryList = [];

            $scope.adddeliveryvalue = function () {
                var deliveryObj =
                    {
                        reqTermsID: 0,
                        reqID: $scope.stateParamsReqID,
                        userID: userService.getUserId(),
                        reqTermsDays: 0,
                        reqTermsPercent: 0,
                        reqTermsType: 'DELIVERY',
                        isRevised: 0
                    };
                $scope.deliveryList.push(deliveryObj);
            };

            $scope.deldeliveryvalue = function (val, id) {

                if (id > 0) {
                    $scope.listTerms.push(id);
                }
                $scope.deliveryList.splice(val, 1);
            };

            $scope.resetdelivery = function () {
                $scope.deliveryList = [];
            };










            $scope.listRequirementTerms = [];

            $scope.SaveRequirementTerms = function (reqID) {

                $scope.listRequirementTerms = [];

                $scope.deliveryList.forEach(function (item, index) {
                    item.reqID = reqID;
                    $scope.listRequirementTerms.push(item);
                });

                $scope.paymentlist.forEach(function (item, index) {

                    item.reqID = reqID;

                    if (item.paymentType == '-') {
                        item.reqTermsDays = -(item.reqTermsDays);
                    }

                    $scope.listRequirementTerms.push(item);
                });

                var params = {
                    "listRequirementTerms": $scope.listRequirementTerms,
                    sessionID: userService.getUserToken()
                };

                auctionsService.SaveRequirementTerms(params)
                    .then(function (response) {
                        if (response.errorMessage != '') {
                            growlService.growl(response.errorMessage, "inverse");
                        }
                    });


            };




            $scope.GetRequirementTerms = function () {
                auctionsService.GetRequirementTerms(userService.getUserId(), $scope.stateParamsReqID, userService.getUserToken())
                    .then(function (response) {
                        if (response && response.length > 0) {
                            $scope.listRequirementTerms = response;

                            $scope.listRequirementTerms.forEach(function (item, index) {
                                if (item.reqTermsType == 'DELIVERY') {
                                    $scope.deliveryList.push(item);
                                    $scope.deliveryRadio = true;
                                }
                                else if (item.reqTermsType == 'PAYMENT') {

                                    if (item.reqTermsDays > 0) {
                                        item.paymentType = '+';
                                    }
                                    else if (item.reqTermsDays < 0) {
                                        item.paymentType = '-';
                                        item.reqTermsDays = -(item.reqTermsDays);
                                    }

                                    $scope.paymentlist.push(item);
                                    $scope.paymentRadio = true;
                                }
                            });



                        }
                    });
            }

           // $scope.GetRequirementTerms();




            $scope.listTerms = [];

            $scope.DeleteRequirementTerms = function () {

                var params = {
                    "listTerms": $scope.listTerms,
                    sessionID: userService.getUserToken()
                };

                auctionsService.DeleteRequirementTerms(params)
                    .then(function (response) {
                        if (response.errorMessage != '') {
                            growlService.growl(response.errorMessage, "inverse");
                        }
                        else {

                        }
                    });
            };




            $scope.paymentTypeFunction = function (type) {
                if (type == 'ADVANCE') {

                }
                if (type == 'POST') {

                }
            }







            $scope.GetCIJList = function () {
                auctionsService.GetCIJList($scope.compID, $scope.sessionid)
                    .then(function (response) {
                        $scope.cijList = response;

                        $scope.cijList.forEach(function (item, index) {
                            var cijObj = $.parseJSON(item.cij);
                            item.cij = cijObj;
                        });


                    })
            }


           // $scope.GetCIJList();



            $scope.GetIndentList = function () {
                auctionsService.GetIndentList(userService.getUserCompanyId(), userService.getUserToken())
                    .then(function (response) {
                        $scope.indentList = response;
                    })
            }

           // $scope.GetIndentList();


            $scope.changeIndent = function (val) {
            };

            $scope.formRequest.category = '';

            $scope.goToStage = function (stage, currentStage) {
                $scope.postRequestLoding = true;
                $scope.tableValidation = $scope.validationFailed = false;
                if (stage == 2 && currentStage == 1) {

                }
                $scope.formRequest.listFwdRequirementItems.forEach(function (item, index) {
                    if (item.productQuantity <= 0 || item.productQuantity == undefined || item.productQuantity == '') {
                        $scope.tableValidation = true;
                        $scope.validationFailed = true;
                    }
                });
                if ($scope.tableValidation) {
                    $scope.postRequestLoding = false;
                    return false;
                }

                if ($scope.formRequest.title == null || $scope.formRequest.title == '') {
                    $scope.titleValidation = true;
                    $scope.postRequestLoding = false;
                    return false;
                }

                if (currentStage == 2 && stage > currentStage) {
                    if ($scope.formRequest.deliveryLocation == null || $scope.formRequest.deliveryLocation == '') {
                        $scope.deliveryLocationValidation = true;
                        $scope.postRequestLoding = false;
                        return false;
                    }
                    if ($scope.formRequest.paymentTerms == null || $scope.formRequest.paymentTerms == '') {
                        $scope.paymentTermsValidation = true;
                        $scope.postRequestLoding = false;
                        return false;
                    }
                    if ($scope.formRequest.deliveryTime == null || $scope.formRequest.deliveryTime == '') {
                        $scope.deliveryTimeValidation = true;
                        $scope.postRequestLoding = false;
                        return false;
                    }
                }
                if (currentStage == 3 && stage > currentStage) {
                    if ($scope.formRequest.urgency == null || $scope.formRequest.urgency == '') {
                        $scope.urgencyValidation = true;
                        $scope.postRequestLoding = false;
                        return false;
                    }
                    
                    if (($scope.formRequest.quotationPriceLimit == null || $scope.formRequest.quotationPriceLimit == '' || $scope.formRequest.quotationPriceLimit <= '') && $scope.formRequest.isQuotationPriceLimit == true) {
                        $scope.quotationPriceLimitValidation = true;
                        $scope.postRequestLoding = false;
                        return false;
                    }
                    if ($scope.formRequest.isQuotationPriceLimit == false) {
                        $scope.formRequest.quotationPriceLimit = 0;
                    }

                    if ($scope.formRequest.noOfQuotationReminders > 0)
                    {
                        $scope.formRequest.noOfQuotationReminders = parseInt($scope.formRequest.noOfQuotationReminders);
                    }
                    if ($scope.formRequest.remindersTimeInterval > 0) {
                        $scope.formRequest.remindersTimeInterval = parseInt($scope.formRequest.remindersTimeInterval);
                    }


                    if ($scope.formRequest.noOfQuotationReminders == null || $scope.formRequest.noOfQuotationReminders == '' || $scope.formRequest.noOfQuotationReminders <= 0 || $scope.formRequest.noOfQuotationReminders > 5) {
                        $scope.noOfQuotationRemindersValidation = true;
                        $scope.postRequestLoding = false;
                        return false;
                    }
                    if ($scope.formRequest.remindersTimeInterval == null || $scope.formRequest.remindersTimeInterval == '' || $scope.formRequest.remindersTimeInterval <= 0) {
                        $scope.remindersTimeIntervalValidation = true;
                        $scope.postRequestLoding = false;
                        return false;
                    }
                    if ($scope.isTechEval == true && (!$scope.selectedQuestionnaire || $scope.selectedQuestionnaire.evalID <= 0)) {
                        $scope.questionnaireValidation = true;
                        $scope.postRequestLoding = false;
                        return false;
                    }
                    if ($scope.formRequest.quotationFreezTime == null || $scope.formRequest.quotationFreezTime == '') {
                        $scope.quotationFreezTimeValidation = true;
                        $scope.postRequestLoding = false;
                        return false;
                    }
                }
                if (!$scope.postRequestLoding) {
                    return false;
                } else {
                    $scope.postRequestLoding = true;
                    var ts = userService.toUTCTicks($scope.formRequest.quotationFreezTime);
                    var m = moment(ts);
                    var quotationDate = new Date(m);
                    var milliseconds = parseInt(quotationDate.getTime() / 1000.0);
                    $scope.formRequest.quotationFreezTime = "/Date(" + milliseconds + "000+0530)/";
                    var ts = userService.toUTCTicks($scope.formRequest.expStartTime);
                    var m = moment(ts);
                    var quotationDate = new Date(m);
                    var milliseconds = parseInt(quotationDate.getTime() / 1000.0);
                    $scope.formRequest.expStartTime = "/Date(" + milliseconds + "000+0530)/";

                    $scope.formRequest.requirementID = $stateParams.Id ? $stateParams.Id : -1;
                    $scope.formRequest.customerID = userService.getUserId();
                    $scope.formRequest.customerFirstname = userService.getFirstname();
                    $scope.formRequest.customerLastname = userService.getLastname();
                    $scope.formRequest.isClosed = "NOTSTARTED";
                    $scope.formRequest.endTime = "";
                    $scope.formRequest.sessionID = userService.getUserToken();
                    $scope.formRequest.subcategories = "";
                    $scope.formRequest.budget = 100000;
                    $scope.formRequest.custCompID = userService.getUserCompanyId();
                    $scope.formRequest.customerCompanyName = userService.getUserCompanyId();

                    for (i = 0; i < $scope.sub.selectedSubcategories.length; i++) {
                        $scope.formRequest.subcategories += $scope.sub.selectedSubcategories[i].subcategory + ",";
                    }

                    var category = [];
                    if ($scope.formRequest.category) {
                        category.push($scope.formRequest.category);
                    }
                    $scope.formRequest.category = category;
                    if (!$scope.formRequest.isForwardBidding) {
                        $scope.formRequest.cloneID = 0;
                        if ($stateParams.reqObj && $stateParams.reqObj.cloneId && $stateParams.reqObj.cloneId !== "") {
                            $scope.formRequest.cloneID = $stateParams.reqObj.cloneId;
                        }

                        PRMFwdReqServiceService.postrequirementdata($scope.formRequest)
                            .then(function (response) {
                                if (response.objectID != 0) {
                                    if ($scope.selectedProject) {
                                        $scope.selectedProject.sessionID = userService.getUserToken();
                                        $scope.selectedProject.requirement = {};
                                        $scope.selectedProject.requirement.requirementID = response.objectID;
                                        var param = { item: $scope.selectedProject };
                                    }

                                    $scope.SaveReqDepartments(response.objectID);
                                    if ($scope.selectedQuestionnaire != null && $scope.selectedQuestionnaire != undefined && $scope.selectedQuestionnaire.evalID != undefined) {
                                        techevalService.getquestionnaire($scope.selectedQuestionnaire.evalID, 1)
                                            .then(function (response1) {
                                                $scope.selectedQuestionnaire = response1;

                                                if ($scope.isTechEval) {
                                                    $scope.selectedQuestionnaire.reqID = response.objectID;
                                                }
                                                else {
                                                    $scope.selectedQuestionnaire.reqID = 0;
                                                }
                                                $scope.selectedQuestionnaire.sessionID = userService.getUserToken();
                                                var params = {
                                                    questionnaire: $scope.selectedQuestionnaire
                                                }
                                                techevalService.assignquestionnaire(params)
                                                    .then(function (response) {
                                                    })
                                            })
                                    }
                                    if (stage) {
                                        $state.go('save-requirement', { Id: response.objectID });
                                    }
                                }
                            });
                    }
                }             
            }

            $scope.goBack = function (stage, reqID) {
                $state.go('save-requirement', { Id: reqID });
            }

            $scope.removeAttach = function (index) {
                $scope.formRequest.multipleAttachments.splice(index, 1);
            }

            $scope.productsList = [];
            $scope.getProducts = function () {
                catalogService.getUserProducts($scope.compID, userService.getUserId())
                    .then(function (response) {
                        $scope.productsList = response;
                        //$scope.productsListRaw = response;
                        //$scope.productsListRaw.forEach(function (item, index) {
                        //        $scope.productsList.push({ "prodName": item.prodName, "prodId": item.prodId });
                        //});
                    });
            }
           // $scope.getProducts();
            // ["Afghanistan", "Albania", "Algeria", "Andorra", "Angola", "Anguilla", "Antigua &amp; Barbuda", "Argentina", "Armenia", "Aruba", "Australia", "Austria", "Azerbaijan", "Bahamas", "Bahrain", "Bangladesh", "Barbados", "Belarus", "Belgium", "Belize", "Benin", "Bermuda", "Bhutan", "Bolivia", "Bosnia &amp; Herzegovina", "Botswana", "Brazil", "British Virgin Islands", "Brunei", "Bulgaria", "Burkina Faso", "Burundi", "Cambodia", "Cameroon", "Cape Verde", "Cayman Islands", "Chad", "Chile", "China", "Colombia", "Congo", "Cook Islands", "Costa Rica", "Cote D Ivoire", "Croatia", "Cruise Ship", "Cuba", "Cyprus", "Czech Republic", "Denmark", "Djibouti", "Dominica", "Dominican Republic", "Ecuador", "Egypt", "El Salvador", "Equatorial Guinea", "Estonia", "Ethiopia", "Falkland Islands", "Faroe Islands", "Fiji", "Finland", "France", "French Polynesia", "French West Indies", "Gabon", "Gambia", "Georgia", "Germany", "Ghana", "Gibraltar", "Greece", "Greenland", "Grenada", "Guam", "Guatemala", "Guernsey", "Guinea", "Guinea Bissau", "Guyana", "Haiti", "Honduras", "Hong Kong", "Hungary", "Iceland", "India", "Indonesia", "Iran", "Iraq", "Ireland", "Isle of Man", "Israel", "Italy", "Jamaica", "Japan", "Jersey", "Jordan", "Kazakhstan", "Kenya", "Kuwait", "Kyrgyz Republic", "Laos", "Latvia", "Lebanon", "Lesotho", "Liberia", "Libya", "Liechtenstein", "Lithuania", "Luxembourg", "Macau", "Macedonia", "Madagascar", "Malawi", "Malaysia", "Maldives", "Mali", "Malta", "Mauritania", "Mauritius", "Mexico", "Moldova", "Monaco", "Mongolia", "Montenegro", "Montserrat", "Morocco", "Mozambique", "Namibia", "Nepal", "Netherlands", "Netherlands Antilles", "New Caledonia", "New Zealand", "Nicaragua", "Niger", "Nigeria", "Norway", "Oman", "Pakistan", "Palestine", "Panama", "Papua New Guinea", "Paraguay", "Peru", "Philippines", "Poland", "Portugal", "Puerto Rico", "Qatar", "Reunion", "Romania", "Russia", "Rwanda", "Saint Pierre &amp; Miquelon", "Samoa", "San Marino", "Satellite", "Saudi Arabia", "Senegal", "Serbia", "Seychelles", "Sierra Leone", "Singapore", "Slovakia", "Slovenia", "South Africa", "South Korea", "Spain", "Sri Lanka", "St Kitts &amp; Nevis", "St Lucia", "St Vincent", "St. Lucia", "Sudan", "Suriname", "Swaziland", "Sweden", "Switzerland", "Syria", "Taiwan", "Tajikistan", "Tanzania", "Thailand", "Timor L'Este", "Togo", "Tonga", "Trinidad &amp; Tobago", "Tunisia", "Turkey", "Turkmenistan", "Turks &amp; Caicos", "Uganda", "Ukraine", "United Arab Emirates", "United Kingdom", "Uruguay", "Uzbekistan", "Venezuela", "Vietnam", "Virgin Islands (US)", "Yemen", "Zambia", "Zimbabwe"];
            //$scope.autofillProduct = function (prodName, index) {
            //    $scope['ItemSelected_' + index] = false;
            //    var output = [];
            //    if (prodName && prodName.trim() != '') {
            //        angular.forEach($scope.productsList, function (prod) {
            //            if (prod.prodName.toLowerCase().indexOf(prodName.trim().toLowerCase()) >= 0) {
            //                output.push(prod);
            //            }
            //        });
            //    }
            //    $scope["filterProducts_"+index]= output;
            //}
            //$scope.fillTextbox = function (selProd, index) {
            //    $scope['ItemSelected_' + index] = true;
            //    $scope.formRequest.listFwdRequirementItems[index].productIDorName = selProd.prodName;
            //    $scope.formRequest.listFwdRequirementItems[index].catalogueItemID = selProd.prodId;
            //    $scope.formRequest.listFwdRequirementItems[index].productNo = selProd.prodNo;
            //    $scope.formRequest.listFwdRequirementItems[index].hsnCode = selProd.prodHSNCode;
            //    $scope.formRequest.listFwdRequirementItems[index].productDescription = selProd.prodDesc;
            //    //$scope.formRequest.listFwdRequirementItems[index].productQuantityInList = []; //;
            //    //$scope.formRequest.listFwdRequirementItems[index].productQuantityInList.push(selProd.prodQty);
            //    $scope.formRequest.listFwdRequirementItems[index].productQuantityIn = selProd.prodQty;
            //    $scope['filterProducts_' + index] = null;
            //}

            // in form catalogue ctrl
            $scope.fillTextbox = function (selProd, index) {
                $scope['ItemSelected_' + index] = true;
                $scope.formRequest.listFwdRequirementItems[index].productIDorName = selProd.prodName;
                $scope.formRequest.listFwdRequirementItems[index].catalogueItemID = selProd.prodId;
                $scope.formRequest.listFwdRequirementItems[index].productNo = selProd.prodNo
                $scope.formRequest.listFwdRequirementItems[index].hsnCode = selProd.prodHSNCode; 
                $scope.formRequest.listFwdRequirementItems[index].productDescription = selProd.prodDesc;
                $scope.formRequest.listFwdRequirementItems[index].productCode = selProd.prodCode;
                //$scope.formRequest.listFwdRequirementItems[index].productQuantityInList = []; //;
                //$scope.formRequest.listFwdRequirementItems[index].productQuantityInList.push(selProd.prodQty);
                $scope.formRequest.listFwdRequirementItems[index].productQuantityIn = selProd.prodQty;
                $scope['filterProducts_' + index] = null;
                $scope["filterProducts_Code_" + index] = null;
                $scope["filterProducts_Code1_" + index] = null;

                $scope.GetProductQuotationTemplate(selProd.prodId, index);
            }

            $scope.autofillProduct = function (prodName, index, fieldType) {
                $scope['ItemSelected_' + index] = false;
                var output = [];
                if (prodName && prodName != '' && prodName.length > 0) {
                    prodName = String(prodName).toUpperCase();
                    output = $scope.productsList.filter(function (product) {
                        if (fieldType == 'NAME') {
                            return (String(product.prodName).toUpperCase().includes(prodName) == true);
                        }
                        else if (fieldType == 'CODE') {
                            return (String(product.prodNo).toUpperCase().includes(prodName) == true);
                        } 
                        else if (fieldType == 'CODEMAIN') {
                            return (String(product.prodCode).toUpperCase().includes(prodName) == true);
                        }
                    });
                }
                if (fieldType == 'NAME') {
                    $scope["filterProducts_" + index] = output;
                }
                else if (fieldType == 'CODE') {
                    $scope["filterProducts_Code_" + index] = output;
                }
                else if (fieldType == 'CODEMAIN') {
                    $scope["filterProducts_Code1_" + index] = output;
                }
            }

            $scope.onBlurProduct = function (index) {
                if ($scope['ItemSelected_' + index] == false) {
                    $scope.formRequest.listFwdRequirementItems[index].productIDorName = "";
                    $scope.formRequest.listFwdRequirementItems[index].productNo = "";
                    $scope.formRequest.listFwdRequirementItems[index].productCode = "";
                }
                //$scope['filterProducts_' + index] = null;
            }

            $scope.formSplitChange = function () {
                var splitenabled = $scope.formRequest.isSplitEnabled;
                $scope.formRequest.listFwdRequirementItems.forEach(function (item, index) {
                    item.splitenabled = splitenabled;
                    if (splitenabled) {
                        item.productQuantity = 1;
                        $scope.formRequest.isDiscountQuotation = 0;
                    }
                });
            }

            $scope.onSplitChange = function (index) {


                if ($scope.formRequest.listFwdRequirementItems[index].splitenabled == true) {
                    $scope.formRequest.listFwdRequirementItems[index].productQuantity = 1;
                }
            }

            $scope.getproductvendors = function () {
                $scope.vendorsLoaded = false;
                $scope.Vendors = [];

                //products.push($scope.formRequest.category);
                //$scope.formRequest.category = category;
                if ($scope.selectedProducts.length > 0) {
                    var params = { 'products': $scope.selectedProducts, 'sessionID': userService.getUserToken(), 'uID': userService.getUserId(), evalID: $scope.isTechEval ? $scope.selectedQuestionnaire.evalID : 0 };
                    $http({
                        method: 'POST',
                        url: domain + 'getvendorsbyproducts',
                        encodeURI: true,
                        headers: { 'Content-Type': 'application/json' },
                        data: params
                    }).then(function (response) {
                        if (response && response.data) {
                            if (response.data.length > 0) {
                                $scope.Vendors = response.data;
                                // if (allVendors === 1) {
                                // $scope.allCompanyVendors = response.data;
                                // allVendors = 0;
                                // }  
                                $scope.vendorsLoaded = true;
                                for (var j in $scope.formRequest.auctionVendors) {
                                    for (var i in $scope.Vendors) {
                                        if ($scope.Vendors[i].vendorName == $scope.formRequest.auctionVendors[j].vendorName) {
                                            $scope.Vendors.splice(i, 1);
                                        }
                                    }
                                }

                                $scope.VendorsTemp = $scope.Vendors;
                                $scope.searchVendors('');

                            }
                            //$scope.formRequest.auctionVendors =[];
                        } else {
                        }
                    }, function (result) {
                    });
                }

            };

            $scope.getCurrencies();
            

            $scope.showInspectionValue = false;

            $scope.showInspection = function (val) {
                
                $scope.showInspectionValue = val ? true : false;


                if (!val) {
                    $scope.formRequest.listSlotDetails.forEach(function (item,index) {
                        item.slot_description = '',
                        item.slot_start_time =  '',
                        item.slot_end_time = '',
                        item.slot_no_of_vendors =  1
                    })
                }


            }



            //if ($stateParams.Id > 0) {
            //    $scope.isCloned = $stateParams.reqObj.cloneId > 0 ? true : false;

            //    if ($scope.isCloned) {
            //        $scope.showInspection(false);
            //    }

            //    //if ($stateParams.reqObj != null) {
            //    //    if ($stateParams.reqObj.cloneId!=null ) {
            //    //        $scope.isCloned = $stateParams.reqObj.cloneId;
            //    //    }
            //    //}
            //    //else if ($stateParams.reqObj == null){
            //    //    window.onbeforeunload = function () {
            //    //        //return "Dude, are you sure you want to refresh? Think of the kittens!";
            //    //        swal("This is a Clone Requirement Changes may not be saved");
            //    //        $state.go("req-fwd-List");
            //    //    }
            //    //}
            //} 







            $scope.itemSlotSNo = 1;

            //$scope.itemnumber = $scope.formRequest.listSlotDetails.length;

            //if ($scope.formRequest.showInspectionValue) {
                $scope.slotDetails =
                    {
                        slotSNo: $scope.itemSlotSNo++,
                        slot_description: '',
                        slot_start_time: '',
                        slot_end_time: '',
                        slot_no_of_vendors: 1
                    }
                $scope.formRequest.listSlotDetails.push($scope.slotDetails);
           // }
                
            $scope.AddSlot = function () {
                //$scope.itemnumber = $scope.formRequest.listSlotDetails.length;
                $scope.slotDetails =
                    {
                        slotSNo: $scope.itemSlotSNo++,
                        slot_description: '',
                        slot_start_time: '',
                        slot_end_time: '',
                        slot_no_of_vendors: 1,

                    }
                $scope.formRequest.listSlotDetails.push($scope.slotDetails);
            };

            $scope.deleteSlot = function (index,slotID) {


                if ($stateParams.Id > 0) {
                    var params = {
                        slotID: slotID,
                        entityID: $stateParams.Id,
                        sessionID: userService.getUserToken()
                    }
                    PRMFwdReqServiceService.DeleteSlot(params)
                        .then(function (response) {
                            if (response.errorMessage != '') {
                                growlService.growl(response.errorMessage, "inverse");
                            } else {
                                $scope.formRequest.listSlotDetails.splice(index, 1);
                                //growlService.growl("CheckList Stage Deleted Successfully", "success");
                            }

                        });
                } else {
                    if ($scope.formRequest.listSlotDetails.length > 1) {
                        $scope.formRequest.listSlotDetails.splice(index, 1);
                    } else {
                        growlService.growl("Can't Delete this Item", "inverse");
                    }

                }




            };
            
            //$scope.populateSlotEndTime = function (startDate) {
            //    if (startDate != '') {
            //        var ts = moment(startDate, "DD-MM-YYYY HH:mm").valueOf();
            //        var m = moment(ts);
            //        var twentyMinutes = new Date(m);
            //        $scope.slotDetails.slot_end_time = moment(twentyMinutes.setMinutes(twentyMinutes.getMinutes() + 120)).format("DD-MM-YYYY HH:mm");
            //    } else {


            //    }
            //}

            //if ($scope.slotDetails.slot_start_time!='') {
            //    $scope.populateSlotEndTime($scope.slotDetails.slot_start_time);
            //}



            $scope.$watch("slotDetails.slot_start_time", function (newValue, oldValue) {
                if (!newValue)
                {
                    return
                }
                else
                {
                    //console.log("value changed", newValue);
                    if ($scope.slotDetails.slot_start_time != '') {
                        var ts = moment($scope.slotDetails.slot_start_time, "DD-MM-YYYY HH:mm").valueOf();
                        var m = moment(ts);
                        var twentyMinutes = new Date(m);
                        // $scope.slotDetails.slot_end_time = moment(twentyMinutes.setMinutes(twentyMinutes.getMinutes() + 120)).format("DD-MM-YYYY HH:mm");
                    } else {


                    }
                }
            })

            $scope.ViewSlot = function (SNo, ReqID, slotIndex) {
                $scope.slotVendors = [];
                var params = { 'slotId': SNo, 'ReqId': ReqID, 'sessionID': userService.getUserToken() };
                PRMFwdReqServiceService.getslotsbyslotid(params)
                    .then(function (response) {
                        $scope.formRequest.listSlotDetails[slotIndex].slotVendors = [];
                        $scope.formRequest.listSlotDetails[slotIndex].slotVendors = response;
                });
            }



            /*region start WORKFLOW*/

            //$scope.getWorkflows = function () {
            //    workflowService.getWorkflowList()
            //        .then(function (response) {
            //            $scope.workflowList = [];
            //            $scope.workflowListTemp = response;
            //            $scope.workflowListTemp.forEach(function (item, index) {
            //                if (item.WorkflowModule == $scope.WorkflowModule) {
            //                    $scope.workflowList.push(item);
            //                }
            //            });

            //            //if (userService.getUserObj().isSuperUser) {
            //            //    $scope.workflowList = $scope.workflowList;
            //            //}
            //            //else {
            //            //    $scope.workflowList = $scope.workflowList.filter(function (item) {
            //            //        return item.deptID == userService.getSelectedUserDeptID();

            //            //    });
            //            //}



            //        });
            //};
            ////form Ctrl
            //$scope.getWorkflows();

            //$scope.getItemWorkflow = function () {
            //    workflowService.getItemWorkflow(0, $stateParams.Id, $scope.WorkflowModule)
            //        .then(function (response) {
            //            $scope.itemWorkflow = response;
            //            if ($scope.itemWorkflow && $scope.itemWorkflow.length > 0 && $scope.itemWorkflow[0].WorkflowTracks.length > 0) {
            //                $scope.currentStep = 0;

            //                var count = 0;

            //                $scope.itemWorkflow[0].WorkflowTracks.forEach(function (track) {

            //                    if (track.status == 'APPROVED' || track.status == 'HOLD') {
            //                        $scope.isFormdisabled = true;
            //                    }

            //                    if (track.status == 'APPROVED') {
            //                        $scope.isWorkflowCompleted = true;
            //                        $scope.orderInfo = track.order;
            //                        $scope.assignToShow = track.status;

            //                    }
            //                    else {
            //                        $scope.isWorkflowCompleted = false;
            //                    }



            //                    if (track.status == 'REJECTED' && count == 0) {
            //                        count = count + 1;
            //                    }

            //                    if ((track.status == 'PENDING' || track.status == 'HOLD') && count == 0) {
            //                        count = count + 1;
            //                        $scope.IsUserApproverForStage(track.approverID);
            //                        $scope.currentAccess = track.order;
            //                    }

            //                    if ((track.status == 'PENDING' || track.status == 'HOLD' || track.status == 'REJECTED') && $scope.currentStep == 0) {
            //                        $scope.currentStep = track.order;
            //                        return false;
            //                    }
            //                });
            //            }
            //        });

            //};

            //$scope.updateTrack = function (step, status) {

            //    $scope.commentsError = '';

            //    if ($scope.isReject) {
            //        $scope.commentsError = 'Please Save Rejected Items/Qty';
            //        return false;
            //    }

            //    if (status == 'REJECTED' && (step.comments == null || step.comments == "")) {
            //        $scope.commentsError = 'Please enter comments';
            //        return false;
            //    }

            //    step.status = status;
            //    step.sessionID = $scope.sessionID;
            //    step.modifiedBy = userService.getUserId();

            //    step.moduleName = $scope.WorkflowModule;

            //    workflowService.SaveWorkflowTrack(step)
            //        .then(function (response) {
            //            if (response.errorMessage != '') {
            //                growlService.growl(response.errorMessage, "inverse");
            //            }
            //            else {
            //                $scope.getItemWorkflow();
            //                //location.reload();
            //                //     $state.go('list-pr');
            //            }
            //        })
            //};

            //$scope.assignWorkflow = function (moduleID) {
            //    workflowService.assignWorkflow(({ wID: $scope.formRequest.workflowID, moduleID: moduleID, user: userService.getUserId(), sessionID: $scope.sessionID }))
            //        .then(function (response) {
            //            if (response.errorMessage != '') {
            //                growlService.growl(response.errorMessage, "inverse");
            //                $scope.isSaveDisable = false;
            //            }
            //            else {
            //                //  $state.go('list-pr');
            //            }
            //        })
            //};

            //$scope.IsUserApprover = false;

            //$scope.functionResponse = false;

            //$scope.IsUserApproverForStage = function (approverID) {
            //    workflowService.IsUserApproverForStage(approverID, userService.getUserId())
            //        .then(function (response) {
            //            $scope.IsUserApprover = response;
            //        });
            //};

            //$scope.isApproverDisable = function (index) {

            //    var disable = true;

            //    var previousStep = {};

            //    $scope.itemWorkflow[0].WorkflowTracks.forEach(function (step, stepIndex) {

            //        if (index == stepIndex) {
            //            if (stepIndex == 0) {
            //                if (userService.getLocalDeptDesigt().deptId == step.department.deptID && userService.getLocalDeptDesigt().desigId == step.approver.desigID &&
            //                    (step.status == 'PENDING' || step.status == 'HOLD')) {
            //                    disable = false;
            //                }
            //                else {
            //                    disable = true;
            //                }
            //            }
            //            else if (stepIndex > 0) {
            //                if (previousStep.status == 'PENDING' || previousStep.status == 'HOLD' || previousStep.status == 'REJECTED') {
            //                    disable = true;
            //                }
            //                else if (userService.getLocalDeptDesigt().deptId == step.department.deptID && userService.getLocalDeptDesigt().desigId == step.approver.desigID &&
            //                    (step.status == 'PENDING' || step.status == 'HOLD')) {
            //                    disable = false;
            //                }
            //                else {
            //                    disable = true;
            //                }
            //            }
            //        }
            //        previousStep = step;
            //    })

            //    return disable;
            //};

            /*region end WORKFLOW*/


            //$scope.GetReqPRList = function () {
            //    var params = {
            //        "userid": userService.getUserId(),
            //        "sessionid": userService.getUserToken()
            //    }
            //    PRMPRServices.getreqprlist(params)
            //        .then(function (response) {
            //            $scope.PRList = response;
            //        })
            //};

            //$scope.GetReqPRList();

            //$scope.getPRNumber = function (pr_ID)
            //{
            //    var params = {
            //        "prid": pr_ID,
            //        "sessionid": userService.getUserToken()
            //    };
            //    PRMPRServices.getprdetails(params)
            //        .then(function (response) {
            //            $scope.PRDetails = response;
            //            if ($scope.PRDetails.PR_ID == pr_ID) {
            //                //$scope.formRequest.PR_ID = $scope.PRDetails.PR_ID;
            //                $scope.formRequest.PR_NUMBER = $scope.PRDetails.PR_NUMBER;
            //            }
            //        })
            //}




            //$scope.GetPRItemsList = function () {
            //    if ($scope.formRequest.PR_ID > 0) {
            //        var params = {
            //            "prid": $scope.formRequest.PR_ID,
            //            "sessionid": userService.getUserToken()
            //        }
            //        PRMPRServices.getpritemslist(params)
            //            .then(function (response) {
            //                $scope.PRItemsList = response;


            //                $scope.formRequest.listFwdRequirementItems = [];

            //                $scope.PRItemsList.forEach(function (item, index) {


            //                    $scope.requirementItems =
            //                        {
            //                            productSNo: index + 1,
            //                            ItemID: 0,
            //                            productIDorName: item.ITEM_NAME,
            //                            productNo: item.ITEM_NUM,
            //                            hsnCode: item.HSN_CODE,
            //                            productDescription: item.ITEM_DESCRIPTION,
            //                            productQuantity: item.REQUIRED_QUANTITY,
            //                            productQuantityIn: item.UNITS,
            //                            productBrand: item.BRAND,
            //                            othersBrands: '',
            //                            isDeleted: 0,
            //                            productImageID: item.ATTACHMENTS,
            //                            attachmentName: '',
            //                            //budgetID: 0,
            //                            //bcInfo: '',
            //                            productCode: item.ITEM_CODE,
            //                            splitenabled: 0,
            //                            fromrange: 0,
            //                            torange: 0,
            //                            requiredQuantity: 0,
            //                            I_LLP_DETAILS: "",
            //                            itemAttachment: "",
            //                            itemMinReduction: 0

            //                        }
            //                    if ($scope.requirementItems.productImageID == null || $scope.requirementItems.productImageID == '') {
            //                        $scope.requirementItems.productImageID = 0;
            //                    } else {
            //                        $scope.requirementItems.productImageID == item.ATTACHMENTS;

            //                    }

            //                    $scope.formRequest.listFwdRequirementItems.push($scope.requirementItems);

            //                    $scope.formRequest.listFwdRequirementItems.forEach(function (item, index) {

            //                        var selProd = $scope.productsList.filter(function (prod) {
            //                            //return prod.prodCode == item.productIDorName;//prodName
            //                            return prod.prodCode == item.productCode;

            //                        });

            //                        if (selProd && selProd != null && selProd.length > 0)
            //                        {
            //                            $scope.autofillProduct(item.productIDorName, index)
            //                            $scope.fillTextbox(selProd[0], index);
            //                        }

                                    

            //                    })
                                

            //                });



            //            })
            //    }
            //};





            //$scope.GetProductQuotationTemplate = function (productId, index) {

            //    var params = {
            //        "catitemid": productId,
            //        "sessionid": userService.getUserToken()
            //    };

            //    catalogService.GetProductQuotationTemplate(params)
            //        .then(function (response) {

            //            if (response) {
                            
            //                $scope.formRequest.listFwdRequirementItems[index].productQuotationTemplate = response;
            //                console.log($scope.formRequest.listFwdRequirementItems[index].productQuotationTemplate);

            //            } else {

            //            }

            //        })
            //};

            



            

        }]);