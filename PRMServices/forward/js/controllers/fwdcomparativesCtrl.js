﻿prmApp
    .controller('fwdcomparativesCtrl', ["$scope", "$state", "$log", "$stateParams", "userService", "auctionsService", "$window",
        "$timeout", "reportingService", "fwdreportingServices" ,
        function ($scope, $state, $log, $stateParams, userService, auctionsService, $window, $timeout, reportingService, fwdreportingServices) {
        $scope.reqId = $stateParams.reqID;
        $scope.isUOMDifferent = false;
        $scope.isCustomer = userService.getUserType() == "CUSTOMER" ? true : false;
        if (!$scope.isCustomer) {
            $state.go('home');
        };

        $scope.userId = userService.getUserId();
        $scope.sessionid = userService.getUserToken();
        $scope.listRequirementTaxes = [];
        $scope.listRequirementTaxeslength = $scope.listRequirementTaxes.length;
        $scope.uomDetails = [];

       

        $scope.handleUOMItems = function (item) {
            if (item) {
                item.reqVendors.forEach(function (vendor, index) {
                    var uomDetailsObj = {
                        itemId: item.itemID,
                        vendorId: vendor.vendorID,
                        containsUOMItem: false
                    }

                    if (vendor.quotationPrices) {
                        uomDetailsObj.containsUOMItem = item.productQuantityIn.toUpperCase() != vendor.quotationPrices.vendorUnits.toUpperCase();
                    }

                    $scope.uomDetails.push(uomDetailsObj);
                });
            }
        }

        $scope.containsUOMITems = function (vendorId) {
            var filteredUOMitems = _.filter($scope.uomDetails, function (uomItem) { return uomItem.vendorId == vendorId && uomItem.containsUOMItem == true; });
            if (filteredUOMitems && filteredUOMitems.length > 0) {
                return true;
            }

            return false;
        }

        $scope.ReqReportForExcel = [];
        $scope.GetReqReportForExcel = function () {
            fwdreportingServices.GetReqReportForExcel($scope.reqId, userService.getUserToken())
                .then(function (response) {
                    $scope.ReqReportForExcel = response;
                    //$scope.ReqReportForExcel.currentDate = new moment($scope.ReqReportForExcel.currentDate).format("DD-MM-YYYY HH:mm");
                    //$scope.ReqReportForExcel.postedOn = new moment($scope.ReqReportForExcel.postedOn).format("DD-MM-YYYY HH:mm");
                    //$scope.ReqReportForExcel.startTime = new moment($scope.ReqReportForExcel.startTime).format("DD-MM-YYYY HH:mm");
                    $scope.ReqReportForExcel.currentDate = userService.toLocalDate($scope.ReqReportForExcel.currentDate);
                    $scope.ReqReportForExcel.postedOn = userService.toLocalDate($scope.ReqReportForExcel.postedOn);
                    $scope.ReqReportForExcel.startTime = userService.toLocalDate($scope.ReqReportForExcel.startTime);
                    $scope.ReqReportForExcel.reqItems.forEach(function (item, index) {
                        $scope.handleUOMItems(item)
                    });
                });
        }

        $scope.GetReqReportForExcel();


        //$scope.getCustomerData = function (userId) {
        //    auctionsService.getrequirementdata({ "reqid": $scope.reqId, "sessionid": userService.getUserToken(), 'userid': userId })
        //        .then(function (response) {
        //            $scope.auctionItem = response;
        //            $scope.auctionItem.auctionVendors = _.filter($scope.auctionItem.auctionVendors, function (x) { return x.companyName !== 'PRICE_CAP'; });
        //            if ($scope.auctionItem.auctionVendors.length > 0) {
        //                $scope.getRequirementData();
        //            };

        //        });
        //}

        //$scope.getVendorData = function (item) {
        //    auctionsService.getrequirementdata({ "reqid": $scope.reqId, "sessionid": userService.getUserToken(), 'userid': item.vendorID })
        //        .then(function (response) {
        //            $scope.auctionItemVendor = response;
        //            item.auctionItemVendor = $scope.auctionItemVendor;
        //        });
        //}

        //$scope.getRequirementData = function () {            
        //    $scope.auctionItem.auctionVendors.forEach(function (item, index) {
        //        if (item.vendorID > 0) {
        //            item.auctionItemVendor = [];
        //            $scope.getVendorData(item);
        //        }
        //    });
        //}

        //$scope.getCustomerData($scope.userId);

        //$scope.auctionItem.auctionVendors.auctionItemVendor.auctionVendors[0].listRequirementItems.productIDorName;


        
        $scope.doPrint = false;

        $scope.printReport = function () {
            $scope.doPrint = true;
            $timeout(function () {
                $window.print();
                $scope.doPrint = false
            }, 1000);
        }


        $scope.htmlToCanvasSaveLoading = false;

        $scope.htmlToCanvasSave = function (format) {
            $scope.htmlToCanvasSaveLoading = true;
            setTimeout(function () {
                try {
                    var name = "Comparisions-ReqID-" + $scope.reqId + "." + format;
                    var canvas = document.createElement("canvas");
                    if (format == 'pdf') {
                        document.getElementById("widget").style["display"] = "";
                    }
                    else {
                        document.getElementById("widget").style["display"] = "inline-block";
                    }

                    html2canvas($("#widget"), {
                        onrendered: function (canvas) {
                            theCanvas = canvas;

                            const a = document.createElement("a");
                            a.style = "display: none";
                            a.href = canvas.toDataURL();

                            // Add Image to HTML
                            //document.body.appendChild(canvas);

                            /* Save As PDF */
                            if (format == 'pdf') {
                                var imgData = canvas.toDataURL();
                                var pdf = new jsPDF();
                                pdf.addImage(imgData, 'JPEG', 0, 0);
                                pdf.save(name);
                            }
                            else {
                                a.download = name;
                                a.click();
                            }

                            // Clean up 
                            //document.body.removeChild(canvas);
                            document.getElementById("widget").style["display"] = "";
                            $scope.htmlToCanvasSaveLoading = false;
                        }
                    });
                }
                catch (err) {
                    document.getElementById("widget").style["display"] = "";
                    $scope.htmlToCanvasSaveLoading = false;
                }
                finally {

                }
            }, 500);

        };

        $scope.getTotalTax = function (quotation) {
            if (quotation) {
                if (!quotation.cGst || quotation.cGst == undefined) {
                    quotation.cGst = 0;
                }

                if (!quotation.sGst || quotation.sGst == undefined) {
                    quotation.sGst = 0;
                }

                if (!quotation.iGst || quotation.iGst == undefined) {
                    quotation.iGst = 0;
                }

                return quotation.cGst + quotation.sGst + quotation.iGst;

            } else {
                return 0;
            }
        }

    }]);