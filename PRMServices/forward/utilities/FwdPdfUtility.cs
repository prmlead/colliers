﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Data;
//using System.IO;
//using PRMServices.Models;

//namespace PRMServices
//{
//    public class FwdPdfUtility
//    {

//        public static List<KeyValuePair<string, DataTable>> MakeDataTableForFwdRequirement(int reqID, FwdRequirement requirement, string quotationItems, UserDetails customer, string folderPath, int flag)
//        {
//            List<KeyValuePair<string, DataTable>> requirementItems = new List<KeyValuePair<string, DataTable>>();

//            #region Item Table
//            DataTable itemTable = new DataTable();

//            var otherBrand = 0;
//            var siteName = 0;

//            foreach (FwdRequirementItems Item in requirement.ListFwdRequirementItems)
//            {
//                if (!string.IsNullOrEmpty(Item.OthersBrands))
//                {
//                    otherBrand++;
//                }

//                if (!string.IsNullOrEmpty(Item.ProductDescription))
//                {
//                    siteName++;
//                }
//            }

//            itemTable.Columns.Add("MATERIAL DESCRIPTION");
//            itemTable.Columns.Add("ITEM NO");
//            itemTable.Columns.Add("QTY");
//            //itemTable.Columns.Add("PREFERRED BRAND");  Item.ProductBrand
//            if (siteName > 0)
//            {
//                itemTable.Columns.Add("SITE NAME");
//            }
//            if (otherBrand > 0)
//            {
                
//                foreach (FwdRequirementItems Item in requirement.ListFwdRequirementItems)
//                {
//                    if (requirement.IsTabular)
//                    {

//                        if (siteName > 0)
//                        {
//                            //itemTable.Rows.Add(Item.ProductIDorName, Item.ProductNo, Item.ProductQuantity + " " + Item.ProductQuantityIn, Item.ProductBrand, Item.ProductDescription);
//                            itemTable.Rows.Add(Item.ProductIDorName, Item.ProductNo, Item.ProductQuantity + " " + Item.ProductQuantityIn, Item.ProductDescription);
//                        }
//                        else
//                        {
//                            itemTable.Rows.Add(Item.ProductIDorName, Item.ProductNo, Item.ProductQuantity + " " + Item.ProductQuantityIn);
//                        }
                        
//                    }
//                }

//            }
//            else
//            {
//                foreach (FwdRequirementItems Item in requirement.ListFwdRequirementItems)
//                {
//                    if (requirement.IsTabular)
//                    {
//                        if (siteName > 0)
//                        {
//                            itemTable.Rows.Add(Item.ProductIDorName, Item.ProductNo, Item.ProductQuantity + " " + Item.ProductQuantityIn, Item.ProductDescription);
//                        }
//                        else
//                        {
//                            itemTable.Rows.Add(Item.ProductIDorName, Item.ProductNo, Item.ProductQuantity + " " + Item.ProductQuantityIn);
//                        }

                            
//                    }
//                }
//            }

//            requirementItems.Add(new KeyValuePair<string, DataTable>("ITEMS_TABLE", itemTable));
//            #endregion Item Table

//            #region Rfq Table

//            DataTable rfqsTable = new DataTable();
//            //Define columns
//            rfqsTable.Columns.Add("Company Name");
//            rfqsTable.Columns.Add(ExceptionHandler(customer.CompanyName.ToString()));


//            rfqsTable.Rows.Add("RFQ # Reference Number", ExceptionHandler(reqID.ToString()));
//            rfqsTable.Rows.Add("Auction Name", ExceptionHandler(requirement.Title.ToString()));
//            rfqsTable.Rows.Add("RFQ Issue Date", toLocal(DateTime.UtcNow));
//            rfqsTable.Rows.Add("Closed Bid Deadline", toLocal(requirement.QuotationFreezTime));
//            rfqsTable.Rows.Add("On-line Auction", toLocal(requirement.ExpStartTime));
//            rfqsTable.Rows.Add("Contact Person & Email Id", ExceptionHandler(requirement.GeneralTC.ToString()));
//            //rfqsTable.Rows.Add("Contact Person & Email Id", ExceptionHandler(requirement.CustFirstName.ToString() + " (" + customer.Email.ToString() + ")"));
//            rfqsTable.Rows.Add("Contact Number", ExceptionHandler(requirement.ContactDetails.ToString()));

//            requirementItems.Add(new KeyValuePair<string, DataTable>("RFQS_TABLE", rfqsTable));

//            #endregion Rfqs Table

//            #region terms Table

//            DataTable termsTable = new DataTable();

//            //Define columns
//            termsTable.Columns.Add("Location/Address");

//            if (requirement.DeliveryLocation == "Location/Address")
//            {
//                termsTable.Columns.Add(ExceptionHandler(requirement.DeliveryLocation) + ".");
//            }
//            else
//            {
//                termsTable.Columns.Add(ExceptionHandler(requirement.DeliveryLocation));
//            }

//            termsTable.Rows.Add("Payment Terms (in days)", ExceptionHandler(requirement.PaymentTerms.ToString()));
//            termsTable.Rows.Add("Delivery Terms", ExceptionHandler(requirement.DeliveryTime.ToString()));
//            //termsTable.Rows.Add("Contact Details", ExceptionHandler(requirement.ContactDetails.ToString()));
//            //termsTable.Rows.Add("General Terms & Condition", ExceptionHandler(requirement.GeneralTC.ToString()));


//            requirementItems.Add(new KeyValuePair<string, DataTable>("TERMS_TABLE", termsTable));

//            #endregion terms table

//            #region Contract Table

//            if (requirement.IsContract == true)
//            {
//                DataTable cntrctTable = new DataTable();

//                //Define columns
//                cntrctTable.Columns.Add("Contract Start Date");
//                cntrctTable.Columns.Add("Contract End Date");

//                cntrctTable.Rows.Add(ExceptionHandler(requirement.ContractStartTime.ToString()), ExceptionHandler(requirement.ContractEndTime.ToString()));

//                requirementItems.Add(new KeyValuePair<string, DataTable>("CNTRCT_TABLE", cntrctTable));
//            }

//            #endregion Contract Table

//            #region trans Table

//            DataTable transTable = new DataTable();

//            //Define columns
//            transTable.Columns.Add("Urgency");
//            transTable.Columns.Add(requirement.Urgency.ToString());


//            transTable.Rows.Add("Currency", requirement.Currency.ToString());

//            //Populate with friends :)

//            requirementItems.Add(new KeyValuePair<string, DataTable>("TRANS_TABLE", transTable));

//            #endregion trans Table

//            #region vendor Table

//            DataTable vendorsTable = new DataTable();

//            //Define columns
//            vendorsTable.Columns.Add("Name");
//            vendorsTable.Columns.Add("Company");
//            vendorsTable.Columns.Add("Email");
//            vendorsTable.Columns.Add("Phone");

//            foreach (VendorDetails vendor in requirement.AuctionVendors)
//            {
//                vendorsTable.Rows.Add(ExceptionHandler(vendor.VendorName),
//                ExceptionHandler(vendor.CompanyName),
//                ExceptionHandler(vendor.Vendor.Email),
//                ExceptionHandler(vendor.Vendor.PhoneNum));

//            }



//            requirementItems.Add(new KeyValuePair<string, DataTable>("VENDORS_TABLE", vendorsTable));

//            #endregion vendor Table


//            //Populate with requirementItems :)
//            return requirementItems;
//        }

//        public static void ExportDataTableToPdfForFwdRequirement(List<KeyValuePair<string, DataTable>> dtblTable, String strPdfPath, int reqId, FwdRequirement requirement, int flag)
//        {
//            System.IO.FileStream fs = new FileStream(strPdfPath, FileMode.Create, FileAccess.Write, FileShare.None);
//            Document document = new Document();
//            document.SetPageSize(iTextSharp.text.PageSize.A4);
//            PdfWriter writer = PdfWriter.GetInstance(document, fs);
//            document.Open();

//            BaseFont bfntHead = BaseFont.CreateFont(BaseFont.TIMES_ROMAN, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
//            BaseFont btnColumnHeader = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
//            BaseFont sideHeaders = BaseFont.CreateFont(BaseFont.HELVETICA_BOLD, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
//            var FontColour = new BaseColor(128, 128, 128);
//            var tabbleHeadColor = new BaseColor(17, 89, 128);
//            var tabbleEvenColor = new BaseColor(242, 242, 242);
//            var tableOddColor = new BaseColor(255, 255, 255);
//            var tabbleFontBodColor = new BaseColor(117, 125, 138);
//            var LineColour = new BaseColor(169, 169, 169);
//            var BlackColour = new BaseColor(0, 0, 0);

//            iTextSharp.text.Font myFont = new iTextSharp.text.Font(bfntHead, 10, iTextSharp.text.Font.NORMAL);

//            iTextSharp.text.Font myComments = new iTextSharp.text.Font(btnColumnHeader, 12, iTextSharp.text.Font.NORMAL);

//            Font fntQutBody = new Font(btnColumnHeader, 8, iTextSharp.text.Font.NORMAL, BlackColour);
//            Font fntHead = new Font(bfntHead, 20, iTextSharp.text.Font.NORMAL, FontColour);
//            Font sideHead = new Font(sideHeaders, 12, iTextSharp.text.Font.NORMAL, BlackColour);
//            Font commentHead = new Font(sideHeaders, 10, iTextSharp.text.Font.NORMAL, BlackColour);
//            Font fntColumnHeader = new Font(bfntHead, 10, iTextSharp.text.Font.NORMAL, tableOddColor);
//            Paragraph p = new Paragraph(new Chunk(new iTextSharp.text.pdf.draw.LineSeparator(0.0F, 100.0F, LineColour, Element.ALIGN_LEFT, 1)));
//            var spacerParagraph = new Paragraph();
//            spacerParagraph.SpacingBefore = 4f;
//            spacerParagraph.SpacingAfter = 0f;


//            Paragraph prgHeading = new Paragraph();
//            prgHeading.Alignment = Element.ALIGN_LEFT;
//            prgHeading.Add(new Chunk("Request for Quotation (RFQ)", fntHead));
//            document.Add(prgHeading);

//            //Add a line seperation

//            document.Add(p);
//            document.Add(spacerParagraph);
//            document.Add(spacerParagraph);

//            KeyValuePair<string, DataTable> rfqs_data = dtblTable.Where(item => item.Key == "RFQS_TABLE").FirstOrDefault();

//            //Write the table
//            //Table RFQ
//            PdfPTable tableRFQ = new PdfPTable(rfqs_data.Value.Columns.Count);
//            tableRFQ.DefaultCell.Padding = 3;
//            tableRFQ.WidthPercentage = 100;
//            tableRFQ.DefaultCell.Border = 0;
//            tableRFQ.HorizontalAlignment = Element.ALIGN_CENTER;
//            tableRFQ.DefaultCell.BorderColor = tableOddColor;

//            for (int i = 0; i < rfqs_data.Value.Columns.Count; i++)
//            {
//                PdfPCell cell = new PdfPCell();
//                cell.BackgroundColor = tabbleEvenColor;
//                cell.PaddingBottom = 8;
//                cell.Border = 0;
//                cell.VerticalAlignment = Element.ALIGN_CENTER;
//                cell.AddElement(new Chunk(rfqs_data.Value.Columns[i].ColumnName.ToString(), myFont));
//                tableRFQ.AddCell(cell);

//            }

//            var oddEvenOne = 1;
//            for (int i = 0; i < rfqs_data.Value.Rows.Count; i++)
//            {
//                if (oddEvenOne % 2 == 0)
//                {
//                    for (int j = 0; j < rfqs_data.Value.Columns.Count; j++)
//                    {
//                        PdfPCell tblcell = new PdfPCell();
//                        tblcell.BackgroundColor = tabbleEvenColor;
//                        tblcell.PaddingTop = 0;
//                        tblcell.PaddingBottom = 8;
//                        tblcell.Border = 0;
//                        tblcell.AddElement(new Chunk(rfqs_data.Value.Rows[i][j].ToString(), myFont));
//                        tableRFQ.AddCell(tblcell);
//                    }
//                }
//                else
//                {
//                    for (int j = 0; j < rfqs_data.Value.Columns.Count; j++)
//                    {
//                        PdfPCell tblcell = new PdfPCell();
//                        tblcell.BackgroundColor = tableOddColor;
//                        tblcell.PaddingTop = 0;
//                        tblcell.PaddingBottom = 8;
//                        tblcell.Border = 0;
//                        tblcell.AddElement(new Chunk(rfqs_data.Value.Rows[i][j].ToString(), myFont));
//                        tableRFQ.AddCell(tblcell);
//                    }
//                }

//                oddEvenOne++;
//            }

//            document.Add(tableRFQ);

//            document.Add(p);

//            document.Add(new Chunk("\n"));

//            Paragraph itemsHeading = new Paragraph();
//            itemsHeading.Alignment = Element.ALIGN_LEFT;
//            itemsHeading.Add(new Chunk("Product Details : ", sideHead));
//            document.Add(itemsHeading);

//            document.Add(spacerParagraph);

//            KeyValuePair<string, DataTable> items_data = dtblTable.Where(item => item.Key == "ITEMS_TABLE").FirstOrDefault();

//            //Table main

//            PdfPTable table = new PdfPTable(items_data.Value.Columns.Count);
//            float[] widths = new float[] { 40, 15, 15, 15, 15 };
//            if (items_data.Value.Columns.Count == 4)
//            {
//                widths = new float[] { 40, 20, 20, 20 };
//            }
//            else if(items_data.Value.Columns.Count == 3)
//            {
//                widths = new float[] { 40, 30, 30 };
//            }
//            table.SetWidths(widths);
//            table.DefaultCell.Padding = 8;
//            table.WidthPercentage = 100;
//            table.HorizontalAlignment = Element.ALIGN_CENTER;
//            table.DefaultCell.BorderColor = tabbleFontBodColor;

//            for (int i = 0; i < items_data.Value.Columns.Count; i++)
//            {
//                PdfPCell cell = new PdfPCell();
//                cell.BackgroundColor = tabbleHeadColor;
//                cell.PaddingBottom = 8;
//                cell.VerticalAlignment = Element.ALIGN_CENTER;
//                cell.AddElement(new Chunk(items_data.Value.Columns[i].ColumnName.ToUpper(), fntColumnHeader));
//                table.AddCell(cell);

//            }
//            //table Data
//            var oddEvenTwo = 1;
//            for (int i = 0; i < items_data.Value.Rows.Count; i++)
//            {
//                if (oddEvenTwo % 2 == 0)
//                {
//                    for (int j = 0; j < items_data.Value.Columns.Count; j++)
//                    {
//                        PdfPCell tblcell = new PdfPCell();
//                        tblcell.BackgroundColor = tabbleEvenColor;
//                        tblcell.PaddingTop = 0;
//                        tblcell.AddElement(new Chunk(items_data.Value.Rows[i][j].ToString(), myFont));
//                        table.AddCell(tblcell);
//                    }
//                }
//                else
//                {
//                    for (int j = 0; j < items_data.Value.Columns.Count; j++)
//                    {
//                        PdfPCell tblcell = new PdfPCell();
//                        tblcell.BackgroundColor = tableOddColor;
//                        tblcell.PaddingTop = 0;
//                        tblcell.AddElement(new Chunk(items_data.Value.Rows[i][j].ToString(), myFont));
//                        table.AddCell(tblcell);
//                    }
//                }

//                oddEvenTwo++;
//            }

//            document.Add(table);
//            document.Add(new Chunk("\n"));

//            Chunk glue = new Chunk(new VerticalPositionMark());
//            if (requirement.ReqComments.ToString() != String.Empty)
//            {
//                Paragraph mainComments = new Paragraph();
//                mainComments.Alignment = Element.ALIGN_LEFT;
//                mainComments.Add(new Chunk("Comments : " + requirement.ReqComments.ToString(), commentHead));
//                document.Add(mainComments);

//                document.Add(new Chunk("\n"));
//            }

//            Paragraph termsHeading = new Paragraph();
//            termsHeading.Alignment = Element.ALIGN_LEFT;
//            termsHeading.Add(new Chunk("Delivery & Payment Terms : ", sideHead));
//            document.Add(termsHeading);

//            document.Add(spacerParagraph);

//            KeyValuePair<string, DataTable> terms_data = dtblTable.Where(item => item.Key == "TERMS_TABLE").FirstOrDefault();

//            //Table Terms & Condition

//            PdfPTable tableTerms = new PdfPTable(terms_data.Value.Columns.Count);
//            float[] termsWidths = new float[] { 30, 70 };
//            tableTerms.SetWidths(termsWidths);
//            tableTerms.DefaultCell.Padding = 3;
//            tableTerms.WidthPercentage = 100;
//            tableTerms.DefaultCell.Border = 0;
//            tableTerms.HorizontalAlignment = Element.ALIGN_CENTER;
//            tableTerms.DefaultCell.BorderColor = tableOddColor;

//            for (int i = 0; i < terms_data.Value.Columns.Count; i++)
//            {
//                PdfPCell cell = new PdfPCell();
//                cell.BackgroundColor = tabbleEvenColor;
//                cell.PaddingBottom = 8;
//                cell.VerticalAlignment = Element.ALIGN_CENTER;
//                cell.AddElement(new Chunk(terms_data.Value.Columns[i].ColumnName.ToString(), myFont));
//                tableTerms.AddCell(cell);

//            }

//            var oddEvenThree = 1;
//            for (int i = 0; i < terms_data.Value.Rows.Count; i++)
//            {
//                if (oddEvenThree % 2 == 0)
//                {
//                    for (int j = 0; j < terms_data.Value.Columns.Count; j++)
//                    {
//                        PdfPCell tblcell = new PdfPCell();
//                        tblcell.BackgroundColor = tabbleEvenColor;
//                        tblcell.PaddingTop = 0;
//                        tblcell.PaddingBottom = 8;
//                        tblcell.AddElement(new Chunk(terms_data.Value.Rows[i][j].ToString(), myFont));
//                        tableTerms.AddCell(tblcell);
//                    }
//                }
//                else
//                {
//                    for (int j = 0; j < terms_data.Value.Columns.Count; j++)
//                    {
//                        PdfPCell tblcell = new PdfPCell();
//                        tblcell.BackgroundColor = tableOddColor;
//                        tblcell.PaddingTop = 0;
//                        tblcell.PaddingBottom = 8;
//                        tblcell.AddElement(new Chunk(terms_data.Value.Rows[i][j].ToString(), myFont));
//                        tableTerms.AddCell(tblcell);
//                    }
//                }

//                oddEvenThree++;
//            }

//            document.Add(tableTerms);

//            document.Add(new Chunk("\n"));


//            //Contract Details start

//            if (requirement.IsContract == true)
//            {

//                Paragraph cntrctHeading = new Paragraph();
//                cntrctHeading.Alignment = Element.ALIGN_LEFT;
//                cntrctHeading.Add(new Chunk("Contract Details : ", sideHead));
//                document.Add(cntrctHeading);

//                document.Add(spacerParagraph);

//                KeyValuePair<string, DataTable> cntrct_data = dtblTable.Where(item => item.Key == "CNTRCT_TABLE").FirstOrDefault();

//                //Table Terms & Condition

//                PdfPTable tableCntrct = new PdfPTable(cntrct_data.Value.Columns.Count);
//                float[] cntrctWidths = new float[] { 50, 50 };
//                tableCntrct.SetWidths(cntrctWidths);
//                tableCntrct.DefaultCell.Padding = 3;
//                tableCntrct.WidthPercentage = 100;
//                tableCntrct.DefaultCell.Border = 0;
//                tableCntrct.HorizontalAlignment = Element.ALIGN_CENTER;
//                tableCntrct.DefaultCell.BorderColor = tableOddColor;

//                for (int i = 0; i < cntrct_data.Value.Columns.Count; i++)
//                {
//                    PdfPCell cell = new PdfPCell();
//                    cell.BackgroundColor = tabbleHeadColor;
//                    cell.PaddingBottom = 8;
//                    cell.VerticalAlignment = Element.ALIGN_CENTER;
//                    cell.AddElement(new Chunk(cntrct_data.Value.Columns[i].ColumnName.ToString(), fntColumnHeader));
//                    tableCntrct.AddCell(cell);

//                }

//                for (int i = 0; i < cntrct_data.Value.Rows.Count; i++)
//                {
//                    for (int j = 0; j < cntrct_data.Value.Columns.Count; j++)
//                    {
//                        PdfPCell tblcell = new PdfPCell();
//                        tblcell.BackgroundColor = tabbleEvenColor;
//                        tblcell.PaddingTop = 0;
//                        tblcell.PaddingBottom = 8;
//                        tblcell.AddElement(new Chunk(cntrct_data.Value.Rows[i][j].ToString(), myFont));
//                        tableCntrct.AddCell(tblcell);
//                    }
//                }

//                document.Add(tableCntrct);

//                document.Add(new Chunk("\n"));
//            }

//            //Contract details end


//            Paragraph transHeading = new Paragraph();
//            transHeading.Alignment = Element.ALIGN_LEFT;
//            transHeading.Add(new Chunk("Transaction Configuration : ", sideHead));
//            document.Add(transHeading);

//            document.Add(spacerParagraph);

//            KeyValuePair<string, DataTable> trans_data = dtblTable.Where(item => item.Key == "TRANS_TABLE").FirstOrDefault();

//            PdfPTable tableTrans = new PdfPTable(trans_data.Value.Columns.Count);
//            tableTrans.SetWidths(termsWidths);
//            tableTrans.DefaultCell.Padding = 3;
//            tableTrans.WidthPercentage = 100;
//            tableTrans.HorizontalAlignment = Element.ALIGN_CENTER;
//            tableTrans.DefaultCell.BorderColor = tableOddColor;

//            for (int i = 0; i < trans_data.Value.Columns.Count; i++)
//            {
//                PdfPCell cell = new PdfPCell();
//                cell.BackgroundColor = tabbleEvenColor;
//                cell.PaddingBottom = 8;
//                cell.VerticalAlignment = Element.ALIGN_CENTER;
//                cell.AddElement(new Chunk(trans_data.Value.Columns[i].ColumnName.ToString(), myFont));
//                tableTrans.AddCell(cell);

//            }

//            var oddEvenFour = 1;
//            for (int i = 0; i < trans_data.Value.Rows.Count; i++)
//            {
//                if (oddEvenFour % 2 == 0)
//                {
//                    for (int j = 0; j < trans_data.Value.Columns.Count; j++)
//                    {
//                        PdfPCell tblcell = new PdfPCell();
//                        tblcell.BackgroundColor = tabbleEvenColor;
//                        tblcell.PaddingTop = 0;
//                        tblcell.PaddingBottom = 8;
//                        tblcell.AddElement(new Chunk(trans_data.Value.Rows[i][j].ToString(), myFont));
//                        tableTrans.AddCell(tblcell);
//                    }
//                }
//                else
//                {
//                    for (int j = 0; j < trans_data.Value.Columns.Count; j++)
//                    {
//                        PdfPCell tblcell = new PdfPCell();
//                        tblcell.BackgroundColor = tableOddColor;
//                        tblcell.PaddingTop = 0;
//                        tblcell.PaddingBottom = 8;
//                        tblcell.AddElement(new Chunk(trans_data.Value.Rows[i][j].ToString(), myFont));
//                        tableTrans.AddCell(tblcell);
//                    }
//                }

//                oddEvenFour++;
//            }

//            document.Add(tableTrans);

//            KeyValuePair<string, DataTable> vendors_data = dtblTable.Where(item => item.Key == "VENDORS_TABLE").FirstOrDefault();

//            if (flag == 1)
//            {

//                document.Add(new Chunk("\n"));

//                Paragraph vendorHeading = new Paragraph();
//                vendorHeading.Alignment = Element.ALIGN_LEFT;
//                vendorHeading.Add(new Chunk("Vendors Details : ", sideHead));
//                document.Add(vendorHeading);

//                document.Add(spacerParagraph);

//                PdfPTable tableVendors = new PdfPTable(vendors_data.Value.Columns.Count);

//                tableVendors.DefaultCell.Padding = 3;
//                tableVendors.WidthPercentage = 100;
//                tableVendors.HorizontalAlignment = Element.ALIGN_CENTER;
//                tableVendors.DefaultCell.BorderColor = tableOddColor;

//                for (int i = 0; i < vendors_data.Value.Columns.Count; i++)
//                {
//                    PdfPCell cell = new PdfPCell();
//                    cell.BackgroundColor = tabbleHeadColor;
//                    cell.PaddingBottom = 8;
//                    cell.VerticalAlignment = Element.ALIGN_CENTER;
//                    cell.AddElement(new Chunk(vendors_data.Value.Columns[i].ColumnName.ToString(), fntColumnHeader));
//                    tableVendors.AddCell(cell);

//                }

//                var oddEvenFive = 1;
//                for (int i = 0; i < vendors_data.Value.Rows.Count; i++)
//                {
//                    if (oddEvenFive % 2 == 0)
//                    {
//                        for (int j = 0; j < vendors_data.Value.Columns.Count; j++)
//                        {
//                            PdfPCell tblcell = new PdfPCell();
//                            tblcell.BackgroundColor = tabbleEvenColor;
//                            tblcell.PaddingTop = 0;
//                            tblcell.PaddingBottom = 8;
//                            tblcell.AddElement(new Chunk(vendors_data.Value.Rows[i][j].ToString(), myFont));
//                            tableVendors.AddCell(tblcell);
//                        }
//                    }
//                    else
//                    {
//                        for (int j = 0; j < vendors_data.Value.Columns.Count; j++)
//                        {
//                            PdfPCell tblcell = new PdfPCell();
//                            tblcell.BackgroundColor = tableOddColor;
//                            tblcell.PaddingTop = 0;
//                            tblcell.PaddingBottom = 8;
//                            tblcell.AddElement(new Chunk(vendors_data.Value.Rows[i][j].ToString(), myFont));
//                            tableVendors.AddCell(tblcell);
//                        }
//                    }

//                    oddEvenFive++;
//                }

//                document.Add(tableVendors);
//            }

//            document.Add(spacerParagraph);
//            document.Add(spacerParagraph);

//            Paragraph prgNote = new Paragraph();
//            prgNote.Alignment = Element.ALIGN_LEFT;
//            prgNote.Add(new Chunk("NOTE : This Document is Auto Generated from PRM360 System. ", fntQutBody));
//            document.Add(prgNote);

//            document.Close();
//            writer.Close();
//            fs.Close();
//        }

//        public static List<KeyValuePair<string, DataTable>> MakeDataTableFwdQuotation(List<FwdRequirementItems> quotationObject, int reqID, int customerID, int vendorID, string sessionID,
//            string warranty, string payment, string duration, string validity, double price, double tax, double freightcharges,
//            double vendorBidPrice, bool isTabular, int revised, string quotationTaxes, double discountAmount, string otherProperties,
//            string deliveryTermsDays, string deliveryTermsPercent, string paymentTermsDays, string paymentTermsPercent,
//            string paymentTermsType, string folderPath, string gstNumber, int isRegret,
//            string selectedVendorCurrency, double packingCharges, double packingChargesTaxPercentage, double packingChargesWithTax,
//            double revpackingCharges, double revpackingChargesWithTax, double installationCharges, double installationChargesTaxPercentage,
//            double installationChargesWithTax, double revinstallationCharges, double revinstallationChargesWithTax, double freightCharges,
//            double freightChargesTaxPercentage, double freightChargesWithTax, double revfreightCharges, double revfreightChargesWithTax,
//            string INCO_TERMS)
//        {

//            List<KeyValuePair<string, DataTable>> requirementItems = new List<KeyValuePair<string, DataTable>>();

//            PRMServices prmservices = new PRMServices();
//            PRMFwdReqService prmFwdService = new PRMFwdReqService();
//            FwdRequirement requirement = prmFwdService.GetRequirementDataOfflinePrivate(reqID, customerID, sessionID);
//            UserDetails customer = prmservices.GetUserDetails(customerID, sessionID);
//            UserDetails vendor = prmservices.GetUserDetails(vendorID, sessionID);

//            #region Item Table
//            //Define columns
//            DataTable itemTable = new DataTable();

//            itemTable.Columns.Add(" MATERIAL DESCRIPTION ");
//            itemTable.Columns.Add(" ITEM NO. ");
//            //itemTable.Columns.Add(" DESCRIPTION ");
//            //itemTable.Columns.Add(" BRAND ");
//            itemTable.Columns.Add(" QTY ");
//            //itemTable.Columns.Add(" COMMENTS ");
//            if (isRegret > 0)
//            {
//                itemTable.Columns.Add(" REGRET COMMENTS ");
//            }
//            itemTable.Columns.Add(" UNIT PRICE ");
//            itemTable.Columns.Add(" TAX (%)");

//            itemTable.Columns.Add(" ITEM PRICE ");


//            bool isUOMDiffertent = false;

//            foreach (FwdRequirementItems quotation in quotationObject)
//            {
//                double itemPrice = 0;
//                double unitPrice = 0;

//                if (revised == 0)
//                {
//                    itemPrice = quotation.ItemPrice;
//                    unitPrice = quotation.UnitPrice;
//                }
//                else if (revised == 1)
//                {
//                    itemPrice = quotation.RevItemPrice;
//                    unitPrice = quotation.RevUnitPrice;
//                }


//                double itemUnitDiscount = 0;
//                double itemCostPrice = 0;

//                if (revised == 1)
//                {
//                    quotation.RevItemPrice = quotation.RevisedItemPrice;
//                    itemUnitDiscount = quotation.RevUnitDiscount;

//                }
//                else if (revised == 0)
//                {
//                    itemUnitDiscount = quotation.UnitDiscount;
//                }

//                itemCostPrice = (quotation.UnitMRP * 100 * 100) / ((itemUnitDiscount * 100) + 10000 + (itemUnitDiscount * (quotation.CGst + quotation.SGst + quotation.IGst)) + ((quotation.CGst + quotation.SGst + quotation.IGst) * 100));

//                double Gst = (quotation.CGst + quotation.SGst + quotation.IGst);

//                if (quotation.IsRegret == true)
//                {
//                    itemPrice = 0;

//                    unitPrice = 0;
//                    quotation.CGst = 0;
//                    quotation.SGst = 0;
//                    quotation.IGst = 0;
//                    Gst = 0;
//                    quotation.UnitMRP = 0;
//                    itemUnitDiscount = 0;
//                }



//                if (quotation.ProductQuantityIn != quotation.VendorUnits)
//                {
//                    itemPrice = 0;
//                    isUOMDiffertent = true;
//                }

//                //quotationObject


//                string itemComments = string.Empty;

//                if (revised == 1)
//                {
//                    itemComments = quotation.ItemLevelRevComments;
//                }
//                else if (revised == 0)
//                {
//                    itemComments = quotation.ItemLevelInitialComments;
//                }

//                double freight = 0;
//                if (revised == 1)
//                {
//                    freight = quotation.ItemRevFreightCharges + ((quotation.ItemRevFreightCharges / 100) * quotation.ItemFreightTAX);
//                }
//                else
//                {
//                    freight = quotation.ItemFreightCharges + ((quotation.ItemFreightCharges / 100) * quotation.ItemFreightTAX);
//                }

//                if (isUOMDiffertent)
//                {
//                    vendorBidPrice = 0;
//                    price = 0;
//                }

//                if (revised == 1)
//                {
//                    packingCharges = revpackingCharges;
//                    packingChargesWithTax = revpackingChargesWithTax;
//                    installationCharges = revinstallationCharges;
//                    installationChargesWithTax = revinstallationChargesWithTax;
//                    freightCharges = revfreightCharges;
//                    freightChargesWithTax = revfreightChargesWithTax;

//                }

//                if (isRegret == 0)
//                {
//                    itemTable.Rows.Add(quotation.ProductIDorName,
//                        quotation.ProductNo,
//                        //quotation.ProductDescription,
//                        //quotation.ProductBrand,
//                        quotation.ProductQuantity + " " + quotation.ProductQuantityIn,
//                        //itemComments,
//                        unitPrice,
//                        Gst,
//                        itemPrice);
//                }
//                else if (isRegret > 0)
//                {
//                    itemTable.Rows.Add(quotation.ProductIDorName,
//                        quotation.ProductNo,
//                        //quotation.ProductDescription,
//                        //quotation.ProductBrand,
//                        quotation.ProductQuantity + " " + quotation.ProductQuantityIn,
//                        //itemComments,
//                        quotation.RegretComments,
//                        unitPrice,
//                        Gst,
//                        itemPrice);
//                }
//            }
//            if (isRegret == 0)
//            {
//                itemTable.Rows.Add("/*//*/",
//                    "/*//*/",
//                    //"/*//*/",
//                    //"/*//*/",
//                    "/*//*/",
//                    //"/*//*/",
//                    "/*//*/",
//                    "Total Item Price",
//                    price);
//                if (packingCharges != 0)
//                {
//                    itemTable.Rows.Add("/*//*/",
//                        "/*//*/",
//                        //"/*//*/", 
//                        //"/*//*/",
//                        //"/*//*/",
//                        "Packing & Forward Charges",
//                        packingCharges,
//                        packingChargesTaxPercentage,
//                        packingChargesWithTax);
//                }
//                if (installationCharges != 0)
//                {
//                    itemTable.Rows.Add("/*//*/",
//                        "/*//*/",
//                        //"/*//*/", 
//                        //"/*//*/",
//                        //"/*//*/",
//                        "De-Installation Charges",
//                        installationCharges,
//                        installationChargesTaxPercentage,
//                        installationChargesWithTax);
//                }
//                if (freightCharges != 0)
//                {
//                    itemTable.Rows.Add("/*//*/",
//                        "/*//*/",
//                        //"/*//*/", 
//                        //"/*//*/",
//                        //"/*//*/",
//                        "Freight Charges",
//                        freightCharges,
//                        freightChargesTaxPercentage,
//                        freightChargesWithTax);
//                }
//                itemTable.Rows.Add("/*//*/",
//                    "/*//*/",
//                    //"/*//*/", 
//                    //"/*//*/",
//                    //"/*//*/",
//                    "/*//*/",
//                    "/*//*/",
//                    "Grand Total in " + selectedVendorCurrency,
//                    vendorBidPrice);
//            }
//            else if (isRegret > 0)
//            {
//                itemTable.Rows.Add("/*//*/",
//                    "/*//*/",
//                    //"/*//*/", 
//                    //"/*//*/",
//                    //"/*//*/",
//                    "/*//*/",
//                    "/*//*/",
//                    "/*//*/",
//                    "Total Item Price",
//                    price);
//                if (packingCharges != 0)
//                {
//                    itemTable.Rows.Add("/*//*/",
//                        "/*//*/",
//                        //"/*//*/", 
//                        //"/*//*/",
//                        //"/*//*/",
//                        "/*//*/",
//                        "Packing & Forward Charges",
//                        packingCharges,
//                        packingChargesTaxPercentage,
//                        packingChargesWithTax);
//                }
//                if (installationCharges != 0)
//                {
//                    itemTable.Rows.Add("/*//*/",
//                        "/*//*/",
//                        //"/*//*/", 
//                        //"/*//*/",
//                        //"/*//*/",
//                        "/*//*/",
//                        "De-Installation Charges",
//                        installationCharges,
//                        installationChargesTaxPercentage,
//                        installationChargesWithTax);
//                }
//                if (freightCharges != 0)
//                {
//                    itemTable.Rows.Add("/*//*/",
//                        "/*//*/",
//                        //"/*//*/", 
//                        //"/*//*/",
//                        //"/*//*/",
//                        "/*//*/",
//                        "Freight Charges",
//                        freightCharges,
//                        freightChargesTaxPercentage,
//                        freightChargesWithTax);
//                }
//                itemTable.Rows.Add("/*//*/",
//                    "/*//*/",
//                    //"/*//*/", 
//                    //"/*//*/",
//                    //"/*//*/",
//                    "/*//*/",
//                    "/*//*/",
//                    "/*//*/",
//                    "Grand Total in " + selectedVendorCurrency,
//                    vendorBidPrice);
//            }

//            requirementItems.Add(new KeyValuePair<string, DataTable>("ITEMS_TABLE", itemTable));

//            #endregion Item Table

//            #region terms & condition

//            DataTable termsTable = new DataTable();

//            //Define columns
//            termsTable.Columns.Add(" Warranty ");
//            if (string.IsNullOrEmpty(warranty))
//            {
//                termsTable.Columns.Add(" N ");
//            }
//            else
//            {
//                if (warranty == "Warranty")
//                {
//                    termsTable.Columns.Add(ExceptionHandler(warranty) + ".");
//                }
//                else
//                {
//                    termsTable.Columns.Add(ExceptionHandler(warranty));
//                }
//            }

//            if (!string.IsNullOrEmpty(otherProperties))
//            {
//                termsTable.Rows.Add(" Other Properties ", ExceptionHandler(otherProperties.ToString()));
//            }
//            if (!string.IsNullOrEmpty(payment))
//            {
//                termsTable.Rows.Add(" Payment Terms ", ExceptionHandler(payment.ToString()));
//            }
//            if (!string.IsNullOrEmpty(duration))
//            {
//                termsTable.Rows.Add(" Delivery Terms ", ExceptionHandler(duration.ToString()));
//            }
//            if (!string.IsNullOrEmpty(INCO_TERMS))
//            {
//                termsTable.Rows.Add(" Inco Terms ", ExceptionHandler(INCO_TERMS.ToString()));
//            }

//            requirementItems.Add(new KeyValuePair<string, DataTable>("TERMS_TABLE", termsTable));

//            #endregion terms & condition


//            return requirementItems;
//        }

//        public static void ExportDataTableFwdQuotationToPdf(List<KeyValuePair<string, DataTable>> dtblTable, String strPdfPath, int reqId, int customerID,
//            int vendorID, string sessionID, string duration, string gstNumber, string validity, int revised, string title, int reqIDFromJs)
//        {

//            PRMServices prmservices = new PRMServices();
//            PRMFwdReqService prmFwdService = new PRMFwdReqService();
//            FwdRequirement requirement = prmFwdService.GetRequirementDataOfflinePrivate(reqIDFromJs, customerID, sessionID);
//            UserDetails customer = prmservices.GetUserDetails(customerID, sessionID);
//            UserDetails vendor = prmservices.GetUserDetails(vendorID, sessionID);

//            System.IO.FileStream fs = new FileStream(strPdfPath, FileMode.Create, FileAccess.Write, FileShare.None);
//            Document document = new Document();
//            KeyValuePair<string, DataTable> items_data = dtblTable.Where(item => item.Key == "ITEMS_TABLE").FirstOrDefault();
//            if (items_data.Value.Columns.Count <= 8)
//            {
//                document.SetPageSize(iTextSharp.text.PageSize.A4);
//            }
//            else
//            {

//                document.SetPageSize(iTextSharp.text.PageSize.A4.Rotate());
//            }
//            PdfWriter writer = PdfWriter.GetInstance(document, fs);
//            document.Open();


//            BaseFont bfntHead = BaseFont.CreateFont(BaseFont.TIMES_ROMAN, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
//            BaseFont btnColumnHeader = BaseFont.CreateFont(BaseFont.TIMES_ROMAN, BaseFont.CP1250, BaseFont.NOT_EMBEDDED);
//            BaseFont sideHeaders = BaseFont.CreateFont(BaseFont.HELVETICA_BOLD, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
//            var FontColour = new BaseColor(128, 128, 128);
//            var tabbleHeadColor = new BaseColor(17, 89, 128);
//            var tabbleEvenColor = new BaseColor(242, 242, 242);
//            var tableOddColor = new BaseColor(255, 255, 255);
//            var tabbleFontBodColor = new BaseColor(117, 125, 138);
//            var LineColour = new BaseColor(169, 169, 169);
//            var BlackColour = new BaseColor(0, 0, 0);
//            Font fntHead = new Font(bfntHead, 18, iTextSharp.text.Font.NORMAL, FontColour);
//            Font sideHead = new Font(sideHeaders, 12, iTextSharp.text.Font.NORMAL, BlackColour);
//            Font tblHead = new Font(btnColumnHeader, 8, iTextSharp.text.Font.NORMAL, tableOddColor);
//            Font tblBody = new Font(btnColumnHeader, 8, iTextSharp.text.Font.NORMAL, BlackColour);
//            Font tblBodyBold = new Font(sideHeaders, 8, iTextSharp.text.Font.NORMAL, BlackColour);
//            Font fntColumnHeader = new Font(btnColumnHeader, 8, iTextSharp.text.Font.NORMAL, tableOddColor);
//            Font fntQutBody = new Font(btnColumnHeader, 8, iTextSharp.text.Font.NORMAL, BlackColour);
//            Paragraph p = new Paragraph(new Chunk(new iTextSharp.text.pdf.draw.LineSeparator(0.0F, 100.0F, LineColour, Element.ALIGN_LEFT, 1)));
//            p.SpacingBefore = -1f;
//            var spacerParagraph = new Paragraph();
//            spacerParagraph.SpacingBefore = 3f;
//            spacerParagraph.SpacingAfter = 0f;
//            Chunk glue = new Chunk(new VerticalPositionMark());
//            Paragraph mainHead = new Paragraph(vendor.CompanyName.ToString(), fntHead);
//            mainHead.Add(new Chunk(glue));

//            if (revised > 0)
//            {
//                mainHead.Add("Revised Price Quotation");
//            }
//            else
//            {
//                mainHead.Add("Price Quotation");
//            }
//            mainHead.SpacingAfter = 2f;
//            document.Add(mainHead);

//            document.Add(p);
//            document.Add(spacerParagraph);

//            Paragraph rfqTitle = new Paragraph();
//            rfqTitle.Alignment = Element.ALIGN_LEFT;
//            rfqTitle.Add(new Chunk("Auction Name : " + title, sideHead));
//            document.Add(rfqTitle);

//            document.Add(spacerParagraph);
//            document.Add(spacerParagraph);

//            Paragraph QutHead = new Paragraph("Quotation For : " + reqIDFromJs, sideHead);
//            QutHead.Add(new Chunk(glue));
//            QutHead.Add(" Dated On : " + toLocal(DateTime.UtcNow));
//            document.Add(QutHead);

//            Paragraph QutName = new Paragraph("Name : " + (customer.FirstName + " " + customer.LastName).ToString(), fntQutBody);
//            QutName.Add(new Chunk(glue));
//            QutName.Add("Phone : " + vendor.PhoneNum.ToString());
//            document.Add(QutName);

//            Paragraph QutCmpny = new Paragraph("Company : " + customer.CompanyName.ToString(), fntQutBody);
//            QutCmpny.Add(new Chunk(glue));
//            QutCmpny.Add("Prepared by : " + (vendor.FirstName + " " + vendor.LastName).ToString());
//            document.Add(QutCmpny);

//            Paragraph QutAdd = new Paragraph("Address : " + customer.Address.ToString(), fntQutBody);
//            QutAdd.Add(new Chunk(glue));
//            QutAdd.Add("E-mail : " + vendor.Email.ToString());
//            document.Add(QutAdd);

//            Paragraph QutPhone = new Paragraph("Phone : " + customer.PhoneNum.ToString(), fntQutBody);
//            QutPhone.Add(new Chunk(glue));
//            QutPhone.Add("Gst Number : " + gstNumber.ToString());//gstNumber.ToString()
//            document.Add(QutPhone);

//            document.Add(p);
//            document.Add(spacerParagraph);

//            Paragraph prodheading = new Paragraph(" Product Details : ", sideHead);
//            prodheading.Add(new Chunk(glue));
//            prodheading.Add("Validity : " + validity.ToString());
//            document.Add(prodheading);

//            document.Add(spacerParagraph);
//            KeyValuePair<string, DataTable> items_total_data = dtblTable.Where(item => item.Key == "ITEMS_TABLE_TOTAL").FirstOrDefault();

//            float[] widths = new float[] { 20, 20, 15, 10, 15, 10, 10 };
//            PdfPTable tableItems = new PdfPTable(items_data.Value.Columns.Count);

//            if (items_data.Value.Columns.Count == 6)
//            {
//                widths = new float[] { 16, 10, 22, 12, 16, 12 };
//            }
//            if (items_data.Value.Columns.Count == 8)
//            {
//                widths = new float[] { 12, 8, 20, 10, 13, 9, 9, 9 };
//            }
//            if (items_data.Value.Columns.Count == 9)
//            {
//                widths = new float[] { 12, 8, 20, 10, 13, 9, 9, 9, 10 };
//            }
//            else if (items_data.Value.Columns.Count == 10)
//            {
//                widths = new float[] { 11, 7, 18, 10, 10, 8, 8, 9, 10, 9 };
//            }
//            else if (items_data.Value.Columns.Count == 11)
//            {
//                widths = new float[] { 11, 8, 19, 8, 5, 8, 8, 4, 9, 10, 10 };
//            }
//            else if (items_data.Value.Columns.Count == 12)
//            {
//                widths = new float[] { 11, 7, 16, 8, 5, 7, 7, 7, 5, 9, 10, 8 };
//            }
//            tableItems.SetWidths(widths);
//            tableItems.DefaultCell.Padding = 3;
//            tableItems.WidthPercentage = 100;
//            tableItems.HorizontalAlignment = Element.ALIGN_CENTER;
//            tableItems.DefaultCell.BorderColor = tableOddColor;

//            var columnCount = items_data.Value.Columns.Count - 1;
//            for (int i = 0; i < items_data.Value.Columns.Count; i++)
//            {
//                PdfPCell cell = new PdfPCell();
//                cell.BackgroundColor = tabbleHeadColor;
//                cell.PaddingBottom = 8;
//                cell.VerticalAlignment = Element.ALIGN_CENTER;
//                cell.AddElement(new Chunk(items_data.Value.Columns[i].ColumnName.ToString(), tblHead));
//                tableItems.AddCell(cell);

//            }

//            var oddEvenFive = 1;
//            for (int i = 0; i < items_data.Value.Rows.Count; i++)
//            {
//                var incColspan = 1;
//                if (oddEvenFive % 2 == 0)
//                {
//                    for (int j = 0; j < items_data.Value.Columns.Count; j++)
//                    {
//                        if (items_data.Value.Rows[i][j].ToString() != "/*//*/")
//                        {
//                            PdfPCell tblcell = new PdfPCell();
//                            tblcell.BackgroundColor = tabbleEvenColor;
//                            tblcell.PaddingTop = 0;
//                            tblcell.PaddingBottom = 8;
//                            tblcell.Colspan = incColspan;
//                            tblcell.VerticalAlignment = Element.ALIGN_CENTER;
//                            if (items_data.Value.Rows.Count == oddEvenFive)
//                            {
//                                tblcell.AddElement(new Chunk(items_data.Value.Rows[i][j].ToString(), tblBodyBold));
//                            }
//                            else
//                            {
//                                tblcell.AddElement(new Chunk(items_data.Value.Rows[i][j].ToString(), tblBody));
//                            }

//                            tableItems.AddCell(tblcell);
//                            incColspan = 1;
//                        }
//                        else
//                        {
//                            incColspan++;
//                        }
//                    }
//                }
//                else
//                {
//                    for (int j = 0; j < items_data.Value.Columns.Count; j++)
//                    {
//                        if (items_data.Value.Rows[i][j].ToString() != "/*//*/")
//                        {
//                            PdfPCell tblcell = new PdfPCell();
//                            tblcell.BackgroundColor = tableOddColor;
//                            tblcell.PaddingTop = 0;
//                            tblcell.PaddingBottom = 8;
//                            tblcell.Colspan = incColspan;
//                            //tblcell.Colspan = items_data.Value.Columns.Count;
//                            tblcell.VerticalAlignment = Element.ALIGN_CENTER;
//                            if (items_data.Value.Rows.Count == oddEvenFive)
//                            {
//                                tblcell.AddElement(new Chunk(items_data.Value.Rows[i][j].ToString(), tblBodyBold));
//                            }
//                            else
//                            {
//                                tblcell.AddElement(new Chunk(items_data.Value.Rows[i][j].ToString(), tblBody));
//                            }
//                            tableItems.AddCell(tblcell);
//                            incColspan = 1;
//                        }
//                        else
//                        {
//                            incColspan++;
//                        }
//                    }
//                }

//                oddEvenFive++;
//            }

//            document.Add(tableItems);

//            document.Add(spacerParagraph);
//            document.Add(spacerParagraph);
//            document.Add(spacerParagraph);

//            Paragraph prgheading = new Paragraph();
//            prgheading.Alignment = Element.ALIGN_LEFT;
//            prgheading.Add(new Chunk(" TERMS AND CONDITIONS : ", sideHead));
//            document.Add(prgheading);


//            document.Add(spacerParagraph);

//            KeyValuePair<string, DataTable> terms_data = dtblTable.Where(item => item.Key == "TERMS_TABLE").FirstOrDefault();

//            //Table Terms & Condition

//            PdfPTable tableTerms = new PdfPTable(terms_data.Value.Columns.Count);
//            float[] widthsTerms = new float[] { 30, 70 };
//            tableTerms.SetWidths(widthsTerms);
//            tableTerms.DefaultCell.Padding = 3;
//            tableTerms.WidthPercentage = 100;
//            tableTerms.DefaultCell.Border = 0;
//            tableTerms.HorizontalAlignment = Element.ALIGN_CENTER;
//            tableTerms.DefaultCell.BorderColor = tableOddColor;

//            for (int i = 0; i < terms_data.Value.Columns.Count; i++)
//            {
//                PdfPCell cell = new PdfPCell();
//                cell.BackgroundColor = tabbleEvenColor;
//                cell.PaddingBottom = 8;
//                cell.Border = 0;
//                cell.VerticalAlignment = Element.ALIGN_CENTER;
//                cell.AddElement(new Chunk(terms_data.Value.Columns[i].ColumnName.ToString(), tblBody));
//                tableTerms.AddCell(cell);

//            }

//            var oddEvenThree = 1;
//            for (int i = 0; i < terms_data.Value.Rows.Count; i++)
//            {
//                if (oddEvenThree % 2 == 0)
//                {
//                    for (int j = 0; j < terms_data.Value.Columns.Count; j++)
//                    {
//                        PdfPCell tblcell = new PdfPCell();
//                        tblcell.BackgroundColor = tabbleEvenColor;
//                        tblcell.PaddingTop = 0;
//                        tblcell.PaddingBottom = 8;
//                        tblcell.Border = 0;
//                        tblcell.AddElement(new Chunk(terms_data.Value.Rows[i][j].ToString(), tblBody));
//                        tableTerms.AddCell(tblcell);
//                    }
//                }
//                else
//                {
//                    for (int j = 0; j < terms_data.Value.Columns.Count; j++)
//                    {
//                        PdfPCell tblcell = new PdfPCell();
//                        tblcell.BackgroundColor = tableOddColor;
//                        tblcell.PaddingTop = 0;
//                        tblcell.PaddingBottom = 8;
//                        tblcell.Border = 0;
//                        tblcell.AddElement(new Chunk(terms_data.Value.Rows[i][j].ToString(), tblBody));
//                        tableTerms.AddCell(tblcell);
//                    }
//                }

//                oddEvenThree++;
//            }

//            document.Add(tableTerms);

//            document.Add(spacerParagraph);
//            document.Add(spacerParagraph);

//            Paragraph prgNote = new Paragraph();
//            prgNote.Alignment = Element.ALIGN_LEFT;
//            prgNote.Add(new Chunk("NOTE : This Document is Auto Generated from PRM360 System. ", fntQutBody));
//            document.Add(prgNote);

//            document.Add(spacerParagraph);
//            document.Add(spacerParagraph);
//            document.Add(spacerParagraph);

//            Paragraph prgThanks = new Paragraph();
//            prgThanks.Alignment = Element.ALIGN_CENTER;
//            prgThanks.Add(new Chunk(" THANK YOU FOR YOUR BUSINESS! ", sideHead));
//            document.Add(prgThanks);

//            document.Close();
//            writer.Close();
//            fs.Close();

//        }

//        public static string ExceptionHandler(string value)
//        {
//            if (string.IsNullOrEmpty(value))
//            {
//                value = " ";
//            }
//            return value;
//        }

//        //public static string dateFormatChange(DateTime? value)
//        //{

//        //    DateTime s1 = System.Convert.ToDateTime(value);
//        //    DateTime date = (s1);
//        //    String frmdt = date.ToString("dd/MM/yyyy  hh:mm:ss tt");
//        //    return frmdt + " (UTC +0)";
//        //}

//        public static DateTime toLocal(DateTime? date)
//        {
//            DateTime convertedDate = DateTime.SpecifyKind(DateTime.Parse(date.ToString()), DateTimeKind.Utc);
//            var kind = convertedDate.Kind;
//            DateTime dt = convertedDate.ToLocalTime();
//            return dt;
//        }


//        private void PdfUtilities_Load(object sender, EventArgs e)
//        {

//        }
//    }
//}