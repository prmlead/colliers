﻿using System;
using System.IO;
using System.Data;
using System.Linq;
using System.Web;
using System.Configuration;
using System.Collections.Generic;
using System.ServiceModel.Activation;
using PRMServices.Common;
using PRMServices.Models;
using PdfSharp.Pdf;
using TheArtOfDev.HtmlRenderer.PdfSharp;
using PRMServices.SQLHelper;
using CORE = PRM.Core.Common;
using System.Text.RegularExpressions;
using SendGrid.Helpers.Mail;
using System.Globalization;
using System.Net;
using PRMServices.PRMService.Neuland.POPDF;
using System.Text;
using PRM.Core.Common;
using System.ServiceModel.Web;
using Newtonsoft.Json;
using System.Net.Mail;
using System.Threading.Tasks;
using System.Dynamic;
using Attachment = System.Net.Mail.Attachment;
using Newtonsoft.Json.Linq;

namespace PRMServices
{
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class PRMPOService : IPRMPOService
    {
        private IDatabaseHelper sqlHelper = DatabaseProvider.GetDatabaseProvider();
        private NLog.Logger logger = NLog.LogManager.GetCurrentClassLogger();


        public string GetBillRequests(int COMP_ID, int U_ID, int IS_CUSTOMER, string sessionid, string poNumber,
            string projectIds,string billsStatus, string packageIds, string subPackageIds, string vendorIds,
            string fromDate, string toDate, int PageSize = 0, int NumberOfRecords = 0)
        {
            string json = null;
            Utilities.ValidateSession(sessionid);

            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            sd.Add("P_COMP_ID", COMP_ID);
            sd.Add("P_U_ID", U_ID);
            sd.Add("P_IS_CUSTOMER", IS_CUSTOMER);
            sd.Add("P_PO_NUMBER", poNumber);
            sd.Add("P_PROJECT_IDS", projectIds);
            sd.Add("P_BILL_STATUS", billsStatus);
            sd.Add("P_PACKAGE_IDS", packageIds);
            sd.Add("P_SUB_PACKAGE_IDS", subPackageIds);
            sd.Add("P_VENDOR_IDS", vendorIds);
            sd.Add("P_FROM_DATE", fromDate);
            sd.Add("P_TO_DATE", toDate);
            sd.Add("P_PAGE", PageSize);
            sd.Add("P_PAGE_SIZE", NumberOfRecords);
            DataSet dataSet = sqlHelper.SelectList("po_GetBillRequests", sd);
            if (dataSet != null && dataSet.Tables.Count > 0 && dataSet.Tables[0].Rows.Count > 0)
            {
                json = JsonConvert.SerializeObject(dataSet, Formatting.Indented);
            }

            return json;
        }

        public Response CreateBillSubmission(List<BillRequests> billRequestList, int BILL_ID, string sessionid)
        {
            Response response = new Response();
            PRMServices prm = new PRMServices();
            Utilities.ValidateSession(sessionid);
            foreach (BillRequests billRequest in billRequestList) 
            {
                List<CheckList> checkListArr = new List<CheckList>();
                var checkLists = Newtonsoft.Json.JsonConvert.DeserializeObject<List<CheckList>>(billRequest.CHECK_LIST_JSON);
                if (checkLists != null && checkLists.Count > 0)
                {
                    foreach (CheckList cl in checkLists)
                    {
                        CheckList checkList = new CheckList();
                        checkList.isChecked = cl.isChecked;
                        checkList.value = cl.value;
                        checkList.MULTIPLE_ATTACHMENTS = new List<FileUpload>();
                        if (cl != null && cl.MULTIPLE_ATTACHMENTS != null && cl.MULTIPLE_ATTACHMENTS.Count > 0)
                        {
                            foreach (FileUpload fd in cl.MULTIPLE_ATTACHMENTS)
                            {
                                var newFileObj = new FileUpload();
                                //fd.FileName = Regex.Replace(fd.FileName, @"[^0-9a-zA-Z.]+", "PRM_");
                                if (fd.FileID <= 0)
                                {
                                    var attachName = string.Empty;
                                    long tick = DateTime.UtcNow.Ticks;
                                    attachName = HttpContext.Current.Server.MapPath(Utilities.FILE_URL + fd.FileName);
                                    prm.SaveFile(attachName, fd.FileStream);
                                    attachName = fd.FileName;
                                    Response res = prm.SaveAttachment(attachName);
                                    if (res.ErrorMessage != "")
                                    {
                                        response.ErrorMessage = res.ErrorMessage;
                                    }

                                    fd.FileID = res.ObjectID;
                                    newFileObj.FileID = fd.FileID;
                                    newFileObj.FileName = fd.FileName;
                                    newFileObj.FileStream = null;
                                }
                                else if (fd.FileID > 0)
                                {
                                    var fileData = prm.DownloadFile(Convert.ToString(fd.FileID), sessionid);
                                    newFileObj.FileID = fd.FileID;
                                    newFileObj.FileName = fd.FileName;
                                    newFileObj.FileStream = null;
                                }
                                checkList.MULTIPLE_ATTACHMENTS.Add(newFileObj);
                            }
                        }
                        checkListArr.Add(checkList);
                    }
                }
                billRequest.CHECK_LIST_JSON = Newtonsoft.Json.JsonConvert.SerializeObject(checkListArr);
                try
                {
                    CORE.DataNamesMapper<BillRequests> mapper = new CORE.DataNamesMapper<BillRequests>();
                    SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                    sd.Add("P_ROW_ID", billRequest.ROW_ID);
                    sd.Add("P_BILL_ID", BILL_ID);
                    sd.Add("P_V_COMP_ID", billRequest.V_COMP_ID);
                    sd.Add("P_C_COMP_ID", billRequest.C_COMP_ID);
                    sd.Add("P_PROJECT_ID", billRequest.PROJECT_ID);
                    sd.Add("P_PACKAGE_ID", billRequest.PACKAGE_ID);
                    sd.Add("P_SUB_PACKAGE_ID", billRequest.SUB_PACKAGE_ID);
                    sd.Add("P_PO_NUMBER", billRequest.PO_NUMBER);
                    sd.Add("P_PO_VALUE", billRequest.PO_VALUE);
                    sd.Add("P_BILL_REQUEST_AMOUNT", billRequest.BILL_REQUEST_AMOUNT);
                    //sd.Add("P_BILL_CERTIFIED_AMOUNT", billRequest.BILL_CERTIFIED_AMOUNT);
                    sd.Add("P_BILL_ATTACHMENTS", string.Empty);
                    sd.Add("P_U_ID", billRequest.CREATED_BY);
                    sd.Add("P_PO_ID", billRequest.PO_ID);
                    sd.Add("P_CHECK_LIST_JSON", billRequest.CHECK_LIST_JSON);
                    sd.Add("P_BILL_TYPE", billRequest.BILL_TYPE);
                    sd.Add("P_IS_VALID", billRequest.IS_VALID);
                    DataSet dataset = sqlHelper.SelectList("po_SaveBillSubmission", sd);
                    if (dataset != null && dataset.Tables.Count > 0 && dataset.Tables[0].Rows.Count > 0 && dataset.Tables[0].Rows[0][0] != null)
                    {
                        response.ObjectID = dataset.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToInt32(dataset.Tables[0].Rows[0][0].ToString()) : -1;
                        response.ErrorMessage = dataset.Tables[0].Rows[0][1] != DBNull.Value ? Convert.ToString(dataset.Tables[0].Rows[0][1].ToString()) : string.Empty;

                        if (BILL_ID <= 0 && dataset.Tables.Count > 0 && dataset.Tables[0].Rows.Count > 0)
                        {
                            BILL_ID = Convert.ToInt32(dataset.Tables[0].Rows[0]["BILL_ID"]);
                        }

                    }
                }
                catch (Exception ex)
                {
                    response.ErrorMessage = ex.Message;
                }
            }
            
            return response;
        }

        public Response SubmitForBillCertification(BillRequests billRequest, string sessionid)
        {
            Response response = new Response();

            try
            {
                Utilities.ValidateSession(sessionid);
                CORE.DataNamesMapper<BillRequests> mapper = new CORE.DataNamesMapper<BillRequests>();
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                sd.Add("P_BILL_ID", billRequest.BILL_ID);
                sd.Add("P_V_COMP_ID", billRequest.V_COMP_ID);
                DataSet dataset = sqlHelper.SelectList("po_SaveBillSubmission", sd);
                if (dataset != null && dataset.Tables.Count > 0 && dataset.Tables[0].Rows.Count > 0 && dataset.Tables[0].Rows[0][0] != null)
                {
                    response.ObjectID = dataset.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToInt32(dataset.Tables[0].Rows[0][0].ToString()) : -1;
                    response.ErrorMessage = dataset.Tables[0].Rows[0][1] != DBNull.Value ? Convert.ToString(dataset.Tables[0].Rows[0][1].ToString()) : string.Empty;
                }
            }
            catch (Exception ex)
            {
                response.ErrorMessage = ex.Message;
            }

            return response;
        }

        public Response UploadBillCertification(BillRequests billCertificates, string sessionid)
        {
            Response response = new Response();

            PRMServices prm = new PRMServices();
            string fileName = string.Empty;
            //if (billCertificates.MULTIPLE_ATTACHMENTS_BILL_CERTIFICATION != null && billCertificates.MULTIPLE_ATTACHMENTS_BILL_CERTIFICATION.Count > 0)
            //{
            //    foreach (FileUpload fd in billCertificates.MULTIPLE_ATTACHMENTS_BILL_CERTIFICATION)
            //    {
            //        fd.FileName = Regex.Replace(fd.FileName, @"[^0-9a-zA-Z.]+", "PRM_");
            //        if (fd.FileStream.Length > 0 && !string.IsNullOrEmpty(fd.FileName))
            //        {
            //            var attachName = string.Empty;
            //            long tick = DateTime.UtcNow.Ticks;
            //            attachName = HttpContext.Current.Server.MapPath(Utilities.FILE_URL + fd.FileName);
            //            prm.SaveFile(attachName, fd.FileStream);
            //            attachName = fd.FileName;
            //            Response res = prm.SaveAttachment(attachName);
            //            if (res.ErrorMessage != "")
            //            {
            //                response.ErrorMessage = res.ErrorMessage;
            //            }

            //            fd.FileID = res.ObjectID;
            //        }
            //        else if (fd.FileID > 0)
            //        {
            //            var fileData = prm.DownloadFile(Convert.ToString(fd.FileID), sessionid);
            //        }
            //        fileName += Convert.ToString(fd.FileID) + ",";
            //    }
            //    fileName = fileName.Substring(0, fileName.Length - 1);
            //}
            List<CheckList> checkListArr = new List<CheckList>();
            var checkLists = Newtonsoft.Json.JsonConvert.DeserializeObject<List<CheckList>>(billCertificates.CHECK_LIST_JSON);
            if (checkLists != null && checkLists.Count > 0)
            {
                foreach (CheckList cl in checkLists)
                {
                    CheckList checkList = new CheckList();
                    checkList.isChecked = cl.isChecked;
                    checkList.value = cl.value;
                    checkList.MULTIPLE_ATTACHMENTS = new List<FileUpload>();
                    if (cl != null && cl.MULTIPLE_ATTACHMENTS != null && cl.MULTIPLE_ATTACHMENTS.Count > 0)
                    {
                        foreach (FileUpload fd in cl.MULTIPLE_ATTACHMENTS)
                        {
                            var newFileObj = new FileUpload();
                            //fd.FileName = Regex.Replace(fd.FileName, @"[^0-9a-zA-Z.]+", "PRM_");
                            if (fd.FileID <= 0)
                            {
                                var attachName = string.Empty;
                                long tick = DateTime.UtcNow.Ticks;
                                attachName = HttpContext.Current.Server.MapPath(Utilities.FILE_URL + fd.FileName);
                                prm.SaveFile(attachName, fd.FileStream);
                                attachName = fd.FileName;
                                Response res = prm.SaveAttachment(attachName);
                                if (res.ErrorMessage != "")
                                {
                                    response.ErrorMessage = res.ErrorMessage;
                                }

                                fd.FileID = res.ObjectID;
                                newFileObj.FileID = fd.FileID;
                                newFileObj.FileName = fd.FileName;
                                newFileObj.FileStream = null;
                            }
                            else if (fd.FileID > 0)
                            {
                                var fileData = prm.DownloadFile(Convert.ToString(fd.FileID), sessionid);
                                newFileObj.FileID = fd.FileID;
                                newFileObj.FileName = fd.FileName;
                                newFileObj.FileStream = null;
                            }
                            checkList.MULTIPLE_ATTACHMENTS.Add(newFileObj);
                        }
                    }
                    checkListArr.Add(checkList);
                }
            }
            billCertificates.CHECK_LIST_JSON = Newtonsoft.Json.JsonConvert.SerializeObject(checkListArr);

            try
            {
                Utilities.ValidateSession(sessionid);
                CORE.DataNamesMapper<BillRequests> mapper = new CORE.DataNamesMapper<BillRequests>();
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                sd.Add("P_BILL_ID", billCertificates.BILL_ID);
                sd.Add("P_PO_NUMBER", billCertificates.PO_NUMBER);
                sd.Add("P_U_ID", billCertificates.MODIFIED_BY);
                sd.Add("P_BILL_CERTIFICATES", string.Empty);
                sd.Add("P_CHECK_LIST_JSON", billCertificates.CHECK_LIST_JSON);
                sd.Add("P_BILL_CERTIFIED_AMOUNT", billCertificates.BILL_CERTIFIED_AMOUNT);
                sd.Add("P_BILL_CERTIFICATE_WF_ID", billCertificates.BILL_CERTIFICATE_WF_ID);
                DataSet dataset = sqlHelper.SelectList("po_UploadBillCertification", sd);
                if (dataset != null && dataset.Tables.Count > 0 && dataset.Tables[0].Rows.Count > 0 && dataset.Tables[0].Rows[0][0] != null)
                {
                    response.ObjectID = dataset.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToInt32(dataset.Tables[0].Rows[0][0].ToString()) : -1;
                    response.ErrorMessage = dataset.Tables[0].Rows[0][1] != DBNull.Value ? Convert.ToString(dataset.Tables[0].Rows[0][1].ToString()) : string.Empty;
                    billCertificates.BILL_CERTIFICATE_WF_ID = dataset.Tables[0].Rows[0][2] != DBNull.Value ? Convert.ToInt32(dataset.Tables[0].Rows[0][2].ToString()) : -1;

                    if (response.ObjectID > 0 && billCertificates.BILL_CERTIFICATE_WF_ID > 0 && response.ErrorMessage =="")
                    {
                        PRMWFService pRMWF = new PRMWFService();
                        Response res2 = pRMWF.AssignWorkflow(billCertificates.BILL_CERTIFICATE_WF_ID, response.ObjectID, billCertificates.MODIFIED_BY, sessionid);
                    }
                }
            }
            catch (Exception ex)
            {
                response.ErrorMessage = ex.Message;
            }

            return response;
        }

        public Response AcknowledgeOrRejectBillRequest(BillRequests billSubmission, string sessionid)
        {
            Response response = new Response();

            try
            {
                Utilities.ValidateSession(sessionid);
                CORE.DataNamesMapper<BillRequests> mapper = new CORE.DataNamesMapper<BillRequests>();
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                sd.Add("P_PO_NUMBER", billSubmission.PO_NUMBER);
                sd.Add("P_BILL_ID", billSubmission.BILL_ID);
                sd.Add("P_U_ID", billSubmission.MODIFIED_BY);
                sd.Add("P_BILL_REQUEST_STATUS", billSubmission.BILL_REQUEST_STATUS);
                DataSet dataset = sqlHelper.SelectList("po_AckOrRejBillRequest", sd);
                if (dataset != null && dataset.Tables.Count > 0 && dataset.Tables[0].Rows.Count > 0 && dataset.Tables[0].Rows[0][0] != null)
                {
                    response.ObjectID = dataset.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToInt32(dataset.Tables[0].Rows[0][0].ToString()) : -1;
                    response.ErrorMessage = dataset.Tables[0].Rows[0][1] != DBNull.Value ? Convert.ToString(dataset.Tables[0].Rows[0][1].ToString()) : string.Empty;
                }
            }
            catch (Exception ex)
            {
                response.ErrorMessage = ex.Message;
            }

            return response;
        }

        public string GetPOScheduleDetails(int COMP_ID, string PO_NUMBER, int VALIDATE, string sessionid)
        {
            string json = null;
            Utilities.ValidateSession(sessionid);

            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            sd.Add("P_COMP_ID", COMP_ID);
            sd.Add("P_PO_NUMBER", PO_NUMBER);
            sd.Add("P_VALIDATE", VALIDATE);
            DataSet dataSet = sqlHelper.SelectList("po_GetPOScheduleDetails", sd);
            if (dataSet != null && dataSet.Tables.Count > 0 && dataSet.Tables[0].Rows.Count > 0)
            {
                json = JsonConvert.SerializeObject(dataSet, Formatting.Indented);
            }

            return json;
        }

        public string GetInvoiceDetails(int COMP_ID, string INVOICE_ID, string sessionid)
        {
            string json = null;
            Utilities.ValidateSession(sessionid);

            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            sd.Add("P_COMP_ID", COMP_ID);
            sd.Add("P_INVOICE_ID", INVOICE_ID);           
            DataSet dataSet = sqlHelper.SelectList("po_GetInvoiceDetails", sd);
            if (dataSet != null && dataSet.Tables.Count > 0 && dataSet.Tables[0].Rows.Count > 0)
            {
                json = JsonConvert.SerializeObject(dataSet, Formatting.Indented);
            }

            return json;
        }

        public string GetInvoiceAmountForPO(int U_ID,int COMP_ID, string PO_NUMBER, string sessionid)
        {
            string json = null;
            Utilities.ValidateSession(sessionid);

            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            sd.Add("P_U_ID", U_ID);
            sd.Add("P_COMP_ID", COMP_ID);
            sd.Add("P_PO_NUMBER", PO_NUMBER);
            DataSet dataSet = sqlHelper.SelectList("po_GetInvoiceAmountForPONumber", sd);
            if (dataSet != null && dataSet.Tables.Count > 0 && dataSet.Tables[0].Rows.Count > 0)
            {
                json = JsonConvert.SerializeObject(dataSet, Formatting.Indented);
            }

            return json;
        }

        public Response AmendPODetails(List<POItem> poAmendItemsDetails, int poAmendmentID, string sessionId) 
        {
            Response details = new Response();
            PRMServices prm = new PRMServices();

            try
            {
                Utilities.ValidateSession(sessionId);
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };

                foreach (POItem poitem in poAmendItemsDetails) 
                {

                    var attachmentArr = new List<FileUpload>();
                    string jsonString = null;
                    var attachments = Newtonsoft.Json.JsonConvert.DeserializeObject<List<FileUpload>>(poitem.MULTIPLE_ATTACHMENTS);
                    if (attachments != null && attachments.Count > 0)
                    {
                        foreach (FileUpload fd in attachments)
                        {
                            var newFileObj = new FileUpload();
                            if (fd.FileID <= 0)
                            {
                                var attachName = string.Empty;
                                long tick = DateTime.UtcNow.Ticks;
                                attachName = HttpContext.Current.Server.MapPath(Utilities.FILE_URL + fd.FileName);
                                prm.SaveFile(attachName, fd.FileStream);
                                attachName = fd.FileName;
                                Response res = prm.SaveAttachment(attachName);
                                if (res.ErrorMessage != "")
                                {
                                    details.ErrorMessage = res.ErrorMessage;
                                }

                                fd.FileID = res.ObjectID;
                                newFileObj.FileID = fd.FileID;
                                newFileObj.FileName = fd.FileName;
                                newFileObj.FileStream = null;
                            }
                            else if (fd.FileID > 0)
                            {
                                var fileData = prm.DownloadFile(Convert.ToString(fd.FileID), sessionId);
                                newFileObj.FileID = fd.FileID;
                                newFileObj.FileName = fd.FileName;
                                newFileObj.FileStream = null;
                            }
                            attachmentArr.Add(newFileObj);
                        }
                        jsonString = Newtonsoft.Json.JsonConvert.SerializeObject(attachmentArr);
                    }

                    sd = new SortedDictionary<object, object>() { };
                    sd.Add("P_ROW_ID", poitem.ROW_ID);
                    sd.Add("P_PO_AMENDMENT_ID", poAmendmentID);
                    sd.Add("P_PO_SCH_ROW_ID", poitem.PO_SCH_ROW_ID);
                    sd.Add("P_PO_NUMBER", poitem.PO_NUMBER);
                    sd.Add("P_PROJECT_ID", poitem.PROJECT_ID);
                    sd.Add("P_PACKAGE_ID", poitem.PACKAGE_ID);
                    sd.Add("P_SUB_PACKAGE_ID", poitem.SUB_PACKAGE_ID);
                    sd.Add("P_RATE", poitem.UNIT_PRICE);
                    sd.Add("P_TAX_AMOUNT", poitem.TAX_AMOUNT);
                    sd.Add("P_CGST", poitem.CGST);
                    sd.Add("P_SGST", poitem.SGST);
                    sd.Add("P_IGST", poitem.IGST);
                    sd.Add("P_ORDER_QTY", 1);
                    sd.Add("P_DESCRIPTION", poitem.DESCRIPTION);
                    sd.Add("P_VENDOR_ID", poitem.VENDOR_ID);
                    sd.Add("P_VENDOR_CODE", poitem.VENDOR_CODE);
                    sd.Add("P_UOM", poitem.UOM);
                    sd.Add("P_PO_STATUS", poitem.PO_STATUS);
                    sd.Add("P_PO_TYPE", poitem.PO_TYPE);
                    sd.Add("P_PAYMENT_TERMS", poitem.PAYMENT_TERMS);
                    sd.Add("P_IS_PO_AMENDMENT_DONE", poitem.IS_PO_AMENDMENT_DONE ? 1 : 0);
                    sd.Add("P_C_COMP_ID", poitem.CUSTOMER_COMP_ID);
                    sd.Add("P_V_COMP_ID", poitem.VENDOR_COMP_ID);
                    sd.Add("P_U_ID", poitem.CUSTOMER_USER_ID);
                    sd.Add("P_DELIVERY_DATE", poitem.DELIVERY_DATE);
                    sd.Add("P_WF_ID", poitem.WF_ID);
                    sd.Add("P_MULTIPLE_ATTACHMENTS", jsonString);
                    sd.Add("P_TOTAL_AMOUNT", poitem.TOTAL_AMOUNT);
                    sd.Add("P_OVERALL_AMOUNT", poitem.TOTAL_AMOUNT + poitem.TAX_AMOUNT);
                    var dataset = sqlHelper.SelectList("po_updatePOAmendment", sd);

                    if (dataset != null && dataset.Tables.Count > 0 && dataset.Tables[0].Rows.Count > 0 && dataset.Tables[0].Rows[0][0] != null)
                    {
                        details.ObjectID = dataset.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToInt32(dataset.Tables[0].Rows[0][0].ToString()) : -1;
                        poAmendmentID = details.ObjectID;
                        details.ErrorMessage = dataset.Tables[0].Rows[0][1] != DBNull.Value ? Convert.ToString(dataset.Tables[0].Rows[0][1].ToString()) : string.Empty;
                    }

                    if (poAmendmentID <= 0 && dataset.Tables.Count > 0 && dataset.Tables[0].Rows.Count > 0 && string.IsNullOrEmpty(details.ErrorMessage))
                    {
                        poAmendmentID = Convert.ToInt32(dataset.Tables[0].Rows[0]["PO_AMENDMENT_ID"]);
                    }

                }

                if (details.ObjectID > 0 && string.IsNullOrEmpty(details.ErrorMessage))
                {
                    PRMWFService pRMWF = new PRMWFService();
                    Response res2 = pRMWF.AssignWorkflow(poAmendItemsDetails[0].WF_ID, Convert.ToInt32(details.ObjectID), poAmendItemsDetails[0].CUSTOMER_USER_ID, sessionId, "PO_AMENDMENT");
                }
            }
            catch (Exception ex)
            {
                details.ErrorMessage = ex.Message;
            }


            return details;
        }

        public Response UpdatePORevisionNumber(string poNumber, List<POItem> poDetails, string sessionId)
        {
            Response details = new Response();
            PRMServices prm = new PRMServices();

            try
            {
                Utilities.ValidateSession(sessionId);
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                sd.Add("P_PO_NUMBER", poNumber);
                sd.Add("P_PO_AMENDMENT_ID", Convert.ToInt32(poDetails.FirstOrDefault().PO_AMENDMENT_ID));
                DataSet dataset = sqlHelper.SelectList("po_UpdatePORevisionNumber", sd);

                if (dataset != null && dataset.Tables.Count > 0 && dataset.Tables[0].Rows.Count > 0 && dataset.Tables[0].Rows[0][0] != null)
                {
                    details.ErrorMessage = dataset.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToString(dataset.Tables[0].Rows[0][0].ToString()) : string.Empty;
                }

                if (poDetails !=null && poDetails.Count > 0) 
                {
                    foreach (var po in poDetails) 
                    {
                        po.PO_NUMBER = details.ErrorMessage;
                    }
                    SavePODetailsNew(poDetails, sessionId);
                }
            }
            catch (Exception ex)
            {
                details.ErrorMessage = ex.Message;
            }


            return details;
        }
        public string GetBillRequestsFilterValues(int COMP_ID, int U_ID, int IS_CUSTOMER, string FROM_DATE, string TO_DATE, string sessionid)
        {
            string json = null;
            Utilities.ValidateSession(sessionid);

            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            sd.Add("P_COMP_ID", COMP_ID);
            sd.Add("P_U_ID", U_ID);
            sd.Add("P_IS_CUSTOMER", IS_CUSTOMER);
            sd.Add("P_FROM_DATE", FROM_DATE);
            sd.Add("P_TO_DATE", TO_DATE);
            DataSet dataSet = sqlHelper.SelectList("po_GetBillRequestFilterValues", sd);
            if (dataSet != null && dataSet.Tables.Count > 0 && dataSet.Tables[0].Rows.Count > 0)
            {
                json = JsonConvert.SerializeObject(dataSet, Formatting.Indented);
            }

            return json;
        }

        public string GetInvoiceFilterValues(int COMP_ID, int U_ID, int IS_CUSTOMER, string fromDate, string toDate, string sessionid)
        {
            string json = null;
            Utilities.ValidateSession(sessionid);

            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            sd.Add("P_COMP_ID", COMP_ID);
            sd.Add("P_U_ID", U_ID);
            sd.Add("P_IS_CUSTOMER", IS_CUSTOMER);
            sd.Add("P_FROM_DATE", fromDate);
            sd.Add("P_TO_DATE", toDate);
            DataSet dataSet = sqlHelper.SelectList("po_GetInvoiceFilterValues", sd);
            if (dataSet != null && dataSet.Tables.Count > 0 && dataSet.Tables[0].Rows.Count > 0)
            {
                json = JsonConvert.SerializeObject(dataSet, Formatting.Indented);
            }

            return json;
        }

        public List<POItem> GetPOList(int compId, int vendorId, int projectId, string poNumber,
            string projectIds, string packageIds, string costCenters, string vendorIds, string searchKeyword, string fromdate, string todate,
            int page, int size, string sessionId)
        {
            List<POItem> details = new List<POItem>();
            try
            {
                if (string.IsNullOrEmpty(fromdate))
                {
                    fromdate = DateTime.UtcNow.AddDays(-30).ToString("yyyy-MM-dd");
                }

                if (string.IsNullOrEmpty(todate))
                {
                    todate = DateTime.UtcNow.AddDays(1).ToString("yyyy-MM-dd");
                }

                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                Utilities.ValidateSession(sessionId);
                sd.Add("P_COMP_ID", compId);
                sd.Add("P_VENDOR_ID", vendorId);
                sd.Add("P_PO_NUMBER", poNumber);
                sd.Add("P_PROJECT_IDS", projectIds);
                sd.Add("P_PACKAGE_IDS", packageIds);
                sd.Add("P_COST_CENTERS", costCenters);
                sd.Add("P_VENDOR_IDS", vendorIds);
                sd.Add("P_SEARCH", searchKeyword);
                sd.Add("P_FROM_DATE", fromdate);
                sd.Add("P_TO_DATE", todate);
                sd.Add("P_PAGE", page);
                sd.Add("P_PAGE_SIZE", size);
                CORE.DataNamesMapper<POItem> mapper = new CORE.DataNamesMapper<POItem>();
                DataSet ds = sqlHelper.SelectList("cp_GetPOList", sd);
                details = mapper.Map(ds.Tables[0]).ToList();

                if (details != null && details.Count > 0)
                {
                    List<int> compIds = new List<int>();
                    foreach (var item in details)
                    {
                        if (!compIds.Contains(item.VENDOR_COMP_ID))
                        {
                            compIds.Add(item.VENDOR_COMP_ID);
                        }
                    }

                    DataTable dt = sqlHelper.SelectQuery($"SELECT * FROM companygstinfo WHERE COMP_ID IN ({string.Join(",", compIds)}) order by DATE_CREATED desc");
                    if (dt != null && dt.Rows.Count > 0)
                    {
                        CORE.DataNamesMapper<CompanyGST> mapper1 = new CORE.DataNamesMapper<CompanyGST>();
                        var userGSTInfo = mapper1.Map(dt).ToList();
                        if (userGSTInfo != null && userGSTInfo.Count > 0)
                        {
                            foreach (var item in details)
                            {
                                if (userGSTInfo.Any(c => c.COMP_ID == item.VENDOR_COMP_ID))
                                {
                                    item.GST_NUMBER = userGSTInfo.First(c => c.COMP_ID == item.VENDOR_COMP_ID).GST_NUMBER;
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                details.Clear();
                details.Add(new POItem() { ErrorMessage = ex.Message });
            }

            return details;
        }

        public List<POItem> GetPODetails(string poNumber, string sessionId)
        {
            List<POItem> details = new List<POItem>();
            try
            {
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                Utilities.ValidateSession(sessionId);
                sd.Add("P_PO_NUMBER", poNumber);
                CORE.DataNamesMapper<POItem> mapper = new CORE.DataNamesMapper<POItem>();
                DataSet ds = sqlHelper.SelectList("cp_GetPODetails", sd);
                details = mapper.Map(ds.Tables[0]).ToList();

                if (details != null && details.Count > 0)
                {
                    List<int> compIds = new List<int>();
                    foreach (var item in details)
                    {
                        if (!compIds.Contains(item.VENDOR_COMP_ID))
                        {
                            compIds.Add(item.VENDOR_COMP_ID);
                        }
                    }

                    DataTable dt = sqlHelper.SelectQuery($"SELECT * FROM companygstinfo WHERE COMP_ID IN ({string.Join(",", compIds)}) order by DATE_CREATED desc");
                    if (dt != null && dt.Rows.Count > 0)
                    {
                        CORE.DataNamesMapper<CompanyGST> mapper1 = new CORE.DataNamesMapper<CompanyGST>();
                        var userGSTInfo = mapper1.Map(dt).ToList();
                        if (userGSTInfo != null && userGSTInfo.Count > 0)
                        {
                            foreach (var item in details)
                            {
                                if (userGSTInfo.Any(c => c.COMP_ID == item.VENDOR_COMP_ID))
                                {
                                    item.VENDOR_GST_NUMBER = userGSTInfo.First(c => c.COMP_ID == item.VENDOR_COMP_ID).GST_NUMBER;
                                }
                            }
                        }
                    }
                }

                if (details != null && details.Count > 0)
                {
                    sd = new SortedDictionary<object, object>() { };
                    sd.Add("P_PO_NUMBER", poNumber);
                    DataSet ds1 = sqlHelper.SelectList("cp_GetPOPDFDetails", sd);

                    if (ds1 != null && ds1.Tables.Count > 0 && ds1.Tables[0].Rows.Count > 0)
                    {
                        var row = ds1.Tables[0].Rows[0];
                        details[0].PO_PDF_JSON = row["PO_PDF_JSON"] != DBNull.Value ? Convert.ToString(row["PO_PDF_JSON"]) : string.Empty;
                    }
                }
            }
            catch (Exception ex)
            {
                details.Clear();
                details.Add(new POItem() { ErrorMessage = ex.Message });
            }

            return details;
        }


        public List<POItem> GetPOAmendmentDetails(string poNumber, int PO_AMENDMENT_ID, string sessionId)
        {
            List<POItem> details = new List<POItem>();
            try
            {
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                Utilities.ValidateSession(sessionId);
                sd.Add("P_PO_NUMBER", poNumber);
                sd.Add("P_PO_AMENDMENT_ID", PO_AMENDMENT_ID);
                CORE.DataNamesMapper<POItem> mapper = new CORE.DataNamesMapper<POItem>();
                DataSet ds = sqlHelper.SelectList("cp_GetPOAmendmentDetails", sd);
                details = mapper.Map(ds.Tables[0]).ToList();
                
            }
            catch (Exception ex)
            {
                details.Clear();
                details.Add(new POItem() { ErrorMessage = ex.Message });
            }

            return details;
        }

        private Response SavePODetailsNew(List<POItem> details, string sessionId)
        {
            Response response = new Response();

            try
            {
                foreach (var detail in details)
                {
                    SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                    sd.Add("P_PROJECT_ID", detail.PROJECT_ID);
                    sd.Add("P_PACKAGE_ID", detail.PACKAGE_ID);
                    sd.Add("P_SUB_PACKAGE_ID", detail.SUB_PACKAGE_ID);
                    sd.Add("P_DESCRIPTION", detail.DESCRIPTION);
                    sd.Add("P_VENDOR_ID", detail.VENDOR_ID);
                    sd.Add("P_PO_NUMBER", detail.PO_NUMBER);
                    //sd.Add("P_PO_AMEND_TAX_AMOUNT", detail.PO_AMEND_TAX_AMOUNT);
                    //sd.Add("P_PO_AMEND_TOTAL_AMOUNT", detail.PO_AMEND_TOTAL_AMOUNT);
                    //sd.Add("P_PO_AMEND_OVERALL_AMOUNT", detail.PO_AMEND_OVERALL_AMOUNT);
                    DataSet dataset = sqlHelper.SelectList("cp_SavePODetailsNew", sd);

                    if (dataset != null && dataset.Tables.Count > 0 && dataset.Tables[0].Rows.Count > 0 && dataset.Tables[0].Rows[0][0] != null)
                    {
                        response.ObjectID = dataset.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToInt32(dataset.Tables[0].Rows[0][0]) : 0;
                    }
                }

                SortedDictionary<object, object> sd1 = new SortedDictionary<object, object>() { };
                sd1.Add("P_PROJECT_ID", details[0].PROJECT_ID);
                DataSet dataset1 = sqlHelper.SelectList("cp_UpdateQCSPOValue", sd1);
            }
            catch (Exception ex)
            {
                response.ErrorMessage = ex.Message;
            }

            return response;
        }


        public Response SavePODetails(List<POItem> details, string sessionId)
        {
            Response response = new Response();

            try
            {
                DateTime currentTime = DateTime.UtcNow;
                var uniqueVendors = details.Select(v => v.VENDOR_ID).Distinct();
                foreach (var vendorId in uniqueVendors)
                {
                    //string poNumber = $"COLLIERS-PO-{vendorId}-" + DateTime.Now.Ticks.ToString();
                    string poNumber = $"PO/{DateTime.Now.Year}/";
                    var vendorItems = details.Where(i => i.VENDOR_ID == vendorId);
                    if (vendorItems != null && vendorItems.Count() > 0)
                    {
                        int poSerialNumber = 1;
                        string projectCode = "";
                        int projectId = vendorItems.First().PROJECT_ID;
                        string projectDetails = $"SELECT TOP 1 PROJ_ID, PO_SERIAL_NUMBER FROM Projects WHERE PROJECT_ID = {projectId}";
                        DataTable projectDetailsDT = sqlHelper.SelectQuery(projectDetails);
                        if (projectDetailsDT != null && projectDetailsDT.Rows.Count > 0)
                        {
                            poSerialNumber = projectDetailsDT.Rows[0]["PO_SERIAL_NUMBER"] != DBNull.Value ? Convert.ToInt16(projectDetailsDT.Rows[0]["PO_SERIAL_NUMBER"]) : 1;
                            projectCode = projectDetailsDT.Rows[0]["PROJ_ID"] != DBNull.Value ? Convert.ToString(projectDetailsDT.Rows[0]["PROJ_ID"]) : "";
                            poSerialNumber = poSerialNumber + 1;
                            poNumber = poNumber + projectCode + "/" + (poSerialNumber);

                            sqlHelper.ExecuteNonQuery_IUD($"UPDATE Projects SET PO_SERIAL_NUMBER = {poSerialNumber} WHERE PROJECT_ID = {projectId}");
                        }

                        foreach (var detail in vendorItems)
                        {
                            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                            sd.Add("P_ROW_ID", detail.ROW_ID);
                            sd.Add("P_PROJECT_ID", detail.PROJECT_ID);
                            sd.Add("P_C_COMP_ID", detail.CUSTOMER_COMP_ID);
                            sd.Add("P_PO_NUMBER", poNumber);
                            sd.Add("P_PACKAGE_ID", detail.PACKAGE_ID);
                            sd.Add("P_SUB_PACKAGE_ID", detail.SUB_PACKAGE_ID);
                            sd.Add("P_DESCRIPTION", detail.DESCRIPTION);
                            sd.Add("P_PO_DESCRIPTION", detail.PO_DESCRIPTION);
                            sd.Add("P_VENDOR_ID", detail.VENDOR_ID);
                            sd.Add("P_VENDOR_CODE", detail.VENDOR_CODE);
                            sd.Add("P_DELV_DATE", currentTime);
                            sd.Add("P_UNIT", detail.UOM);
                            sd.Add("P_QUANTITY", 1);
                            sd.Add("P_RATE", detail.UNIT_PRICE);
                            sd.Add("P_TOTAL_AMOUNT", detail.UNIT_PRICE * detail.ORDER_QTY);
                            sd.Add("P_CGST", detail.CGST);
                            sd.Add("P_SGST", detail.SGST);
                            sd.Add("P_IGST", detail.IGST);
                            decimal tax = (detail.CGST + detail.SGST + detail.IGST);
                            var taxAmount = (detail.UNIT_PRICE * detail.ORDER_QTY) * (tax / 100);
                            sd.Add("P_TAX_AMOUNT", taxAmount);
                            sd.Add("P_OVERALL_AMOUNT", (detail.UNIT_PRICE * detail.ORDER_QTY) + taxAmount);
                            sd.Add("P_USER", (detail.CUSTOMER_USER_ID));
                            sd.Add("P_PAYMENT_TERMS", detail.PAYMENT_TERMS);
                            sd.Add("P_IS_FROM_PO_AMENDMENT", detail.IS_FROM_PO_AMENDMENT ? 1 : 0);
                            DataSet dataset = sqlHelper.SelectList("cp_SavePODetails", sd);

                            if (dataset != null && dataset.Tables.Count > 0 && dataset.Tables[0].Rows.Count > 0 && dataset.Tables[0].Rows[0][0] != null)
                            {
                                response.ObjectID = dataset.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToInt32(dataset.Tables[0].Rows[0][0]) : 0;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                response.ErrorMessage = ex.Message;
            }

            return response;
        }

        public Response EditPODetails(POItem item, string sessionId)
        {
            Response response = new Response();
            List<Attachment> attachments = new List<Attachment>();
            List<FileUpload> vendorattachmentArr = new List<FileUpload>();
            PRMServices prm = new PRMServices();
            List<KeyValuePair> strs = new List<KeyValuePair>();
            try
            {
                Utilities.ValidateSession(sessionId);
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                sd.Add("P_DELIVERY_DATE", item.DELIVERY_DATE);
                sd.Add("P_PO_TYPE", item.PO_TYPE);
                sd.Add("P_PO_STATUS", item.PO_STATUS);
                sd.Add("P_PO_NUMBER", item.PO_NUMBER);
                sd.Add("P_USER", item.CUSTOMER_USER_ID);
                sd.Add("P_NOTIFY_VENDOR", item.NOTIFY_VENDOR);
                DataSet dataSet = sqlHelper.SelectList("cp_EditPODetails", sd);
                response.ObjectID = 1;

                if (item != null && item.NOTIFY_VENDOR > 0)
                {
                    sd = new SortedDictionary<object, object>() { };
                    sd.Add("P_PROJECT_ID", item.PROJECT_ID);
                    sd.Add("P_VENDOR_ID", item.VENDOR_ID);
                    DataSet dataset = sqlHelper.SelectList("po_GetEmailAttachments", sd);

                    if (dataset !=null && dataset.Tables[0].Rows.Count > 0)
                    {
                        foreach (DataRow rowValue in dataset.Tables[0].Rows)
                        {
                            KeyValuePair values = new KeyValuePair();
                            values.Key1 = rowValue["QUOTE_PDF"] != DBNull.Value ? Convert.ToString(rowValue["QUOTE_PDF"]) : string.Empty;
                            values.Value = rowValue["AMENDMENT_ATTACHMENTS"] != DBNull.Value ? Convert.ToString(rowValue["AMENDMENT_ATTACHMENTS"]) : string.Empty;
                            if (!string.IsNullOrEmpty(values.Value))
                            {
                                var val = JsonConvert.DeserializeObject<dynamic>(values.Value);
                                values.Value = val[0].fileID.ToString();
                                values.Key = 1;
                            }
                            else {
                                values.Value = values.Key1;
                                values.Key = 0;
                            }
                            strs.Add(values);
                        }
                    }

                    if (strs.Count > 0)
                    {
                        List<KeyValuePair> newList = new List<KeyValuePair>();
                        newList = strs.Where(a => a.Key > 0).ToList();
                        if (newList.Count > 0)
                        {
                            strs = strs.Where(a => a.Key > 0).ToList();
                        }
                        if (newList.Count <= 0)
                        {
                            strs = strs.Where(a => a.Key <= 0).ToList();
                        }
                    }

                    foreach (var multiAttachments in strs)
                    {
                        Attachment singleAttachment = null;
                        var fileData = Utilities.DownloadFile(Convert.ToString(multiAttachments.Value), sessionId);
                        if (fileData.FileStream != null && !string.IsNullOrEmpty(fileData.FileName))
                        {
                            singleAttachment = new Attachment(fileData.FileStream, fileData.FileName);
                        }
                        attachments.Add(singleAttachment);
                    }
                    if (item.VendorAttachment != null)
                    {
                        foreach (FileUpload fd in item.VendorAttachment)
                        {
                            //    var newFileObj = new FileUpload();
                            //    if (fd.FileID <= 0)
                            //    {

                            //        var attachName = string.Empty;
                            //        long tick = DateTime.UtcNow.Ticks;
                            //        attachName = HttpContext.Current.Server.MapPath(Utilities.FILE_URL + fd.FileName);
                            //        prm.SaveFile(attachName, fd.FileStream);
                            //        attachName = fd.FileName;
                            //        Response res = prm.SaveAttachment(attachName);
                            //        if (res.ErrorMessage != "")
                            //        {
                            //            response.ErrorMessage = res.ErrorMessage;
                            //        }

                            //        fd.FileID = res.ObjectID;
                            //        newFileObj.FileID = fd.FileID;
                            //        newFileObj.FileName = fd.FileName;
                            //        newFileObj.FileStream = null;
                            //    }
                            //    else if (fd.FileID > 0)
                            //    {
                            //        var fileData = prm.DownloadFile(Convert.ToString(fd.FileID), sessionId);
                            //        newFileObj.FileID = fd.FileID;
                            //        newFileObj.FileName = fd.FileName;
                            //        newFileObj.FileStream = null;
                            //    }
                            //    vendorattachmentArr.Add(newFileObj);
                            //}
                            if (fd.FileID <= 0)
                            {
                                var attachName = string.Empty;
                                long tick = DateTime.UtcNow.Ticks;
                                attachName = HttpContext.Current.Server.MapPath(Utilities.FILE_URL + fd.FileName);
                                prm.SaveFile(attachName, fd.FileStream);
                                attachName = fd.FileName;
                                Response res = prm.SaveAttachment(attachName);
                                if (res.ErrorMessage != "")
                                {
                                    response.ErrorMessage = res.ErrorMessage;
                                }
                                fd.FileID = res.ObjectID;
                                if (fd.FileID > 0)
                                {
                                    Attachment singleAttachment = new Attachment(new MemoryStream(fd.FileStream), attachName);
                                    attachments.Add(singleAttachment);
                                }
                            }
                            else if (fd.FileID > 0)
                            {
                                Attachment singleAttachment = null;
                                var fileData = prm.DownloadFile(Convert.ToString(fd.FileID), sessionId);
                                if (fileData.FileStream != null && !string.IsNullOrEmpty(fileData.FileName))
                                {
                                    singleAttachment = new Attachment(fileData.FileStream, fileData.FileName);
                                }

                                attachments.Add(singleAttachment);
                            }
                        }

                        var dt = sqlHelper.SelectQuery($"SELECT TOP 1 VENDOR_ID, V.U_EMAIL, V.U_FNAME, V.U_LNAME, V.COMP_NAME FROM POScheduleDetails PO INNER JOIN vendors V ON V.U_ID = PO.VENDOR_ID WHERE PO_NUMBER = '{item.PO_NUMBER}' AND V.IS_PRIMARY = 1;");
                        if (dt != null && dt.Rows.Count > 0)
                        {
                            int vendorId = dt.Rows[0]["VENDOR_ID"] != DBNull.Value ? Convert.ToInt32(dt.Rows[0]["VENDOR_ID"]) : 0;
                            string email = dt.Rows[0]["U_EMAIL"] != DBNull.Value ? Convert.ToString(dt.Rows[0]["U_EMAIL"]) : "";
                            string fName = dt.Rows[0]["U_FNAME"] != DBNull.Value ? Convert.ToString(dt.Rows[0]["U_FNAME"]) : "";
                            string lName = dt.Rows[0]["U_LNAME"] != DBNull.Value ? Convert.ToString(dt.Rows[0]["U_LNAME"]) : "";
                            string compName = dt.Rows[0]["COMP_NAME"] != DBNull.Value ? Convert.ToString(dt.Rows[0]["COMP_NAME"]) : "";
                            if (vendorId > 0 && !string.IsNullOrWhiteSpace(email))
                            {
                                System.Xml.XmlDocument doc = new System.Xml.XmlDocument();
                                doc.Load(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "bin") + "/EmailTemplates/EmailFormats.xml");
                                System.Xml.XmlNode footernode = doc.DocumentElement.SelectSingleNode("EmailFooter");
                                string websiteURL = ConfigurationManager.AppSettings["WEBSITE_URL"] + "/prm360.html#/list-po-new";
                                string subject = "PO Released - " + item.PO_NUMBER;
                                string body = $@"Hi {fName} {lName}, <br/> PO has been released. PO Number: {item.PO_NUMBER}. <br/> Please click <a href='{websiteURL}'>here<a/> to view the PO details (PDF).";
                                body += footernode.InnerText;
                                SendEmail(email, subject, body, item.SessionID, attachments).ConfigureAwait(false);
                                //prm.SendEmail(email, subject, body, 0, 0, "NOTIFY", sessionId, null, attachments, null, 0, "").ConfigureAwait(false);

                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                response.ErrorMessage = ex.Message;
            }

            return response;
        }

        public Response EditPOItemDetails(POItem item, string sessionId)
        {
            Response response = new Response();

            try
            {
                Utilities.ValidateSession(sessionId);
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                sd.Add("P_PO_NUMBER", item.PO_NUMBER);
                sd.Add("P_PACKAGE_ID", item.PACKAGE_ID);
                sd.Add("P_SUB_PACKAGE_ID", item.SUB_PACKAGE_ID);
                sd.Add("P_PO_DESCRIPTION", item.PO_DESCRIPTION);
                sd.Add("P_USER", item.CUSTOMER_USER_ID);
                DataSet dataSet = sqlHelper.SelectList("cp_EditPOItemDetails", sd);
                response.ObjectID = 1;
            }
            catch (Exception ex)
            {
                response.ErrorMessage = ex.Message;
            }

            return response;
        }

        public List<CArrayKeyValue> GetPOFilters(int compId, string sessionId)
        {
            List<CArrayKeyValue> details = new List<CArrayKeyValue>();
            try
            {
                Utilities.ValidateSession(sessionId);
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                sd.Add("P_COMP_ID", compId);
                var dataset = sqlHelper.SelectList("po_GetPOScheduleFilters", sd);

                if (dataset != null && dataset.Tables.Count > 0 && dataset.Tables[0].Rows.Count > 0)
                {
                    CArrayKeyValue key = new CArrayKeyValue();
                    key.Name = "PACKAGE";
                    key.ArrayPair = new List<KeyValuePair>();
                    foreach (var row in dataset.Tables[0].AsEnumerable())
                    {
                        KeyValuePair keyValuePair = new KeyValuePair();
                        keyValuePair.Key = row["PACKAGE_ID"] != DBNull.Value ? Convert.ToInt32(row["PACKAGE_ID"]) : 0;
                        keyValuePair.Value = row["PACKAGE_NAME"] != DBNull.Value ? Convert.ToString(row["PACKAGE_NAME"]) : "";
                        key.ArrayPair.Add(keyValuePair);
                    }

                    details.Add(key);
                }

                if (dataset != null && dataset.Tables.Count > 1 && dataset.Tables[1].Rows.Count > 0)
                {
                    CArrayKeyValue key = new CArrayKeyValue();
                    key.Name = "PROJECT";
                    key.ArrayPair = new List<KeyValuePair>();
                    foreach (var row in dataset.Tables[1].AsEnumerable())
                    {
                        KeyValuePair keyValuePair = new KeyValuePair();
                        keyValuePair.Key = row["PROJECT_ID"] != DBNull.Value ? Convert.ToInt32(row["PROJECT_ID"]) : 0;
                        keyValuePair.Value = row["PROJECT_NAME"] != DBNull.Value ? Convert.ToString(row["PROJECT_NAME"]) : "";
                        key.ArrayPair.Add(keyValuePair);
                    }

                    details.Add(key);
                }

                if (dataset != null && dataset.Tables.Count > 2 && dataset.Tables[2].Rows.Count > 0)
                {
                    CArrayKeyValue key = new CArrayKeyValue();
                    key.Name = "COST_CENTER";
                    key.ArrayPair = new List<KeyValuePair>();
                    foreach (var row in dataset.Tables[2].AsEnumerable())
                    {
                        KeyValuePair keyValuePair = new KeyValuePair();
                        keyValuePair.Key1 = row["COST_CENTER"] != DBNull.Value ? Convert.ToString(row["COST_CENTER"]) : "";
                        keyValuePair.Value = row["COST_CENTER"] != DBNull.Value ? Convert.ToString(row["COST_CENTER"]) : "";
                        key.ArrayPair.Add(keyValuePair);
                    }

                    details.Add(key);
                }

                

                if (dataset != null && dataset.Tables.Count > 3 && dataset.Tables[3].Rows.Count > 0)
                {
                    CArrayKeyValue key = new CArrayKeyValue();
                    key.Name = "VENDORS";
                    key.ArrayPair = new List<KeyValuePair>();
                    foreach (var row in dataset.Tables[3].AsEnumerable())
                    {
                        KeyValuePair keyValuePair = new KeyValuePair();
                        keyValuePair.Key = row["VENDOR_ID"] != DBNull.Value ? Convert.ToInt32(row["VENDOR_ID"]) : 0;
                        keyValuePair.Value = row["VENDOR_COMPANY"] != DBNull.Value ? Convert.ToString(row["VENDOR_COMPANY"]) : "";
                        key.ArrayPair.Add(keyValuePair);
                    }

                    details.Add(key);
                }                
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error in retrieving Data");
            }

            return details;
        }

        public string GetBillsBasedOnID(string PO_NUMBER, int BILL_ID, string sessionid) 
        {
            string json = null;
            try
            {
                Utilities.ValidateSession(sessionid);
                CORE.DataNamesMapper<BillRequests> mapper = new CORE.DataNamesMapper<BillRequests>();
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                sd.Add("P_PO_NUMBER", PO_NUMBER);
                sd.Add("P_BILL_ID", BILL_ID);
                DataSet dataSet = sqlHelper.SelectList("po_GetBillsBasedOnID", sd);
                if (dataSet != null && dataSet.Tables.Count > 0 && dataSet.Tables[0].Rows.Count > 0)
                {
                    json = JsonConvert.SerializeObject(dataSet, Formatting.Indented);
                }
                
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error in retrieving Data");
            }

            return json;

        }

        public string GetPODetailsBasedOnID(string PO_NUMBER, string sessionid)
        {
            string json = null;
            try
            {
                Utilities.ValidateSession(sessionid);
                CORE.DataNamesMapper<BillRequests> mapper = new CORE.DataNamesMapper<BillRequests>();
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                sd.Add("P_PO_NUMBER", PO_NUMBER);
                //sd.Add("P_PROJECT_ID", PROJECT_ID);
                //sd.Add("P_PACKAGE_ID", PACKAGE_ID);
                //sd.Add("P_SUB_PACKAGE_ID", SUB_PACKAGE_ID);
                DataSet dataSet = sqlHelper.SelectList("po_GetPODetailsBasedOnID", sd);
                if (dataSet != null && dataSet.Tables.Count > 0 && dataSet.Tables[0].Rows.Count > 0)
                {
                    json = JsonConvert.SerializeObject(dataSet, Formatting.Indented);
                }

            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error in retrieving Data");
            }

            return json;

        }

        public Response savePOtermsandconditions(string PO_NUMBER ,List<TermsConditions> TERMS_CONDITIONS, string sessionId)
        {
            Response response = new Response();

            try
            {
                Utilities.ValidateSession(sessionId);
                CORE.DataNamesMapper<TermsConditions> mapper = new CORE.DataNamesMapper<TermsConditions>();
                //SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };

                //sd.Add("P_MAP_ID", 0);

                foreach (var details in TERMS_CONDITIONS)
                {
                    SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                    sd.Add("P_MAP_ID", details.MAP_ID);
                    sd.Add("P_U_ID", details.U_ID);
                    sd.Add("P_COMP_ID", details.COMP_ID);
                    sd.Add("P_PO_NUMBER", PO_NUMBER);
                    sd.Add("P_PO_TYPE", details.PO_TYPE);
                    sd.Add("P_TITLE", details.TITLE);
                    sd.Add("P_DESCRIPTION", details.DESCRIPTION);




                    DataSet dataset = sqlHelper.SelectList("po_MapTermsAndConditions", sd);
                    if (dataset != null && dataset.Tables.Count > 0 && dataset.Tables[0].Rows.Count > 0 && dataset.Tables[0].Rows[0][0] != null)
                    {
                        response.ObjectID = dataset.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToInt32(dataset.Tables[0].Rows[0][0].ToString()) : -1;
                        response.ErrorMessage = dataset.Tables[0].Rows[0][1] != DBNull.Value ? Convert.ToString(dataset.Tables[0].Rows[0][1].ToString()) : string.Empty;
                    }
                }

            }
            catch (Exception ex)
            {
                response.ErrorMessage = ex.Message;
            }

            return response;
        }

        public Response SaveInvoiceDetails(List<InvoiceDetails> invoiceDetailsList, int invocieId, string sessionId)
        {
            Response response = new Response();
            PRMServices prm = new PRMServices();

            try
            {
                foreach (var detail in invoiceDetailsList)
                {
                    var attachmentArr = new List<FileUpload>();
                    string jsonString = null;
                    var attachments = Newtonsoft.Json.JsonConvert.DeserializeObject<List<FileUpload>>(detail.MULTIPLE_ATTACHMENTS);
                    if (attachments != null && attachments.Count > 0)
                    {
                        foreach (FileUpload fd in attachments)
                        {
                            var newFileObj = new FileUpload();
                            if (fd.FileID <= 0)
                            {
                                var attachName = string.Empty;
                                long tick = DateTime.UtcNow.Ticks;
                                attachName = HttpContext.Current.Server.MapPath(Utilities.FILE_URL + fd.FileName);
                                prm.SaveFile(attachName, fd.FileStream);
                                attachName = fd.FileName;
                                Response res = prm.SaveAttachment(attachName);
                                if (res.ErrorMessage != "")
                                {
                                    response.ErrorMessage = res.ErrorMessage;
                                }

                                fd.FileID = res.ObjectID;
                                newFileObj.FileID = fd.FileID;
                                newFileObj.FileName = fd.FileName;
                                newFileObj.FileStream = null;
                            }
                            else if (fd.FileID > 0)
                            {
                                var fileData = prm.DownloadFile(Convert.ToString(fd.FileID), sessionId);
                                newFileObj.FileID = fd.FileID;
                                newFileObj.FileName = fd.FileName;
                                newFileObj.FileStream = null;
                            }
                            attachmentArr.Add(newFileObj);
                        }
                        jsonString = Newtonsoft.Json.JsonConvert.SerializeObject(attachmentArr);
                    }

                    SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                    sd.Add("P_ROW_ID", detail.ROW_ID);
                    sd.Add("P_INVOICE_ID", invocieId);
                    sd.Add("P_BILL_ID", detail.BILL_ID);
                    sd.Add("P_PO_NUMBER", detail.PO_NUMBER);
                    sd.Add("P_PROJECT_ID", detail.PROJECT_ID);
                    sd.Add("P_VENDOR_ID", detail.VENDOR_ID);
                    sd.Add("P_VENDOR_CODE", detail.VENDOR_CODE);
                    sd.Add("P_INVOICE_NUMBER", detail.INVOICE_NUMBER);
                    sd.Add("P_INVOICE_DESCRIPTION", detail.INVOICE_DESCRIPTION);
                    sd.Add("P_INVOICE_DATE", detail.INVOICE_DATE);
                    sd.Add("P_UNIT_RATE", detail.UNIT_RATE);
                    sd.Add("P_CGST", detail.CGST);
                    sd.Add("P_SGST", detail.SGST);
                    sd.Add("P_IGST", detail.IGST);
                    sd.Add("P_OTHERS", detail.OTHERS);
                    sd.Add("P_BILL_CERTIFIED_AMOUNT", detail.BILL_CERTIFIED_AMOUNT);
                    sd.Add("P_INVOICE_AMOUNT", detail.INVOICE_AMOUNT);
                    sd.Add("P_TOTAL_AMOUNT", detail.TOTAL_AMOUNT);
                    //sd.Add("P_TAX_AMOUNT", detail.TAX_AMOUNT);
                    sd.Add("P_CREATED_BY", detail.CREATED_BY);
                    sd.Add("P_V_COMP_ID", detail.V_COMP_ID);
                    sd.Add("P_C_COMP_ID", detail.C_COMP_ID);
                    sd.Add("P_TAX_AMOUNT", detail.TAX_AMOUNT);
                    sd.Add("P_MULTIPLE_ATTACHMENTS", jsonString);
                    sd.Add("P_INVOICE_WF_ID", detail.INVOICE_WF_ID);




                    DataSet dataset = sqlHelper.SelectList("po_SaveInvoiceDetails", sd);
                    if (dataset != null && dataset.Tables.Count > 0 && dataset.Tables[0].Rows.Count > 0 && dataset.Tables[0].Rows[0][0] != null)
                    {
                        response.ObjectID = dataset.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToInt32(dataset.Tables[0].Rows[0][0].ToString()) : -1;
                        response.ErrorMessage = dataset.Tables[0].Rows[0][1] != DBNull.Value ? Convert.ToString(dataset.Tables[0].Rows[0][1].ToString()) : string.Empty;
                        detail.INVOICE_WF_ID = dataset.Tables[0].Rows[0][2] != DBNull.Value ? Convert.ToInt32(dataset.Tables[0].Rows[0][2].ToString()) : -1;

                        if (response.ObjectID > 0 && detail.INVOICE_WF_ID > 0 && response.ErrorMessage == "")
                        {
                            PRMWFService pRMWF = new PRMWFService();
                            Response res2 = pRMWF.AssignWorkflow(detail.INVOICE_WF_ID, Convert.ToInt32(response.ObjectID), detail.VENDOR_ID, sessionId, "VENDOR_INVOICE");
                        }
                    }

                    if (invocieId <= 0 && dataset.Tables.Count > 0 && dataset.Tables[0].Rows.Count > 0)
                    {
                        invocieId = Convert.ToInt32(dataset.Tables[0].Rows[0]["INVOICE_ID"]);
                    }
                    
                }
            }
            catch (Exception ex)
            {
                response.ErrorMessage = ex.Message;
            }

            return response;
        }

        public string GetInvoicesList(int COMP_ID, int U_ID, int IS_CUSTOMER, string fromDate, string toDate, string poNumber, string projectIds, string invoiceNumbers, string invoiceStatus, string sessionid, int PageSize = 0, int NumberOfRecords = 0)
        {
            string json = null;
            try
            {
                if (string.IsNullOrEmpty(fromDate))
                {
                    fromDate = DateTime.UtcNow.AddDays(-30).ToString("yyyy-MM-dd");
                }

                if (string.IsNullOrEmpty(toDate))
                {
                    toDate = DateTime.UtcNow.AddDays(1).ToString("yyyy-MM-dd");
                }
                Utilities.ValidateSession(sessionid);
                CORE.DataNamesMapper<BillRequests> mapper = new CORE.DataNamesMapper<BillRequests>();
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                sd.Add("P_COMP_ID", COMP_ID);
                sd.Add("P_U_ID", U_ID);
                sd.Add("P_IS_CUSTOMER", IS_CUSTOMER);
                sd.Add("P_FROM_DATE", fromDate);
                sd.Add("P_TO_DATE", toDate);
                sd.Add("P_PO_NUMBER", poNumber);
                sd.Add("P_PROJECT_ID", projectIds);
                sd.Add("P_INVOICE_NUMBER", invoiceNumbers);
                sd.Add("P_INVOICE_STATUS", invoiceStatus);
                sd.Add("P_PAGE", PageSize);
                sd.Add("P_PAGE_SIZE", NumberOfRecords);
                DataSet dataSet = sqlHelper.SelectList("po_GetInvoicesList", sd);
                if (dataSet != null && dataSet.Tables.Count > 0 && dataSet.Tables[0].Rows.Count > 0)
                {
                    json = JsonConvert.SerializeObject(dataSet, Formatting.Indented);
                }

            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error in retrieving Data");
            }

            return json;

        }

        public Response SavePOPDFDetails(POItem detail, string sessionId)
        {
            Response response = new Response();

            try
            {
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                sd.Add("P_PO_NUMBER", detail.PO_NUMBER);
                sd.Add("P_PO_PDF_JSON", detail.PO_PDF_JSON);
                sd.Add("P_USER", (detail.CUSTOMER_USER_ID));
                
                DataSet dataSet = sqlHelper.SelectList("cp_SavePOPDFDetails", sd);

                if (dataSet != null && dataSet.Tables.Count > 0 && dataSet.Tables[0].Rows.Count > 0)
                {
                    var PO_NUMBER = dataSet.Tables[0].Rows[0]["PO_NUMBER"] != DBNull.Value ? Convert.ToString(dataSet.Tables[0].Rows[0]["PO_NUMBER"]) : string.Empty;
                    if (string.IsNullOrWhiteSpace(PO_NUMBER))
                    {
                        throw new Exception("PO is in COMPLETE status, PDF details cannot be saved. Please contact support.");
                    }
                }
            }
            catch (Exception ex)
            {
                response.ErrorMessage = ex.Message;
            }

            return response;
        }

        public async Task SendEmail(string To, string Subject, string Body, string sessionID = null, List<System.Net.Mail.Attachment> ListAttachment = null)
        {
            try
            {
                PRMServices prm = new PRMServices();
                Communication communication = new Communication();
                communication = Utilities.GetCommunicationData("EMAIL_FROM", sessionID);
                Body = Body.Replace("COMPANY_LOGO_ID", communication.Company.CompanyID > 0 ? communication.Company.CompanyID.ToString() : "0");
                Body = Body.Replace("COMPANY_NAME", !string.IsNullOrEmpty(communication.Company.CompanyName) ? communication.Company.CompanyName : "PRM360");
                Body = Body.Replace("USER_NAME", !string.IsNullOrEmpty(communication.User.FirstName + " " + communication.User.LastName) ? communication.User.FirstName + " " + communication.User.LastName : "");
                Body = Body.Replace("COMPANY_ADDRESS", !string.IsNullOrEmpty(communication.CompanyAddress) ? communication.CompanyAddress : "");
                SmtpClient smtpClient = new SmtpClient(ConfigurationManager.AppSettings["MAILHOST"].ToString());
                System.Net.NetworkCredential credentials = new System.Net.NetworkCredential(ConfigurationManager.AppSettings["MAILHOST_USER"].ToString(), ConfigurationManager.AppSettings["MAILHOST_PWD"].ToString());
                smtpClient.Credentials = credentials;
                smtpClient.EnableSsl = false;
                MailMessage mail = new MailMessage();
                mail.From = new MailAddress(prm.GetFromAddress(""), prm.GetFromDisplayName(""));
                mail.BodyEncoding = System.Text.Encoding.ASCII;
                List<string> ToAddresses = To.Split(',').ToList<string>();
                foreach (string address in ToAddresses)
                {
                    if (!string.IsNullOrEmpty(address))
                    {
                        mail.To.Add(new MailAddress(address));
                    }

                }

                if (ListAttachment != null)
                {
                    foreach (System.Net.Mail.Attachment attachment in ListAttachment)
                    {
                        if (attachment != null)
                        {
                            mail.Attachments.Add(attachment);
                        }
                    }
                }

                mail.Subject = Subject;
                mail.IsBodyHtml = true;
                mail.Body = Body;
                await smtpClient.SendMailAsync(mail);
                Response response = new Response();
                response.ObjectID = 1;
            }
            catch (Exception ex)
            {
                Response response = new Response();
                response.ErrorMessage = ex.Message;
            }
        }
        public string GetPoAudit(int COMP_ID, string PO_NUMBER, string sessionid)
        {
            string json = null;
            try
            {
                Utilities.ValidateSession(sessionid);
                CORE.DataNamesMapper<BillRequests> mapper = new CORE.DataNamesMapper<BillRequests>();
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                sd.Add("P_COMP_ID", COMP_ID);
                sd.Add("P_PO_NUMBER", PO_NUMBER);
                DataSet dataSet = sqlHelper.SelectList("po_GetPoAudit", sd);
                if (dataSet != null && dataSet.Tables.Count > 0 && dataSet.Tables[0].Rows.Count > 0)
                {
                    json = JsonConvert.SerializeObject(dataSet, Formatting.Indented);
                }

            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error in retrieving Data");
            }

            return json;

        }


        public Response deactiveBillRequest(int BILL_ID, int IS_VALID, int MODIFIED_BY, string sessionid)
        {
            Response details = new Response();
            try
            {
                Utilities.ValidateSession(sessionid);
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };

                sd.Add("P_BILL_ID", BILL_ID);
                sd.Add("P_IS_VALID", IS_VALID);
                sd.Add("P_MODIFIED_BY", MODIFIED_BY);

                CORE.DataNamesMapper<Response> mapper = new CORE.DataNamesMapper<Response>();
                var dataset = sqlHelper.SelectList("po_deactiveBillRequest", sd);
                if (dataset != null && dataset.Tables.Count > 0 && dataset.Tables[0].Rows.Count > 0 && dataset.Tables[0].Rows[0][0] != null)
                {
                    details.ObjectID = dataset.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToInt32(dataset.Tables[0].Rows[0][0].ToString()) : -1;
                    details.ErrorMessage = dataset.Tables[0].Rows[0][1] != DBNull.Value ? Convert.ToString(dataset.Tables[0].Rows[0][1].ToString()) : string.Empty;
                }
            }
            catch (Exception ex)
            {
                logger.Error("Error in deleteBillRequest.", ex.Message);
            }
            return details;
        }


        //POScheduleDetails poDetails, POScheduleDetailsItems poItems, TermsConditions poTc
        public FileResponse PDFSave(dynamic tcData, string poNumber, int compId, int revision, string sessionId, bool saveToServer, bool validateSession = false)
        {
            FileResponse response = new FileResponse();

            var pdfFileName = string.Empty;

            string data = string.Empty;

            if (validateSession)
            {
                Utilities.ValidateSession(sessionId);

                List<POItem> details = GetPODetails(poNumber, sessionId);
                PRMProjectServices prmpro = new PRMProjectServices();
                ProjectDetails PD = prmpro.GetProjectDetails(details[0].PROJECT_ID, sessionId);
                dynamic dynObj = new ExpandoObject();
                if (details != null && details.Count > 0)
                {


                    //var poValue = details.Select(a => a.PO_VALUE).FirstOrDefault();
                    var poValue = details.Sum(a => a.TOTAL_VALUE);
                    var wordConversion = PriceInWords(poValue);

                    details.ForEach(a =>
                    {
                        a.PO_VALUE_WORDS = wordConversion;
                        a.GST_AMOUNT = Math.Round(a.GST_AMOUNT, 2);
                        a.CGST_AMOUNT = Math.Round(a.CGST_AMOUNT, 2);
                        a.SGST_AMOUNT = Math.Round(a.SGST_AMOUNT, 2);
                        a.IGST_AMOUNT = Math.Round(a.IGST_AMOUNT, 2);
                        a.PO_VALUE = Math.Round(poValue, 2);
                        a.PO_VALUE_WITHOUTTAX = Math.Round(a.PO_VALUE_WITHOUTTAX, 2);
                    });

                    dynObj.poDetails = details[0];
                    if (details[0].PO_TYPE == "WORK_ORDER")
                    {
                        dynObj.PO_TYPE_DESC = "Work Order (Item Rate Contract)";
                    }

                    if (details[0].PO_TYPE == "LOCAL_SALES")
                    {
                        dynObj.PO_TYPE_DESC = "Purchase Order (Local Sales)";
                    }

                    if (details[0].PO_TYPE == "SERVICE_AGREEMENT")
                    {
                        dynObj.PO_TYPE_DESC = "Service Agreeement";
                    }

                    if (details[0].PO_TYPE == "IMPORT")
                    {
                        dynObj.PO_TYPE_DESC = "Purchase Order (Import)";
                    }
                    dynObj.poDetailItems = details;
                    dynObj.poData = details[0].PO_PDF_JSON;
                    dynObj.projDetails = PD;
                    if (PD.BILL_TO_ADDRESS != null)
                    {
                        dynamic jsonBillAddress = JsonConvert.DeserializeObject<JObject>(PD.BILL_TO_ADDRESS);
                        dynObj.projDetails.BILL_TO_ADDRESS = jsonBillAddress["LEGAL_ENTITY_NAME"].ToString() + " " + jsonBillAddress["ADDRESS"].ToString();
                    }
                    dynObj.poPDFDetails = JsonConvert.DeserializeObject<dynamic>(tcData);
                    data = JsonConvert.SerializeObject(dynObj);
                }
            }

            try
            {
                if (!string.IsNullOrEmpty(data))
                {
                    // Deserialize JSON into dynamic
                    var deserializedData = JsonConvert.DeserializeObject<dynamic>(data);

                    if (deserializedData != null)
                    {
                        var poDetails = deserializedData.poDetails;
                        var poData = deserializedData.poData;
                        string currentFolder = AppDomain.CurrentDomain.BaseDirectory;
                       

                        var PO_CLIENT_IMG = Path.Combine(currentFolder, "js", "resources", "images", "po_client_logo.png");

                        var poPDFDetails = deserializedData.poPDFDetails;
                        var projDetails = deserializedData.projDetails;
                        var poDetailItems = deserializedData.poDetailItems;
                        var dictionary = new Dictionary<string, string>();
                        Dictionary<string, List<string>> itemDetails = new Dictionary<string, List<string>>();

                        if (poDetails != null)
                        {
                            ConvertJObjectToDictionary(JObject.FromObject(poDetails), dictionary);
                        }
                        if (PO_CLIENT_IMG != null)
                        {
                            dictionary.Add("PO_CLIENT_IMG", PO_CLIENT_IMG);
                        }

                        if (poData != null)
                        {
                            JObject jsonObject = JsonConvert.DeserializeObject<JObject>(poData.ToString());
                            ConvertJObjectToDictionary(jsonObject, dictionary);                          
                        }
                     
                        if (poPDFDetails != null)
                        {
                            ConvertJObjectToDictionary(JObject.FromObject(poPDFDetails), dictionary);
                        }

                        if (projDetails != null)
                        {
                            ConvertProjectDetailsJObjectToDictionary(JObject.FromObject(projDetails), dictionary);
                        }

                       
                        if (poDetailItems != null)
                        {
                            int i = 1;
                            string[] desiredProperties = new string[]
                            {
                                "ROW_ID", "PO_DESCRIPTION", "DELIVERY_DATE","UNIT", "ORDER_QTY", "TOTAL_AMOUNT","TOTAL_VALUE"
                            };
                            foreach (var item in poDetailItems)
                            {
                                List<string> propertyValues = new List<string>();
                                propertyValues.AddRange(desiredProperties);
                                // Cast the item as JObject to access its properties
                                var jsonObject = (JObject)item;


                                propertyValues[0] = i.ToString();
                                propertyValues[1] = jsonObject["PO_DESCRIPTION"].ToString();
                                propertyValues[2] = Convert.ToDateTime(jsonObject["DELIVERY_DATE"]).ToString("dd/MM/yyyy");
                                propertyValues[3] = "NA";
                                propertyValues[4] = jsonObject["ORDER_QTY"].ToString();
                                propertyValues[5] = jsonObject["TOTAL_AMOUNT"].ToString();
                                propertyValues[6] = jsonObject["TOTAL_AMOUNT"].ToString();
                                // Loop through each property in the JObject
                               

                                itemDetails[$"Row-{i}"] = propertyValues;
                                i++;
                            }
                        }
                        string attachmentPath = string.Empty;
                        try
                        {
                            var query1 = sqlHelper.ExecuteQuery($@"select ATTACHMENTS from potermsandconditions where PO_TYPE = '{poDetails["PO_TYPE"]}' 
                                AND TITLE = 'ATTACHMENTS' AND COMP_ID = {compId} ORDER BY DATE_CREATED DESC;");
                            var attachments = string.Empty;
                            var convertedAttachments = new List<FileUpload>();
                            if (query1 != null && query1.Tables.Count > 0 && query1.Tables[0].Rows.Count > 0)
                            {
                                attachments = query1.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToString(query1.Tables[0].Rows[0][0]) : string.Empty;
                                convertedAttachments = Newtonsoft.Json.JsonConvert.DeserializeObject<List<FileUpload>>(attachments);
                            }

                            if (!string.IsNullOrEmpty(attachments))
                            {
                                attachmentPath = HttpContext.Current.Server.MapPath(Utilities.FILE_URL + convertedAttachments.FirstOrDefault().FileName);
                            }
                        }
                        catch (Exception ex)
                        {
                            throw new Exception($@"Attachment not found for the po type {dictionary["PO_TYPE"].ToString()}");
                        }

                        FileResponse file = new FileResponse();


                        file = PdfUtilities.DownloadPOPDF(dictionary, itemDetails, attachmentPath, saveToServer);

                        response = file;

                        //Response res = SaveAttachment(pdfFileName);
                        //if (res.ErrorMessage != "")
                        //{
                        //    response.FileId = res.ObjectID;
                        //}
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Error("error while generating pdf" + ex.Message);
            }
            return response;
        }

        // Function to convert JObject to Dictionary<string, string>
        Dictionary<string, string> ConvertJObjectToDictionary(JObject obj, Dictionary<string, string> dictionary)
        {
            foreach (var property in obj.Properties())
            {
                if (property.Value != null)
                {
                    if (property.Value.Type == JTokenType.String || property.Value.Type == JTokenType.Integer || property.Value.Type == JTokenType.Float)
                    {
                        dictionary[property.Name] = property.Value.ToString();
                    }
                }
            }
            return dictionary;
        }

        Dictionary<string, string> ConvertProjectDetailsJObjectToDictionary(JObject obj, Dictionary<string, string> dictionary)
        {
            var excludedProperties = new List<string>{"PO_VALUE","PO_NUMBER","PROJECT_NAME","CONTRACT_LEGAL_ENTITY","CONTRACT_PAN","GST_NUMBER","CLIENT_NAME"}; 
            foreach (var property in obj.Properties())
            {
                if (property.Value != null && !excludedProperties.Contains(property.Name))
                {
                    if (property.Value.Type == JTokenType.String || property.Value.Type == JTokenType.Integer || property.Value.Type == JTokenType.Float)
                    {
                        dictionary[property.Name] = property.Value.ToString();
                    }
                }
            }
            return dictionary;
        }


        private static readonly string[] SingleDigits = { "Zero", "One", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine" };
        private static readonly string[] DoubleDigits = { "Ten", "Eleven", "Twelve", "Thirteen", "Fourteen", "Fifteen", "Sixteen", "Seventeen", "Eighteen", "Nineteen" };
        private static readonly string[] TensPlace = { "", "Ten", "Twenty", "Thirty", "Forty", "Fifty", "Sixty", "Seventy", "Eighty", "Ninety" };

        private static string HandleTens(int dgt, int prevDgt)
        {
            return dgt == 0 ? "" : (dgt == 1 ? DoubleDigits[prevDgt] : TensPlace[dgt]);
        }

        private static string HandleUnitPlace(int dgt, int nxtDgt, string denom)
        {
            return (dgt != 0 && nxtDgt != 1 ? " " + SingleDigits[dgt] : "") + (nxtDgt != 0 || dgt > 0 ? " " + denom : "");
        }

        public static string PriceInWords(decimal price)
        {
            string str = "";
            string priceString = price.ToString("0"); // Convert to string without decimal
            List<string> words = new List<string>();

            if (string.IsNullOrEmpty(priceString) || !decimal.TryParse(priceString, out _))
            {
                return str;
            }

            if (price > 0 && priceString.Length <= 10)
            {
                for (int digitIdx = 0; digitIdx < priceString.Length; digitIdx++)
                {
                    int digit = priceString[digitIdx] - '0';
                    int nxtDigit = digitIdx + 1 < priceString.Length ? priceString[digitIdx + 1] - '0' : 0;
                    int position = priceString.Length - digitIdx - 1;

                    switch (position)
                    {
                        case 0:
                            words.Add(HandleUnitPlace(digit, nxtDigit, ""));
                            break;
                        case 1:
                            words.Add(HandleTens(digit, nxtDigit));
                            break;
                        case 2:
                            words.Add(digit != 0 ? " " + SingleDigits[digit] + " Hundred" + (nxtDigit != 0 ? " and" : "") : "");
                            break;
                        case 3:
                            words.Add(HandleUnitPlace(digit, nxtDigit, "Thousand"));
                            break;
                        case 4:
                            words.Add(HandleTens(digit, nxtDigit));
                            break;
                        case 5:
                            words.Add(HandleUnitPlace(digit, nxtDigit, "Lakh"));
                            break;
                        case 6:
                            words.Add(HandleTens(digit, nxtDigit));
                            break;
                        case 7:
                            words.Add(HandleUnitPlace(digit, nxtDigit, "Crore"));
                            break;
                        case 8:
                            words.Add(HandleTens(digit, nxtDigit));
                            break;
                        case 9:
                            words.Add(digit != 0 ? " " + SingleDigits[digit] + " Hundred" + (nxtDigit != 0 ? " and" : " Crore") : "");
                            break;
                    }
                }
                str = string.Join("", words.AsReadOnly().ToArray());
            }

            return str.Trim();
        }

    }

}
