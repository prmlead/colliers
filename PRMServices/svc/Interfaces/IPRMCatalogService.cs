﻿using System.Collections.Generic;
using System.ServiceModel;
using System.ServiceModel.Web;
using PRMName = PRMServices.Models;
using PRMServices.Models.Catalog;


namespace PRMServices
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IPRMRealTimePriceService" in both code and config file together.
    [ServiceContract]
    public interface IPRMCatalogService
    {
        #region categories
        //GET
        [OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getcategories?compId={compId}&sessionId={sessionId}&PageSize={PageSize}&NumberOfRecords={NumberOfRecords}")]
        List<Category> GetCategories(int compId, string sessionId, int PageSize = 0, int NumberOfRecords = 0);

        [OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getcategorybyid?compId={compId}&catId={catId}&sessionId={sessionId}")]
        Category GetCategoryById(int compId, int catId, string sessionId);

        [OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "GetCategoryByCode?compId={compId}&catCode={catCode}&sessionId={sessionId}")]
        Category GetCategoryByCode(int compId, string catCode, string sessionId);

        [OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "GetCategoryByName?compId={compId}&catName={catname}&sessionId={sessionId}")]
        Category GetCategoryByName(int compId, string catname, string sessionId);

        [OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "SearchCategories?searchString={searchString}&sessionId={sessionId}")]
        List<Category> SearchCategories(string searchString, string sessionId);

        [OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "GetSubCategories?parentCatId={parentcatid}&companyId={companyId}&sessionId={sessionId}")]
        List<Category> GetSubCategories(int parentCatId, int companyId, string sessionId);

        [OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "GetProductSubCategories?prodId={prodId}&parentCatId={parentcatid}&companyId={companyId}&sessionId={sessionId}")]
        List<Category> GetProductSubCategories(int prodId, int parentCatId, int companyId, string sessionId);

        [OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "GetVendorCategories?vendorId={vendorId}&parentCatId={parentcatid}&companyId={companyId}&sessionId={sessionId}&type={type}&PageSize={PageSize}&NumberOfRecords={NumberOfRecords}&searchString={searchString}")]
        List<Category> GetVendorCategories(int vendorId, int parentCatId, int companyId, string sessionId, string type, int PageSize = 0, int NumberOfRecords = 0, string searchString = null);


        [OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "GetVendorProducts?vendorId={vendorId}&companyId={companyId}&sessionId={sessionId}")]
        List<Product> GetVendorProducts(int vendorId, int companyId, string sessionId);


        //POST
        [OperationContract]
        [WebInvoke(Method = "POST",
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "addcategory")]
        CatalogResponse AddCategory(Category reqCategory, string sessionID);
        
        [OperationContract]
        [WebInvoke(Method = "POST",
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "deletecategory")]
        CatalogResponse DeleteCategory(Category reqCategory, string sessionID);

        /// <summary>
        /// if there is any change in category,like category order
        /// </summary>
        /// <param name="reqCategory"></param>
        /// <returns></returns>
        [OperationContract]
        [WebInvoke(Method = "POST",
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "updatecategory")]
        CatalogResponse UpdateCategory(Category reqCategory, string sessionID);

        #endregion categories


        #region products
        //GET
        [OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getproducts?compId={compId}&sessionId={sessionId}&PageSize={PageSize}&NumberOfRecords={NumberOfRecords}&searchString={searchString}")]
        List<Product> GetProducts(int compId, string sessionId, int PageSize = 0, int NumberOfRecords = 0, string searchString = null);

        [OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getuserproducts?compId={compId}&userId={userId}&sessionId={sessionId}&PageSize={PageSize}&NumberOfRecords={NumberOfRecords}&searchString={searchString}&isCas={isCas}&isMfcd={isMfcd}&isnamesearch={isnamesearch}")]
        List<Product> GetUserProducts(int compId, int userId, string sessionId, int PageSize = 0, int NumberOfRecords = 0, string searchString = null, bool isCas = false, bool isMfcd = false, bool isnamesearch = false);

        [OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getmaterialproducts?compId={compId}&sessionId={sessionId}")]
        List<Product> GetMaterialProducts(int compId, string sessionId);

        //GetNonCoreProducts

        [OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getNonCoreproducts?compId={compId}&userId={userId}&sessionId={sessionId}")]
        List<Product> GetNonCoreProducts(int compId, int userId, string sessionId);

        [OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getproddatareport?reportType={reportType}&catitemid={catitemid}&sessionid={sessionid}")]
        List<VendorProductData> GetProdDataReport(string reportType, int catitemid, string sessionid);

        [WebInvoke(Method = "POST",
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "getproductsbyfilters")]
        //[WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getproductbyid?compId={compId}&prodId={prodId}&sessionId={sessionId}")]
        List<Product> GetProductsByFilters(int compId, string sessionId, List<ProductFilters> prodFilters);

        [OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getproductbyid?compId={compId}&prodId={prodId}&sessionId={sessionId}")]
        Product GetProductById(int compId, int prodId, string sessionId);

        [OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getproductbyname?compId={compId}&prodName={prodName}&sessionId={sessionId}")]
        Product GetProductByName(int compId, string prodName, string sessionId);

        [OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "searchproducts?compId={compId}&searchString={searchString}&sessionId={sessionId}")]
        List<Product> SearchProducts(int compId, string searchString, string sessionId);

        [OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "GetProductsByCategory?compId={compId}&catIds={catIds}&sessionId={sessionId}")]
        List<Product> GetProductsByCategory(int compId, int catIds,  string sessionId);

        [OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getvendormycatalogfile?userid={userid}&sessionid={sessionid}")]
        PRMName.Response GetUserMyCatalogFileId(int userid, string sessionid);

        [OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getpendingcontracts?compid={compid}&sessionid={sessionid}")]
        List<ContractManagementDetails> GetPendingContracts(int compid, string sessionid);

        [OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getproductcontracts?productids={productids}&sessionid={sessionid}")]
        List<ContractManagementDetails> GetProductContracts(string productids, string sessionid);

        [OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "GetAllProductsByCategories?compId={compId}&catIds={catIds}&sessionId={sessionId}")]
        List<Product> GetAllProductsByCategories(int compId, string catIds, string sessionId);

        [OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getproductVendors?productID={ProductID}&sessionId={sessionID}")]
        List<PRMName.ProductVendorDetails> GetProductVendors(int ProductID, string sessionID);

        //public List<PRMName.ProductVendorDetails> GetProductVendors(int ProductID, string sessionID)

        //POST
        [OperationContract]
        [WebInvoke(Method = "POST",
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "addproduct")]
        CatalogResponse AddProduct(Product reqProduct, string sessionID);

        [OperationContract]
        [WebInvoke(Method = "POST",
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "updateproduct")]
        CatalogResponse UpdateProduct(Product reqProduct, string sessionID);

        [OperationContract]
        [WebInvoke(Method = "POST",
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "updateproductcategories")]
        CatalogResponse updateproductcategories(int prodId,int compId, string catIds, int user, int statusCheck, string sessionId);

        [OperationContract]
        [WebInvoke(Method = "POST",
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "deleteproduct")]
        CatalogResponse DeleteProduct(Product reqProduct, string sessionID);

        [OperationContract]
        [WebInvoke(Method = "POST",
       RequestFormat = WebMessageFormat.Json,
       ResponseFormat = WebMessageFormat.Json,
       BodyStyle = WebMessageBodyStyle.WrappedRequest,
       UriTemplate = "isProdudtEditAllowed")]
        CatalogResponse IsProdudtEditAllowed(Product reqProduct, string sessionID);

        #endregion products

        #region Attribute calls
        [OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getAttribute?CompanyId={CompanyId}&sessionid={sessionId}")]
        List<AttributeModel> GetAttribute(int CompanyId, string sessionId);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="CompanyId"></param>
        /// <param name="entityType"> 0 for Product and 1 for Category</param>
        /// <param name="sessionId"></param>
        /// <returns></returns>
        [OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "GetProperties?CompanyId={CompanyId}&entityId={entityId}&entityType={entityType}&sessionid={sessionId}")]
        List<PropertyModel> GetProperties(int CompanyId, int entityId, int entityType, string sessionId);



        [OperationContract]
        [WebInvoke(Method = "POST",
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "SaveCatalogProperty")]
        CatalogResponse SaveCatalogProperty(PropertyModel property, string sessionId);


        [OperationContract]
        [WebInvoke(Method = "POST",
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "saveEntityProperties")]
        CatalogResponse SaveEntityProperties(List<PropertyEntityModel> propertyobj, string sessionId);
        #endregion Attribute calls

        [OperationContract]
        [WebInvoke(Method = "POST",
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        UriTemplate = "importentity")]
        CatalogResponse ImportEntity(ImportEntity entity, int compId, int user, string sessionId);

        [OperationContract]
        [WebInvoke(Method = "POST",
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        UriTemplate = "importcataloguecategories")]
        CatalogResponse ImportCatalogueCategories(ImportEntity entity, int compId, int user, string sessionId);

        [OperationContract]
        [WebInvoke(Method = "POST",
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        UriTemplate = "importvendoritemcategories")]
        CatalogResponse ImportVendorItemCategories(ImportEntity entity, int compId, int vendorId, int user, string sessionId);


        [OperationContract]
        [WebInvoke(Method = "POST",
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "saveVendorCatalog")]
        CatalogResponse SaveVendorCatalog(int vendorId, int compId, string catIds, string prodIds, int user, string sessionId);



        [OperationContract]
        [WebInvoke(Method = "POST",
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "saveVendorCatalog1")]
        CatalogResponse SaveVendorCatalog1(int vendorId, int compId, string catIds, string prodIds, int user, string sessionId);

        [OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getcompanyconfiguration?compid={compID}&configkey={configKey}&sessionid={sessionID}")]
        List<CompanyConfiguration> GetCompanyConfiguration(int compID, string configKey, string sessionID);

        [OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getprodvendordata?vendorid={vendorid}&catitemid={catitemid}&sessionid={sessionid}")]
        List<VendorProductData> GetProdVendorData(int vendorid, int catitemid, string sessionid);

        [OperationContract]
        [WebInvoke(Method = "POST",
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "saveproductquotationtemplate")]
        CatalogResponse SaveProductQuotationTemplate(ProductQuotationTemplate productquotationtemplate, string sessionid);

        [OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getproductquotationtemplate?catitemid={catitemid}&sessionid={sessionid}")]
        List<ProductQuotationTemplate> GetProductQuotationTemplate(int catitemid, string sessionid);

    }
}
 