﻿using System.ServiceModel;
using System.ServiceModel.Web;
using PRMServices.Models;
using System.Collections.Generic;
using System;

namespace PRMServices
{
    [ServiceContract]
    public interface IPRMAPMCService
    {

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getapmclist?userid={userID}&sessionid={sessionID}")]
        List<APMC> GetAPMCList(int userID, string sessionID);

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getapmcnegotiation?apmcnegid={apmcnegid}&userid={userID}&sessionid={sessionID}")]
        APMCDashboard GetAPMCNegotiation(int apmcnegid, int userID, string sessionID);

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getapmcvendorlist?userid={userID}&sessionid={sessionID}")]
        List<APMCVendor> GetAPMCVendorList(int userID, string sessionID);

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getapmcsettingslist?apmcid={APMCID}&userid={userID}&sessionid={sessionID}")]
        List<APMCSettings> GetAPMCSettingsList(int APMCID, int userID, string sessionID);

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getapmc?apmcid={apmcID}&userid={userID}&sessionid={sessionID}")]
        APMC GetAPMC(int apmcID, int userID, string sessionID);

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getapmcaudit?apmcnegid={apmcnegID}&vendorid={vendorID}&userid={userID}&sessionid={sessionID}")]
        List<APMCAudit> GetAPMCAudit(int apmcnegID, int userID, int vendorID, string sessionID);

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getapmcvendorquantitylist?apmcnegid={apmcnegid}&vendorid={vendorID}&userid={userID}&sessionid={sessionID}")]
        List<APMCVendorQuantity> GetAPMCVendorQuantityList(int apmcnegid, int userID, int vendorID, string sessionID);

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getapmcnegotiationlist?userid={userID}&sessionid={sessionID}&fromdate={fromdate}&todate={todate}")]
        APMCDashboard GetAPMCNegotiationList(int userID, string sessionID, DateTime fromdate, DateTime todate);


        [OperationContract]
        [WebInvoke(Method = "POST",
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "saveapmcvendorquantitylist")]
        Response SaveAPMCVendorQuantityList(List<APMCVendorQuantity> list);

        [OperationContract]
        [WebInvoke(Method = "POST",
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "saveapmc")]
        Response SaveAPMC(APMC apmc);

        [OperationContract]
        [WebInvoke(Method = "POST",
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "saveapmcsettings")]
        Response SaveAPMCSettings(APMCSettings apmcSettings);

        [OperationContract]
        [WebInvoke(Method = "POST",
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "saveapmcvendor")]
        Response SaveAPMCVendor(APMCVendor apmcVendor);

        [OperationContract]
        [WebInvoke(Method = "POST",
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "saveapmcinput")]
        Response SaveAPMCInput(APMCInput apmcInput);

        [OperationContract]
        [WebInvoke(Method = "POST",
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "saveapmcnegotiation")]
        Response SaveAPMCNegotiation(APMCNegotiation apmcNegotiation);

        [OperationContract]
        [WebInvoke(Method = "POST",
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "saveapmcnegotiationlist")]
        Response SaveAPMCNegotiationList(List<APMCNegotiation> apmcNegotiationList, int apmcNegotiationID, int userID);

        [OperationContract]
        [WebInvoke(Method = "POST",
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "checkuniqueifexists")]
        bool CheckUniqueIfExists(string param, string idtype, string sessionID);
    }
}
