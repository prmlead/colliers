﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using System.Configuration;
using System.Data.SqlClient;
using PRMServices.Models;
using SendGrid.Helpers.Mail;
using System.IO;

namespace PRMServices
{
    [ServiceContract]
    public interface IPRMPOService
    {
        [WebInvoke(Method = "POST",
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        ResponseFormat = WebMessageFormat.Json,
        RequestFormat = WebMessageFormat.Json,
        UriTemplate = "editPODetails")]
        Response EditPODetails(POItem item, string sessionId);

        [WebInvoke(Method = "POST",
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        ResponseFormat = WebMessageFormat.Json,
        RequestFormat = WebMessageFormat.Json,
        UriTemplate = "editPOItemDetails")]
        Response EditPOItemDetails(POItem item, string sessionId);

        [WebInvoke(Method = "POST",
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        ResponseFormat = WebMessageFormat.Json,
        RequestFormat = WebMessageFormat.Json,
        UriTemplate = "savePODetails")]
        Response SavePODetails(List<POItem> details, string sessionId);

        [WebInvoke(Method = "POST",
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        ResponseFormat = WebMessageFormat.Json,
        RequestFormat = WebMessageFormat.Json,
        UriTemplate = "savePOtermsandconditions")]
        Response savePOtermsandconditions(string PO_NUMBER, List<TermsConditions> TERMS_CONDITIONS, string sessionId);

        [WebInvoke(Method = "POST",
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        ResponseFormat = WebMessageFormat.Json,
        RequestFormat = WebMessageFormat.Json,
        UriTemplate = "savePOPDFDetails")]
        Response SavePOPDFDetails(POItem detail, string sessionId);

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getBillRequests?COMP_ID={COMP_ID}&U_ID={U_ID}" +
            "&IS_CUSTOMER={IS_CUSTOMER}&sessionid={sessionid}&poNumber={poNumber}&projectIds={projectIds}&billsStatus={billsStatus}&packageIds={packageIds}&subPackageIds={subPackageIds}&vendorIds={vendorIds}&fromDate={fromDate}&toDate={toDate}&PageSize={PageSize}&NumberOfRecords={NumberOfRecords}")]
        string GetBillRequests(int COMP_ID, int U_ID, int IS_CUSTOMER, string sessionid, string poNumber, string projectIds,string billsStatus, string packageIds, string subPackageIds, string vendorIds,
            string fromDate, string toDate, int PageSize = 0, int NumberOfRecords = 0);

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getPOScheduleDetails?COMP_ID={COMP_ID}&PO_NUMBER={PO_NUMBER}&VALIDATE={VALIDATE}&sessionid={sessionid}")]
        string GetPOScheduleDetails(int COMP_ID, string PO_NUMBER, int VALIDATE, string sessionid);

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getInvoicedetails?COMP_ID={COMP_ID}&INVOICE_ID={INVOICE_ID}&sessionid={sessionid}")]
        string GetInvoiceDetails(int COMP_ID, string INVOICE_ID, string sessionid);

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "GetInvoiceAmountForPO?U_ID={U_ID}&COMP_ID={COMP_ID}&PO_NUMBER={PO_NUMBER}&sessionid={sessionid}")]
        string GetInvoiceAmountForPO(int U_ID, int COMP_ID, string PO_NUMBER, string sessionid);


        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getBillRequestsFilterValues?COMP_ID={COMP_ID}&U_ID={U_ID}&IS_CUSTOMER={IS_CUSTOMER}&FROM_DATE={FROM_DATE}&TO_DATE={TO_DATE}&sessionid={sessionid}")]
        string GetBillRequestsFilterValues(int COMP_ID, int U_ID, int IS_CUSTOMER,string FROM_DATE,string TO_DATE,  string sessionid);


        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getInvoiceFilterValues?COMP_ID={COMP_ID}&U_ID={U_ID}&IS_CUSTOMER={IS_CUSTOMER}&fromDate={fromDate}&toDate={toDate}&sessionid={sessionid}")]
        string GetInvoiceFilterValues(int COMP_ID, int U_ID, int IS_CUSTOMER, string fromDate, string toDate, string sessionid);

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getPOFilters?compId={compId}&sessionId={sessionId}")]
        List<CArrayKeyValue> GetPOFilters(int compId, string sessionId);

        [OperationContract]
        [WebInvoke(Method = "POST",
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "getPOList")]
        List<POItem> GetPOList(int compId, int vendorId, int projectId, string poNumber,
            string projectIds, string packageIds, string costCenters, string vendorIds, string searchKeyword, string fromdate, string todate,
            int page, int size, string sessionId);

        [OperationContract]
        [WebInvoke(Method = "POST",
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "getPODetails")]
        List<POItem> GetPODetails(string poNumber, string sessionId);

        [OperationContract]
        [WebInvoke(Method = "POST",
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "amendPODetails")]
        Response AmendPODetails(List<POItem> poAmendItemsDetails, int poAmendmentID, string sessionId);

        [OperationContract]
        [WebInvoke(Method = "POST",
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "UpdatePORevisionNumber")]
        Response UpdatePORevisionNumber(string poNumber, List<POItem> poDetails, string sessionId);
        
        [OperationContract]
        [WebInvoke(Method = "POST",
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "getPOAmendmentDetails")]
        List<POItem> GetPOAmendmentDetails(string poNumber, int PO_AMENDMENT_ID, string sessionId);


        [OperationContract]
        [WebInvoke(Method = "POST",
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "createBillSubmission")]
        Response CreateBillSubmission(List<BillRequests> billRequestList, int BILL_ID, string sessionid);

        [OperationContract]
        [WebInvoke(Method = "POST",
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "acknowledgeOrRejectBillRequest")]
        Response AcknowledgeOrRejectBillRequest(BillRequests billSubmission, string sessionid);

        [OperationContract]
        [WebInvoke(Method = "POST",
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "submitForBillCertification")]
        Response SubmitForBillCertification(BillRequests billRequest, string sessionid);

        [OperationContract]
        [WebInvoke(Method = "POST",
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "uploadBillCertification")]
        Response UploadBillCertification(BillRequests billCertificates, string sessionid);

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getBillsBasedOnID?PO_NUMBER={PO_NUMBER}&BILL_ID={BILL_ID}&sessionid={sessionid}")]
        string GetBillsBasedOnID(string PO_NUMBER, int BILL_ID,string sessionid);

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getPODetailsBasedOnID?PO_NUMBER={PO_NUMBER}&sessionid={sessionid}")]
        string GetPODetailsBasedOnID(string PO_NUMBER, string sessionid);

        [OperationContract]
        [WebInvoke(Method = "POST",
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "saveInvoiceDetails")]
        Response SaveInvoiceDetails(List<InvoiceDetails> invoiceDetailsList, int invocieId, string sessionid);

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getInvoicesList?COMP_ID={COMP_ID}&U_ID={U_ID}&IS_CUSTOMER={IS_CUSTOMER}&fromDate={fromDate}&toDate={toDate}&poNumber={poNumber}&projectIds={projectIds}&invoiceNumbers={invoiceNumbers}&invoiceStatus={invoiceStatus}&sessionid={sessionid}&PageSize={PageSize}&NumberOfRecords={NumberOfRecords}")]
        string GetInvoicesList(int COMP_ID, int U_ID, int IS_CUSTOMER, string fromDate, string toDate, string poNumber, string projectIds, string invoiceNumbers, string invoiceStatus, string sessionid, int PageSize = 0, int NumberOfRecords = 0);

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "GetPoAudit?COMP_ID={COMP_ID}&PO_NUMBER={PO_NUMBER}&sessionid={sessionid}")]
        string GetPoAudit(int COMP_ID, string PO_NUMBER, string sessionid);


        [OperationContract]
        [WebInvoke(Method = "POST",
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "deactiveBillRequest")]
        Response deactiveBillRequest(int BILL_ID,int IS_VALID,int MODIFIED_BY, string sessionid);


        [OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "pdfSave?tcData={tcData}&poNumber={poNumber}&compId={compId}&revision={revision}&sessionId={sessionId}&saveToServer={saveToServer}&validateSession={validateSession}")]
        FileResponse PDFSave(dynamic tcData, string poNumber, int compId, int revision, string sessionId, bool saveToServer, bool validateSession = false);


        //[WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getposchedulelist?compid={compid}&uid={uid}&search={search}" +
        //   "&categoryid={categoryid}&productid={productid}&supplier={supplier}&postatus={postatus}&deliverystatus={deliverystatus}&plant={plant}&fromdate={fromdate}&todate={todate}&page={page}&pagesize={pagesize}" +
        //   "&onlycontracts={onlycontracts}&excludecontracts={excludecontracts}&ackStatus={ackStatus}&buyer={buyer}&purchaseGroup={purchaseGroup}&sessionid={sessionid}")]
        //List<POScheduleDetails> GetPOScheduleList(int compid, int uid, string search, string categoryid, string productid, string supplier, string postatus, string deliverystatus, string plant,
        //   string fromdate, string todate, int page, int pagesize, int onlycontracts, int excludecontracts, string ackStatus, string buyer, string purchaseGroup, string sessionid);
    }
}
