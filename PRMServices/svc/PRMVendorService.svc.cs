﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.Activation;
using PRM.Core;
using PRM.Services.Vendors;
using PRMServices.Models;
using PRM.Services.Configurations;
using PRMServices.Models.Vendor;
using PRM.Core.Domain.Vendors;
using PRMServices.SQLHelper;
using System.Configuration;
using System.Threading.Tasks;
using System.Net.Mail;
using System.Xml;
using System.IO;
using System.Data;

namespace PRMServices
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "PRMVendorService" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select PRMVendorService.svc or PRMVendorService.svc.cs at the Solution Explorer and start debugging.
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class PRMVendorService : IPRMVendorService
    {
        private IDatabaseHelper sqlHelper = DatabaseProvider.GetDatabaseProvider();
        public PRMVendorService(IWorkContext workingContext, IVendorService vendorServcie, IRegistrationFormService registrationFormService)
        {
            _workingContext = workingContext;
            _vendorServcie = vendorServcie;
            _registrationFormService = registrationFormService;
        }

        private readonly IWorkContext _workingContext;
        private readonly IVendorService _vendorServcie;
        private readonly IRegistrationFormService _registrationFormService;

        public Response CalculateVendorScore(string userId)
        {
            var score = _vendorServcie.CalculateScore(_workingContext.CurrentCompany, Convert.ToInt32(userId));

            return new Response() { ObjectID = score };
        }

        public List<VendorScoreModel>  GetVendorScore(string vendorId)
        {
            var list = _vendorServcie.GetVendorScore(_workingContext.CurrentCompany,Convert.ToInt32(vendorId)).ToList();

            var ModelList = new List<VendorScoreModel>();
            if(list != null && list.Any())
            {
                list.ForEach(e =>
                {
                    ModelList.Add(new VendorScoreModel() {
                        DefaultValue = Convert.ToInt32(e["DefaultValue"]),
                         Field = Convert.ToString(e["Field"]),
                          Value = Convert.ToInt32(e["Value"])
                    });

                });
            }


            return ModelList;
        }

        public Response SaveVendorScore(VendorScoreUpdateModel model)
        {
            var list = new List<VendorScore>();
           foreach(var score in model.Scores)
            {
                list.Add(new VendorScore() { Field = score.Field, Value = score.Value });
            }
            _vendorServcie.SaveVendorScore(model.User_Id, list);

            return new Response() { Message = "Updated successfully" };

        }


        public int[] GetVendorSubCategories(string userId)
        {
            return _vendorServcie.GetVendorSubCategort(Convert.ToInt32(userId));
        }

        public Response SaveVendorStatus(VendorStatusUpdateModel model)
        {
            var venorr = _vendorServcie.GetVendorList(new { U_ID = model.User_Id }).FirstOrDefault();
            if(venorr != null)
            {
                venorr.Status = model.Status;
                venorr.Reason = model.Reason;
                venorr.Comment = model.Comment;
                _vendorServcie.UpdateVendor(venorr);
                _vendorServcie.UpdateVedorCategory(model.User_Id, model.Category);
                _vendorServcie.UpdateVedorSubCategory(model.User_Id, _workingContext.CurrentCompany, model.SubCategories);

                if (model.Status == 4)
                {
                    string websiteURL = ConfigurationManager.AppSettings["WEBSITE_URL"] + "/prm360.html#/vendorRegistration1/" + model.User_Id;
                    string body = $@"Hi {venorr.FirstName}, <br/> Your pre-qualification form has been rejected. Please visit the <a href='{websiteURL}'> link</a> to re-submit the form.";
                    XmlDocument doc = new XmlDocument();
                    doc.Load(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "bin") + "/EmailTemplates/EmailFormats.xml");
                    XmlNode footernode = doc.DocumentElement.SelectSingleNode("EmailFooter");
                    body += footernode.InnerText;
                    string query = $"delete from workflowtrack where MODULE_ID = {model.User_Id} and wf_id IN (select wf_id from workflows where COMP_ID = dbo.GetCompanyID({ConfigurationManager.AppSettings["COMPANY_ID"]}) and WF_TITLE = 'VENDOR_FORM_WORKFLOW');";
                    sqlHelper.ExecuteNonQuery_IUD(query);
                    SendEmail(venorr.Email, "PREQUALIFICATION FORM - Rejected", body, model.SessionID, null, null).ConfigureAwait(false);
                }
            }

            return new Response() { Message = "Vendor status updated" };

        }

        public List<Vendor> GetVendorPreQualificationDetails(int vendorId, int compId, string sessionId)
        {
            List<Vendor> vendors = new List<Vendor>();
            try
            {
                Utilities.ValidateSession(sessionId);
                string query = $"SELECT VF.*, V.U_EMAIL, V.COMP_NAME FROM VendorForm VF INNER JOIN vendors V ON V.U_ID = VF.VENDOR_ID AND V.IS_PRIMARY = 1 where V.COMP_ID = {compId} AND FORM_JSON NOT IN ( '[]') ";
                if (vendorId > 0)
                {
                    query += " AND VF.VENDOR_ID = " + vendorId;
                }

                var dataset = sqlHelper.ExecuteQuery(query);
                if (dataset != null && dataset.Tables.Count > 0 && dataset.Tables[0].Rows.Count > 0)
                {
                    foreach (var row in dataset.Tables[0].AsEnumerable())
                    {
                        var vendor = new Vendor();
                        vendor.Id = row["VENDOR_ID"] != DBNull.Value ? Convert.ToInt32(row["VENDOR_ID"]) : 0;
                        vendor.Email = row["U_EMAIL"] != DBNull.Value ? Convert.ToString(row["U_EMAIL"]) : "";
                        vendor.CompanyName = row["COMP_NAME"] != DBNull.Value ? Convert.ToString(row["COMP_NAME"]) : "";
                        vendor.AdditionalInfo = row["FORM_JSON"] != DBNull.Value ? Convert.ToString(row["FORM_JSON"]) : "";
                        vendors.Add(vendor);
                    }
                }
            }
            catch (Exception ex)
            {
                vendors.Clear();
            }

            return vendors;
        }

        public async Task SendEmail(string To, string Subject, string Body, string sessionID = null, Attachment attachment = null, List<Attachment> ListAttachment = null)
        {
            try
            {
                PRMServices prm = new PRMServices();
                Communication communication = new Communication();
                communication = Utilities.GetCommunicationData("EMAIL_FROM", sessionID);
                Body = Body.Replace("COMPANY_LOGO_ID", communication.Company.CompanyID > 0 ? communication.Company.CompanyID.ToString() : "0");
                Body = Body.Replace("COMPANY_NAME", !string.IsNullOrEmpty(communication.Company.CompanyName) ? communication.Company.CompanyName : "PRM360");
                Body = Body.Replace("USER_NAME", !string.IsNullOrEmpty(communication.User.FirstName + " " + communication.User.LastName) ? communication.User.FirstName + " " + communication.User.LastName : "");
                Body = Body.Replace("COMPANY_ADDRESS", !string.IsNullOrEmpty(communication.CompanyAddress) ? communication.CompanyAddress : "");
                SmtpClient smtpClient = new SmtpClient(ConfigurationManager.AppSettings["MAILHOST"].ToString());
                System.Net.NetworkCredential credentials = new System.Net.NetworkCredential(ConfigurationManager.AppSettings["MAILHOST_USER"].ToString(), ConfigurationManager.AppSettings["MAILHOST_PWD"].ToString());
                smtpClient.Credentials = credentials;
                smtpClient.EnableSsl = false;
                MailMessage mail = new MailMessage();
                mail.From = new MailAddress(prm.GetFromAddress(""), prm.GetFromDisplayName(""));
                mail.BodyEncoding = System.Text.Encoding.ASCII;
                List<string> ToAddresses = To.Split(',').ToList<string>();
                foreach (string address in ToAddresses)
                {
                    if (!string.IsNullOrEmpty(address))
                    {
                        mail.To.Add(new MailAddress(address));
                    }

                }

                mail.Subject = Subject;
                mail.IsBodyHtml = true;
                mail.Body = Body;
                await smtpClient.SendMailAsync(mail);
                Response response = new Response();
                response.ObjectID = 1;
            }
            catch (Exception ex)
            {
                Response response = new Response();
                response.ErrorMessage = ex.Message;
            }
        }
    }
}
