﻿using PRMServices.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.ServiceModel.Activation;
using PRMServices.SQLHelper;

namespace PRMServices
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select Service1.svc or Service1.svc.cs at the Solution Explorer and start debugging.
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]

    public class PRMPriceCapService : IPRMPriceCapService
    {
        private IDatabaseHelper sqlHelper = DatabaseProvider.GetDatabaseProvider();
        #region Service Calls

        public Requirement GetReqData(int reqID, string sessionID)
        {
            Requirement requirement = new Requirement();
            try
            {
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                sd.Add("P_REQ_ID", reqID);
                DataSet ds = sqlHelper.SelectList("pc_GetReqData", sd);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    DataRow row = ds.Tables[0].Rows[0];
                    string[] arr = new string[] { };
                    byte[] test = new byte[] { };
                    requirement.Title = row["REQ_TITLE"] != DBNull.Value ? Convert.ToString(row["REQ_TITLE"]) : string.Empty;
                    requirement.Description = row["REQ_DESC"] != DBNull.Value ? Convert.ToString(row["REQ_DESC"]) : string.Empty;
                    requirement.RequirementID = reqID;

                    List<VendorDetails> vendorDetails = new List<VendorDetails>();
                    if (ds.Tables[1].Rows.Count > 0)
                    {
                        foreach (DataRow row1 in ds.Tables[1].Rows)
                        {
                            VendorDetails vendor = new VendorDetails();
                            vendor.PO = new RequirementPO();
                            vendor.VendorID = row1["U_ID"] != DBNull.Value ? Convert.ToInt32(row1["U_ID"]) : 0;
                            vendorDetails.Add(vendor);
                        }
                    }

                    int productSNo = 0;
                    List<RequirementItems> ListRequirementItems = new List<RequirementItems>();
                    if (ds.Tables[2].Rows.Count > 0)
                    {
                        foreach (DataRow row2 in ds.Tables[2].Rows)
                        {
                            RequirementItems RequirementItems = new RequirementItems();
                            RequirementItems.ProductSNo = productSNo++;
                            RequirementItems.ItemID = row2["ITEM_ID"] != DBNull.Value ? Convert.ToInt32(row2["ITEM_ID"]) : 0;
                            RequirementItems.RequirementID = row2["REQ_ID"] != DBNull.Value ? Convert.ToInt32(row2["REQ_ID"]) : 0;
                            RequirementItems.ProductQuantity = row2["QUANTITY"] != DBNull.Value ? Convert.ToDouble(row2["QUANTITY"]) : 0;
                            RequirementItems.OthersBrands = row2["OTHER_BRAND"] != DBNull.Value ? Convert.ToString(row2["OTHER_BRAND"]) : string.Empty;
                            RequirementItems.ProductImageID = row2["IMAGE_ID"] != DBNull.Value ? Convert.ToInt32(row2["IMAGE_ID"]) : 0;
                            RequirementItems.IsDeleted = row2["IS_DELETED"] != DBNull.Value ? Convert.ToInt32(row2["IS_DELETED"]) : 0;
                            RequirementItems.ProductQuantityIn = row2["QUANTITY_IN"] != DBNull.Value ? Convert.ToString(row2["QUANTITY_IN"]) : string.Empty;
                            RequirementItems.SelectedVendorID = row2["SELECTED_VENDOR_ID"] != DBNull.Value ? Convert.ToInt32(row2["SELECTED_VENDOR_ID"]) : 0;

                            RequirementItems.ProductIDorName = row2["PROD_ID"] != DBNull.Value ? Convert.ToString(row2["PROD_ID"]) : string.Empty;
                            RequirementItems.ProductNo = row2["PROD_NO"] != DBNull.Value ? Convert.ToString(row2["PROD_NO"]) : string.Empty;
                            RequirementItems.ProductDescription = row2["DESCRIPTION"] != DBNull.Value ? Convert.ToString(row2["DESCRIPTION"]) : string.Empty;
                            RequirementItems.ProductBrand = row2["BRAND"] != DBNull.Value ? Convert.ToString(row2["BRAND"]) : string.Empty;
                            ListRequirementItems.Add(RequirementItems);
                        }
                        requirement.ItemSNoCount = productSNo;
                        requirement.ListRequirementItems = ListRequirementItems;
                    }

                    List<RequirementItems> ListQuotations = new List<RequirementItems>();
                    if (ds.Tables[3].Rows.Count > 0)
                    {
                        foreach (DataRow row3 in ds.Tables[3].Rows)
                        {
                            RequirementItems Quotation = new RequirementItems();
                            Quotation.ItemID = row3["ITEM_ID"] != DBNull.Value ? Convert.ToInt32(row3["ITEM_ID"]) : 0;
                            Quotation.ItemPrice = row3["PRICE"] != DBNull.Value ? Convert.ToDouble(row3["PRICE"]) : 0;
                            Quotation.OthersBrands = row3["OTHER_BRANDS"] != DBNull.Value ? Convert.ToString(row3["OTHER_BRANDS"]) : string.Empty; ;
                            Quotation.RevItemPrice = row3["REVICED_PRICE"] != DBNull.Value ? Convert.ToDouble(row3["REVICED_PRICE"]) : 0; ;
                            Quotation.UnitPrice = row3["UNIT_PRICE"] != DBNull.Value ? Convert.ToDouble(row3["UNIT_PRICE"]) : 0; ;
                            Quotation.RevUnitPrice = row3["REV_UNIT_PRICE"] != DBNull.Value ? Convert.ToDouble(row3["REV_UNIT_PRICE"]) : 0; ;
                            Quotation.CGst = row3["C_GST"] != DBNull.Value ? Convert.ToDouble(row3["C_GST"]) : 0; ;
                            Quotation.SGst = row3["S_GST"] != DBNull.Value ? Convert.ToDouble(row3["S_GST"]) : 0; ;
                            Quotation.IGst = row3["I_GST"] != DBNull.Value ? Convert.ToDouble(row3["I_GST"]) : 0; ;
                            Quotation.UnitMRP = row3["UNIT_MRP"] != DBNull.Value ? Convert.ToDouble(row3["UNIT_MRP"]) : 0; ;
                            Quotation.UnitDiscount = row3["UNIT_DISCOUNT"] != DBNull.Value ? Convert.ToDouble(row3["UNIT_DISCOUNT"]) : 0; ;
                            Quotation.RevUnitDiscount = row3["REV_UNIT_DISCOUNT"] != DBNull.Value ? Convert.ToDouble(row3["REV_UNIT_DISCOUNT"]) : 0; ;
                            ListQuotations.Add(Quotation);
                        }
                    }

                    foreach (var item in requirement.ListRequirementItems)
                    {
                        foreach (var quote in ListQuotations)
                        {
                            if (item.ItemID == quote.ItemID)
                            {
                                item.ItemPrice = quote.ItemPrice;
                                item.OthersBrands = quote.OthersBrands;
                                item.RevItemPrice = quote.RevItemPrice;
                                item.UnitPrice = quote.UnitPrice;
                                item.RevUnitPrice = quote.RevUnitPrice;
                                item.CGst = quote.CGst;
                                item.SGst = quote.SGst;
                                item.IGst = quote.IGst;
                                item.UnitMRP = quote.UnitMRP;
                                item.UnitDiscount = quote.UnitDiscount;
                                item.RevUnitDiscount = quote.RevUnitDiscount;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Requirement req = new Requirement();
                req.ErrorMessage = ex.Message;
                return req;
            }

            return requirement;
        }

        public Response SavePriceCap(int reqID, List<RequirementItems> listReqItems, 
            double price, int isDiscountQuotation, string sessionID)
        {
            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            Response response = new Response();
            try
            {
                foreach (var item in listReqItems)
                {
                    sd = new SortedDictionary<object, object>() { };
                    sd.Add("P_REQ_ID", item.RequirementID);
                    sd.Add("P_ITEM_ID", item.ItemID);
                    sd.Add("P_PRICE", item.ItemPrice);
                    sd.Add("P_REVICED_PRICE", item.RevItemPrice);
                    sd.Add("P_UNIT_PRICE", item.UnitPrice);
                    sd.Add("P_REV_UNIT_PRICE", item.RevUnitPrice);
                    sd.Add("P_C_GST", item.CGst);
                    sd.Add("P_S_GST", item.SGst);
                    sd.Add("P_I_GST", item.IGst);
                    sd.Add("P_UNIT_MRP", item.UnitMRP);
                    sd.Add("P_UNIT_DISCOUNT", item.UnitDiscount);
                    sd.Add("P_REV_UNIT_DISCOUNT", item.RevUnitDiscount);
                    sqlHelper.SelectList("pc_SavePriceCapItemPrices", sd);
                }

                sd = new SortedDictionary<object, object>() { };
                sd.Add("P_REQ_ID", reqID);
                sd.Add("P_PRICE", price);
                sd.Add("P_IS_DISCOUNT_QUOTATION", isDiscountQuotation);
                DataSet ds = sqlHelper.SelectList("pc_SavePriceCap", sd);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
                {
                    response.ObjectID = ds.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToInt16(ds.Tables[0].Rows[0][0].ToString()) : -1;
                    response.ErrorMessage = ds.Tables[0].Rows[0][1] != DBNull.Value ? Convert.ToString(ds.Tables[0].Rows[0][1].ToString()) : string.Empty;
                }
            }
            catch (Exception ex)
            {
                response.ErrorMessage = ex.Message;
            }

            return response;
        }

        #endregion
    }
}