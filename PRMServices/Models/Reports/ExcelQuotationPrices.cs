﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;
using PRMServices.Models.Catalog;

namespace PRMServices.Models
{
    [DataContract]
    public class ExcelQuotationPrices : Entity
    {
        [DataMember(Name = "itemID")]
        public int ItemID { get; set; }

        [DataMember(Name = "requirementID")]
        public int RequirementID { get; set; }

        string productIDorName = string.Empty;
        [DataMember(Name = "productIDorName")]
        public string ProductIDorName
        {
            get
            {
                if (!string.IsNullOrEmpty(productIDorName))
                { return productIDorName; }
                else
                { return ""; }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                { productIDorName = value; }
            }
        }

        string productIDorNameCustomer = string.Empty;
        [DataMember(Name = "productIDorNameCustomer")]
        public string ProductIDorNameCustomer
        {
            get
            {
                if (!string.IsNullOrEmpty(productIDorNameCustomer))
                { return productIDorNameCustomer; }
                else
                { return ""; }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                { productIDorNameCustomer = value; }
            }
        }

        string productNo = string.Empty;
        [DataMember(Name = "productNo")]
        public string ProductNo
        {
            get
            {
                if (!string.IsNullOrEmpty(productNo))
                { return productNo; }
                else
                { return ""; }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                { productNo = value; }
            }
        }

        string productNoCustomer = string.Empty;
        [DataMember(Name = "productNoCustomer")]
        public string ProductNoCustomer
        {
            get
            {
                if (!string.IsNullOrEmpty(productNoCustomer))
                { return productNoCustomer; }
                else
                { return ""; }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                { productNoCustomer = value; }
            }
        }

        string productDescription = string.Empty;
        [DataMember(Name = "productDescription")]
        public string ProductDescription
        {
            get
            {
                if (!string.IsNullOrEmpty(productDescription))
                { return productDescription; }
                else
                { return ""; }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                { productDescription = value; }
            }
        }

        string productDescriptionCustomer = string.Empty;
        [DataMember(Name = "productDescriptionCustomer")]
        public string ProductDescriptionCustomer
        {
            get
            {
                if (!string.IsNullOrEmpty(productDescriptionCustomer))
                { return productDescriptionCustomer; }
                else
                { return ""; }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                { productDescriptionCustomer = value; }
            }
        }

        string productBrand = string.Empty;
        [DataMember(Name = "productBrand")]
        public string ProductBrand
        {
            get
            {
                if (!string.IsNullOrEmpty(productBrand))
                { return productBrand; }
                else
                { return ""; }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                { productBrand = value; }
            }
        }

        string productBrandCustomer = string.Empty;
        [DataMember(Name = "productBrandCustomer")]
        public string ProductBrandCustomer
        {
            get
            {
                if (!string.IsNullOrEmpty(productBrandCustomer))
                { return productBrandCustomer; }
                else
                { return ""; }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                { productBrandCustomer = value; }
            }
        }

        string othersBrands = string.Empty;
        [DataMember(Name = "othersBrands")]
        public string OthersBrands
        {
            get
            {
                if (!string.IsNullOrEmpty(othersBrands))
                { return othersBrands; }
                else
                { return ""; }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                { othersBrands = value; }
            }
        }

        [DataMember(Name = "isDeleted")]
        public int IsDeleted { get; set; }
       

        [DataMember(Name = "productQuantity")]
        public double ProductQuantity { get; set; }

        string productQuantityIn = string.Empty;
        [DataMember(Name = "productQuantityIn")]
        public string ProductQuantityIn
        {
            get
            {
                if (!string.IsNullOrEmpty(productQuantityIn))
                { return productQuantityIn; }
                else
                { return ""; }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                { productQuantityIn = value; }
            }
        }

        [DataMember(Name = "productImageID")]
        public int ProductImageID { get; set; }

        [DataMember(Name = "createdDate")]
        public DateTime? CreatedDate { get; set; }

        string attachmentName = string.Empty;
        [DataMember(Name = "attachmentName")]
        public string AttachmentName
        {
            get
            {
                if (!string.IsNullOrEmpty(attachmentName))
                { return attachmentName; }
                else
                { return ""; }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                { attachmentName = value; }
            }
        }

        string attachmentBase64 = string.Empty;
        [DataMember(Name = "attachmentBase64")]
        public string AttachmentBase64
        {
            get
            {
                if (!string.IsNullOrEmpty(attachmentBase64))
                { return attachmentBase64; }
                else
                { return ""; }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                { attachmentBase64 = value; }
            }
        }

        [DataMember(Name = "itemAttachment")]
        public byte[] ItemAttachment { get; set; }

        [DataMember(Name = "quotation")]
        public byte[] Quotation { get; set; }

        [DataMember(Name = "fileType")]
        public string FileType { get; set; }

        [DataMember(Name = "fileId")]
        public int FileId { get; set; }

        [DataMember(Name = "itemPrice")]
        public double ItemPrice { get; set; }

        [DataMember(Name = "tax")]
        public double Tax { get; set; }

        [DataMember(Name = "productSNo")]
        public int ProductSNo { get; set; }

        [DataMember(Name = "selectedVendorID")]
        public int SelectedVendorID { get; set; }

        [DataMember(Name = "revisedItemPrice")]
        public double RevisedItemPrice { get; set; }

        [DataMember(Name = "revisedInitialprice")]
        public double RevisedInitialprice { get; set; }

        [DataMember(Name = "revisedFreightcharges")]
        public double RevisedFreightcharges { get; set; }

        [DataMember(Name = "revisedVendorBidPrice")]
        public double RevisedVendorBidPrice { get; set; }

        [DataMember(Name = "isRevised")]
        public int IsRevised { get; set; }

        [DataMember(Name = "revitemPrice")]
        public double RevItemPrice { get; set; }

        [DataMember(Name = "isRegret")]
        public bool IsRegret { get; set; }

        string regretComments = string.Empty;
        [DataMember(Name = "regretComments")]
        public string RegretComments
        {
            get
            {
                if (!string.IsNullOrEmpty(regretComments))
                { return regretComments; }
                else
                { return ""; }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                { regretComments = value; }
            }
        }

        string vendorUnits = string.Empty;
        [DataMember(Name = "vendorUnits")]
        public string VendorUnits
        {
            get
            {
                if (!string.IsNullOrEmpty(vendorUnits))
                { return vendorUnits; }
                else
                { return ""; }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                { vendorUnits = value; }
            }
        }

        string vendorBrand = string.Empty;
        [DataMember(Name = "vendorBrand")]
        public string VendorBrand
        {
            get
            {
                if (!string.IsNullOrEmpty(vendorBrand))
                { return vendorBrand; }
                else
                { return ""; }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                { vendorBrand = value; }
            }
        }

        [DataMember(Name = "unitPrice")]
        public double UnitPrice { get; set; }

        [DataMember(Name = "revUnitPrice")]
        public double RevUnitPrice { get; set; }

        [DataMember(Name = "cGst")]
        public double CGst { get; set; }

        [DataMember(Name = "sGst")]
        public double SGst { get; set; }

        [DataMember(Name = "iGst")]
        public double IGst { get; set; }

        [DataMember(Name = "unitMRP")]
        public double UnitMRP { get; set; }

        [DataMember(Name = "unitDiscount")]
        public double UnitDiscount { get; set; }

        [DataMember(Name = "revUnitDiscount")]
        public double RevUnitDiscount { get; set; }

        [DataMember(Name = "vendorID")]
        public int VendorID { get; set; }

        [DataMember(Name = "costPrice")]
        public double CostPrice { get; set; }

        [DataMember(Name = "netPrice")]
        public double NetPrice { get; set; }

        [DataMember(Name = "marginAmount")]
        public double MarginAmount { get; set; }


        string vendorRemarks = string.Empty;
        [DataMember(Name = "vendorRemarks")]
        public string VendorRemarks
        {
            get
            {
                if (!string.IsNullOrEmpty(vendorRemarks))
                { return vendorRemarks; }
                else
                { return ""; }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                { vendorRemarks = value; }
            }
        }


        string itemLevelInitialComments = string.Empty;
        [DataMember(Name = "itemLevelInitialComments")]
        public string ItemLevelInitialComments
        {
            get
            {
                if (!string.IsNullOrEmpty(itemLevelInitialComments))
                { return itemLevelInitialComments; }
                else
                { return ""; }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                { itemLevelInitialComments = value; }
            }
        }

        string itemLevelRevComments = string.Empty;
        [DataMember(Name = "itemLevelRevComments")]
        public string ItemLevelRevComments
        {
            get
            {
                if (!string.IsNullOrEmpty(itemLevelRevComments))
                { return itemLevelRevComments; }
                else
                { return ""; }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                { itemLevelRevComments = value; }
            }
        }

        [DataMember(Name = "itemFreightCharges")]
        public double ItemFreightCharges { get; set; }

        [DataMember(Name = "itemFreightTAX")]
        public double ItemFreightTAX { get; set; }

        [DataMember(Name = "itemRevFreightCharges")]
        public double ItemRevFreightCharges { get; set; }

        [DataMember(Name = "revitemPriceCB")]
        public double RevItemPriceCB { get; set; }

        [DataMember(Name = "revUnitPriceCB")]
        public double RevUnitPriceCB { get; set; }

        string productQuotationTemplateJson = string.Empty;
        [DataMember(Name = "productQuotationTemplateJson")]
        public string ProductQuotationTemplateJson
        {
            get
            {
                if (!string.IsNullOrEmpty(productQuotationTemplateJson))
                { return productQuotationTemplateJson; }
                else
                { return ""; }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                { productQuotationTemplateJson = value; }
            }
        }

        [DataMember(Name = "productQuotationTemplateArray")]
        public List<ProductQuotationTemplate> ProductQuotationTemplateArray { get; set; }


        // ITEM LEVEL RANKING //
        [DataMember(Name = "itemRank")]
        public int ItemRank { get; set; }
        // ITEM LEVEL RANKING //

    }
}