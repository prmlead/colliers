﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace PRMServices.Models
{
    [DataContract]
    public class ReportData : Entity
    {
        [DataMember(Name = "requirementID")]
        public int RequirementID { get; set; }

        [DataMember(Name = "customerID")]
        public int CustomerID { get; set; }

        string title = string.Empty;
        [DataMember(Name = "title")]
        public string Title
        {
            get
            {
                if (!string.IsNullOrEmpty(title))
                {
                    return title;
                }
                else
                {
                    return "";
                }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                {
                    title = value;
                }
            }
        }

        [DataMember(Name ="vendorID")]
        public int VendorID { get; set; }

        string companyName = string.Empty;
        [DataMember(Name = "companyName")]
        public string CompanyName
        {
            get
            {
                if (!string.IsNullOrEmpty(companyName))
                {
                    return companyName;
                }
                else
                {
                    return "";
                }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                {
                    companyName = value;
                }
            }
        }

        string userPhone = string.Empty;
        [DataMember(Name = "userPhone")]
        public string UserPhone
        {
            get
            {
                if (!string.IsNullOrEmpty(userPhone))
                {
                    return userPhone;
                }
                else
                {
                    return "";
                }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                {
                    userPhone = value;
                }
            }
        }

        string userEmail = string.Empty;
        [DataMember(Name = "userEmail")]
        public string UserEmail
        {
            get
            {
                if (!string.IsNullOrEmpty(userEmail))
                {
                    return userEmail;
                }
                else
                {
                    return "";
                }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                {
                    userEmail = value;
                }
            }
        }

        [DataMember(Name ="itemID")]
        public int ItemID { get; set; }

        string productBrand = string.Empty;
        [DataMember(Name = "productBrand")]
        public string ProductBrand
        {
            get
            {
                if (!string.IsNullOrEmpty(productBrand))
                {
                    return productBrand;
                }
                else
                {
                    return "";
                }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                {
                    productBrand = value;
                }
            }
        }

        string status = string.Empty;
        [DataMember(Name = "status")]
        public string Status
        {
            get
            {
                if (!string.IsNullOrEmpty(status))
                {
                    return status;
                }
                else
                {
                    return "";
                }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                {
                    status = value;
                }
            }
        }

        [DataMember(Name = "productQuantity")]
        public decimal ProductQuantity { get; set; }

        string productQuantityIn = string.Empty;
        [DataMember(Name = "productQuantityIn")]
        public string ProductQuantityIn
        {
            get
            {
                if (!string.IsNullOrEmpty(productQuantityIn))
                {
                    return productQuantityIn;
                }
                else
                {
                    return "";
                }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                {
                    productQuantityIn = value;
                }
            }
        }


        [DataMember(Name = "costPrice")]
        public decimal CostPrice { get; set; }

        [DataMember(Name = "revCostPrice")]
        public decimal RevCostPrice { get; set; }

        [DataMember(Name = "netPrice")]
        public decimal NetPrice { get; set; }

        [DataMember(Name = "revNetPrice")]
        public decimal RevNetPrice { get; set; }

        [DataMember(Name = "unitMRP")]
        public decimal UnitMRP { get; set; }

        [DataMember(Name = "marginAmount")]
        public decimal MarginAmount { get; set; }

        [DataMember(Name = "unitDiscount")]
        public decimal UnitDiscount { get; set; }

        [DataMember(Name = "revUnitDiscount")]
        public decimal RevUnitDiscount { get; set; }

        [DataMember(Name = "gst")]
        public decimal Gst { get; set; }
        
        [DataMember(Name = "maxInitMargin")]
        public decimal MaxInitMargin { get; set; }
        
        [DataMember(Name = "maxFinalMargin")]
        public decimal MaxFinalMargin { get; set; }

        [DataMember(Name ="savings")]
        public decimal Savings { get; set; }

        [DataMember(Name = "revMarginAmount")]
        public decimal RevMarginAmount { get; internal set; }

        DateTime postedDate = DateTime.MaxValue;
        [DataMember(Name = "postedDate")]
        public DateTime PostedDate {
            get
            {
                return postedDate;
            }
            set
            {
                postedDate = value;
            }
        }

        [DataMember(Name = "startTime")]
        public DateTime? StartTime { get; set; }

        string othersBrands = string.Empty;
        [DataMember(Name = "othersBrands")]
        public string OthersBrands
        {
            get
            {
                if (!string.IsNullOrEmpty(othersBrands))
                { return othersBrands; }
                else
                { return ""; }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                { othersBrands = value; }
            }
        }

    }
}