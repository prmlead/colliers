﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace PRMServices.Models
{
    [DataContract]
    public class FileUpload
    {
        [DataMember(Name = "fileStream")]
        public byte[] FileStream { get; set; }

        string fileName = string.Empty;
        [DataMember(Name = "fileName")]
        public string FileName
        {
            get
            {
                if (!string.IsNullOrEmpty(fileName))
                { return fileName; }
                else
                { return ""; }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                { fileName = value; }
            }
        }

        [DataMember(Name = "fileID")]
        public int FileID { get; set; }

        string fileType = string.Empty;
        [DataMember(Name = "fileType")]
        public string FileType
        {
            get
            {
                if (!string.IsNullOrEmpty(fileType))
                { return fileType; }
                else
                { return ""; }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                { fileType = value; }
            }
        }
    }
}