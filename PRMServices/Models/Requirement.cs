﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace PRMServices.Models
{
    [DataContract]
    public class Requirement : Entity
    {
        [DataMember(Name = "lotId")]
        public int LotId { get; set; }

        [DataMember(Name = "requirementID")]
        public int RequirementID { get; set; }

        [DataMember(Name = "requirementNumber")]
        public string RequirementNumber { get; set; }

        [DataMember(Name = "customerID")]
        public int CustomerID { get; set; }

        [DataMember(Name = "biddingType")]
        public string BiddingType { get; set; }

        [DataMember(Name = "custFirstName")]
        public string CustFirstName { get; set; }
        
        [DataMember(Name = "custLastName")]
        public string CustLastName { get; set; }

        //[DataMember(Name = "title")]
        //public string Title { get; set; }

        string title = string.Empty;
        [DataMember(Name = "title")]
        public string Title
        {
            get
            {
                if (!string.IsNullOrEmpty(title))
                { return title; }
                else
                { return ""; }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                { title = value; }
            }
        }

        [DataMember(Name = "description")]
        public string Description { get; set; }

        [DataMember(Name = "postedOn")]
        public DateTime? PostedOn { get; set; }

        [DataMember(Name = "startTime")]
        public DateTime? StartTime { get; set; }

        [DataMember(Name = "endTime")]
        public DateTime? EndTime { get; set; }

        DateTime quotationFreezTime = DateTime.MaxValue;
        [DataMember(Name = "quotationFreezTime")]
        public DateTime? QuotationFreezTime
        {
            get
            {
                return quotationFreezTime;
            }
            set
            {
                if (value != null && value.HasValue)
                {
                    quotationFreezTime = value.Value;
                }
            }
        }

        DateTime expStartTime = DateTime.MaxValue;
        [DataMember(Name = "expStartTime")]
        public DateTime? ExpStartTime
        {
            get
            {
                return expStartTime;
            }
            set
            {
                if (value != null && value.HasValue)
                {
                    expStartTime = value.Value;
                }
            }
        }

        [DataMember(Name = "budget")]
        public string Budget { get; set; }

        //[DataMember(Name = "deliveryLocation")]
        //public string DeliveryLocation { get; set; }


        string deliveryLocation = string.Empty;
        [DataMember(Name = "deliveryLocation")]
        public string DeliveryLocation
        {
            get
            {
                if (!string.IsNullOrEmpty(deliveryLocation))
                { return deliveryLocation; }
                else
                { return ""; }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                { deliveryLocation = value; }
            }
        }

        [DataMember(Name = "status")]
        public string Status { get; set; }

        [DataMember(Name = "urgency")]
        public string Urgency { get; set; }

        [DataMember(Name = "category")]
        public string[] Category { get; set; }

        [DataMember(Name = "taxes")]
        public string Taxes { get; set; }

        [DataMember(Name = "paymentTerms")]
        public string PaymentTerms { get; set; }

        [DataMember(Name = "isClosed")]
        public string IsClosed { get; set; }

        [DataMember(Name = "attachmentName")]
        public string AttachmentName { get; set; }

        [DataMember(Name = "auctionVendors")]
        public List<VendorDetails> AuctionVendors { get; set; }

        [DataMember(Name = "requirementVendorsList")]
        public List<VendorDetails> RequirementVendorsList { get; set; }

        [DataMember(Name = "customerEmails")]
        public List<CustomerEmails> CustomerEmails { get; set; }

        [DataMember(Name = "price")]
        public double Price { get; set; }

        [DataMember(Name = "timeLeft")]
        public long TimeLeft { get; set; }
        
        string deliveryTime = string.Empty;
        [DataMember(Name = "deliveryTime")]
        public string DeliveryTime
        {
            get
            {
                if (!string.IsNullOrEmpty(deliveryTime))
                {
                    return deliveryTime;
                }
                else
                {
                    return "";
                }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                {
                    deliveryTime = value;
                }
            }
        }
        
        [DataMember(Name = "inclusiveTax")]
        public bool InclusiveTax { get; set; }

        [DataMember(Name = "includeFreight")]
        public bool IncludeFreight { get; set; }

        [DataMember(Name="minBidAmount")]
        public double MinBidAmount { get; set; }

        [DataMember(Name = "minBid")]
        public int MinBid { get; set; }

        [DataMember(Name="superUserID")]
        public int SuperUserID { get; set; }

        [DataMember(Name = "selectedVendorID")]
        public int SelectedVendorID { get; set; }


        [DataMember(Name = "poLink")]
        public string POLink { get; set; }

        [DataMember(Name = "selectedVendor")]
        public VendorDetails SelectedVendor { get; set; }

        [DataMember(Name = "customerCompanyName")]
        public string CustomerCompanyName { get; set; }
        [DataMember(Name = "customerCompanyId")]
        public int CustomerCompanyId { get; set; }

        [DataMember(Name = "subcategories")]
        public string Subcategories { get; set; }

        [DataMember(Name="isStopped")]
        public bool IsStopped { get; set; }

        [DataMember(Name = "savings")]
        public Double Savings { get; set; }

        [DataMember(Name = "checkBoxSms")]
        public bool CheckBoxSms { get; set; }

        [DataMember(Name = "checkBoxEmail")]
        public bool CheckBoxEmail { get; set; }

        [DataMember(Name = "isNegotiationEnded")]
        public int IsNegotiationEnded { get; set; }

        [DataMember(Name = "minReduceAmount")]
        public double MinReduceAmount { get; set; }

        [DataMember(Name = "attachmentBase64")]
        public String AttachmentBase64 { get; set; }

        

        string currency = "INR";
        [DataMember(Name = "currency")]
        public string Currency 
        {
            get
            {
                if (!string.IsNullOrEmpty(currency))
                {
                    return currency;
                }
                else
                {
                    return "INR";
                }
            }
            set
            {
                if(!string.IsNullOrEmpty(value))
                {
                    currency = value;
                }                
            }
        }

        [DataMember(Name = "timeZoneID")]
        public int TimeZoneID { get; set; }

        string timeZone = string.Empty;
        [DataMember(Name = "timeZone")]      
        public string TimeZone
        {
            get
            {
                if (!string.IsNullOrEmpty(timeZone))
                {
                    return timeZone;
                }
                else
                {
                    return "";
                }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                {
                    timeZone = value;
                } 
            }
        }


        [DataMember(Name = "listRequirementItems")]
        public List<RequirementItems> ListRequirementItems { get; set; }

        [DataMember(Name = "isTabular")]
        public bool IsTabular { get; set; }

        string reqComments = string.Empty;
        [DataMember(Name = "reqComments")]      
        public string ReqComments
        {
            get
            {
                if (!string.IsNullOrEmpty(reqComments))
                {
                    return reqComments;
                }
                else
                {
                    return "";
                }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                {
                    reqComments = value;
                } 
            }
        }

        string reqComments1 = string.Empty;
        [DataMember(Name = "reqComments1")]
        public string ReqComments1
        {
            get
            {
                if (!string.IsNullOrEmpty(reqComments1))
                {
                    return reqComments1;
                }
                else
                {
                    return "";
                }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                {
                    reqComments1 = value;
                }
            }
        }

        [DataMember(Name = "isSubmit")]
        public int IsSubmit { get; set; }

        string negotiationDuration = string.Empty;
        [DataMember(Name = "negotiationDuration")]
        public string NegotiationDuration
        {
            get
            {
                if (!string.IsNullOrEmpty(negotiationDuration))
                {
                    return negotiationDuration;
                }
                else
                {
                    return "";
                }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                {
                    negotiationDuration = value;
                } 
            }
        }

        [DataMember(Name = "minVendorComparision")]
        public double MinVendorComparision { get; set; }

        [DataMember(Name = "vendorsCount")]
        public int VendorsCount { get; set; }


        string stringCategory = string.Empty;
        [DataMember(Name = "stringCategory")]
        public string StringCategory
        {
            get
            {
                if (!string.IsNullOrEmpty(stringCategory))
                {
                    return stringCategory;
                }
                else
                {
                    return "";
                }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                {
                    stringCategory = value;
                }
            }
        }

        [DataMember(Name = "leastVendorPrice")]
        public double LeastVendorPrice { get; set; }

        [DataMember(Name = "intialLeastVendorPrice")]
        public double IntialLeastVendorPrice { get; set; }

        [DataMember(Name = "NegotiationSettings")]
        public NegotiationSettings NegotiationSettings { get; set; }

        [DataMember(Name = "itemsAttachment")]
        public byte[] ItemsAttachment { get; set; }

        [DataMember(Name = "listRequirementTaxes")]
        public List<RequirementTaxes> ListRequirementTaxes { get; set; }

        [DataMember(Name = "itemSNoCount")]
        public int ItemSNoCount { get; set; }

        [DataMember(Name = "taxSNoCount")]
        public int TaxSNoCount { get; set; }

        [DataMember(Name = "reqPDF")]
        public int ReqPDF { get; set; }

        [DataMember(Name = "reqPDFCustomer")]
        public int ReqPDFCustomer { get; set; }


        [DataMember(Name = "noOfVendorsInvited")]
        public int NoOfVendorsInvited { get; set; }

        [DataMember(Name = "noOfVendorsParticipated")]
        public int NoOfVendorsParticipated { get; set; }

        [DataMember(Name = "priceBeforeNegotiation")]
        public double PriceBeforeNegotiation { get; set; }

        [DataMember(Name = "priceAfterNegotiation")]
        public double priceAfterNegotiation { get; set; }

        [DataMember(Name = "reportReq")]
        public int ReportReq { get; set; }

        [DataMember(Name = "reportItemWise")]
        public int ReportItemWise { get; set; }

        [DataMember(Name = "reqType")]
        public string ReqType { get; set; }

        [DataMember(Name = "isRFP")]
        public bool IsRFP { get; set; }

        [DataMember(Name = "priceCapValue")]
        public double PriceCapValue { get; set; }

        [DataMember(Name = "customerReqAccess")]
        public bool CustomerReqAccess { get; set; }

        [DataMember(Name = "custCompID")]
        public int CustCompID { get; set; }



        [DataMember(Name = "isQuotationPriceLimit")]
        public bool IsQuotationPriceLimit { get; set; }

        [DataMember(Name = "quotationPriceLimit")]
        public double QuotationPriceLimit { get; set; }

        [DataMember(Name = "noOfQuotationReminders")]
        public int NoOfQuotationReminders { get; set; }

        [DataMember(Name = "remindersTimeInterval")]
        public int RemindersTimeInterval { get; set; }

        [DataMember(Name = "postedUser")]
        public User PostedUser { get; set; }

        [DataMember(Name = "superUser")]
        public User SuperUser { get; set; }

        [DataMember(Name = "isUnitPriceBidding")]
        public int IsUnitPriceBidding { get; set; }

        [DataMember(Name = "deleteQuotations")]
        public bool DeleteQuotations { get; set; }

        [DataMember(Name = "materialDispachmentLink")]
        public int MaterialDispachmentLink { get; set; }

        [DataMember(Name = "materialReceivedLink")]
        public int MaterialReceivedLink { get; set; }

        [DataMember(Name = "isForwardBidding")]
        public bool IsForwardBidding { get; set; }

        [DataMember(Name = "listRequirementTerms")]
        public List<RequirementTerms> ListRequirementTerms { get; set; }

        [DataMember(Name = "indentID")]
        public int IndentID { get; set; }



        

        [DataMember(Name = "cloneID")]
        public int CloneID { get; set; }

        [DataMember(Name = "inv")]
        public InvolvedParties Inv
        {
            get;set;
        }

        [DataMember(Name = "isDiscountQuotation")]
        public int IsDiscountQuotation { get; set; }

        #region Forward Bidding

        string inspectionTimelines = string.Empty;
        [DataMember(Name = "inspectionTimelines")]
        public string InspectionTimelines
        {
            get
            {
                if (!string.IsNullOrEmpty(inspectionTimelines))
                {
                    return inspectionTimelines;
                }
                else
                {
                    return "";
                }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                {
                    inspectionTimelines = value;
                }
            }
        }

        string inspectionPOC = string.Empty;
        [DataMember(Name = "inspectionPOC")]
        public string InspectionPOC
        {
            get
            {
                if (!string.IsNullOrEmpty(inspectionPOC))
                {
                    return inspectionPOC;
                }
                else
                {
                    return "";
                }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                {
                    inspectionPOC = value;
                }
            }
        }

        string securityTerms = string.Empty;
        [DataMember(Name = "securityTerms")]
        public string SecurityTerms
        {
            get
            {
                if (!string.IsNullOrEmpty(securityTerms))
                {
                    return securityTerms;
                }
                else
                {
                    return "";
                }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                {
                    securityTerms = value;
                }
            }
        }

        [DataMember(Name = "securityAmount")]
        public decimal SecurityAmount { get; set; }
        #endregion

        string contactDetails = string.Empty;
        [DataMember(Name = "contactDetails")]
        public string ContactDetails
        {
            get
            {
                if (!string.IsNullOrEmpty(contactDetails))
                { return contactDetails; }
                else
                { return ""; }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                { contactDetails = value; }
            }
        }

        string generalTC = string.Empty;
        [DataMember(Name = "generalTC")]
        public string GeneralTC
        {
            get
            {
                if (!string.IsNullOrEmpty(generalTC))
                { return generalTC; }
                else
                { return ""; }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                { generalTC = value; }
            }
        }


        [DataMember(Name = "isRevUnitDiscountEnable")]
        public int IsRevUnitDiscountEnable { get; set; }

        [DataMember(Name = "totalCount")]
        public int TotalCount { get; set; }

        [DataMember(Name = "multipleAttachments")]
        public List<FileUpload> MultipleAttachments { get; set; }

        [DataMember(Name = "contractStartTime")]
        public string ContractStartTime { get; set; }

        [DataMember(Name = "contractEndTime")]
        public string ContractEndTime { get; set; }

        [DataMember(Name = "isContract")]
        public bool IsContract { get; set; }

        [DataMember(Name = "userName")]
        public string UserName { get; set; }

        string prCode = string.Empty;
        [DataMember(Name = "prCode")]
        public string PrCode
        {
            get
            {
                if (!string.IsNullOrEmpty(prCode))
                { return prCode; }
                else
                { return ""; }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                { prCode = value; }
            }
        }

        [DataMember(Name = "PR_ID")]
        public string PR_ID { get; set; }

        [DataMember(Name = "PR_ITEM_IDS")]
        public string PR_ITEM_IDS { get; set; }

        #region #CB-0-2018-12-05

        [DataMember(Name = "IS_CB_ENABLED")]
        public bool IS_CB_ENABLED { get; set; }

        [DataMember(Name = "CB_END_TIME")]
        public DateTime? CB_END_TIME { get; set; }

        [DataMember(Name = "CB_TIME_LEFT")]
        public long CB_TIME_LEFT { get; set; }

        [DataMember(Name = "CB_STOP_QUOTATIONS")]
        public bool CB_STOP_QUOTATIONS { get; set; }

        [DataMember(Name = "IS_CB_COMPLETED")]
        public bool IS_CB_COMPLETED { get; set; }

        #endregion #CB-0-2018-12-05


        [DataMember(Name = "DATETIME_NOW")]
        public DateTime? DATETIME_NOW { get; set; }

        [DataMember(Name = "LAST_BID_ID")]
        public int LAST_BID_ID { get; set; }

        [DataMember(Name = "isPOGenerated")]
        public bool IsPOGenerated { get; set; }

        [DataMember(Name = "isLockReq")]
        public int IsLockReq { get; set; }

        [DataMember(Name = "module")]
        public string Module { get; set; }

        [DataMember(Name = "REQ_TYPE")]
        public int REQ_TYPE { get; set; }

        [DataMember(Name = "quotationNotSubmitted")]
        public string QuotationNotSubmitted { get; set; }

        [DataMember(Name = "IS_CB_NO_REGRET")]
        public bool IS_CB_NO_REGRET { get; set; }

        [DataMember(Name = "lotCount")]
        public int LotCount { get; set; }

        [DataMember(Name = "lotAuctions")]
        public List<LotAuctions> LotAuctions { get; set; }

        [DataMember(Name = "isTechScoreReq")]
        public int IsTechScoreReq { get; set; }

        [DataMember(Name = "auditComments")]
        public string AuditComments { get; set; }

        [DataMember(Name = "auditVersion")]
        public int AuditVersion { get; set; }

        [DataMember(Name = "templateId")]
        public int TemplateId { get; set; }


        [DataMember(Name = "projectName")]
        public string ProjectName { get; set; }
        [DataMember(Name = "projectDetails")]
        public string ProjectDetails { get; set; }

        [DataMember(Name = "prNumbers")]
        public string PRNumbers { get; set; }

        [DataMember(Name = "prCreator")]
        public string PRCreator { get; set; }

        [DataMember(Name = "PLANT_CODES")]
        public string PLANT_CODES { get; set; }

        [DataMember(Name = "PLANTS")]
        public string PLANTS { get; set; }

        [DataMember(Name = "PURCHASE_GROUP_CODES")]
        public string PURCHASE_GROUP_CODES { get; set; }

        [DataMember(Name = "dateModified")]
        public DateTime? DateModified { get; set; }

        [DataMember(Name = "qcsDetails")]
        public List<QCSReportDetails> QCSDetails { get; set; }

        [DataMember(Name = "APPROVED_PKG_EXISTS")]
        public int APPROVED_PKG_EXISTS { get; set; }
    }

    public class LotAuctions
    {
        [DataMember(Name = "reqId")]
        public int ReqId { get; set; }

        [DataMember(Name = "reqTitle")]
        public string ReqTitle { get; set; }
    }

    [DataContract]
    public class CustomerEmails
    {
        [DataMember(Name = "wtcID")]
        public int WtcID { get; set; }

        [DataMember(Name = "userID")]
        public int UserID { get; set; }

        [DataMember(Name = "mail")]
        public string Mail { get; set; }

        [DataMember(Name = "status")]
        public int Status { get; set; }

        [DataMember(Name = "reqId")]
        public int ReqId { get; set; }
    }
}