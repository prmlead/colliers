﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace PRMServices.Models
{
    [DataContract]
    public class CategoryObj : Entity
    {
        [DataMember(Name="id")]
        public int ID { get; set; }

        [DataMember(Name = "category")]
        public string Category { get; set; }

        [DataMember(Name = "subcategory")]
        public string Subcategory { get; set; }

        [DataMember(Name = "ticked")]
        public bool Ticked { get; set; }

        [DataMember(Name = "compID")]
        public int CompID { get; set; }

        [DataMember(Name = "catObj")]
        public List<CategoryObj> CatObj { get; set; }
    }
}