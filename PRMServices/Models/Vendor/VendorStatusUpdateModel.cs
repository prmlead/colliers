﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace PRMServices.Models.Vendor
{
    [DataContract]
    public class VendorStatusUpdateModel : Entity
    {

        [DataMember]
        public int User_Id { get; set; }

        [DataMember]
        public int Status { get; set; }

        [DataMember]
        public string Reason { get; set; }

        [DataMember]
        public string Comment { get; set; }

        [DataMember]
        public string Category { get; set; }

        [DataMember]
        public int[] SubCategories { get; set; }

    }
}