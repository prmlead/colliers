﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace PRMServices.Models
{
    [DataContract]
    public class UploadQuotationTemp : Response
    {
        public string quotationItems { get; set; }

        public int reqID { get; set; }

        public string customerID { get; set; }

        public string userID { get; set; }

        public string warranty { get; set; }

        public string payment { get; set; }

        public string duration { get; set; }

        public string validity { get; set; }

        public string price { get; set; }

        public string tax { get; set; }

        public string freightcharges { get; set; }

        public string vendorBidPrice { get; set; }

        public string IsTabular { get; set; }

        public string revised { get; set; }

        public string quotationTaxes { get; set; }

        public string discountAmount { get; set; }

        public string otherProperties { get; set; }

        public string deliveryTermsDays { get; set; }

        public string deliveryTermsPercent { get; set; }

        public string paymentTermsDays { get; set; }

        public string paymentTermsPercent { get; set; }

        public string paymentTermsType { get; set; }

        public string folderPath { get; set; }

        public string uploadType { get; set; }

        public string desFileName { get; set; }

        public Requirement requirement { get; set; }

        public int CallerID { get; set; }
    }
}