﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace PRMServices.Models
{
    [DataContract]
    public class ExpenditureAnalysis : Entity
    {
        [DataMember(Name = "equipmentCost")]
        public int EquipmentCost { get; set; }

        [DataMember(Name = "serviceCost")]
        public int ServiceCost { get; set; }

        [DataMember(Name = "sparesCost")]
        public string SparesCost { get; set; }

    }
}