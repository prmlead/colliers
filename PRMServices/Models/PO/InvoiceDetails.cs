﻿
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using PRM.Core.Common;
using PRMServices.Models;

namespace PRMServices
{
    [DataContract]
    public class InvoiceDetails
    {
        [DataMember] [DataNames("ROW_ID")] public int ROW_ID { get; set; }
        [DataMember] [DataNames("INVOICE_ID")] public int INVOICE_ID { get; set; }
        [DataMember] [DataNames("BILL_ID")] public int BILL_ID { get; set; }
        [DataMember] [DataNames("PROJECT_ID")] public int PROJECT_ID { get; set; }
        [DataMember] [DataNames("VENDOR_ID")] public int VENDOR_ID { get; set; }
        [DataMember] [DataNames("VENDOR_CODE")] public string VENDOR_CODE { get; set; }
        [DataMember] [DataNames("INVOICE_NUMBER")] public string INVOICE_NUMBER { get; set; }
        [DataMember] [DataNames("INVOICE_DESCRIPTION")] public string INVOICE_DESCRIPTION { get; set; }
        [DataMember] [DataNames("INVOICE_DATE")] public DateTime? INVOICE_DATE { get; set; }
        [DataMember] [DataNames("INVOICE_DATE1")] public DateTime? INVOICE_DATE1 { get; set; }
        [DataMember] [DataNames("UNIT_RATE")] public decimal UNIT_RATE { get; set; }
        [DataMember] [DataNames("CGST")] public decimal CGST { get; set; }
        [DataMember] [DataNames("SGST")] public decimal SGST { get; set; }
        [DataMember] [DataNames("IGST")] public decimal IGST { get; set; }
        [DataMember] [DataNames("OTHERS")] public decimal OTHERS { get; set; }
        [DataMember] [DataNames("BILL_CERTIFIED_AMOUNT")] public decimal BILL_CERTIFIED_AMOUNT { get; set; }
        [DataMember] [DataNames("INVOICE_AMOUNT")] public decimal INVOICE_AMOUNT { get; set; }
        [DataMember] [DataNames("TOTAL_AMOUNT")] public decimal TOTAL_AMOUNT { get; set; }
        [DataMember] [DataNames("TAX_AMOUNT")] public decimal TAX_AMOUNT { get; set; }
        //[DataMember] [DataNames("PACKAGE_ID")] public int PACKAGE_ID { get; set; }
        //[DataMember] [DataNames("SUB_PACKAGE_ID")] public int SUB_PACKAGE_ID { get; set; }
        [DataMember] [DataNames("PO_NUMBER")] public string PO_NUMBER { get; set; }
        [DataMember] [DataNames("CREATED_BY")] public int CREATED_BY { get; set; }
        [DataMember] [DataNames("MODIFIED_BY")] public int MODIFIED_BY { get; set; }
        [DataMember] [DataNames("V_COMP_ID")] public int V_COMP_ID { get; set; }
        [DataMember] [DataNames("C_COMP_ID")] public int C_COMP_ID { get; set; }
        [DataMember] [DataNames("DATE_CREATED")] public DateTime? DATE_CREATED { get; set; }
        [DataMember] [DataNames("DATE_MODIFIED")] public DateTime? DATE_MODIFIED { get; set; }
        [DataMember] [DataNames("MULTIPLE_ATTACHMENTS")] public string MULTIPLE_ATTACHMENTS { get; set; }
        [DataMember] [DataNames("INVOICE_WF_ID")] public int INVOICE_WF_ID { get; set; }


    }
}
 