﻿
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using PRM.Core.Common;
using PRMServices.Models;

namespace PRMServices
{
    [DataContract]
    public class BillRequests : ResponseAudit
    {
        [DataMember] [DataNames("ROW_ID")] public int ROW_ID { get; set; }
        [DataMember] [DataNames("BILL_ID")] public int BILL_ID { get; set; }
        [DataMember] [DataNames("V_COMP_ID")] public int V_COMP_ID { get; set; }
        [DataMember] [DataNames("C_COMP_ID")] public int C_COMP_ID { get; set; }
        [DataMember] [DataNames("PO_ID")] public int PO_ID { get; set; }
        [DataMember] [DataNames("PROJECT_ID")] public int PROJECT_ID { get; set; }
        [DataMember] [DataNames("PACKAGE_ID")] public int PACKAGE_ID { get; set; }
        [DataMember] [DataNames("SUB_PACKAGE_ID")] public int SUB_PACKAGE_ID { get; set; }
        [DataMember] [DataNames("PO_NUMBER")] public string PO_NUMBER { get; set; }
        [DataMember] [DataNames("VENDOR_NAME")] public string VENDOR_NAME { get; set; }
        [DataMember] [DataNames("PO_VALUE")] public decimal PO_VALUE { get; set; }
        [DataMember] [DataNames("BILL_ATTACHMENTS")] public string BILL_ATTACHMENTS { get; set; }
        [DataMember] [DataNames("BILL_REQUEST_AMOUNT")] public decimal BILL_REQUEST_AMOUNT { get; set; }
        [DataMember] [DataNames("BILL_CERTIFIED_AMOUNT")] public decimal BILL_CERTIFIED_AMOUNT { get; set; }
        [DataMember] [DataNames("BILL_REQUEST_STATUS")] public string BILL_REQUEST_STATUS { get; set; }
        [DataMember] [DataNames("BILL_CERTIFICATION_STATUS")] public string BILL_CERTIFICATION_STATUS { get; set; }
        [DataMember] [DataNames("CREATED_BY")] public int CREATED_BY { get; set; }
        [DataMember] [DataNames("MODIFIED_BY")] public int MODIFIED_BY { get; set; }
        [DataMember] [DataNames("BILL_CERTIFICATE_WF_ID")] public int BILL_CERTIFICATE_WF_ID { get; set; }
        [DataMember] [DataNames("DATE_CREATED")] public DateTime? DATE_CREATED { get; set; }
        [DataMember] [DataNames("DATE_MODIFIED")] public DateTime? DATE_MODIFIED { get; set; }
        [DataMember] [DataNames("MULTIPLE_ATTACHMENTS")] public List<FileUpload> MULTIPLE_ATTACHMENTS { get; set; }
        [DataMember] [DataNames("MULTIPLE_ATTACHMENTS_BILL_CERTIFICATION")] public List<FileUpload> MULTIPLE_ATTACHMENTS_BILL_CERTIFICATION { get; set; }
        [DataMember] [DataNames("CHECK_LIST_JSON")] public string CHECK_LIST_JSON { get; set; }
        [DataMember] [DataNames("BILL_TYPE")] public string BILL_TYPE { get; set; }
        [DataMember] [DataNames("IS_VALID")] public int IS_VALID { get; set; }
    }

    public class CheckList
    {
        [DataMember] [DataNames("isChecked")] public bool isChecked { get; set; }
        [DataMember] [DataNames("value")] public string value { get; set; }
        [DataMember] [DataNames("MULTIPLE_ATTACHMENTS")] public List<FileUpload> MULTIPLE_ATTACHMENTS { get; set; }
        //[DataMember] [DataNames("ATTACHMENTS")] public string ATTACHMENTS { get; set; }
        //[DataMember] [DataNames("MULTIPLE_ATTACHMENTS_ARR")] public List<FileUpload> MULTIPLE_ATTACHMENTS_ARR { get; set; }
    }
}
 