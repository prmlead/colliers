﻿
using System;
using System.Runtime.Serialization;
using PRM.Core.Common;
using PRMServices.Models;

namespace PRMServices
{
    [DataContract]
    public class POSchedule : ResponseAudit
    {
        [DataMember(Name = "PURCHASE_ORDER_ID")]
        [DataNames("PURCHASE_ORDER_ID")]
        public string PURCHASE_ORDER_ID { get; set; }

        [DataMember(Name = "ITEM_ID")]
        [DataNames("ITEM_ID")]
        public int ITEM_ID { get; set; }

        [DataMember(Name = "QUANITTY")]
        [DataNames("QUANITTY")]
        public decimal QUANITTY { get; set; }

        [DataMember(Name = "DELIVERY_DATE")]
        [DataNames("DELIVERY_DATE")]
        public DateTime? DELIVERY_DATE
        {
            get; set;
        }

        [DataMember(Name = "COMMENTS")]
        [DataNames("COMMENTS")]
        public string COMMENTS
        {
            get; set;
        }


        [DataMember(Name = "TOTAL_COUNT")]
        [DataNames("TOTAL_COUNT")]
        public int TOTAL_COUNT { get; set; }

        [DataMember(Name = "PO_ID")]
        [DataNames("PO_ID")]
        public int PO_ID { get; set; }

        [DataMember(Name = "VENDOR_ID")]
        [DataNames("VENDOR_ID")]
        public int VENDOR_ID { get; set; }

    }
}