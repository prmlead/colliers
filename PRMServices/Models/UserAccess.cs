﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace PRMServices.Models
{
    [DataContract]
    public class UserAccess : Entity
    {       
        [DataMember(Name = "userID")]
        public int UserID { get; set; }

        [DataMember(Name = "acsID")]
        public int AcsID { get; set; }        

        [DataMember(Name = "dateCreated")]
        public DateTime? DateCreated { get; set; }

        [DataMember(Name = "dateModified")]
        public DateTime? DateModified { get; set; }

        [DataMember(Name = "createddBy")]
        public int CreateddBy { get; set; }

        [DataMember(Name = "modifiedBy")]
        public int ModifiedBy { get; set; }

        [DataMember(Name = "isEntitled")]
        public bool IsEntitled { get; set; }

        [DataMember(Name = "accessType")]
        public string AccessType { get; set; }
    }
}