﻿using PRMServices.Models.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace PRMServices.Models.Settings.Registrations
{
    [DataContract]
    public class RegistrationFormModel : BaseEntityModel
    {
        /// <summary>
        /// Field Name
        /// </summary>
        [DataMember]

        public string   Name { get; set; }


        /// <summary>
        /// Display name
        /// </summary>
        [DataMember]
        public string DisplayName { get; set; }

        /// <summary>
        /// Field Required
        /// </summary>
        [DataMember]
        public bool Required { get; set; }

        /// <summary>
        /// Field type
        /// </summary>
        [DataMember]
        public string Type { get; set; }

        /// <summary>
        /// Field score
        /// </summary>
        [DataMember]
        public int Score { get; set; }

        /// <summary>
        /// Is system field
        /// </summary>
        [DataMember]
        public bool SystemField { get; set; }


        /// <summary>
        /// Display order
        /// </summary>
        [DataMember]
        public int DisplayOrder { get; set; }

        /// <summary>
        /// Allow attachement
        /// </summary>
        [DataMember]
        public bool AllowAttachment { get; set; }



    }
}