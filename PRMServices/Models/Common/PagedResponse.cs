﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace PRMServices.Models.Common
{
    [DataContract]
    public class PagedResponse
    {
        [DataMember]
        public int PageIndex { get; set; }

        [DataMember]
        public int PageSize { get; set; }

        [DataMember]
        public long TotalCount { get; set; }

        [DataMember]
        public int TotalPages { get; set; }

        [DataMember]
        public bool HasPreviousPage { get; set; }

        [DataMember]
        public bool HasNextPage { get; set; }

        [DataMember]
        public object Data { get; set; }
    }
}