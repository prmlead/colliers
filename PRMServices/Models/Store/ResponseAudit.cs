﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

using PRMServices.Models;

namespace PRMServices.Models
{
    [DataContract]
    public class ResponseAudit
    {
        int objectId = 0;
        [DataMember(Name = "objectID")]
        public int ObjectID
        {
            get
            {
                return this.objectId;
            }
            set
            {
                this.objectId = value;
            }
        }

        string msg = string.Empty;
        [DataMember(Name = "message")]
        public string Message
        {
            get
            {
                return this.msg;
            }
            set
            {
                this.msg = value;
            }
        }

        string eMessage = string.Empty;
        [DataMember(Name = "errorMessage")]
        public string ErrorMessage
        {
            get
            {
                return this.eMessage;
            }
            set
            {
                this.eMessage = value;
            }
        }

        string sessionId = string.Empty;
        [DataMember(Name = "sessionID")]
        public string SessionID
        {
            get
            {
                return this.sessionId;
            }
            set
            {
                this.sessionId = value;
            }
        }

        DateTime dateCreated = DateTime.MaxValue;
        [DataMember(Name = "dateCreated")]
        public DateTime DateCreated
        {
            get
            {
                return this.dateCreated;
            }
            set
            {
                this.dateCreated = value;
            }
        }

        DateTime dateModified = DateTime.MaxValue;
        [DataMember(Name = "dateModified")]
        public DateTime DateModified
        {
            get
            {
                return this.dateModified;
            }
            set
            {
                this.dateModified = value;
            }
        }


        [DataMember(Name = "createdBy")]
        public int CreatedBy
        {
            get;
            set;
        }

        [DataMember(Name = "modifiedBy")]
        public int ModifiedBY
        {
            get;
            set;
        }


        [DataMember(Name = "deptID")]
        public int DeptID
        {
            get;
            set;
        }

        string location = string.Empty;
        [DataMember(Name = "location")]
        public string Location
        {
            get
            {
                return this.location;
            }
            set
            {
                this.location = value;
            }
        }

    }
}