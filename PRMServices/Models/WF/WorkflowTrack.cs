﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace PRMServices.Models
{
    [DataContract]
    public class WorkflowTrack : ResponseAudit
    {
        [DataMember(Name = "trackID")]
        public int TrackID { get; set; }

        [DataMember(Name = "approverID")]
        public int ApproverID { get; set; }

        [DataMember(Name = "approver")]
        public CompanyDesignations Approver { get; set; }

        [DataMember(Name = "department")]
        public CompanyDepartments Department { get; set; }

        [DataMember(Name = "workflow")]
        public Workflow WorkflowDetails { get; set; }

        string status = string.Empty;
        [DataMember(Name = "status")]
        public string Status
        {
            get
            {
                return string.IsNullOrEmpty(status) ? string.Empty : status;
            }
            set
            {
                status = value;
            }
        }

        [DataMember(Name = "order")]
        public int Order { get; set; }

        [DataMember(Name = "moduleID")]
        public int ModuleID { get; set; }

        string comments = string.Empty;
        [DataMember(Name = "comments")]
        public string Comments
        {
            get
            {
                return string.IsNullOrEmpty(comments) ? string.Empty : comments;
            }
            set
            {
                comments = value;
            }
        }

        string indNo = string.Empty;
        [DataMember(Name = "indNo")]
        public string IndNo
        {
            get
            {
                return string.IsNullOrEmpty(indNo) ? string.Empty : indNo;
            }
            set
            {
                indNo = value;
            }
        }

        string moduleCode = string.Empty;
        [DataMember(Name = "moduleCode")]
        public string ModuleCode
        {
            get
            {
                return string.IsNullOrEmpty(moduleCode) ? string.Empty : moduleCode;
            }
            set
            {
                moduleCode = value;
            }
        }

        string moduleName = string.Empty;
        [DataMember(Name = "moduleName")]
        public string ModuleName
        {
            get
            {
                return string.IsNullOrEmpty(moduleName) ? string.Empty : moduleName;
            }
            set
            {
                moduleName = value;
            }
        }

        [DataMember(Name = "approverDetails")]
        public User ApproverDetails { get; set; }


        string subModuleName = string.Empty;
        [DataMember(Name = "subModuleName")]
        public string SubModuleName
        {
            get
            {
                return string.IsNullOrEmpty(subModuleName) ? string.Empty : subModuleName;
            }
            set
            {
                subModuleName = value;
            }
        }


        [DataMember(Name = "subModuleID")]
        public int SubModuleID { get; set; }

        string attachments = string.Empty;
        [DataMember(Name = "attachments")]
        public string Attachments
        {
            get
            {
                return string.IsNullOrEmpty(attachments) ? string.Empty : attachments;
            }
            set
            {
                attachments = value;
            }
        }


        [DataMember(Name = "multipleAttachments")]
        public List<FileUpload> MultipleAttachments { get; set; }

        [DataMember(Name = "emailTemplate")]
        public List<EmailTemplate> EmailTemplate { get; set; }

        [DataMember(Name = "isLastApprover")]
        public bool IsLastApprover { get; set; }

        [DataMember(Name = "revisionPODetails")]
        public List<POItem> PODetails { get; set; }
     
    }


    public class EmailTemplate 
    {

        string subject = string.Empty;

        [DataMember(Name = "subject")]
        public string Subject
            {
                get
                {
                    if (!string.IsNullOrEmpty(subject))
                    { return subject; }
                    else
                    { return ""; }
                }
                set
                {
                    if (!string.IsNullOrEmpty(value))
                    { subject = value; }
                }
            }



        string content = string.Empty;

        [DataMember(Name = "content")]
        public string Content
        {
            get
            {
                if (!string.IsNullOrEmpty(content))
                { return content; }
                else
                { return ""; }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                { content = value; }
            }
        }


        string type = string.Empty;
        [DataMember(Name = "type")]
        public string Type
        {
            get
            {
                if (!string.IsNullOrEmpty(type))
                { return type; }
                else
                { return ""; }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                { type = value; }
            }
        }


        string action = string.Empty;
        [DataMember(Name = "action")]
        public string Action
        {
            get
            {
                if (!string.IsNullOrEmpty(action))
                { return action; }
                else
                { return ""; }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                { action = value; }
            }
        }



    }

}