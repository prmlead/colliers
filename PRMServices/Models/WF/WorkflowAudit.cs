﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace PRMServices.Models
{
    [DataContract]
    public class WorkflowAudit : ResponseAudit
    {
        [DataMember(Name = "auditID")]
        public int AuditID { get; set; }

        [DataMember(Name = "workflowTrack")]
        public WorkflowTrack TrackDetails { get; set; }

        string status = string.Empty;
        [DataMember(Name = "status")]
        public string Status
        {
            get
            {
                return string.IsNullOrEmpty(status) ? string.Empty : status;
            }
            set
            {
                status = value;
            }
        }

        [DataMember(Name = "level")]
        public int Level { get; set; }

        string comments = string.Empty;
        [DataMember(Name = "comments")]
        public string Comments
        {
            get
            {
                return string.IsNullOrEmpty(comments) ? string.Empty : comments;
            }
            set
            {
                comments = value;
            }
        }
    }
}