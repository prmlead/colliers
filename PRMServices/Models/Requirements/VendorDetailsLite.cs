﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using PRM.Core.Common;

namespace PRMServices.Models
{
    [DataContract]
    public class VendorDetailsLite : Entity
    {
        [DataNames("U_ID")]
        [DataMember(Name = "vendorID")]
        public int VendorID { get; set; }

        [DataNames("VEND_INIT_PRICE")]
        [DataMember(Name = "initialPrice")]
        public decimal InitialPrice { get; set; }

        [DataNames("VEND_RUN_PRICE")]
        [DataMember(Name = "runningPrice")]
        public decimal RunningPrice { get; set; }

        [DataNames("VEND_TOTAL_PRICE")]
        [DataMember(Name = "totalInitialPrice")]
        public decimal TotalInitialPrice { get; set; }

        [DataNames("VEND_TOTAL_PRICE_RUNNING")]        
        [DataMember(Name = "totalRunningPrice")]
        public decimal TotalRunningPrice { get; set; }

        [DataMember(Name = "rank")]
        public int Rank { get; set; }

        [DataNames("REV_PRICE")]
        [DataMember(Name = "revPrice")]
        public decimal RevPrice { get; set; }

        [DataNames("REV_VEND_TOTAL_PRICE")]
        [DataMember(Name = "revVendorTotalPrice")]
        public decimal RevVendorTotalPrice { get; set; }

        [DataNames("REV_VEND_TOTAL_PRICE_NO_TAX")]
        [DataMember(Name = "revVendorTotalPriceNoTax")]
        public decimal RevVendorTotalPriceNoTax { get; set; }

        [DataNames("LAST_ACTIVE_TIME")]
        [DataMember(Name = "lastActiveTime")]
        public string LastActiveTime { get; set; }

        [DataNames("LANDING_PRICE")]
        [DataMember(Name = "landingPrice")]
        public decimal LandingPrice { get; set; }

        [DataNames("REV_LANDING_PRICE")]
        [DataMember(Name = "revLandingPrice")]
        public decimal RevLandingPrice { get; set; }

        [DataMember(Name = "listRequirementItems")]
        public List<RequirementItemsLite> RequirementItems { get; set; }

        [DataNames("REV_PRICE_CB")]
        [DataMember(Name = "revPriceCB")]
        public decimal RevPriceCB { get; set; }

        [DataNames("REV_VEND_TOTAL_PRICE_CB")]
        [DataMember(Name = "revVendorTotalPriceCB")]
        public decimal RevVendorTotalPriceCB { get; set; }

        [DataNames("VEND_TOTAL_PRICE_RUNNING_CB")]
        [DataMember(Name = "totalRunningPriceCB")]
        public decimal TotalRunningPriceCB { get; set; }

        [DataNames("FREEZE_CB")]
        [DataMember(Name = "FREEZE_CB")]
        public bool FREEZE_CB { get; set; }

        [DataNames("VEND_FREEZE_CB")]
        [DataMember(Name = "VEND_FREEZE_CB")]
        public bool VEND_FREEZE_CB { get; set; }

        [DataNames("AUCTION_MODIFIED_DATE")]
        [DataMember(Name = "auctionModifiedDate")]
        public DateTime? AuctionModifiedDate { get; set; }
    }
}