﻿using PRMServices.Models.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PRMServices.Models.Requirements
{
    public class RequirementTemplateModel : BaseEntityModel
    {
        public string Name { get; set; }

        public string Description { get; set; }

        public bool Active { get; set; }

        public bool LimitedToDepartment { get; set; }

        public int[] AllowedDepartments { get; set; }


    }
}