﻿using PRM.Core.Common;
using System;
using System.Runtime.Serialization;

namespace PRMServices.Models
{
    [DataContract]
    public class Branch : Entity
    {
        [DataMember] [DataNames("BRANCH_ID")] public int BRANCH_ID { get; set; }
        [DataMember] [DataNames("COMP_ID")] public int COMP_ID { get; set; }
        [DataMember] [DataNames("BRANCH_CODE")] public string BRANCH_CODE { get; set; }
        [DataMember] [DataNames("BRANCH_NAME")] public string BRANCH_NAME { get; set; }
        [DataMember] [DataNames("BRANCH_ADDRESS")] public string BRANCH_ADDRESS { get; set; }
        [DataMember] [DataNames("CREATED_BY")] public int CREATED_BY { get; set; }
        [DataMember] [DataNames("CREATED")] public DateTime CREATED { get; set; }
        [DataMember] [DataNames("MODIFIED_BY")] public int MODIFIED_BY { get; set; }
        [DataMember] [DataNames("MODIFIED")] public DateTime MODIFIED { get; set; }

        [DataMember] public User[] users { get; set; }

    }
}
