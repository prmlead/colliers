﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace PRMServices.Models

{
    [DataContract]
    public class OpsRequirement : Entity 
    {
        [DataMember(Name = "requirementID")]
        public int RequirementID { get; set; }

        [DataMember(Name = "customerID")]
        public int CustomerID { get; set; }

        string title = string.Empty;
        [DataMember(Name = "title")]
        public string Title
        {
            get
            {
                if (!string.IsNullOrEmpty(title))
                { return title; }
                else
                { return ""; }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                { title = value; }
            }
        }

        [DataMember(Name = "vendorsInvited")]
        public int VendorsInvited { get; set; }

        [DataMember(Name = "SURROGATED_VENDOR_COUNT")]
        public int SURROGATED_VENDOR_COUNT { get; set; }
        [DataMember(Name = "totalCount")]
        public int TotalCount { get; set; }

        [DataMember(Name = "is_QUOTATION_REJECTED")]
        public string IS_QUOTATION_REJECTED { get; set; }

        [DataMember(Name = "vendorsOnline")]
        public int VendorsOnline { get; set; }

        [DataMember(Name = "numberOfBids")]
        public int NumberOfBids { get; set; }

        //[DataMember(Name = "notBidded")]
        //public int NotBidded { get; set; }

        [DataMember(Name = "quotation_FREEZ_TIME")]
        public DateTime? QUOTATION_FREEZ_TIME { get; set; }


        //[DataMember(Name = "quotation_FREEZ_TIME")]
        //public DateTime QUOTATION_FREEZ_TIME
        //{
        //    get
        //    { return DateTime.Now; }
        //    set { }
        //}

        //[DataMember(Name = "exp_START_TIME")]
        //public DateTime EXP_START_TIME
        //{ get
        //    { return DateTime.Now; }
        //    set { }
        //}


        [DataMember(Name = "exp_START_TIME")]
        public DateTime? EXP_START_TIME { get; set; }

        //[DataMember(Name = "exp_START_TIME")]
        //public DateTime EXP_START_TIME
        //{
        //    get ;
        //    set
        //    {
        //        DateTime = DateTime.Now;
        //    }
        //}

        //DateTime start_TIME = DateTime.MaxValue;
        //[DataMember(Name = "start_TIME")]
        //public DateTime? START_TIME
        //{
        //    get
        //    {
        //        return start_TIME;
        //    }
        //    set
        //    {
        //        if (value != null && value.HasValue)
        //        {
        //            start_TIME = value.Value;
        //        }
        //    }
        //}

        //[DataMember(Name = "start_TIME")]
        //public DateTime START_TIME
        //{
        //    get;
        //    set
        //    {
        //        DateTime = DateTime.Now;
        //    }
        //}

        [DataMember(Name = "start_TIME")]
        public DateTime? START_TIME { get; set; }

        [DataMember(Name = "end_TIME")]
        public DateTime? END_TIME { get; set; }


        [DataMember(Name = "closed")]
        public string CLOSED { get; set; }

        [DataMember(Name = "recuirementValue")]
        public Double RecuirementValue { get; set; }

        [DataMember(Name = "savings")]
        public Double SAVINGS { get; set; }

        [DataMember(Name = "savings_BEFORE_AUCTION")]
        public Double SAVINGS_BEFORE_AUCTION { get; set; }

        [DataMember(Name = "miniReducAmount")]
        public int MiniReducAmount { get; set; }

        [DataMember(Name = "biddingType")]
        public string BiddingType { get; set; }

        [DataMember(Name = "reqPostedBy")]
        public string ReqPostedBy { get; set; }

        [DataMember(Name = "isDiscountQuotation")]
        public int IsDiscountQuotation { get; set; }

        [DataMember(Name = "customerName")]
        public string CustomerName { get; set; }

        [DataMember(Name = "postedByName")]
        public string PostedByrName { get; set; }

        [DataMember(Name = "customerPhone")]
        public string CustomerPhone { get; set; }

        [DataMember(Name = "customerMail")]
        public string CustomerMail { get; set; }

        string contactDetails = string.Empty;
        [DataMember(Name = "contactDetails")]
        public string ContactDetails
        {
            get
            {
                if (!string.IsNullOrEmpty(contactDetails))
                { return contactDetails; }
                else
                { return ""; }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                { contactDetails = value; }
            }
        }

        [DataMember(Name = "isCapitalReq")]
        public bool IsCapitalReq { get; set; }

        [DataMember(Name = "clientID")]
        public int ClientID { get; set; }

        [DataMember(Name = "reqItems")]
        public int ReqItems { get; set; }

        [DataMember(Name = "attachmentName")]
        public string AttachmentName { get; set; }

        [DataMember(Name = "timeLeft")]
        public long TimeLeft { get; set; }

        [DataMember(Name = "isSplitEnabled")]
        public bool IsSplitEnabled { get; set; }

        [DataMember(Name = "IS_CB_ENABLED")]
        public bool IS_CB_ENABLED { get; set; }

        [DataMember(Name = "CB_END_TIME")]
        public DateTime? CB_END_TIME { get; set; }

        [DataMember(Name = "CB_TIME_LEFT")]
        public long CB_TIME_LEFT { get; set; }

        [DataMember(Name = "CB_STOP_QUOTATIONS")]
        public bool CB_STOP_QUOTATIONS { get; set; }

        [DataMember(Name = "IS_CB_COMPLETED")]
        public bool IS_CB_COMPLETED { get; set; }

        [DataMember(Name = "DATETIME_NOW")]
        public DateTime? DATETIME_NOW { get; set; }

        [DataMember(Name = "LOT_ID")]
        public int LOT_ID { get; set; }

        [DataMember(Name = "LOT_ORDER")]
        public int LOT_ORDER { get; set; }

        [DataMember(Name = "req_POSTED_ON")]
        public DateTime? REQ_POSTED_ON { get; set; }

        [DataMember(Name = "isSlotCreated")]
        public int IsSlotCreated { get; set; }

        [DataMember(Name = "vendorsParticipated")]
        public Double VendorsParticipated { get; set; }

        [DataMember(Name = "minIbitialQuotationPrice")]
        public Double MinIbitialQuotationPrice { get; set; }

        [DataMember(Name = "userName")]
        public string UserName { get; set; }

        [DataMember(Name = "companyName")]
        public string CompanyName { get; set; }

        [DataMember(Name = "vendor")]
        public User Vendor { get; set; }


        [DataMember(Name = "quotationURL")]
        public string QuotationURL { get; set; }

        [DataMember(Name = "PR_NUMBERS")]
        public string PR_NUMBERS { get; set; }

        [DataMember(Name = "PLANT_CODES")]
        public string PLANT_CODES { get; set; }

        [DataMember(Name = "PLANT_NAME")]
        public string PLANT_NAME { get; set; }

        [DataMember(Name = "REQ_NUMBER")]
        public string REQ_NUMBER { get; set; }


        [DataMember(Name = "startTime")]
        public DateTime? StartTime { get; set; }

        [DataMember(Name = "endTime")]
        public DateTime? EndTime { get; set; }
    }
}

