﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;


namespace PRMServices.Models
{
    [DataContract]
    public class OpsReports : Entity
    {
        int objectId = 0;
        [DataMember(Name = "objectID")]
        public int ObjectID
        {
            get
            {
                return this.objectId;
            }
            set
            {
                this.objectId = value;
            }
        }


        [DataMember(Name = "reqTransactions")]
        public List<OpsRequirement> ReqTransactions
        {
            get;
            set;
        }

        [DataMember(Name = "subUsersRegistered")]
        public List<OpsUserDetails> SubUsersRegistered
        {
            get;
            set;
        }

        [DataMember(Name = "vendorsRegistered")]
        public List<OpsUserDetails> VendorsRegistered
        {
            get;
            set;
        }


        [DataMember(Name = "regularTransactions")]
        public List<Requirement> regularTransactions
        {
            get;
            set;
        }


        [DataMember(Name = "subUsersRegisteredAll")]
        public List<User> SubUsersRegisteredAll
        {
            get;
            set;
        }

        [DataMember(Name = "vendorsRegisteredAll")]
        public List<User> VendorsRegisteredAll
        {
            get;
            set;
        }

        [DataMember(Name = "PRRFQData")]
        public List<PRRFQData> PRRFQData
        {
            get;
            set;
        }

        [DataMember(Name = "QCS_CURRENT_APPROVERS")]
        public List<QCSCURRENTAPPROVERS> QCS_CURRENT_APPROVERS
        {
            get;
            set;
        }

        [DataMember(Name = "QCS_APPROVAL_HISTORY")]
        public List<QCSAPPROVALHISTORY> QCS_APPROVAL_HISTORY
        {
            get;
            set;
        }

        [DataMember(Name = "VENDOR_RESPONSE_TIME")]
        public List<VENDORRESPONSETIME> VENDOR_RESPONSE_TIME
        {
            get;
            set;
        }

        [DataMember(Name = "PR_PO_DATA")]
        public List<PRPOData> PR_PO_DATA
        {
            get;
            set;
        }


        [DataMember(Name = "RFQDataWithPR")]
        public List<Requirement> RFQDataWithPR
        {
            get;
            set;
        }
    }

    public class PRRFQData : Entity
    {


        [DataMember(Name = "PR_NUMBER")]
        public string PR_NUMBER
        {
            get;
            set;
        }

        [DataMember(Name = "REQ_COUNT")]
        public int REQ_COUNT { get; set; }

        [DataMember(Name = "REQ_IDS")]
        public string REQ_IDS { get; set; }

        [DataMember(Name = "PR_ITEM_IDS")]
        public string PR_ITEM_IDS { get; set; }

        [DataMember(Name = "RELEASE_DATE")]
        public DateTime? RELEASE_DATE { get; set; }

        [DataMember(Name = "CREATED_DATE")]
        public DateTime? CREATED_DATE { get; set; }

        [DataMember(Name = "MODIFIED_DATE")]
        public DateTime? MODIFIED_DATE { get; set; }

        [DataMember(Name = "CREATED_BY")]
        public string CREATED_BY { get; set; }

        [DataMember(Name = "totalCount")]
        public int TotalCount { get; set; }

    }

    public class QCSCURRENTAPPROVERS
    {

        [DataMember(Name = "REQ_NUMBER")]
        public string REQ_NUMBER { get; set; }

        [DataMember(Name = "PR_NUMBER")]
        public string PR_NUMBER { get; set; }

        [DataMember(Name = "PR_LINE_ITEM")]
        public string PR_LINE_ITEM { get; set; }

        [DataMember(Name = "QCS_CODE")]
        public string QCS_CODE { get; set; }

        [DataMember(Name = "WF_TITLE")]
        public string WF_TITLE { get; set; }

        [DataMember(Name = "REQ_ID")]
        public int REQ_ID { get; set; }

        [DataMember(Name = "QCS_ID")]
        public int QCS_ID { get; set; }

        [DataMember(Name = "IS_PRIMARY_ID")]
        public int IS_PRIMARY_ID { get; set; }

        [DataMember(Name = "REQ_TITLE")]
        public string REQ_TITLE { get; set; }

        [DataMember(Name = "QCS_TYPE")]
        public string QCS_TYPE { get; set; }

        [DataMember(Name = "APPROVAL_STATUS")]
        public string APPROVAL_STATUS { get; set; }

        [DataMember(Name = "MODIFIED_BY_NAME")]
        public string MODIFIED_BY_NAME { get; set; }

        [DataMember(Name = "LATEST_ACTION_ON")]
        public DateTime? LATEST_ACTION_ON { get; set; }

        [DataMember(Name = "CREATED_DATE")]
        public DateTime? CREATED_DATE { get; set; }

        [DataMember(Name = "CREATED_BY_NAME")]
        public string CREATED_BY_NAME { get; set; }

        [DataMember(Name = "totalCount")]
        public int TotalCount { get; set; }

    }

    public class QCSAPPROVALHISTORY
    {

        [DataMember(Name = "WF_STATUS")]
        public string WF_STATUS { get; set; }

        [DataMember(Name = "WF_COMMENTS")]
        public string WF_COMMENTS { get; set; }

        [DataMember(Name = "MODIFIED_BY")]
        public string MODIFIED_BY { get; set; }

        [DataMember(Name = "CREATED")]
        public DateTime? CREATED { get; set; }

    }

    public class VENDORRESPONSETIME
    {
        [DataMember(Name = "VENDOR_ID")]
        public int VENDOR_ID { get; set; }

        [DataMember(Name = "VENDOR_NAME")]
        public string VENDOR_NAME { get; set; }

        [DataMember(Name = "VENDOR_INVITED")]
        public int VENDOR_INVITED { get; set; }

        [DataMember(Name = "VENDOR_PARTICIPATED")]
        public int VENDOR_PARTICIPATED { get; set; }

        [DataMember(Name = "VENDOR_NOT_PARTICIPATED")]
        public int VENDOR_NOT_PARTICIPATED { get; set; }

        [DataMember(Name = "VENDOR_EMAIL")]
        public string VENDOR_EMAIL { get; set; }

        [DataMember(Name = "COMP_NAME")]
        public string COMP_NAME { get; set; }

        [DataMember(Name = "RESPONSE_TIME_ALL")]
        public double RESPONSE_TIME_ALL { get; set; }

        [DataMember(Name = "totalCount")]
        public int TotalCount { get; set; }

    }

    public class PRPOData
    {

        [DataMember(Name = "PO_CREATED_DATE")]
        public DateTime? PO_CREATED_DATE { get; set; }

        [DataMember(Name = "REQ_POSTED_ON")]
        public DateTime? REQ_POSTED_ON { get; set; }

        [DataMember(Name = "PR_NUMBER")]
        public string PR_NUMBER { get; set; }

        [DataMember(Name = "PO_NUMBER")]
        public string PO_NUMBER { get; set; }

        [DataMember(Name = "REQ_ID")]
        public int REQ_ID { get; set; }

        [DataMember(Name = "REQ_NUMBER")]
        public string REQ_NUMBER { get; set; }

        [DataMember(Name = "VENDORS_INVITED")]
        public int VENDORS_INVITED { get; set; }

        [DataMember(Name = "VENDORS_PARTICIPATED")]
        public int VENDORS_PARTICIPATED { get; set; }

        [DataMember(Name = "SURROGATED_VENDOR")]
        public int SURROGATED_VENDOR { get; set; }

        [DataMember(Name = "ALL_REQ_VENDORS")]
        public string ALL_REQ_VENDORS { get; set; }

        [DataMember(Name = "REQ_TITLE")]
        public string REQ_TITLE { get; set; }

        [DataMember(Name = "PR_RELEASE_DATE")]
        public DateTime? PR_RELEASE_DATE { get; set; }

        [DataMember(Name = "PR_PO_VENDOR_LIST")]
        public List<PRPOVENDORDATA> PR_PO_VENDOR_LIST { get; set; }

        [DataMember(Name = "PR_ID")]
        public int PR_ID { get; set; }

        [DataMember(Name = "totalCount")]
        public int TotalCount { get; set; }
    }

    public class PRPOVENDORDATA
    {

        [DataMember(Name = "QUOTED_ON")]
        public DateTime? QUOTED_ON { get; set; }

        [DataMember(Name = "VENDOR_FULL_NAME")]
        public string VENDOR_FULL_NAME { get; set; }

        [DataMember(Name = "VENDOR_COMP_NAME")]
        public string VENDOR_COMP_NAME { get; set; }

        [DataMember(Name = "REQ_ID")]
        public int REQ_ID { get; set; }

        [DataMember(Name = "PR_ID")]
        public int PR_ID { get; set; }

    }
}