﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;
using PRM.Core.Models;

namespace PRMServices.Models.Catalog
{
    [DataContract]
    public class Category : EntityExt
    {
        [DataMember(Name = "catId")]
        public int CategoryId { get; set; }

        [DataMember(Name = "totalCategories")]
        public int TotalCategories { get; set; }

        [DataMember(Name = "compId")]
        public int CompanyId { get; set; }

        [DataMember(Name = "catCode")]
        public string CategoryCode { get; set; }

        [DataMember(Name = "catName")]
        public string CategoryName { get; set; }

        [DataMember(Name = "catDesc")]
        public string CategoryDesc { get; set; }

        [DataMember(Name = "catPath")]
        public string CategoryPath { get; set; }

        [DataMember(Name = "catOrder")]
        public int CategoryOrder { get; set; }

        [DataMember(Name = "catParentId")]
        public int CatParentId { get; set; }

        [DataMember(Name = "isValid")]
        public int IsValid { get; set; }

        [DataMember(Name = "isDefault")]
        public int IsDefault { get; set; }

        [DataMember(Name = "subCatCount")]
        public int ChildCount { get; set; }

        [DataMember(Name = "catSelected")]
        public int catSelected { get; set; }

        [DataMember(Name = "nodeChecked")]
        public bool nodeChecked { get; set; }

        [DataMember(Name = "nodes")]
        public List<Category> subCategories { get; set; }

        [DataMember(Name = "departments")]
        public string Departments { get; set; }

        [DataMember(Name = "IS_CORE")]
        public int IS_CORE { get; set; }
    }
}