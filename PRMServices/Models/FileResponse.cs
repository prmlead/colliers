﻿using PRM.Core.Common;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;


namespace PRMServices.Models
{
    [DataContract]
    public class FileResponse
    {
        [DataMember] [DataNames("fileName")] public string FileName { get; set; }
        [DataMember] [DataNames("fileStream")] public Stream FileStream { get; set; }
        [DataMember] [DataNames("message")] public string Message { get; set; }
        [DataMember] [DataNames("fileId")] public int? FileId { get; set; }
    }
}