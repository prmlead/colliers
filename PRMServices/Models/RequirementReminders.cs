﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace PRMServices.Models
{
    public class RequirementReminders : Entity
    {
        [DataMember(Name = "requirementID")]
        public int RequirementID { get; set; }

        [DataMember(Name = "requirementTitle")]
        public string RequirementTitle { get; set; }

        [DataMember(Name = "startTime")]
        public DateTime StartTime { get; set; }

        [DataMember(Name = "userID")]
        public int UserID { get; set; }

        [DataMember(Name = "phone")]
        public string Phone { get; set; }

        [DataMember(Name = "email")]
        public string Email { get; set; }

        [DataMember(Name = "firstName")]
        public string FirstName { get; set; }

        [DataMember(Name = "lastName")]
        public string LastName { get; set; }

        [DataMember(Name = "quotationfreeztime")]
        public DateTime QuotationFreezTime { get; set; }

        [DataMember(Name = "job")]
        public string Job { get; set; }

        [DataMember(Name = "requirementNumber")]
        public string RequirementNumber { get; set; }
        
        [DataMember(Name = "noOfQuotatioReminders")]
        public int NoOfQuotatioReminders { get; set; }

        [DataMember(Name = "remindersTimeInterval")]
        public int RemindersTimeInterval { get; set; }

        [DataMember(Name = "noOfQuotatioRemindersSent")]
        public int NoOfQuotatioRemindersSent { get; set; }            
    }
}