﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace PRMServices.Models
{
    [DataContract]
    public class LogisticQuotationItems : Entity
    {
        [DataMember(Name = "quotationItemID")]
        public int QuotationItemID { get; set; }

        [DataMember(Name = "itemID")]
        public int ItemID { get; set; }

        [DataMember(Name = "vendorID")]
        public int VendorID { get; set; }

        [DataMember(Name = "requirementID")]
        public int RequirementID { get; set; }

        string airline = string.Empty;
        [DataMember(Name = "airline")]
        public string Airline
        {
            get
            {
                if (!string.IsNullOrEmpty(airline))
                { return airline; }
                else
                { return ""; }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                { airline = value; }
            }
        }






        [DataMember(Name = "unitTariff")]
        public double unitTariff { get; set; }

        [DataMember(Name = "unitPrice")]
        public double UnitPrice { get; set; }

        [DataMember(Name = "revUnitPrice")]
        public double RevUnitPrice { get; set; }

        [DataMember(Name = "itemPrice")]
        public double ItemPrice { get; set; }

        [DataMember(Name = "revitemPrice")]
        public double RevItemPrice { get; set; }

        [DataMember(Name = "cGst")]
        public double CGst { get; set; }

        [DataMember(Name = "sGst")]
        public double SGst { get; set; }

        [DataMember(Name = "iGst")]
        public double IGst { get; set; }

        [DataMember(Name = "xRay")]
        public double XRay { get; set; }

        [DataMember(Name = "fsc")]
        public double Fsc { get; set; }

        [DataMember(Name = "ssc")]
        public double Ssc { get; set; }

        [DataMember(Name = "misc")]
        public double Misc { get; set; }

        [DataMember(Name = "hazCharges")]
        public double HazCharges { get; set; }

        [DataMember(Name = "customClearance")]
        public double customClearance { get; set; }

        [DataMember(Name = "revCustomClearance")]
        public double RevCustomClearance { get; set; }

        [DataMember(Name = "cgFees")]
        public double CgFees { get; set; }

        [DataMember(Name = "adc")]
        public double Adc { get; set; }

        [DataMember(Name = "awbFees")]
        public double AwbFees { get; set; }

        [DataMember(Name = "ediFees")]
        public double EdiFees { get; set; }

        [DataMember(Name = "serviceCharges")]
        public double ServiceCharges { get; set; }

        [DataMember(Name = "revServiceCharges")]
        public double RevServiceCharges { get; set; }

        [DataMember(Name = "otherCharges")]
        public double OtherCharges { get; set; }

        [DataMember(Name = "revOtherCharges")]
        public double RevOtherCharges { get; set; }

        [DataMember(Name = "terminalHandling")]
        public double TerminalHandling { get; set; }

        [DataMember(Name = "ams")]
        public double Ams { get; set; }

        [DataMember(Name = "forwordDgFees")]
        public double ForwordDgFees { get; set; }







        [DataMember(Name = "unitTariffType")]
        public int unitTariffType { get; set; }

        [DataMember(Name = "unitPriceType")]
        public int UnitPriceType { get; set; }

        [DataMember(Name = "revUnitPriceType")]
        public int RevUnitPriceType { get; set; }

        [DataMember(Name = "itemPriceType")]
        public int ItemPriceType { get; set; }

        [DataMember(Name = "revitemPriceType")]
        public int RevItemPriceType { get; set; }

        [DataMember(Name = "cGstType")]
        public int CGstType { get; set; }

        [DataMember(Name = "sGstType")]
        public int SGstType { get; set; }

        [DataMember(Name = "iGstType")]
        public int IGstType { get; set; }

        [DataMember(Name = "xRayType")]
        public int XRayType { get; set; }

        [DataMember(Name = "fscType")]
        public int FscType { get; set; }

        [DataMember(Name = "sscType")]
        public int SscType { get; set; }

        [DataMember(Name = "miscType")]
        public int MiscType { get; set; }

        [DataMember(Name = "hazChargesType")]
        public int HazChargesType { get; set; }

        [DataMember(Name = "customClearanceType")]
        public int customClearanceType { get; set; }

        [DataMember(Name = "revCustomClearanceType")]
        public int RevCustomClearanceType { get; set; }

        [DataMember(Name = "cgFeesType")]
        public int CgFeesType { get; set; }

        [DataMember(Name = "adcType")]
        public int AdcType { get; set; }

        [DataMember(Name = "awbFeesType")]
        public int AwbFeesType { get; set; }

        [DataMember(Name = "ediFeesType")]
        public int EdiFeesType { get; set; }

        [DataMember(Name = "serviceChargesType")]
        public int ServiceChargesType { get; set; }

        [DataMember(Name = "revServiceChargesType")]
        public int RevServiceChargesType { get; set; }

        [DataMember(Name = "otherChargesType")]
        public int OtherChargesType { get; set; }

        [DataMember(Name = "revOtherChargesType")]
        public int RevOtherChargesType { get; set; }

        [DataMember(Name = "terminalHandlingType")]
        public int TerminalHandlingType { get; set; }

        [DataMember(Name = "amsType")]
        public int AmsType { get; set; }

        [DataMember(Name = "forwordDgFeesType")]
        public int ForwordDgFeesType { get; set; }






        string routing = string.Empty;
        [DataMember(Name = "routing")]
        public string Routing
        {
            get
            {
                if (!string.IsNullOrEmpty(routing))
                { return routing; }
                else
                { return ""; }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                { routing = value; }
            }
        }

        string transit = string.Empty;
        [DataMember(Name = "transit")]
        public string Transit
        {
            get
            {
                if (!string.IsNullOrEmpty(routing))
                { return transit; }
                else
                { return ""; }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                { transit = value; }
            }
        }

        [DataMember(Name = "isSelected")]
        public int IsSelected { get; set; }

        [DataMember(Name = "isDeleted")]
        public int IsDeleted { get; set; }
        
        [DataMember(Name = "productImageID")]
        public int ProductImageID { get; set; }
        
        string attachmentName = string.Empty;
        [DataMember(Name = "attachmentName")]
        public string AttachmentName
        {
            get
            {
                if (!string.IsNullOrEmpty(attachmentName))
                { return attachmentName; }
                else
                { return ""; }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                { attachmentName = value; }
            }
        }

        string attachmentBase64 = string.Empty;
        [DataMember(Name = "attachmentBase64")]
        public string AttachmentBase64
        {
            get
            {
                if (!string.IsNullOrEmpty(attachmentBase64))
                { return attachmentBase64; }
                else
                { return ""; }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                { attachmentBase64 = value; }
            }
        }

        [DataMember(Name = "itemAttachment")]
        public byte[] ItemAttachment { get; set; }

        [DataMember(Name = "quotation")]
        public byte[] Quotation { get; set; }

        [DataMember(Name = "fileType")]
        public string FileType { get; set; }

        [DataMember(Name = "isRevised")]
        public int IsRevised { get; set; }


        [DataMember(Name = "isEnabled")]
        public Boolean IsEnabled { get; set; }

    }
}