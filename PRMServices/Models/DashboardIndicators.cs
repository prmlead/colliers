﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;
using PRM.Core.Common;

namespace PRMServices.Models
{
    [DataContract]
    public class DashboardIndicators : Entity
    {
        [DataMember] [DataNames("CHART_SERIES")] public List<ChartValues> CHART_SERIES { get; set; }
        [DataMember] [DataNames("TOTAL_UNIQUE_QUOTE_SUBMIT_VENDORS")] public int TOTAL_UNIQUE_QUOTE_SUBMIT_VENDORS { get; set; }
        [DataMember] [DataNames("TOTAL_VENDORS_CREATED")] public int TOTAL_VENDORS_CREATED { get; set; }
        [DataMember] [DataNames("TOTAL_TRANSACTIONS")] public int TOTAL_TRANSACTIONS { get; set; }
        [DataMember] [DataNames("TOTAL_QCS_CREATED")] public int TOTAL_QCS_CREATED { get; set; }
        [DataMember] [DataNames("TOTAL_SAVINGS")] public decimal TOTAL_SAVINGS { get; set; }
        [DataMember] [DataNames("QCS_TOTAL_SAVINGS")] public decimal QCS_TOTAL_SAVINGS { get; set; }
        [DataMember] [DataNames("TOTAL_PROCUREMENT_VALUE")] public decimal TOTAL_PROCUREMENT_VALUE { get; set; }
        [DataMember] [DataNames("TOTAL_PROFIT")] public decimal TOTAL_PROFIT { get; set; }
        [DataMember] [DataNames("TOTAL_LOSS")] public decimal TOTAL_LOSS { get; set; }
        [DataMember] [DataNames("TOTAL_PO_COUNT")] public int TOTAL_PO_COUNT { get; set; }
        [DataMember] [DataNames("TOTAL_ASN_COUNT")] public int TOTAL_ASN_COUNT { get; set; }
        [DataMember] [DataNames("TOTAL_INVOICE_COUNT")] public int TOTAL_INVOICE_COUNT { get; set; }
    }

    [DataContract]
    public class ChartValues
    {
        [DataMember] [DataNames("COUNT_BY_DATES")] public int COUNT_BY_DATES { get; set; }
        [DataMember] [DataNames("POSTED_DATES")] public DateTime? POSTED_DATES { get; set; }
        [DataMember] [DataNames("MODULE_NAME")] public string MODULE_NAME { get; set; }
    }

}