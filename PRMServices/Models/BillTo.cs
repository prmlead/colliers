﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;
using PRM.Core.Common;

namespace PRMServices.Models
{
    [DataContract]
    public class BillTo
    {
        [DataMember] [DataNames("BILL_TO_ID")] public int BILL_TO_ID { get; set; }
        [DataMember] [DataNames("COMP_ID")] public int COMP_ID { get; set; }
        [DataMember] [DataNames("U_ID")] public int U_ID { get; set; }
        [DataMember] [DataNames("LEGAL_ENTITY_NAME")] public string LEGAL_ENTITY_NAME { get; set; }
        [DataMember] [DataNames("ADDRESS")] public string ADDRESS { get; set; }
        [DataMember] [DataNames("SPOC_EMAIL_ADDRESS")] public string SPOC_EMAIL_ADDRESS { get; set; }
        [DataMember] [DataNames("GSTIN")] public string GSTIN { get; set; }
        [DataMember] [DataNames("PAN")] public string PAN { get; set; }
        [DataMember] [DataNames("DATE_CREATED")] public DateTime? DATE_CREATED { get; set; }
        [DataMember] [DataNames("DATE_MODIFIED")] public DateTime? DATE_MODIFIED { get; set; }


    }
}