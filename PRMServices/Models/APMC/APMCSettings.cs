﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace PRMServices.Models
{
    [DataContract]
    public class APMCSettings : Entity
    {
        [DataMember(Name = "apmcID")]
        public int APMCID { get; set; }

        [DataMember(Name = "apmcSettingsID")]
        public int APMCSettingsID { get; set; }

        [DataMember(Name = "relatedCess")]
        public double RelatedCess { get; set; }

        [DataMember(Name = "hamali")]
        public double Hamali { get; set; }

        [DataMember(Name = "freight")]
        public double Freight { get; set; }

        [DataMember(Name = "sutli")]
        public double Sutli { get; set; }

        [DataMember(Name = "sc")]
        public double SC { get; set; }

        [DataMember(Name = "brokerage")]
        public double Brokerage { get; set; }

        [DataMember(Name = "adat")]
        public double Adat { get; set; }

        [DataMember(Name = "packing")]
        public double Packing { get; set; }

        [DataMember(Name = "companyID")]
        public int CompanyID { get; set; }

        [DataMember(Name = "customer")]
        public User Customer { get; set; }

        [DataMember(Name = "apmcSettingID")]
        public int APMCSettingID { get; set; }
    }
}