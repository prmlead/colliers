﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;
using PRM.Core.Common;

namespace PRMServices.Models
{
    [DataContract]
    public class QCSPRDetails : Entity
    {

        
        [DataMember] [DataNames("REQ_ID")] public int REQ_ID { get; set; }
        [DataMember] [DataNames("RFQ_CREATOR")] public int RFQ_CREATOR { get; set; }
        [DataMember] [DataNames("PURCHASE_GROUP")] public string PURCHASE_GROUP { get; set; }
        [DataMember] [DataNames("REQ_STATUS")] public string REQ_STATUS { get; set; }
        [DataMember] [DataNames("PR_NUMBER")] public string PR_NUMBER { get; set; }
        [DataMember] [DataNames("REQ_CURRENCY")] public string REQ_CURRENCY { get; set; }
        [DataMember] [DataNames("VENDOR_DETAILS")] public List<vendorCurrency> VENDOR_DETAILS { get; set; }


    }

    public class vendorCurrency : Entity
    {
        [DataMember] [DataNames("U_ID")] public int U_ID { get; set; }

        [DataMember] [DataNames("VENDOR_NAME")] public string VENDOR_NAME { get; set; }

        [DataMember] [DataNames("SELECTED_VENDOR_CURRENCY")] public string SELECTED_VENDOR_CURRENCY { get; set; }
        [DataMember] [DataNames("VENDOR_CURRENCY")] public string VENDOR_CURRENCY { get; set; }
        [DataMember] [DataNames("VENDOR_CURRENCY_ID")] public int VENDOR_CURRENCY_ID { get; set; }
    }
}