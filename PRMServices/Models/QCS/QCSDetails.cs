﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;
using PRM.Core.Common;

namespace PRMServices.Models
{
    [DataContract]
    public class QCSDetails : Entity
    {

        [DataMember] [DataNames("QCS_ID")] public int QCS_ID { get; set; }
        [DataMember] [DataNames("QCS_CODE")] public string QCS_CODE { get; set; }
        [DataMember] [DataNames("QCS_TYPE")] public string QCS_TYPE { get; set; }
        [DataMember] [DataNames("REQ_ID")] public int REQ_ID { get; set; }
        [DataMember] [DataNames("REQ_NUMBER")] public string REQ_NUMBER { get; set; }
        [DataMember] [DataNames("PO_CODE")] public string PO_CODE { get; set; }
        [DataMember] [DataNames("RECOMMENDATIONS")] public string RECOMMENDATIONS { get; set; }
        [DataMember] [DataNames("UNIT_CODE")] public string UNIT_CODE { get; set; }
        [DataMember] [DataNames("U_ID")] public int U_ID { get; set; }
        [DataMember] [DataNames("CREATED_USER")] public string CREATED_USER { get; set; }
        [DataMember] [DataNames("CREATED_DATE")] public DateTime? CREATED_DATE { get; set; }
        [DataMember] [DataNames("MODIFIED_DATE")] public DateTime? MODIFIED_DATE { get; set; }
        [DataMember] [DataNames("WF_ID")] public int WF_ID { get; set; }
        [DataMember] [DataNames("REQ_JSON")] public string REQ_JSON { get; set; }
        [DataMember] [DataNames("CREATED_BY")] public int CREATED_BY { get; set; }
        [DataMember] [DataNames("MODIFIED_BY")] public int MODIFIED_BY { get; set; }
        [DataMember] [DataNames("IS_PRIMARY_ID")] public int IS_PRIMARY_ID { get; set; }
        [DataMember] [DataNames("IS_VALID")] public int IS_VALID { get; set; }
        [DataMember] [DataNames("IS_TAX_INCLUDED")] public bool IS_TAX_INCLUDED { get; set; }
        [DataMember] [DataNames("REQ_TITLE")] public string REQ_TITLE { get; set; }
        [DataMember] [DataNames("APPROVER_RANGE")] public decimal APPROVER_RANGE { get; set; }
        [DataMember] [DataNames("SAVINGS")] public decimal SAVINGS { get; set; }
        [DataMember] [DataNames("SAVINGS_IN_REQUIRED_CURRENCY")] public decimal SAVINGS_IN_REQUIRED_CURRENCY { get; set; }
        [DataMember] [DataNames("TOTAL_PROCUREMENT_VALUE")] public decimal TOTAL_PROCUREMENT_VALUE { get; set; }
        [DataMember] [DataNames("TOTAL_PROCUREMENT_VALUE_IN_REQUIRED_CURRENCY")] public decimal TOTAL_PROCUREMENT_VALUE_IN_REQUIRED_CURRENCY { get; set; }
        [DataMember] [DataNames("REQ_CURRENCY_FACTOR")] public decimal REQ_CURRENCY_FACTOR { get; set; }
        [DataMember] [DataNames("APPROVAL_COUNT")] public int APPROVAL_COUNT { get; set; }
        [DataMember] [DataNames("FIRST_APPROVER_DESIG_ID")] public int FIRST_APPROVER_DESIG_ID { get; set; }
        [DataMember] [DataNames("FIRST_APPROVER_DEPT_ID")] public int FIRST_APPROVER_DEPT_ID { get; set; }
        [DataMember] [DataNames("STATUS")] public string STATUS { get; set; }
        [DataMember] [DataNames("VENDOR_ITEM_ASSIGNMENT")] public Vendor.VendorItemAssignment[] VENDOR_ITEM_ASSIGNMENT { get; set; }
        [DataMember] [DataNames("TOTAL_PROFIT")] public decimal TOTAL_PROFIT { get; set; }
        [DataMember] [DataNames("TOTAL_LOSS")] public decimal TOTAL_LOSS { get; set; }
        [DataMember] [DataNames("LPP_VALUE")] public List<LPPEntity> LPP_VALUE { get; set; }
        [DataMember] [DataNames("PROJECT_ID")] public int PROJECT_ID { get; set; }
        [DataMember] [DataNames("BUDGET_ID")] public int BUDGET_ID { get; set; }
        [DataMember] [DataNames("SENT_FOR_APPROVAL")] public int SENT_FOR_APPROVAL { get; set; }
        [DataMember] [DataNames("PACKAGE_APPROVAL_ID")] public int PACKAGE_APPROVAL_ID { get; set; }

    }
}