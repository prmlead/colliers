﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;
using PRM.Core.Common;

namespace PRMServices.Models
{
    [DataContract]
    public class TermsConditions : ResponseAudit
    {
        [DataMember] [DataNames("TERM_ID")] public int TERM_ID { get; set; }
        [DataMember] [DataNames("U_ID")] public int U_ID { get; set; }
        [DataMember] [DataNames("COMP_ID")] public int COMP_ID { get; set; }
        [DataMember] [DataNames("MAP_ID")] public int MAP_ID { get; set; }
        [DataMember] [DataNames("PO_TYPE")] public string PO_TYPE { get; set; }
        [DataMember] [DataNames("TITLE")] public string TITLE { get; set; }
        [DataMember] [DataNames("DESCRIPTION")] public string DESCRIPTION { get; set; }
        [DataMember] [DataNames("IsValid")] public int IsValid { get; set; }
        [DataMember] [DataNames("IS_REQUIRED")] public string IS_REQUIRED { get; set; }
        // [DataMember] [DataNames("sessionID")] public string sessionID { get; set; }


    }
}