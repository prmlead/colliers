﻿prmApp
    .controller('listprCtrl', ["$rootScope", "$scope", "$stateParams", "$log", "$state", "$window", "userService", "auctionsService", "storeService", "growlService", "PRMPRServices",
        "PRMCustomFieldService", "catalogService", "$location",
        function ($rootScope, $scope, $stateParams, $log, $state, $window, userService, auctionsService, storeService, growlService, PRMPRServices, PRMCustomFieldService, catalogService, $location) {

            $scope.userID = userService.getUserId();
            $scope.sessionID = userService.getUserToken();
            $scope.deptID = userService.getSelectedUserDepartmentDesignation().deptID;
            $scope.sessionID = userService.getUserToken();
            $scope.compId = userService.getUserCompanyId();
            $scope.myAuctions1 = [];
            $scope.myAuctionsFiltred = [];
            $scope.selectedPRItems = [];
            $scope.prmTemplates = [];
            $scope.selectedTemplate = {};
            $scope.selectedRFP;
            $scope.selectedPR;
            $scope.chooseContractOption = false;
            $scope.stateDetails = {
                poTemplate: 'po-contract-domestic-zsdm'
            };
            $scope.filteredRequirements = [];
            $scope.PRStats = {
                totalPRs: 0,
                newPRs: 0,
                partialPRs: 0,
                inProgressPRs: 0,
                totalPRItems: 0,
                totalRFQPosted: 0
            };
            $scope.prExcelReport = [];

            /********  CONSOLIDATE PR ********/
            $scope.prDet = {
                prLevel: true
            };
            /********  CONSOLIDATE PR ********/

            $scope.filtersList = {
                plantList: [],
                wbsCodeList: [],
                projectTypeList: [],
                projectNameList: [],
                sectionHeadList: [],
                purchaseGroupList1: [],
                statusList: [],
                profitCentreList: [],
                requisitionList: []
            };

            $scope.filters = {
                status: '',
                plant: {},
                projectType: {},
                sectionHead: {},
                wbsCode: {},
                projectName: {},
                purcahseGroup: {},
                material: '',
                docType: '',
                purchaseGroup: '',
                searchKeyword: '',
                profitCentre: {},
                requisitioners: {},
                newPrStatus: {},
                requisitionerName: {},
                searchRequirement: '',
                selectedRequirement: null,
                prToDate: '',
                prFromDate: ''
            };

            $scope.filters.prToDate = moment().format('YYYY-MM-DD');
            $scope.filters.prFromDate = moment().subtract(30, "days").format("YYYY-MM-DD");
            $scope.PRItemStats = {
                totalPRs: 0,
                newPRs: 0,
                partialPRs: 0,
                inProgressPRs: 0,
                totalPRItems: 0,
                totalRFQPosted: 0
            };
            /*PAGINATION CODE*/
            $scope.totalItems = 0;
            $scope.currentPage = 1;
            $scope.itemsPerPage = 10;
            $scope.maxSize = 5;

            $scope.PlantsList = [{
                FIELD_NAME: 'All',
                FIELD_VALUE: ''
            }];
           
            $scope.materialGroupList = [{
                FIELD_NAME: 'All',
                FIELD_VALUE: ''
            }];
            $scope.purchaseGroupList = [{
                FIELD_NAME: 'All',
                FIELD_VALUE: ''
            }];
            $scope.docTypeList = [{
                FIELD_NAME: 'All',
                FIELD_VALUE: ''
            }];
            $scope.prStatusList = [{
                FIELD_NAME: 'All',
                FIELD_VALUE: ''
            }];

            $scope.setPage = function (pageNo) {
                $scope.currentPage = pageNo;
                $scope.getprlist(($scope.currentPage - 1), 10, $scope.filters.searchKeyword);
            };

            $scope.pageChanged = function () {
                ////console.log('Page changed to: ' + $scope.currentPage);
            };

            $scope.totalItems2 = 0;
            $scope.currentPage2 = 1;
            $scope.itemsPerPage2 = 10;
            $scope.maxSize2 = 5;

            $scope.setPage2 = function (pageNo) {
                $scope.currentPage1 = pageNo;
            };

            $scope.pageChanged2 = function () {
                ////console.log('Page changed to: ' + $scope.currentPage);
            };

            /*PAGINATION CODE*/


            $scope.PRList = [];
            $scope.filteredPRList = [];
            $scope.PRItems = [];

            $('.selected-items-box').bind('click', function (e) {
                $('.multiple-selection-dropdown .list').slideToggle('fast');
            });
            var isFilter = false;
            $scope.setFilters = function (currentPage) {
                $scope.PRStats.totalPRs = 0;
                $scope.PRStats.newPRs =  0;
                $scope.PRStats.partialPRs = 0;
                $scope.PRStats.totalPRItems = 0;
                $scope.PRStats.totalRFQPosted = 0;
                $scope.PRStats.inProgressPRs = 0;

                if ($scope.prDet.prLevel) {
                    $scope.filteredPRList = $scope.PRList;
                    $scope.totalItems = $scope.filteredPRList.length;


                    if ($scope.filters.searchKeyword || !_.isEmpty($scope.filters.newPrStatus) || !_.isEmpty($scope.filters.profitCentre) || !_.isEmpty($scope.filters.plant) ||
                        !_.isEmpty($scope.filters.projectType) || !_.isEmpty($scope.filters.requisitionerName) ||
                        !_.isEmpty($scope.filters.sectionHead) || !_.isEmpty($scope.filters.wbsCode) || !_.isEmpty($scope.filters.purcahseGroup) ||
                        !_.isEmpty($scope.filters.projectName)) {
                       // $scope.getprlist(currentPage, 10, $scope.filters.searchKeyword);
                        $scope.getprlist(0, 10, $scope.filters.searchKeyword);
                    } else {
                        
                        if ($scope.initialPRPageArray && $scope.initialPRPageArray.length > 0) {
                            $scope.PRList = $scope.initialPRPageArray;
                            if ($scope.PRList && $scope.PRList.length > 0) {
                                $scope.totalItems = $scope.PRList[0].TOTAL_PR_COUNT;
                                $scope.PRStats.totalPRs = $scope.totalItems;
                                $scope.PRStats.newPRs = $scope.PRList[0].newPRs;
                                $scope.PRStats.partialPRs = $scope.PRList[0].partialPRs;
                                $scope.PRStats.totalPRItems = $scope.PRList[0].totalPRItems;
                                $scope.PRStats.totalRFQPosted = $scope.PRList[0].totalRFQPosted;
                                $scope.PRStats.inProgressPRs = $scope.PRList[0].inProgressPRs;
                                $scope.filteredPRList = $scope.PRList;
                            }

                        }
                    }
                } else {
                    $scope.getPRSBasedOnItem(0, 10);
                }
                
            };

            $scope.filterByDate = function () {
                $scope.PRStats.totalPRs = 0;
                $scope.PRStats.newPRs =  0;
                $scope.PRStats.partialPRs = 0;
                $scope.PRStats.totalPRItems = 0;
                $scope.PRStats.totalRFQPosted = 0;
                $scope.PRStats.inProgressPRs = 0;

                if ($scope.prDet.prLevel) {
                    $scope.getFilterValues();
                    $scope.filteredPRList = $scope.PRList;
                    $scope.totalItems = $scope.filteredPRList.length;
                    $scope.getprlist(0, 10, $scope.filters.searchKeyword);
                } else {
                    $scope.getPRSBasedOnItem(0,10);
                }
            };
            
            $scope.totalCount = 0;
            $scope.searchString = '';
            $scope.initialPRPageArray = [];

            $scope.getprlist = function (recordsFetchFrom,pageSize,searchString) {
                
                var plant, projectType, sectionHead, wbsCode, profitCentre, purchaseCode, creatorName, clientName, prStatus, fromDate, toDate;

                if (_.isEmpty($scope.filters.prFromDate)) {
                    fromDate = '';
                } else {
                    fromDate = $scope.filters.prFromDate;
                }

                if (_.isEmpty($scope.filters.prToDate)) {
                    toDate = '';
                } else {
                    toDate = $scope.filters.prToDate;
                }

                if (_.isEmpty($scope.filters.plant)) {
                    plant = '';
                } else if ($scope.filters.plant && $scope.filters.plant.length > 0) {
                    var plants = _($scope.filters.plant)
                        .filter(item => item.id)
                        .map('id')
                        .value();
                    plant = plants.join(',');
                }

                if (_.isEmpty($scope.filters.projectType)) {
                    projectType = '';
                } else if ($scope.filters.projectType && $scope.filters.projectType.length > 0) {
                    var projectTypes = _($scope.filters.projectType)
                        .filter(item => item.name)
                        .map('name')
                        .value();
                    projectType = projectTypes.join(',');
                }

                if (_.isEmpty($scope.filters.sectionHead)) {
                    sectionHead = '';
                } else if ($scope.filters.sectionHead && $scope.filters.sectionHead.length > 0) {
                    var sectionHeads = _($scope.filters.sectionHead)
                        .filter(item => item.name)
                        .map('name')
                        .value();
                    sectionHead = sectionHeads.join(',');
                }

                if (_.isEmpty($scope.filters.wbsCode)) {
                    wbsCode = '';
                } else if ($scope.filters.wbsCode && $scope.filters.wbsCode.length > 0) {
                    var wbsCodes = _($scope.filters.wbsCode)
                        .filter(item => item.name)
                        .map('name')
                        .value();
                    wbsCode = wbsCodes.join(',');
                }

                if (_.isEmpty($scope.filters.profitCentre)) {
                    profitCentre = '';
                } else if ($scope.filters.profitCentre && $scope.filters.profitCentre.length > 0) {
                    var profitCentres = _($scope.filters.profitCentre)
                        .filter(item => item.name)
                        .map('name')
                        .value();
                    profitCentre = profitCentres.join(',');
                }

                if (_.isEmpty($scope.filters.purcahseGroup)) {
                    purchaseCode = '';
                } else if ($scope.filters.purcahseGroup && $scope.filters.purcahseGroup.length > 0) {
                    var purchaseCodes = _($scope.filters.purcahseGroup)
                        .filter(item => item.name)
                        .map('name')
                        .value();
                    purchaseCode = purchaseCodes.join(',');
                }

                if (_.isEmpty($scope.filters.requisitionerName)) {
                    creatorName = '';
                } else if ($scope.filters.requisitionerName && $scope.filters.requisitionerName.length > 0) {
                    var creators = _($scope.filters.requisitionerName)
                        .filter(item => item.id)
                        .map('id')
                        .value();
                    creatorName = creators.join(',');
                }

                if (_.isEmpty($scope.filters.projectName)) {
                    clientName = '';
                } else if ($scope.filters.projectName && $scope.filters.projectName.length > 0) {
                    var clientNames = _($scope.filters.projectName)
                        .filter(item => item.id)
                        .map('id')
                        .value();
                    clientName = clientNames.join(',');
                }

                if (_.isEmpty($scope.filters.newPrStatus)) {
                    prStatus = '';
                } else if ($scope.filters.newPrStatus && $scope.filters.newPrStatus.length > 0) {
                    var statusFilters = _($scope.filters.newPrStatus)
                        .filter(item => item.id)
                        .map('id')
                        .value();
                    prStatus = statusFilters.join(',');
                }
                
                var params = {
                    "userid": userService.getUserId(),
                    "sessionid": userService.getUserToken(),
                    "deptid": userService.getSelectedUserDepartmentDesignation().deptID,
                    "desigid": userService.getSelectedUserDesigID(),
                    "depttypeid": userService.getSelectedUserDepartmentDesignation().deptTypeID,
                    "plant": plant,
                    "projectType": projectType,
                    "sectionHead": sectionHead,
                    "wbsCode": wbsCode,
                    "profitCentre": profitCentre,
                    "purchaseCode": purchaseCode,
                    "creatorName": creatorName,
                    "clientName": clientName,
                    "prStatus": prStatus,
                    "searchString": searchString ? searchString : "",
                    "PageSize": recordsFetchFrom * pageSize,
                    'fromDate': fromDate,
                    'toDate': toDate,
                    "NumberOfRecords": pageSize
                };

                $scope.pageSizeTemp = (params.PageSize + 1);
                //$scope.NumberOfRecords = recordsFetchFrom > 0 ? ((recordsFetchFrom + 1) * pageSize) : $scope.PRStats.totalPRs;
                $scope.NumberOfRecords = ((recordsFetchFrom + 1) * pageSize);
               
                PRMPRServices.getprlist(params)
                    .then(function (response) {
                        //$scope.consolidatedReport = response;
                        if (response && response.length > 0) {
                            response.forEach(function (item, index) {
                                var releaseDateTemp = item.RELEASE_DATE ? moment(item.RELEASE_DATE).format("DD-MM-YYYY") : '-';
                                item.RELEASE_DATE = releaseDateTemp != '-' ? releaseDateTemp.contains("1970") ? '-' : releaseDateTemp : '-';
                                item.MODIFIED_DATE = userService.toLocalDate(item.MODIFIED_DATE).split(' ')[0];
                            });
                        }
                        if (!$scope.downloadExcel) {
                            $scope.PRList = [];
                            $scope.filteredPRList = [];
                            if (response && response.length > 0) {
                                response.forEach(function (item, index) {
                                    item.selectPRSForRFQ = false;
                                    $scope.PRList.push(item);
                                    if ($scope.initialPRPageArray.length <= 9) { // Push Initial 10 Records When Page is Loaded because needed in SetFilters function it's getting called every time (need to modify directive code)  #Crap Code need to remove(should think of another solution)
                                        $scope.initialPRPageArray.push(item);
                                    }
                                });

                            }

                            if ($scope.PRList && $scope.PRList.length > 0) {
                                $scope.totalItems = $scope.PRList[0].TOTAL_PR_COUNT;
                                $scope.PRStats.totalPRs = $scope.totalItems;
                                $scope.PRStats.newPRs = $scope.PRList[0].newPRs;
                                $scope.PRStats.partialPRs = $scope.PRList[0].partialPRs;
                                $scope.PRStats.totalPRItems = $scope.PRList[0].totalPRItems;
                                $scope.PRStats.totalRFQPosted = $scope.PRList[0].totalRFQPosted;
                                $scope.PRStats.inProgressPRs = $scope.PRList[0].inProgressPRs;
                                $scope.filteredPRList = $scope.PRList;
                            }
                        } else {
                            if (response && response.length > 0) {
                                $scope.prExcelReport = response;
                                downloadPRExcel()
                            } else {
                                swal("Error!", "No records.", "error");
                                $scope.downloadExcel = false;
                            }
                        }

                    });
            };

            $scope.getprlist(0,10,$scope.searchString);
            $scope.filterValues = [];

            $scope.getFilterValues = function () {
                var params =
                {
                    "compid": $scope.compId,
                    'fromDate': $scope.filters.prFromDate,
                    'toDate': $scope.filters.prToDate
                };

                let plantListTemp = [];
                let wbsCodeTemp = [];
                let projectTypeTemp = [];
                let projectNameTemp = [];
                let sectionHeadTemp = [];
                let purchaseGroupTemp = [];
                let statusTemp = [];
                let profitCentreTemp = [];
                let requisitionListTemp = [];

                PRMPRServices.getFilterValues(params)
                    .then(function (response) {
                        if (response && response.length > 0) {
                            $scope.filterValues = response;
                            if ($scope.filterValues && $scope.filterValues.length > 0 ) {
                                $scope.filterValues.forEach(function (item, index) {
                                    if (item.TYPE === 'WBS_CODE') {
                                        wbsCodeTemp.push({ id: item.ID, name: item.NAME });
                                    } else if (item.TYPE === 'PROFIT_CENTER') {
                                        profitCentreTemp.push({ id: item.ID, name: item.NAME });
                                    } else if (item.TYPE === 'PROJECT_TYPE') {
                                        projectTypeTemp.push({ id: item.ID, name: item.NAME });
                                    } else if (item.TYPE === 'SECTION_HEAD') {
                                        sectionHeadTemp.push({ id: item.ID, name: item.NAME });
                                    } else if (item.TYPE === 'PROJECT_DESCRIPTION') {
                                        projectNameTemp.push({ id: item.ID, name: item.NAME });
                                    } else if (item.TYPE === 'PLANTS') {
                                        plantListTemp.push({ id: +item.NAME, name: item.ID + ' - ' + item.NAME });
                                    } else if (item.TYPE === 'PURCHASE_GROUP') {
                                       // purchaseGroupTemp.push({ id: +item.NAME, name: item.ID });
                                        purchaseGroupTemp.push({ id: item.ID, name: item.NAME });
                                    } else if (item.TYPE === 'PR_CREATOR_NAME') {
                                        requisitionListTemp.push({ id: item.ID, name: item.NAME });
                                    } else if (item.TYPE === 'PR_STATUS') {
                                        statusTemp.push({ id: item.ID, name: item.NAME });
                                    }

                                });

                                $scope.filtersList.requisitionList = requisitionListTemp;
                                $scope.filtersList.plantList = plantListTemp;
                                $scope.filtersList.wbsCodeList = wbsCodeTemp;
                                $scope.filtersList.projectTypeList = projectTypeTemp;
                                $scope.filtersList.sectionHeadList = sectionHeadTemp;
                                $scope.filtersList.projectNameList = projectNameTemp;
                                $scope.filtersList.purchaseGroupList1 = purchaseGroupTemp;
                                $scope.filtersList.statusList = statusTemp;
                                $scope.filtersList.profitCentreList = profitCentreTemp;
                            }
                        }
                    });

            };
            $scope.getFilterValues();


            $scope.selectedPRItems = [];

            $scope.getPrItems = function (prDetails, createRequirement) {
                if (prDetails) {
                    var params = {
                        "prid": prDetails.PR_ID
                    };
                    PRMPRServices.getpritemslist(params)
                        .then(function (response) {
                            console.log(response);
                            $scope.PRItems = response;
                            $scope.PRItems.forEach(function (item, index) {
                                //item.CREATED_DATE = userService.toLocalDate(item.CREATED_DATE).split(' ')[0];
                                //item.DELIVERY_DATE = userService.toLocalDate(item.DELIVERY_DATE).split(' ')[0];
                            });

                            prDetails.PRItems = $scope.PRItems;

                            if (createRequirement) {
                                if (prDetails) {
                                    prDetails.isSelected = true;
                                    //$scope.selectedPRItems = [];
                                    if (prDetails.PRItems) {
                                        prDetails.PRItems.forEach(function (prItem) {
                                            prItem.isSelected = true;
                                        });
                                    }
                                    $scope.selectedPRItems.push(prDetails);
                                    //$scope.createRequirementMultiplePR();
                                }
                                $scope.selectedPRItems = _.uniqBy($scope.selectedPRItems, 'PR_ID');
                            }
                            
                            if (prDetails.selectPRSForRFQ) {
                                prDetails.PRItems.forEach(function (item, index) {
                                    item.isCheckedPrItem = true;
                                });
                            }
                        });
                }
            };


            $scope.goToPrEdit = function (id) {
                var url = $state.href("save-pr-details", { "Id": id });
                window.open(url, '_self');
            };

            $scope.goToPrAction = function (id) {
                var url = $state.href("pr-actions", { "Id": id });
                window.open(url, '_blank');
            };

            $scope.createRequirement = function (prDetails) {
                $scope.getPrItems(prDetails, true);
            };

            $scope.createRequirementMultiplePR = function () {
                //let validSelectedPRs = _.filter($scope.selectedPRItems, function (prObj) {
                //    return prObj.isSelected;
                //});

                ////if (validSelectedPRs && validSelectedPRs.length > 0) {
                ////    $state.go('save-requirementAdv', { 'prDetailsList': validSelectedPRs });
                ////}
            };

            $scope.addToRequirementList = function (prDetails, item, action) {
                if (action === 'ADD') {
                    item.isSelected = true;
                    prDetails.isSelected = true;
                    let index = _.findIndex($scope.selectedPRItems, function (pr) {
                        return pr.PR_ID === prDetails.PR_ID;
                    });

                    if (index < 0) {
                        $scope.selectedPRItems.push(prDetails);
                    }
                    
                } else {
                    item.isSelected = false;
                    let selectedPRItems = _.filter(prDetails.PRItems, function (prItem) {
                        return prItem.isSelected;
                    });

                    if (selectedPRItems && selectedPRItems.length > 0) {
                        prDetails.isSelected = true;
                    } else {
                        prDetails.isSelected = false;
                    }
                }


                console.log($scope.selectedPRItems);
            };

            $scope.showRequirementButton = function () {
                let isvisible = false;
                $scope.selectedPRNumbers = '';

                if ($scope.selectedPRItems && $scope.selectedPRItems.length > 0) {
                    let validSelectedPRs = _.filter($scope.selectedPRItems, function (prObj) {
                        return prObj.isSelected;
                    });

                    if (validSelectedPRs && validSelectedPRs.length > 0) {
                        var prNumbers = _(validSelectedPRs)
                            .map('PR_NUMBER')
                            .value();
                        $scope.selectedPRNumbers = prNumbers.join(',');
                        isvisible = true;

                    }
                }
                
                return isvisible;
            };
            $scope.requirementMappingClick = function () {
                $scope.ProductContractsLoaded = false;
                $scope.ProductContracts = [];
                $scope.ProductRequirements = [];
                $scope.getProductContracts();
                $scope.getProductRelatedRequirements();
            };
            $scope.getProductRelatedRequirements = function () {
                let productArray = [];
                $scope.contractPODetails = {
                    contractTable: [],
                    plantsArray: []
                }; //Keep local

                if ($scope.prDet.prLevel) {
                    let validSelectedPRs = _.filter($scope.selectedPRItems, function (prItemObj) {
                        return prItemObj.isSelected;
                    });

                    validSelectedPRs.forEach(function (prObj, index) {
                        prObj.PRItems.forEach(function (item, index1) {
                            if (!item.CONTRACT_NUMBER && !item.REQ_ID) {
                                productArray.push(item.PRODUCT_ID);
                                let contactTableExistngItem = _.filter($scope.contractPODetails.contractTable, function (tableItem) {
                                    return tableItem.productId === item.PRODUCT_ID;
                                });

                                if (!_.some($scope.contractPODetails.plantsArray, { plantCode: prObj.PLANT })) {
                                    $scope.contractPODetails.plantsArray.push({
                                        plantCode: prObj.PLANT,
                                        plantName: prObj.PLANT_NAME
                                    });
                                }
                                if (contactTableExistngItem && contactTableExistngItem.length > 0) {
                                    contactTableExistngItem[0].plantName = contactTableExistngItem[0].plantName + ',' + prObj.PLANT_NAME;
                                    contactTableExistngItem[0].plantCode = contactTableExistngItem[0].plantCode + ',' + prObj.PLANT;
                                    contactTableExistngItem[0].prNumber = contactTableExistngItem[0].prNumber + ',' + prObj.PR_NUMBER;
                                    contactTableExistngItem[0].prQuantity = contactTableExistngItem[0].prQuantity + prObj.REQUIRED_QUANTITY;
                                } else {
                                    $scope.contractPODetails.contractTable.push({
                                        productName: item.ITEM_NAME,
                                        productId: item.PRODUCT_ID,
                                        plantName: prObj.PLANT_NAME,
                                        plantCode: prObj.PLANT,
                                        plantCodeArr: [],
                                        prNumber: prObj.PR_NUMBER,
                                        prQuantity: item.REQUIRED_QUANTITY
                                    });
                                }
                            }
                        });
                    });
                }

                if (!$scope.prDet.prLevel && $scope.selectedItemsForRFQ && $scope.selectedItemsForRFQ.length > 0) {
                    //$scope.selectedItemsForRFQ.forEach(function (item, index) {
                    //    productArray.push(item.PRODUCT_ID);
                    //});

                    $scope.selectedItemsForRFQ.forEach(function (prItem, index) {
                        prItem.PlantCodeArr = [];
                        prItem.PRNumbers = '';
                        prItem.ItemPRS.forEach(function (prObj, index1) {
                            productArray.push(prItem.PRODUCT_ID);
                            prItem.PRNumbers = prItem.PRNumbers + ',' + prObj.PR_NUMBER;
                            let contactTableExistngItem = _.filter($scope.contractPODetails.contractTable, function (tableItem) {
                                return tableItem.productId === prItem.PRODUCT_ID;
                            });

                            if (!prItem.PlantCodeArr.includes(prObj.PLANT)) {
                                prItem.PlantCodeArr.push(prObj.PLANT);
                            }

                            if (!_.some($scope.contractPODetails.plantsArray, { plantCode: prObj.PLANT })) {
                                $scope.contractPODetails.plantsArray.push({
                                    plantCode: prObj.PLANT,
                                    plantName: prObj.PLANT_NAME
                                });
                            }

                            if (contactTableExistngItem && contactTableExistngItem.length > 0) {
                                contactTableExistngItem[0].plantName = contactTableExistngItem[0].plantName + ',' + prObj.PLANT_NAME;
                                contactTableExistngItem[0].plantCode = contactTableExistngItem[0].plantCode + ',' + prObj.PLANT;
                                contactTableExistngItem[0].prNumber = contactTableExistngItem[0].prNumber + ',' + prObj.PR_NUMBER;
                                contactTableExistngItem[0].prQuantity = contactTableExistngItem[0].prQuantity + prItem.RFQ_QUANTITY;
                            } else {
                                $scope.contractPODetails.contractTable.push({
                                    productName: prItem.ITEM_NAME,
                                    productId: prItem.PRODUCT_ID,
                                    plantName: prObj.PLANT_NAME,
                                    plantCode: prObj.PLANT,
                                    plantCodeArr: [],
                                    prNumber: prObj.PR_NUMBER,
                                    prQuantity: prItem.RFQ_QUANTITY
                                });
                            }
                        });
                    });
                }

                if (productArray && productArray.length > 0) {
                    let productIds = productArray.join();
                    if (productIds) {
                        auctionsService.GetRequirementsByProductIds({ "productids": productIds, "compid": $scope.compId, "sessionid": userService.getUserToken() })
                            .then(function (response) {
                                if (response) {
                                    $scope.ProductContractsLoaded = true;
                                    $scope.ProductRequirements = response;
                                    if ($scope.ProductRequirements && $scope.ProductRequirements.length > 0) {
                                        $scope.ProductRequirements[0].isSelected = true;
                                        $scope.ProductRequirements.forEach(function (item, index) {
                                            item.endTime = item.endTime ? userService.toLocalDate(item.endTime).split(' ')[0] : '-';
                                            item.isSelected = false;
                                        });
                                    }
                                }
                            });
                    }
                }
            };
           
            $scope.getProductContracts = function () {
                let productArray = [];
                $scope.contractPODetails = {
                    contractTable: [],
                    plantsArray: []
                }; //Keep local

                if ($scope.prDet.prLevel) {
                    let validSelectedPRs = _.filter($scope.selectedPRItems, function (prItemObj) {
                        return prItemObj.isSelected;
                    });

                    validSelectedPRs.forEach(function (prObj, index) {
                        prObj.PRItems.forEach(function (item, index1) {
                            if (!item.CONTRACT_NUMBER && !item.REQ_ID && item.isSelected) {
                                productArray.push(item.PRODUCT_ID);
                                let contactTableExistngItem = _.filter($scope.contractPODetails.contractTable, function (tableItem) {
                                    return tableItem.productId === item.PRODUCT_ID;
                                });

                                if (!_.some($scope.contractPODetails.plantsArray, { plantCode: prObj.PLANT })) {
                                    $scope.contractPODetails.plantsArray.push({
                                        plantCode: prObj.PLANT,
                                        plantName: prObj.PLANT_NAME
                                    });
                                }
                                if (contactTableExistngItem && contactTableExistngItem.length > 0) {
                                    contactTableExistngItem[0].plantName = contactTableExistngItem[0].plantName + ',' + prObj.PLANT_NAME;
                                    contactTableExistngItem[0].plantCode = contactTableExistngItem[0].plantCode + ',' + prObj.PLANT;
                                    contactTableExistngItem[0].prNumber = contactTableExistngItem[0].prNumber + ',' + prObj.PR_NUMBER;
                                    contactTableExistngItem[0].prQuantity = contactTableExistngItem[0].prQuantity + prObj.REQUIRED_QUANTITY;
                                } else {
                                    $scope.contractPODetails.contractTable.push({
                                        productName: item.ITEM_NAME,
                                        productId: item.PRODUCT_ID,
                                        plantName: prObj.PLANT_NAME,
                                        plantCode: prObj.PLANT,
                                        plantCodeArr: [],
                                        prNumber: prObj.PR_NUMBER,
                                        prQuantity: item.REQUIRED_QUANTITY
                                    });
                                }
                            }
                        });
                    });
                }

                if (!$scope.prDet.prLevel && $scope.selectedItemsForRFQ && $scope.selectedItemsForRFQ.length > 0) {
                    //$scope.selectedItemsForRFQ.forEach(function (item, index) {
                    //    productArray.push(item.PRODUCT_ID);
                    //});

                    $scope.selectedItemsForRFQ.forEach(function (prItem, index) {
                        prItem.PlantCodeArr = [];
                        prItem.PRNumbers = '';
                        prItem.ItemPRS.forEach(function (prObj, index1) {
                            productArray.push(prItem.PRODUCT_ID);
                            prItem.PRNumbers = prItem.PRNumbers + ',' + prObj.PR_NUMBER;
                            let contactTableExistngItem = _.filter($scope.contractPODetails.contractTable, function (tableItem) {
                                return tableItem.productId === prItem.PRODUCT_ID;
                            });

                            if (!prItem.PlantCodeArr.includes(prObj.PLANT)) {
                                prItem.PlantCodeArr.push(prObj.PLANT);
                            }

                            if (!_.some($scope.contractPODetails.plantsArray, { plantCode: prObj.PLANT })) {
                                $scope.contractPODetails.plantsArray.push({
                                    plantCode: prObj.PLANT,
                                    plantName: prObj.PLANT_NAME
                                });
                            }

                            if (contactTableExistngItem && contactTableExistngItem.length > 0) {
                                contactTableExistngItem[0].plantName = contactTableExistngItem[0].plantName + ',' + prObj.PLANT_NAME;
                                contactTableExistngItem[0].plantCode = contactTableExistngItem[0].plantCode + ',' + prObj.PLANT;
                                contactTableExistngItem[0].prNumber = contactTableExistngItem[0].prNumber + ',' + prObj.PR_NUMBER;
                                contactTableExistngItem[0].prQuantity = contactTableExistngItem[0].prQuantity + prItem.RFQ_QUANTITY;
                            } else {
                                $scope.contractPODetails.contractTable.push({
                                    productName: prItem.ITEM_NAME,
                                    productId: prItem.PRODUCT_ID,
                                    plantName: prObj.PLANT_NAME,
                                    plantCode: prObj.PLANT,
                                    plantCodeArr: [],
                                    prNumber: prObj.PR_NUMBER,
                                    prQuantity: prItem.RFQ_QUANTITY
                                });
                            }
                        });
                    });
                }

                if (productArray && productArray.length > 0) {
                    let productIds = productArray.join();
                    if (productIds) {
                        catalogService.GetProductContracts(productIds)
                            .then(function (response) {
                                if (response) {
                                    $scope.ProductContractsLoaded = true;
                                    console.log(response);
                                    $scope.ProductContracts = response;
                                    if ($scope.ProductContracts && $scope.ProductContracts.length > 0) {
                                        $scope.ProductContracts.forEach(function (contract) {
                                            contract.startTime = userService.toLocalDate(contract.startTime);
                                            contract.endTime = userService.toLocalDate(contract.endTime);
                                        });

                                        if ($scope.contractPODetails && $scope.contractPODetails.contractTable && $scope.contractPODetails.contractTable.length > 0) {
                                            $scope.contractPODetails.contractTable.forEach(function (contract) {
                                                contract.contactsArray = _.filter($scope.ProductContracts, function (contractitem) {
                                                    return contractitem.ProductId === contract.productId;
                                                });
                                            });
                                        }
                                    }
                                }
                            });
                    }
                }
            };
            $scope.chooseContract = function () {
                $scope.chooseContractOption = false;
                if ($scope.contractPODetails.contractTable && $scope.contractPODetails.contractTable.length > 0) {
                    $scope.contractPODetails.contractTable.forEach(function (contactItem) {
                        if (contactItem && contactItem.contactsArray && contactItem.contactsArray.length > 0) {
                            contactItem.contactsArray.forEach(function (contract) {
                                if (contract.isSelected) {
                                    $scope.chooseContractOption = true;
                                }
                            });
                        }
                    });
                }
            };
            $scope.navigateToPOForm = function () {
                if ($scope.contractPODetails && $scope.contractPODetails.contractTable && $scope.contractPODetails.contractTable.length > 0) {
                    angular.element('#templateSelection').modal('hide');
                    let selectedTemplate = '';
                    if ($scope.stateDetails.poTemplate === 'po-contract-domestic-zsdm') {
                        selectedTemplate = 'ZSDM';
                    } else if ($scope.stateDetails.poTemplate === 'po-contract-import-zsim') {
                        selectedTemplate = 'ZSIM';
                    } else if ($scope.stateDetails.poTemplate === 'po-contract-bonded-wh') {
                        selectedTemplate = 'ZSBW';
                    } else if ($scope.stateDetails.poTemplate === 'po-contract-service-zssr') {
                        selectedTemplate = 'ZSSR';
                    }

                    let prDetails = [];
                    let isGMP = false;
                    if ($scope.prDet.prLevel) {
                        prDetails = _.filter($scope.selectedPRItems, function (prItemObj) {
                            return prItemObj.isSelected;
                        });

                        let temp = _.filter(prDetails, function (prItemObj) {
                            return prItemObj.GMP === 'GMP';
                        });

                        if (temp && temp.length > 0) {
                            isGMP = true;
                        }

                        prDetails[0].isGMP = isGMP;
                    }
                    else {
                        var rfqPRSList = $scope.selectedItemsForRFQ.filter(function (item) {
                            return item.selectForRFQ && item.RFQ_QUANTITY !== item.OVERALL_ITEM_QUANTITY;
                        });

                        rfqPRSList.forEach(function (prItem, index) {
                            if (!isGMP && prItem && prItem.ItemPRS && prItem.ItemPRS.length > 0) {
                                prItem.ItemPRS.forEach(function (prObj, index) {
                                    if (!isGMP && prObj && prObj.GMP === 'GMP') {
                                        isGMP = true;
                                    }
                                });
                            }
                        });

                        prDetails.push({
                            PRItems: rfqPRSList
                        });

                        prDetails[0].isGMP = isGMP;
                    }

                    let vendorAssignments = [];
                    $scope.contractPODetails.contractTable.forEach(function (contactItem, index) {
                        if (contactItem) {
                            contactItem.contactsArray.forEach(function (item, index) {
                                if (item.isSelected) {
                                    vendorAssignments.push({
                                        vendorID: item.vendorId,
                                        vendorCode: item.selectedVendorCode,
                                        vendorName: item.companyName,
                                        itemID: item.ProductId,
                                        item: item,
                                        assignedQty: contactItem.prQuantity,
                                        assignedPrice: item.netPrice, //item.price,
                                        totalPrice: item.netPrice, // item.price, //contactItem.prQuantity * item.price,
                                        currency: '',
                                        contractNumber: item.number
                                    });
                                }
                            });
                        }
                    });

                    $state.go(
                        $scope.stateDetails.poTemplate,
                        { 'contractDetails': $scope.contractPODetails, 'prDetails': prDetails, 'detailsObj': vendorAssignments, 'templateName': selectedTemplate, 'quoteLink': $location.absUrl() }
                    );
                }
            };

            $scope.showRequirementItemsButton = function () {
                let isvisible = false;

                if ($scope.selectedItemsForRFQ && $scope.selectedItemsForRFQ.length > 0) {

                    let selectedPRItems = _.filter($scope.selectedItemsForRFQ, function (prItem) {
                        return prItem.selectForRFQ && prItem.OVERALL_ITEM_QUANTITY != prItem.RFQ_QUANTITY;
                    });

                    if (selectedPRItems && selectedPRItems.length > 0) {
                        isvisible = true;
                    }
                }
                return isvisible;
            };

            $scope.showLinkToRFP = function (prDetails) {
                $scope.selectedPR = prDetails;
                $scope.getAuctions();
            };

            $scope.getAuctions = function () {
                auctionsService.SearchRequirements({ "search": '', "excludeprlinked": true, "compid": $scope.compId, "sessionid": userService.getUserToken() })
                    .then(function (response) {
                        if (response) {
                            $scope.myAuctions1 = [];
                            $scope.myAuctionsFiltred = [];
                            $scope.myAuctions1 = response;
                            $scope.myAuctionsFiltred = $scope.myAuctions1;
                        }
                    });
            };

            $scope.LinkToRFP = function () {
                if ($scope.selectedPR && $scope.myAuctionsFiltred && $scope.myAuctionsFiltred.length > 0) {
                    let selectedRFPs = $scope.myAuctionsFiltred.filter(function (rfq) {
                        return rfq.isSelected;
                    });

                    if (selectedRFPs && selectedRFPs.length > 0) {
                        selectedRFPs.forEach(function (rfpDetails, itemIndexs) {
                            var params = {
                                "userid": userService.getUserId(),
                                "sessionid": userService.getUserToken(),
                                "reqid": rfpDetails.requirementId,
                                "prid": $scope.selectedPR.PR_ID
                            };
                            PRMPRServices.linkRFPToPR(params)
                                .then(function (response) {
                                    if (response && response.errorMessage === '') {
                                        growlService.growl("Successfully link to RFP", "success");
                                        angular.element('#linkRFP').modal('hide');
                                        $scope.myAuctionsFiltred = [];
                                        console.log(response);
                                        $scope.myAuctions1.forEach(function (rfpTemp, itemIndexs) {
                                            rfpTemp.isSelected = false;
                                        });
                                        $scope.myAuctionsFiltred.forEach(function (rfpTemp, itemIndexs) {
                                            rfpTemp.isSelected = false;
                                        });
                                    } else {
                                        growlService.growl("Error linking PR to RFP, please contact support team.", "inverse");
                                    }
                                });
                        });
                    }
                }
            };

            $scope.isLast = function (last) {
                var a = '';
                if (last) {
                    a = '';
                } else {
                    a = ',';
                }
                return a;
            };

            $scope.searchRFPs = function (str) {
                var filterText = str ? str.toUpperCase() : '';
                if (!filterText) {
                    $scope.myAuctionsFiltred = $scope.myAuctions1;
                }
                else {
                    $scope.myAuctionsFiltred = $scope.myAuctions1.filter(function (req) {
                        return req.REQ_TITLE.toUpperCase().includes(filterText) || String(req.requirementId) === filterText || String(req.REQ_NUMBER) === filterText;
                    });
                }
            };



            /********  CONSOLIDATE PR ********/
            $scope.prDet = {
                prLevel: true
            };

            $scope.totalItems1 = 0;
            $scope.currentPage1 = 1;
            $scope.itemsPerPage1 = 10;
            $scope.maxSize1 = 5;

            $scope.setPage1 = function (pageNo) {
                $scope.currentPage1 = pageNo;
                $scope.getPRSBasedOnItem(($scope.currentPage1 - 1), 10);
            };

            $scope.pageChanged1 = function (pageNo) {
                ////console.log('Page changed to: ' + $scope.currentPage);
            };


            $scope.itemsList = [];
            $scope.getPRSBasedOnItem = function (recordsFetchFrom, pageSize) {
                $scope.selectedPRNumbers = '';
                var plant, projectType, sectionHead, wbsCode, profitCentre = '',purchaseCode = '';

                if (_.isEmpty($scope.filters.plant)) {
                    plant = '';
                } else if ($scope.filters.plant && $scope.filters.plant.length > 0) {
                    var plants = _($scope.filters.plant)
                        .filter(item => item.id)
                        .map('id')
                        .value();
                    plant = plants.join(',');
                }

                if (_.isEmpty($scope.filters.projectType)) {
                    projectType = '';
                } else if ($scope.filters.projectType && $scope.filters.projectType.length > 0) {
                    var projectTypes = _($scope.filters.projectType)
                        .filter(item => item.name)
                        .map('name')
                        .value();
                    projectType = projectTypes.join(',');
                }

                if (_.isEmpty($scope.filters.sectionHead)) {
                    sectionHead = '';
                } else if ($scope.filters.sectionHead && $scope.filters.sectionHead.length > 0) {
                    var sectionHeads = _($scope.filters.sectionHead)
                        .filter(item => item.name)
                        .map('name')
                        .value();
                    sectionHead = sectionHeads.join(',');
                }

                if (_.isEmpty($scope.filters.wbsCode)) {
                    wbsCode = '';
                } else if ($scope.filters.wbsCode && $scope.filters.wbsCode.length > 0) {
                    var wbsCodes = _($scope.filters.wbsCode)
                        .filter(item => item.name)
                        .map('name')
                        .value();
                    wbsCode = wbsCodes.join(',');
                }

                if (_.isEmpty($scope.filters.profitCentre)) {
                    profitCentre = '';
                } else if ($scope.filters.profitCentre && $scope.filters.profitCentre.length > 0) {
                    var profitCentres = _($scope.filters.profitCentre)
                        .filter(item => item.name)
                        .map('name')
                        .value();
                    profitCentre = profitCentres.join(',');
                }

                if (_.isEmpty($scope.filters.purcahseGroup)) {
                    purchaseCode = '';
                } else if ($scope.filters.purcahseGroup && $scope.filters.purcahseGroup.length > 0) {
                    var purchaseCodes = _($scope.filters.purcahseGroup)
                        .filter(item => item.id)
                        .map('id')
                        .value();
                    purchaseCode = purchaseCodes.join(',');
                }
                if (_.isEmpty($scope.filters.requisitionerName)) {
                    creatorName = '';
                } else if ($scope.filters.requisitionerName && $scope.filters.requisitionerName.length > 0) {
                    var creators = _($scope.filters.requisitionerName)
                        .filter(item => item.id)
                        .map('id')
                        .value();
                    creatorName = creators.join(',');
                }
                if (_.isEmpty($scope.filters.projectName)) {
                    clientName = '';
                } else if ($scope.filters.projectName && $scope.filters.projectName.length > 0) {
                    var clientNames = _($scope.filters.projectName)
                        .filter(item => item.id)
                        .map('id')
                        .value();
                    clientName = clientNames.join(',');
                }
                
                var params = {
                    "plant": plant,
                    "projectType": projectType,
                    "sectionHead": sectionHead,
                    "wbsCode": wbsCode,
                    "profitCentre": profitCentre,
                    "purchaseCode": purchaseCode,
                    "creatorName": creatorName,
                    "clientName": clientName,
                    "search": $scope.filters.searchKeyword ? $scope.filters.searchKeyword : '',
                    "PageSize": recordsFetchFrom * pageSize,
                    "NumberOfRecords": pageSize
                };
                $scope.pageSizeTemp1 = (params.PageSize + 1);
                $scope.NumberOfRecords = ((recordsFetchFrom+1) * pageSize);
                
                PRMPRServices.GetIndividualItems(params)
                    .then(function (response) {
                        $scope.itemsList = response;
                        $scope.itemsList.forEach(function (item, index) {
                            item.selectForRFQ = false;
                            item.IsDisabled = false;
                            item.SELECTED_QUANTITY = 0;
                            item.RFQ_QUANTITY = 0;
                            item.FILTERED_PR_COUNT = 0;
                        });
                        $scope.totalItems1 = $scope.itemsList[0].TOTAL_PR_ITEM_COUNT;
                        $scope.PRItemStats.totalPRs = $scope.totalItems1;
                        ////addCategoryToFilters(response);
                        //if ($scope.itemsList.length > 0) {
                        //    $scope.totalItems1 = $scope.itemsList.length;
                        //} else {
                        //    $scope.totalItems1 = 0;
                        //}

                    });
            };

            $scope.getPRLevelView = function () {
                $scope.selectedPRNumbers = '';
            };

            $scope.ItemPRS = [];
            $scope.ItemPRSTemp = [];
            $scope.ItemPRSPopUp = [];
            $scope.ItemPRSPopUpTemp = [];

            $scope.GetPRSbyItem = function (prItem,type,productid) {

                var plant, projectType, sectionHead, wbsCode, profitCentre = '';

                if (_.isEmpty($scope.filters.plant)) {
                    plant = '';
                } else if ($scope.filters.plant && $scope.filters.plant.length > 0) {
                    var plants = _($scope.filters.plant)
                        .filter(item => item.id)
                        .map('id')
                        .value();
                    plant = plants.join(',');
                }

                if (_.isEmpty($scope.filters.projectType)) {
                    projectType = '';
                } else if ($scope.filters.projectType && $scope.filters.projectType.length > 0) {
                    var projectTypes = _($scope.filters.projectType)
                        .filter(item => item.name)
                        .map('name')
                        .value();
                    projectType = projectTypes.join(',');
                }

                if (_.isEmpty($scope.filters.sectionHead)) {
                    sectionHead = '';
                } else if ($scope.filters.sectionHead && $scope.filters.sectionHead.length > 0) {
                    var sectionHeads = _($scope.filters.sectionHead)
                        .filter(item => item.name)
                        .map('name')
                        .value();
                    sectionHead = sectionHeads.join(',');
                }

                if (_.isEmpty($scope.filters.wbsCode)) {
                    wbsCode = '';
                } else if ($scope.filters.wbsCode && $scope.filters.wbsCode.length > 0) {
                    var wbsCodes = _($scope.filters.wbsCode)
                        .filter(item => item.name)
                        .map('name')
                        .value();
                    wbsCode = wbsCodes.join(',');
                }

                if (_.isEmpty($scope.filters.profitCentre)) {
                    profitCentre = '';
                } else if ($scope.filters.profitCentre && $scope.filters.profitCentre.length > 0) {
                    var profitCentres = _($scope.filters.profitCentre)
                        .filter(item => item.name)
                        .map('name')
                        .value();
                    profitCentre = profitCentres.join(',');
                }



                var params = {
                    "PRODUCT_ID": prItem.PRODUCT_ID,
                    "plant": plant,
                    "projectType": projectType,
                    "sectionHead": sectionHead,
                    "wbsCode": wbsCode,
                    "profitCentre": profitCentre,
                    "search": $scope.filters.searchKeyword ? $scope.filters.searchKeyword : ''
                };

                if (!prItem.ItemPRS) {
                    PRMPRServices.GetPRSbyItem(params)
                        .then(function (response) {
                            $scope.ItemPRS = response;
                            $scope.ItemPRSPopUp = response;
                            $scope.ItemPRSPopUpTemp = response;
                            $scope.ItemPRSTemp = response;
                            //prItem.FILTERED_PR_COUNT = $scope.ItemPRS.length;
                            var prids = _.uniqBy($scope.ItemPRS, 'PR_NUMBER');
                            prItem.FILTERED_PR_COUNT = prids.length;
                            /**** Disabling For All Items ****/
                            $scope.itemsList.forEach(function (item, index) {
                                item.IsDisabled = false;
                            });
                            /**** Disabling For All Items ****/
                           
                            $scope.ItemPRS.forEach(function (item, index) {
                                //item.CREATED_DATE = moment(item.CREATED_DATE).format("YYYY-MM-DD");
                               
                                var releaseDateTemp = item.RELEASE_DATE ? moment(item.RELEASE_DATE).format("DD-MM-YYYY") : '-';
                                item.RELEASE_DATE = releaseDateTemp != '-' ? releaseDateTemp.contains("1970") ? '-' : releaseDateTemp : '-';

                                //if (item.COMPLETED_ITEMS === item.TOTAL_ITEMS) {
                                //    
                                //    item.NEW_PR_STATUS = "In Progess"
                                //} else if (item.COMPLETED_ITEMS > 0 && item.COMPLETED_ITEMS < item.TOTAL_ITEMS) {
                                //    item.NEW_PR_STATUS = "Partial"
                                //} else {
                                //    item.NEW_PR_STATUS = "New"
                                //}
                                

                                prItem.isExpanded1 = true;
                                
                                item.isChecked = prItem.isExpanded1;
                               

                            });

                            

                            $scope.ItemPRSPopUp = $scope.ItemPRS;
                            $scope.ItemPRSPopUpTemp = $scope.ItemPRS;
                            $scope.ItemPRSTemp = $scope.ItemPRS;

                            //$scope.ItemPRSPopUp.forEach(function (itemPopup, indexPopup) {
                            //    itemPopup.CREATED_DATE = userService.toLocalDate(itemPopup.CREATED_DATE).split(' ')[0];
                            //    itemPopup.RELEASE_DATE = userService.toLocalDate(itemPopup.RELEASE_DATE).split(' ')[0];
                            //    prItem.isExpanded1 = true;
                            //    itemPopup.isChecked = prItem.isExpanded1;
                            //});

                            //$scope.ItemPRSPopUpTemp.forEach(function (itemPopupTemp, indexPopup) {
                            //    itemPopupTemp.CREATED_DATE = userService.toLocalDate(itemPopupTemp.CREATED_DATE).split(' ')[0];
                            //    itemPopupTemp.RELEASE_DATE = userService.toLocalDate(itemPopupTemp.RELEASE_DATE).split(' ')[0];
                            //    prItem.isExpanded1 = true;
                            //    itemPopupTemp.isChecked = prItem.isExpanded1;
                            //});

                            $scope.totalItems2 = $scope.ItemPRSPopUp.length;


                            calculateQuantity($scope.ItemPRS, prItem);
                            $scope.ItemPRSTemp.forEach(function (item, index) {
                                //item.CREATED_DATE = userService.toLocalDate(item.CREATED_DATE);
                                prItem.isExpanded1 = true;
                                item.isChecked = prItem.isExpanded1;
                            });
                            prItem.ItemPRS = $scope.ItemPRS;

                            prItem.ItemPRSTemp = prItem.ItemPRS;

                            /********** RFQ Posting With Selected Items ***********/
                            if (prItem.selectForRFQ) {
                                $scope.AddItemToRequirement(prItem, prItem.selectForRFQ);
                            }
                            /********** RFQ Posting With Selected Items ***********/
                            
                            if (type && type === 'ITEM_DISPLAY')
                            {
                                $scope.displayItems(productid);
                            }


                        });
                } else {
                    /**** Disabling For All Items ****/
                    //calculateQuantity($scope.ItemPRS, prItem);
                    $scope.itemsList.forEach(function (item, index) {
                        item.IsDisabled = false;
                    });
                    /**** Disabling For All Items ****/


                    /********** RFQ Posting With Selected Items ***********/
                    if (prItem.selectForRFQ) {
                        $scope.AddItemToRequirement(prItem, prItem.selectForRFQ);
                    }
                    /********** RFQ Posting With Selected Items ***********/

                    if (type && type === 'ITEM_DISPLAY') {
                        $scope.displayItems(productid);
                    }
                }

            };

            $scope.searchPR = function (searchText, prItem) {
                var filterText = angular.lowercase(searchText);
                if (!filterText || filterText == '' || filterText == undefined || filterText == null) {
                    prItem.ItemPRS = prItem.ItemPRSTemp;
                    $scope.consolidate(prItem, filterText);
                }
                else {
                    prItem.ItemPRS = prItem.ItemPRSTemp.filter(function (pr) {
                        return (String(angular.lowercase(pr.PR_NUMBER)).includes(filterText) == true);
                    });
                }

                $scope.totalItems1 = prItem.ItemPRS.length;

            };


            $scope.unSelectAll = function (prItem, searchedPR) {

                if (!prItem.isExpanded1) {
                    prItem.ItemPRS.forEach(function (item, index) {
                        if (item.REQ_ID <= 0) {
                            item.isChecked = false;
                        }
                    });
                    $scope.consolidate(prItem, searchedPR, prItem.isExpanded1, 'OVERALL');
                } else {
                    prItem.ItemPRS.forEach(function (item, index) {
                        item.isChecked = true;
                    });
                    $scope.consolidate(prItem, searchedPR, prItem.isExpanded1, 'OVERALL');
                }

            };

            $scope.consolidate = function (pr, searchedPR, isChecked, type) {
                if (searchedPR == null || searchedPR == "undefined" || searchedPR == "") {
                    if (pr && pr.ItemPRS.length > 0) {
                        var consolidateQuantity = pr.ItemPRS.filter(function (items) { return items.isChecked && items.REQ_ID <= 0; });
                        var getRFQQuantity = pr.ItemPRS.filter(function (items) { return items.isChecked && items.REQ_ID > 0; });
                        if (consolidateQuantity && consolidateQuantity.length > 0) {
                            pr.SELECTED_QUANTITY = _.sumBy(consolidateQuantity, 'TOTAL_PR_ITEM_QUANTITY');
                            pr.RFQ_QUANTITY = _.sumBy(getRFQQuantity, 'TOTAL_PR_ITEM_QUANTITY');
                        } else {
                            pr.SELECTED_QUANTITY = 0;
                        }
                    }
                }
                else {
                    searchedPR = angular.lowercase(searchedPR);
                    var searchedFilteredPRS = [];
                    if (pr.isExpanded1) {
                        if (type === 'OVERALL') {
                            searchedFilteredPRS = $scope.ItemPRSTemp;
                            if (searchedFilteredPRS && searchedFilteredPRS.length > 0) {
                                calculateQuantity(searchedFilteredPRS, pr);
                            }
                        } else {
                            if (!isChecked) {
                                var unCheckedPRID = _.result(_.find(pr.ItemPRS, function (items) {
                                    return !items.isChecked;
                                }), 'PR_ID'); // get Unchecked PR IR's

                                searchedFilteredPRS = $scope.ItemPRSTemp.filter(function (item) { return item.PR_ID != unCheckedPRID; }); // remove the unchecked PR and calculate

                                if (searchedFilteredPRS && searchedFilteredPRS.length > 0) {
                                    calculateQuantity(searchedFilteredPRS, pr);
                                }

                            } else {
                                searchedFilteredPRS = $scope.ItemPRSTemp; // Assign All The PR's from DB
                                if (searchedFilteredPRS && searchedFilteredPRS.length > 0) {
                                    calculateQuantity(searchedFilteredPRS, pr);
                                }
                            }
                        }
                    } else {
                        if (type === 'OVERALL') {

                            var prOverallIds = _(pr.ItemPRS)
                                .filter(item => !item.isChecked)
                                .map('PR_ID')
                                .value();

                            searchedFilteredPRS = $scope.ItemPRSTemp.filter(function (item) { return !prOverallIds.includes(item.PR_ID); }); // remove the unchecked PR and calculate

                            if (searchedFilteredPRS && searchedFilteredPRS.length > 0) {
                                calculateQuantity(searchedFilteredPRS, pr);
                            }

                        } else {
                            if (!isChecked) {
                                var itemUnCheckedPRID = _.result(_.find(pr.ItemPRS, function (items) {
                                    return !items.isChecked;
                                }), 'PR_ID'); // get Unchecked PR IR's

                                searchedFilteredPRS = $scope.ItemPRSTemp.filter(function (item) { return item.PR_ID != itemUnCheckedPRID; }); // remove the unchecked PR and calculate

                                if (searchedFilteredPRS && searchedFilteredPRS.length > 0) {
                                    calculateQuantity(searchedFilteredPRS, pr);
                                }

                            } else {
                                searchedFilteredPRS = $scope.ItemPRSTemp; // Assign All The PR's from DB
                                if (searchedFilteredPRS && searchedFilteredPRS.length > 0) {
                                    calculateQuantity(searchedFilteredPRS, pr);
                                }
                            }
                        }
                    }

                }
            };


            function calculateQuantity(searchedFilteredPRS, pr) {
                var searchConsolidatedQuantity = searchedFilteredPRS.filter(function (items) { return items.isChecked && items.REQ_ID <= 0; }); // calculate all the checked PR's Quantity
                var getRFQQuantity = searchedFilteredPRS.filter(function (items) { return items.isChecked && items.REQ_ID > 0; });
                if (searchConsolidatedQuantity && searchConsolidatedQuantity.length > 0) {
                    pr.SELECTED_QUANTITY = _.sumBy(searchConsolidatedQuantity, 'TOTAL_PR_ITEM_QUANTITY');
                    pr.RFQ_QUANTITY = _.sumBy(getRFQQuantity, 'TOTAL_PR_ITEM_QUANTITY');
                } else if (getRFQQuantity && getRFQQuantity.length > 0) {
                    pr.RFQ_QUANTITY = _.sumBy(getRFQQuantity, 'TOTAL_PR_ITEM_QUANTITY');
                } else {
                    pr.SELECTED_QUANTITY = 0;
                }
            }

            function addCategoryToFilters(catArray) {
                var categoryListresp = catArray;
                categoryListresp = _.uniqBy(categoryListresp, 'CATEGORY_ID');
                categoryListresp.forEach(function (item, index) {
                    var catObj = {
                        FIELD_NAME: '',
                        FIELD_VALUE: ''
                    };
                    catObj.FIELD_NAME = item.CategoryName;
                    catObj.FIELD_VALUE = item.CATEGORY_ID;
                    $scope.categoryList.push(catObj);
                });

                $scope.categoryList = _.uniqBy($scope.categoryList, 'FIELD_VALUE');

            }


            $scope.PostRequirement = function (prItem) {
                $scope.itemsList.forEach(function (item, index) {
                    item.IsDisabled = true;
                });
                if (prItem.selectForRFQ) {
                    $scope.GetPRSbyItem(prItem);
                } else {
                    $scope.itemsList.forEach(function (item, index) {
                        item.IsDisabled = false;
                    });

                    $scope.AddItemToRequirement(prItem, prItem.selectForRFQ);
                }
            };

            $scope.selectedItemsForRFQ = [];
            $scope.AddItemToRequirement = function (prItem, type) {

                if (type) {
                    var checkedPRIDs = _(prItem.ItemPRS)
                        .filter(item => item.isChecked)
                        .map('PR_ID')
                        .value();

                    prItem.PR_ID = checkedPRIDs.join(',');
                    $scope.selectedItemsForRFQ.push(prItem);
                } else {
                    if ($scope.selectedItemsForRFQ && $scope.selectedItemsForRFQ.length > 0) {
                        var itemIndex = _.findIndex($scope.selectedItemsForRFQ, function (item) {
                            return item.PRODUCT_ID === prItem.PRODUCT_ID;
                        });

                        if (itemIndex >= 0) {
                            $scope.selectedItemsForRFQ.splice(itemIndex, 1);
                        }
                    }
                }

                $scope.selectedItemsForRFQ = _.uniqBy($scope.selectedItemsForRFQ, 'PRODUCT_ID');

            };

            $scope.createRFQWithConsolidatedItems = function () {
                let prmFieldMappingDetails = {};
                let isServiceRelatedITems = false;
                let isEmptyItemCode = false;
                var template = $scope.selectedTemplate && $scope.selectedTemplate.TEMPLATE_NAME ? $scope.selectedTemplate.TEMPLATE_NAME : 'PRM-DEFAULT';
                var params = {
                    "templateid": 0,
                    "templatename": template,
                    "sessionid": userService.getUserToken()
                };

                var rfqPRSList = $scope.selectedItemsForRFQ.filter(function (item) {
                    return item.selectForRFQ && item.RFQ_QUANTITY != item.OVERALL_ITEM_QUANTITY;
                });                

                PRMCustomFieldService.GetTemplateFields(params).then(function (mappingDetails) {
                    mappingDetails.forEach(function (item, index) {
                        prmFieldMappingDetails[item.FIELD_NAME] = item;
                    });

                    if (prmFieldMappingDetails) {
                        prmFieldMappingDetails.IS_SEARCH_FOR_CAS = prmFieldMappingDetails.PRODUCT_CODE.FIELD_LABEL.toLowerCase().contains("cas number");
                        prmFieldMappingDetails.IS_SEARCH_FOR_MFCD = prmFieldMappingDetails.PRODUCT_NUMBER.FIELD_LABEL.toLowerCase().contains("mfcd code");
                        if (!isServiceRelatedITems && prmFieldMappingDetails.IS_SEARCH_FOR_CAS) {
                            isServiceRelatedITems = true;
                        }
                    }

                    rfqPRSList.forEach(function (item, index) {
                        if (!isEmptyItemCode && !(item.ITEM_CODE_CAS || item.CASNR) && !item.REQ_ID) {
                            isEmptyItemCode = true;
                        }
                    });

                    if (isServiceRelatedITems && isEmptyItemCode && false) {
                        swal("Error!", 'Selected template is not Valid as empty CAS number');
                    }
                    else if (rfqPRSList && rfqPRSList.length > 0) {
                        if ($scope.filters.selectedRequirement && $scope.filters.selectedRequirement.requirementId) {
                            $state.go('save-requirementAdv', { 'Id': $scope.filters.selectedRequirement.requirementId, 'prItemsList': rfqPRSList, 'selectedPRNumbers': $scope.prDet.prLevel ? $scope.selectedPRNumbers : '' });
                        } else {
                            $state.go('save-requirementAdv', { 'prItemsList': rfqPRSList, 'selectedTemplate': $scope.selectedTemplate, 'selectedPRNumbers': $scope.prDet.prLevel ? $scope.selectedPRNumbers : '' });
                        }
                    }
                });
            };

        /********  CONSOLIDATE PR ********/

            $scope.navigateToRequirement = function () {
                angular.element('#templateSelection').modal('hide');
                if ($scope.prDet.prLevel) {
                    $scope.GetPRMTemplateFields();
                } else {
                    $scope.createRFQWithConsolidatedItems();
                }
            };

            $scope.GetPRMTemplates = function () {
                PRMCustomFieldService.GetTemplates().then(function (response) {
                    $scope.prmTemplates = response;
                    if ($scope.prmTemplates && $scope.prmTemplates.length > 0) {
                        $scope.selectedTemplate = $scope.prmTemplates[0];
                    }
                });
            };

            $scope.GetPRMTemplates();


            $scope.GetPRMTemplateFields = function () {
                let prmFieldMappingDetails = {};
                let isServiceRelatedITems = false;
                let isEmptyItemCode = false;
                var template = $scope.selectedTemplate && $scope.selectedTemplate.TEMPLATE_NAME ? $scope.selectedTemplate.TEMPLATE_NAME : 'PRM-DEFAULT';
                var params = {
                    "templateid": 0,
                    "templatename": template,
                    "sessionid": userService.getUserToken()
                };

                PRMCustomFieldService.GetTemplateFields(params).then(function (mappingDetails) {
                    mappingDetails.forEach(function (item, index) {
                        prmFieldMappingDetails[item.FIELD_NAME] = item;
                    });

                    if (prmFieldMappingDetails) {
                        prmFieldMappingDetails.IS_SEARCH_FOR_CAS = prmFieldMappingDetails.PRODUCT_CODE.FIELD_LABEL.toLowerCase().contains("cas number");
                        prmFieldMappingDetails.IS_SEARCH_FOR_MFCD = prmFieldMappingDetails.PRODUCT_NUMBER.FIELD_LABEL.toLowerCase().contains("mfcd code");
                        if (!isServiceRelatedITems && prmFieldMappingDetails.IS_SEARCH_FOR_CAS) {
                            isServiceRelatedITems = true;
                        }
                    }

                    let validSelectedPRs = _.filter($scope.selectedPRItems, function (prItemObj) {
                        return prItemObj.isSelected;
                    });

                    validSelectedPRs.forEach(function (prObj, index) {
                        prObj.PRItems.forEach(function (item, index1) {
                            if (prmFieldMappingDetails.IS_SEARCH_FOR_CAS && (item.ITEM_CODE_CAS || item.CASNR)) {
                                item.ITEM_CODE = item.ITEM_CODE_CAS || item.CASNR;
                            }

                            if (prmFieldMappingDetails.IS_SEARCH_FOR_MFCD && (item.ITEM_CODE_MFCD || item.MFCD_NUMBER)) {
                                item.ITEM_NUM = item.ITEM_CODE_MFCD || item.MFCD_NUMBER;
                            }

                            if (!isEmptyItemCode && !(item.ITEM_CODE_CAS || item.CASNR) && !item.REQ_ID && item.isCheckedPrItem) {
                                isEmptyItemCode = true;
                            }
                        });
                    });

                    if (isServiceRelatedITems && isEmptyItemCode && false) {
                        swal("Error!", 'Selected template is not Valid as empty CAS number');
                    }
                    else if (validSelectedPRs && validSelectedPRs.length > 0) {
                        if ($scope.filters.selectedRequirement && $scope.filters.selectedRequirement.requirementId) {
                            $state.go('save-requirementAdv', { 'Id': $scope.filters.selectedRequirement.requirementId, 'prDetailsList': validSelectedPRs, 'selectedPRNumbers': $scope.prDet.prLevel ? $scope.selectedPRNumbers : '' });
                        } else {
                            $state.go('save-requirementAdv', { 'prDetailsList': validSelectedPRs, 'selectedTemplate': $scope.selectedTemplate, 'selectedPRNumbers': $scope.prDet.prLevel ? $scope.selectedPRNumbers : '' });
                        }
                    }                    
                });
            };


            $scope.convertDate = function (date) {
                if (date) {
                    var convertedDateTemp = moment(date).format("DD-MM-YYYY");
                    return convertedDateTemp.contains("1970") ? '-' : convertedDateTemp;
                } else {
                    return '-';
                }
            };


            $scope.ItemsListPopup = [];

            $scope.showPRItemlevelDetails = function (prItem) {
                $scope.GetPRSbyItem(prItem, 'ITEM_DISPLAY', prItem.PRODUCT_ID);
            };

            $scope.displayItems = function (productId) {

                var PR_IDS = '';

                var pridspop = _($scope.ItemPRSPopUp)
                    .filter(item => item.PR_ID)
                    .map('PR_ID')
                    .value();
                PR_IDS = pridspop.join(',');

                var params = {
                    "prIds": PR_IDS
                };

                PRMPRServices.GetItemDetails(params)
                    .then(function (response) {
                        $scope.ItemsListPopup = response;

                        $scope.ItemsListPopup = $scope.ItemsListPopup.filter(function (item) {
                            return item.PRODUCT_ID === productId;
                        });

                    });
            };
            
            $scope.filterPRsPopup = function (searchText) {

                var filterText = angular.lowercase(searchText);

                if (filterText) {
                    $scope.ItemPRSPopUp = $scope.ItemPRSPopUpTemp.filter(function (pr) {
                        return (String(angular.lowercase(pr.PR_NUMBER)).includes(filterText) == true);
                    });
                } else {
                    $scope.ItemPRSPopUp = $scope.ItemPRSPopUpTemp;
                }

                $scope.totalItems2 = $scope.ItemPRSPopUp.length;
            };

            $scope.selectMultiplePRS = [];

            $scope.createRFQWithMultiplePR = function (prDet)
            {
                if (prDet.selectPRSForRFQ) {
                    var getCheckedPRIDs = _($scope.filteredPRList)
                        .filter(pr => pr.selectPRSForRFQ)
                        .map('PR_ID')
                        .value(); // GET checked PR_ID's

                    if (getCheckedPRIDs && getCheckedPRIDs.length > 0) {

                        var checkedPRObjects = $scope.filteredPRList.filter(function (prItem, prIndex) { return getCheckedPRIDs[0] === prItem.PR_ID; });

                        if (checkedPRObjects && checkedPRObjects.length > 0) {
                            $scope.createRequirement(prDet, true);
                        }
                    }
                } else {
                    
                    var getUnCheckedPRIDs = _($scope.selectedPRItems)
                        .filter(pr => !pr.selectPRSForRFQ)
                        .map('PR_ID')
                        .value(); // GET un checked PR_ID's 

                    prDet.PRItems.forEach(function (prItem, prIndex) {
                        prItem.isCheckedPrItem = false;
                    });
                    
                    var prDetIndex = _.findIndex($scope.selectedPRItems, function (item) {
                        return item.PR_ID === getUnCheckedPRIDs[0];
                    }); // filter the PR in the RFQ posting List and get the Index
                    
                    if (prDetIndex >= 0) { // if PR Found
                        $scope.selectedPRItems.splice(prDetIndex, 1); //remove the PR  and PR Items
                    }
                }
            };

            $scope.downloadExcel = false;
            $scope.GetReport = function () {
                $scope.PRList = [];
                $scope.downloadExcel = true;
                $scope.getprlist(0, 0, $scope.searchString);
            };

            $scope.searchRequirement = function () {
                $scope.filteredRequirements = [];
                auctionsService.SearchRequirements({ "search": $scope.filters.searchRequirement, "compid": $scope.compId, "sessionid": userService.getUserToken() })
                    .then(function (response) {
                        if (response) {
                            $scope.filteredRequirements = response;
                        }
                    });
            };

            $scope.selectRequirement = function () {
                //console.log($scope.filters.selectedRequirement);
            };

            function downloadPRExcel() {
                alasql('SELECT PR_NUMBER as [PR Number],PLANT as [Plant],PLANT_NAME as [Plant Name], ' +
                    'PR_CREATOR_NAME as [PR Creator Name], NEW_PR_STATUS as [Status],RELEASE_DATE as [Release Date], ' +
                    'GMP as [GMP],PURCHASE_GROUP_CODE as [Purchase Group Code], ' +
                    'PURCHASE_GROUP_NAME as [Purchase Group Name]' +

                    'INTO XLSX(?, { headers: true, sheetid: "PR_DETAILS", style: "font-size:15px;background:#233646;color:#FFF;" }) FROM ? ',
                    ["PR Details.xlsx", $scope.prExcelReport]);
                $scope.downloadExcel = false;
            }
            
        }]);