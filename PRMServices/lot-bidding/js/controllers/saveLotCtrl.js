prmApp
    // =========================================================================
    // COMMON FORMS
    // =========================================================================

    .controller('saveLotCtrl', ["$state", "$stateParams", "$scope", "auctionsService", "userService", "$http", "$window", "domain", "fileReader", "growlService", "$log", "$filter", "ngDialog", "techevalService", "catalogService", "PRMLotReqService", "SignalRFactory", "signalRHubName",
        function ($state, $stateParams, $scope, auctionsService, userService, $http, $window, domain, fileReader, growlService, $log, $filter, ngDialog, techevalService, catalogService, PRMLotReqService, SignalRFactory, signalRHubName) {
            $scope.stateParamsLotId = $stateParams.Id;
            $scope.compid = userService.getUserCompanyId();
            $scope.parentRequirementDetails = {};
            $scope.lotRequirementItems = [];


            $scope.lotrequirements = [];
            $scope.lotDetails = {
                LotId: 0,
                CompId: userService.getUserCompanyId(),
                LotTitle: '',
                LotDesc: '',
                StartTime: '',
                EndTime: '',
                Duration: 0,
                ProjectId: 0,
                TotalLots: 0,
                Status: ''
            };
            $scope.minDateMoment = moment();
            $scope.previousDate = '';

            auctionsService.getrequirementdata({ "reqid": $stateParams.reqId, "sessionid": userService.getUserToken(), 'userid': userService.getUserId() })
            .then(function (response) {
                if (response) {
                    $scope.parentRequirementDetails = response;
                }
            });

            $scope.setEndTime = function () {
                $scope.NegotiationSettings = $scope.parentRequirementDetails.NegotiationSettings;
                var duration = $scope.NegotiationSettings.negotiationDuration.split(" ", 4);
                $scope.days = parseInt(duration[0]);
                duration = duration[1];
                duration = duration.split(":", 4);
                $scope.hours = parseInt(duration[0]);
                $scope.mins = parseInt(duration[1]);

                $scope.NegotiationSettings.negotiationDuration = $scope.days + ' ' + $scope.hours + ':' + $scope.mins + ':00.0';

                $scope.NegotiationSettings.negotiationDuration = $scope.days + ' ' + $scope.hours + ':' + $scope.mins + ':00.0';

                if ($scope.lotDetails.StartTime != '' || $scope.lotDetails.StartTime != 'undefined') {
                    $scope.lotDetails.EndTime = moment($scope.lotDetails.StartTime, "DD-MM-YYYY HH:mm").add(moment.duration($scope.mins, 'minutes'));
                    $scope.lotDetails.EndTime = moment($scope.lotDetails.EndTime, "DD-MM-YYYY HH:mm").add(moment.duration($scope.hours, 'hours'));
                    $scope.lotDetails.EndTime = moment($scope.lotDetails.EndTime, "DD-MM-YYYY HH:mm").add(moment.duration($scope.days, 'days'));
                }

            }

            $scope.getLotRequirementItems = function () {
                PRMLotReqService.lotRequirementItems($stateParams.Id)
                    .then(function (response) {
                        if (response) {
                            $scope.lotRequirementItems = response;
                            if (response.length > 0) {
                                $scope.lotDetails.StartTime = userService.toLocalDate(response[response.length - 1].endTime);
                                if ($scope.lotDetails.StartTime != '') {
                                    $scope.minDateMoment = moment($scope.lotDetails.StartTime, "DD-MM-YYYY HH:mm");
                                }
                                $scope.setEndTime();

                                $scope.validations.isDisabled = true;
                            } else {
                                $scope.lotDetails.StartTime = $scope.previousDate;
                                if ($scope.lotDetails.StartTime != '') {
                                    $scope.minDateMoment = moment($scope.lotDetails.StartTime, "DD-MM-YYYY HH:mm");
                                }
                                $scope.setEndTime();
                            }
                        }
                    });
            };

            $scope.getLotRequirementItems();

            $scope.filteredWorders = [];

            $scope.isAlreadyInLot = function (requirementItem) {
                $scope.filteredWorders = [];
                $scope.lotRequirementItems.forEach(function ( obj, index) {
                    $scope.filteredWorders.push(obj.title);
                })
                var isValid = true;
                if (requirementItem && $scope.filteredWorders && $scope.filteredWorders.length > 0) {
                    if ($scope.filteredWorders.includes(requirementItem.productNo)) {
                        isValid = false;
                    }
                }

                return isValid;
            };


            $scope.allValidations = false;
            $scope.validations = {
                lotTitleValidation: '',
                currentTimeValidation: '',
                endTimeValidation: '',
                selectedItem: '',
                isDisabled : false

            }


            $scope.createLotRequirement = function () {

                $scope.allValidations = false;
                $scope.validations.selectedItem = $scope.validations.lotTitleValidation = $scope.validations.currentTimeValidation = $scope.validations.EndTimeValidation = '';

                console.log($scope.parentRequirementDetails.listRequirementItems);
                var selectedItems = _.filter($scope.parentRequirementDetails.listRequirementItems, function (o) {
                    if (o && o.lotSelect) return o;
                });

                var selectedItemIds = selectedItems.map(function (elem) {
                    return elem && elem.itemID;
                }).join(",");

                $scope.parentRequirementDetails.listRequirementItems.forEach(function (item, itemIndexs) {
                    if (item) {
                        item.lotSelect = false;
                    }
                });

                if ($scope.lotDetails.LotTitle == '' || $scope.lotDetails.LotTitle == 'undefined') {
                    $scope.validations.lotTitleValidation = 'Please Enter Lot Title';
                    $scope.allValidations = true;
                    return false;
                }

                if ($scope.lotDetails.StartTime == '') {
                    $scope.validations.currentTimeValidation = 'Please Enter Start Time';
                    $scope.allValidations = true;
                    return false;
                }

                if ($scope.lotDetails.EndTime == '') {
                    $scope.validations.EndTimeValidation = 'Please Enter End Time';
                    $scope.allValidations = true;
                    return false;
                }

                var ts = moment($scope.lotDetails.StartTime, "DD-MM-YYYY HH:mm").valueOf();
                var m = moment(ts);
                var quotationFreezTime = new Date(m);

                var ts1 = moment($scope.lotDetails.EndTime, "DD-MM-YYYY HH:mm").valueOf();
                var m1 = moment(ts1);
                var expStartTime = new Date(m1);

                auctionsService.getdate()
                    .then(function (GetDateResponse) {

                        var CurrentDateToLocal = userService.toLocalDate(GetDateResponse);

                        var ts = moment(CurrentDateToLocal, "DD-MM-YYYY HH:mm").valueOf();
                        var m = moment(ts);
                        var deliveryDate = new Date(m);
                        var milliseconds = parseInt(deliveryDate.getTime() / 1000.0);
                        var CurrentDateToTicks = "/Date(" + milliseconds + "000+0530)/";

                        var CurrentDate = moment(new Date(parseInt(CurrentDateToTicks.substr(6))));

                        if (quotationFreezTime < CurrentDate) {
                            $scope.validations.currentTimeValidation = 'Start Time should be greater than Current Time';
                            $scope.allValidations = true;
                            return false;
                        }
                        else if (quotationFreezTime >= expStartTime) {
                            $scope.validations.EndTimeValidation = 'End Time should be greater than Start Time';
                            $scope.allValidations = true;
                            return false;
                        }

                        if (selectedItemIds == '') {
                            $scope.validations.selectedItem = 'Please Select Atleast One Item';
                            $scope.allValidations = true;
                            return false;
                        }

                        if ($scope.allValidations) {
                            return false;
                        }



                        var params = {};
                        params.itemids = selectedItemIds;
                        params.lotid = $stateParams.Id;
                        params.title = $scope.lotDetails.LotTitle;
                        params.desc = $scope.lotDetails.LotDesc;
                        params.start = "/Date(" + userService.toUTCTicks($scope.lotDetails.StartTime) + "+0530)/";
                        params.end = "/Date(" + userService.toUTCTicks($scope.lotDetails.EndTime) + "+0530)/";
                        params.sessionid = userService.getUserToken();
                        params.user = userService.getUserId();

                        PRMLotReqService.SaveLotRequirement(params)
                            .then(function (response) {
                                if (response.errorMessage !== '') {
                                    growlService.growl(response.errorMessage, "inverse");
                                }
                                else {
                                    $scope.previousDate = $scope.lotDetails.EndTime;
                                    $scope.validations.isDisabled = true;
                                    growlService.growl("Details saved successfully.", "success");
                                }

                                $scope.lotDetails = {
                                    LotTitle: '',
                                    LotDesc: '',
                                };
                                $scope.getLotRequirementItems();
                                $log.info(response);
                            });
                    });
            };

        }]);