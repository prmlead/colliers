﻿prmApp
    .controller('moduleErrorListCtrl', ["$rootScope", "$scope", "$stateParams", "$log", "$state", "$window", "userService", "auctionsService", "storeService", "growlService", "PRMPOService", "poService",
        "PRMCustomFieldService", "fileReader", "$uibModal", "$filter", "$http", "poDomain","PRMUploadServices","$element",
        function ($rootScope, $scope, $stateParams, $log, $state, $window, userService, auctionsService, storeService, growlService, PRMPOService, poService, PRMCustomFieldService, fileReader, $uibModal, $filter, $http, poDomain, PRMUploadServices, $element) {

            /*PAGINATION CODE*/
            $scope.totalItems = 0;
            $scope.currentPage = 1;
            $scope.itemsPerPage = 10;
            $scope.maxSize = 5;
            $scope.moduleName = $stateParams.moduleName;

            $scope.getModuleBasedErrorDetails = function () {
                $scope.modulErrorList = [];
                PRMUploadServices.getModuleBasedErrorDetails({ "MODULE": "PO_DETAILS" }).then(function (response) {
                    $scope.modulErrorList = response;
                    $scope.totalItems = $scope.modulErrorList.length;
                });
            };

            $scope.getModuleBasedErrorDetails();


            $scope.getDate = function (value) {
                return userService.toLocalDate(value);
            }

        }]);