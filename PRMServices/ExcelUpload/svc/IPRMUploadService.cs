﻿using PRMServices.Models;
using System.Collections.Generic;
using System.ServiceModel;
using System.ServiceModel.Web;

namespace PRMServices
{
    [ServiceContract]
    public interface IPRMUploadService
    {
        [OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getExcelUploadTemplates?templateName={templateName}&compid={compID}&uid={uid}&sessionid={sessionID}")]
        string GetExcelUploadTemplates(string templateName, int compID, int uid, string sessionID);

        [OperationContract]
        [WebInvoke(Method = "POST",
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        UriTemplate = "uploadTemplate")]
        List<ExcelErrorUpload> UploadTemplate(string name, int compID, string sessionID, string tableName, byte[] attachment, int U_ID, int proceedFurther, string fileName);

        [OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getModuleBasedErrorDetails?MODULE={MODULE}&compid={compID}&sessionid={sessionID}")]
        List<ModuleBasedError> GetModuleBasedErrorDetails(string MODULE, int compID, string sessionID);

    }
}
