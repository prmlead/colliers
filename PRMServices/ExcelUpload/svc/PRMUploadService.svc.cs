﻿using System;
using System.IO;
using System.Data;
using System.Linq;
using System.Collections.Generic;
using System.ServiceModel.Activation;
using PRMServices.Models;
using PRMServices.SQLHelper;
using CORE = PRM.Core.Common;
using OfficeOpenXml;
using OfficeOpenXml.DataValidation;
using System.Data.SqlClient;
using OfficeOpenXml.Style;
using System.Text.RegularExpressions;
using Newtonsoft.Json;

namespace PRMServices
{
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class PRMUploadService : IPRMUploadService
    {
        private static NLog.Logger logger = NLog.LogManager.GetCurrentClassLogger();
        private IDatabaseHelper sqlHelper = DatabaseProvider.GetDatabaseProvider();
        public PRMServices prm = new PRMServices();
        public string GetExcelUploadTemplates(string templateName, int compID, int uid, string sessionID)
        {
            Utilities.ValidateSession(sessionID);
            int maxRows = 1048576;
            List<ExcelUpload> details = new List<ExcelUpload>();
            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            sd.Add("P_COMP_ID", compID);
            sd.Add("P_TEMPLATE_NAME", templateName);
            CORE.DataNamesMapper<ExcelUpload> mapper = new CORE.DataNamesMapper<ExcelUpload>();
            DataSet ds = sqlHelper.SelectList("rp_GetExcelUploadTemplates", sd);
            details = mapper.Map(ds.Tables[0]).ToList();
            MemoryStream ms = new MemoryStream();
            byte[] contents = null;
            ExcelPackage ExcelPkg = new ExcelPackage();
            ExcelWorksheet xlWorkSheet = ExcelPkg.Workbook.Worksheets.Add(templateName);
            ExcelWorksheet xlWorkSheet1 = ExcelPkg.Workbook.Worksheets.Add("Sheet1");
            var i = 1;
            //int nextColumn = 1;
            for (int j = 0; j <= details.Count - 1; j++)
            {
                xlWorkSheet.Cells[i, j + 1].Value = details[j].UI_COLUMN_NAME.ToString();
                if (details[j].IS_MANDATE > 0) {
                    xlWorkSheet.Cells[i, j + 1].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    xlWorkSheet.Cells[i, j + 1].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.OrangeRed);
                }
                string alphabet = ExcelCellAddress.GetColumnLetter(xlWorkSheet.Dimension.End.Column);

                if (details[j].IS_DROP_DOWN_REQUIRED > 0)
                {
                    var ListValidation = xlWorkSheet.DataValidations.AddListValidation($"{alphabet}2:{alphabet}{maxRows}");
                    ListValidation.ShowErrorMessage = true;
                    ListValidation.ErrorStyle = ExcelDataValidationWarningStyle.stop;
                    ListValidation.ErrorTitle = "The value you entered is not valid";
                    ListValidation.Error = $"Please select the value from the list.";
                    ListValidation.AllowBlank = details[j].IS_MANDATE == 1 ? false : true;
                    var dropDownValues = details[j].DROP_DOWN_VALUES.Split(',');
                    foreach (var dropDownValue in dropDownValues)
                    {
                        ListValidation.Formula.Values.Add(dropDownValue);
                    }
                }
            }
            ExcelPkg.SaveAs(ms);
            if (ms != null)
            {
                contents = ms.ToArray();
            }

            return Convert.ToBase64String(ms.ToArray());
        }

        private static char getNextChar(char c)
        {
            // convert char to ascii
            int ascii = (int)c;
            // get the next ascii
            int nextAscii = ascii + 1;
            // convert ascii to char
            char nextChar = (char)nextAscii;
            return nextChar;
        }

        public List<ExcelErrorUpload> UploadTemplate(string name, int compID, string sessionID, string tableName, byte[] attachment, int U_ID, int proceedFurther, string fileName)
        {
            //Response response = new Response();
            Utilities.ValidateSession(sessionID);
            List<ExcelUpload> details = new List<ExcelUpload>();
            List<ExcelErrorUpload> errdetails = new List<ExcelErrorUpload>();
            Dictionary<string, string> dropdownValues = new Dictionary<string, string>();
            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            sd.Add("P_COMP_ID", compID);
            sd.Add("P_TEMPLATE_NAME", name);
            CORE.DataNamesMapper<ExcelUpload> mapper = new CORE.DataNamesMapper<ExcelUpload>();
            DataSet ds = sqlHelper.SelectList("rp_GetExcelUploadTemplates", sd);
            details = mapper.Map(ds.Tables[0]).ToList();
            string sheetName = string.Empty;
            try
            {
                if (attachment != null)
                {
                    DataTable currentData = new DataTable();
                    using (MemoryStream ms = new MemoryStream())
                    {
                        ms.Write(attachment, 0, attachment.Length);
                        using (ExcelPackage package = new ExcelPackage(ms))
                        {
                            currentData = Utilities.WorksheetToDataTable(package.Workbook.Worksheets[1]);
                            sheetName = package.Workbook.Worksheets[1].Name;
                        }
                    }

                    if (currentData.Rows.Count <= 0)
                    {
                        ExcelErrorUpload errdetails1 = new ExcelErrorUpload();
                        errdetails1.SUCCESS_COUNT = 0;
                        errdetails1.FAILED_COUNT = 1;
                        errdetails1.REASON = "No Records";
                        errdetails1.IS_SUCCESS = false;
                        errdetails.Add(errdetails1);
                        return errdetails;
                    }

                    DataTable tempDT = new DataTable();
                    DataTable tempDTErr = new DataTable();
                    //currentData = currentData.Rows.Cast<DataRow>().Where(row => !row.ItemArray.All(f => string.IsNullOrEmpty((string)f))).CopyToDataTable(); // this is to remove if datarows are empty.
                    List<string> columnNames = getDBColumnNameForMapping(details, ds.Tables[0], currentData).Split(',').ToList(); // This is for Bulk Mapping
                    List<string> errcolumnNames = $"REASON,TEMPLATE_NAME,COMP_ID,U_ID,DATE_CREATED".Split(',').ToList(); // This is for Bulk Mapping
                    List<SqlBulkCopyColumnMapping> columnMappings = new List<SqlBulkCopyColumnMapping>();
                    List<SqlBulkCopyColumnMapping> columnMappingsErr = new List<SqlBulkCopyColumnMapping>();
                    tempDT.Clear();
                    tempDTErr.Clear();
                    if (details[0].IS_JOB_ID_REQUIRED == 1)
                    {
                        tempDT.Columns.Add("JOB_ID", System.Type.GetType("System.Guid"));
                    }
                    tempDT.Columns.Add("DATE_CREATED", typeof(DateTime));
                    tempDT.Columns.Add("CREATED_BY", typeof(Int64));
                    tempDT.Columns.Add("ROW_ID", typeof(Int64));
                    foreach (var column in columnNames)
                    {
                        tempDT.Columns.Add(column);
                    }
                    foreach (DataColumn col in tempDT.Columns)
                    {
                        columnMappings.Add(new SqlBulkCopyColumnMapping(col.ColumnName, col.ColumnName));
                    }
                    tempDTErr.Columns.Add("JOB_ID", System.Type.GetType("System.Guid"));
                    foreach (var column in errcolumnNames)
                    {
                        tempDTErr.Columns.Add(column);
                    }
                    foreach (DataColumn col in tempDTErr.Columns)
                    {
                        columnMappingsErr.Add(new SqlBulkCopyColumnMapping(col.ColumnName, col.ColumnName));
                    }
                    var totalChunks = currentData.AsEnumerable().ToList();
                    int Totalcount = currentData.Rows.Count;
                    tempDT.Rows.Clear();
                    int rowIndex = 0;
                    var guid = Guid.NewGuid();
                    foreach (var row in totalChunks)
                    {
                        var newrow = tempDT.NewRow();
                        var errnewrow = tempDTErr.NewRow();
                        if (tempDT.Columns.Contains("JOB_ID"))
                        {
                            newrow["JOB_ID"] = guid;
                        }
                        if (tempDT.Columns.Contains("DATE_CREATED"))
                        {
                            newrow["DATE_CREATED"] = DateTime.UtcNow;
                        }
                        if (tempDT.Columns.Contains("CREATED_BY"))
                        {
                            newrow["CREATED_BY"] = U_ID;
                        }
                        string columnName = string.Empty;
                        string columnDataType = string.Empty;
                        bool isDatatypeColumnError = false;
                        bool isMandateColumnError = false;
                        bool isDropDownValueError = false;
                        bool isPhoneNumberError = false;
                        string isValueNotExistsInSysError = string.Empty;
                        List<string> allMandateErrorColumns = new List<string>();
                        Dictionary<string, string> allDatatypeErrorColumns = new Dictionary<string, string>();
                        List<string> allDropDownValueErrorColumns = new List<string>();
                        Dictionary<string, string> allValueNotExistsErrorColumns = new Dictionary<string, string>();
                        if (rowIndex == 0)
                        {
                            int rowNumber = rowIndex + 2;
                            int index = 0;
                            foreach (DataColumn dc in row.Table.Columns)
                            {
                                if (tempDT.Columns.Contains("ROW_ID"))
                                {
                                    newrow["ROW_ID"] = rowNumber;
                                }
                                isMandateColumnError = false;
                                isDatatypeColumnError = false;
                                isDropDownValueError = false;
                                isPhoneNumberError = false;
                                isValueNotExistsInSysError = string.Empty;
                                columnName = GetCurrentDataColumnName(dc, details);
                                row.Table.Columns[index].ColumnName = columnName;
                                row.Table.AcceptChanges();
                                string rowValue = GetDataRowVaue(row, columnName);
                                columnDataType = GetCurrentDataColumnType(columnName, details).ToString();
                                isMandateColumnError = GetRowValueMandate(columnName, details, rowValue, isMandateColumnError);
                                string errMessage = string.Empty;
                                if (isMandateColumnError)
                                {
                                    allMandateErrorColumns.Add(GetUIColumnName(columnName, details));
                                }
                                /***** compare data types errors,drop down values exists,db values exists start *****/

                                if (!string.IsNullOrEmpty(rowValue)) {
                                    isDatatypeColumnError = GetRowValueDatatype(columnDataType, isDatatypeColumnError, rowValue);// if row value is not null and then check data types
                                    if (isDatatypeColumnError) {
                                        allDatatypeErrorColumns.Add(GetUIColumnName(columnName, details), columnDataType);
                                    }
                                    if (!isDatatypeColumnError) {
                                        isDropDownValueError = GetRowDropDownValue(columnName, details, rowValue, compID);// if row value is not null and data types are matched then check whether the dropdown values are matched
                                        if (isDropDownValueError) {
                                            allDropDownValueErrorColumns.Add(GetUIColumnName(columnName, details));
                                        }
                                    }
                                    //if (!isDatatypeColumnError && !isDropDownValueError) {
                                    //    isValueNotExistsInSysError = IsValueExistsInSystem(columnName, details, rowValue, compID);// if row value is not null and data types,drop down values are matched then check whether the value exists in DB.
                                    //    if (!string.IsNullOrEmpty(isValueNotExistsInSysError)) {
                                    //        allValueNotExistsErrorColumns.Add(GetUIColumnName(columnName, details), isValueNotExistsInSysError);
                                    //    }
                                    //}
                                    if (isPhoneNumberColumn(details, columnName))
                                    {
                                        isPhoneNumberError = ValidatePhoneNumber(rowValue);
                                        if (isPhoneNumberError) { 
                                            allDatatypeErrorColumns.Add(GetUIColumnName(columnName, details), "PHONE_NUMBER");
                                        }
                                    }

                                    if ((columnDataType == "decimal" || columnDataType == "double") && Convert.ToDecimal(rowValue) <= 0)
                                    {
                                        allDatatypeErrorColumns.Add(GetUIColumnName(columnName, details), columnDataType);
                                    }

                                }
                                if (columnDataType == "datetime" && allDatatypeErrorColumns.Count <= 0 && allDropDownValueErrorColumns.Count <= 0 && allValueNotExistsErrorColumns.Count <= 0) {
                                    rowValue = string.IsNullOrEmpty(rowValue) ? null : Convert.ToDateTime(rowValue).ToString("yyyy-MM-dd");
                                }

                                if (columnDataType == "decimal" || columnDataType == "double")
                                {
                                    rowValue = string.IsNullOrEmpty(rowValue) ? "0" : rowValue;
                                }

                                /***** compare data types errors,drop down values exists,db values exists start *****/

                                if (allMandateErrorColumns.Count <= 0 && allDatatypeErrorColumns.Count <= 0 && allDropDownValueErrorColumns.Count <= 0 && allValueNotExistsErrorColumns.Count <= 0) {
                                    newrow[columnName] = rowValue;
                                }
                                index++;
                            }

                            if (allMandateErrorColumns.Count > 0)
                            {
                                string allErrorColumnNames = string.Join(",", allMandateErrorColumns.ToList());
                                ErrorData(allErrorColumnNames, guid, name, compID, U_ID, rowNumber, errdetails, errorCodes(1), fileName);
                            }

                            if (allDatatypeErrorColumns.Count > 0)
                            {
                                ValidateDatatypeErrors(allDatatypeErrorColumns, guid, name, compID, U_ID, rowNumber, errdetails, fileName);
                            }

                            if (allDropDownValueErrorColumns.Count > 0)
                            {
                                string allDropDownErrorColumnNames = string.Join(",", allDropDownValueErrorColumns.ToList());
                                ErrorData(allDropDownErrorColumnNames, guid, name, compID, U_ID, rowNumber, errdetails, errorCodes(4), fileName);
                            }

                            if (allValueNotExistsErrorColumns.Count > 0)
                            {
                                //string valuesNotExistsErrorColumns = string.Join(",", allValueNotExistsErrorColumns.ToList());
                                //ErrorData(valuesNotExistsErrorColumns, guid, name, compID, U_ID, rowNumber, errdetails, errorCodes(5), fileName);
                                ValidateDBErrors(allValueNotExistsErrorColumns, guid, name, compID, U_ID, rowNumber, errdetails, fileName);
                            }
                        }
                        else
                        {
                            int rowNumber = rowIndex + 2;
                            foreach (DataColumn dc in row.Table.Columns)
                            {
                                if (tempDT.Columns.Contains("ROW_ID"))
                                {
                                    newrow["ROW_ID"] = rowNumber;
                                }
                                isMandateColumnError = false;
                                isDatatypeColumnError = false;
                                isDropDownValueError = false;
                                isPhoneNumberError = false;
                                isValueNotExistsInSysError = string.Empty;
                                columnName = dc.ColumnName;
                                string rowValue = GetDataRowVaue(row, columnName);
                                columnDataType = GetCurrentDataColumnType(columnName, details).ToString();
                                isMandateColumnError = GetRowValueMandate(columnName, details, rowValue, isMandateColumnError);
                                string errMessage = string.Empty;
                                if (isMandateColumnError)
                                {
                                    allMandateErrorColumns.Add(GetUIColumnName(columnName, details));
                                }
                                /***** compare data types errors,drop down values exists,db values exists start *****/
                                if (!string.IsNullOrEmpty(rowValue))
                                {
                                    isDatatypeColumnError = GetRowValueDatatype(columnDataType, isDatatypeColumnError, rowValue);// if row value is not null and then check data types 
                                    if (isDatatypeColumnError)
                                    {
                                        allDatatypeErrorColumns.Add(GetUIColumnName(columnName, details), columnDataType);
                                    }
                                    if (!isDatatypeColumnError)
                                    {
                                        isDropDownValueError = GetRowDropDownValue(columnName, details, rowValue, compID);// if row value is not null and data types are matched then check whether the dropdown values are matched
                                        if (isDropDownValueError)
                                        {
                                            allDropDownValueErrorColumns.Add(GetUIColumnName(columnName, details));
                                        }
                                    }
                                    if (!isDatatypeColumnError && !isDropDownValueError)
                                    {
                                        isValueNotExistsInSysError = IsValueExistsInSystem(columnName, details, rowValue, compID);// if row value is not null and data types,drop down values are matched then check whether the value exists in DB.
                                        if (!string.IsNullOrEmpty(isValueNotExistsInSysError))
                                        {
                                            allValueNotExistsErrorColumns.Add(GetUIColumnName(columnName, details), isValueNotExistsInSysError);
                                        }
                                    }
                                    if (isPhoneNumberColumn(details,columnName)) 
                                    {
                                        isPhoneNumberError = ValidatePhoneNumber(rowValue);
                                        if (isPhoneNumberError)
                                        {
                                            allDatatypeErrorColumns.Add(GetUIColumnName(columnName, details), "PHONE_NUMBER");
                                        }
                                    }

                                    if ((columnDataType == "decimal" || columnDataType == "double") && Convert.ToDecimal(rowValue) <= 0)
                                    {
                                        allDatatypeErrorColumns.Add(GetUIColumnName(columnName, details), columnDataType);
                                    }
                                }

                                if (columnDataType == "datetime" && allDatatypeErrorColumns.Count <= 0 && allDropDownValueErrorColumns.Count <= 0 && allValueNotExistsErrorColumns.Count <= 0)
                                {
                                    rowValue = string.IsNullOrEmpty(rowValue) ? null : Convert.ToDateTime(rowValue).ToString("yyyy-MM-dd");
                                }

                                if (columnDataType == "decimal" || columnDataType == "double")
                                {
                                    rowValue = string.IsNullOrEmpty(rowValue) ? "0" : rowValue;
                                }

                                /***** compare data types errors,drop down values exists,db values exists start *****/

                                if (allMandateErrorColumns.Count <= 0 && allDatatypeErrorColumns.Count <= 0 && allDropDownValueErrorColumns.Count <= 0 && allValueNotExistsErrorColumns.Count <= 0) {
                                    newrow[columnName] = rowValue;
                                }
                            }

                            if (allMandateErrorColumns.Count > 0)
                            {
                                string allErrorColumnNames = string.Join(",", allMandateErrorColumns.ToList());
                                ErrorData(allErrorColumnNames, guid, name, compID, U_ID, rowNumber, errdetails, errorCodes(1), fileName);
                            }

                            if (allDatatypeErrorColumns.Count > 0)
                            {
                                ValidateDatatypeErrors(allDatatypeErrorColumns, guid, name, compID, U_ID, rowNumber, errdetails, fileName);
                            }

                            if (allDropDownValueErrorColumns.Count > 0)
                            {
                                string allDropDownErrorColumnNames = string.Join(",", allDropDownValueErrorColumns.ToList());
                                ErrorData(allDropDownErrorColumnNames, guid, name, compID, U_ID, rowNumber, errdetails, errorCodes(4), fileName);
                            }

                            if (allValueNotExistsErrorColumns.Count > 0)
                            {
                                ValidateDBErrors(allValueNotExistsErrorColumns, guid, name, compID, U_ID, rowNumber, errdetails, fileName);
                            }

                        }
                        if (allMandateErrorColumns.Count <= 0 && allDatatypeErrorColumns.Count <= 0 && allDropDownValueErrorColumns.Count <= 0 && allValueNotExistsErrorColumns.Count <= 0) {
                            tempDT.Rows.Add(newrow);
                        }
                        rowIndex++;
                    }

                    if (errdetails.Count > 0 && proceedFurther == 1)
                    {
                        foreach (var errRow in errdetails)
                        {
                            var rowVal = tempDTErr.NewRow();
                            rowVal["JOB_ID"] = errRow.JOB_ID;
                            rowVal["REASON"] = errRow.REASON;
                            rowVal["TEMPLATE_NAME"] = errRow.TEMPLATE_NAME;
                            rowVal["COMP_ID"] = errRow.COMP_ID;
                            rowVal["U_ID"] = errRow.U_ID;
                            rowVal["DATE_CREATED"] = errRow.DATE_CREATED;
                            tempDTErr.Rows.Add(rowVal);
                        }
                        sqlHelper.BulkInsert(tempDTErr, "UPLOAD_MODULE_ERROR_DETAILS", columnMappingsErr);
                        tempDTErr.Rows.Clear();
                    }

                    if (tempDT.Rows.Count > 0 && proceedFurther == 1)
                    {
                        sqlHelper.BulkInsert(tempDT, tableName, columnMappings);
                        int SuccessfullRecCount = 0;
                        int FailedRecCount = 0;
                        if (name.Equals("PO_DETAILS"))
                        {
                            SortedDictionary<object, object> sd1 = new SortedDictionary<object, object>() { };
                            sd1.Add("P_JOB_ID", guid);
                            DataSet data = sqlHelper.SelectList("erp_process_sap_po_schedule_details", sd1);
                            SuccessfullRecCount = Convert.ToInt32(data.Tables[0].Rows[0][0]);
                            int temp1 = (tempDT.Rows.Count - SuccessfullRecCount);
                            FailedRecCount = (temp1 <= 0 ? errdetails.Count : temp1);
                            errdetails.Clear();
                            ExcelErrorUpload errdetails1 = new ExcelErrorUpload();
                            errdetails1.SUCCESS_COUNT = SuccessfullRecCount;
                            errdetails1.FAILED_COUNT = FailedRecCount;
                            errdetails1.TOTAL_COUNT = (SuccessfullRecCount + FailedRecCount);
                            errdetails1.IS_SUCCESS = true;
                            errdetails.Add(errdetails1);
                        }
                        else if (name.Equals("VENDOR_DETAILS"))
                        {
                            SortedDictionary<object, object> sd1 = new SortedDictionary<object, object>() { };
                            sd1.Add("P_JOB_ID", guid);
                            DataSet data = sqlHelper.SelectList("erp_process_sap_vendor_details", sd1);
                            SuccessfullRecCount = tempDT.Rows.Count;
                            FailedRecCount = errdetails.Count;
                            errdetails.Clear();
                            ExcelErrorUpload errdetails1 = new ExcelErrorUpload();
                            errdetails1.SUCCESS_COUNT = SuccessfullRecCount;
                            errdetails1.FAILED_COUNT = FailedRecCount;
                            errdetails1.TOTAL_COUNT = (SuccessfullRecCount + FailedRecCount);
                            errdetails1.IS_SUCCESS = true;
                            errdetails.Add(errdetails1);
                            //if (errdetails1.SUCCESS_COUNT > 0)
                            //{
                            //    PRMServices prm = new PRMServices();
                            //    prm.TriggerEmailsForVendorsFromIntegration(sessionID, guid);
                            //}
                        }
                        else if (name.Equals("PAYMENT"))
                        {
                            SortedDictionary<object, object> sd1 = new SortedDictionary<object, object>() { };
                            sd1.Add("P_JOB_ID", guid);
                            DataSet data = sqlHelper.SelectList("erp_process_sap_payment_details", sd1);
                            SuccessfullRecCount = tempDT.Rows.Count;
                            FailedRecCount = errdetails.Count;
                            errdetails.Clear();
                            ExcelErrorUpload errdetails1 = new ExcelErrorUpload();
                            errdetails1.SUCCESS_COUNT = SuccessfullRecCount;
                            errdetails1.FAILED_COUNT = FailedRecCount;
                            errdetails1.TOTAL_COUNT = (SuccessfullRecCount + FailedRecCount);
                            errdetails1.IS_SUCCESS = true;
                            errdetails.Add(errdetails1);
                        } else if (name.Equals("INVOICES"))
                        {
                            SortedDictionary<object, object> sd1 = new SortedDictionary<object, object>() { };
                            sd1.Add("P_JOB_ID", guid);
                            DataSet data = sqlHelper.SelectList("erp_process_sap_cash_flow_details", sd1);
                            SuccessfullRecCount = Convert.ToInt32(data.Tables[0].Rows[0][0]);
                            int temp1 = (tempDT.Rows.Count - SuccessfullRecCount);
                            FailedRecCount = (temp1 <= 0 ? errdetails.Count : temp1);
                            errdetails.Clear();
                            ExcelErrorUpload errdetails1 = new ExcelErrorUpload();
                            errdetails1.SUCCESS_COUNT = SuccessfullRecCount;
                            errdetails1.FAILED_COUNT = FailedRecCount;
                            errdetails1.TOTAL_COUNT = (SuccessfullRecCount + FailedRecCount);
                            errdetails1.IS_SUCCESS = true;
                            errdetails.Add(errdetails1);
                        }
                        else if (name.Equals("REVENUE"))
                        {
                            SortedDictionary<object, object> sd1 = new SortedDictionary<object, object>() { };
                            sd1.Add("P_JOB_ID", guid);
                            DataSet data = sqlHelper.SelectList("erp_process_sap_cash_flow_details", sd1);
                            SuccessfullRecCount = Convert.ToInt32(data.Tables[0].Rows[0][0]);
                            int temp1 = (tempDT.Rows.Count - SuccessfullRecCount);
                            FailedRecCount = (temp1 <= 0 ? errdetails.Count : temp1);
                            errdetails.Clear();
                            ExcelErrorUpload errdetails1 = new ExcelErrorUpload();
                            errdetails1.SUCCESS_COUNT = SuccessfullRecCount;
                            errdetails1.FAILED_COUNT = FailedRecCount;
                            errdetails1.TOTAL_COUNT = (SuccessfullRecCount + FailedRecCount);
                            errdetails1.IS_SUCCESS = true;
                            errdetails.Add(errdetails1);
                        }
                        tempDT.Rows.Clear();
                    }
                }
            }
            catch (Exception ex)
            {
                ExcelErrorUpload errdetails1 = new ExcelErrorUpload();
                errdetails1.SUCCESS_COUNT = 0;
                errdetails1.FAILED_COUNT = 1;
                errdetails1.REASON = ex.Message;
                errdetails1.IS_SUCCESS = false;
                errdetails.Add(errdetails1);
                logger.Error(ex, ex.Message);
            }

            return errdetails;
        }
        private bool ValidatePhoneNumber(string rowValue)
        {
            Match isMatch = Regex.Match(rowValue, @"^\s*(?:\+?(\d{1,3}))?[-. (]*(\d{3})[-. )]*(\d{3})[-. ]*(\d{4})(?: *x(\d+))?\s*$", RegexOptions.IgnoreCase);
            return isMatch.Success ? false : true;
        }
        private bool isPhoneNumberColumn(List<ExcelUpload> details, string columnName) 
        {
            int isPhoneColumn = details.Where(a => a.DB_COLUMN_NAME.Equals(columnName.ToString())).FirstOrDefault().IS_PHONE > 0 ? 1 : 0;
            return isPhoneColumn > 0 ? true : false;
        }
        private bool GetRowDropDownValue(string columnName, List<ExcelUpload> details, string rowValue, int compID) 
        {
            bool isValidRowDropDownValue = false;
            int isdropdownRequired = details.Where(a => a.DB_COLUMN_NAME.Equals(columnName.ToString())).FirstOrDefault().IS_DROP_DOWN_REQUIRED;
            string drpdwnVals = details.Where(a => a.DB_COLUMN_NAME.Equals(columnName.ToString())).FirstOrDefault().DROP_DOWN_VALUES;
            List<string> dropdownValues = !string.IsNullOrEmpty(drpdwnVals) ? drpdwnVals.Split(',').ToList() : null;
            if (isdropdownRequired == 1 && dropdownValues.Count > 0) 
            {
                int foundIndex = dropdownValues.FindIndex(a => a.Equals(rowValue));
                isValidRowDropDownValue = foundIndex >= 0 ? false : true;
            }
            return isValidRowDropDownValue;        
        }
        private string IsValueExistsInSystem(string columnName, List<ExcelUpload> details, string rowValue, int compID) {
            string doesValueExistInDB = string.Empty;
            string query = details.Where(a => a.DB_COLUMN_NAME.Equals(columnName.ToString())).FirstOrDefault().FETCH_QUERY;
            if (!string.IsNullOrEmpty(query))
            {
                DataSet dsQuery = new DataSet();
                query = $"{query.Replace("$$$$", compID.ToString()).Replace(string.Concat("####",columnName,"####"), "'" + rowValue + "'").Replace("####","'")}";
                dsQuery = sqlHelper.ExecuteQuery(query);
                if (dsQuery != null && dsQuery.Tables[0].Rows.Count > 0)
                {
                    DataRow row = dsQuery.Tables[0].Rows[0];
                    string ERR_MESSAGE = row["ERR_MESSAGE"] != DBNull.Value ? Convert.ToString(row["ERR_MESSAGE"]) : string.Empty;
                    if (!string.IsNullOrEmpty(ERR_MESSAGE)) { 
                        doesValueExistInDB = ERR_MESSAGE;
                    }
                }
            }
            return doesValueExistInDB;
        }
        private void ValidateDatatypeErrors(Dictionary<string,string> allDatatypeErrorColumns,Guid guid, string name, int compID,int U_ID, int rowIndex, List<ExcelErrorUpload> errdetails, string fileName) {
            var values = allDatatypeErrorColumns.Where(a => a.Value.Equals("number") || a.Value.Equals("decimal") || a.Value.Equals("double")).GroupBy(a => a.Value).SelectMany(a => a);
            var datetimeValues = allDatatypeErrorColumns.Where(a => a.Value.Equals("datetime")).GroupBy(a => a.Value).SelectMany(a => a);
            var phoneNumberValues = allDatatypeErrorColumns.Where(a => a.Value.Equals("PHONE_NUMBER")).GroupBy(a => a.Value).SelectMany(a => a);
            if (values.ToList().Count > 0)
            {
                string allErrorColumnNames = string.Join(",", values.Select(p => p.Key));
                ErrorData(allErrorColumnNames, guid, name, compID, U_ID, rowIndex, errdetails, errorCodes(2), fileName);
            }

            if (datetimeValues.ToList().Count > 0)
            {
                string allErrorColumnNames = string.Join(",", datetimeValues.Select(p => p.Key));
                ErrorData(allErrorColumnNames, guid, name, compID, U_ID, rowIndex, errdetails, $" {errorCodes(3)} {String.Join(",", validDates())}", fileName);
            }

            if (phoneNumberValues.ToList().Count > 0)
            {
                string allErrorColumnNames = string.Join(",", phoneNumberValues.Select(p => p.Key));
                ErrorData(allErrorColumnNames, guid, name, compID, U_ID, rowIndex, errdetails, errorCodes(6), fileName);
            }
        }
        private void ValidateDBErrors(Dictionary<string, string> allDBErrorColumns, Guid guid, string name, int compID, int U_ID, int rowIndex, List<ExcelErrorUpload> errdetails, string fileName)
        {
            var values = allDBErrorColumns.GroupBy(a => a.Value).SelectMany(a => a);
            if (values.ToList().Count > 0)
            {
                foreach (var erroValue in values.ToList()) { 
                    ErrorData(erroValue.Key, guid, name, compID, U_ID, rowIndex, errdetails, erroValue.Value, fileName);
                }
            }
        }
        private void ErrorData(string columnName, Guid jobId, string templateName, int compID, int U_ID, int excelRowNumber, 
            List<ExcelErrorUpload> errdetails, string errMessage, string fileName) 
        {
            int foundIndex = errdetails.FindIndex(a=>a.ROW_NUMBER == excelRowNumber);
            if (foundIndex >= 0) {
                errdetails[foundIndex].REASON += $",<strong> following columns {columnName} {errMessage}</strong>";
            } else { 
                ExcelErrorUpload errObject = new ExcelErrorUpload();
                errObject.JOB_ID = jobId;
                errObject.REASON = $"<strong>The Row Number {excelRowNumber} in {fileName}  with the column names {columnName} {errMessage}</strong>";
                errObject.TEMPLATE_NAME = templateName;
                errObject.COMP_ID = compID;
                errObject.U_ID = U_ID;
                errObject.DATE_CREATED = DateTime.UtcNow;
                errObject.ROW_NUMBER = excelRowNumber;
                errObject.COLUMN_NAME = columnName;
                errdetails.Add(errObject);
            }
        }
        private static string GetUIColumnName(string columnName, List<ExcelUpload> eu)
        {
            string uiColumnName = string.Empty;
            uiColumnName = eu.Where(a => a.DB_COLUMN_NAME.Equals(columnName.ToString())).FirstOrDefault().UI_COLUMN_NAME.ToString();
            return uiColumnName;
        }
        private static string GetCurrentDataColumnName(DataColumn columnName, List<ExcelUpload> eu)
        {
            string dbColumnName = string.Empty;
            dbColumnName = eu.Where(a => a.UI_COLUMN_NAME.Equals(columnName.ToString())).FirstOrDefault().DB_COLUMN_NAME.ToString();
            return dbColumnName;
        }
        private static string GetCurrentDataColumnType(string columnName, List<ExcelUpload> eu)
        {
            string dbColumnDataType = string.Empty;
            dbColumnDataType = eu.Where(a => a.DB_COLUMN_NAME.Equals(columnName.ToString())).FirstOrDefault().COLUMN_DATA_TYPE.ToString();
            return dbColumnDataType;
        }
        private static bool GetRowValueMandate(string columnName, List<ExcelUpload> excelUploadDetails, string rowValue, bool isMandateColumnError) {
            bool isMandate = false;
            isMandate = excelUploadDetails.Where(a => a.DB_COLUMN_NAME.Equals(columnName.ToString())).FirstOrDefault().IS_MANDATE > 0 ? true : false;
            if (string.IsNullOrEmpty(rowValue) && isMandate)
            {
                isMandateColumnError = true;
            }
            return isMandateColumnError;
        }
        private static bool GetRowValueDatatype(string columnDataType, bool isDatatypeColumnError, string rowValue)
        {
            if ((columnDataType.ToLower().Equals("decimal") || columnDataType.ToLower().Equals("double")))
            {
                decimal myDec;
                var Result = decimal.TryParse(rowValue, out myDec);
                if (!Result)
                {
                    isDatatypeColumnError = true;
                }
            }
            else if (columnDataType.ToLower().Equals("number"))
            {
                int n;
                bool Result = int.TryParse(rowValue, out n);
                if (!Result)
                {
                    isDatatypeColumnError = true;
                }
            }
            else if (columnDataType.ToLower().Equals("datetime"))
            {
                DateTime dtValue;
                var formats = validDates();
                rowValue = rowValue.Split(' ')[0];
                var datetimeValue = DateTime.TryParseExact(rowValue, formats, System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None, out dtValue);
                if (!datetimeValue)
                {
                    isDatatypeColumnError = true;
                }
            }
            return isDatatypeColumnError;
        }
        private static string[] validDates() {
            return new[] { "dd/MM/yyyy", "yyyy-MM-dd", "dd-MM-yyyy" };
        }
        private static string GetDataRowVaue(DataRow row, string columnName)
        {
            if (row.Table.Columns.Contains(columnName))
            {
                string rowValue = row[columnName].ToString();
                
                return row[columnName] != DBNull.Value ? rowValue : null;
            }
            else
            {
                return null;
            }
        }
        public string getDBColumnNameForMapping(List<ExcelUpload> eu,DataTable dt,DataTable dt1) {
            string values = string.Empty;
            List<string> UIColumnNames = new List<string>();
            var columns = dt1.Columns;
            if (columns != null && columns.Count > 0) {
                foreach (var column in columns) {
                    UIColumnNames.Add(eu.Where(a => a.UI_COLUMN_NAME.Equals(column.ToString())).FirstOrDefault().DB_COLUMN_NAME.ToString());
                }
            }
            values = string.Join(",", UIColumnNames);
            return values;
        }
        public List<ModuleBasedError> GetModuleBasedErrorDetails(string MODULE_NAME, int compID, string sessionID) 
        {
            List<ModuleBasedError> details = new List<ModuleBasedError>();
            try
            {
                Utilities.ValidateSession(sessionID);
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                sd.Add("P_COMP_ID", compID);
                sd.Add("P_MODULE_NAME", MODULE_NAME);
                CORE.DataNamesMapper<ModuleBasedError> mapper = new CORE.DataNamesMapper<ModuleBasedError>();
                DataSet ds = sqlHelper.SelectList("rp_GetModuleBasedErrorDetails", sd);
                details = mapper.Map(ds.Tables[0]).ToList();
            }
            catch (Exception ex) {
                logger.Error("error in GetModuleBasedErrorDetails >>" + ex.Message);
            }
            return details;
        }
        private string errorCodes(int errCodeNumber) 
        {
            string strValue = string.Empty;
            switch (errCodeNumber) 
            {
                case 1:
                    strValue = " are Empty";
                    break;
                case 2:
                    strValue = " has invalid data provided accepted are only numbers and should be greater than 0";
                    break;
                case 3:
                    strValue = " has invalid date provided accepted formats are : ";
                    break;
                case 4:
                    strValue = " doesn't match the dropdown values";
                    break;
                case 5:
                    strValue = " value doesn't exist in the system";
                    break;
                case 6:
                    strValue = " value is not of a phone number format.";
                    break;
                default: 
                    break;
            }


            return strValue;
        }
    }

}