﻿using System;
using System.Collections.Generic;
using System.Data;
using OfficeOpenXml;
using PRMServices.Models;
using PRMServices.SQLHelper;

namespace PRMServices.Common
{
    public static class PRMUtility
    {
        public static CustomProperty GetCustPropObject(DataRow row)
        {
            CustomProperty detail = new CustomProperty();
            detail.CustPropID = Convert.ToInt32(row.GetColumnValue("CUST_PROP_ID", detail.CustPropID.GetType()));
            detail.CompanyID = Convert.ToInt32(row.GetColumnValue("COMP_ID", detail.CompanyID.GetType()));
            detail.CustPropCode = Convert.ToString(row.GetColumnValue("CUST_PROP_CODE", detail.CustPropCode.GetType()));
            detail.CustPropDesc = Convert.ToString(row.GetColumnValue("CUST_PROP_DESC", detail.CustPropDesc.GetType()));
            detail.CustPropType = Convert.ToString(row.GetColumnValue("CUST_PROP_TYPE", detail.CustPropType.GetType()));
            detail.CustPropDefault = Convert.ToString(row.GetColumnValue("CUST_PROP_DEFAULT", detail.CustPropDefault.GetType()));
            detail.ModuleName = Convert.ToString(row.GetColumnValue("CUST_MODULE", detail.ModuleName.GetType()));
            detail.EntityID = Convert.ToInt32(row.GetColumnValue("ENTITY_ID", detail.EntityID.GetType()));
            detail.CustPropValue = Convert.ToString(row.GetColumnValue("CUST_PROP_VALUE", detail.CustPropValue.GetType()));

            return detail;
        }

        public static DataTable WorksheetToDataTable(ExcelWorksheet oSheet)
        {
            int totalRows = oSheet.Dimension.End.Row;
            int totalCols = oSheet.Dimension.End.Column;
            DataTable dt = new DataTable(oSheet.Name);
            DataRow dr = null;
            for (int i = 1; i <= totalRows; i++)
            {
                if (i > 1) dr = dt.Rows.Add();
                for (int j = 1; j <= totalCols; j++)
                {
                    if (i == 1)
                    {
                        dt.Columns.Add(oSheet.Cells[i, j].Value.ToString());
                    }
                    else
                    {
                        if (oSheet.Cells[i, j] != null && oSheet.Cells[i, j].Value != null)
                        {
                            dr[j - 1] = oSheet.Cells[i, j].Value.ToString();
                        }
                        else
                        {
                            dr[j - 1] = string.Empty;
                        }
                    }
                }
            }
            return dt;
        }

        public static DataSet SaveCompanyCustProp(CustomProperty companyProp)
        {
            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            sd.Add("P_CUST_PROP_ID", companyProp.CustPropID);
            sd.Add("P_COMP_ID", companyProp.CompanyID);
            sd.Add("P_CUST_PROP_CODE", companyProp.CustPropCode);
            sd.Add("P_CUST_PROP_DESC", companyProp.CustPropDesc);
            sd.Add("P_CUST_PROP_TYPE", companyProp.CustPropType);
            sd.Add("P_CUST_PROP_DEFAULT", companyProp.CustPropDefault);
            sd.Add("P_CUST_MODULE", companyProp.ModuleName);
            sd.Add("P_USER", companyProp.ModifiedBY);
            DataSet ds = DatabaseProvider.GetDatabaseProvider().SelectList("cp_SaveCompanyCustProp", sd);
            return ds;
        }

        public static DataSet SaveCompanyPropValue(CustomProperty companyProp)
        {
            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            sd.Add("P_CUST_PROP_ID", companyProp.CustPropID);
            sd.Add("P_ENTITY_ID", companyProp.EntityID);
            sd.Add("P_CUST_PROP_VALUE", companyProp.CustPropValue);
            sd.Add("P_USER", companyProp.ModifiedBY);
            DataSet ds = DatabaseProvider.GetDatabaseProvider().SelectList("cp_SaveCompanyCustPropValues", sd);
            return ds;
        }
    }
}