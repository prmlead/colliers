﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using OfficeOpenXml;
using PRMServices.Models;
using PRMServices.SQLHelper;

namespace PRMServices.Common
{
    public static class StoreUtility
    {
        public static Store GetStoreObject(DataRow row)
        {
            Store detail = new Store();
            detail.StoreID = Convert.ToInt32(row.GetColumnValue("STORE_ID", detail.StoreID.GetType()));
            detail.CompanyID = Convert.ToInt32(row.GetColumnValue("COMP_ID", detail.CompanyID.GetType()));
            detail.IsMainBranch = Convert.ToInt32(row.GetColumnValue("IS_MAIN", detail.IsMainBranch.GetType()));
            detail.MainStoreID = Convert.ToInt32(row.GetColumnValue("MAIN_STORE_ID", detail.MainStoreID.GetType()));
            detail.StoreCode = Convert.ToString(row.GetColumnValue("STORE_CODE", detail.StoreCode.GetType()));
            detail.StoreDescription = Convert.ToString(row.GetColumnValue("STORE_DESC", detail.StoreDescription.GetType()));
            detail.StoreInCharge = Convert.ToString(row.GetColumnValue("STORE_INCHARGE", detail.StoreInCharge.GetType()));
            detail.StoreDetails = Convert.ToString(row.GetColumnValue("STORE_DETAILS", detail.StoreDetails.GetType()));
            detail.IsValid = Convert.ToInt32(row.GetColumnValue("IS_VALID", detail.IsValid.GetType()));
            detail.StoreAddress = Convert.ToString(row.GetColumnValue("STORE_ADDRESS", detail.StoreAddress.GetType()));
            detail.StoreBranches = Convert.ToInt32(row.GetColumnValue("STORE_BRANCHES", detail.StoreBranches.GetType()));

            return detail;
        }

        public static StoreItem GetStoreItemObject(DataRow row)
        {
            StoreItem detail = new StoreItem();
            detail.DoAlert = 0;
            var doAlert = row.GetColumnObject("DO_ALERT");
            if(doAlert == null)
            {
                doAlert = 0;
            }
            else
            {
                if(doAlert.ToString().ToLower().Equals("true") || doAlert.ToString().ToLower().Equals("1"))
                {
                    detail.DoAlert = 1;
                }
            }

            detail.ItemID = Convert.ToInt32(row.GetColumnValue("ITEM_ID", detail.ItemID.GetType()));
            detail.StoreDetails = GetStoreObject(row);

            if(row.GetColumnObject("ITEM_CODE") == null)
            {
                detail.ItemCode = Convert.ToString(row.GetColumnValue("HSN_CODE", detail.ItemCode.GetType()));
            }
            else
            {
                detail.ItemCode = Convert.ToString(row.GetColumnValue("ITEM_CODE", detail.ItemCode.GetType()));
            }

            if (row.GetColumnObject("ITEM_NAME") == null)
            {
                detail.ItemName = Convert.ToString(row.GetColumnValue("SUB_CODE", detail.ItemCode.GetType()));
            }
            else
            {
                detail.ItemName = Convert.ToString(row.GetColumnValue("ITEM_NAME", detail.ItemCode.GetType()));
            }

            //detail.ItemName = Convert.ToString(row.GetColumnValue("ITEM_NAME", detail.ItemName.GetType()));
            detail.ItemType = Convert.ToString(row.GetColumnValue("ITEM_TYPE", detail.ItemType.GetType()));
            detail.ItemSubType = Convert.ToString(row.GetColumnValue("ITEM_SUBTYPE", detail.ItemSubType.GetType()));
            detail.ItemDescription = Convert.ToString(row.GetColumnValue("ITEM_DESC", detail.ItemDescription.GetType()));
            detail.TotalStock = Convert.ToDecimal(row.GetColumnValue("TOTAL_STOCK", detail.TotalStock.GetType()));
            detail.InStock = Convert.ToDecimal(row.GetColumnValue("IN_STOCK", detail.InStock.GetType()));
            detail.OnOrder = Convert.ToDecimal(row.GetColumnValue("ON_ORDER", detail.OnOrder.GetType()));
            detail.ItemPrice = Convert.ToDecimal(row.GetColumnValue("ITEM_PRICE", detail.ItemPrice.GetType()));
            if (row.GetColumnObject("IS_VALID") == null)
            {
                detail.IsValid = 1;
            }
            else
            {
                detail.IsValid = Convert.ToInt32(row.GetColumnValue("IS_VALID", detail.IsValid.GetType()));
            }

            //detail.IsValid = Convert.ToInt32(row.GetColumnValue("IS_VALID", detail.IsValid.GetType()));
            //detail.DoAlert = Convert.ToInt32(row.GetColumnValue("DO_ALERT", detail.DoAlert.GetType()));
            detail.Threshold = Convert.ToInt32(row.GetColumnValue("THRESHOLD", detail.Threshold.GetType()));
            detail.IGST = Convert.ToDecimal(row.GetColumnValue("IGST", detail.IGST.GetType()));
            detail.CGST = Convert.ToDecimal(row.GetColumnValue("CGST", detail.CGST.GetType()));
            detail.SGST = Convert.ToDecimal(row.GetColumnValue("SGST", detail.SGST.GetType()));
            detail.ItemUnits = Convert.ToString(row.GetColumnValue("ITEM_UNITS", detail.ItemUnits.GetType()));
            detail.HSNCode = Convert.ToString(row.GetColumnValue("HSN_CODE", detail.HSNCode.GetType()));
            detail.AttachmentId = Convert.ToString(row.GetColumnValue("ATTACHMENT_ID", detail.AttachmentId.GetType()));



            return detail;
        }

        public static CustomProperty GetStoreItemPropObject1(DataRow row)
        {
            CustomProperty detail = new CustomProperty();
            detail.CustPropID = Convert.ToInt32(row.GetColumnValue("CUST_PROP_ID", detail.CustPropID.GetType()));
            detail.CompanyID = Convert.ToInt32(row.GetColumnValue("COMP_ID", detail.CompanyID.GetType()));
            detail.CustPropCode = Convert.ToString(row.GetColumnValue("CUST_PROP_CODE", detail.CustPropCode.GetType()));
            detail.CustPropDesc = Convert.ToString(row.GetColumnValue("CUST_PROP_DESC", detail.CustPropDesc.GetType()));
            detail.CustPropType = Convert.ToString(row.GetColumnValue("CUST_PROP_TYPE", detail.CustPropType.GetType()));
            detail.CustPropDefault = Convert.ToString(row.GetColumnValue("CUST_PROP_DEFAULT", detail.CustPropDefault.GetType()));
            detail.EntityID = Convert.ToInt32(row.GetColumnValue("ENTITY_ID", detail.EntityID.GetType()));
            detail.CustPropValue = Convert.ToString(row.GetColumnValue("CUST_PROP_VALUE", detail.CustPropValue.GetType()));

            return detail;
        }

        public static StoreItemDetails GetStoreItemDetailsObject(DataRow row)
        {
            StoreItemDetails detail = new StoreItemDetails();
            detail.ItemDetailID = Convert.ToInt32(row.GetColumnValue("ITEM_DETAIL_ID", detail.ItemDetailID.GetType()));
            detail.ItemID = Convert.ToInt32(row.GetColumnValue("ITEM_ID", detail.ItemID.GetType()));
            detail.SerialNo = Convert.ToString(row.GetColumnValue("SERIAL_NO", detail.SerialNo.GetType()));
            detail.WarrantyNo = Convert.ToString(row.GetColumnValue("WARRANTY_NO", detail.WarrantyNo.GetType()));
            detail.WarrantyDate = Convert.ToDateTime(row.GetColumnValue("WARRANTY_DATE", detail.WarrantyDate.GetType()));
            detail.GinID = Convert.ToInt32(row.GetColumnValue("GIN_ID", detail.GinID.GetType()));
            detail.GrnID = Convert.ToInt32(row.GetColumnValue("GRN_ID", detail.GrnID.GetType()));
            detail.PoID = Convert.ToInt32(row.GetColumnValue("PO_ID", detail.PoID.GetType()));
            detail.CreatedBy = Convert.ToInt32(row.GetColumnValue("CREATED_BY", detail.CreatedBy.GetType()));
            detail.DateCreated = Convert.ToDateTime(row.GetColumnValue("CREATED", detail.DateCreated.GetType()));
            detail.ModifiedBY = Convert.ToInt32(row.GetColumnValue("MODIFIED_BY", detail.ModifiedBY.GetType()));
            detail.DateModified = Convert.ToDateTime(row.GetColumnValue("MODIFIED", detail.DateModified.GetType()));

            return detail;
        }

        public static StoreItemReceive GetStoreItemReceiveDetailsObject(DataRow row)
        {
            StoreItemReceive detail = new StoreItemReceive();
            detail.GRNID = Convert.ToInt32(row.GetColumnValue("GRN_ID", detail.GRNID.GetType()));
            detail.GRNCode = Convert.ToString(row.GetColumnValue("GRN_CODE", detail.GRNCode.GetType()));
            //detail.StoreItemDetails = GetStoreItemObject(row);
            detail.IndentNo = Convert.ToString(row.GetColumnValue("INDENT_NO", detail.IndentNo.GetType()));
            detail.PONo = Convert.ToString(row.GetColumnValue("PO_NO", detail.PONo.GetType()));
            detail.SupplierName = Convert.ToString(row.GetColumnValue("SUPPLIER_NAME", detail.SupplierName.GetType()));
            detail.ReceiveDate = Convert.ToDateTime(row.GetColumnValue("RECEIVE_DATE", detail.ReceiveDate.GetType()));
            detail.ReceiveLocation = Convert.ToString(row.GetColumnValue("RECEIVE_LOCATION", detail.ReceiveLocation.GetType()));
            detail.CostCenter = Convert.ToString(row.GetColumnValue("COST_CENTER", detail.CostCenter.GetType()));
            detail.OrderQty = Convert.ToDecimal(row.GetColumnValue("ORDER_QTY", detail.OrderQty.GetType()));
            detail.ReceivedQty = Convert.ToDecimal(row.GetColumnValue("RECEIVED_QTY", detail.ReceivedQty.GetType()));
            detail.ReceivedBy = Convert.ToString(row.GetColumnValue("RECEIVED_BY", detail.ReceivedBy.GetType()));
            detail.CheckedBy = Convert.ToString(row.GetColumnValue("CHECKED_BY", detail.CheckedBy.GetType()));
            detail.ItemCondition = Convert.ToString(row.GetColumnValue("ITEM_CONDITION", detail.ItemCondition.GetType()));
            detail.Comments = Convert.ToString(row.GetColumnValue("COMMENTS", detail.Comments.GetType()));
            detail.PoAmount = Convert.ToDecimal(row.GetColumnValue("PO_AMOUNT", detail.PoAmount.GetType()));
            detail.PaidAmount = Convert.ToDecimal(row.GetColumnValue("ADVANCE_PAID", detail.PaidAmount.GetType()));
            detail.PaymentMode = Convert.ToString(row.GetColumnValue("PAYMENT_MODE", detail.PaymentMode.GetType()));
            detail.IndenterName = Convert.ToString(row.GetColumnValue("INDENTER_NAME", detail.IndenterName.GetType()));
            detail.GINCodeLink = Convert.ToString(row.GetColumnValue("GIN_CODE_LINK", detail.GINCodeLink.GetType()));

            return detail;
        }

        public static StoreItemIssue GetStoreItemIssueDetailsObject(DataRow row)
        {
            StoreItemIssue detail = new StoreItemIssue();
            detail.GINID = Convert.ToInt32(row.GetColumnValue("GIN_ID", detail.GINID.GetType()));
            detail.GINCode = Convert.ToString(row.GetColumnValue("GIN_CODE", detail.GINCode.GetType()));
            //detail.StoreItemDetails = GetStoreItemObject(row);
            detail.IndentNo = Convert.ToString(row.GetColumnValue("INDENT_NO", detail.IndentNo.GetType()));
            detail.ShipName = Convert.ToString(row.GetColumnValue("SHIP_NAME", detail.ShipName.GetType()));
            detail.ShipLine1 = Convert.ToString(row.GetColumnValue("SHIP_LINE1", detail.ShipLine1.GetType()));
            detail.ShipCity = Convert.ToString(row.GetColumnValue("SHIP_CITY", detail.ShipCity.GetType()));
            detail.ShipState = Convert.ToString(row.GetColumnValue("SHIP_STATE", detail.ShipState.GetType()));
            detail.ShipCountry = Convert.ToString(row.GetColumnValue("SHIP_COUNTRY", detail.ShipCountry.GetType()));
            detail.ShipZip = Convert.ToString(row.GetColumnValue("SHIP_ZIP", detail.ShipZip.GetType()));
            detail.Phone = Convert.ToString(row.GetColumnValue("PHONE", detail.Phone.GetType()));
            detail.IssueDate = Convert.ToDateTime(row.GetColumnValue("ISSUE_DATE", detail.IssueDate.GetType()));
            detail.IssueLocation = Convert.ToString(row.GetColumnValue("ISSUE_LOCATION", detail.IssueLocation.GetType()));
            detail.ShipMode = Convert.ToString(row.GetColumnValue("SHIP_MODE", detail.ShipMode.GetType()));
            detail.ShipTrackNo = Convert.ToString(row.GetColumnValue("SHIP_TRACK_NO", detail.ShipTrackNo.GetType()));
            detail.ShipModeDetails = Convert.ToString(row.GetColumnValue("SHIP_MODE_DETAILS", detail.ShipModeDetails.GetType()));
            detail.OrderQty = Convert.ToDecimal(row.GetColumnValue("ORDER_QTY", detail.OrderQty.GetType()));
            detail.ShipQty = Convert.ToDecimal(row.GetColumnValue("SHIP_QTY", detail.ShipQty.GetType()));
            detail.Comments = Convert.ToString(row.GetColumnValue("COMMENTS", detail.Comments.GetType()));

            return detail;
        }

        public static DataTable WorksheetToDataTable(ExcelWorksheet oSheet)
        {
            int totalRows = oSheet.Dimension.End.Row;
            int totalCols = oSheet.Dimension.End.Column;
            DataTable dt = new DataTable(oSheet.Name);
            DataRow dr = null;
            for (int i = 1; i <= totalRows; i++)
            {
                if (i > 1) dr = dt.Rows.Add();
                for (int j = 1; j <= totalCols; j++)
                {
                    if (i == 1)
                    {
                        dt.Columns.Add(oSheet.Cells[i, j].Value.ToString());
                    }
                    else
                    {
                        if (oSheet.Cells[i, j] != null && oSheet.Cells[i, j].Value != null)
                        {
                            dr[j - 1] = oSheet.Cells[i, j].Value.ToString();
                        }
                        else
                        {
                            dr[j - 1] = string.Empty;
                        }
                    }
                }
            }
            return dt;
        }

        public static DataSet SaveStoreEntity(Store store)
        {
            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            sd.Add("P_STORE_ID", store.StoreID);
            sd.Add("P_COMP_ID", store.CompanyID);
            sd.Add("P_IS_MAIN", store.IsMainBranch);
            sd.Add("P_MAIN_STORE_ID", store.MainStoreID);
            sd.Add("P_STORE_CODE", store.StoreCode);
            sd.Add("P_STORE_DESC", store.StoreDescription);
            sd.Add("P_STORE_INCHARGE", store.StoreInCharge);
            sd.Add("P_STORE_DETAILS", store.StoreDetails);
            sd.Add("P_IS_VALID", store.IsValid);
            sd.Add("P_STORE_ADDRESS", store.StoreAddress);
            sd.Add("P_USER", store.ModifiedBY);
            DataSet ds = DatabaseProvider.GetDatabaseProvider().SelectList("st_SaveStoreDetails", sd);
            return ds;
        }

        public static DataSet SaveStoreItemEntity(StoreItem item)
        {
            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            sd.Add("P_ITEM_ID", item.ItemID);
            sd.Add("P_STORE_ID", item.StoreDetails.StoreID);
            sd.Add("P_ITEM_CODE", item.ItemCode);
            sd.Add("P_ITEM_NAME", item.ItemName);
            sd.Add("P_ITEM_TYPE", item.ItemType);
            sd.Add("P_ITEM_SUBTYPE", item.ItemSubType);
            sd.Add("ITEM_DESC", item.ItemDescription);
            sd.Add("P_TOTAL_STOCK", item.TotalStock);
            sd.Add("P_IN_STOCK", item.InStock);
            sd.Add("P_ON_ORDER", item.OnOrder);
            sd.Add("P_ITEM_PRICE", item.ItemPrice);
            sd.Add("P_IS_VALID", item.IsValid);
            sd.Add("P_DO_ALERT", item.DoAlert);
            sd.Add("P_THRESHOLD", item.Threshold);
            sd.Add("P_IGST", item.IGST);
            sd.Add("P_CGST", item.CGST);
            sd.Add("P_SGST", item.SGST);
            sd.Add("P_USER", item.ModifiedBY);
            sd.Add("P_ITEM_UNITS", item.ItemUnits);
            sd.Add("P_HSN_CODE", item.HSNCode);
            sd.Add("P_ATTACHMENT_ID", item.AttachmentId);
            DataSet ds = DatabaseProvider.GetDatabaseProvider().SelectList("st_SaveStoreItem", sd);
            return ds;
        }

        public static DataSet SaveStoreItemDetailsEntity(StoreItemDetails item)
        {
            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            sd.Add("P_ITEM_DETAIL_ID", item.ItemDetailID);
            sd.Add("P_ITEM_ID", item.ItemID);
            sd.Add("P_SERIAL_NO", item.SerialNo);
            sd.Add("P_WARRANTY_NO", item.WarrantyNo);
            sd.Add("P_WARRANTY_DATE", item.WarrantyDate);
            sd.Add("P_GIN_ID", item.GinID);
            sd.Add("P_GRN_ID", item.GrnID);
            sd.Add("P_PO_ID", item.PoID);
            sd.Add("P_USER", item.UserID);
            DataSet ds = DatabaseProvider.GetDatabaseProvider().SelectList("st_SaveStoreItemDetails", sd);
            return ds;
        }

        public static DataSet SaveStoreItemReceiveEntity(StoreItemReceive item)
        {
            DataSet ds = new DataSet();
            foreach (var storeItem in item.StoreItems)
            {
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                sd.Add("P_GRN_ID", item.GRNID);
                sd.Add("P_GRN_CODE", item.GRNCode);
                sd.Add("P_ITEM_ID", storeItem.ItemID);
                sd.Add("P_INDENT_NO", item.IndentNo);
                sd.Add("P_PO_NO", item.PONo);
                sd.Add("P_SUPPLIER_NAME", item.SupplierName);
                sd.Add("P_RECEIVE_DATE", item.ReceiveDate);
                sd.Add("P_RECEIVE_LOCATION", item.ReceiveLocation);
                sd.Add("P_COST_CENTER", item.CostCenter);
                sd.Add("P_ORDER_QTY", storeItem.TotalStock);
                sd.Add("P_RECEIVED_QTY", storeItem.InStock);
                sd.Add("P_RECEIVED_BY", item.ReceivedBy);
                sd.Add("P_CHECKED_BY", item.CheckedBy);
                sd.Add("P_ITEM_CONDITION", item.ItemCondition);
                sd.Add("P_COMMENTS", item.Comments);
                sd.Add("P_PO_AMOUNT", item.PoAmount);
                sd.Add("P_ADVANCE_PAID", item.PaidAmount);
                sd.Add("P_PAYMENT_MODE", item.PaymentMode);
                sd.Add("P_INDENTER_NAME", item.IndenterName);
                sd.Add("P_GIN_CODE_LINK", item.GINCodeLink);
                sd.Add("P_USER", item.ModifiedBY);
                ds = DatabaseProvider.GetDatabaseProvider().SelectList("st_SaveStoreItemReceive", sd);
                Response response = new Response();
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
                {
                    response.ObjectID = ds.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToInt16(ds.Tables[0].Rows[0][0].ToString()) : -1;
                }

                if (storeItem.ItemDetails!=null && storeItem.ItemDetails.Length > 0)
                {
                    foreach(var itemDetails in storeItem.ItemDetails)
                    {
                        itemDetails.ItemID = storeItem.ItemID;
                        itemDetails.GrnID = response.ObjectID;
                        SaveStoreItemDetailsEntity(itemDetails);
                    }
                }
            }

            return ds;
        }

        public static DataSet SaveStoreItemIssueEntity(StoreItemIssue item)
        {
            DataSet ds = new DataSet();
            foreach (var storeItem in item.StoreItemDetails)
            {
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                sd.Add("P_GIN_ID", item.GINID);
                sd.Add("P_GIN_CODE", item.GINCode);
                sd.Add("P_ITEM_ID", storeItem.ItemID);
                sd.Add("P_INDENT_NO", item.IndentNo);
                sd.Add("P_SHIP_NAME", item.ShipName);
                sd.Add("P_SHIP_LINE1", item.ShipLine1);
                sd.Add("P_SHIP_CITY", item.ShipCity);
                sd.Add("P_SHIP_STATE", item.ShipState);
                sd.Add("P_SHIP_COUNTRY", item.ShipCountry);
                sd.Add("P_SHIP_ZIP", item.ShipZip);
                sd.Add("P_PHONE", item.Phone);
                sd.Add("P_ISSUE_DATE", item.IssueDate);
                sd.Add("P_ISSUE_LOCATION", item.IssueLocation);
                sd.Add("P_SHIP_MODE", item.ShipMode);
                sd.Add("P_SHIP_TRACK_NO", item.ShipTrackNo);
                sd.Add("P_SHIP_MODE_DETAILS", item.ShipModeDetails);
                sd.Add("P_ORDER_QTY", storeItem.TotalStock);
                sd.Add("P_SHIP_QTY", storeItem.InStock);
                sd.Add("P_COMMENTS", item.Comments);
                sd.Add("P_USER", item.ModifiedBY);
                ds = DatabaseProvider.GetDatabaseProvider().SelectList("st_SaveStoreItemIssue", sd);
                Response response = new Response();
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
                {
                    response.ObjectID = ds.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToInt16(ds.Tables[0].Rows[0][0].ToString()) : -1;
                }

                if (storeItem.ItemDetails != null && storeItem.ItemDetails.Length > 0)
                {
                    string itemDetailsIds = string.Join(",", storeItem.ItemDetails.Select(x => x.ItemDetailID.ToString()));
                    string query = string.Format("UPDATE StoreItemDetails SET GIN_ID = {0} WHERE ITEM_ID = {1} AND ITEM_DETAIL_ID IN ({2});", response.ObjectID, storeItem.ItemID, itemDetailsIds);
                    DatabaseProvider.GetDatabaseProvider().ExecuteQuery(query);
                }
            }

            return ds;
        }

        public static DataSet SaveStoreItemProp1(CustomProperty storeItemProp)
        {
            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            sd.Add("P_CUST_PROP_ID", storeItemProp.CustPropID);
            sd.Add("P_COMP_ID", storeItemProp.CompanyID);
            sd.Add("P_CUST_PROP_CODE", storeItemProp.CustPropCode);
            sd.Add("P_CUST_PROP_DESC", storeItemProp.CustPropDesc);
            sd.Add("P_CUST_PROP_TYPE", storeItemProp.CustPropType);
            sd.Add("P_CUST_PROP_DEFAULT", storeItemProp.CustPropDefault);
            sd.Add("P_USER", storeItemProp.ModifiedBY);

            DataSet ds = DatabaseProvider.GetDatabaseProvider().SelectList("st_SaveItemCustProp", sd);
            return ds;
        }

        public static DataSet SaveStoreItemPropValue1(CustomProperty storeItemProp)
        {
            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            sd.Add("P_CUST_PROP_ID", storeItemProp.CustPropID);
            sd.Add("P_ITEM_ID", storeItemProp.EntityID);
            sd.Add("P_CUST_PROP_VALUE", storeItemProp.CustPropValue);
            sd.Add("P_USER", storeItemProp.ModifiedBY);
            DataSet ds = DatabaseProvider.GetDatabaseProvider().SelectList("st_SaveItemCustPropValues", sd);

            return ds;
        }
    }
}