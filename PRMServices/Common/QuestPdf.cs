﻿using Newtonsoft.Json;
using QuestPDF.Fluent;
using QuestPDF.Helpers;
using QuestPDF.Infrastructure;
using System.Collections.Generic;


public class PurchaseOrderDocument : IDocument
{
    public Dictionary<string, string> PdfData { get; set; }


    public Dictionary<string, List<string>> PdfItems { get; set; }
    public DocumentMetadata GetMetadata() => DocumentMetadata.Default;

    public DocumentSettings GetSettings()
    {
        return new DocumentSettings();
    }

    public void Compose(IDocumentContainer container)
    {
        container
            .Page(page =>
            {
                page.Margin(35);
                page.Header().PaddingBottom(10).Element(ComposeHeader);
                page.Content().Element(ComposeContent);
                page.Footer().AlignCenter().Text(x =>
                {
                    x.CurrentPageNumber();
                    x.Span(" / ");
                    x.TotalPages();
                });

                page.Background().Padding(10, Unit.Millimetre).Element(backgroundContainer =>
                {
                    backgroundContainer.Border(0.5f, Unit.Millimetre).BorderColor(Colors.Black);
                });
            });
    }



    //void ComposeHeader(IContainer container)
    //{
    //    container.Row(row =>
    //    {
    //        row.RelativeItem().Column(column =>
    //        {
    //            column.Item()
    //                .Text($"Colliers")
    //                .FontSize(24).Bold().FontColor(Colors.Black).FontFamily("Trebuchet");
    //        });

    //        //row.ConstantItem(100).Height(50).Placeholder();
    //        row.ConstantItem(100).Height(50).Image("js/resources/images/client_logo.png").FitArea();

    //    });


    //}

     void ComposeHeader(IContainer container)
    {
        container.Column(column =>
        {
            // Header Row
            column.Item().PaddingBottom(10).Row(row =>
            {
                row.RelativeItem().Column(col =>
                {
                    col.Item()
                        .Text("Colliers")
                        .FontSize(24).Bold()
                        .FontColor(Colors.Black)
                        .FontFamily("Helvetica"); // Using a common font
                    col.Item()
                        .Text(PdfData["PO_TYPE"])
                        .FontSize(10).Bold()
                        .FontColor(Colors.Black)
                        .FontFamily("Helvetica"); // Using a common font
                });


                row.ConstantItem(100).Height(50).Image("js/resources/images/client_logo.png").FitArea();

            });// Adds padding below the row


            // Table Section
            column.Item().Table(table =>
            {
                table.ColumnsDefinition(columns =>
                {
                    columns.RelativeColumn();
                    columns.RelativeColumn();
                    columns.RelativeColumn();
                    columns.RelativeColumn();
                    columns.RelativeColumn();
                    columns.RelativeColumn();
                    columns.RelativeColumn();
                    columns.RelativeColumn();
                });

                if (PdfData != null && PdfData.ContainsKey("PO_PDF_JSON"))
                {
                    var testData = PdfData;
                    var parsedData = JsonConvert.DeserializeObject<Dictionary<string, string>>(PdfData["PO_PDF_JSON"]);
                    PdfData = parsedData;
                    PdfData["SHIP_TO_ADDRESS"] = testData["SHIP_TO_ADDRESS"];
                    PdfData["BILL_TO_ADDRESS"] = testData["BILL_TO_ADDRESS"];

                }

                void AddHeaderCell(string text, int key)
                {
                    string cellText = key == 1 ? text : (PdfData.ContainsKey(text) ? PdfData[text] : "");
                    table.Cell()
                        .Border(1)
                        .BorderColor(Colors.Grey.Lighten2)
                        .Background(Color.FromRGB(5, 66, 140))
                        .Padding(3)
                        .Text(cellText)
                        .FontSize(8).FontColor(Color.FromRGB(255, 255, 255));
                }

                // Usage in your code
                AddHeaderCell("Vendor Regn No.:", 1);
                AddHeaderCell("VENDOR_CODE", 0);
                AddHeaderCell("PO No.:", 1);
                AddHeaderCell("PO_NUMBER", 0);
                AddHeaderCell("Project Code:", 1);
                AddHeaderCell("PROJ_ID", 0);
                AddHeaderCell("Date:", 1);
                AddHeaderCell("PO_DATE", 0);
            });
        });
    }


void ComposeContent(IContainer container)
    {
        container
            .Shrink()
            .Border(1) // Apply border around the entire table
            .BorderColor(Colors.Grey.Lighten2)
            .Background(Color.FromRGB(249, 249, 249))
            .Table(table =>
            {
                table.ColumnsDefinition(columns =>
                {
                    //columns.ConstantColumn(100);
                    columns.RelativeColumn();
                    columns.RelativeColumn();
                    columns.RelativeColumn();
                    columns.RelativeColumn();
                    columns.RelativeColumn();
                    columns.RelativeColumn();
                    columns.RelativeColumn();
                    columns.RelativeColumn();

                });

                table.ExtendLastCellsToTableBottom();

                //table.Cell().Border(1).BorderColor(Colors.Grey.Lighten2).Padding(3).Text("Vendor Regn No.:").FontSize(8);
                //table.Cell().Border(1).BorderColor(Colors.Grey.Lighten2).Padding(3).Text(PdfData.ContainsKey("VENDOR_CODE") ? PdfData["VENDOR_CODE"] : "").FontSize(8);
                //table.Cell().Border(1).BorderColor(Colors.Grey.Lighten2).Padding(3).Text("PO No.:").FontSize(8);
                //table.Cell().Border(1).BorderColor(Colors.Grey.Lighten2).Padding(3).Text(PdfData.ContainsKey("PO_NUMBER") ? PdfData["PO_NUMBER"] : "").FontSize(8);
                //table.Cell().Border(1).BorderColor(Colors.Grey.Lighten2).Padding(3).Text("Project Code:").FontSize(8);
                //table.Cell().Border(1).BorderColor(Colors.Grey.Lighten2).Padding(3).Text(PdfData.ContainsKey("PROJ_ID") ? PdfData["PROJ_ID"] : "").FontSize(8);
                //table.Cell().Border(1).BorderColor(Colors.Grey.Lighten2).Padding(3).Text("Date:").FontSize(8);
                //table.Cell().Border(1).BorderColor(Colors.Grey.Lighten2).Padding(3).Text(PdfData.ContainsKey("PO_DATE") ? PdfData["PO_DATE"] : "").FontSize(8);



                //table.Cell().ColumnSpan(8).Border(1).BorderColor(Colors.Grey.Lighten2).Background(Color.FromRGB(5,66,140,255)).Padding(3).Text(" A.ClientDetails").FontSize(8).FontColor(Color.FromRGB(255, 255, 255));
                //table.Cell().ColumnSpan(3).Border(1).BorderColor(Colors.Grey.Lighten2).Background(Color.FromRGB(223, 239, 215)).Padding(3).Text("Project Name").FontSize(8).Bold();
                //table.Cell().ColumnSpan(5).Border(1).BorderColor(Colors.Grey.Lighten2).Padding(3).Text(PdfData.ContainsKey("PROJECT_NAME") ? PdfData["PROJECT_NAME"] : "").FontSize(8);
                //table.Cell().ColumnSpan(3).Border(1).BorderColor(Colors.Grey.Lighten2).Background(Color.FromRGB(223, 239, 215)).Padding(3).Text("Legal Entity Name").FontSize(8).Bold();
                //table.Cell().ColumnSpan(5).Border(1).BorderColor(Colors.Grey.Lighten2).Padding(3).Text(PdfData.ContainsKey("CLIENT_NAME") ? PdfData["CLIENT_NAME"] : "").FontSize(8);
                //table.Cell().ColumnSpan(3).Border(1).BorderColor(Colors.Grey.Lighten2).Background(Color.FromRGB(223, 239, 215)).Padding(3).Text("Address").FontSize(8).Bold();
                //table.Cell().ColumnSpan(1).Border(1).BorderColor(Colors.Grey.Lighten2).Padding(3).Text(PdfData.ContainsKey("CLIENT_ADDRESS") ? PdfData["CLIENT_ADDRESS"] : "").FontSize(8);
                //table.Cell().ColumnSpan(1).Border(1).BorderColor(Colors.Grey.Lighten2).Background(Color.FromRGB(223, 239, 215)).Padding(3).Text("GSTIN").FontSize(8).Bold();
                //table.Cell().ColumnSpan(1).Border(1).BorderColor(Colors.Grey.Lighten2).Padding(3).Text(PdfData.ContainsKey("CLIENT_GST") ? PdfData["CLIENT_GST"] : "").FontSize(8);
                //table.Cell().ColumnSpan(1).Border(1).BorderColor(Colors.Grey.Lighten2).Background(Color.FromRGB(223, 239, 215)).Padding(3).Text("PAN").FontSize(8).Bold();
                //table.Cell().ColumnSpan(1).Border(1).BorderColor(Colors.Grey.Lighten2).Padding(3).Text(PdfData.ContainsKey("CLIENT_PAN") ? PdfData["CLIENT_PAN"] : "").FontSize(8);

                //table.Cell().ColumnSpan(8).Border(1).BorderColor(Colors.Grey.Lighten2).Background(Color.FromRGB(5,66,140,255)).Padding(3).Text("B. Colliers").FontSize(8).FontColor(Color.FromRGB(255, 255, 255));
                //table.Cell().ColumnSpan(3).Border(1).BorderColor(Colors.Grey.Lighten2).Background(Color.FromRGB(223, 239, 215)).Padding(3).Text("Legal Entity Name").FontSize(8).Bold();
                //table.Cell().ColumnSpan(5).Border(1).BorderColor(Colors.Grey.Lighten2).Padding(3).Text(PdfData.ContainsKey("CONTRACT_LEGAL_ENTITY") ? PdfData["CONTRACT_LEGAL_ENTITY"] : "").FontSize(8);
                //table.Cell().ColumnSpan(3).Border(1).BorderColor(Colors.Grey.Lighten2).Background(Color.FromRGB(223, 239, 215)).Padding(3).Text("Address").FontSize(8).Bold();
                //table.Cell().ColumnSpan(5).Border(1).BorderColor(Colors.Grey.Lighten2).Padding(3).Text(PdfData.ContainsKey("CONTRACT_ADDR") ? PdfData["CONTRACT_ADDR"] : "").FontSize(8);
                //table.Cell().ColumnSpan(3).Border(1).BorderColor(Colors.Grey.Lighten2).Background(Color.FromRGB(223, 239, 215)).Padding(3).Text("SPOC email address").FontSize(8).Bold();
                //table.Cell().ColumnSpan(1).Border(1).BorderColor(Colors.Grey.Lighten2).Padding(3).Text(PdfData.ContainsKey("CONTRACT_SPOC_EMAIL") ? PdfData["CONTRACT_SPOC_EMAIL"] : "").FontSize(8);
                //table.Cell().ColumnSpan(1).Border(1).BorderColor(Colors.Grey.Lighten2).Background(Color.FromRGB(223, 239, 215)).Padding(3).Text("GSTIN").FontSize(8).Bold();
                //table.Cell().ColumnSpan(1).Border(1).BorderColor(Colors.Grey.Lighten2).Padding(3).Text(PdfData.ContainsKey("CONTRACT_GST") ? PdfData["CONTRACT_GST"] : "").FontSize(8);
                //table.Cell().ColumnSpan(1).Border(1).BorderColor(Colors.Grey.Lighten2).Background(Color.FromRGB(223, 239, 215)).Padding(3).Text("PAN").FontSize(8).Bold();
                //table.Cell().ColumnSpan(1).Border(1).BorderColor(Colors.Grey.Lighten2).Padding(3).Text(PdfData.ContainsKey("CONTRACT_PAN") ? PdfData["CONTRACT_PAN"] : "").FontSize(8);

                //table.Cell().ColumnSpan(8).Border(1).BorderColor(Colors.Grey.Lighten2).Background(Color.FromRGB(5,66,140,255)).Padding(3).Text("C. Vendor Details").FontSize(8).FontColor(Color.FromRGB(255, 255, 255));
                //table.Cell().ColumnSpan(3).Border(1).BorderColor(Colors.Grey.Lighten2).Background(Color.FromRGB(223, 239, 215)).Padding(3).Text("Legal Entity Name").FontSize(8).Bold();
                //table.Cell().ColumnSpan(5).Border(1).BorderColor(Colors.Grey.Lighten2).Padding(3).Text(PdfData.ContainsKey("VENDOR_COMPANY") ? PdfData["VENDOR_COMPANY"] : "").FontSize(8);
                //table.Cell().ColumnSpan(3).Border(1).BorderColor(Colors.Grey.Lighten2).Background(Color.FromRGB(223, 239, 215)).Padding(3).Text("Address").FontSize(8).Bold();
                //table.Cell().ColumnSpan(5).Border(1).BorderColor(Colors.Grey.Lighten2).Padding(3).Text(PdfData.ContainsKey("VENDOR_ADDRESS") ? PdfData["VENDOR_ADDRESS"] : "").FontSize(8);
                //table.Cell().ColumnSpan(3).Border(1).BorderColor(Colors.Grey.Lighten2).Background(Color.FromRGB(223, 239, 215)).Padding(3).Text("Vendor email address").FontSize(8).Bold();
                //table.Cell().ColumnSpan(1).Border(1).BorderColor(Colors.Grey.Lighten2).Padding(3).Text(PdfData.ContainsKey("VENDOR_PRIMARY_EMAIL") ? PdfData["VENDOR_PRIMARY_EMAIL"] : "").FontSize(8);
                //table.Cell().ColumnSpan(1).Border(1).BorderColor(Colors.Grey.Lighten2).Background(Color.FromRGB(223, 239, 215)).Padding(3).Text("GSTIN").FontSize(8).Bold();
                //table.Cell().ColumnSpan(1).Border(1).BorderColor(Colors.Grey.Lighten2).Padding(3).Text(PdfData.ContainsKey("GST_NUMBER") ? PdfData["GST_NUMBER"] : "").FontSize(8);
                //table.Cell().ColumnSpan(1).Border(1).BorderColor(Colors.Grey.Lighten2).Background(Color.FromRGB(223, 239, 215)).Padding(3).Text("PAN").FontSize(8).Bold();
                //table.Cell().ColumnSpan(1).Border(1).BorderColor(Colors.Grey.Lighten2).Padding(3).Text(PdfData.ContainsKey("VENDOR_PAN") ? PdfData["VENDOR_PAN"] : "").FontSize(8);

                //table.Cell().ColumnSpan(8).Border(1).BorderColor(Colors.Grey.Lighten2).Background(Color.FromRGB(5,66,140,255)).Padding(3).Text("D. Delivery / Invoice Details").FontSize(8).FontColor(Color.FromRGB(255, 255, 255));
                //table.Cell().ColumnSpan(3).Border(1).BorderColor(Colors.Grey.Lighten2).Background(Color.FromRGB(223, 239, 215)).Padding(3).Text("Ship To:").FontSize(8).Bold();
                //table.Cell().ColumnSpan(3).Border(1).BorderColor(Colors.Grey.Lighten2).Background(Color.FromRGB(223, 239, 215)).Padding(3).Text("Bill To: (Shown on Invoice)").FontSize(8).Bold();
                //table.Cell().ColumnSpan(2).Border(1).BorderColor(Colors.Grey.Lighten2).Background(Color.FromRGB(223, 239, 215)).Padding(3).Text("Send Orginal Invoice to:").FontSize(8).Bold();
                //table.Cell().ColumnSpan(3).Border(1).BorderColor(Colors.Grey.Lighten2).Padding(3).Text(PdfData.ContainsKey("SHIP_TO_ADDRESS") ? PdfData["SHIP_TO_ADDRESS"] : "").FontSize(8);
                //table.Cell().ColumnSpan(3).Border(1).BorderColor(Colors.Grey.Lighten2).Padding(3).Text(PdfData.ContainsKey("BILL_TO_ADDRESS") ? PdfData["BILL_TO_ADDRESS"] : "").FontSize(8);
                //table.Cell().ColumnSpan(2).Border(1).BorderColor(Colors.Grey.Lighten2).Padding(3).Text(PdfData.ContainsKey("INVOICE_TO_ADDRESS") ? PdfData["INVOICE_TO_ADDRESS"] : "").FontSize(8);

                void AddClientCell(string text, int key,uint colspan)
                {
                    string cellText = key == 1 ? text : (PdfData.ContainsKey(text) ? PdfData[text] : "");
                    if(text == "PO_VALUE_WORDS")
                    {
                        cellText = "Indian Rupee " + cellText + " Only.";
                    }
                    if(colspan == 8)
                    {
                        table.Cell()
                        .ColumnSpan(colspan)
                            .Border(1)
                            .BorderColor(Colors.Grey.Lighten2)
                            .Padding(3)
                            .Text(cellText)
                            .FontSize(8)
                            .Bold();
                    }
                    else
                    {
                        if (key == 1)
                        {
                            table.Cell()
                        .ColumnSpan(colspan)
                            .Border(1)
                            .BorderColor(Colors.Grey.Lighten2)
                            .Padding(3)
                            .Text(cellText)
                            .FontSize(8)
                            .Bold();
                        }
                        else
                        {
                            table.Cell()
                        .ColumnSpan(colspan)
                            .Border(1)
                            .BorderColor(Colors.Grey.Lighten2)
                            .Padding(3)
                            .Text(cellText)
                            .FontSize(8);

                        }
                    }
                    
                    
                }

                // Usage in your code
                AddClientCell("A.ClientDetails", 1,8);
                AddClientCell("Project Name", 1,3);
                AddClientCell("PROJECT_NAME", 0,5);
                AddClientCell("Legal Entity Name", 1,3);
                AddClientCell("CLIENT_NAME", 0,5);
                AddClientCell("Address:", 1,3);
                AddClientCell("CLIENT_ADDRESS", 0,1);
                AddClientCell("GSTIN:", 1,1);
                AddClientCell("CLIENT_GST", 0,1);
                AddClientCell("PAN:", 1, 1);
                AddClientCell("CLIENT_PAN", 0, 1);

                AddClientCell("B. Colliers", 1, 8);
                AddClientCell("Legal Entity Name", 1, 3);
                AddClientCell("CONTRACT_LEGAL_ENTITY", 0, 5);
                AddClientCell("Address", 1, 3);
                AddClientCell("CONTRACT_ADDR", 0, 5);
                AddClientCell("SPOC email address:", 1, 3);
                AddClientCell("CONTRACT_SPOC_EMAIL", 0, 1);
                AddClientCell("GSTIN:", 1, 1);
                AddClientCell("CONTRACT_GST", 0, 1);
                AddClientCell("PAN:", 1, 1);
                AddClientCell("CONTRACT_PAN", 0, 1);

                AddClientCell("C. Vendor Details", 1, 8);
                AddClientCell("Legal Entity Name", 1, 3);
                AddClientCell("VENDOR_COMPANY", 0, 5);
                AddClientCell("Address", 1, 3);
                AddClientCell("VENDOR_ADDRESS", 0, 5);
                AddClientCell("Vendor email address:", 1, 3);
                AddClientCell("VENDOR_PRIMARY_EMAIL", 0, 1);
                AddClientCell("GSTIN:", 1, 1);
                AddClientCell("GST_NUMBER", 0, 1);
                AddClientCell("PAN:", 1, 1);
                AddClientCell("VENDOR_PAN", 0, 1);

                AddClientCell("D. Delivery / Invoice Details", 1, 8);
                AddClientCell("Ship To:", 1, 3);
                AddClientCell("Bill To: (Shown on Invoice)", 1, 3);
                AddClientCell("Send Orginal Invoice to:", 1, 2);
                AddClientCell("SHIP_TO_ADDRESS", 0, 3);
                AddClientCell("BILL_TO_ADDRESS", 0, 3);
                AddClientCell("INVOICE_TO_ADDRESS", 0, 2);

                //table.Cell().ColumnSpan(8).Border(1).BorderColor(Colors.Grey.Lighten2).Background(Color.FromRGB(5,66,140,255)).Padding(3).Text("E. Item / Services to be Procured (Scope of Work)").FontSize(8).FontColor(Color.FromRGB(255, 255, 255));
                //table.Cell().ColumnSpan(1).Border(1).BorderColor(Colors.Grey.Lighten2).Background(Color.FromRGB(223, 239, 215)).Padding(3).Text("Item").FontSize(8).Bold().AlignCenter();
                //table.Cell().ColumnSpan(2).Border(1).BorderColor(Colors.Grey.Lighten2).Background(Color.FromRGB(223, 239, 215)).Padding(3).Text("Description ").FontSize(8).Bold().AlignCenter();
                //table.Cell().ColumnSpan(1).Border(1).BorderColor(Colors.Grey.Lighten2).Background(Color.FromRGB(223, 239, 215)).Padding(3).Text("Delivery Date").FontSize(8).Bold().AlignCenter();
                //table.Cell().ColumnSpan(1).Border(1).BorderColor(Colors.Grey.Lighten2).Background(Color.FromRGB(223, 239, 215)).Padding(3).Text("Unit").FontSize(8).Bold().AlignCenter();
                //table.Cell().ColumnSpan(1).Border(1).BorderColor(Colors.Grey.Lighten2).Background(Color.FromRGB(223, 239, 215)).Padding(3).Text("Quantity").FontSize(8).Bold().AlignCenter();
                //table.Cell().ColumnSpan(1).Border(1).BorderColor(Colors.Grey.Lighten2).Background(Color.FromRGB(223, 239, 215)).Padding(3).Text("Rate").FontSize(8).Bold().AlignCenter();
                //table.Cell().ColumnSpan(1).Border(1).BorderColor(Colors.Grey.Lighten2).Background(Color.FromRGB(223, 239, 215)).Padding(3).Text("Total Amount").FontSize(8).Bold().AlignCenter();



                void AddItemCell(string text, int key, uint colspan)
                {
                    string cellText = text;
                    if (colspan == 8)
                    {
                        table.Cell()
                        .ColumnSpan(colspan)
                            .Border(1)
                            .BorderColor(Colors.Grey.Lighten2)
                            .Padding(3)
                            .Text(cellText)
                            .FontSize(8).Bold();
                    }
                    else
                    {
                        if (key == 1)
                        {
                            table.Cell()
                        .ColumnSpan(colspan)
                            .Border(1)
                            .BorderColor(Colors.Grey.Lighten2)
                            .Padding(3)
                            .Text(cellText)
                            .FontSize(8)
                            .Bold().AlignCenter();
                        }
                        else
                        {
                            table.Cell()
                        .ColumnSpan(colspan)
                            .Border(1)
                            .BorderColor(Colors.Grey.Lighten2)
                            .Padding(3)
                            .Text(cellText)
                            .FontSize(8).AlignCenter();

                        }
                    }


                }


                AddItemCell("E. Item / Services to be Procured (Scope of Work)", 1, 8);
                AddItemCell("Item:", 1, 1);
                AddItemCell("Description", 1, 2);
                AddItemCell("Delivery Date", 1, 1);
                AddItemCell("Unit", 1, 1);
                AddItemCell("Quantity", 1, 1);
                AddItemCell("Rate", 1, 1);
                AddItemCell("Total Amount", 1, 1);


                
                foreach (var item in PdfItems)
                {
                    AddItemCell(item.Value[0].ToString(), 0, 1);
                    AddItemCell(item.Value[1].ToString(), 0, 2);
                    AddItemCell(item.Value[2].ToString(), 0, 1);
                    AddItemCell(item.Value[3].ToString(), 0, 1);
                    AddItemCell(item.Value[4].ToString(), 0, 1);
                    AddItemCell(item.Value[5].ToString(), 0, 1);
                    AddItemCell(item.Value[6].ToString(), 0, 1);

                    //table.Cell().ColumnSpan(1).Border(1).BorderColor(Colors.Grey.Lighten2).Padding(3).Text(item.Value[0].ToString()).FontSize(8).AlignCenter(); // Item
                    //table.Cell().ColumnSpan(2).Border(1).BorderColor(Colors.Grey.Lighten2).Padding(3).Text(item.Value[1].ToString()).FontSize(8).AlignCenter(); // Description
                    //table.Cell().ColumnSpan(1).Border(1).BorderColor(Colors.Grey.Lighten2).Padding(3).Text(item.Value[2].ToString()).FontSize(8).AlignCenter(); // Delivery Date
                    //table.Cell().ColumnSpan(1).Border(1).BorderColor(Colors.Grey.Lighten2).Padding(3).Text(item.Value[3].ToString()).FontSize(8).AlignCenter(); // Unit
                    //table.Cell().ColumnSpan(1).Border(1).BorderColor(Colors.Grey.Lighten2).Padding(3).Text(item.Value[4].ToString()).FontSize(8).AlignCenter(); // Quantity
                    //table.Cell().ColumnSpan(1).Border(1).BorderColor(Colors.Grey.Lighten2).Padding(3).Text(item.Value[5].ToString()).FontSize(8).AlignCenter(); // Rate
                    //table.Cell().ColumnSpan(1).Border(1).BorderColor(Colors.Grey.Lighten2).Padding(3).Text(item.Value[6].ToString()).FontSize(8).AlignCenter(); // Total Amount

                }

                table.Cell().ColumnSpan(6).Border(1).BorderColor(Colors.Grey.Lighten2).Padding(3).Text("Basic Total:").FontSize(8).Bold().AlignRight();
                table.Cell().ColumnSpan(2).Border(1).BorderColor(Colors.Grey.Lighten2).Padding(3)
                .Text($"₹ {(PdfData.ContainsKey("PO_VALUE_WITHOUTTAX") ? PdfData["PO_VALUE_WITHOUTTAX"] : "")}")
                .FontSize(8).AlignRight();

                table.Cell().ColumnSpan(6).Border(1).BorderColor(Colors.Grey.Lighten2).Padding(3).Text("Applicable GST:").FontSize(8).Bold().AlignRight();
                table.Cell().ColumnSpan(2).Border(1).BorderColor(Colors.Grey.Lighten2).Padding(3)
                .Text($"₹ {(PdfData.ContainsKey("PO_GST_VALUE") ? PdfData["PO_GST_VALUE"] : "")}")
                .FontSize(8).AlignRight();

                table.Cell().ColumnSpan(6).Border(1).BorderColor(Colors.Grey.Lighten2).Padding(3).Text("GRAND TOTAL:").FontSize(8).Bold().AlignRight();
                table.Cell().ColumnSpan(2).Border(1).BorderColor(Colors.Grey.Lighten2).Padding(3)
                .Text($"₹ {(PdfData.ContainsKey("PO_VALUE") ? PdfData["PO_VALUE"] : "")}")
                .FontSize(8).AlignRight();

                //table.Cell().ColumnSpan(8).Border(1).BorderColor(Colors.Grey.Lighten2).Background(Color.FromRGB(5,66,140,255)).Padding(3).Text("F. Amount Details").FontSize(8).FontColor(Color.FromRGB(255, 255, 255));
                //table.Cell().ColumnSpan(3).Border(1).BorderColor(Colors.Grey.Lighten2).Background(Color.FromRGB(223, 239, 215)).Padding(3).Text("Contract / Purchase Order Value including FULL GROSS tax.").FontSize(8).Bold();
                //table.Cell().ColumnSpan(1).Border(1).BorderColor(Colors.Grey.Lighten2).Padding(3).Text(PdfData.ContainsKey("PO_VALUE") ? PdfData["PO_VALUE"] : "").FontSize(8);
                //table.Cell().ColumnSpan(1).Border(1).BorderColor(Colors.Grey.Lighten2).Padding(3).Text("Amount in words:").FontSize(8);
                //table.Cell().ColumnSpan(3).Border(1).BorderColor(Colors.Grey.Lighten2).Padding(3).Text(PdfData.ContainsKey("PO_VALUE_WORDS") ? PdfData["PO_VALUE_WORDS"] : "").FontSize(8);

                AddClientCell("F. Amount Details", 1, 8);
                AddClientCell("Contract / Purchase Order Value including FULL GROSS tax.", 1, 3);
                AddClientCell("PO_VALUE", 0, 1);
                AddClientCell("Amount in words:", 1, 1);
                AddClientCell("PO_VALUE_WORDS", 0, 3);

                AddClientCell("G. Terms and Conditions are as follows", 1, 8);
                AddItemCell("Item", 1, 2);
                AddItemCell("Title", 1, 3);
                AddItemCell("Description", 1, 3);
                AddItemCell("1", 1, 2);
                AddClientCell("Form of Contract(e.g.purchase order, contract etc)", 1, 3);
                AddClientCell("PO_TYPE_DESC", 0, 3);
                AddItemCell("2", 1, 2);
                AddClientCell("Contract Start / Order Date (DD/MM/YY)", 1, 3);
                AddClientCell("poContractStartDate", 0, 3);
                AddItemCell("3", 1, 2);
                AddClientCell("Order Delivery Date (DD/MM/YY)", 1, 3);
                AddClientCell("poOrderDeliveryDate", 0, 3);
                AddItemCell("4", 1, 2);
                AddClientCell("Project Milestone", 1, 3);
                AddClientCell("RESPONSE1", 0, 3);
                AddItemCell("5", 1, 2);
                AddClientCell("Liquidated and Ascertained Damages", 1, 3);
                AddClientCell("RESPONSE2", 0, 3);
                AddItemCell("6", 1, 2);
                AddClientCell("Defects Liability Period / Warranty", 1, 3);
                AddClientCell("RESPONSE3", 0, 3);
                AddItemCell("7", 1, 2);
                AddClientCell("Retention", 1, 3);
                AddClientCell("RESPONSE4", 0, 3);
                AddItemCell("8", 1, 2);
                AddClientCell("Performance Bond", 1, 3);
                AddClientCell("RESPONSE5", 0, 3);
                AddItemCell("9", 1, 2);
                AddClientCell("Performance Bond", 1, 3);
                AddClientCell("RESPONSE6", 0, 3);
                AddItemCell("10", 1, 2);
                AddClientCell("Payment terms (down payments, milestones, % completion etc)", 1, 3);
                AddClientCell("RESPONSE7", 0, 3);
                AddItemCell("11", 1, 2);
                AddClientCell("Project Specific Terms and Conditions", 1, 3);
                AddClientCell("RESPONSE8", 0, 3);
                AddItemCell("12", 1, 2);
                AddClientCell("Termination", 1, 3);
                AddClientCell("RESPONSE9", 0, 3);
                AddItemCell("13", 1, 2);
                AddClientCell("Insurance of the Works", 1, 3);
                AddClientCell("RESPONSE10", 0, 3);
                AddItemCell("14", 1, 2);
                AddClientCell("Labour Compliances", 1, 3);
                AddClientCell("RESPONSE11", 0, 3);
                AddItemCell("15", 1, 2);
                AddClientCell("Compliances", 1, 3);
                AddClientCell("RESPONSE12", 0, 3);
                AddItemCell("16", 1, 2);
                AddClientCell("Professional Indemnity Liability Insurance(Insurance for any loss due to alleged neglect, error or omission of professional indemnity,for consultant or contractor’s designed element only)", 1, 3);
                AddClientCell("RESPONSE13", 0, 3);
                //table.Cell().ColumnSpan(8).Border(1).BorderColor(Colors.Grey.Lighten2).Background(Color.FromRGB(5,66,140,255)).Padding(3).Text("G. Terms and Conditions are as follows").FontSize(8).FontColor(Color.FromRGB(255, 255, 255));
                //table.Cell().ColumnSpan(2).Border(1).BorderColor(Colors.Grey.Lighten2).Background(Color.FromRGB(223, 239, 215)).Padding(3).Text("Item").FontSize(8).Bold();
                //table.Cell().ColumnSpan(3).Border(1).BorderColor(Colors.Grey.Lighten2).Background(Color.FromRGB(223, 239, 215)).Padding(3).Text("Title").FontSize(8).Bold();
                //table.Cell().ColumnSpan(3).Border(1).BorderColor(Colors.Grey.Lighten2).Background(Color.FromRGB(223, 239, 215)).Padding(3).Text("Description").FontSize(8).Bold();

                //table.Cell().ColumnSpan(2).Border(1).BorderColor(Colors.Grey.Lighten2).Background(Color.FromRGB(223, 239, 215)).Padding(3).Text("1").FontSize(8).Bold();
                //table.Cell().ColumnSpan(3).Border(1).BorderColor(Colors.Grey.Lighten2).Background(Color.FromRGB(223, 239, 215)).Padding(3).Text("Form of Contract (e.g. purchase order, contract etc)").FontSize(8).Bold();
                //table.Cell().ColumnSpan(3).Border(1).BorderColor(Colors.Grey.Lighten2).Padding(3).Text(PdfData.ContainsKey("PO_TYPE_DESC") ? PdfData["PO_TYPE_DESC"] : "").FontSize(8);
                //table.Cell().ColumnSpan(2).Border(1).BorderColor(Colors.Grey.Lighten2).Background(Color.FromRGB(223, 239, 215)).Padding(3).Text("2").FontSize(8).Bold();
                //table.Cell().ColumnSpan(3).Border(1).BorderColor(Colors.Grey.Lighten2).Background(Color.FromRGB(223, 239, 215)).Padding(3).Text("Contract Start / Order Date (DD/MM/YY)").FontSize(8).Bold();
                //table.Cell().ColumnSpan(3).Border(1).BorderColor(Colors.Grey.Lighten2).Padding(3).Text(PdfData.ContainsKey("poContractStartDate") ? PdfData["poContractStartDate"] : "").FontSize(8);
                //table.Cell().ColumnSpan(2).Border(1).BorderColor(Colors.Grey.Lighten2).Background(Color.FromRGB(223, 239, 215)).Padding(3).Text("3").FontSize(8).Bold();
                //table.Cell().ColumnSpan(3).Border(1).BorderColor(Colors.Grey.Lighten2).Background(Color.FromRGB(223, 239, 215)).Padding(3).Text("Order Delivery Date (DD/MM/YY)").FontSize(8).Bold();
                //table.Cell().ColumnSpan(3).Border(1).BorderColor(Colors.Grey.Lighten2).Padding(3).Text(PdfData.ContainsKey("poOrderDeliveryDate") ? PdfData["poOrderDeliveryDate"] : "").FontSize(8);
                //table.Cell().ColumnSpan(2).Border(1).BorderColor(Colors.Grey.Lighten2).Background(Color.FromRGB(223, 239, 215)).Padding(3).Text("4").FontSize(8).Bold();
                //table.Cell().ColumnSpan(3).Border(1).BorderColor(Colors.Grey.Lighten2).Background(Color.FromRGB(223, 239, 215)).Padding(3).Text("Project Milestone").FontSize(8).Bold();
                //table.Cell().ColumnSpan(3).Border(1).BorderColor(Colors.Grey.Lighten2).Padding(3).Text(PdfData.ContainsKey("RESPONSE1") ? PdfData["RESPONSE1"] : "").FontSize(8);
                //table.Cell().ColumnSpan(2).Border(1).BorderColor(Colors.Grey.Lighten2).Background(Color.FromRGB(223, 239, 215)).Padding(3).Text("5").FontSize(8).Bold();
                //table.Cell().ColumnSpan(3).Border(1).BorderColor(Colors.Grey.Lighten2).Background(Color.FromRGB(223, 239, 215)).Padding(3).Text("Liquidated and Ascertained Damages").FontSize(8).Bold();
                //table.Cell().ColumnSpan(3).Border(1).BorderColor(Colors.Grey.Lighten2).Padding(3).Text(PdfData.ContainsKey("RESPONSE2") ? PdfData["RESPONSE2"] : "").FontSize(8);
                //table.Cell().ColumnSpan(2).Border(1).BorderColor(Colors.Grey.Lighten2).Background(Color.FromRGB(223, 239, 215)).Padding(3).Text("6").FontSize(8).Bold();
                //table.Cell().ColumnSpan(3).Border(1).BorderColor(Colors.Grey.Lighten2).Background(Color.FromRGB(223, 239, 215)).Padding(3).Text("Defects Liability Period / Warranty").FontSize(8).Bold();
                //table.Cell().ColumnSpan(3).Border(1).BorderColor(Colors.Grey.Lighten2).Padding(3).Text(PdfData.ContainsKey("RESPONSE3") ? PdfData["RESPONSE3"] : "").FontSize(8);
                //table.Cell().ColumnSpan(2).Border(1).BorderColor(Colors.Grey.Lighten2).Background(Color.FromRGB(223, 239, 215)).Padding(3).Text("7").FontSize(8).Bold();
                //table.Cell().ColumnSpan(3).Border(1).BorderColor(Colors.Grey.Lighten2).Background(Color.FromRGB(223, 239, 215)).Padding(3).Text("Retention").FontSize(8).Bold();
                //table.Cell().ColumnSpan(3).Border(1).BorderColor(Colors.Grey.Lighten2).Padding(3).Text(PdfData.ContainsKey("RESPONSE4") ? PdfData["RESPONSE4"] : "").FontSize(8);
                //table.Cell().ColumnSpan(2).Border(1).BorderColor(Colors.Grey.Lighten2).Background(Color.FromRGB(223, 239, 215)).Padding(3).Text("8").FontSize(8).Bold();
                //table.Cell().ColumnSpan(3).Border(1).BorderColor(Colors.Grey.Lighten2).Background(Color.FromRGB(223, 239, 215)).Padding(3).Text("Performance Bond").FontSize(8).Bold();
                //table.Cell().ColumnSpan(3).Border(1).BorderColor(Colors.Grey.Lighten2).Padding(3).Text(PdfData.ContainsKey("RESPONSE5") ? PdfData["RESPONSE5"] : "").FontSize(8);
                //table.Cell().ColumnSpan(2).Border(1).BorderColor(Colors.Grey.Lighten2).Background(Color.FromRGB(223, 239, 215)).Padding(3).Text("9").FontSize(8).Bold();
                //table.Cell().ColumnSpan(3).Border(1).BorderColor(Colors.Grey.Lighten2).Background(Color.FromRGB(223, 239, 215)).Padding(3).Text("Bank Guarantee (for advance payment)").FontSize(8).Bold();
                //table.Cell().ColumnSpan(3).Border(1).BorderColor(Colors.Grey.Lighten2).Padding(3).Text(PdfData.ContainsKey("RESPONSE6") ? PdfData["RESPONSE6"] : "").FontSize(8);
                //table.Cell().ColumnSpan(2).Border(1).BorderColor(Colors.Grey.Lighten2).Background(Color.FromRGB(223, 239, 215)).Padding(3).Text("10").FontSize(8).Bold();
                //table.Cell().ColumnSpan(3).Border(1).BorderColor(Colors.Grey.Lighten2).Background(Color.FromRGB(223, 239, 215)).Padding(3).Text("Payment terms (down payments, milestones, % completion etc)").FontSize(8).Bold();
                //table.Cell().ColumnSpan(3).Border(1).BorderColor(Colors.Grey.Lighten2).Padding(3).Text(PdfData.ContainsKey("RESPONSE7") ? PdfData["RESPONSE7"] : "").FontSize(8);
                //table.Cell().ColumnSpan(2).Border(1).BorderColor(Colors.Grey.Lighten2).Background(Color.FromRGB(223, 239, 215)).Padding(3).Text("11").FontSize(8).Bold();
                //table.Cell().ColumnSpan(3).Border(1).BorderColor(Colors.Grey.Lighten2).Background(Color.FromRGB(223, 239, 215)).Padding(3).Text("Project Specific Terms and Conditions").FontSize(8).Bold();
                //table.Cell().ColumnSpan(3).Border(1).BorderColor(Colors.Grey.Lighten2).Padding(3).Text(PdfData.ContainsKey("RESPONSE8") ? PdfData["RESPONSE8"] : "").FontSize(8);
                //table.Cell().ColumnSpan(2).Border(1).BorderColor(Colors.Grey.Lighten2).Background(Color.FromRGB(223, 239, 215)).Padding(3).Text("12").FontSize(8).Bold();
                //table.Cell().ColumnSpan(3).Border(1).BorderColor(Colors.Grey.Lighten2).Background(Color.FromRGB(223, 239, 215)).Padding(3).Text("Termination").FontSize(8).Bold();
                //table.Cell().ColumnSpan(3).Border(1).BorderColor(Colors.Grey.Lighten2).Padding(3).Text(PdfData.ContainsKey("RESPONSE9") ? PdfData["RESPONSE9"] : "").FontSize(8);
                //table.Cell().ColumnSpan(2).Border(1).BorderColor(Colors.Grey.Lighten2).Background(Color.FromRGB(223, 239, 215)).Padding(3).Text("13").FontSize(8).Bold();
                //table.Cell().ColumnSpan(3).Border(1).BorderColor(Colors.Grey.Lighten2).Background(Color.FromRGB(223, 239, 215)).Padding(3).Text("Insurance of the Works").FontSize(8).Bold();
                //table.Cell().ColumnSpan(3).Border(1).BorderColor(Colors.Grey.Lighten2).Padding(3).Text(PdfData.ContainsKey("RESPONSE10") ? PdfData["RESPONSE10"] : "").FontSize(8);
                //table.Cell().ColumnSpan(2).Border(1).BorderColor(Colors.Grey.Lighten2).Background(Color.FromRGB(223, 239, 215)).Padding(3).Text("14").FontSize(8).Bold();
                //table.Cell().ColumnSpan(3).Border(1).BorderColor(Colors.Grey.Lighten2).Background(Color.FromRGB(223, 239, 215)).Padding(3).Text("Labour Compliances").FontSize(8).Bold();
                //table.Cell().ColumnSpan(3).Border(1).BorderColor(Colors.Grey.Lighten2).Padding(3).Text(PdfData.ContainsKey("RESPONSE11") ? PdfData["RESPONSE11"] : "").FontSize(8);
                //table.Cell().ColumnSpan(2).Border(1).BorderColor(Colors.Grey.Lighten2).Background(Color.FromRGB(223, 239, 215)).Padding(3).Text("15").FontSize(8).Bold();
                //table.Cell().ColumnSpan(3).Border(1).BorderColor(Colors.Grey.Lighten2).Background(Color.FromRGB(223, 239, 215)).Padding(3).Text("Compliances").FontSize(8).Bold();
                //table.Cell().ColumnSpan(3).Border(1).BorderColor(Colors.Grey.Lighten2).Padding(3).Text(PdfData.ContainsKey("RESPONSE12") ? PdfData["RESPONSE12"] : "").FontSize(8);
                //table.Cell().ColumnSpan(2).Border(1).BorderColor(Colors.Grey.Lighten2).Background(Color.FromRGB(223, 239, 215)).Padding(3).Text("16").FontSize(8).Bold();
                //table.Cell().ColumnSpan(3).Border(1).BorderColor(Colors.Grey.Lighten2).Background(Color.FromRGB(223, 239, 215)).Padding(3).Text("Professional Indemnity Liability Insurance(Insurance for any loss due to alleged neglect, error or omission of professional indemnity,for consultant or contractor’s designed element only) ").FontSize(8).Bold();
                //table.Cell().ColumnSpan(3).Border(1).BorderColor(Colors.Grey.Lighten2).Padding(3).Text(PdfData.ContainsKey("RESPONSE13") ? PdfData["RESPONSE13"] : "").FontSize(8);

                table.Cell().ColumnSpan(8).Padding(3).Text("Supporting Bill of Quantities & Detailed Terms & Conditions documents attached").FontSize(8);
                table.Cell().ColumnSpan(2).Padding(3).Text("Signature").FontSize(8);
                table.Cell().ColumnSpan(3).Padding(3).Text("Signature").FontSize(8);
                table.Cell().ColumnSpan(3).Padding(3).Text("Signature").FontSize(8);
                table.Cell().ColumnSpan(2).Padding(3).Text("(Name)").FontSize(8);
                var name = PdfData.ContainsKey("NAME") ? PdfData["NAME"] : ""; // Capture the value outside the lambda
                var name1 = PdfData.ContainsKey("NAME1") ? PdfData["NAME1"] : ""; // Capture the value outside the lambda
                var vendorCompany = PdfData.ContainsKey("VENDOR_COMPANY") ? PdfData["VENDOR_COMPANY"] : ""; // Capture the value outside the lambda
                var desig = PdfData.ContainsKey("DESIG") ? PdfData["DESIG"] : ""; // Capture the value outside the lambda
                var desig1 = PdfData.ContainsKey("DESIG1") ? PdfData["DESIG1"] : ""; // Capture the value outside the lambda

                void AddSignatureCell(string text, uint colspan)
                {
                    table.Cell().ColumnSpan(colspan).Padding(3).Column(column =>
                    {
                        column.Item()
                            .Text(text) // Use the captured variable
                            .FontSize(8);

                        column.Item()
                            .Row(row =>
                            {
                                row.RelativeItem() // This ensures the line spans the full width of the column
                                    .Height(1) // Adjust the thickness of the line
                                    .Background(Colors.Black); // Adjust the color as needed
                        });
                    });
                }


                AddSignatureCell(name,3);
                AddSignatureCell(name1, 3);
                AddSignatureCell(vendorCompany, 2);
                AddSignatureCell(desig, 3);
                AddSignatureCell(desig1, 3);


                //table.Cell().ColumnSpan(3).Padding(3).Column(column =>
                //{
                //    column.Item()
                //        .Text(name) // Use the captured variable
                //        .FontSize(8);

                //    column.Item()
                //        .Row(row =>
                //        {
                //            row.RelativeItem() // This ensures the line spans the full width of the column
                //                .Height(1) // Adjust the thickness of the line
                //                .Background(Colors.Black); // Adjust the color as needed
                //        });
                //});
                //table.Cell().ColumnSpan(3).Padding(3).Column(column =>
                //{
                //    column.Item()
                //        .Text(name1) // Use the captured variable
                //        .FontSize(8);

                //    column.Item()
                //        .Row(row =>
                //        {
                //            row.RelativeItem() // This ensures the line spans the full width of the column
                //                .Height(1) // Adjust the thickness of the line
                //                .Background(Colors.Black); // Adjust the color as needed
                //        });
                //});
                //table.Cell().ColumnSpan(2).Padding(3).Column(column =>
                //{
                //    column.Item()
                //        .Text(vendorCompany) // Use the captured variable
                //        .FontSize(8);

                //    column.Item()
                //        .Row(row =>
                //        {
                //            row.RelativeItem() // This ensures the line spans the full width of the column
                //                .Height(1) // Adjust the thickness of the line
                //                .Background(Colors.Black); // Adjust the color as needed
                //        });
                //});
                //table.Cell().ColumnSpan(3).Padding(3).Column(column =>
                //{
                //    column.Item()
                //        .Text(desig) // Use the captured variable
                //        .FontSize(8);

                //    column.Item()
                //        .Row(row =>
                //        {
                //            row.RelativeItem() // This ensures the line spans the full width of the column
                //                .Height(1) // Adjust the thickness of the line
                //                .Background(Colors.Black); // Adjust the color as needed
                //        });
                //});
                //table.Cell().ColumnSpan(3).Padding(3).Column(column =>
                //{
                //    column.Item()
                //        .Text(desig1) // Use the captured variable
                //        .FontSize(8);

                //    column.Item()
                //        .Row(row =>
                //        {
                //            row.RelativeItem() // This ensures the line spans the full width of the column
                //                .Height(1) // Adjust the thickness of the line
                //                .Background(Colors.Black); // Adjust the color as needed
                //        });
                //});
                table.Cell().ColumnSpan(8).Border(1).BorderColor(Colors.Grey.Lighten2).Padding(3).Text("Colliers International India Property Services Private Limited").FontSize(8).AlignRight();

              
            });
    }


}


