prmApp
    // =========================================================================
    // Auction Tiles
    // =========================================================================
    .controller('auctionsCtrl', function ($timeout, $state, $scope, $log, growlService, userService, auctionsService, $http, $rootScope, SignalRFactory, signalRHubName) {
        $scope.myHotAuctionsLoaded = false;
        /*$scope.myAuctionsLoaded = false;*/
        $scope.todaysAuctionsLoaded = false;
        $scope.myHotAuctionsLoaded = false;
        $scope.scheduledAuctionsLoaded = false;
        /*$scope.myAuctions = [];*/
        $scope.todaysAuctionsLoaded = false;
        $scope.scheduledAuctionsLoaded = false;
        $scope.hotLimit = 4;
        $scope.todaysLimit = 4;
        $scope.scheduledLimit = 4;
        $scope.loggedIn = userService.isLoggedIn();
        $scope.isVendor = (userService.getUserType() == "VENDOR") ? true : false;
        $log.info(userService.getUserType());
        $scope.scheduledAuctionsMessage = $scope.todayAuctionsMessage = $scope.hotAuctionsMessage = "Loading data, please wait.";

        $scope.reduceTime = function (timerId, time) {
            addCDSeconds(timerId, time);
        }

        $scope.stopBid = function (item) {
            $scope.myHotAuctions[0].TimeLeft = 60;
        }

        $scope.options = {
            loop: true,
            dots: false,
            margin: 10,
            autoplay: true,
            autoplayTimeout: 2000,
            autoplayHoverPause: true,
            responsive: {
                0: {
                    items: 1,
                },
                600: {
                    items: 3,
                },
                1000: {
                    items: 4,
                    loop: false
                }
            }
        };
        $scope.data = {
            userID: 1,
            sessionID: 1,
            section: "CURRENT"
        }
        $scope.myHotAuctions = [];
        $scope.scheduledAuctions = [];
        $scope.todaysAuctions = [];
        $log.info($scope.loggedIn);

        $log.info('trying to connect to service');
        var requirementHub = SignalRFactory('', signalRHubName);
        $log.info('connected to service');

        $scope.$on("$destroy", function () {
            $log.info('disconecting signalR')
            requirementHub.stop();
            $log.info('disconected signalR')
        });

        $scope.currentUserObj = userService.getUserObj();
        $log.info($scope.currentUserObj);

        requirementHub.on('checkRequirement', function (obj) {
            var hotreqIDs = _.map($scope.myHotAuctions, 'auctionID');
            var todaysreqIDs = _.map($scope.todaysAuctions, 'auctionID');
            var scheduledreqIDs = _.map($scope.scheduledAuctions, 'auctionID');
            if (hotreqIDs.indexOf(obj.requirementID) > -1 || todaysreqIDs.indexOf(obj.requirementID) > -1 || scheduledreqIDs.indexOf(obj.requirementID) > -1) {
                $scope.getMiniItems();
            }
        })

        $scope.getMiniItems = function () {
            if ($scope.loggedIn) {
                auctionsService.getauctions({ "action": "runningauctions", "section": "CURRENT", "userid": userService.getUserId(), "sessionid": userService.getUserToken(), "limit": $scope.hotLimit })
                    .then(function (response) {
                        $scope.myHotAuctions = response;
                        $log.info(response);
                        if ($scope.myHotAuctions.length > 0) {
                            $scope.myHotAuctionsLoaded = true;
                            //$(".hotauctions").owlCarousel(options);
                        } else {
                            $scope.myHotAuctionsLoaded = false;
                            $scope.hotAuctionsMessage = "There are no Negotiations running right now for you.";
                        }

                    });


                auctionsService.getDashboardStats({ "userid": userService.getUserId(), "sessionid": userService.getUserToken() })
                    .then(function (response) {
                        $scope.dashboardStats = response;

                        $scope.dashboardStats.totalTurnover = parseFloat($scope.dashboardStats.totalTurnover).toFixed(2);
                        //console.log($scope.dashboardStats.totalTurnover);
                        $scope.dashboardStats.totalTurnover = parseInt($scope.dashboardStats.totalTurnover);

                        if (userService.getUserType() == "CUSTOMER") {
                            $scope.dashboardStats.totalSaved = parseFloat($scope.dashboardStats.totalSaved).toFixed(2);
                            //console.log($scope.dashboardStats.totalSaved);
                            $scope.dashboardStats.totalSaved = parseInt($scope.dashboardStats.totalSaved);

                        }

                    });

                auctionsService.getauctions({ "section": "TODAYS", "userid": userService.getUserId(), "sessionid": userService.getUserToken(), "limit": $scope.todaysLimit })
                    .then(function (response) {
                        $scope.todaysAuctions = response;
                        $scope.todaysAuctionsLoaded = true;
                        if ($scope.todaysAuctions.length > 0) {
                            $scope.todaysAuctionsLoaded = true;
                            //$(".todayauctions").owlCarousel(options);
                        } else {
                            $scope.todaysAuctionsLoaded = false;
                            $scope.todayAuctionsMessage = "There are no Negotiations scheduled today for you.";
                        }
                    });
                auctionsService.getauctions({ "section": "SCHEDULED", "userid": userService.getUserId(), "sessionid": userService.getUserToken(), "limit": $scope.scheduledLimit })
                    .then(function (response) {
                        $scope.scheduledAuctions = response;
                        $scope.scheduledAuctionsLoaded = true;
                        if ($scope.scheduledAuctions.length > 0) {
                            $scope.scheduledAuctionsLoaded = true;
                            // $(".scheduledauctions").owlCarousel(options);
                        } else {
                            $scope.scheduledAuctionsLoaded = false;
                            $scope.scheduledAuctionsMessage = "There are no Negotiations scheduled for you in the future.";
                        }
                    });
            }
        }

        $scope.getMiniItems();

        // var intervalPromise = window.setInterval(function(){
        //     if(window.location.hash == "#/home"){
        //         $scope.getMiniItems();
        //     }            
        // }, 10000);

        $scope.addItem = function () {
            var obj = {
                auctionTimerId: 1000,
                title: "test",
                price: 11244354,
                bids: 24,
                auctionEnds: '123465'
            }
            $scope.myHotAuctions.push(obj);
        }

        $scope.makeaBid1 = function () {
            var bidPrice = $("#makebidvalue").val();
            //$log.info($scope.auctionItem.minPrice);
            $log.info(bidPrice + "::Auction cntrl");

            if (bidPrice == "") {
                $scope.bidPriceEmpty = true;
                $scope.bidPriceValidation = false;
                return false;
            } else if (!isNaN($scope.auctionItem.minPrice) && $scope.auctionItem.minPrice > 0 && bidPrice >= $scope.auctionItem.minPrice) {
                $scope.bidPriceValidation = true;
                $scope.bidPriceEmpty = false;
                return false;
            } else {
                $scope.bidPriceValidation = false;
                $scope.bidPriceEmpty = false;
            }
            if (($scope.bidAttachementName == "" || $scope.bidAttachement.length == 0) && $scope.quotationStatus == false) {
                $scope.bidAttachementValidation = true;
                return false;
            } else {
                $scope.bidAttachementValidation = false;
            }
            //return false;
            //$scope.auctionItem.minPrice
            var params = {};
            params.reqID = parseInt(id);
            params.sessionID = userService.getUserToken();
            params.userID = parseInt(userService.getUserId());
            params.price = parseFloat(bidPrice);
            params.quotation = $scope.bidAttachement;
            params.quotationName = $scope.bidAttachementName;
            //$log.info(params);
            //var parties = params.reqID + "$" + params.userID + "$" + params.price + "$" + params.quotation + "$" + params.quotationName + "$" + params.sessionID;
            requirementHub.invoke('MakeBid', params, function (req) {
                //auctionsService.makeabid(params).then(function(req){
                if (req.errorMessage == '') {
                    swal("Thanks !", "Your bidding process has been successfully updated", "success");
                    $("#makebidvalue").val("");
                    $(".removeattachedquotes").trigger('click');
                } else {
                    swal("Error!", req.errorMessage, "error");
                }
            });
        }

        $scope.reduceTime = function (timerId, time) {
            addCDSeconds(timerId, time);
        }

        $scope.stopBid = function (item) {
            $scope.myHotAuctions[0].TimeLeft = 60;
        }

        $scope.changeScheduledAuctionsLimit = function () {
            $scope.scheduledLimit = 8;
            $scope.getMiniItems();
        }

        $scope.changeHotAuctionsLimit = function () {
            $scope.hotLimit = 8;
            $scope.getMiniItems();
        }

        $scope.changeTodaysAuctionsLimit = function () {
            $scope.todaysLimit = 8;
            $scope.getMiniItems();
        }

        $scope.totalItems = 0;
        $scope.totalLeads = 0;
        $scope.currentPage = 1;
        $scope.itemsPerPage = 5;

        $scope.currentPage2 = 1;
        $scope.itemsPerPage2 = 5;

        $scope.maxSize = 5; //Number of pager buttons to show

        $scope.setPage = function (pageNo) {
            $scope.currentPage = pageNo;
        };

        $scope.pageChanged = function () {
            //console.log('Page changed to: ' + $scope.currentPage);
        };


        $scope.myAuctionsLoaded = false;
        $scope.myAuctions = [];
        $scope.myActiveLeads = [];

        $scope.myActiveLeadsEnded = [];
        $scope.myActiveLeadsNotStarted = [];

        var loginUserData = userService.getUserObj();

        $scope.updateUserDataFromService = function () {
            loginUserData = userService.getUserObj();

            this.userId = userService.getUserId();
            $scope.isOTPVerified = loginUserData.isOTPVerified;
            $scope.isEmailOTPVerified = loginUserData.isEmailOTPVerified;
            $scope.credentialsVerified = loginUserData.credentialsVerified;
            if ($scope.isOTPVerified && $scope.isEmailOTPVerified) {
                auctionsService.getmyAuctions({ "userid": userService.getUserId(), "sessionid": userService.getUserToken() })
                    .then(function (response) {
                        $scope.myAuctions = response;
                        if ($scope.myAuctions.length > 0) {
                            $scope.myAuctionsLoaded = true;
                            $scope.totalItems = $scope.myAuctions.length;
                        } else {
                            $scope.myAuctionsLoaded = false;
                            $scope.totalItems = 0;
                            $scope.myAuctionsMessage = "There are no auctions running right now for you.";
                        }
                    });

                $scope.totalLeads = 0;
                $scope.totalLeads2 = 0;

                auctionsService.getactiveleads({ "userid": userService.getUserId(), "sessionid": userService.getUserToken() })
                    .then(function (response) {
                        $scope.myActiveLeads = response;
                        if ($scope.myActiveLeads.length > 0) {
                            $scope.myAuctionsLoaded = true;

                            $scope.myActiveLeads.forEach(function (item, index) {
                                if (item.status == 'UNCONFIRMED' || item.status == 'NOTSTARTED' || item.status == 'STARTED') {

                                    item.quotationFreezTime = new moment(item.quotationFreezTime).format("DD-MM-YYYY HH:mm");

                                    $scope.myActiveLeadsNotStarted.push(item);

                                    $scope.totalLeads = $scope.myActiveLeadsNotStarted.length;
                                }
                                else {

                                    item.quotationFreezTime = new moment(item.quotationFreezTime).format("DD-MM-YYYY HH:mm");

                                    $scope.myActiveLeadsEnded.push(item);
                                    $scope.totalLeads2 = $scope.myActiveLeadsEnded.length;
                                }
                            })
                        } else {
                            $scope.totalLeads = 0;
                            $scope.myAuctionsLoaded = false;
                            $scope.myAuctionsMessage = "There are no auctions running right now for you.";
                        }
                    });
            }
        }

        $scope.updateUserDataFromService();







    });﻿prmApp

    .controller('bidHistoryCtrl', function ($scope, $http, $state, domain, $filter, $stateParams, $timeout, auctionsService, userService, SignalRFactory, fileReader, growlService) {
        $scope.bidhistory = {};
        $scope.bidhistory.uID = $stateParams.Id;
        $scope.bidhistory.reqID = $stateParams.reqID;

        $scope.bidHistory = {};
        $scope.CovertedDate = '';
        $scope.Name = 'No previous bids';
        $scope.GetBidHistory = function () {
            auctionsService.GetBidHistory({ "reqid": $scope.bidhistory.reqID, 'userid': $scope.bidhistory.uID, "sessionid": userService.getUserToken() })
                .then(function (response) {
                    $scope.bidHistory = response;

                    if ($scope.bidHistory.length > 0) {
                        $scope.Name = $scope.bidHistory[0].firstName + ' ' + $scope.bidHistory[0].lastName;
                    }

                    //var data = response.createdTime;
                    //var date = new Date(parseInt(data.substr(6)));
                    //$scope.bidHistory.createdTime = date.getDate() + '/' + (date.getMonth() + 1) + '/' + date.getFullYear();

                });
        }

        $scope.GetBidHistory();

        $scope.GetDateconverted = function (dateBefore) {
            //var data = dateBefore;
            //var date = new Date(parseInt(data.substr(6)));
            //$scope.CovertedDate = date.getDate() + '/' + (date.getMonth() + 1) + '/' + date.getFullYear();
            //return $scope.CovertedDate;

            var date = dateBefore.split('+')[0].split('(')[1];
            var newDate = new Date(parseInt(parseInt(date)));
            $scope.CovertedDate = newDate.toString().replace('Z', '');
            return $scope.CovertedDate;

        }

    });﻿prmApp
    .controller('companyDepartmentsCtrl', function ($scope, $stateParams, $log, $state, $window, userService, auctionsService, storeService, growlService) {        

        $scope.userID = userService.getUserId();
        $scope.sessionID = userService.getUserToken();

        $scope.companyDepartments = [];

        $scope.department = {
            userID: $scope.userID,
            deptID: 0,
            deptCode: '',
            deptDesc: '',
            deptAdmin: $scope.userID,
            sessionID: $scope.sessionID
        };

        $scope.addnewdeptView = false;

        $scope.GetCompanyDepartments = function () {
            auctionsService.GetCompanyDepartments($scope.userID, $scope.sessionID)
               .then(function (response) {
                   $scope.companyDepartments = response;
               })
        }

        $scope.GetCompanyDepartments();


        $scope.SaveCompanyDepartment = function () {

            var params = {
                "companyDepartment": $scope.department
            };

            auctionsService.SaveCompanyDepartment(params)
               .then(function (response) {
                   if (response.errorMessage != '') {
                       growlService.growl(response.errorMessage, "inverse");
                   }
                   else {
                       growlService.growl("Department Saved Successfully.", "success");
                       $scope.GetCompanyDepartments();
                       $scope.addnewdeptView = false;
                       $scope.department = {
                           userID: $scope.userID,
                           deptID: 0,
                           deptAdmin: $scope.userID,
                           sessionID: $scope.sessionID
                       };
                       //$window.history.back();
                   }
               });
            
        };

        $scope.editDepartment = function (companyDepartment) {
            $scope.addnewdeptView = true;
            $scope.department = companyDepartment;
            $scope.department.sessionID = $scope.sessionID;
            $scope.deptAdmin = $scope.userID;
        };
        
        $scope.closeEditDepartment = function () {
            $scope.addnewdeptView = false;
            $scope.department = {
                userID: $scope.userID,
                deptID: 0,
                deptAdmin: $scope.userID,
                sessionID: $scope.sessionID
            };
        };


        $scope.DeleteDepartment = function (companyDepartment) {

            var params = {
                deptID: companyDepartment.deptID,
                sessionID: $scope.sessionID
            };

            auctionsService.DeleteDepartment(params)
               .then(function (response) {
                   if (response.errorMessage != '') {
                       growlService.growl(response.errorMessage, "inverse");
                   }
                   else {
                       growlService.growl("Department Deleted Successfully.", "success");
                       $scope.GetCompanyDepartments();
                       //$window.history.back();
                   }
               });
            $scope.addnewdeptView = false;
        };

        $scope.departmentUsers = [];
        $scope.deptUsersView = false;
        $scope.deptCode = '';

        $scope.GetDepartmentUsers = function (companyDepartment) {
            auctionsService.GetDepartmentUsers(companyDepartment.deptID, $scope.userID, $scope.sessionID)
               .then(function (response) {
                   $scope.departmentUsers = response;
               })
            $scope.deptCode = companyDepartment.deptCode;
            $scope.deptUsersView = true;

            document.body.scrollTop = 0; // For Chrome, Safari and Opera 
            document.documentElement.scrollTop = 0; // For IE and Firefox
        }
        
        $scope.SaveUserDepartments = function () {

            var params = {
                "listUserDepartments": $scope.departmentUsers,
                "sessionID": userService.getUserToken()
            };

            auctionsService.SaveUserDepartments(params)
               .then(function (response) {
                   if (response.errorMessage != '') {
                       //swal("Error!", 'Not Saved', 'error');

                       growlService.growl(response.errorMessage, "inverse");
                   }
                   else {

                       growlService.growl("Users Added to Department Successfully.", "success");                       
                       $scope.deptUsersView = false;
                   }
               })
        };

        $scope.cancelDeptUsersView = function () {            
            $scope.deptUsersView = false;
            $scope.deptCode = '';
        };

});﻿prmApp
    .controller('comparativesCtrl', function ($scope, $state, $log, $stateParams, userService, auctionsService) {
        $scope.reqId = $stateParams.reqId;
        $scope.userId = userService.getUserId();
        $scope.sessionid = userService.getUserToken();

        $scope.listRequirementTaxes = [];
        $scope.listRequirementTaxeslength = $scope.listRequirementTaxes.length;


        $scope.getCustomerData = function (userId) {
            auctionsService.getrequirementdata({ "reqid": $scope.reqId, "sessionid": userService.getUserToken(), 'userid': userId })
                .then(function (response) {
                    $scope.auctionItem = response;

                    if ($scope.auctionItem.auctionVendors.length > 0) {
                        $scope.getRequirementData();
                    };

                });
        }

        $scope.getVendorData = function (item) {
            auctionsService.getrequirementdata({ "reqid": $scope.reqId, "sessionid": userService.getUserToken(), 'userid': item.vendorID })
                .then(function (response) {
                    $scope.auctionItemVendor = response;

                    item.auctionItemVendor = $scope.auctionItemVendor;
                    $log.info(item.auctionItemVendor.listRequirementItems[0].productIDorName);

                });
        }

        $scope.getRequirementData = function () {            
            $scope.auctionItem.auctionVendors.forEach(function (item, index) {
                if (item.vendorID > 0) {
                    item.auctionItemVendor = [];
                    $scope.getVendorData(item);
                }
            });
            //$log.info($scope.auctionItem);
        }

        $scope.getCustomerData($scope.userId);

        //$scope.auctionItem.auctionVendors.auctionItemVendor.auctionVendors[0].listRequirementItems.productIDorName;

    });prmApp
.controller('EditableRowCtrl', function ($scope, $state, $stateParams, userService, auctionsService, fileReader) {
	$scope.users = [
    {id: 1, name: 'awesome user1', status: 2, group: 4, groupName: 'admin'},
    {id: 2, name: 'awesome user2', status: undefined, group: 3, groupName: 'vip'},
    {id: 3, name: 'awesome user3', status: 2, group: null}
  ]; 

  $scope.statuses = [
    {value: 1, text: 'status1'},
    {value: 2, text: 'status2'},
    {value: 3, text: 'status3'},
    {value: 4, text: 'status4'}
  ]; 

  $scope.groups = [];
  $scope.loadGroups = function() {
    return $scope.groups.length ? null : $http.get('/groups').success(function(data) {
      $scope.groups = data;
    });
  };

  $scope.showGroup = function(user) {
    if(user.group && $scope.groups.length) {
      var selected = $filter('filter')($scope.groups, {id: user.group});
      return selected.length ? selected[0].text : 'Not set';
    } else {
      return user.groupName || 'Not set';
    }
  };

  $scope.showStatus = function(user) {
    var selected = [];
    if(user.status) {
      selected = $filter('filter')($scope.statuses, {value: user.status});
    }
    return selected.length ? selected[0].text : 'Not set';
  };

  $scope.checkName = function(data, id) {
    if (id === 2 && data !== 'awesome') {
      return "Username 2 should be `awesome`";
    }
  };

  $scope.saveUser = function(data, id) {
    //$scope.user not updated yet
    angular.extend(data, {id: id});
    return $http.post('/saveUser', data);
  };

  // remove user
  $scope.removeUser = function(index) {
    $scope.users.splice(index, 1);
  };

  // add user
  $scope.addUser = function() {
    $scope.inserted = {
      id: $scope.users.length+1,
      name: '',
      status: null,
      group: null 
    };
    $scope.users.push($scope.inserted);
  };
});prmApp

    .controller('editFormCtrl', function ($scope, $http, $state, domain, $filter, $stateParams, $timeout, auctionsService, userService, SignalRFactory, fileReader, growlService) {
        $scope.Id = $stateParams.Id;
        $scope.Vendors = [];
        $scope.selectedA = [];
        $scope.selectedB = [];
        $scope.formRequest = {
            auctionVendors: []
        };
        var origReq = {
            auctionVendors: []
        };
        $scope.categories = [];
        $scope.vendorsLoaded = false;
        $scope.selectVendorShow = true;
        $scope.addVendorShow = false;

        $scope.getData = function () {
            auctionsService.getrequirementdata({ "reqid": $stateParams.Id, "sessionid": userService.getUserToken(), "userid": userService.getUserId() })
                .then(function (response) {

                    var category = response.category[0];
                    response.category = category;
                    response.taxes = parseInt(response.taxes);
                    response.paymentTerms = parseInt(response.paymentTerms);
                    $scope.formRequest = response;
                    //$scope.formRequest.urgency.push(urgency);
                    $scope.SelectedVendors = $scope.formRequest.auctionVendors;
                    $scope.getvendors();
                });
            $http({
                method: 'GET',
                url: domain + 'PRMServiceQA.svc/REST/getcategories?sessionid=' + userService.getUserToken(),
                encodeURI: true,
                headers: { 'Content-Type': 'application/json' }
            }).then(function (response) {
                if (response && response.data) {
                    if (response.data.length > 0) {
                        $scope.categories = response.data;
                        $scope.showCategoryDropdown = true;
                    }
                } else {
                    //console.log(response.data[0].errorMessage);
                }
            }, function (result) {
                //console.log("there is no current auctions");
            });

        }
        $scope.getData();
        $scope.getFile = function () {
            $scope.progress = 0;
            // //console.log($("#attachement"));
            $scope.file = $("#attachement")[0].files[0];
            //console.log($("#attachement")[0].files[0]);
            fileReader.readAsDataUrl($scope.file, $scope)
                .then(function (result) {
                    var bytearray = new Uint8Array(result);
                    $scope.formRequest.attachment = $.makeArray(bytearray);
                    $scope.formRequest.attachmentName = $scope.file.name;
                });
        };

        $scope.changeCategory = function () {
            swal({
                title: "Are you sure?",
                text: "The existing vendors will be removed and an email will be sent to them notifying the same.",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#F44336",
                confirmButtonText: "Yes, I am sure",
                closeOnConfirm: true
            }, function () {
                $scope.getvendors();
                $scope.formRequest.auctionVendors = [];
            });

        }




        $scope.postRequest = function () {
            ////console.log($scope.formRequest);
            $scope.formRequest.requirementID = $scope.Id;
            $scope.formRequest.customerID = userService.getUserId();
            $scope.formRequest.customerFirstname = userService.getFirstname();
            $scope.formRequest.customerLastname = userService.getLastname();
            $scope.formRequest.isClosed = "NOTSTARTED";
            $scope.formRequest.endTime = "";
            $scope.formRequest.sessionID = userService.getUserToken();
            var categories = [];
            categories.push($scope.formRequest.category);
            $scope.formRequest.category = categories;
            //console.log($scope.formRequest.urgency);
            swal({
                title: "Are you sure?",
                text: "This will send an email invite to any new vendors you selected.",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#F44336",
                confirmButtonText: "OK",
                closeOnConfirm: true
            }, function () {
                auctionsService.postrequirementdata($scope.formRequest)
                    .then(function (response) {
                        if (response.objectID > 0) {
                            swal({
                                title: "Done!",
                                text: "Requirement Updated Successfully",
                                type: "success",
                                showCancelButton: false,
                                confirmButtonColor: "#DD6B55",
                                confirmButtonText: "Ok",
                                closeOnConfirm: true
                            },
                                function () {
                                    //$state.go('view-requirement');
                                    $state.go('view-requirement', { 'Id': response.objectID });
                                });
                        }
                    });
            });

        }

        $scope.getvendors = function () {
            $scope.vendorsLoaded = false;
            if ($scope.formRequest.category != undefined) {
                var category = [];
                category.push($scope.formRequest.category);
                var params = { 'Categories': category, 'sessionID': userService.getUserToken(), 'userid': userService.getUserId() };
                $http({
                    method: 'POST',
                    url: domain + 'PRMServiceQA.svc/REST/getvendors',
                    encodeURI: true,
                    headers: { 'Content-Type': 'application/json' },
                    data: params
                }).then(function (response) {
                    if (response && response.data) {
                        if (response.data.length > 0) {
                            $scope.Vendors = response.data;
                            $scope.vendorsLoaded = true;
                            for (var j in $scope.formRequest.auctionVendors) {
                                for (var i in $scope.Vendors) {
                                    if ($scope.Vendors[i].vendorName == $scope.formRequest.auctionVendors[j].vendorName) {
                                        $scope.Vendors.splice(i, 1);
                                    }
                                }
                            }
                            //$scope.Vendors = response.data;                            
                        }
                    } else {
                        //console.log(response.data[0].errorMessage);
                    }
                }, function (result) {
                    //console.log("there is no current auctions");
                });
            }

        };


        $scope.selectForA = function (item) {
            var index = $scope.selectedA.indexOf(item);
            if (index > -1) {
                $scope.selectedA.splice(index, 1);
            } else {
                $scope.selectedA.splice($scope.selectedA.length, 0, item);
            }
        }

        $scope.selectForB = function (item) {
            var index = $scope.selectedB.indexOf(item);
            if (index > -1) {
                $scope.selectedB.splice(index, 1);
            } else {
                $scope.selectedB.splice($scope.selectedA.length, 0, item);
            }
        }

        $scope.AtoB = function () {
            for (i = 0; i < $scope.selectedA.length; i++) {
                $scope.formRequest.auctionVendors.push($scope.selectedA[i]);
                $scope.Vendors.splice($scope.Vendors.indexOf($scope.selectedA[i]), 1);
            }
            $scope.reset();
        }

        $scope.BtoA = function () {
            for (i = 0; i < $scope.selectedB.length; i++) {
                $scope.Vendors.push($scope.selectedB[i]);
                $scope.formRequest.auctionVendors.splice($scope.formRequest.auctionVendors.indexOf($scope.selectedB[i]), 1);
            }
            $scope.reset();
        }

        $scope.reset = function () {
            $scope.selectedA = [];
            $scope.selectedB = [];
        }

        // $scope.getFile = function () {
        //     $scope.progress = 0;
        //     fileReader.readAsDataUrl($scope.file, $scope)
        //     .then(function(result) {
        //         $scope.formRequest.attachment = result;
        //     });
        // };


        $scope.newVendor = {};

        $scope.addVendor = function () {
            $scope.emailRegx = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            //$scope.emailRegx = /^[a-z]+[a-z0-9._]+@[a-z]+\.[a-z.]{2,5}$/;
            var addVendorValidationStatus = false;
            $scope.firstvalidation = $scope.lastvalidation = $scope.contactvalidation = $scope.emailvalidation = $scope.categoryvalidation = $scope.emailregxvalidation = false;
            if ($scope.newVendor.firstName == "" || $scope.newVendor.firstName === undefined) {
                $scope.firstvalidation = true;
                addVendorValidationStatus = true;
            }
            if ($scope.newVendor.lastName == "" || $scope.newVendor.lastName === undefined) {
                $scope.lastvalidation = true;
                addVendorValidationStatus = true;
            }
            if ($scope.newVendor.contactNum == "" || $scope.newVendor.contactNum === undefined || isNaN($scope.newVendor.contactNum)) {
                $scope.contactvalidation = true;
                addVendorValidationStatus = true;
            }
            if ($scope.newVendor.email == "" || $scope.newVendor.email === undefined) {
                $scope.emailvalidation = true;
                addVendorValidationStatus = true;
            } else if (!$scope.emailRegx.test($scope.newVendor.email)) {
                $scope.emailregxvalidation = true;
                addVendorValidationStatus = true;
            }
            if ($scope.formRequest.category == "" || $scope.formRequest.category === undefined) {
                $scope.categoryvalidation = true;
                addVendorValidationStatus = true;
            }
            if (addVendorValidationStatus) {
                return false;
            }
            var vendCategories = [];
            $scope.newVendor.category = $scope.formRequest.category;
            vendCategories.push($scope.newVendor.category);
            var params = {
                "vendorInfo": {
                    "firstName": $scope.newVendor.firstName,
                    "lastName": $scope.newVendor.lastName,
                    "email": $scope.newVendor.email,
                    "contactNum": $scope.newVendor.contactNum,
                    "username": $scope.newVendor.contactNum,
                    "password": $scope.newVendor.contactNum,
                    "rating": 1,
                    "category": vendCategories,
                    "panNum": ("panno" in $scope.newVendor) ? $scope.newVendor.panno : "",
                    "serviceTaxNum": ("serviceTaxNo" in $scope.newVendor) ? $scope.newVendor.serviceTaxNo : "",
                    "vatNum": ("vatNum" in $scope.newVendor) ? $scope.newVendor.vatNum : "",
                    "referringUserID": parseInt(userService.getUserId()),
                    "knownSince": ("knownSince" in $scope.newVendor) ? $scope.newVendor.knownSince : "",
                    "errorMessage": "",
                    "sessionID": userService.getUserToken()
                }
            };
            $http({
                method: 'POST',
                url: domain + 'PRMServiceQA.svc/REST/addnewvendor',
                encodeURI: true,
                headers: { 'Content-Type': 'application/json' },
                data: params
            }).then(function (response) {
                if (response && response.data && response.data.errorMessage == "") {
                    $scope.formRequest.auctionVendors.push({ vendorName: $scope.newVendor.firstName + " " + $scope.newVendor.lastName, vendorID: response.data.objectID });
                    $scope.newVendor = null;
                    $scope.newVendor = {};
                    //$scope.addVendorForm.$setPristine();
                    $scope.addVendorShow = false;
                    $scope.selectVendorShow = true;
                    growlService.growl("Vendor Added Successfully.", 'inverse');
                } else if (response && response.data && response.data.errorMessage) {
                    growlService.growl(response.data.errorMessage, 'inverse');
                } else {
                    growlService.growl('Unexpected Error Occurred', 'inverse');
                }
            });
        }
    });prmApp
    // =========================================================================
    // COMMON FORMS
    // =========================================================================

    .controller('formCtrl', function ($state, $stateParams, $scope, auctionsService, userService, $http, domain, fileReader, growlService, $log, $filter, ngDialog) {


        if ($stateParams.Id) {
            $scope.stateParamsReqID = $stateParams.Id;
        } else {
            $scope.stateParamsReqID = 0;
        };


        var curDate = new Date();
        var today = moment();
        var tomorrow = today.add('days', 1);
        var dateObj = $('.datetimepicker').datetimepicker({
            format: 'DD/MM/YYYY',
            useCurrent: false,
            minDate: tomorrow,
            keepOpen: false
        });
        $scope.subcategories = [];
        $scope.sub = {
            selectedSubcategories: [],
        }
        $scope.selectedCurrency = {};
        $scope.currencies = [];


        $scope.postRequestLoding = false;
        $scope.selectedSubcategories = [];



        $scope.selectVendorShow = true;
        $scope.isEdit = false;
        //Input Slider
        this.nouisliderValue = 4;
        this.nouisliderFrom = 25;
        this.nouisliderTo = 80;
        this.nouisliderRed = 35;
        this.nouisliderBlue = 90;
        this.nouisliderCyan = 20;
        this.nouisliderAmber = 60;
        this.nouisliderGreen = 75;

        //Color Picker
        this.color = '#03A9F4';
        this.color2 = '#8BC34A';
        this.color3 = '#F44336';
        this.color4 = '#FFC107';

        $scope.Vendors = [];
        $scope.categories = [];
        $scope.selectedA = [];
        $scope.selectedB = [];
        $scope.showCategoryDropdown = false;
        $scope.checkVendorPhoneUniqueResult = false;
        $scope.checkVendorEmailUniqueResult = false;
        $scope.checkVendorPanUniqueResult = false;
        $scope.checkVendorTinUniqueResult = false;
        $scope.checkVendorStnUniqueResult = false;
        $scope.showFreeCreditsMsg = false;
        $scope.showNoFreeCreditsMsg = false;
        $scope.formRequest = {
            isTabular: true,
            auctionVendors: [],
            listRequirementItems: [],
            isQuotationPriceLimit: false,
            quotationPriceLimit: 0,
            quotationFreezTime: '',
            deleteQuotations: false
        };
        $scope.Vendors.city = "";
        $scope.Vendors.quotationUrl = "";
        $scope.vendorsLoaded = false;
        $scope.requirementAttachment = [];


        $scope.sessionid = userService.getUserToken();


        $scope.budgetValidate = function () {
            $log.info("siva");
            if ($scope.formRequest.budget != "" && (isNaN($scope.formRequest.budget) || $scope.formRequest.budget.indexOf('.') > -1)) {
                $scope.postRequestLoding = false;
                swal({
                    title: "Error!",
                    text: "Please enter valid budget, budget should be greater than 1,00,000.",
                    type: "error",
                    showCancelButton: false,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Ok",
                    closeOnConfirm: true
                },
                    function () {

                    });

                $scope.formRequest.budget = "";
            }
        };

        $scope.clickToOpen = function () {
            ngDialog.open({ template: 'login/termsAddNewReq.html', width: 1000 });
        };

        $scope.changeCategory = function () {
            $scope.formRequest.auctionVendors = [];
            $scope.loadSubCategories();
            $scope.getvendors();
        }

        $scope.getCreditCount = function () {
            userService.getProfileDetails({ "userid": userService.getUserId(), "sessionid": userService.getUserToken() })
                .then(function (response) {
                    $scope.userDetails = response;
                    $scope.selectedCurrency = $filter('filter')($scope.currencies, { value: response.currency });
                    $scope.selectedCurrency = $scope.selectedCurrency[0];
                    if (response.creditsLeft) {
                        $scope.showFreeCreditsMsg = true;
                    } else {
                        $scope.showNoFreeCreditsMsg = true;
                    }
                });
        }



        $scope.getvendors = function () {
            $scope.vendorsLoaded = false;
            var category = [];

            category.push($scope.formRequest.category);
            //$scope.formRequest.category = category;
            if ($scope.formRequest.category != undefined) {
                var params = { 'Categories': category, 'sessionID': userService.getUserToken(), 'uID': userService.getUserId() };
                $http({
                    method: 'POST',
                    url: domain + 'getvendors',
                    encodeURI: true,
                    headers: { 'Content-Type': 'application/json' },
                    data: params
                }).then(function (response) {
                    if (response && response.data) {
                        if (response.data.length > 0) {
                            $scope.Vendors = response.data;
                            $scope.vendorsLoaded = true;
                            for (var j in $scope.formRequest.auctionVendors) {
                                for (var i in $scope.Vendors) {
                                    if ($scope.Vendors[i].vendorName == $scope.formRequest.auctionVendors[j].vendorName) {
                                        $scope.Vendors.splice(i, 1);
                                    }
                                }
                            }
                        }
                        //$scope.formRequest.auctionVendors =[];
                    } else {
                        //console.log(response.data[0].errorMessage);
                    }
                }, function (result) {
                    //console.log("there is no current auctions");
                });
            }

        };

        /*$scope.getvendorsbysubcat = function () {
            $scope.vendorsLoaded = false;
            var category = [];

            category.push($scope.sub.selectedSubcategories);
            //$scope.formRequest.category = category;
            if ($scope.formRequest.category != undefined) {
                var params = { 'Categories': category, 'sessionID': userService.getUserToken(), 'uID': userService.getUserId() };
                $http({
                    method: 'POST',
                    url: domain + 'getvendorsbycatnsubcat',
                    encodeURI: true,
                    headers: { 'Content-Type': 'application/json' },
                    data: params
                }).then(function (response) {
                    if (response && response.data) {
                        if (response.data.length > 0) {
                            $scope.Vendors = response.data;
                            $scope.vendorsLoaded = true;
                            for (var j in $scope.formRequest.auctionVendors) {
                                for (var i in $scope.Vendors) {
                                    if ($scope.Vendors[i].vendorName == $scope.formRequest.auctionVendors[j].vendorName) {
                                        $scope.Vendors.splice(i, 1);
                                    }
                                }
                            }
                        }
                        //$scope.formRequest.auctionVendors =[];
                    } else {
                        //console.log(response.data[0].errorMessage);
                    }
                }, function (result) {
                    //console.log("there is no current auctions");
                });
            }

        };*/

        $scope.isRequirementPosted = 0;

        $scope.getData = function () {
            $http({
                method: 'GET',
                url: domain + 'getcategories?userid=' + userService.getUserId() + '&sessionid=' + userService.getUserToken(),
                encodeURI: true,
                headers: { 'Content-Type': 'application/json' }
            }).then(function (response) {
                if (response && response.data) {

                    if (response.data.length > 0) {
                        $scope.categories = _.uniq(_.map(response.data, 'category'));
                        $scope.categoriesdata = response.data;
                        $scope.showCategoryDropdown = true;
                    }
                } else {
                    //console.log(response.data[0].errorMessage);
                }
            }, function (result) {
                //console.log("there is no current auctions");
            });
            $http({
                method: 'GET',
                url: domain + 'getkeyvaluepairs?parameter=CURRENCY',
                encodeURI: true,
                headers: { 'Content-Type': 'application/json' }
            }).then(function (response) {
                if (response && response.data) {
                    if (response.data.length > 0) {
                        $scope.currencies = response.data;
                        $scope.getCreditCount();
                    }
                } else {
                    //console.log(response.data[0].errorMessage);
                }
            }, function (result) {
                //console.log("there is no current auctions");
            });
            if ($stateParams.Id) {
                var id = $stateParams.Id;
                $scope.isEdit = true;

                auctionsService.getrequirementdata({ "reqid": $stateParams.Id, "sessionid": userService.getUserToken(), "userid": userService.getUserId() })
                    .then(function (response) {

                        var category = response.category[0];
                        response.category = category;
                        response.taxes = parseInt(response.taxes);
                        //response.paymentTerms = parseInt(response.paymentTerms);
                        $scope.formRequest = response;

                        $scope.itemSNo = $scope.formRequest.itemSNoCount;

                        $scope.formRequest.checkBoxEmail = true;
                        $scope.formRequest.checkBoxSms = true;
                        $scope.loadSubCategories();

                        $scope.isRequirementPosted = $scope.formRequest.auctionVendors.length;


                        $scope.selectedSubcategories = response.subcategories.split(",");
                        for (i = 0; i < $scope.selectedSubcategories.length; i++) {
                            for (j = 0; j < $scope.subcategories.length; j++) {
                                if ($scope.selectedSubcategories[i] == $scope.subcategories[j].subcategory) {
                                    $scope.subcategories[j].ticked = true;
                                }
                            }
                        }
                        //console.log($scope.formRequest);
                        //$scope.getvendors();
                        $scope.selectSubcat();
                        $scope.formRequest.attFile = response.attachmentName;
                        //$scope.formRequest.deliveryTime = new moment($scope.formRequest.deliveryTime).format("DD-MM-YYYY");
                        $scope.formRequest.quotationFreezTime = new moment($scope.formRequest.quotationFreezTime).format("DD-MM-YYYY HH:mm");
                        //$scope.formRequest.urgency.push(urgency);
                        $scope.SelectedVendors = $scope.formRequest.auctionVendors;
                    });
            }

        };

        $scope.showSimilarNegotiationsButton = function (value, searchstring) {
            $scope.showSimilarNegotiations = value;
            if (!value) {
                $scope.CompanyLeads = {};
            }
            if (value) {
                if (searchstring.length < 3) {
                    $scope.CompanyLeads = {};
                }
                if (searchstring.length > 2) {
                    $scope.searchstring = searchstring;
                    $scope.GetCompanyLeads(searchstring);
                }
            }
            return $scope.showSimilarNegotiations;
        }

        $scope.showSimilarNegotiations = false;

        $scope.CompanyLeads = {};

        $scope.searchstring = '';

        $scope.GetCompanyLeads = function (searchstring) {
            if (searchstring.length < 3) {
                $scope.CompanyLeads = {};
            }
            if ($scope.showSimilarNegotiations && searchstring.length > 2) {
                $scope.searchstring = searchstring;
                var params = { "userid": userService.getUserId(), "searchstring": $scope.searchstring, "searchtype": 'Title', "sessionid": userService.getUserToken() };
                auctionsService.GetCompanyLeads(params)
                    .then(function (response) {
                        $scope.CompanyLeads = response;
                        $scope.CompanyLeads.forEach(function (item, index) {
                            item.postedOn = new moment(item.postedOn).format("DD-MM-YYYY HH:mm");
                        })
                    });
            }
        }


        $scope.changeScheduledAuctionsLimit = function () {
            $scope.scheduledLimit = 8;
            $scope.getMiniItems();
        }

        $scope.loadSubCategories = function () {
            $scope.subcategories = _.filter($scope.categoriesdata, { category: $scope.formRequest.category });
            /*$scope.subcategories = _.map($scope.subcategories, 'subcategory');*/
            //console.log($scope.subcategories);
        }



        $scope.selectSubcat = function (subcat) {
            if (!$scope.isEdit) {
                $scope.formRequest.auctionVendors = [];
            }
            $scope.vendorsLoaded = false;
            var category = [];
            var count = 0;
            var succategory = "";
            $scope.sub.selectedSubcategories = $filter('filter')($scope.subcategories, { ticked: true });
            selectedcount = $scope.sub.selectedSubcategories.length;
            if (selectedcount > 0) {
                succategory = _.map($scope.sub.selectedSubcategories, 'id');
                category.push(succategory);
                //console.log(category);
                //$scope.formRequest.category = category;
                if ($scope.formRequest.category != undefined) {
                    var params = { 'Categories': succategory, 'sessionID': userService.getUserToken(), 'count': selectedcount, 'uID': userService.getUserId() };
                    $http({
                        method: 'POST',
                        url: domain + 'getvendorsbycatnsubcat',
                        encodeURI: true,
                        headers: { 'Content-Type': 'application/json' },
                        data: params
                    }).then(function (response) {
                        if (response && response.data) {
                            if (response.data.length > 0) {
                                $scope.Vendors = response.data;
                                $scope.vendorsLoaded = true;
                                for (var j in $scope.formRequest.auctionVendors) {
                                    for (var i in $scope.Vendors) {
                                        if ($scope.Vendors[i].vendorName == $scope.formRequest.auctionVendors[j].vendorName) {
                                            $scope.Vendors.splice(i, 1);
                                        }
                                    }
                                }
                            }
                            //$scope.formRequest.auctionVendors =[];
                        } else {
                            //console.log(response.data[0].errorMessage);
                        }
                    }, function (result) {
                        //console.log("there is no current auctions");
                    });
                }
            } else {
                $scope.getvendors();
            }

        }

        $scope.getData();

        $scope.checkVendorUniqueResult = function (idtype, inputvalue) {
            if (inputvalue == "" || inputvalue == undefined) {
                return false;
            }
            /*$scope.checkVendorPhoneUniqueResult=false;
            $scope.checkVendorEmailUniqueResult=false;*/
            userService.checkUserUniqueResult(inputvalue, idtype).then(function (response) {
                if (idtype == "PHONE") {
                    $scope.checkVendorPhoneUniqueResult = !response;
                } else if (idtype == "EMAIL") {
                    $scope.checkVendorEmailUniqueResult = !response;
                }
                else if (idtype == "PAN") {
                    $scope.checkVendorPanUniqueResult = !response;
                }
                else if (idtype == "TIN") {
                    $scope.checkVendorTinUniqueResult = !response;
                }
                else if (idtype == "STN") {
                    $scope.checkVendorStnUniqueResult = !response;
                }
            });
        };
        $scope.selectForA = function (item) {
            var index = $scope.selectedA.indexOf(item);
            if (index > -1) {
                $scope.selectedA.splice(index, 1);
            } else {
                $scope.selectedA.splice($scope.selectedA.length, 0, item);
            }
            for (i = 0; i < $scope.selectedA.length; i++) {
                $scope.formRequest.auctionVendors.push($scope.selectedA[i]);
                $scope.Vendors.splice($scope.Vendors.indexOf($scope.selectedA[i]), 1);
            }
            $scope.reset();
        }

        $scope.selectForB = function (item) {
            var index = $scope.selectedB.indexOf(item);
            if (index > -1) {
                $scope.selectedB.splice(index, 1);
            } else {
                $scope.selectedB.splice($scope.selectedA.length, 0, item);
            }
            for (i = 0; i < $scope.selectedB.length; i++) {
                $scope.Vendors.push($scope.selectedB[i]);
                $scope.formRequest.auctionVendors.splice($scope.formRequest.auctionVendors.indexOf($scope.selectedB[i]), 1);
            }
            $scope.reset();
        }

        $scope.AtoB = function () {

        }

        $scope.BtoA = function () {

        }

        $scope.reset = function () {
            $scope.selectedA = [];
            $scope.selectedB = [];
        }

        // $scope.getFile = function () {
        //     $scope.progress = 0;
        //     fileReader.readAsDataUrl($scope.file, $scope)
        //     .then(function(result) {
        //         $scope.formRequest.attachment = result;
        //     });
        // };

        $scope.getFile = function () {
            $scope.progress = 0;

            $scope.file = $("#attachement")[0].files[0];

            fileReader.readAsDataUrl($scope.file, $scope)
                .then(function (result) {
                    var bytearray = new Uint8Array(result);
                    $scope.formRequest.attachment = $.makeArray(bytearray);
                    //$scope.bidAttachementName=$scope.file.name;
                    //$scope.formRequest.attachment=$scope.file.name;
                    $scope.formRequest.attachmentName = $scope.file.name;
                });
        };


        $scope.newVendor = {};
        $scope.Attaachmentparams = {};
        $scope.deleteAttachment = function (reqid) {
            $scope.Attaachmentparams = {
                reqID: reqid,
                userID: userService.getUserId()
            }
            auctionsService.deleteAttachment($scope.Attaachmentparams)
                .then(function (response) {
                    if (response.errorMessage != "") {
                        growlService.growl(response.errorMessage, "inverse");
                    } else {
                        growlService.growl("Attachment deleted Successfully", "inverse");
                        $scope.getData();
                    }
                });
        }

        $scope.newVendor.panno = "";
        $scope.newVendor.vatNum = "";
        $scope.newVendor.serviceTaxNo = "";

        $scope.addVendor = function () {
            $scope.emailRegx = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            $scope.mobileRegx = /^\+?([0-9]{2})\)?[-. ]?([0-9]{4})[-. ]?([0-9]{4})$/;
            $scope.panregx = /^([a-zA-Z]{5})(\d{4})([a-zA-Z]{1})$/;
            //$scope.emailRegx = /^[a-z]+[a-z0-9._]+@[a-z]+\.[a-z.]{2,5}$/;
            var addVendorValidationStatus = false;
            $scope.firstvalidation = $scope.companyvalidation = $scope.lastvalidation = $scope.contactvalidation = $scope.emailvalidation = $scope.categoryvalidation = $scope.emailregxvalidation = $scope.contactvalidationlength = $scope.panregxvalidation = $scope.tinvalidation = $scope.stnvalidation = $scope.knownsincevalidation = $scope.vendorcurrencyvalidation = false;
            if ($scope.newVendor.companyName == "" || $scope.newVendor.companyName === undefined) {
                $scope.companyvalidation = true;
                addVendorValidationStatus = true;
            }
            if ($scope.newVendor.firstName == "" || $scope.newVendor.firstName === undefined) {
                $scope.firstvalidation = true;
                addVendorValidationStatus = true;
            }
            if ($scope.newVendor.lastName == "" || $scope.newVendor.lastName === undefined) {
                $scope.lastvalidation = true;
                addVendorValidationStatus = true;
            }
            if ($scope.newVendor.contactNum == "" || $scope.newVendor.contactNum === undefined || isNaN($scope.newVendor.contactNum)) {
                $scope.contactvalidation = true;
                addVendorValidationStatus = true;
            }
            else if ($scope.newVendor.contactNum.length != 10) {
                $scope.contactvalidationlength = true;
                addVendorValidationStatus = true;
            }
            if ($scope.newVendor.email == "" || $scope.newVendor.email === undefined) {
                $scope.emailvalidation = true;
                addVendorValidationStatus = true;
            }
            else if (!$scope.emailRegx.test($scope.newVendor.email)) {
                $scope.emailregxvalidation = true;
                addVendorValidationStatus = true;
            }
            if ($scope.newVendor.vendorcurrency == "" || $scope.newVendor.vendorcurrency === undefined) {
                $scope.vendorcurrencyvalidation = true;
                addVendorValidationStatus = true;
            }
            if ($scope.newVendor.panno != "" && $scope.newVendor.panno != undefined && !$scope.panregx.test($scope.newVendor.panno)) {
                $scope.panregxvalidation = true;
                addVendorValidationStatus = true;
            }
            if ($scope.newVendor.vatNum != "" && $scope.newVendor.vatNum != undefined && $scope.newVendor.vatNum.length != 11) {
                $scope.tinvalidation = true;
                addVendorValidationStatus = true;
            }

            if ($scope.newVendor.serviceTaxNo != "" && $scope.newVendor.serviceTaxNo != undefined && $scope.newVendor.serviceTaxNo.length != 15) {
                $scope.stnvalidation = true;
                addVendorValidationStatus = true;
            }
            if ($scope.formRequest.category == "" || $scope.formRequest.category === undefined) {
                $scope.categoryvalidation = true;
                addVendorValidationStatus = true;
            }
            if ($scope.checkVendorEmailUniqueResult || $scope.checkVendorEmailUniqueResult) {
                addVendorValidationStatus = true;
            }
            if (addVendorValidationStatus) {
                return false;
            }
            var vendCAtegories = [];
            $scope.newVendor.category = $scope.formRequest.category;
            vendCAtegories.push($scope.newVendor.category);
            var params = {
                "register": {
                    "firstName": $scope.newVendor.firstName,
                    "lastName": $scope.newVendor.lastName,
                    "email": $scope.newVendor.email,
                    "phoneNum": $scope.newVendor.contactNum,
                    "username": $scope.newVendor.contactNum,
                    "password": $scope.newVendor.contactNum,
                    "companyName": $scope.newVendor.companyName ? $scope.newVendor.companyName : "",
                    "isOTPVerified": 0,
                    "category": $scope.newVendor.category,
                    "userType": "VENDOR",
                    "panNumber": ("panno" in $scope.newVendor) ? $scope.newVendor.panno : "",
                    "stnNumber": ("serviceTaxNo" in $scope.newVendor) ? $scope.newVendor.serviceTaxNo : "",
                    "vatNumber": ("vatNum" in $scope.newVendor) ? $scope.newVendor.vatNum : "",
                    "referringUserID": userService.getUserId(),
                    "knownSince": ("knownSince" in $scope.newVendor) ? $scope.newVendor.knownSince : "",
                    "errorMessage": "",
                    "sessionID": "",
                    "userID": 0,
                    "department": "",
                    "currency": $scope.newVendor.vendorcurrency.key
                }
            };
            $http({
                method: 'POST',
                url: domain + 'register',
                encodeURI: true,
                headers: { 'Content-Type': 'application/json' },
                data: params
            }).then(function (response) {
                if (response && response.data && response.data.errorMessage == "") {
                    $scope.formRequest.auctionVendors.push({ vendorName: $scope.newVendor.firstName + " " + $scope.newVendor.lastName, companyName: $scope.newVendor.companyName, vendorID: response.data.objectID });
                    $scope.newVendor = null;
                    $scope.newVendor = {};
                    //$scope.addVendorForm.$setPristine();
                    $scope.addVendorShow = false;
                    $scope.selectVendorShow = true;
                    growlService.growl("Vendor Added Successfully.", 'inverse');
                } else if (response && response.data && response.data.errorMessage) {
                    growlService.growl(response.data.errorMessage, 'inverse');
                } else {
                    growlService.growl('Unexpected Error Occurred', 'inverse');
                }
            });
        }

        //$scope.checkboxModel = {
        //    value1: true
        //};

        $scope.formRequest.checkBoxEmail = true;
        $scope.formRequest.checkBoxSms = true;
        $scope.postRequestLoding = false;
        $scope.formRequest.urgency = '';
        $scope.formRequest.deliveryLocation = '';
        $scope.formRequest.paymentTerms = '';
        $scope.formRequest.isSubmit = 0;


        $scope.titleValidation = $scope.attachmentNameValidation = false;
        $scope.deliveryLocationValidation = $scope.paymentTermsValidation = $scope.deliveryTimeValidation = false;
        $scope.urgencyValidation = $scope.quotationFreezTimeValidation = $scope.selectedCurrencyValidation = $scope.quotationPriceLimitValidation = $scope.noOfQuotationRemindersValidation = false;

        $scope.postRequest = function (isSubmit, pageNo) {

            $scope.titleValidation = $scope.attachmentNameValidation = false;
            $scope.deliveryLocationValidation = $scope.paymentTermsValidation = $scope.deliveryTimeValidation = false;
            $scope.urgencyValidation = $scope.quotationFreezTimeValidation = $scope.selectedCurrencyValidation = $scope.quotationPriceLimitValidation = $scope.noOfQuotationRemindersValidation = false;

            $scope.postRequestLoding = true;
            $scope.formRequest.isSubmit = isSubmit;

            if (isSubmit == 1 || pageNo == 1) {
                if ($scope.formRequest.title == null || $scope.formRequest.title == '') {
                    $scope.titleValidation = true;
                    $scope.postRequestLoding = false;
                    return false;
                }
                if (isSubmit == 1) {
                    if ($scope.formRequest.isTabular) {
                        $scope.formRequest.listRequirementItems.forEach(function (item, index) {
                            if (!item.attachmentName || item.attachmentName == '') {
                                $scope.attachmentNameValidation = true;
                                return false;
                            }
                        })

                    }
                    if (!$scope.formRequest.isTabular) {
                        if ($scope.formRequest.description == null || $scope.formRequest.description == '') {
                            $scope.descriptionValidation = true;
                            $scope.postRequestLoding = false;
                            return false;
                        }
                    }
                }
            }

            if (isSubmit == 1) {
                if ($scope.formRequest.deliveryLocation == null || $scope.formRequest.deliveryLocation == '') {
                    $scope.deliveryLocationValidation = true;
                    $scope.postRequestLoding = false;
                    return false;
                }
                if ($scope.formRequest.paymentTerms == null || $scope.formRequest.paymentTerms == '') {
                    $scope.paymentTermsValidation = true;
                    $scope.postRequestLoding = false;
                    return false;
                }
                if ($scope.formRequest.deliveryTime == null || $scope.formRequest.deliveryTime == '') {
                    $scope.deliveryTimeValidation = true;
                    $scope.postRequestLoding = false;
                    return false;
                }
            }

            if (isSubmit == 1) {
                if ($scope.formRequest.urgency == null || $scope.formRequest.urgency == '') {
                    $scope.urgencyValidation = true;
                    $scope.postRequestLoding = false;
                    return false;
                }
                if ($scope.formRequest.quotationFreezTime == null || $scope.formRequest.quotationFreezTime == '') {
                    $scope.quotationFreezTimeValidation = true;
                    $scope.postRequestLoding = false;
                    return false;
                }
                if (($scope.formRequest.quotationPriceLimit == null || $scope.formRequest.quotationPriceLimit == '' || $scope.formRequest.quotationPriceLimit <= '') && $scope.formRequest.isQuotationPriceLimit == true) {
                    $scope.quotationPriceLimitValidation = true;
                    $scope.postRequestLoding = false;
                    return false;
                }
                if ($scope.formRequest.isQuotationPriceLimit == false) {
                    $scope.formRequest.quotationPriceLimit = 0;
                }
                if ($scope.formRequest.noOfQuotationReminders == null || $scope.formRequest.noOfQuotationReminders == '' || $scope.formRequest.noOfQuotationReminders <= 0 || $scope.formRequest.noOfQuotationReminders > 5) {
                    $scope.noOfQuotationRemindersValidation = true;
                    $scope.postRequestLoding = false;
                    return false;
                }
                if ($scope.formRequest.remindersTimeInterval == null || $scope.formRequest.remindersTimeInterval == '' || $scope.formRequest.remindersTimeInterval <= 0) {
                    $scope.remindersTimeIntervalValidation = true;
                    $scope.postRequestLoding = false;
                    return false;
                }
            }


            if (pageNo != 4) {
                $scope.textMessage = "Save as Draft.";
            }

            if ($scope.formRequest.checkBoxEmail == true && $scope.formRequest.checkBoxSms == true && pageNo == 4) {
                $scope.textMessage = "This will send an email and sms invite to all the vendors selected above.";
            }
            else if ($scope.formRequest.checkBoxEmail == true && pageNo == 4) {
                $scope.textMessage = "This will send an email invite to all the vendors selected above.";
            }
            else if ($scope.formRequest.checkBoxSms == true && pageNo == 4) {
                $scope.textMessage = "This will send an SMS invite to all the vendors selected above.";
            }
            else if (pageNo == 4) {
                $scope.textMessage = "This will not send an SMS or EMAIL the vendors selected above.";
            }
            $scope.formRequest.currency = $scope.selectedCurrency.value;
            $scope.formRequest.timeZoneID = 190;
            swal({
                title: "Are you sure?",
                text: $scope.textMessage,
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#F44336",
                confirmButtonText: "OK",
                closeOnConfirm: true
            }, function () {
                $scope.postRequestLoding = true;
                //var ts = moment($scope.formRequest.deliveryTime, "DD-MM-YYYY HH:mm").valueOf();
                //var m = moment(ts);
                //var deliveryDate = new Date(m);
                //var milliseconds = parseInt(deliveryDate.getTime() / 1000.0);
                //$scope.formRequest.deliveryTime = "/Date(" + milliseconds + "000+0530)/";

                var ts = moment($scope.formRequest.quotationFreezTime, "DD-MM-YYYY HH:mm").valueOf();
                var m = moment(ts);
                var quotationDate = new Date(m);
                var milliseconds = parseInt(quotationDate.getTime() / 1000.0);
                $scope.formRequest.quotationFreezTime = "/Date(" + milliseconds + "000+0530)/";

                $scope.formRequest.requirementID = $stateParams.Id ? $stateParams.Id : -1;
                $scope.formRequest.customerID = userService.getUserId();
                $scope.formRequest.customerFirstname = userService.getFirstname();
                $scope.formRequest.customerLastname = userService.getLastname();
                $scope.formRequest.isClosed = "NOTSTARTED";
                $scope.formRequest.endTime = "";
                $scope.formRequest.sessionID = userService.getUserToken();
                $scope.formRequest.subcategories = "";
                $scope.formRequest.budget = 100000;
                $scope.formRequest.custCompID = userService.getUserCompanyId();
                $scope.formRequest.customerCompanyName = userService.getUserCompanyId();


                for (i = 0; i < $scope.sub.selectedSubcategories.length; i++) {
                    $scope.formRequest.subcategories += $scope.sub.selectedSubcategories[i].subcategory + ",";
                }
                var category = [];
                category.push($scope.formRequest.category);
                $scope.formRequest.category = category;
                auctionsService.postrequirementdata($scope.formRequest)
                    .then(function (response) {
                        //console.log(response);
                        if (response.objectID != 0) {

                            $scope.SaveReqDepartments(response.objectID);

                            swal({
                                title: "Done!",
                                text: "Requirement Created Successfully",
                                type: "success",
                                showCancelButton: false,
                                confirmButtonColor: "#DD6B55",
                                confirmButtonText: "Ok",
                                closeOnConfirm: true
                            },
                                function () {
                                    $scope.postRequestLoding = false;
                                    //$state.go('view-requirement');
                                    $state.go('view-requirement', { 'Id': response.objectID });
                                });

                        }
                    });
            });
            $scope.postRequestLoding = false;
        }


        $scope.ItemFile = '';
        $scope.itemSNo = 1;
        $scope.ItemFileName = '';

        $scope.itemnumber = $scope.formRequest.listRequirementItems.length;

        $scope.requirementItems =
            {
                productSNo: $scope.itemSNo++,
                ItemID: 0,
                productIDorName: '',
                productNo: '',
                productDescription: '',
                productQuantity: 0,
                productBrand: '',
                othersBrands: '',
                isDeleted: 0,
                itemAttachment: '',
                attachmentName: ''
            }
        $scope.formRequest.listRequirementItems.push($scope.requirementItems);

        $scope.AddItem = function () {
            $scope.itemnumber = $scope.formRequest.listRequirementItems.length;
            $scope.requirementItems =
                {
                    productSNo: $scope.itemSNo++,
                    ItemID: 0,
                    productIDorName: '',
                    productNo: '',
                    productDescription: '',
                    productQuantity: 0,
                    productBrand: '',
                    othersBrands: '',
                    isDeleted: 0,
                    itemAttachment: '',
                    attachmentName: ''
                }
            $scope.formRequest.listRequirementItems.push($scope.requirementItems);
        };

        $scope.deleteItem = function (SNo) {
            //$scope.formRequest.listRequirementItems[SNo].isDeleted = 0;
            //$scope.formRequest.listRequirementItems.splice(SNo, 1);
            $scope.formRequest.listRequirementItems = _.filter($scope.formRequest.listRequirementItems, function (x) { return x.productSNo !== SNo; });
        };



        $scope.getFile1 = function (id, itemid, ext) {
            $scope.file = $("#" + id)[0].files[0];

            fileReader.readAsDataUrl($scope.file, $scope)
                .then(function (result) {
                    if (id == "itemsAttachment") {
                        if (ext != "xlsx") {
                            swal("Error!", "File type should be XSLX. Please download the template and  fill values accordingly.", "error");
                            return;
                        }
                        var bytearray = new Uint8Array(result);
                        $scope.formRequest.itemsAttachment = $.makeArray(bytearray);
                        $scope.formRequest.listRequirementItems = [];
                        //$scope.formRequest.itemsAttachmentName = $scope.file.name;
                    }

                    if (id != "itemsAttachment") {
                        var bytearray = new Uint8Array(result);
                        var arrayByte = $.makeArray(bytearray);
                        var ItemFileName = $scope.file.name;
                        var index = _.indexOf($scope.formRequest.listRequirementItems, _.find($scope.formRequest.listRequirementItems, function (o) { return o.productSNo == id; }));
                        var obj = $scope.formRequest.listRequirementItems[index];
                        obj.itemAttachment = arrayByte;
                        obj.attachmentName = ItemFileName;
                        $scope.formRequest.listRequirementItems.splice(index, 1, obj);
                    }
                });
        }

        $scope.pageNo = 1;

        $scope.nextpage = function (pageNo) {
            $scope.tableValidation = false;
            $scope.deliveryLocationValidation = $scope.paymentTermsValidation = $scope.deliveryTimeValidation = departmentsValidation = false;
            $scope.urgencyValidation = $scope.quotationFreezTimeValidation = $scope.selectedCurrencyValidation = $scope.quotationPriceLimitValidation = $scope.noOfQuotationRemindersValidation = $scope.remindersTimeIntervalValidation = false;

            if (pageNo == 1 || pageNo == 4) {
                if ($scope.formRequest.title == null || $scope.formRequest.title == '') {
                    $scope.titleValidation = true;
                    $scope.postRequestLoding = false;
                    return false;
                }

                if ($scope.formRequest.isTabular) {

                    $scope.formRequest.listRequirementItems.forEach(function (item, index) {
                        if (item.productIDorName == null || item.productIDorName == '') {
                            $scope.tableValidation = true;
                        }
                        if (item.productNo == null || item.productNo == '') {
                            $scope.tableValidation = true;
                        }
                        if (item.productDescription == null || item.productDescription == '') {
                            $scope.tableValidation = true;
                        }
                        if (item.productQuantity <= 0 || item.productQuantity == undefined || item.productQuantity == '') {
                            $scope.tableValidation = true;
                        }
                        if (item.productQuantityIn == null || item.productQuantityIn == '') {
                            $scope.tableValidation = true;
                        }
                        if (item.productBrand == null || item.productBrand == '') {
                            $scope.tableValidation = true;
                        }
                    });


                    if ($scope.tableValidation) {
                        return false;
                    }



                }
                if (!$scope.formRequest.isTabular) {
                    if ($scope.formRequest.description == null || $scope.formRequest.description == '') {
                        $scope.descriptionValidation = true;
                        $scope.postRequestLoding = false;
                        return false;
                    }
                }
            }

            if (pageNo == 2 || pageNo == 4) {
                if ($scope.formRequest.deliveryLocation == null || $scope.formRequest.deliveryLocation == '') {
                    $scope.deliveryLocationValidation = true;
                    $scope.postRequestLoding = false;
                    return false;
                }
                if ($scope.formRequest.paymentTerms == null || $scope.formRequest.paymentTerms == '') {
                    $scope.paymentTermsValidation = true;
                    $scope.postRequestLoding = false;
                    return false;
                }
                if ($scope.formRequest.deliveryTime == null || $scope.formRequest.deliveryTime == '') {
                    $scope.deliveryTimeValidation = true;
                    $scope.postRequestLoding = false;
                    return false;
                }
                if ($scope.reqDepartments.length > 0) {

                    $scope.departmentsValidation = true;

                    $scope.reqDepartments.forEach(function (item, index) {
                        if (item.isValid == true || $scope.noDepartments == true)
                            $scope.departmentsValidation = false;
                    });

                    if ($scope.departmentsValidation == true) {
                        return false;
                    };

                }
            }

            if (pageNo == 3 || pageNo == 4) {
                if ($scope.formRequest.urgency == null || $scope.formRequest.urgency == '') {
                    $scope.urgencyValidation = true;
                    $scope.postRequestLoding = false;
                    return false;
                }
                if ($scope.formRequest.quotationFreezTime == null || $scope.formRequest.quotationFreezTime == '') {
                    $scope.quotationFreezTimeValidation = true;
                    $scope.postRequestLoding = false;
                    return false;
                }                

                if (($scope.formRequest.quotationPriceLimit == null || $scope.formRequest.quotationPriceLimit == '' || $scope.formRequest.quotationPriceLimit <= 0) && $scope.formRequest.isQuotationPriceLimit == true) {
                    $scope.quotationPriceLimitValidation = true;
                    $scope.postRequestLoding = false;
                    return false;
                }
                if ($scope.formRequest.noOfQuotationReminders == null || $scope.formRequest.noOfQuotationReminders == '' || $scope.formRequest.noOfQuotationReminders <= 0 || $scope.formRequest.noOfQuotationReminders > 5) {
                    $scope.noOfQuotationRemindersValidation = true;
                    $scope.postRequestLoding = false;
                    return false;
                }
                if ($scope.formRequest.remindersTimeInterval == null || $scope.formRequest.remindersTimeInterval == '' || $scope.formRequest.remindersTimeInterval <= 0) {
                    $scope.remindersTimeIntervalValidation = true;
                    $scope.postRequestLoding = false;
                    return false;
                }
            }

            if (pageNo == 4) {
                $scope.formRequest.subcategoriesView = '';
                for (i = 0; i < $scope.sub.selectedSubcategories.length; i++) {
                    $scope.formRequest.subcategoriesView += $scope.sub.selectedSubcategories[i].subcategory + ",";
                }
            }

            $scope.pageNo = $scope.pageNo + 1;
            // return $scope.pageNo;
            // location.reload();
        }

        $scope.previouspage = function () {
            $scope.pageNo = $scope.pageNo - 1;
            // location.reload();
            //return $scope.pageNo;
        }

        $scope.gotopage = function (pageNo) {
            $scope.pageNo = pageNo;
            return $scope.pageNo;
        }


        $scope.reqDepartments = [];
        $scope.userDepartments = [];

        $scope.GetReqDepartments = function () {
            $scope.reqDepartments = [];
            auctionsService.GetReqDepartments(userService.getUserId(), $scope.stateParamsReqID, userService.getUserToken())
                .then(function (response) {
                    if (response && response.length > 0) {
                        $scope.reqDepartments = response;

                        $scope.noDepartments = true;

                        $scope.reqDepartments.forEach(function (item, index) {
                            if (item.isValid == true) {
                                $scope.noDepartments = false;
                            }
                        });

                    }
                });
        }

        $scope.GetReqDepartments();


        $scope.SaveReqDepartments = function (reqID) {

            $scope.reqDepartments.forEach(function (item, index) {
                item.reqID = reqID;
                item.userID = userService.getUserId();
            })

            var params = {
                "listReqDepartments": $scope.reqDepartments,
                sessionID: userService.getUserToken()
            };

            auctionsService.SaveReqDepartments(params)
                .then(function (response) {
                    if (response.errorMessage != '') {
                        growlService.growl(response.errorMessage, "inverse");
                    }
                    else {
                        growlService.growl(".", "success");
                        //$scope.GetCompanyDepartments();
                        //$scope.addnewdeptView = false;
                        //$scope.department = {
                        //    userID: $scope.userID,
                        //    deptID: 0,
                        //    sessionID: $scope.sessionID
                        //};
                        ////$window.history.back();
                    }
                });

        };

        $scope.noDepartments = false;


        $scope.noDepartmentsFunction = function (value) {

            $scope.departmentsValidation = false;

            if (value == 'NA') {
                $scope.reqDepartments.forEach(function (item, index) {
                    item.isValid = false;
                });
            }
            else {
                $scope.reqDepartments.forEach(function (item, index) {
                    if (item.isValid == true)
                        $scope.noDepartments = false;
                });
            }
        };

        
    });prmApp
    //=================================================
    // LOGIN
    //=================================================

    .controller('frgtPassCtrl', function ($timeout, $state, $scope, growlService, userService, auctionsService, $http, domain, $rootScope, fileReader) {

        //Status
        $scope.login = 1;
        $scope.register = 0;
        $scope.otp = 0;
        $scope.otpvalue = 0;
        $scope.verification = 0;
        $scope.verificationObj = {};
        $scope.otpobj = {};
        $scope.register_vendor = 0;
        $scope.forgot = 0;
        $scope.forgotpassword = {};
        $scope.loggedIn = userService.isLoggedIn();
        $scope.userError = {};
        $scope.user = {};
        $scope.checkEmailUniqueResult = false;
        $scope.checkPhoneUniqueResult = false;
        $scope.vendorregisterobj = $scope.registerobj = {};
        $scope.otpvalueValidation = false;
        $scope.otpvalueValidationEmpty = false;
        $scope.otpvalueValidationError = false;
        $scope.categories = [];

        $scope.registrationbtn = function () {
            $scope.register = 1;
            $scope.registerobj = {};
            $scope.otp = $scope.forgot = $scope.register_vendor = $scope.login = 0;
            $scope.checkEmailUniqueResult = false;
            $scope.checkPhoneUniqueResult = false;
        };
        $scope.loginbtn = function () {
            $scope.login = 1;
            $scope.user = {};
            $scope.otp = $scope.forgot = $scope.register_vendor = $scope.register = 0;
        };
        $scope.forgotbtn = function () {
            $scope.otp = $scope.login = $scope.register_vendor = $scope.register = 0;
            $scope.forgot = 1;
        };

        $scope.sendOTPagain = function () {
            userService.resendotp(userService.getUserId());
        };
        $scope.vendorregistrationbtn = function () {
            $scope.otp = $scope.login = $scope.forgot = $scope.register = 0;
            $scope.vendorregisterobj = {};
            $scope.register_vendor = 1;
            $scope.checkEmailUniqueResult = false;
            $scope.checkPhoneUniqueResult = false;
        };

        $scope.PhoneValidate = function () {
            //console.log("siva  1111111---->"); 
            var phoneno = /^\d{10}$/;
            var input = $scope.registerobj.phoneNum;
            if (input = phoneno) {
                //console.log("siva  2222222---->");
                return true
                //console.log("siva  2222222---->");

            }

        };

        function validate(evt) {
            var theEvent = evt || window.event;
            var key = theEvent.keyCode || theEvent.which;
            key = String.fromCharCode(key);
            var regex = /[0-9]|\./;
            if (!regex.test(key)) {
                theEvent.returnValue = false;
                if (theEvent.preventDefault) theEvent.preventDefault();
            }
        }


        $scope.PhoneValidate1 = function () {
            if ($scope.vendorregisterobj.phoneNum != "" && isNaN($scope.vendorregisterobj.phoneNum)) {
                return false;
            }
        };


        $scope.EmailValidate = function () {
            var re = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/
            var result = re.test($scope.registerobj.email);
            if (!result) {
                $scope.showMessage = true;
                $scope.msg = $scope.getErrMsg("Please enter a valid Email Address");
            } else {
                $scope.showMessage = false;
                $scope.msg = '';
            }
        }


        $scope.EmailValidateVendor = function () {
            var re = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/
            var result = re.test($scope.vendorregisterobj.email);
            if (!result) {
                $scope.showMessage = true;
                $scope.msg = $scope.getErrMsg("Please enter a valid Email Address");
            } else {
                $scope.showMessage = false;
                $scope.msg = '';
            }
        }


        $scope.userregistration = function () {
            var re = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/
            var result = re.test($scope.registerobj.email);
            if (isNaN($scope.registerobj.phoneNum)) {
                $scope.showMessage = true;
                $scope.msg = $scope.getErrMsg("Please enter a valid Phone Number");
            } else if (!result) {
                $scope.showMessage = true;
                $scope.msg = $scope.getErrMsg("Please enter a valid Email Address");
            } else {
                $scope.showMessage = false;
                $scope.msg = '';
            }
            if (!$scope.showMessage) {
                $scope.registerobj.username = $scope.registerobj.email;
                userService.userregistration($scope.registerobj).then(function (error) {
                    $scope.loggedIn = userService.isLoggedIn();
                    if (error.errorMessage != "") {
                        $scope.showMessage = true;
                        $scope.msg = $scope.getErrMsg(error);
                    } else {
                        $scope.otp = 1;
                        $scope.forgot = $scope.login = $scope.register_vendor = $scope.register = 0;
                    }
                });
            }
        };

        $scope.getCategories = function () {
            auctionsService.getCategories()
                .then(function (response) {
                    $scope.categories = response;
                })
        }

        $scope.getCategories();

        $scope.docsVerification = function () {
            var params = { 'userID': userService.getUserId(), 'files': $scope.CredentialUpload, 'sessionID': userService.getUserToken() };
            $http({
                method: 'POST',
                url: domain + 'updatecredentials',
                encodeURI: true,
                headers: { 'Content-Type': 'application/json' },
                data: params
            }).then(function (response) {
                //console.log(response);
                if (response && response.data && response.data.errorMessage == "") {
                    $state.go('pages.profile.profile-about');
                    growlService.growl('Welcome to PRM360! Your credentials are being verified. Our associates will contact you as soon as it is done.', 'inverse');
                } else {
                    //console.log(response.data[0].errorMessage);
                }
            }, function (result) {
                //console.log("there is no current auctions");
            });
        }

        $scope.verifyOTP = function () {
            $scope.otpvalue = $scope.otpobj.otp;
            $scope.otpvalue.phone = $scope.registerobj.phoneNum;
            if ($scope.otpvalue == "") {
                $scope.otpvalueValidation = true;
                $scope.otpvalueValidationEmpty = true;
            } else {
                $scope.otpvalueValidationEmpty = false;
                $scope.otpvalueValidation = false;
            }
            if (isNaN($scope.otpvalue)) {
                $scope.otpvalueValidationError = true;
                $scope.otpvalueValidation = true;
            } else {
                $scope.otpvalueValidationError = false;
                $scope.otpvalueValidation = false;
            }
            if (!$scope.otpvalueValidation) {
                userService.verifyOTP($scope.otpvalue)
                    .then(function (response) {
                        if (response.errorMessage == "") {
                            if (response.userInfo.isOTPVerified == 1) {
                                $scope.isOTPVerified = 1;

                                swal("Done!", "Mobile OTP Verified successfully.", "success");
                            } else {
                                $scope.isOTPVerified = 0;
                                swal("Warning", "Please enter valid OTP", "warning");
                            }
                        } else {
                            swal("Warning", response.errorMessage, "warning");
                        }
                    });
            }
        };

        $scope.getFile1 = function (id, doctype, ext) {
            $scope.progress = 0;
            $scope.file = $("#" + id)[0].files[0];
            $scope.docType = doctype + "." + ext;
            fileReader.readAsDataUrl($scope.file, $scope)
                .then(function (result) {
                    var bytearray = new Uint8Array(result);
                    var fileobj = {};
                    fileobj.fileStream = $.makeArray(bytearray);
                    fileobj.fileType = $scope.docType;
                    fileobj.isVerified = 0;
                    //$scope.verificationObj.attachmentName=$scope.file.name;
                    $scope.CredentialUpload.push(fileobj);
                });
            ////console.log($scope.CredentialUpload);
        };

        $scope.changePhoneNumber = function () {
            $scope.newphonenumber = $("#newphonenumber").val();
            //console.log($scope.newphonenumber);
            if ($scope.newphonenumber == "") {
                $scope.newphonenumber_errors = true;
                $scope.newphonenumber_required_error = true;
                return false;
            } else {
                $scope.newphonenumber_errors = false;
                $scope.newphonenumber_required_error = false;
            }
            if (isNaN($scope.newphonenumber)) {
                $scope.newphonenumber_errors = true;
                $scope.newphonenumber_validation_error = true;
                return false;
            } else {
                $scope.newphonenumber_errors = false;
                $scope.newphonenumber_validation_error = false;
            }
            if (!$scope.newphonenumber_errors) {
                var userinfo = userService.getUserObj();
                userinfo.phoneNum = $scope.newphonenumber;
                userinfo.sessionID = userService.getUserToken();
                userinfo.emailAddress = userinfo.email;
                userinfo.aboutUs = "";
                userinfo.companyName = "";
                userinfo.logoFile = { "fileName": '', 'fileStream': "" };
                userService.updateUser(userinfo);
                $scope.changepasswordstatus = false;
            }
        };

        $scope.vendorregistration = function () {
            if (isNaN($scope.vendorregisterobj.phoneNum)) {
                $scope.showMessage = true;
                $scope.msg = $scope.getErrMsg("Please enter a valid Phone Number");
            } else {
                $scope.showMessage = false;
                $scope.msg = '';
            }
            $scope.vendorregisterobj.username = $scope.vendorregisterobj.email;
            userService.vendorregistration($scope.vendorregisterobj).then(function (error) {
                $scope.loggedIn = userService.isLoggedIn();
                /*if (error) {
                    $scope.showMessage = true;
                    $scope.msg = $scope.getErrMsg(error);
                }else{
                   $scope.otp =1;  
                   $scope.forgot = $scope.login = $scope.register_vendor = $scope.register = 0;                     
                }*/
                if (error.errorMessage != "") {
                    $scope.showMessage = true;
                    $scope.msg = $scope.getErrMsg(error);
                } else {
                    $scope.otp = 1;
                    $scope.forgot = $scope.login = $scope.register_vendor = $scope.register = 0;
                }
            });
        };

        $scope.RegisterVendor = function () {
            //console.log($scope.vendorregisterobj);
            if (isNaN($scope.vendorregisterobj.phoneNum)) {
                $scope.showMessage = true;
                $scope.msg = $scope.getErrMsg("Please enter a valid Phone Number");
            } else {
                $scope.showMessage = false;
                $scope.msg = '';
            }
            if (!$scope.showMessage) {
                $scope.vendorregisterobj.username = $scope.vendorregisterobj.email;
                userService.vendorregistration($scope.vendorregisterobj).then(function (error) {
                    $scope.loggedIn = userService.isLoggedIn();
                    if (error.errorMessage != "") {
                        $scope.showMessage = true;
                        $scope.msg = $scope.getErrMsg(error);
                    } else {
                        $scope.otp = 1;
                        $scope.forgot = $scope.login = $scope.register_vendor = $scope.register = 0;
                    }
                });
            }
        }


        $scope.RegisterVendor1 = function () {
            //console.log($scope.vendorregisterobj);
            if (isNaN($scope.vendorregisterobj.phoneNum)) {
                $scope.showMessage = true;
                $scope.msg = $scope.getErrMsg("Please enter a valid Phone Number");
            } else {
                $scope.showMessage = false;
                $scope.msg = '';
            }
            if (!$scope.showMessage) {
                $scope.vendorregisterobj.username = $scope.vendorregisterobj.email;
                userService.vendorregistration1($scope.vendorregisterobj).then(function (error) {
                    $scope.loggedIn = userService.isLoggedIn();
                    if (error.errorMessage != "") {
                        $scope.showMessage = true;
                        $scope.msg = $scope.getErrMsg(error);
                    } else {
                        $scope.otp = 1;
                        $scope.forgot = $scope.login = $scope.register_vendor = $scope.register = 0;
                    }
                });
            }
        }



        $scope.checkUserUniqueResult = function (idtype, inputvalue) {
            if (inputvalue == "" || inputvalue == undefined) {
                return false;
            }
            $scope.checkPhoneUniqueResult = false;
            $scope.checkEmailUniqueResult = false;
            userService.checkUserUniqueResult(inputvalue, idtype).then(function (response) {
                if (idtype == "PHONE") {
                    $scope.checkPhoneUniqueResult = !response;
                } else if (idtype == "EMAIL") {
                    $scope.checkEmailUniqueResult = !response;
                }
            });
        };
        $scope.forgotpasswordfunction = function () {
            userService.forgotpassword($scope.forgotpassword)
                .then(function (response) {
                    if (response.data.errorMessage == "") {
                        $scope.forgotpassword = {};
                        swal("Done!", "Email sent to your registered email.", "success");
                        $scope.login = 1;
                        $scope.user = {};
                        $scope.forgot = $scope.register_vendor = $scope.register = 0;
                    } else {
                        swal("Warning", "Please check the email address you have entered.", "warning");
                    }
                });
        };


        $scope.closeMsg = function () {
            $scope.showMessage = false;
        }
        $scope.loginSubmit = function () {
            userService.login($scope.user).then(function (error) {
                $scope.loggedIn = userService.isLoggedIn();
                if (error.errorMessage != "") {
                    $scope.showMessage = true;
                    $scope.msg = $scope.getErrMsg(error);
                } else if (error.userInfo.credentialsVerified == 0 || error.userInfo.isOTPVerified == 0) {
                    $state.go('pages.profile.profile-about');
                    //$scope.otp =1;  
                    //$scope.verification =$scope.forgot = $scope.login = $scope.register_vendor = $scope.register = 0; 
                }
                //else if(error.userInfo.credentialsVerified==0){
                //     $scope.verification =1;  
                //     $scope.otp =$scope.forgot = $scope.login = $scope.register_vendor = $scope.register = 0; 
                // }
            });
        };
    });prmApp
    // =========================================================================
    // Header
    // =========================================================================
    .controller('headerCtrl', function ($timeout, messageService, $scope, userService) {


        // Top Search
        this.openSearch = function () {
            angular.element('#header').addClass('search-toggled');
            angular.element('#top-search-wrap').find('input').focus();
        }

        $scope.showAddNewReq = function () {
            if (window.location.hash != "#/login" && userService.getUserType() == "CUSTOMER") {
                return true;
            } else {
                return false;
            }
        }



        $scope.showIsUserVendor = function () {
            if (window.location.hash != "#/login" && userService.getUserType() == "VENDOR") {
                return true;
            } else {
                return false;
            }
        }



        this.closeSearch = function () {
            angular.element('#header').removeClass('search-toggled');
        }

        // Get messages and notification for header
        this.img = messageService.img;
        this.user = messageService.user;
        this.user = messageService.text;

        /* this.messageResult = messageService.getMessage(this.img, this.user, this.text);*/


        //Clear Notification
        this.clearNotification = function ($event) {
            $event.preventDefault();

            var x = angular.element($event.target).closest('.listview');
            var y = x.find('.lv-item');
            var z = y.size();

            angular.element($event.target).parent().fadeOut();

            x.find('.list-group').prepend('<i class="grid-loading hide-it"></i>');
            x.find('.grid-loading').fadeIn(1500);
            var w = 0;

            y.each(function () {
                var z = $(this);
                $timeout(function () {
                    z.addClass('animated fadeOutRightBig').delay(1000).queue(function () {
                        z.remove();
                    });
                }, w += 150);
            })

            $timeout(function () {
                angular.element('#notifications').addClass('empty');
            }, (z * 150) + 200);
        }

        // Clear Local Storage
        this.clearLocalStorage = function () {

            //Get confirmation, if confirmed clear the localStorage
            swal({
                title: "Are you sure?",
                text: "All your saved localStorage values will be removed",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#F44336",
                confirmButtonText: "Yes, delete it!",
                closeOnConfirm: false
            }, function () {
                localStorage.clear();
                swal("Done!", "localStorage is cleared", "success");
            });

        }

        //Fullscreen View
        this.fullScreen = function () {
            //Launch
            function launchIntoFullscreen(element) {
                if (element.requestFullscreen) {
                    element.requestFullscreen();
                } else if (element.mozRequestFullScreen) {
                    element.mozRequestFullScreen();
                } else if (element.webkitRequestFullscreen) {
                    element.webkitRequestFullscreen();
                } else if (element.msRequestFullscreen) {
                    element.msRequestFullscreen();
                }
            }

            //Exit
            function exitFullscreen() {
                if (document.exitFullscreen) {
                    document.exitFullscreen();
                } else if (document.mozCancelFullScreen) {
                    document.mozCancelFullScreen();
                } else if (document.webkitExitFullscreen) {
                    document.webkitExitFullscreen();
                }
            }

            if (exitFullscreen()) {
                launchIntoFullscreen(document.documentElement);
            }
            else {
                launchIntoFullscreen(document.documentElement);
            }
        }

    });﻿prmApp

    // =========================================================================
    // AUCTION ITEM
    // =========================================================================

    .controller('itemCtrl', function ($scope, $rootScope, $filter, $stateParams, $http, domain, fileReader, $state, $timeout, auctionsService, userService, SignalRFactory, growlService, $log, signalRHubName, ngDialog) {

        var id = $stateParams.Id;
        $scope.reqId = $stateParams.Id;

        $scope.isCustomer = userService.getUserType() == "CUSTOMER" ? true : false;

        if ($scope.isCustomer) {
            auctionsService.GetIsAuthorized(userService.getUserId(), $scope.reqId, userService.getUserToken())
                .then(function (response) {
                    if (response.errorMessage != "") {
                        growlService.growl(response.errorMessage, "inverse");
                        $state.go('home');
                        return false;
                    }
                });
        }

        $scope.Loding = false;
        $scope.makeaBidLoding = false;
        $scope.showTimer = false;
        $scope.userIsOwner = false;
        $scope.sessionid = userService.getUserToken();
        $scope.allItemsSelected = true;
        $scope.RevQuotationfirstvendor = "";
        $scope.disableBidButton = false;

        $scope.nonParticipatedMsg = '';
        $scope.quotationRejecteddMsg = '';
        $scope.quotationNotviewedMsg = '';
        $scope.revQuotationRejecteddMsg = '';
        $scope.revQuotationNotviewedMsg = '';
        $scope.quotationApprovedMsg = '';
        $scope.revQuotationApprovedMsg = '';

        $scope.reduceBidAmountNote = '';
        $scope.incTaxBidAmountNote = '';
        $scope.noteForBidValue = '';
        $scope.disableDecreaseButtons = true;
        $scope.disableAddButton = true;
        $scope.NegotiationEnded = false;

        $scope.uploadQuotationButtonMsg = 'The Submit Quotation option would be available only if prices are entered above.';
        $scope.uploadQuotationTaxValidationMsg = 'Please check all Tax Feilds';

        $scope.currentID = -1;
        $scope.timerStyle = { 'color': '#000' };
        $scope.savingsStyle = { 'color': '#228B22' };
        $scope.restartStyle = { 'color': '#f00' };
        //$scope.signalR.on('updateTime',function(data){
        $scope.bidAttachement = [];
        $scope.bidAttachementName = "";
        $scope.bidAttachementValidation = false;
        $scope.bidPriceEmpty = false;
        $scope.bidPriceValidation = false;
        $scope.showStatusDropDown = false;
        $scope.vendorInitialPrice = 0;
        $scope.showGeneratePOButton = false;
        $scope.isDeleted = false;
        $scope.ratingForVendor = 0;
        $scope.ratingForCustomer = 0;
        $scope.participatedVendors = [];
        var requirementHub;
        // $scope.isTimerStarted = false;

        $scope.auctionItem = {
            auctionVendors: [],
            listRequirementItems: [],
            listRequirementTaxes: []
        }

        var requirementData = {};

        $scope.quotationAttachment = null;

        $scope.days = 0;
        $scope.hours = 0;
        $scope.mins = 0;

        $scope.NegotiationSettings = {
            negotiationDuration: ''
        }

        $scope.NegotiationSettings.negotiationDuration = $scope.days + ' ' + $scope.hours + ':' + $scope.mins + ':00.0';

        $scope.auctionItem.reqType = 'REGULAR';
        $scope.auctionItem.priceCapValue = 0;
        $scope.auctionItem.isUnitPriceBidding = 0;

        $log.info('trying to connect to service')
        requirementHub = SignalRFactory('', signalRHubName);
        $log.info('connected to service')

        $scope.$on("$destroy", function () {
            $log.info('disconecting signalR')
            requirementHub.stop();
            $log.info('disconected signalR')
        });

        /*$.ajax({
            url: "http://www.timeapi.org/ist/now.json", 
            type: "GET",   
            dataType: 'jsonp',
            cache: false,
            success: function(response){
                alert(response.dateString);                   
            }           
        }); */

        $scope.reconnectHub = function () {
            if (requirementHub) {
                if (requirementHub.getStatus() == 0) {
                    requirementHub.reconnect();
                    return true;
                }
            } else {
                requirementHub = SignalRFactory('', signalRHubName);
            }

        }

        $scope.checkConnection = function () {
            if (requirementHub) {
                return requirementHub.getStatus();
            } else {
                return 0;
            }
        }

        $scope.invokeSignalR = function (methodName, params) {
            if ($scope.checkConnection() == 1) {
                requirementHub.invoke(methodName, params, function (req) {

                })
            } else {
                $scope.reconnectHub();
                requirementHub.invoke(methodName, params, function (req) {

                })
            }
        }

        $scope.clickToOpen = function () {
            ngDialog.open({ template: 'login/termsUpdateStartTime.html', width: 1000 });
        };

        $scope.updateTimeLeftSignalR = function () {
            var parties = id + "$" + userService.getUserId() + "$" + "10000" + "$" + userService.getUserToken();
            $scope.invokeSignalR('UpdateTime', parties);
            //requirementHub.invoke('UpdateTime', parties, function (req) {
            //    //$log.info(req);
            //})
        };

        $scope.newPriceToBeQuoted = 0;

        $scope.reduceBidAmount = function () {
            if (!isNaN($scope.reduceBidValue) && $scope.reduceBidValue != "" && $scope.reduceBidValue >= 0 && $scope.auctionItem.minPrice > $scope.reduceBidValue) {
                $("#reduceBidValue").val($scope.reduceBidValue);


                $("#makebidvalue").val(($scope.auctionItem.minPrice - $scope.reduceBidValue).toFixed(4));
                $("#reduceBidValue1").val(($scope.auctionItem.minPrice - $scope.reduceBidValue).toFixed(4));
                //angular.element($event.target).parent().addClass('fg-line fg-toggled');
            }
            else {
                $("#makebidvalue").val($scope.auctionItem.minPrice);
                //swal("Error!", "Invalid bid value.", "error");
                $scope.reduceBidValue = 0;
                $("#reduceBidValue").val($scope.reduceBidValue);
            }


        };

        $scope.bidAmount = function () {
            if ($scope.vendorBidPrice != "" && $scope.vendorBidPrice >= 0 && $scope.auctionItem.minPrice > $scope.vendorBidPrice) {
                $("#reduceBidValue").val($scope.auctionItem.minPrice - $scope.vendorBidPrice);
                /*angular.element($event.target).parent().addClass('fg-line fg-toggled');*/
            }
            else {
                $("#reduceBidValue").val(0);
                swal("Error!", "Invalid bid value.", "error");
                $scope.vendorBidPrice = 0;
            }
        };

        $scope.formRequest = {
            selectedVendor: {}
        };

        $scope.selectVendor = function () {
            var selVendID = $scope.formRequest.selectedVendor.vendorID;
            var winVendID = $scope.auctionItem.auctionVendors[0].vendorID;
            if (!$scope.formRequest.selectedVendor.reason && selVendID != winVendID) {
                growlService.growl("Please enter the reason for choosing the particular vendor", "inverse");
                return false;
            }
            var params = {
                userID: userService.getUserId(),
                vendorID: $scope.formRequest.selectedVendor.vendorID,
                reqID: $scope.auctionItem.requirementID,
                reason: $scope.formRequest.selectedVendor.reason ? $scope.formRequest.selectedVendor.reason : (selVendID != winVendID ? $scope.formRequest.selectedVendor.reason : ""),
                sessionID: userService.getUserToken()
            }
            auctionsService.selectVendor(params)
                .then(function (response) {
                    if (response.errorMessage == "") {
                        growlService.growl("Vendor " + response.userInfo.firstName + " " + response.userInfo.lastName + " has been selected for the final  ", "inverse");
                        $scope.getData();
                    }
                })
        }

        $scope.exportItemsToExcel = function () {
            var mystyle = {
                sheetid: 'QuotationDetails',
                headers: true,
                column: {
                    style: 'font-size:15px;background:#233646;color:#FFF;'
                }
            };

            alasql('SELECT itemID as [ItemID], productIDorName as [ProductName], productNo as [ProductNumber], productDescription as [Description], productQuantity as [Quantity], productBrand as Brand, productQuantityIn as [Units],unitPrice as UnitPrice, price as Price INTO XLSX(?,{headers:true,sheetid: "QuotationDetails", style: "font-size:15px;background:#233646;color:#FFF;"}) FROM ?', ["QuotationDetails.xlsx", $scope.auctionItem.listRequirementItems]);
        }

        $scope.rateVendor = function (vendorID) {
            var params = {
                uID: userService.getUserId(),
                userID: vendorID,
                rating: $scope.ratingForVendor,
                sessionID: userService.getUserToken()
            }
            auctionsService.rateVendor(params)
                .then(function (response) {
                    if (response.errorMessage == "") {
                        growlService.growl("Rating saved successfully. You can edit your rating as well.", "inverse");
                    }
                })
        }

        $scope.rateCustomer = function () {
            var params = {
                uID: userService.getUserId(),
                userID: $scope.auctionItem.customerID,
                rating: $scope.ratingForCustomer,
                sessionID: userService.getUserToken()
            }
            auctionsService.rateVendor(params)
                .then(function (response) {
                    if (response.errorMessage == "") {
                        growlService.growl("Rating saved successfully. You can edit your rating as well.", "inverse");
                    }
                })
        }

        $scope.makeABidDisable = false;

        $scope.makeaBid1 = function () {
            $scope.makeABidDisable = true;
            var bidPrice = $("#makebidvalue").val();
            //bidPrice = bidPrice - (bidPrice * $scope.auctionItem.auctionVendors[0].taxes / 100);
            //$log.info($scope.auctionItem.minPrice);
            $log.info(bidPrice);

            if (bidPrice == "" || Number(bidPrice) <= 0) {
                $scope.bidPriceEmpty = true;
                $scope.bidPriceValidation = false;
                $("#makebidvalue").val("");
                $scope.makeABidDisable = false;
                return false;
            } else if (!isNaN($scope.auctionItem.minPrice) && $scope.auctionItem.minPrice > 0 && bidPrice >= $scope.auctionItem.minPrice) {
                $scope.bidPriceValidation = true;
                $scope.bidPriceEmpty = false;
                $scope.makeABidDisable = false;
                $scope.makeABidDisable = false;
                return false;
            } else {
                $scope.bidPriceValidation = false;
                $scope.bidPriceEmpty = false;
            }
            if (($scope.bidAttachementName == "" || $scope.bidAttachement.length == 0) && $scope.quotationStatus == false) {
                $scope.bidAttachementValidation = true;
                $scope.makeABidDisable = false;
                return false;
            } else {
                $scope.bidAttachementValidation = false;
            }
            if (bidPrice > $scope.auctionItem.auctionVendors[0].runningPrice - $scope.auctionItem.minBidAmount) {
                swal("Error!", "Your amount must be at least " + $scope.auctionItem.minBidAmount + " less than your previous bid.", 'error');
                $scope.makeABidDisable = false;
                return false;
            }
            if (bidPrice < (90 * $scope.auctionItem.auctionVendors[0].runningPrice / 100)) {

                swal("Error!", "You are reducing more than 10% of current bid amount. The Maximum reduction amount should not exceed more than 10% from current bid amount" + bidPrice + ".", "error");
                $scope.makeABidDisable = false;
                return false;
                //swal({
                //    title: "Are you sure?",
                //    text: "You are reducing more than 5% of current bid amount. Are you sure you want to go ahead with the bid amount " + bidPrice + "?",
                //    type: "warning",
                //    showCancelButton: true,
                //    confirmButtonColor: "#F44336",
                //    confirmButtonText: "OK",
                //    closeOnConfirm: true
                //}, function () {
                //    var params = {};
                //    params.reqID = parseInt(id);
                //    params.sessionID = userService.getUserToken();
                //    params.userID = parseInt(userService.getUserId());
                    
                //    params.price = parseFloat(bidPrice);
                //    params.quotationName = $scope.bidAttachementName;
                    
                //    requirementHub.invoke('MakeBid', params, function (req) {
                        
                //        if (req.errorMessage == '') {
                //            swal("Thanks !", "Your bidding process has been successfully updated", "success");
                //            $scope.reduceBidValue = "";
                //            $("#makebidvalue").val("");
                //            $("#reduceBidValue").val("");
                            
                //        } else {
                //            if (req.errorMessage == 'The value you quoted is too close to another bid. Please quote a lower price.') {
                //                var parties = id + "$" + userService.getUserId() + "$" + userService.getUserToken();
                //                requirementHub.invoke('CheckRequirement', parties, function () {
                //                    swal("Error!", req.errorMessage, "error");
                //                    $scope.reduceBidValue = "";
                //                    $("#makebidvalue").val("");
                //                    $("#reduceBidValue").val("");
                //                });

                //            } else {
                //                swal("Error!", req.errorMessage, "error");
                //            }
                //        }
                //    });
                //});
            }
            else {
                // function () {
                var params = {};
                params.reqID = parseInt(id);
                params.sessionID = userService.getUserToken();
                params.userID = parseInt(userService.getUserId());
                //bidPrice = bidPrice - (bidPrice * $scope.auctionItem.auctionVendors[0].taxes / 100);
                params.price = parseFloat(bidPrice);
                params.quotationName = $scope.bidAttachementName;

                //$log.info(params);
                //var parties = params.reqID + "$" + params.userID + "$" + params.price + "$" + params.quotation + "$" + params.quotationName + "$" + params.sessionID;

                if (1 == 1) {
                    $scope.items = {
                        itemsList: $scope.auctionItem.listRequirementItems,
                        userID: userService.getUserId(),
                        reqID: params.reqID,
                        price : $scope.revtotalprice,
                        vendorBidPrice: $scope.revvendorBidPrice,
                        freightcharges: $scope.revfreightcharges,
                        
                    };

                    auctionsService.SaveRunningItemPrice($scope.items)
                        .then(function (response) {
                            if (response.errorMessage != "") {
                                growlService.growl(response.errorMessage, "inverse");
                            }
                        });
                }


                requirementHub.invoke('MakeBid', params, function (req) {
                    //auctionsService.makeabid(params).then(function(req){
                    if (req.errorMessage == '') {
                        $scope.makeABidDisable = false;
                        swal("Thanks !", "Your bidding process has been successfully updated", "success");

                        $scope.reduceBidValue = "";
                        $("#makebidvalue").val("");
                        $("#reduceBidValue").val("");
                        //$scope.getData();
                        // $(".removeattachedquotes").trigger('click');
                    } else {
                        if (req.errorMessage == 'The value you quoted is too close to another bid. Please quote a lower price.') {
                            $scope.makeABidDisable = false;
                            var parties = id + "$" + userService.getUserId() + "$" + userService.getUserToken();
                            requirementHub.invoke('CheckRequirement', parties, function () {
                                swal("Error!", req.errorMessage, "error");
                                $scope.reduceBidValue = "";
                                $("#makebidvalue").val("");
                                $("#reduceBidValue").val("");
                                //$scope.getData();
                            });

                        } else {
                            $scope.makeABidDisable = false;
                            swal("Error!", req.errorMessage, "error");
                        }
                    }
                });
                // });
            }
            //return false;
            //$scope.auctionItem.minPrice      
        }

        $scope.recalculate = function () {
            var params = {};
            params.reqID = id;
            params.sessionID = userService.getUserToken();
            params.userID = userService.getUserId();
            var parties = id + "$" + userService.getUserId() + "$" + userService.getUserToken();
            $scope.invokeSignalR('CheckRequirement', parties);
            swal("Done!", "A refresh command has been sent to everyone.", "success");
            //requirementHub.invoke('CheckRequirement', parties, function () {
            //    $scope.getData();
            //    //$scope.$broadcast('timer-set-countdown-seconds', 0);
            //    swal("Done!", "A refresh command has been sent to everyone.", "success");
            //});
        }


        //return false;
        //$scope.auctionItem.minPrice


        $scope.setFields = function () {
            if ($scope.auctionItem.status == "CLOSED") {
                $scope.mactrl.skinSwitch('green');
                if (($scope.auctionItem.customerID == userService.getUserId() || $scope.auctionItem.superUserID == userService.getUserId() || $scope.auctionItem.customerReqAccess)) {
                    $scope.errMsg = "Negotiation has been completed. You can generate the Purchase Order by pressing the button below.";
                }
                else {
                    $scope.errMsg = "Negotiation has completed.";
                }
                $scope.showStatusDropDown = false;
                $scope.showGeneratePOButton = true;
            } else if ($scope.auctionItem.status == "UNCONFIRMED" || $scope.auctionItem.status == "NOTSTARTED") {
                $scope.mactrl.skinSwitch('teal');
                $scope.errMsg = "Negotiation has not started yet.";
                $scope.showStatusDropDown = false;
                $scope.auctionStarted = false;
                $scope.timeLeftMessage = "Negotiation Starts in: ";
                $scope.startBtns = true;
                $scope.customerBtns = false;
            } else if ($scope.auctionItem.status == "STARTED") {
                $scope.mactrl.skinSwitch('orange');
                $scope.errMsg = "Negotiation has started.";
                $scope.showStatusDropDown = false;
                $scope.auctionStarted = true;

                // Dont Change This Text "Negotiation Ends in: " it has dependency in ENDNEGOTIATION
                $scope.timeLeftMessage = "Negotiation Ends in: ";
                // Dont Change This Text "Negotiation Ends in: " it has dependency in ENDNEGOTIATION

                $scope.startBtns = false;
                $scope.customerBtns = true;
            } else if ($scope.auctionItem.status == "DELETED") {
                $scope.mactrl.skinSwitch('bluegray');
                $scope.errMsg = "This requirement has been cancelled.";
                $scope.showStatusDropDown = false;
                $scope.isDeleted = true;
            } else if ($scope.auctionItem.status == "Negotiation Ended") {
                $scope.mactrl.skinSwitch('bluegray');
                $scope.errMsg = "Negotiation has been completed.";
                $scope.showStatusDropDown = false;
            } else if ($scope.auctionItem.status == "Vendor Selected") {
                $scope.mactrl.skinSwitch('bluegray');
                if ($scope.auctionItem.isTabular) {
                    $scope.errMsg = "Please select vendors for all items in order to provide Purchase Order Information.";
                } else {
                    $scope.errMsg = "Please click the button below to provide the Purchase Order information.";
                }
                $scope.showStatusDropDown = false;
            } else if ($scope.auctionItem.status == "PO Processing") {
                $scope.mactrl.skinSwitch('lightblue');
                $scope.errMsg = "The PO has been generated. Please find the PO here: ";
                $scope.showStatusDropDown = false;
            } else {
                $scope.mactrl.skinSwitch('lightblue');
                $scope.showStatusDropDown = true;
            }
            if (($scope.auctionItem.customerID == userService.getUserId() || $scope.auctionItem.superUserID == userService.getUserId() || $scope.auctionItem.customerReqAccess)) {
                $scope.userIsOwner = true;
                if ($scope.userIsOwner && !$rootScope.isUserEntitled(546654) && $scope.auctionItem.status == "STARTED") {
                    swal("Error!", "Live negotiation Access Denined", "error");
                    $state.go('home');
                }
                if ($scope.userIsOwner && !$rootScope.isUserEntitled(591159)) {
                    swal("Error!", "View Requirement Access Denined", "error");
                    $state.go('home');
                }
                $scope.options = ['PO Sent', 'Material Received', 'Payment Processing', 'Payment Released'];
                $scope.options.push($scope.auctionItem.status);
            }
            var start = $scope.auctionItem.startTime.split('+')[0].split('(')[1];
            auctionsService.getdate()
                .then(function (responseFromServer) {
                    var dateFromServer = new Date(parseInt(responseFromServer.substr(6)));
                    $log.debug(dateFromServer);
                    var curDate = dateFromServer;

                    var myEpoch = curDate.getTime();
                    $scope.timeLeftMessage = "";
                    if (start > myEpoch) {
                        $scope.auctionStarted = false;
                        $scope.timeLeftMessage = "Negotiation Starts in: ";
                        $scope.startBtns = true;
                        $scope.customerBtns = false;
                    } else {
                        $scope.auctionStarted = true;

                        // Dont Change This Text "Negotiation Ends in: " it has dependency in ENDNEGOTIATION
                        $scope.timeLeftMessage = "Negotiation Ends in: ";
                        // Dont Change This Text "Negotiation Ends in: " it has dependency in ENDNEGOTIATION

                        $scope.startBtns = false;
                        $scope.customerBtns = true;
                    }
                    if ($scope.auctionItem.customerID != userService.getUserId() && $scope.auctionItem.superUserID != userService.getUserId() && !$scope.auctionItem.customerReqAccess) {
                        $scope.startBtns = false;
                        $scope.customerBtns = false;
                    }
                    if ($scope.auctionItem.timeLeft == null || $scope.auctionItem.timeLeft < 1) {
                        $scope.showTimer = false;
                        $scope.disableButtons();
                    } else {
                        $scope.showTimer = true;
                    }
                    //$scope.auctionItem.postedOn = new Date(parseFloat($scope.auctionItem.postedOn.substr(6)));
                    var date = $scope.auctionItem.postedOn.split('+')[0].split('(')[1];
                    var newDate = new Date(parseInt(parseInt(date)));
                    $scope.auctionItem.postedOn = newDate.toString().replace('Z', '');

                    //var date1 = $scope.auctionItem.deliveryTime.split('+')[0].split('(')[1];
                    //var newDate1 = new Date(parseInt(parseInt(date1)));
                    //$scope.auctionItem.deliveryTime = newDate1.toString().replace('Z', '');

                    //var date2 = $scope.auctionItem.quotationFreezTime.split('+')[0].split('(')[1];
                    //var newDate2 = new Date(parseInt(parseInt(date2)));
                    //$scope.auctionItem.quotationFreezTime = newDate2.toString().replace('Z', '');

                    var minPrice = 0;
                    if ($scope.auctionItem.status == "NOTSTARTED") {
                        $scope.auctionItem.minPrice = $scope.auctionItem.auctionVendors[0] ? $scope.auctionItem.auctionVendors[0].initialPrice : 0;
                    } else {
                        $scope.auctionItem.minPrice = $scope.auctionItem.auctionVendors[0] ? $scope.auctionItem.auctionVendors[0].runningPrice : 0;
                    }
                    //$scope.auctionItem.deliveryTime = $scope.auctionItem.deliveryTime.split("GMT")[0];
                    for (var i in $scope.auctionItem.auctionVendors) {

                        $scope.nonParticipatedMsg = '';
                        $scope.quotationRejecteddMsg = '';
                        $scope.quotationNotviewedMsg = '';
                        $scope.revQuotationRejecteddMsg = '';
                        $scope.revQuotationNotviewedMsg = '';
                        $scope.quotationApprovedMsg = '';
                        $scope.revQuotationApprovedMsg = '';

                        var vendor = $scope.auctionItem.auctionVendors[i]

                        if (vendor.vendorID == userService.getUserId() && vendor.quotationUrl == "") {
                            $scope.quotationStatus = false;
                            $scope.quotationUploaded = false;
                        } else {
                            $scope.quotationStatus = true;
                            if ($scope.auctionItem.customerID != userService.getUserId() && $scope.auctionItem.superUserID != userService.getUserId() && !$scope.auctionItem.customerReqAccess) {
                                $scope.quotationUploaded = true;
                            }
                            $scope.quotationUrl = vendor.quotationUrl;
                            $scope.revquotationUrl = vendor.revquotationUrl;
                            $scope.vendorQuotedPrice = vendor.runningPrice;
                        }
                        if (i == 0 && vendor.initialPrice != 0) {
                            minPrice = vendor.initialPrice;
                        } else {
                            if (vendor.initialPrice < minPrice && vendor.initialPrice != 0) {
                                minPrice = vendor.initialPrice;
                            }
                        }
                        $scope.vendorInitialPrice = minPrice;
                        var runningMinPrice = 0;
                        if ($scope.auctionItem.auctionVendors[i].runningPrice > 0 && $scope.auctionItem.auctionVendors[i].runningPrice < $scope.vendorInitialPrice) {
                            runningMinPrice = $scope.auctionItem.auctionVendors[i].runningPrice;
                        }
                        //$scope.auctionItem.minPrice = runningMinPrice;
                        if ($scope.auctionItem.auctionVendors[i].runningPrice == 0) {
                            $scope.auctionItem.auctionVendors[i].runningPrice = 'NA';
                            $scope.auctionItem.auctionVendors[i].totalPriceIncl = 'NA';
                            $scope.auctionItem.auctionVendors[i].rank = 'NA';
                        } else {
                            $scope.vendorRank = vendor.rank;
                            if (vendor.rank == 1) {
                                $scope.toprankerName = vendor.vendorName;
                                if (userService.getUserId() == vendor.vendorID) {
                                    $scope.options = ['PO Accepted', 'Material Dispatched', 'Payment Acknowledged'];
                                    $scope.options.push($scope.auctionItem.status);
                                    if ($scope.auctionItem.status == "STARTED") {
                                        $scope.enableMakeBids = true;
                                    }
                                }
                            }
                            //$scope.auctionItem.auctionVendors[i].totalPriceIncl = $scope.auctionItem.auctionVendors[i].runningPrice + ($scope.auctionItem.auctionVendors[i].runningPrice * $scope.auctionItem.auctionVendors[i].taxes) / 100;
                            $scope.auctionItem.auctionVendors[i].totalPriceIncl = $scope.auctionItem.auctionVendors[i].runningPrice;
                        }
                        if ($scope.auctionItem.auctionVendors[i].initialPrice == 0) {
                            $scope.auctionItem.auctionVendors[i].initialPrice = 'NA';
                            $scope.nonParticipatedMsg = 'You have missed an opportunity to participate in this Negotiation.';
                        }

                        if ($scope.auctionItem.auctionVendors[i].isQuotationRejected == 1) {
                            $scope.quotationRejecteddMsg = 'Your quotation rejected by the customer.';
                        }
                        else if ($scope.auctionItem.auctionVendors[i].isQuotationRejected == -1) {
                            $scope.quotationNotviewedMsg = 'Your quotation Not viewed by the customer.';
                        }
                        else if ($scope.auctionItem.auctionVendors[i].isRevQuotationRejected == 1) {
                            $scope.revQuotationRejecteddMsg = 'Your Rev.Quotation rejected by the customer.';
                        }
                        else if ($scope.auctionItem.auctionVendors[i].isRevQuotationRejected == -1) {
                            $scope.revQuotationNotviewedMsg = 'Your Rev.Quotation Not viewed by the customer.';
                        }

                        if ($scope.auctionItem.auctionVendors[i].isQuotationRejected == 0) {
                            $scope.quotationApprovedMsg = 'Your Quotation Approved by the customer.';
                        }
                        if ($scope.auctionItem.auctionVendors[i].isRevQuotationRejected == 0) {
                            $scope.revQuotationApprovedMsg = 'Your Rev.Quotation Approved by the customer.';
                        }

                    }
                    $scope.$broadcast('timer-set-countdown-seconds', $scope.auctionItem.timeLeft);
                    $('.datetimepicker').datetimepicker({
                        useCurrent: false,
                        icons: {
                            time: 'glyphicon glyphicon-time',
                            date: 'glyphicon glyphicon-calendar',
                            up: 'glyphicon glyphicon-chevron-up',
                            down: 'glyphicon glyphicon-chevron-down',
                            previous: 'glyphicon glyphicon-chevron-left',
                            next: 'glyphicon glyphicon-chevron-right',
                            today: 'glyphicon glyphicon-screenshot',
                            clear: 'glyphicon glyphicon-trash',
                            close: 'glyphicon glyphicon-remove'

                        },

                        minDate: curDate
                    });
                })


            $scope.reduceBidAmountNote = 'Enter the value which you want to reduce from bid Amount.';
            //$scope.incTaxBidAmountNote = 'Please Enter the consolidate bid value including tax amount.';
            $scope.incTaxBidAmountNote = '';
            //$scope.noteForBidValue = 'NOTE : If you fill one field, other will be autocalculated.';
            $scope.noteForBidValue = '';

        }



        $scope.vendorBidPrice = 0;
        $scope.revvendorBidPrice = 0;
        $scope.auctionStarted = true;
        $scope.customerBtns = true;
        $scope.showVendorTable = true;
        $scope.quotationStatus = true;
        $scope.toprankerName = "";
        $scope.vendorRank = 0;
        $scope.quotationUrl = "";
        $scope.revquotationUrl = "";
        $scope.vendorBtns = false;
        $scope.vendorQuotedPrice = 0;
        $scope.startBtns = false;
        $scope.commentsvalidation = false;
        // $scope.userType = userService.getUserType();
        // if ($scope.userType == "VENDOR") {
        //     $scope.customerBtns = false;
        //     $scope.vendorBtns = true;
        //     $scope.showVendorTable = false;
        // }
        $scope.enableMakeBids = false;
        $scope.price = "";
        $scope.startTime = '';
        $scope.customerID = userService.getUserId();

        $scope.poDetails = {};

        $scope.bidHistory = {};

        $scope.GetBidHistory = function () {
            auctionsService.GetBidHistory({ "reqid": $stateParams.Id, "sessionid": userService.getUserToken(), 'userid': userService.getUserId() })
                .then(function (response) {
                    $scope.bidHistory = response;
                });
        }

        $scope.GetBidHistory();

        $scope.vendorID = 0;

        $scope.validity = '';

        if ($scope.auctionItem.urgency == 'Critical (Will be Closed Today)') {
            $scope.validity = '1 Day';
        }
        if ($scope.auctionItem.urgency == 'High (Will be Closed in 2 Days)') {
            $scope.validity = '2 Days';
        }
        if ($scope.auctionItem.urgency == 'Medium (Will be Closed in 5 Days)') {
            $scope.validity = '5 Days';
        }
        if ($scope.auctionItem.urgency == 'Low (Will Take 10-15 Days Time)') {
            $scope.validity = '10-15 Days';
        }

        $scope.warranty = 'As Per OEM';
        $scope.duration = $scope.auctionItem.deliveryTime;
        $scope.payment = $scope.auctionItem.paymentTerms;


        $scope.isQuotationRejected = -1;

        $scope.starttimecondition1 = 0;
        $scope.starttimecondition2 = 0;

        $scope.revQuotationUrl1 = 0;
        $scope.L1QuotationUrl = 0;


        $scope.notviewedcompanynames = '';
        $scope.starreturn = false;

        $scope.getData = function (methodName, callerID) {
            auctionsService.getrequirementdata({ "reqid": $stateParams.Id, "sessionid": userService.getUserToken(), 'userid': userService.getUserId() })
                .then(function (response) {
                    $scope.auctionItem = response;

                    if (callerID == userService.getUserId() || callerID == undefined) {
                        $scope.auctionItemVendor = $scope.auctionItem;
                    }
                    
                    $scope.notviewedcompanynames = '';
                    $scope.auctionItem.auctionVendors.forEach(function (item, index) {
                        if (item.quotationUrl != '' && item.isQuotationRejected == -1) {
                            $scope.Loding = false;
                            $scope.notviewedcompanynames += item.companyName + ', ';
                            $scope.starreturn = true;
                        }
                    })


                    if ($scope.auctionItem.listRequirementItems != null)
                        $scope.auctionItem.listRequirementItems.forEach(function (item, index) {
                            item.TempRevUnitPrice = 0;
                            item.TempRevUnitPrice = item.revUnitPrice;
                    })



                    $scope.notviewedcompanynames = $scope.notviewedcompanynames.substring(0, $scope.notviewedcompanynames.length - 2);


                    if ($scope.auctionItem.listRequirementTaxes.length == 0) {
                        $scope.auctionItem.listRequirementTaxes = $scope.listRequirementTaxes;
                    }
                    else {
                        $scope.listRequirementTaxes = $scope.auctionItem.listRequirementTaxes;
                    }

                    $scope.listRequirementTaxeslength = $scope.listRequirementTaxes.length;

                    $scope.reqTaxSNo = $scope.auctionItem.taxSNoCount;

                    $scope.NegotiationSettings = $scope.auctionItem.NegotiationSettings;
                    var duration = $scope.NegotiationSettings.negotiationDuration.split(" ", 4);
                    $scope.days = parseInt(duration[0]);
                    duration = duration[1];
                    duration = duration.split(":", 4);
                    $scope.hours = parseInt(duration[0]);
                    $scope.mins = parseInt(duration[1]);

                    $scope.NegotiationSettings.negotiationDuration = $scope.days + ' ' + $scope.hours + ':' + $scope.mins + ':00.0';

                    if (!response.auctionVendors || response.auctionVendors.length == 0) {
                        $scope.auctionItem.auctionVendors = [];
                        $scope.auctionItem.description = "";
                    }
                    $scope.auctionItem.quotationFreezTime = new moment($scope.auctionItem.quotationFreezTime).format("DD-MM-YYYY HH:mm");
                    if ($scope.auctionItem.auctionVendors && $scope.auctionItem.auctionVendors.length != 0) {
                        $scope.totalprice = $scope.auctionItem.auctionVendors[0].initialPriceWithOutTaxFreight;
                        $scope.vendorTaxes = $scope.auctionItem.auctionVendors[0].taxes;
                        $scope.vendorBidPrice = $scope.auctionItem.auctionVendors[0].totalInitialPrice;
                        $scope.freightcharges = $scope.auctionItem.auctionVendors[0].vendorFreight;
                        $scope.discountAmount = $scope.auctionItem.auctionVendors[0].discount;
                        $scope.vendorBidPriceWithoutDiscount = $scope.vendorBidPrice + $scope.discountAmount;
                        $scope.vendorID = $scope.auctionItem.auctionVendors[0].vendorID;
                        $scope.warranty = $scope.auctionItem.auctionVendors[0].warranty;
                        $scope.duration = $scope.auctionItem.auctionVendors[0].duration;
                        $scope.payment = $scope.auctionItem.auctionVendors[0].payment;
                        $scope.validity = $scope.auctionItem.auctionVendors[0].validity;
                        $scope.otherProperties = $scope.auctionItem.auctionVendors[0].otherProperties;

                        $scope.revvendorTaxes = $scope.auctionItem.auctionVendors[0].taxes;
                        $scope.isQuotationRejected = $scope.auctionItem.auctionVendors[0].isQuotationRejected;
                        $scope.quotationRejectedComment = $scope.auctionItem.auctionVendors[0].quotationRejectedComment;
                        $scope.revQuotationRejectedComment = $scope.auctionItem.auctionVendors[0].revQuotationRejectedComment;

                        if ($scope.auctionItem.urgency == 'Critical (Will be Closed Today)' && $scope.validity == '') {
                            $scope.validity = '1 Day';
                        }
                        else if ($scope.auctionItem.urgency == 'High (Will be Closed in 2 Days)' && $scope.validity == '') {
                            $scope.validity = '2 Days';
                        }
                        else if ($scope.auctionItem.urgency == 'Medium (Will be Closed in 5 Days)' && $scope.validity == '') {
                            $scope.validity = '5 Days';
                        }
                        else if ($scope.auctionItem.urgency == 'Low (Will Take 10-15 Days Time)' && $scope.validity == '') {
                            $scope.validity = '10-15 Days';
                        }
                        if ($scope.warranty == '') {
                            $scope.warranty = 'As Per OEM';
                        }
                        if ($scope.duration == '') {
                            $scope.duration = new moment($scope.auctionItem.deliveryTime).format("DD-MM-YYYY");
                            $scope.duration = $scope.auctionItem.deliveryTime;
                        }
                        if ($scope.payment == '') {
                            $scope.payment = $scope.auctionItem.paymentTerms;
                        }



                        $scope.auctionItem.listRequirementItems.forEach(function (item, index) {
                            if (!item.selectedVendorID || item.selectedVendorID == 0) {
                                $scope.allItemsSelected = false;
                            }
                            if ($scope.auctionItem.status == "Negotiation Ended" || $scope.auctionItem.status == "Vendor Selected" || $scope.auctionItem.status == "PO Processing") {
                                if (item.selectedVendorID > 0) {
                                    var vendorArray = $filter('filter')($scope.auctionItem.auctionVendors, { vendorID: item.selectedVendorID });
                                    if (vendorArray.length > 0) {
                                        item.selectedVendor = vendorArray[0];
                                    }
                                } else {
                                    item.selectedVendor = $scope.auctionItem.auctionVendors[0];
                                }
                            }
                        })
                        if (!$scope.auctionItem.isStopped && $scope.auctionItem.timeLeft > 0) {
                            $scope.disableAddButton = false;
                        }

                        $scope.revtotalprice = $scope.auctionItem.auctionVendors[0].revPrice;
                        // $scope.vendorTaxes = $scope.auctionItem.auctionVendors[0].taxes;
                        $scope.revvendorBidPrice = $scope.auctionItem.auctionVendors[0].revVendorTotalPrice;
                        $scope.revfreightcharges = $scope.auctionItem.auctionVendors[0].revVendorFreight;
                        $scope.starttimecondition1 = $scope.auctionItem.auctionVendors[0].isQuotationRejected;


                        $scope.revQuotationUrl1 = $scope.auctionItem.auctionVendors[0].revquotationUrl;

                        if ($scope.auctionItem.auctionVendors[0].companyName == 'PRICE_CAP') {
                            $scope.revQuotationUrl1 = $scope.auctionItem.auctionVendors[1].revquotationUrl;
                        }

                        $scope.L1QuotationUrl = $scope.auctionItem.auctionVendors[0].quotationUrl;


                        //= $filter('filter')($scope.auctionItem.auctionVendors, {revquotationUrl > ''});
                        //$scope.RevQuotationfirstvendor 
                        $scope.auctionItem.auctionVendors.forEach(function (item, index) {
                            if (item.revquotationUrl != '') {
                                $scope.RevQuotationfirstvendor = item.revquotationUrl;
                            }
                        })

                    }

                    if ($scope.auctionItem.auctionVendors && $scope.auctionItem.auctionVendors.length >= 2) {
                        $scope.starttimecondition2 = $scope.auctionItem.auctionVendors[1].isQuotationRejected;
                    }
                    //$scope.vendorID = $scope.auctionItem.auctionVendors[0].vendorID;

                    $scope.participatedVendors = $scope.auctionItem.auctionVendors.filter(function (vendor) {
                        return (vendor.runningPrice > 0 && vendor.companyName != 'PRICE_CAP');
                    });

                    $scope.revisedParticipatedVendors = $scope.participatedVendors.filter(function (vendor) {
                        return (vendor.revquotationUrl != null && vendor.revquotationUrl != '' && vendor.companyName != 'PRICE_CAP');
                    })

                    $scope.revisedApprovedVendors = $scope.participatedVendors.filter(function (vendor) {
                        return (vendor.revquotationUrl != null && vendor.revquotationUrl != '' && vendor.companyName != 'PRICE_CAP' && vendor.isRevQuotationRejected == 0);
                    })


                    if ($scope.auctionItem.status != 'DELETED' && $scope.auctionItem.status != 'UNCONFIRMED' && $scope.auctionItem.status != 'STARTED' && $scope.auctionItem.status != 'NOTSTARTED' && $scope.auctionItem.status != 'Negotiation Ended') {
                        auctionsService.getpodetails({ "reqid": $stateParams.Id, "sessionid": userService.getUserToken(), 'userid': userService.getUserId() })
                            .then(function (response) {
                                $scope.poDetails = response;
                                //$scope.updatedeliverydateparams.date = $scope.poDetails.expectedDelivery;
                                if (response != undefined) {
                                    //var date = $scope.poDetails.expectedDelivery.split('+')[0].split('(')[1];
                                    var date1 = moment($scope.poDetails.expectedDelivery).format('DD/MM/YYYY');
                                    $scope.updatedeliverydateparams.date = date1;

                                    //var date1 = $scope.poDetails.paymentScheduleDate.split('+')[0].split('(')[1];
                                    //var newDate1 = new Date(parseInt(parseInt(date1)));
                                    var date2 = moment($scope.poDetails.expPaymentDate).format('DD/MM/YYYY');
                                    $scope.updatepaymentdateparams.date = date2;
                                    if ($scope.updatepaymentdateparams.date == '31/12/9999') {
                                        $scope.updatepaymentdateparams.date = 'Payment Date';
                                    }
                                }
                            });
                    }

                    //  $scope.auctionItem.description = $scope.auctionItem.description.replace("\u000a", "\n")
                    $scope.description = $scope.auctionItem.description.replace(/\u000a/g, "</br>");
                    $scope.deliveryLocation = $scope.auctionItem.deliveryLocation.replace(/\u000a/g, "</br>");
                    $scope.paymentTerms = $scope.auctionItem.paymentTerms.replace(/\u000a/g, "</br>");

                    var id = parseInt(userService.getUserId());
                    var result = $scope.auctionItem.auctionVendors.filter(function (obj) {
                        return obj.vendorID == id;
                    });
                    if (id != $scope.auctionItem.customerID && id != $scope.auctionItem.superUserID && !$scope.auctionItem.customerReqAccess && result.length == 0) {
                        swal("Access denied", "You do not have access to this requirement because you are not part of this requirements process.", 'error');
                        $state.go('home');
                    } else {
                        $scope.setFields();
                        //auctionsService.getcomments({ "reqid": $stateParams.Id, "userid": userService.getUserId(), "sessionid": userService.getUserToken() })
                        //    .then(function (response) {
                        //        $scope.Comments = response;
                        //    });

                    }

                });
            //$scope.$broadcast('timer-start');
        }



        $scope.getData();
        $scope.AmountSaved = 0;

        /*var intervalPromise = window.setInterval(function(){
            if(window.location.hash.indexOf("#/view-requirement") > -1){
                $scope.getData();
            }            
        }, 10000);*/

        $scope.selectItemVendor = function (itemID, vendorID) {
            var params = {
                reqID: $scope.auctionItem.requirementID,
                userID: userService.getUserId(),
                itemID: itemID,
                vendorID: vendorID,
                sessionID: userService.getUserToken()
            };
            auctionsService.itemwiseselectvendor(params)
                .then(function (response) {
                    if (response.errorMessage == '') {
                        var index = -1;
                        for (var i = 0, len = $scope.auctionItem.listRequirementItems.length; i < len; i++) {
                            if ($scope.auctionItem.listRequirementItems[i].itemID === itemID) {
                                index = i;
                                $scope.auctionItem.listRequirementItems[i].selectedVendorID = vendorID;
                                break;
                            }
                            if ($scope.auctionItem.listRequirementItems[i].selectedVendor == 0) {
                                $scope.allItemsSelected = false;
                            } else {
                                $scope.allItemsSelected = true;
                            }
                        }
                        swal("Success!", "This item has been assigned to Vendor", "success");
                        $scope.getData();
                    } else {
                        swal("Error", 'An Unexpected error has occurred. Please contact support@prm360.com for resolving this.', 'error');
                    }

                })
        }

        $scope.$watch('auctionItem.listRequirementItems', function (oldVal, newVal) {
            $log.debug("items list has changed");
            newVal.forEach(function (item, index) {
                if (!item.selectedVendorID || item.selectedVendorID == 0) {
                    $scope.allItemsSelected = false;
                }
            })
        })

        $scope.generatePDFonHTML = function () {
            auctionsService.getdate()
                .then(function (response) {
                    var date = new Date(parseInt(response.substr(6)));
                    var obj = $scope.auctionItem;
                    $scope.POTemplate = "<div id='POTemplate' style='display:none;'><html><head><title>PRM360</title><style>.date{margin-left: 850px;}.to{margin-left: 250px;}.name{margin-left: 300px;}.sub{margin-left: 450px;}img{position: absolute; left: 750px; top:75px; z-index: -1;}</style></head><body><header><br><br><br><img src='acads360.jpg' width='50' height='50'><h1 align='center'>PRM360<img </h1></header><br><div class='date'><p><b>Date:</b> " + date + "</p><p><b>PO No:</b> " + obj.requirementID + "</p></div><div class='to'><p>To,</p><p><b>" + obj.CompanyName + ",</b></p><p><b>" + obj.deliveryLocation + ".</b></p></div><p class='name'><b>Hello </b> " + obj.auctionVendors[0].vendorName + "</p><p class='sub'><b>Sub:</b> " + obj.title + "</p><p align='center'><b>Bill of Material</b></p><table border='1' cellpadding='2' style='width:60%' align='center'><tr><th>Product Name</th><th>Description</th><th>Price</th></tr><tr><td>" + obj.title + "</td><td>" + obj.description + "</td><td>" + obj.price + "</td></tr></table><p class='to'><b>Terms & Conditions</b></p><div class='name'> <p>1. Payment : " + obj.paymentTerms + ".</p><p>2. Delivery : " + obj.deliveryLocation + ".</p><p>3. Tax : " + obj.taxes + ".</p></div><p class='to'><b>Billing and Shipping Address:</b></p><p class='to'>Savvy Associates, # 3-4-174/21/2, Radha Krishna Nagar, Attapur, Hyderguda, Hyderabad - 500048</p><p align=center>This is a system generated PO, henceforth sign and seal is not required.</p><br><footer class='to'>M/s. Savvy Associates, H.No: 3-4-174/21/2, Radha Krishna Nagar, Attapur, Hyderguda, Hyderabad – 48Contact,M: 91-9949245791.,<br>E: savvyassociates@gmail.com.<br><b>URL:</b> www.savvyassociates.com. </footer></body></html></div>";
                    var content = document.getElementById('content');
                    content.insertAdjacentHTML('beforeend', $scope.POTemplate);
                })
        }

        $scope.generatePOasPDF = function (divName) {
            $scope.generatePDFonHTML();
            var printContents = document.getElementById(divName).innerHTML;
            var originalContents = document.body.innerHTML;

            if (navigator.userAgent.toLowerCase().indexOf('chrome') > -1) {
                var popupWin = window.open('', '_blank', 'width=600,height=600,scrollbars=no,menubar=no,toolbar=no,location=no,status=no,titlebar=no');
                popupWin.window.focus();
                popupWin.document.write('<!DOCTYPE html><html><head>' +
                    '<link rel="stylesheet" type="text/css" href="style.css" />' +
                    '</head><body onload="window.print()"><div class="reward-body">' + printContents + '</div></html>');
                popupWin.onbeforeunload = function (event) {
                    popupWin.close();
                    return '.\n';
                };
                popupWin.onabort = function (event) {
                    popupWin.document.close();
                    popupWin.close();
                }
            } else {
                var popupWin = window.open('', '_blank', 'width=800,height=600');
                popupWin.document.open();
                popupWin.document.write('<html><head><link rel="stylesheet" type="text/css" href="style.css" /></head><body onload="window.print()">' + printContents + '</html>');
                popupWin.document.close();
            }
            popupWin.document.close();
            var params = {
                reqid: $scope.auctionItem.requirementID,
                userid: userService.getUserId(),
                status: 'PO Processing',
                type: "WINVENDOR",
                sessionID: userService.getUserToken()
            };
            auctionsService.updateStatus(params)
                .then(function (response) {
                    if (response.errorMessage == '') {
                        $scope.getData();
                        //doc.save("DOC.PDF");
                    } else {
                        swal("Error", 'An Unexpected error has occurred. Please contact support@prm360.com for resolving this.', 'error');
                    }

                })

            return true;
        }

        $scope.generatePO = function () {
            var doc = new jsPDF();
            doc.setFontSize(40);
            //doc.text(40, 30, "Octocat loves jsPDF", 4);
            /*doc.fromHTML($("#POTemplate")[0], 15, 15, {
                "width": 170,
                function() {
                    $scope.POFile = $.makeArray(new Uint8Array(doc.output('arraybuffer')));
                }
            })*/

            var params = {
                reqid: $scope.auctionItem.requirementID,
                userid: userService.getUserId(),
                status: 'PO Processing',
                type: "WINVENDOR",
                sessionID: userService.getUserToken()
            };
            auctionsService.updateStatus(params)
                .then(function (response) {
                    if (response.errorMessage == '') {
                        $scope.getData();
                        doc.save("DOC.PDF");
                    } else {
                        swal("Error", 'An Unexpected error has occurred. Please contact support@prm360.com for resolving this.', 'error');
                    }

                })
        }

        $scope.generatePOinServer = function () {
            if ($scope.POTemplate == "") {
                $scope.generatePDFonHTML();

            }
            var doc = new jsPDF('p', 'in', 'letter');
            var specialElementHandlers = {};
            var doc = new jsPDF();
            //doc.setFontSize(40);
            doc.fromHTML($scope.POTemplate, 0.5, 0.5, {
                'width': 7.5, // max width of content on PDF
            });
            //doc.save("DOC.PDF");
            doc.output("dataurl");
            $scope.POFile = $.makeArray(new Uint8Array(doc.output('arraybuffer')));
            var params = {
                POfile: $scope.POFile,
                reqid: $scope.auctionItem.requirementID,
                userid: userService.getUserId(),
                POfileName: 'PO_req_' + $scope.auctionItem.requirementID + '.pdf',
                sessionID: userService.getUserToken()
            }

            auctionsService.generatePOinServer(params)
                .then(function (response) {
                    if (response.errorMessage == '') {
                        $scope.showStatusDropDown = true;
                        $scope.getData();
                    } else {
                        swal("Error", 'An Unexpected error has occurred. Please contact support@prm360.com for resolving this.', 'error');
                    }

                })
        }

        $scope.updateStatus = function (status) {
            var params = {
                reqid: $scope.auctionItem.requirementID,
                userid: userService.getUserId(),
                status: status,
                type: "ALLVENDORS",
                sessionID: userService.getUserToken()
            };
            auctionsService.updateStatus(params)
                .then(function (response) {
                    if (response.errorMessage == '') {
                        $scope.getData();
                    } else {
                        swal("Error", 'An Unexpected error has occurred. Please contact support@prm360.com for resolving this.', 'error');
                    }

                })
        }

        $scope.uploadquotationsfromexcel = function (status) {
            var params = {
                reqID: $scope.auctionItem.requirementID,
                userID: userService.getUserId(),
                sessionID: userService.getUserToken(),
                quotationAttachment: $scope.quotationAttachment
            };
            auctionsService.uploadquotationsfromexcel(params)
                .then(function (response) {
                    if (response.errorMessage == '') {
                        location.reload();
                    } else {
                        swal("Error", response.errorMessage, 'error');
                    }

                })
        }

        $scope.updatedeliverydateparams = {
            date: ''
        };



        $scope.updatedeliverdate = function () {
            var ts = moment($scope.updatedeliverydateparams.date, "DD-MM-YYYY HH:mm").valueOf();
            var m = moment(ts);
            var deliveryDate = new Date(m);
            var milliseconds = parseInt(deliveryDate.getTime() / 1000.0);
            $scope.updatedeliverydateparams.date = "/Date(" + milliseconds + "000+0530)/";

            var params = {
                reqid: $scope.auctionItem.requirementID,
                userid: userService.getUserId(),
                date: $scope.updatedeliverydateparams.date,
                type: "DELIVERY",
                sessionID: userService.getUserToken()
            };
            auctionsService.updatedeliverdate(params)
                .then(function (response) {
                    if (response.errorMessage == '') {
                        $scope.getData();
                    } else {
                        swal("Error", 'An Unexpected error has occurred. Please contact support@prm360.com for resolving this.', 'error');
                    }

                })

        }


        $scope.updatepaymentdateparams = {
            date: ''
        };



        $scope.updatepaymentdate = function () {
            var ts = moment($scope.updatepaymentdateparams.date, "DD-MM-YYYY HH:mm").valueOf();
            var m = moment(ts);
            var deliveryDate = new Date(m);
            var milliseconds = parseInt(deliveryDate.getTime() / 1000.0);
            $scope.updatepaymentdateparams.date = "/Date(" + milliseconds + "000+0530)/";

            var params = {
                reqid: $scope.auctionItem.requirementID,
                userid: userService.getUserId(),
                date: $scope.updatepaymentdateparams.date,
                type: "PAYMENT",
                sessionID: userService.getUserToken()
            };
            auctionsService.updatepaymentdate(params)
                .then(function (response) {
                    if (response.errorMessage == '') {
                        $scope.getData();
                    } else {
                        swal("Error", 'An Unexpected error has occurred. Please contact support@prm360.com for resolving this.', 'error');
                    }

                })
        }



        $scope.RestartNegotiation = function () {
            var params = {};
            params.reqID = id;
            params.sessionID = userService.getUserToken();
            params.userID = userService.getUserId();
            var parties = id + "$" + userService.getUserId() + "$" + userService.getUserToken();
            $scope.isnegotiationended = false;
            $scope.NegotiationEnded = false;
            $scope.invokeSignalR('RestartNegotiation', parties);
        }

        $scope.saveComment = function () {
            var commentText = "";
            if ($scope.newComment && $scope.newComment != "") {
                commentText = $socpe.newComment;
            } else if ($("#comment")[0].value != '') {
                commentText = $("#comment")[0].value;
            } else {
                $scope.commentsvalidation = true;
            }
            if (!$scope.commentsvalidation) {
                var params = {};
                auctionsService.getdate()
                    .then(function (response) {
                        var date = new Date(parseInt(response.substr(6)));
                        var myEpoch = date.getTime();
                        params.requirementID = id;
                        params.firstName = "";
                        params.lastName = "";
                        params.replyCommentID = -1;
                        params.commentID = -1;
                        params.errorMessage = "";
                        params.createdTime = "/Date(" + myEpoch + "+0000)/";
                        params.sessionID = userService.getUserToken();
                        params.userID = userService.getUserId();
                        params.commentText = commentText;
                        $scope.invokeSignalR('SaveComment', params);
                        //requirementHub.invoke('SaveComment', params, function (response) {
                        //	//auctionsService.savecomment(params).then(function (response) {
                        //	$scope.getData();
                        //	$scope.newComment = "";
                        //	$scope.commentsvalidation = false;
                        //});
                    });
            }
        }

        $scope.getFile = function () {
            $scope.progress = 0;
            var quotation = $("#quotation")[0].files[0];
            if (quotation != undefined && quotation != '') {
                $scope.file = $("#quotation")[0].files[0];
                $log.info($("#quotation")[0].files[0]);
            }


            //var quotation1 = $("#quotation1")[0].files[0];
            //if (quotation1 != undefined && quotation1 != '') {
            //$scope.file = $("#quotation1")[0].files[0];
            //$log.info($("#quotation1")[0].files[0]);
            //}
            fileReader.readAsDataUrl($scope.file, $scope)
                .then(function (result) {
                    var bytearray = new Uint8Array(result);
                    if (quotation != undefined && quotation != '') {
                        $scope.bidAttachement = $.makeArray(bytearray);
                        $scope.bidAttachementName = $scope.file.name;
                        $scope.enableMakeBids = true;
                    }
                });
        };


        $scope.updateTime = function (time) {
            var isDone = false;
            $scope.disableDecreaseButtons = true;
            $scope.disableAddButton = true;
            $scope.$on('timer-tick', function (event, args) {
                var temp = event.targetScope.countdown;

                if (!isDone) {
                    if (time < 0 && temp + time < 0) {
                        growlService.growl("You cannot reduce the time when it is already below 60 seconds", "inverse");
                        isDone = true;
                        return false;
                    }
                    addCDSeconds("timer", time);

                    isDone = true;
                    var params = {};
                    params.reqID = id;
                    params.sessionID = userService.getUserToken();
                    params.userID = userService.getUserId();
                    params.newTicks = ($scope.countdownVal + time);
                    var parties = id + "$" + userService.getUserId() + "$" + params.newTicks + "$" + userService.getUserToken();
                    $scope.invokeSignalR('UpdateTime', parties);
                    //requirementHub.invoke('UpdateTime', parties, function (req) {
                    //    //if ((temp + time) >= 60) {
                    //    //    $scope.disableDecreaseButtons = false;
                    //    //} else {
                    //    //    $scope.disableDecreaseButtons = true;
                    //    //}
                    //    if (!$scope.auctionItem.isStopped && $scope.auctionItem.timeLeft > 0) {
                    //        $scope.disableAddButton = false;
                    //    }
                    //    //$log.info(req);
                    //})
                }
            });
        }

        $scope.Loding = false;

        $scope.priceCapError = false;


        $scope.updateAuctionStart = function () {
            $scope.priceCapError = false;
            $scope.Loding = true;
            $scope.NegotiationSettingsValidationMessage = '';
            $scope.NegotiationTimeValidation($scope.days, $scope.hours, $scope.mins, $scope.NegotiationSettings.minReductionAmount, $scope.NegotiationSettings.rankComparision);
            if ($scope.NegotiationSettingsValidationMessage != '') {
                $scope.Loding = false;
                return;
            }

            var params = {};
            params.auctionID = id;

            var startValue = $("#startTimeDate").val(); //$scope.startTime; //Need fix on this.

            if (startValue && startValue != null && startValue != "") {

                //var ts = moment($scope.startTime, "DD-MM-YYYY HH:mm").valueOf();
                var ts = moment(startValue, "DD-MM-YYYY HH:mm").valueOf();
                var m = moment(ts);
                var auctionStartDate = new Date(m);
                auctionsService.getdate()
                    .then(function (response1) {
                        var CurrentDate = moment(new Date(parseInt(response1.substr(6))));
                        $log.debug(CurrentDate < auctionStartDate);
                        $log.debug('div' + auctionStartDate);
                        if (CurrentDate >= auctionStartDate) {
                            $scope.Loding = false;
                            swal("Done!", "Your Negotiation Start Time should be greater than current time.", "error");
                            return;
                        }


                        var milliseconds = parseInt(auctionStartDate.getTime() / 1000.0);
                        params.postedOn = "/Date(" + milliseconds + "000+0530)/";
                        params.auctionEnds = "/Date(" + milliseconds + "000+0530)/";
                        params.customerID = userService.getUserId();
                        params.sessionID = userService.getUserToken();

                        $scope.NegotiationSettings.negotiationDuration = $scope.days + ' ' + $scope.hours + ':' + $scope.mins + ':00.0';
                        params.NegotiationSettings = $scope.NegotiationSettings;

                        $scope.auctionItem.auctionVendors.forEach(function (item, index) {
                            if (item.companyName == "PRICE_CAP") {
                                if (item.totalRunningPrice > $scope.auctionItem.auctionVendors[0].totalRunningPrice) {
                                    $scope.priceCapError = true;
                                }
                            }
                        })

                        if ($scope.starreturn) {
                            $scope.Loding = false;
                            swal("Not Allowed", "You are not allowed to schedule the Negotiation time until all the Vendor Quotations are validated. (They should be either approved/rejected). The following vendors quotations are yet to be validated. (" + $scope.notviewedcompanynames + ")", "error");
                            return;
                        }

                        if ($scope.auctionItem.auctionVendors.length == 0 || $scope.auctionItem.auctionVendors[0].quotationUrl == "" || $scope.auctionItem.auctionVendors[1].quotationUrl == "" || $scope.auctionItem.auctionVendors[0].isQuotationRejected != 0 || $scope.auctionItem.auctionVendors[1].isQuotationRejected != 0 || $scope.starttimecondition1 != 0 || $scope.starttimecondition2 != 0) {
                            $scope.Loding = false;
                            swal("Not Allowed", "You are not allowed to create a start time until at least 2 vendors Approved.", "error");
                            return;
                        }
                        if ($scope.auctionItem.auctionVendors[0].companyName == "") {
                            $scope.Loding = false;
                            swal("Not Allowed", "You are not allowed to create a start time until at least 2 vendors Approved.", "error");
                            return;
                        }
                        if ($scope.priceCapError == true) {
                            $scope.Loding = false;
                            swal("Not Allowed", "Price CAP Value should be less then all the vendors price.", "error");
                            return;
                        }
                        else {
                            $scope.Loding = true;
                            requirementHub.invoke('UpdateAuctionStartSignalR', params, function (req) {
                                $scope.Loding = false;
                                swal("Done!", "Your Negotiation Start Time Updated Successfully!", "success");
                                $scope.Loding = false;
                                var start = $scope.auctionItem.startTime.split('+')[0].split('(')[1];
                                auctionsService.getdate()
                                    .then(function (response) {
                                        var curDate = new Date(parseInt(response.substr(6)));
                                        
                                        var myEpoch = curDate.getTime();
                                        if (($scope.auctionItem.customerID == userService.getUserId() || $scope.auctionItem.superUserID == userService.getUserId() || $scope.auctionItem.customerReqAccess) && start > myEpoch) {
                                            $scope.startBtns = true;
                                            $scope.customerBtns = false;
                                        } else {

                                            $scope.startBtns = false;
                                            $scope.disableButtons();
                                            $scope.customerBtns = true;
                                        }

                                        if ($scope.auctionItem.timeLeft == null || $scope.auctionItem.timeLeft < 0) {
                                            $scope.showTimer = false;
                                        } else {

                                            $scope.showTimer = true;
                                        }
                                    })
                            })
                        }
                    })

                $scope.Loding = false;


            } else {
                $scope.Loding = false;
                alert("Please enter the date and time to update Start Time to.");
            }
            $scope.Loding = false;
        }
        $scope.makeaBidLoding = false;

        //DESCQUOTATIONUPLOAD
        $scope.makeaBid = function (call, isReviced) {
            $scope.makeaBidLoding = true;

            var bidPrice = 0;

            if (isReviced == 0) {
                var bidPrice = $("#quotationamount").val();
            }
            if (isReviced == 1) {

                var bidPrice = $("#revquotationamount").val();
            }

            var params = {};
            params.reqID = parseInt(id);
            params.sessionID = userService.getUserToken();
            params.userID = parseInt(userService.getUserId());

            params.price = parseFloat(bidPrice);
            params.quotation = $scope.bidAttachement;
            params.quotationName = $scope.bidAttachementName;
            params.freightcharges = $scope.freightcharges;
            params.discountAmount = $scope.discountAmount;

            params.tax = $scope.vendorTaxes;

            params.warranty = $scope.warranty;
            params.duration = $scope.duration;
            params.payment = $scope.payment;
            params.validity = $scope.validity;

            params.otherProperties = $scope.otherProperties;

            params.quotationType = 'USER';
            params.type = 'quotation';


            //params.unitPrice = $scope.auctionItem.unitPrice;
            //params.productQuantity = $scope.auctionItem.productQuantity;
            //params.cGst = $scope.auctionItem.cGst;
            //params.sGst = $scope.auctionItem.sGst;
            //params.iGst = $scope.auctionItem.iGst;



            params.listRequirementTaxes = $scope.listRequirementTaxes;
            
            params.quotationObject = $scope.auctionItem.listRequirementItems;

            if (isReviced == 0) {
                params.revised = isReviced;
                params.priceWithoutTax = $scope.totalprice;
                params.price = $scope.vendorBidPrice;
                params.freightcharges = $scope.freightcharges;
                params.discountAmount = $scope.discountAmount;
            }
            if (isReviced == 1) {
                params.revised = isReviced;
                params.priceWithoutTax = $scope.revtotalprice;
                params.price = $scope.revvendorBidPrice;
                params.freightcharges = $scope.revfreightcharges;
                params.discountAmount = $scope.discountAmount;

            }

            if ($scope.auctionItem.status == 'UNCONFIRMED' && $scope.auctionItem.isQuotationPriceLimit && $scope.vendorBidPrice > $scope.auctionItem.quotationPriceLimit) {
                swal("Error!", 'Your Amount Should be less than Quotation Price limit :' + $scope.auctionItem.quotationPriceLimit, "error");
                return;
            };

            auctionsService.makeabid(params).then(function (req) {
                if (req.errorMessage == '') {
                    swal({
                        title: "Thanks!",
                        text: "Your Quotation has been Uploaded Successfully",
                        type: "success",
                        showCancelButton: false,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Ok",
                        closeOnConfirm: true
                    },
                        function () {
                            location.reload();
                        });

                    $("#quotationamount").val("");
                    $scope.quotationStatus = true;
                    $scope.auctionStarted = false;
                    $scope.getData();
                    $scope.makeaBidLoding = false;
                } else {
                    $scope.makeaBidLoding = false;
                    swal("Error!", req.errorMessage, "error");
                }
            });           
        }

        $scope.revquotationupload = function () {
            $scope.makeaBidLoding = true;
            if (($scope.bidAttachementName == "" || $scope.bidAttachement.length == 0) && $scope.quotationStatus == false) {
                $scope.bidAttachementValidation = true;
                $scope.makeaBidLoding = false;
                return false;
            } else {
                $scope.bidAttachementValidation = false;
            }
            var params = {};
            params.reqID = parseInt(id);
            params.sessionID = userService.getUserToken();
            params.userID = parseInt(userService.getUserId());
            params.price = 0;
            params.quotation = $scope.bidAttachement;
            params.quotationName = $scope.bidAttachementName;
            params.tax = 0;
            auctionsService.revquotationupload(params).then(function (req) {
                if (req.errorMessage == '') {
                    swal({
                        title: "Thanks!",
                        text: "Your Revised Quotation Uploaded Successfully",
                        type: "success",
                        showCancelButton: false,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Ok",
                        closeOnConfirm: true
                    },
                        function () {
                            location.reload();
                        });
                    $("#quotationamount1").val("");
                    $scope.quotationStatus = true;
                    $scope.auctionStarted = false;
                    $scope.getData();
                    $scope.makeaBidLoding = false;
                } else {
                    $scope.makeaBidLoding = false;
                    swal("Error!", req.errorMessage, "error");
                }
            });
        }

        $scope.$on('timer-tick', function (event, args) {
            $scope.countdownVal = event.targetScope.countdown;
            if (event.targetScope.countdown < 61 && !$scope.disableStopBids && !$scope.disablereduceTime) {
                $timeout($scope.disableButtons(), 1000);
            }
            if (event.targetScope.countdown > 60) {
                $timeout($scope.enableButtons(), 1000);
            }
            if (event.targetScope.countdown == 60 || event.targetScope.countdown == 59 || event.targetScope.countdown == 30 || event.targetScope.countdown == 29) {
                //var msie = $(document) [0].documentMode;
                var ua = window.navigator.userAgent;
                var msieIndex = ua.indexOf("MSIE ");
                var msieIndex2 = ua.indexOf("Trident");
                // if is IE (documentMode contains IE version)
                var params = {};
                params.reqID = id;
                params.sessionID = userService.getUserToken();
                params.userID = userService.getUserId();
                //var parties = id + "$" + userService.getUserId() + "$" + userService.getUserToken();
                //requirementHub.invoke('CheckRequirement', parties, function () {
                //    $scope.getData();
                //    //$scope.$broadcast('timer-set-countdown-seconds', 0);
                //    //swal("Negotiation Completed!", "Congratulations! you procurement process is now completed. " + $scope.toprankerName + " is the least bider with the value " + $scope.auctionItem.minPrice + " \n Your savings through PRM :" + ($scope.vendorInitialPrice - $scope.auctionItem.minPrice), "success");
                //});
                if (msieIndex > 0 || msieIndex2 > 0) {
                    $scope.getData();
                }
            }

            if (event.targetScope.countdown <= 0) {
                if ($scope.auctionStarted && ($scope.auctionItem.status == "CLOSED" || $scope.auctionItem.status == "STARTED")) {
                    $scope.auctionItem.minPrice = $scope.auctionItem.auctionVendors[0].runningPrice;
                    if (!$scope.NegotiationEnded) {

                        auctionsService.isnegotiationended(id, userService.getUserToken())
                            .then(function (response) {
                                if (response.errorMessage == '') {
                                    if (response.objectID == 1) {
                                        //if (($scope.auctionItem.customerID == userService.getUserId() || $scope.auctionItem.superUserID == userService.getUserId() || $scope.auctionItem.customerReqAccess)) {
                                        var params = {};
                                        params.reqID = id;
                                        params.sessionID = userService.getUserToken();
                                        params.userID = userService.getUserId();
                                        var parties = id + "$" + userService.getUserId() + "$" + userService.getUserToken();
                                        $scope.NegotiationEnded = true;
                                        $scope.invokeSignalR('EndNegotiation', parties);
                                        //requirementHub.invoke('EndNegotiation', parties, function () {
                                        //    //$scope.$broadcast('timer-set-countdown-seconds', 0);
                                        //    $scope.NegotiationEnded = true;
                                        //    //swal("Negotiation Completed!", "Congratulations! you procurement process is now completed. " + $scope.toprankerName + " is the least bider with the value " + $scope.auctionItem.minPrice + " \n Your savings through PRM :" + ($scope.vendorInitialPrice - $scope.auctionItem.minPrice), "success");
                                        //    //if ((userService.getUserId() == $scope.auctionItem.customerID || $scope.auctionItem.superUserID == userService.getUserId() || $scope.auctionItem.customerReqAccess)) {
                                        //    //    swal("Negotiation Completed!", "Congratulations! your procurement process is now completed. " + $scope.toprankerName + " is the least bidder with the value " + $scope.auctionItem.minPrice + " " + $scope.auctionItem.currency + " \n Your savings through PRM 360 :" + ($scope.auctionItem.savings) + " " + $scope.auctionItem.currency, "success");
                                        //    //} else if (req.userIDList.indexOf(parseInt(userService.getUserId())) > -1 && $scope.timeLeftMessage == "Negotiation Ends in: ") {
                                        //    //    if ($scope.vendorRank == 1) {
                                        //    //        swal("Negotiation Completed!", "Congratulations! you are the least bidder for this requirement. Your price is : " + $scope.auctionItem.minPrice + " " + $scope.auctionItem.currency + "\n Customer would be reaching out you shortly. All the best!", "success");
                                        //    //    } else {
                                        //    //        swal("Negotiation Completed!", "Bidding Completed.\n Thank You for your interest on this requirement. You ranked " + $scope.vendorRank + " in this requirement. Thank you for your participation.", "success");
                                        //    //    }
                                        //    //}
                                        //});                        
                                    }
                                }
                            })
                    }
                } else if ($scope.auctionItem.status == "NOTSTARTED") {
                    var params = {};
                    params.reqID = id;
                    params.sessionID = userService.getUserToken();
                    params.userID = userService.getUserId();

                    auctionsService.StartNegotiation(params)
                        .then(function (response) {
                            if (response.errorMessage == '') {
                                $scope.getData();
                            }
                        })

                    var parties = id + "$" + userService.getUserId() + "$" + userService.getUserToken();
                    $scope.invokeSignalR('CheckRequirement', parties);
                    //requirementHub.invoke('CheckRequirement', parties, function () {
                    //    $scope.getData();
                    //    //$scope.$broadcast('timer-set-countdown-seconds', 0);
                    //    //swal("Negotiation Completed!", "Congratulations! you procurement process is now completed. " + $scope.toprankerName + " is the least bider with the value " + $scope.auctionItem.minPrice + " \n Your savings through PRM :" + ($scope.vendorInitialPrice - $scope.auctionItem.minPrice), "success");
                    //});
                }

                //$scope.getData();
            }
            if (event.targetScope.countdown <= 120) {
                $scope.timerStyle = { 'color': '#f00' };
            }
            if (event.targetScope.countdown > 120 && $scope.auctionItem.status == 'NOTSTARTED') {
                $scope.timerStyle = { 'color': '#000' };
            }
            if (event.targetScope.countdown > 120 && $scope.auctionItem.status != 'NOTSTARTED') {
                $scope.timerStyle = { 'color': '#228B22' };
            }
            if (event.targetScope.countdown <= 60 && $scope.auctionItem.status == 'STARTED') {
                $scope.disableDecreaseButtons = true;
            }
            if (event.targetScope.countdown > 60 && $scope.auctionItem.status == 'STARTED') {
                $scope.disableDecreaseButtons = false;
            }
            if (event.targetScope.countdown <= 0 && $scope.auctionItem.status == 'NOTSTARTED') {
                $scope.showTimer = false;

                //$scope.getData();
                window.location.reload();
            }
            if (event.targetScope.countdown < 1) {
                var parties = id + "$" + userService.getUserId() + "$" + userService.getUserToken();
                $scope.invokeSignalR('CheckRequirement', parties);
                //requirementHub.invoke('CheckRequirement', parties, function () {

                //});
            }
            if (event.targetScope.countdown <= 3) {
                $scope.disableAddButton = true;
            }
            if (event.targetScope.countdown < 2) {
                $scope.disableBidButton = true;
            }
            if (event.targetScope.countdown > 3) {
                //$scope.getData();
                $scope.disableAddButton = false;
            }
            if (event.targetScope.countdown >= 2) {
                $scope.disableBidButton = false;
            }
        });


        requirementHub.on('checkRequirement', function (req) {
            //$log.info("client function called");
            // if($scope.auctionItem.requirementID == req.requirementID){
            //     $scope.auctionItem = req;
            //     $scope.setFields();
            // }
            if (!$scope.NegotiationEnded) {
                $scope.$broadcast('timer-start');
            }
            if (id == req.requirementID && (userService.getUserId() == req.customerID || userService.getUserId() == req.superUserID || req.userIDList.indexOf(parseInt(userService.getUserId())) > -1 || req.custCompID == userService.getUserCompanyId())) {
                //|| $scope.auctionItem.customerReqAccess || req.custCompID == userService.getUserCompanyId()) {
                //if(req.methodName == 'UpdateAuctionStartSignalR && !UpdateTime' && req.userIDList.indexOf(parseInt(userService.getUserId())) > -1){
                //    growlService.growl("Customer has updated the start time.", 'inverse');
                //} else
                if (req.methodName == "UpdateTime" && req.userIDList.indexOf(parseInt(userService.getUserId())) > -1) {
                    growlService.growl("Negotiation time has been updated.", 'inverse');
                } else if (req.methodName == "MakeBid" && (userService.getUserId() == req.customerID || req.superUserID == userService.getUserId() || $scope.auctionItem.customerReqAccess || req.custCompID == userService.getUserCompanyId())) {                    
                    growlService.growl("A vendor has made a bid.", "success");
                } else if (req.methodName == "RestartNegotiation") {
                    //growlService.growl("A vendor has made a bid.", "inverse");
                    window.location.reload();
                } else if (req.methodName == "RevUploadQuotation") {

                } else if (req.methodName == "StopBids") {
                    if (req.userIDList.indexOf(parseInt(userService.getUserId())) > -1) {
                        growlService.growl("Negotiation Time reduced to 1 minute by the customer. New bids will not extend time.");
                    }
                } else if (!$scope.NegotiationEnded) {
                    //if (!$scope.NegotiationEnded) {
                    auctionsService.isnegotiationended(id, userService.getUserToken())
                        .then(function (response) {
                            if (response.errorMessage == '') {
                                if (response.objectID == 1) {
                                    $scope.NegotiationEnded = true;
                                    if ((userService.getUserId() == req.customerID || req.superUserID == userService.getUserId() || $scope.auctionItem.customerReqAccess)) {
                                        swal("Negotiation Completed!", "Congratulations! your procurement process is now completed. " + $scope.toprankerName + " is the least bidder with the value " + $scope.auctionItem.minPrice + " " + $scope.auctionItem.currency + " \n Your savings through PRM 360 :" + ($scope.auctionItem.savings) + " " + $scope.auctionItem.currency, "success");
                                    } else if (req.userIDList.indexOf(parseInt(userService.getUserId())) > -1 && $scope.timeLeftMessage == "Negotiation Ends in: ") {
                                        if ($scope.vendorRank == 1) {
                                            swal("Negotiation Completed!", "Congratulations! you are the least bidder for this requirement. Your price is : " + $scope.auctionItem.minPrice + " " + $scope.auctionItem.currency + "\n Customer would be reaching out you shortly. All the best!", "success");
                                        } else {
                                            swal("Negotiation Completed!", "Bidding Completed.\n Thank You for your interest on this requirement. You ranked " + $scope.vendorRank + " in this requirement. Thank you for your participation.", "success");
                                        }
                                    }
                                }
                            } else {
                                //swal("Error", "Error in isNegotiationEnded", "inverse");
                            }
                        })
                    //  }


                }
                if (req.methodName == "UploadQuotation" || req.methodName == "RevUploadQuotation") {
                    if (userService.getUserId() == req.customerID || req.superUserID == userService.getUserId() || $scope.auctionItem.customerReqAccess || req.custCompID == userService.getUserCompanyId()) {
                        $scope.getData(req.methodName, req.callerID);
                    }
                } else {
                    $scope.getData(req.methodName, req.callerID);
                }

            }
        })


        $scope.stopBids = function () {
            swal({
                title: "Are you sure?",
                text: "The Negotiation will be stopped after one minute.",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#F44336",
                confirmButtonText: "Yes, Stop Bids!",
                closeOnConfirm: true
            }, function () {
                var params = {};
                params.reqID = id;
                params.sessionID = userService.getUserToken();
                params.userID = userService.getUserId();
                var parties = params.reqID + "$" + params.userID + "$" + params.sessionID;
                $scope.invokeSignalR('StopBids', parties);
                $scope.$broadcast('timer-set-countdown-seconds', 60);
                $scope.disableButtons();
                swal("Done!", "Negotiation time reduced to one minute.", "success");
                //requirementHub.invoke('StopBids', parties, function (req) {
                //    $scope.$broadcast('timer-set-countdown-seconds', 60);
                //    $scope.disableButtons();
                //    swal("Done!", "Negotiation time reduced to one minute.", "success");
                //});
            });
        };

        $scope.disableButtons = function () {
            $scope.buttonsDisabled = true;
        }

        $scope.enableButtons = function () {
            $scope.buttonsDisabled = false;
        }

        $scope.editRequirement = function () {
            $log.info('in edit' + $stateParams.Id);
            $state.go('form.addnewrequirement', { 'Id': $stateParams.Id });
        }

        $scope.generatePOforUser = function () {
            $state.go('generate-po', { 'Id': $stateParams.Id });
        }

        $scope.metrialDispatchmentForm = function () {
            $state.go('material-dispatchment', { 'Id': $stateParams.Id });
        }

        $scope.paymentdetailsForm = function () {
            $state.go('payment-details', { 'Id': $stateParams.Id });
        }


        $scope.deleteRequirement = function () {
            swal({
                title: "Are you sure?",
                text: "The Negotiation will be deleted and an email will be sent out to all vendors involved.",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#F44336",
                confirmButtonText: "Yes, I am sure",
                closeOnConfirm: true
            }, function () {
                var params = {};
                params.reqID = id;
                params.sessionID = userService.getUserToken();
                params.userID = userService.getUserId();
                // auctionsService.updatebidtime(params);
                // swal("Done!", "Auction time reduced to oneminute.", "success");
                var parties = params.reqID + "$" + params.userID + "$" + params.sessionID + "$" + $scope.reason;
                $scope.invokeSignalR('DeleteRequirement', parties);
                //requirementHub.invoke('DeleteRequirement', parties, function (req) {
                //    $scope.$broadcast('timer-set-countdown-seconds', 60);
                //    $scope.disableButtons();
                //    swal("Done!", "Requirement has been cancelled", "success");
                //});
            });
        }



        $scope.DeleteVendorFromAuction = function (VendoID, quotationUrl) {
            if (($scope.auctionItem.auctionVendors.length > 2 || quotationUrl == "") && (quotationUrl == "" || (quotationUrl != "" && $scope.auctionItem.auctionVendors[2].quotationUrl != ""))) {
                swal({
                    title: "Are you sure?",
                    text: "The Vendor will be deleted and an email will be sent out to The vendor.",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#F44336",
                    confirmButtonText: "Yes, I am sure",
                    closeOnConfirm: true
                }, function () {
                    var params = {};
                    params.reqID = id;
                    params.sessionID = userService.getUserToken();
                    params.userID = VendoID;

                    auctionsService.DeleteVendorFromAuction(params)
                        .then(function (response) {
                            if (response.errorMessage == '') {

                                $scope.getData();
                                swal("Done!", "Done! Vendor Deleted Successfully!", "success");
                            } else {
                                swal("Error", "You cannot Delete Vendor", "inverse");
                            }
                        })
                });
            }
            else {
                swal("Not Allowed", "You are not allowed to Delete the Vendors.", "error");
            }
        }


        $scope.vendorsFromPRM = 1;
        $scope.vendorsFromSelf = 2;

        // $scope.resetTimer = function () {
        //     $scope.auctionItem.TimeLeft.seconds = 1800;
        // }




        $scope.totalprice = 0;
        $scope.taxs = 0;
        $scope.vendorTaxes = 0;
        $scope.freightcharges = 0;
        $scope.discountAmount = 0;
        $scope.totalpriceinctaxfreight = 0;

        $scope.vendorBidPriceWithoutDiscount = 0;

        $scope.revtotalprice = 0;
        $scope.revtaxs = 0;
        $scope.revvendorTaxes = $scope.vendorTaxes;
        $scope.revfreightcharges = 0;
        $scope.revtotalpriceinctaxfreight = 0;

        $scope.priceValidationsVendor = '';

        $scope.calculatedSumOfAllTaxes = 0;

        $scope.taxValidation = false;

        $scope.ItemPriceValidation = false;
        $scope.discountfreightValidation = false;

        $scope.gstValidation = false;



        $scope.makeBidUnitPriceValidation = function (revUnitPrice, TempRevUnitPrice, productIDorName) {
            if (TempRevUnitPrice < revUnitPrice) {
                swal("Error!", 'Please enter price less than ' + TempRevUnitPrice + ' of ' + productIDorName);
            }            
        }


        $scope.unitPriceCalculation = function () {

            $scope.gstValidation = false;

            $scope.auctionItem.listRequirementItems.forEach(function (item, index) {

                if ($scope.auctionItem.isTabular && $scope.auctionItem.status == 'UNCONFIRMED') {
                                        
                    var tempUnitPrice = item.unitPrice;
                    var tempCGst = item.cGst;
                    var tempSGst = item.sGst;
                    var tempIGst = item.iGst;

                    if (item.unitPrice == undefined || item.unitPrice <= 0) {
                        item.unitPrice = 0;
                    };

                    item.itemPrice = item.unitPrice * item.productQuantity;

                    if (item.cGst == undefined || item.cGst <= 0) {
                        item.cGst = 0;
                    };
                    if (item.sGst == undefined || item.sGst <= 0) {
                        item.sGst = 0;
                    };
                    if (item.iGst == undefined || item.iGst <= 0) {
                        item.iGst = 0;
                    };

                    if (item.cGst < 10) {
                        if (item.cGst.toString().length > 4) {
                            tempCGst = 0;
                            item.cGst = 0;
                            swal("Error!", 'Please enter valid C-GST');
                        }
                    };

                    if (item.cGst >= 10 || item.cGst <= 100) {
                        if (item.cGst.toString().length > 5) {
                            tempCGst = 0;
                            item.cGst = 0;
                            swal("Error!", 'Please enter valid C-GST');
                        }
                    };

                    if (item.sGst < 10) {
                        if (item.sGst.toString().length > 4) {
                            tempSGst = 0;
                            item.sGst = 0;
                            swal("Error!", 'Please enter valid S-GST');
                        }
                    };

                    if (item.sGst >= 10 || item.sGst <= 100) {
                        if (item.sGst.toString().length > 5) {
                            tempSGst = 0;
                            item.sGst = 0;
                            swal("Error!", 'Please enter valid S-GST');
                        }
                    };

                    if (item.iGst < 10) {
                        if (item.iGst.toString().length > 4) {
                            tempIGst = 0;
                            item.iGst = 0;
                            swal("Error!", 'Please enter valid I-GST');
                        }
                    };

                    if (item.iGst >= 10 || item.iGst <= 100) {
                        if (item.iGst.toString().length > 5) {
                            tempIGst = 0;
                            item.iGst = 0;
                            swal("Error!", 'Please enter valid I-GST');
                        }
                    };

                    item.itemPrice = item.itemPrice + ((item.itemPrice / 100) * (item.cGst + item.sGst + item.iGst));

                    item.unitPrice = tempUnitPrice;
                    item.cGst = tempCGst
                    item.sGst = tempSGst;
                    item.iGst = tempIGst;

                    if (item.cGst < 0 || item.sGst < 0 || item.iGst < 0 || item.cGst == undefined || item.sGst == undefined || item.iGst == undefined || item.cGst > 100 || item.sGst > 100 || item.iGst > 100) {
                        $scope.gstValidation = true
                    };                    
                };


                if ($scope.auctionItem.isTabular && ($scope.auctionItem.status == 'Negotiation Ended' || $scope.auctionItem.status == 'STARTED')) {

                    var tempRevUnitPrice = item.revUnitPrice;
                    var tempCGst = item.cGst;
                    var tempSGst = item.sGst;
                    var tempIGst = item.iGst;

                    if (item.revUnitPrice == undefined || item.revUnitPrice <= 0) {
                        item.revUnitPrice = 0;
                    };

                    item.revitemPrice = item.revUnitPrice * item.productQuantity;

                    if (item.cGst == undefined || item.cGst <= 0) {
                        item.cGst = 0;
                    };
                    if (item.sGst == undefined || item.sGst <= 0) {
                        item.sGst = 0;
                    };
                    if (item.iGst == undefined || item.iGst <= 0) {
                        item.iGst = 0;
                    };

                    item.revitemPrice = item.revitemPrice + ((item.revitemPrice / 100) * (item.cGst + item.sGst + item.iGst));

                    item.revUnitPrice = tempRevUnitPrice;
                    item.cGst = tempCGst
                    item.sGst = tempSGst;
                    item.iGst = tempIGst;

                    if (item.cGst < 0 || item.sGst < 0 || item.iGst < 0 || item.cGst == undefined || item.sGst == undefined || item.iGst == undefined || item.cGst > 100 || item.sGst > 100 || item.iGst > 100) {
                        $scope.gstValidation = true;
                    };
                };
            });
        };


        $scope.getTotalPrice = function (discountAmount, freightcharges, totalprice) {
            $scope.priceValidationsVendor = '';
            $scope.taxValidation = false;
            $scope.ItemPriceValidation = false;
            $scope.discountfreightValidation = false;

            $scope.freightcharges = freightcharges;
            $scope.discountAmount = discountAmount;

            if (!$scope.auctionItem.isTabular) {
                $scope.totalprice = totalprice;
                if (isNaN($scope.totalprice) || $scope.totalprice == undefined || $scope.totalprice < 0 || $scope.totalprice == '') {
                    //$("#totalprice").val(0);
                    $scope.vendorBidPrice = 0;
                    $scope.vendorBidPriceWithoutDiscount = 0;
                }
            }
            else {
                $scope.auctionItem.listRequirementItems.forEach(function (item, index) {
                    if (isNaN(item.itemPrice) || item.itemPrice == undefined || item.itemPrice < 0) {
                        $scope.ItemPriceValidation = true;
                    }
                })
            }

            $scope.freightchargeslocal = $scope.freightcharges;
            if (isNaN($scope.freightcharges) || $scope.freightcharges == undefined || $scope.freightcharges < 0) {
                $scope.freightchargeslocal = 0;
                $scope.discountfreightValidation = true;
            }
            $scope.freightchargeslocal = parseFloat($scope.freightchargeslocal);

            $scope.discountAmountlocal = $scope.discountAmount;
            if (isNaN($scope.discountAmount) || $scope.discountAmount == undefined || $scope.discountAmount < 0) {
                $scope.discountAmountlocal = 0;
                $scope.discountfreightValidation = true;
            }
            $scope.discountAmountlocal = parseFloat($scope.discountAmountlocal);

            if ($scope.auctionItem.isTabular) {
                $scope.totalprice = _.sumBy($scope.auctionItem.listRequirementItems, 'itemPrice');

                //$scope.calculatedSumOfAllTaxes = 0;
                //$scope.listRequirementTaxes.forEach(function (tax, index) {
                //    if (tax.taxPercentage != '' && tax.taxPercentage != undefined) {
                //        $scope.calculatedSumOfAllTaxes += $scope.totalprice * tax.taxPercentage / 100;
                //    }
                //})
                //$scope.vendorTaxes = $scope.calculatedSumOfAllTaxes;
                //$scope.listRequirementTaxes.forEach(function (item, index) {
                //    if (item.taxName == null || item.taxName == '' || item.taxName == undefined) {
                //        $scope.taxValidation = true;
                //    }
                //    if (item.taxPercentage < 0 || item.taxPercentage == null || item.taxPercentage == undefined) {
                //        $scope.taxValidation = true;
                //    }
                //    if (item.taxPercentage <= 0) {
                //        item.taxPercentage = 0;
                //    }
                //});

                //$scope.vendorBidPrice = $scope.totalprice + $scope.vendorTaxes + $scope.freightchargeslocal;

                $scope.vendorBidPrice = $scope.totalprice + $scope.freightchargeslocal;
                
                $scope.vendorBidPriceWithoutDiscount = $scope.vendorBidPrice;
                $scope.vendorBidPrice = $scope.vendorBidPrice - $scope.discountAmountlocal;
                $scope.vendorBidPrice = $scope.vendorBidPrice.toFixed(4);
                if (!$scope.vendorBidPrice || isNaN($scope.vendorBidPrice)) {
                    $scope.vendorBidPrice = 0;
                }
            }
            if (!$scope.auctionItem.isTabular) {
                $scope.totalprice = parseFloat($scope.totalprice);

                $scope.calculatedSumOfAllTaxes = 0;
                $scope.listRequirementTaxes.forEach(function (tax, index) {
                    if (tax.taxPercentage != '' && tax.taxPercentage != undefined) {
                        $scope.calculatedSumOfAllTaxes += $scope.totalprice * tax.taxPercentage / 100;
                    }
                })

                $scope.vendorTaxes = $scope.calculatedSumOfAllTaxes;

                $scope.listRequirementTaxes.forEach(function (item, index) {
                    if (item.taxName == null || item.taxName == '' || item.taxName == undefined) {
                        $scope.taxValidation = true;
                    }
                    if (item.taxPercentage < 0 || item.taxPercentage == null || item.taxPercentage == undefined) {
                        $scope.taxValidation = true;
                    }
                    if (item.taxPercentage <= 0) {
                        item.taxPercentage = 0;
                    }
                });

                $scope.vendorBidPrice = $scope.totalprice + $scope.vendorTaxes + $scope.freightchargeslocal;

                $scope.vendorBidPriceWithoutDiscount = $scope.vendorBidPrice;
                $scope.vendorBidPrice = $scope.vendorBidPrice - $scope.discountAmountlocal;
                $scope.vendorBidPrice = $scope.vendorBidPrice.toFixed(4);
                if (!$scope.vendorBidPrice || isNaN($scope.vendorBidPrice)) {
                    $scope.vendorBidPrice = 0;
                }
            }
        }

        $scope.getRevTotalPrice = function (revfreightcharges, revtotalprice) {
            $scope.revfreightcharges = revfreightcharges;
            $scope.ItemPriceValidation = false;
            $scope.discountfreightValidation = false;

            if (!$scope.auctionItem.isTabular) {
                $scope.revtotalprice = revtotalprice;
                if (isNaN($scope.revtotalprice) || $scope.revtotalprice == undefined || $scope.revtotalprice < 0 || $scope.revtotalprice == '') {
                    $("#revtotalprice").val(0);
                    $scope.revvendorBidPrice = 0;
                }
            }
            else {
                $scope.auctionItem.listRequirementItems.forEach(function (item, index) {
                    if (isNaN(item.revitemPrice) || item.revitemPrice == undefined || item.revitemPrice < 0) {
                        $scope.ItemPriceValidation = true;
                    }
                })
            }
            $scope.revfreightchargeslocal = $scope.revfreightcharges;
            if (isNaN($scope.revfreightcharges) || $scope.revfreightcharges == undefined || $scope.revfreightcharges < 0) {
                $scope.revfreightchargeslocal = 0;
                $scope.discountfreightValidation = true;
            }
            $scope.revfreightchargeslocal = parseFloat($scope.revfreightchargeslocal);


            if ($scope.auctionItem.isTabular) {
                $scope.revtotalprice = _.sumBy($scope.auctionItem.listRequirementItems, 'revitemPrice');

                $scope.calculatedSumOfAllTaxes = 0;
                $scope.listRequirementTaxes.forEach(function (tax, index) {
                    if (tax.taxPercentage != '' && tax.taxPercentage != undefined) {
                        $scope.calculatedSumOfAllTaxes += $scope.revtotalprice * tax.taxPercentage / 100;
                    }
                })
                $scope.vendorTaxes = $scope.calculatedSumOfAllTaxes;

                $scope.revvendorBidPrice = $scope.revtotalprice + $scope.vendorTaxes + $scope.revfreightchargeslocal;
                $scope.revvendorBidPrice = $scope.revvendorBidPrice.toFixed(4);
                if (!$scope.revvendorBidPrice || isNaN($scope.revvendorBidPrice)) {
                    $scope.revvendorBidPrice = 0;
                }

                $scope.reduceBidValue = ($scope.auctionItem.minPrice - $scope.revvendorBidPrice).toFixed(4);

                $scope.reduceBidAmount();

                //reduceBidValue

            }
            if (!$scope.auctionItem.isTabular) {
                $scope.revtotalprice = parseFloat(revtotalprice);

                $scope.calculatedSumOfAllTaxes = 0;
                $scope.listRequirementTaxes.forEach(function (tax, index) {
                    if (tax.taxPercentage != '' && tax.taxPercentage != undefined) {
                        $scope.calculatedSumOfAllTaxes += $scope.revtotalprice * tax.taxPercentage / 100;
                    }
                })
                $scope.vendorTaxes = $scope.calculatedSumOfAllTaxes;

                $scope.revvendorBidPrice = $scope.revtotalprice + $scope.vendorTaxes + $scope.revfreightchargeslocal;
                $scope.revvendorBidPrice = $scope.revvendorBidPrice.toFixed(4);
                if (!$scope.revvendorBidPrice || isNaN($scope.revvendorBidPrice)) {
                    $scope.revvendorBidPrice = 0;
                }
            }

            

        }

        $scope.uploadQuotation = function (quotationType) {
            var params = {
                'quotationObject': $scope.auctionItem.listRequirementItems,
                'userID': userService.getUserId(),
                'reqID': $scope.auctionItem.requirementID,
                'sessionID': userService.getUserToken(),
                'price': $scope.totalprice,
                'tax': $scope.vendorTaxes,
                'freightcharges': $scope.freightcharges,
                'vendorBidPrice': $scope.vendorBidPrice,
                'warranty': $scope.warranty,
                'duration': $scope.duration,
                'payment': $scope.payment,
                'validity': $scope.validity,
                'otherProperties': $scope.otherProperties,
                'quotationType': quotationType,
                'revised': 0,
                'discountAmount': $scope.discountAmount,
                'listRequirementTaxes': $scope.listRequirementTaxes,
                'quotationAttachment': $scope.quotationAttachment
            };

            params.unitPrice = $scope.auctionItem.unitPrice;
            params.productQuantity = $scope.auctionItem.productQuantity;
            params.cGst = $scope.auctionItem.cGst;
            params.sGst = $scope.auctionItem.sGst;
            params.iGst = $scope.auctionItem.iGst;

            if ($scope.auctionItem.status == 'Negotiation Ended') {
                params.price = $scope.revtotalprice,
                    params.tax = $scope.vendorTaxes,
                    params.freightcharges = $scope.revfreightcharges,
                    params.vendorBidPrice = $scope.revvendorBidPrice,
                    params.revised = 1
            };


            if ($scope.auctionItem.status == 'UNCONFIRMED' && $scope.auctionItem.isQuotationPriceLimit && $scope.vendorBidPrice > $scope.auctionItem.quotationPriceLimit) {
                swal("Error!", 'Your Amount Should be less than Quotation Price limit :' + $scope.auctionItem.quotationPriceLimit, "error");
                return;
            };

            auctionsService.uploadQuotation(params)
                .then(function (req) {
                    if (req.errorMessage == '' || req.errorMessage == 'reviced') {
                        if (req.errorMessage == '') {
                            var parties = id + "$" + userService.getUserId() + "$" + userService.getUserToken();
                            //$scope.invokeSignalR('CheckRequirement', parties);
                            //    requirementHub.invoke('CheckRequirement', parties, function () {                    
                            //});
                        }
                        swal({
                            title: "Thanks!",
                            text: "Your Quotation uploaded successfully",
                            type: "success",
                            showCancelButton: false,
                            confirmButtonColor: "#DD6B55",
                            confirmButtonText: "Ok",
                            closeOnConfirm: true
                        },
                            function () {
                                location.reload();
                            });

                    } else {
                        swal("Error!", req.errorMessage, "error");
                    }
                });
        }



        $scope.getFile1 = function (id, itemid, ext) {
            $scope.file = $("#" + id)[0].files[0];
            fileReader.readAsDataUrl($scope.file, $scope)
                .then(function (result) {

                    if (id == "excelquotation") {
                        var bytearray = new Uint8Array(result);
                        $scope.quotationAttachment = $.makeArray(bytearray);
                        $scope.uploadquotationsfromexcel();
                    }
                    else {
                        var bytearray = new Uint8Array(result);
                        var arrayByte = $.makeArray(bytearray);
                        var ItemFileName = $scope.file.name;
                        var index = _.indexOf($scope.auctionItem.listRequirementItems, _.find($scope.auctionItem.listRequirementItems, function (o) { return o.productSNo == id; }));
                        var obj = $scope.auctionItem.listRequirementItems[index];
                        obj.itemAttachment = arrayByte;
                        obj.attachmentName = ItemFileName;
                        $scope.auctionItem.listRequirementItems.splice(index, 1, obj);
                    }
                });
        }

        $scope.QuotationRatioButton = {
            upload: 'generate'
        }

        $scope.rejectreson = '';
        $scope.rejectresonValidation = false;

        $scope.QuatationAprovelvalue = true;

        $scope.QuatationAprovel = function (userID, value, comment, action) {
            if (value && (comment == '' || comment == undefined)) {
                $scope.rejectresonValidation = true;
                return false;
            } else {
                $scope.rejectresonValidation = false;
            }

            var params = {
                'vendorID': userID,
                'reqID': $scope.auctionItem.requirementID,
                'sessionID': userService.getUserToken(),
                'customerID': userService.getUserId(),
                'value': value,
                'reason': comment,
                'action': action
            };

            if (!value) {
                var text = "Quotation Approved Successfully";
            }
            if (value) {
                var text = "Quotation Rejected Successfully";
            }

            auctionsService.QuatationAprovel(params).then(function (req) {
                if (req.errorMessage == '') {
                    swal({
                        title: "Done!",
                        text: text,
                        type: "success",
                        showCancelButton: false,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Ok",
                        closeOnConfirm: true
                    },
                        function () {
                            location.reload();
                        });

                } else {
                    swal("Error!", req.errorMessage, "error");
                }
            });
        }

        $scope.submitButtonShow = 0;

        $scope.submitButtonShowfunction = function (value) {
            $scope.submitButtonShow = value;
        }




        $scope.NegotiationSettingsValidationMessage = '';

        $scope.NegotiationTimeValidation = function (days, hours, mins, minReduction, rankComparision) {

            $scope.days = days;
            $scope.hours = hours;
            $scope.mins = mins;


            $log.info($scope.days);
            $log.info($scope.hours);
            $log.info($scope.mins);

            $scope.NegotiationSettingsValidationMessage = '';

            if (minReduction < 10 || minReduction == undefined) {
                $scope.NegotiationSettingsValidationMessage = 'Set Min 10 for Min.Amount reduction';
                return;
            }

            if (rankComparision < 10 || rankComparision == undefined) {
                $scope.NegotiationSettingsValidationMessage = 'Set Min 10 for Rank Comparision price';
                return;
            }

            if (minReduction >= 10 && rankComparision > minReduction) {
                $scope.NegotiationSettingsValidationMessage = 'Please enter Valid Rank Comparision price less than Min. Amount reduction';
                return;
            }

            if (days == undefined || days < 0) {
                $scope.NegotiationSettingsValidationMessage = 'Please Enter Valid Days';
                return;
            }
            if (hours < 0) {
                $scope.NegotiationSettingsValidationMessage = 'Please Enter Valid Hours';
                return;
            }
            if (mins < 0) {
                $scope.NegotiationSettingsValidationMessage = 'Please Enter Valid Minunts';
                return;
            }
            if (mins >= 60 || mins == undefined) {
                $scope.NegotiationSettingsValidationMessage = 'Please Enter Valid Minunts';
                return;
            }
            if (hours > 24 || hours == undefined) {
                $scope.NegotiationSettingsValidationMessage = 'Please Enter Valid Hours';
                return;
            }
            if (hours == 24 && mins > 0) {
                $scope.NegotiationSettingsValidationMessage = 'Please Enter Valid Hours & Minuts';
                return;
            }
            if (mins < 5 && hours == 0 && days == 0) {
                $scope.NegotiationSettingsValidationMessage = 'Please Enter Min 5 Minutes';
                return;
            }
        }



        $scope.listRequirementTaxes = [];
        $scope.reqTaxSNo = 1;

        $scope.listRequirementTaxeslength = $scope.listRequirementTaxes.length;


        $scope.requirementTaxes =
            {
                taxSNo: $scope.reqTaxSNo++,
                taxName: '',
                taxPercentage: 0
            }
        //$scope.listRequirementTaxes.push($scope.requirementTaxes);

        $scope.AddTax = function () {

            if ($scope.listRequirementTaxes.length > 4) {
                return;
            }
            $scope.requirementTaxes =
                {
                    taxSNo: $scope.reqTaxSNo++,
                    taxName: '',
                    taxPercentage: 0
                }
            $scope.listRequirementTaxes.push($scope.requirementTaxes);
            $scope.listRequirementTaxeslength = $scope.listRequirementTaxes.length;
            $scope.getTotalPrice($scope.discountAmount, $scope.freightcharges, $scope.totalprice);
        };

        $scope.deleteTax = function (SNo) {
            $scope.listRequirementTaxes = _.filter($scope.listRequirementTaxes, function (x) { return x.taxSNo !== SNo; });
            $scope.listRequirementTaxeslength = $scope.listRequirementTaxes.length;
            $scope.getTotalPrice($scope.discountAmount, $scope.freightcharges, $scope.totalprice);
        };


        $scope.reports = {};

        $scope.isReportGenerated = 0;

        $scope.GetReportsRequirement = function () {
            auctionsService.GetReportsRequirement({ "reqid": $stateParams.Id, "sessionid": userService.getUserToken(), 'userid': userService.getUserId() })
                .then(function (response) {
                    $scope.reports = response;
                    $scope.getData();
                    $scope.isReportGenerated = 1;
                });
        }


        $scope.generateReports = function () {
            $scope.isReportGenerated = 0;
        }




        $scope.savepricecap = function () {
            $scope.l1Price = $scope.auctionItem.auctionVendors[0].totalPriceIncl;

            $scope.l1CompnyName = $scope.auctionItem.auctionVendors[0].companyName;

            if ($scope.l1CompnyName == 'PRICE_CAP') {
                $scope.l1Price = $scope.auctionItem.auctionVendors[1].totalPriceIncl;
            }

            if ($scope.auctionItem.reqType == 'PRICE_CAP' && $scope.auctionItem.priceCapValue >= $scope.l1Price) {
                swal("Error!", 'Price CAP Value should be less then all the vendors price', "error");
                return;
            }

            if ($scope.auctionItem.reqType == 'PRICE_CAP' && ($scope.auctionItem.priceCapValue <= 0 || $scope.auctionItem.priceCapValue == undefined || $scope.auctionItem.priceCapValue == '')) {
                swal("Error!", 'Please Enter Valid Price cap', "error");
                return;
            }

            var params = {
                'reqID': $scope.auctionItem.requirementID,
                'sessionID': userService.getUserToken(),
                'userID': userService.getUserId(),
                'reqType': $scope.auctionItem.reqType,
                'priceCapValue': $scope.auctionItem.priceCapValue,
                'isUnitPriceBidding': $scope.auctionItem.isUnitPriceBidding
            };

            auctionsService.savepricecap(params).then(function (req) {
                if (req.errorMessage == '') {
                    swal({
                        title: "Done!",
                        text: 'Requirement Type Saved Successfully',
                        type: "success",
                        showCancelButton: false,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Ok",
                        closeOnConfirm: true
                    },
                        function () {
                            location.reload();
                        });
                } else {
                    swal("Error!", req.errorMessage, "error");
                }
            });
        }




        $scope.goToReqTechSupport = function () {
            //$state.go("reqTechSupport", { "reqId": $scope.reqId }, { reload: true, newtab: true });

            var url = $state.href("reqTechSupport", { "reqId": $scope.reqId });
            window.open(url, '_blank');
        }


    });﻿prmApp

    // =========================================================================
    // AUCTION ITEM
    // =========================================================================

    .controller('itemCtrl', function ($scope, $filter, $stateParams, $http, domain, fileReader, $state, $timeout, auctionsService, userService, SignalRFactory, growlService, $log, signalRHubName) {

        var id = $stateParams.Id;
        $scope.showTimer = false;
        $scope.userIsOwner = false;

        $scope.currentID = -1;
        $scope.timerStyle = { 'color': '#000' };
        //$scope.signalR.on('updateTime',function(data){
        $scope.bidAttachement = [];
        $scope.bidAttachementName = "";
        $scope.bidAttachementValidation = false;
        $scope.bidPriceEmpty = false;
        $scope.bidPriceValidation = false;
        $scope.showStatusDropDown = false;
        $scope.vendorInitialPrice = 0;
        $scope.showGeneratePOButton = false;
        $scope.isDeleted = false;
        $scope.ratingForVendor = 0;
        $scope.ratingForCustomer = 0;
        // $scope.isTimerStarted = false;

        $log.info('trying to connect to service')
        var requirementHub = SignalRFactory('', signalRHubName);
        $log.info('connected to service')

        requirementHub.on('checkRequirement', function (req) {
            //$log.info("client function called");
            // if($scope.auctionItem.requirementID == req.requirementID){
            //     $scope.auctionItem = req;
            //     $scope.setFields();
            // }
            if (id == req.requirementID && (userService.getUserId() == req.customerID || userService.getUserId() == req.superUserID || req.userIDList.indexOf(parseInt(userService.getUserId())) > -1)) {
                if (req.methodName == 'UpdateAuctionStartSignalR' && req.userIDList.indexOf(parseInt(userService.getUserId())) > -1) {
                    growlService.growl("Customer has updated the start time.", 'inverse');
                } else if (req.methodName == "UpdateTime" && req.userIDList.indexOf(parseInt(userService.getUserId())) > -1) {
                    growlService.growl("Auction time has been updated.", 'inverse');
                } else if (req.methodName == "MakeBid" && (userService.getUserId() == req.customerID || req.superUserID == userService.getUserId())) {
                    growlService.growl("A vendor has made a bid.", "inverse");
                }
                $scope.getData();
            }
        })

        /*$.ajax({
            url: "http://www.timeapi.org/ist/now.json", 
            type: "GET",   
            dataType: 'jsonp',
            cache: false,
            success: function(response){                          
                alert(response.dateString);                   
            }           
        }); */

        $scope.updateTimeLeftSignalR = function () {
            var parties = id + "$" + userService.getUserId() + "$" + "10000" + "$" + userService.getUserToken();
            requirementHub.invoke('UpdateTime', parties, function (req) {
                //$log.info(req);
            })
        };

        $scope.selectVendor = function () {
            var params = {
                userID: userService.getUserId(),
                vendorID: vendorID,
                reqID: $scope.auctionItem.requirementID,
                reason: $scope.reason,
                sessionID: userService.getUserToken()
            }
            auctionsService.rateVendor(params)
                .then(function (response) {
                    if (response.errorMessage == "") {
                        growlService.growl("Vendor " + response.userInfo.firstName + " " + response.userInfo.lastName + " has been selected for the final  ", "inverse");
                    }
                })
        }

        $scope.rateVendor = function (vendorID) {
            var params = {
                uID: userService.getUserId(),
                userID: vendorID,
                rating: $scope.ratingForVendor,
                sessionID: userService.getUserToken()
            }
            auctionsService.rateVendor(params)
                .then(function (response) {
                    if (response.errorMessage == "") {
                        growlService.growl("Rating saved successfully. You can edit your rating as well.", "inverse");
                    }
                })
        }

        $scope.rateCustomer = function () {
            var params = {
                uID: userService.getUserId(),
                userID: $scope.auctionItem.customerID,
                rating: $scope.ratingForCustomer,
                sessionID: userService.getUserToken()
            }
            auctionsService.rateVendor(params)
                .then(function (response) {
                    if (response.errorMessage == "") {
                        growlService.growl("Rating saved successfully. You can edit your rating as well.", "inverse");
                    }
                })
        }

        $scope.makeaBid1 = function () {
            var bidPrice = $("#makebidvalue").val();
            //$log.info($scope.auctionItem.minPrice);
            $log.info(bidPrice);



            if (bidPrice == "" || Number(bidPrice) <= 0) {
                $scope.bidPriceEmpty = true;
                $scope.bidPriceValidation = false;
                $("#makebidvalue").val("");
                return false;
            } else if (!isNaN($scope.auctionItem.minPrice) && $scope.auctionItem.minPrice > 0 && bidPrice >= $scope.auctionItem.minPrice) {
                $scope.bidPriceValidation = true;
                $scope.bidPriceEmpty = false;
                return false;
            } else {
                $scope.bidPriceValidation = false;
                $scope.bidPriceEmpty = false;
            }
            if (($scope.bidAttachementName == "" || $scope.bidAttachement.length == 0) && $scope.quotationStatus == false) {
                $scope.bidAttachementValidation = true;
                return false;
            } else {
                $scope.bidAttachementValidation = false;
            }
            if (bidPrice < (95 * $scope.auctionItem.auctionVendors[0].runningPrice / 100)) {
                swal({
                    title: "Are you sure?",
                    text: "The bid price is less than 95% of your previous bid. Are you sure you want to go forward with the bid?",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#F44336",
                    confirmButtonText: "OK",
                    closeOnConfirm: true
                }, function () {
                    var params = {};
                    params.reqID = parseInt(id);
                    params.sessionID = userService.getUserToken();
                    params.userID = parseInt(userService.getUserId());
                    params.price = parseFloat(bidPrice);
                    params.quotationName = $scope.bidAttachementName;
                    //$log.info(params);
                    //var parties = params.reqID + "$" + params.userID + "$" + params.price + "$" + params.quotation + "$" + params.quotationName + "$" + params.sessionID;
                    requirementHub.invoke('MakeBid', params, function (req) {
                        //auctionsService.makeabid(params).then(function(req){
                        if (req.errorMessage == '') {
                            swal("Thanks !", "Your bidding process has been successfully updated", "success");
                            $("#makebidvalue").val("");
                            $scope.getData();
                            // $(".removeattachedquotes").trigger('click');
                        } else {
                            swal("Error!", req.errorMessage, "error");
                        }
                    });
                });
            }
            else if (bidPrice > $scope.auctionItem.auctionVendors[0].runningPrice - $scope.auctionItem.minBidAmount) {
                swal("Error!", "Your amount must be at least " + $scope.auctionItem.minBidAmount + " less than your previous bid.", 'error');
                return false;
            }
            else {
                // function () {
                var params = {};
                params.reqID = parseInt(id);
                params.sessionID = userService.getUserToken();
                params.userID = parseInt(userService.getUserId());
                params.price = parseFloat(bidPrice);
                params.quotationName = $scope.bidAttachementName;
                //$log.info(params);
                //var parties = params.reqID + "$" + params.userID + "$" + params.price + "$" + params.quotation + "$" + params.quotationName + "$" + params.sessionID;
                requirementHub.invoke('MakeBid', params, function (req) {
                    //auctionsService.makeabid(params).then(function(req){
                    if (req.errorMessage == '') {
                        swal("Thanks !", "Your bidding process has been successfully updated", "success");
                        $("#makebidvalue").val("");
                        $scope.getData();
                        // $(".removeattachedquotes").trigger('click');
                    } else {
                        swal("Error!", req.errorMessage, "error");
                    }
                });
                // });
            }
            //return false;
            //$scope.auctionItem.minPrice

        }

        //return false;
        //$scope.auctionItem.minPrice


        $scope.setFields = function () {
            if ($scope.auctionItem.status == "CLOSED") {
                $scope.mactrl.skinSwitch('green');
                if (($scope.auctionItem.customerID == userService.getUserId() || $scope.auctionItem.superUserID == userService.getUserId())) {
                    $scope.errMsg = "Negotiation has been completed. You can generate the Purchase Order by pressing the button below.";
                }
                else {
                    $scope.errMsg = "Negotiation has completed.";
                }
                $scope.showStatusDropDown = false;
                $scope.showGeneratePOButton = true;
            } else if ($scope.auctionItem.status == "UNCONFIRMED" || $scope.auctionItem.status == "NOTSTARTED") {
                $scope.mactrl.skinSwitch('teal');
                $scope.errMsg = "Negotiation has not started yet.";
                $scope.showStatusDropDown = false;
                $scope.auctionStarted = false;
                $scope.timeLeftMessage = "Auction Starts in: ";
                $scope.startBtns = true;
                $scope.customerBtns = false;
            } else if ($scope.auctionItem.status == "STARTED") {
                $scope.mactrl.skinSwitch('orange');
                $scope.errMsg = "Negotiation has started.";
                $scope.showStatusDropDown = false;
                $scope.auctionStarted = true;
                $scope.timeLeftMessage = "Auction Ends in: ";
                $scope.startBtns = false;
                $scope.customerBtns = true;
            } else if ($scope.auctionItem.status == "DELETED") {
                $scope.mactrl.skinSwitch('bluegray');
                $scope.errMsg = "This requirement has been cancelled.";
                $scope.showStatusDropDown = false;
                $scope.isDeleted = true;
            } else if ($scope.auctionItem.status == "PO Generated") {
                $scope.mactrl.skinSwitch('lightblue');
                $scope.errMsg = "The PO has been generated. Please find the PO here: ";
                $scope.showStatusDropDown = true;
            } else {
                $scope.mactrl.skinSwitch('lightblue');
                $scope.showStatusDropDown = true;
            }
            if (($scope.auctionItem.customerID == userService.getUserId() || $scope.auctionItem.superUserID == userService.getUserId())) {
                $scope.userIsOwner = true;
                $scope.options = ['PO Sent', 'Material Received', 'Payment Processing', 'Payment Released'];
                $scope.options.push($scope.auctionItem.status);
            }
            var start = $scope.auctionItem.startTime.split('+')[0].split('(')[1];
            auctionsService.getdate()
                .then(function (responseFromServer) {
                    var dateFromServer = new Date(parseInt(responseFromServer.substr(6)));
                    $log.debug(dateFromServer);
                    var curDate = dateFromServer;

                    var myEpoch = curDate.getTime();
                    $scope.timeLeftMessage = "";
                    if (start > myEpoch) {
                        $scope.auctionStarted = false;
                        $scope.timeLeftMessage = "Negotiation Starts in: ";
                        $scope.startBtns = true;
                        $scope.customerBtns = false;
                    } else {
                        $scope.auctionStarted = true;
                        $scope.timeLeftMessage = "Negotiation Ends in: ";
                        $scope.startBtns = false;
                        $scope.customerBtns = true;
                    }
                    if ($scope.auctionItem.customerID != userService.getUserId() && $scope.auctionItem.superUserID != userService.getUserId()) {
                        $scope.startBtns = false;
                        $scope.customerBtns = false;
                    }
                    if ($scope.auctionItem.timeLeft == null || $scope.auctionItem.timeLeft < 1) {
                        $scope.showTimer = false;
                        $scope.disableButtons();
                    } else {
                        $scope.showTimer = true;
                    }
                    //$scope.auctionItem.postedOn = new Date(parseFloat($scope.auctionItem.postedOn.substr(6)));
                    var date = $scope.auctionItem.postedOn.split('+')[0].split('(')[1];
                    var newDate = new Date(parseInt(parseInt(date)));
                    $scope.auctionItem.postedOn = newDate.toString().replace('Z', '');

                    //var date1 = $scope.auctionItem.deliveryTime.split('+')[0].split('(')[1];
                    //var newDate1 = new Date(parseInt(parseInt(date)));
                    //$scope.auctionItem.deliveryTime = newDate.toString().replace('Z', '');


                    var minPrice = 0;
                    if ($scope.auctionItem.status == "NOTSTARTED") {
                        $scope.auctionItem.minPrice = $scope.auctionItem.auctionVendors[0] ? $scope.auctionItem.auctionVendors[0].initialPrice : 0;
                    } else {
                        $scope.auctionItem.minPrice = $scope.auctionItem.auctionVendors[0] ? $scope.auctionItem.auctionVendors[0].runningPrice : 0;
                    }
                    //$scope.auctionItem.deliveryTime = $scope.auctionItem.deliveryTime.split("GMT")[0];
                    for (var i in $scope.auctionItem.auctionVendors) {
                        var vendor = $scope.auctionItem.auctionVendors[i]

                        if (vendor.vendorID == userService.getUserId() && vendor.quotationUrl == "") {
                            $scope.quotationStatus = false;
                            $scope.quotationUploaded = false;
                        } else {
                            $scope.quotationStatus = true;
                            if ($scope.auctionItem.customerID != userService.getUserId() && $scope.auctionItem.superUserID != userService.getUserId()) {
                                $scope.quotationUploaded = true;
                            }
                            $scope.quotationUrl = vendor.quotationUrl;
                            $scope.vendorQuotedPrice = vendor.runningPrice;
                        }
                        if (i == 0 && vendor.initialPrice != 0) {
                            minPrice = vendor.initialPrice;
                        } else {
                            if (vendor.initialPrice < minPrice && vendor.initialPrice != 0) {
                                minPrice = vendor.initialPrice;
                            }
                        }
                        $scope.vendorInitialPrice = minPrice;
                        var runningMinPrice = 0;
                        if ($scope.auctionItem.auctionVendors[i].runningPrice > 0 && $scope.auctionItem.auctionVendors[i].runningPrice < $scope.vendorInitialPrice) {
                            runningMinPrice = $scope.auctionItem.auctionVendors[i].runningPrice;
                        }
                        //$scope.auctionItem.minPrice = runningMinPrice;
                        if ($scope.auctionItem.auctionVendors[i].runningPrice == 0) {
                            $scope.auctionItem.auctionVendors[i].runningPrice = 'NA';
                            $scope.auctionItem.auctionVendors[i].totalPriceIncl = 'NA';
                            $scope.auctionItem.auctionVendors[i].rank = 'NA';
                        } else {
                            $scope.vendorRank = vendor.rank;
                            if (vendor.rank == 1) {
                                $scope.toprankerName = vendor.vendorName;
                                if (userService.getUserId() == vendor.vendorID) {
                                    $scope.options = ['PO Accepted', 'Material Dispatched', 'Payment Acknowledged'];
                                    $scope.options.push($scope.auctionItem.status);
                                    if ($scope.auctionItem.status == "STARTED") {
                                        $scope.enableMakeBids = true;
                                    }
                                }
                            }
                            $scope.auctionItem.auctionVendors[i].totalPriceIncl = $scope.auctionItem.auctionVendors[i].runningPrice + ($scope.auctionItem.auctionVendors[i].runningPrice * $scope.auctionItem.auctionVendors[i].taxes) / 100;
                        }
                        if ($scope.auctionItem.auctionVendors[i].initialPrice == 0) {
                            $scope.auctionItem.auctionVendors[i].initialPrice = 'NA';
                        }
                    }
                    $scope.$broadcast('timer-set-countdown-seconds', $scope.auctionItem.timeLeft);
                    $('.datetimepicker').datetimepicker({
                        useCurrent: false,
                        icons: {
                            time: 'glyphicon glyphicon-time',
                            date: 'glyphicon glyphicon-calendar',
                            up: 'glyphicon glyphicon-chevron-up',
                            down: 'glyphicon glyphicon-chevron-down',
                            previous: 'glyphicon glyphicon-chevron-left',
                            next: 'glyphicon glyphicon-chevron-right',
                            today: 'glyphicon glyphicon-screenshot',
                            clear: 'glyphicon glyphicon-trash',
                            close: 'glyphicon glyphicon-remove'

                        },

                        minDate: curDate
                    });
                })
        }



        $scope.vendorBidPrice = null;
        $scope.auctionStarted = true;
        $scope.customerBtns = true;
        $scope.showVendorTable = true;
        $scope.quotationStatus = true;
        $scope.toprankerName = "";
        $scope.vendorRank = 0;
        $scope.quotationUrl = "";
        $scope.vendorBtns = false;
        $scope.vendorQuotedPrice = 0;
        $scope.startBtns = false;
        $scope.commentsvalidation = false;
        // $scope.userType = userService.getUserType();
        // if ($scope.userType == "VENDOR") {
        //     $scope.customerBtns = false;
        //     $scope.vendorBtns = true;
        //     $scope.showVendorTable = false;
        // }
        $scope.enableMakeBids = false;
        $scope.price = "";
        $scope.startTime = '';
        $scope.customerID = userService.getUserId();

        $scope.getData = function () {
            auctionsService.getrequirementdata({ "reqid": $stateParams.Id, "sessionid": userService.getUserToken(), 'userid': userService.getUserId() })
                .then(function (response) {
                    $scope.auctionItem = response;

                    //  $scope.auctionItem.description = $scope.auctionItem.description.replace("\u000a", "\n")
                    $scope.desc = $scope.auctionItem.description.replace(/\u000a/g, "</br>");
                    var id = parseInt(userService.getUserId());
                    var result = $scope.auctionItem.auctionVendors.filter(function (obj) {
                        return obj.vendorID == id;
                    });
                    if ((id != $scope.auctionItem.customerID && id != $scope.auctionItem.superUserID) && result.length == 0) {
                        swal("Access denied", "You do not have access to this requirement because you are not part of this requirements process.", 'error');
                        $state.go('home');
                    } else {
                        $scope.setFields();
                        auctionsService.getcomments({ "reqid": $stateParams.Id, "userid": userService.getUserId(), "sessionid": userService.getUserToken() })
                            .then(function (response) {
                                $scope.Comments = response;
                            });

                    }

                });

        }
        $scope.getData();
        $scope.AmountSaved = 0;

        /*var intervalPromise = window.setInterval(function(){
            if(window.location.hash.indexOf("#/view-requirement") > -1){
                $scope.getData();
            }            
        }, 10000);*/

        $scope.generatePDFonHTML = function () {
            auctionsService.getdate()
                .then(function (response) {
                    var date = new Date(parseInt(response.substr(6)));
                    var obj = $scope.auctionItem;
                    $scope.POTemplate = "<div id='POTemplate' style='display:none;'><html><head><title>PRM360</title><style>.date{margin-left: 850px;}.to{margin-left: 250px;}.name{margin-left: 300px;}.sub{margin-left: 450px;}img{position: absolute; left: 750px; top:75px; z-index: -1;}</style></head><body><header><br><br><br><img src='acads360.jpg' width='50' height='50'><h1 align='center'>PRM360<img </h1></header><br><div class='date'><p><b>Date:</b> " + date + "</p><p><b>PO No:</b> " + obj.requirementID + "</p></div><div class='to'><p>To,</p><p><b>" + obj.CompanyName + ",</b></p><p><b>" + obj.deliveryLocation + ".</b></p></div><p class='name'><b>Hello </b> " + obj.auctionVendors[0].vendorName + "</p><p class='sub'><b>Sub:</b> " + obj.title + "</p><p align='center'><b>Bill of Material</b></p><table border='1' cellpadding='2' style='width:60%' align='center'><tr><th>Product Name</th><th>Description</th><th>Price</th></tr><tr><td>" + obj.title + "</td><td>" + obj.description + "</td><td>" + obj.price + "</td></tr></table><p class='to'><b>Terms & Conditions</b></p><div class='name'> <p>1. Payment : " + obj.paymentTerms + ".</p><p>2. Delivery : " + obj.deliveryLocation + ".</p><p>3. Tax : " + obj.taxes + ".</p></div><p class='to'><b>Billing and Shipping Address:</b></p><p class='to'>Savvy Associates, # 3-4-174/21/2, Radha Krishna Nagar, Attapur, Hyderguda, Hyderabad - 500048</p><p align=center>This is a system generated PO, henceforth sign and seal is not required.</p><br><footer class='to'>M/s. Savvy Associates, H.No: 3-4-174/21/2, Radha Krishna Nagar, Attapur, Hyderguda, Hyderabad – 48Contact,M: 91-9949245791.,<br>E: savvyassociates@gmail.com.<br><b>URL:</b> www.savvyassociates.com. </footer></body></html></div>";
                    var content = document.getElementById('content');
                    content.insertAdjacentHTML('beforeend', $scope.POTemplate);
                })
        }

        $scope.generatePOasPDF = function (divName) {
            $scope.generatePDFonHTML();
            var printContents = document.getElementById(divName).innerHTML;
            var originalContents = document.body.innerHTML;

            if (navigator.userAgent.toLowerCase().indexOf('chrome') > -1) {
                var popupWin = window.open('', '_blank', 'width=600,height=600,scrollbars=no,menubar=no,toolbar=no,location=no,status=no,titlebar=no');
                popupWin.window.focus();
                popupWin.document.write('<!DOCTYPE html><html><head>' +
                    '<link rel="stylesheet" type="text/css" href="style.css" />' +
                    '</head><body onload="window.print()"><div class="reward-body">' + printContents + '</div></html>');
                popupWin.onbeforeunload = function (event) {
                    popupWin.close();
                    return '.\n';
                };
                popupWin.onabort = function (event) {
                    popupWin.document.close();
                    popupWin.close();
                }
            } else {
                var popupWin = window.open('', '_blank', 'width=800,height=600');
                popupWin.document.open();
                popupWin.document.write('<html><head><link rel="stylesheet" type="text/css" href="style.css" /></head><body onload="window.print()">' + printContents + '</html>');
                popupWin.document.close();
            }
            popupWin.document.close();
            var params = {
                reqid: $scope.auctionItem.requirementID,
                userid: userService.getUserId(),
                status: 'PO Generated',
                type: "WINVENDOR",
                sessionID: userService.getUserToken()
            };
            auctionsService.updateStatus(params)
                .then(function (response) {
                    if (response.errorMessage == '') {
                        $scope.getData();
                        //doc.save("DOC.PDF");
                    } else {
                        swal("Error", 'An Unexpected error has occurred. Please contact support@prm360.com for resolving this.', 'error');
                    }

                })

            return true;
        }

        $scope.generatePO = function () {
            var doc = new jsPDF();
            doc.setFontSize(40);
            //doc.text(40, 30, "Octocat loves jsPDF", 4);
            /*doc.fromHTML($("#POTemplate")[0], 15, 15, {
                "width": 170,
                function() {
                    $scope.POFile = $.makeArray(new Uint8Array(doc.output('arraybuffer')));
                }
            })*/

            var params = {
                reqid: $scope.auctionItem.requirementID,
                userid: userService.getUserId(),
                status: 'PO Generated',
                type: "WINVENDOR",
                sessionID: userService.getUserToken()
            };
            auctionsService.updateStatus(params)
                .then(function (response) {
                    if (response.errorMessage == '') {
                        $scope.getData();
                        doc.save("DOC.PDF");
                    } else {
                        swal("Error", 'An Unexpected error has occurred. Please contact support@prm360.com for resolving this.', 'error');
                    }

                })
        }

        $scope.generatePOinServer = function () {
            if ($scope.POTemplate == "") {
                $scope.generatePDFonHTML();

            }
            var doc = new jsPDF('p', 'in', 'letter');
            var specialElementHandlers = {};
            var doc = new jsPDF();
            //doc.setFontSize(40);
            doc.fromHTML($scope.POTemplate, 0.5, 0.5, {
                'width': 7.5, // max width of content on PDF
            });
            //doc.save("DOC.PDF");
            doc.output("dataurl");
            $scope.POFile = $.makeArray(new Uint8Array(doc.output('arraybuffer')));
            var params = {
                POfile: $scope.POFile,
                reqid: $scope.auctionItem.requirementID,
                userid: userService.getUserId(),
                POfileName: 'PO_req_' + $scope.auctionItem.requirementID + '.pdf',
                sessionID: userService.getUserToken()
            }

            auctionsService.generatePOinServer(params)
                .then(function (response) {
                    if (response.errorMessage == '') {
                        $scope.showStatusDropDown = true;
                        $scope.getData();
                    } else {
                        swal("Error", 'An Unexpected error has occurred. Please contact support@prm360.com for resolving this.', 'error');
                    }

                })
        }

        $scope.updateStatus = function () {
            var params = {
                reqid: $scope.auctionItem.requirementID,
                userid: userService.getUserId(),
                status: $scope.auctionItem.status,
                type: "WINVENDOR",
                sessionID: userService.getUserToken()
            };
            auctionsService.updateStatus(params)
                .then(function (response) {
                    if (response.errorMessage == '') {
                        $scope.getData();
                    } else {
                        swal("Error", 'An Unexpected error has occurred. Please contact support@prm360.com for resolving this.', 'error');
                    }

                })

        }

        $scope.saveComment = function () {
            var commentText = "";
            if ($scope.newComment && $scope.newComment != "") {
                commentText = $socpe.newComment;
            } else if ($("#comment")[0].value != '') {
                commentText = $("#comment")[0].value;
            } else {
                $scope.commentsvalidation = true;
            }
            if (!$scope.commentsvalidation) {
                var params = {};
                auctionsService.getdate()
                    .then(function (response) {
                        var date = new Date(parseInt(response.substr(6)));
                        var myEpoch = date.getTime();
                        params.requirementID = id;
                        params.firstName = "";
                        params.lastName = "";
                        params.replyCommentID = -1;
                        params.commentID = -1;
                        params.errorMessage = "";
                        params.createdTime = "/Date(" + myEpoch + "+0000)/";
                        params.sessionID = userService.getUserToken();
                        params.userID = userService.getUserId();
                        params.commentText = commentText;
                        requirementHub.invoke('SaveComment', params, function (response) {
                            //auctionsService.savecomment(params).then(function (response) {
                            $scope.getData();
                            $scope.newComment = "";
                            $scope.commentsvalidation = false;
                        });
                    });
            }
        }

        $scope.getFile = function () {
            $scope.progress = 0;
            $scope.file = $("#quotation")[0].files[0];
            $log.info($("#quotation")[0].files[0]);
            fileReader.readAsDataUrl($scope.file, $scope)
                .then(function (result) {
                    var bytearray = new Uint8Array(result);
                    $scope.bidAttachement = $.makeArray(bytearray);
                    $scope.bidAttachementName = $scope.file.name;
                    $scope.enableMakeBids = true;
                });
        };

        $scope.updateTime = function (time) {
            var isDone = false;
            $scope.$on('timer-tick', function (event, args) {
                if (!isDone) {
                    addCDSeconds("timer", time);
                    isDone = true;
                    var params = {};
                    params.reqID = id;
                    params.sessionID = userService.getUserToken();
                    params.userID = userService.getUserId();
                    params.newTicks = ($scope.countdownVal + time);
                    // auctionsService.updatebidtime(params).then(function(response){
                    //     $scope.getData();   
                    // });
                    var parties = id + "$" + userService.getUserId() + "$" + params.newTicks + "$" + userService.getUserToken();
                    requirementHub.invoke('UpdateTime', parties, function (req) {
                        //$log.info(req);
                    })
                }
            });
        }

        $scope.updateAuctionStart = function () {
            var params = {};
            params.auctionID = id;

            var startValue = $scope.startTime;

            if (startValue && startValue != null && startValue != "") {
                var ts = moment($scope.startTime, "DD-MM-yyyy HH:mm").valueOf();
                var m = moment(ts);
                var auctionStartDate = new Date(m);
                auctionsService.getdate()
                    .then(function (response1) {
                        var CurrentDate = moment(new Date(parseInt(response1.substr(6))));
                        $log.debug(CurrentDate < auctionStartDate);
                        $log.debug('div' + auctionStartDate);
                        if (CurrentDate >= auctionStartDate) {

                            swal("Done!", "Your Negotiation Start Time should be greater than current time.", "error");
                            return;
                        }


                        var milliseconds = parseInt(auctionStartDate.getTime() / 1000.0);
                        params.postedOn = "/Date(" + milliseconds + "000+0530)/";
                        params.auctionEnds = "/Date(" + milliseconds + "000+0530)/";
                        params.customerID = userService.getUserId();
                        params.sessionID = userService.getUserToken();
                        if ($scope.auctionItem.auctionVendors.length == 0 || $scope.auctionItem.auctionVendors[0].quotationUrl == "") {
                            swal("Not Allowed", "You are not allowed to create a start time until at least one vendor makes a bid.", "error");
                        } else {

                            requirementHub.invoke('UpdateAuctionStartSignalR', params, function (req) {
                                swal("Done!", "Your Negotiation Start Time Updated Successfully!", "success");
                                // $scope.isTimerStarted = true;
                                $scope.getData();
                                $scope.auctionItem.timeLeft = req.timeLeft;
                                var start = $scope.auctionItem.startTime.split('+')[0].split('(')[1];
                                auctionsService.getdate()
                                    .then(function (response) {
                                        var curDate = new Date(parseInt(response.substr(6)));
                                        //var curDate = new Date();
                                        var myEpoch = curDate.getTime();
                                        if (($scope.auctionItem.customerID == userService.getUserId() || $scope.auctionItem.superUserID == userService.getUserId()) && start > myEpoch) {
                                            $scope.startBtns = true;
                                            $scope.customerBtns = false;
                                        } else {

                                            $scope.startBtns = false;
                                            $scope.disableButtons();
                                            $scope.customerBtns = true;
                                        }

                                        if ($scope.auctionItem.timeLeft == null || $scope.auctionItem.timeLeft < 0) {
                                            $scope.showTimer = false;
                                        } else {

                                            $scope.showTimer = true;
                                        }
                                    })


                                //$log.info(req);
                            })
                        }
                    })




            } else {
                alert("Please enter the date and time to update Start Time to.");
            }
        }

        $scope.makeaBid = function () {
            var bidPrice = $("#quotationamount").val();
            //$log.info($scope.auctionItem.minPrice);
            //$log.info(bidPrice);

            if (bidPrice == "") {
                $scope.bidPriceEmpty = true;
                $scope.bidPriceValidation = false;
                return false;
            } else if (!isNaN($scope.auctionItem.minPrice) && $scope.auctionItem.minPrice > 0 && bidPrice >= $scope.auctionItem.minPrice && ($scope.auctionItem.status != 'UNCONFIRMED' && $scope.auctionItem.status != 'NOTSTARTED')) {
                $scope.bidPriceValidation = true;
                swal("Error", "Entered price is not valid. Please enter valid price", "error");
                $scope.bidPriceEmpty = false;
                return false;
            } else if (!isNaN($scope.auctionItem.minPrice) && $scope.auctionItem.minPrice > 0 && bidPrice >= ($scope.auctionItem.minPrice - $scope.auctionItem.minBidAmount) && ($scope.auctionItem.status != 'UNCONFIRMED' && $scope.auctionItem.status != 'NOTSTARTED')) {
                $scope.bidPriceValidation = true;
                swal("Error", "Your bid price should be at least " + $scope.auctionItem.minBidAmount + " lower than your previous bid.", "error");
                return false;
            } else {
                $scope.bidPriceValidation = false;
                $scope.bidPriceEmpty = false;
            }
            if (($scope.bidAttachementName == "" || $scope.bidAttachement.length == 0) && $scope.quotationStatus == false) {
                $scope.bidAttachementValidation = true;
                return false;
            } else {
                $scope.bidAttachementValidation = false;
            }
            //return false;
            //$scope.auctionItem.minPrice
            var params = {};
            params.reqID = parseInt(id);
            params.sessionID = userService.getUserToken();
            params.userID = parseInt(userService.getUserId());
            params.price = parseFloat(bidPrice);
            params.quotation = $scope.bidAttachement;
            params.quotationName = $scope.bidAttachementName;
            params.tax = $scope.vendorTaxes;
            //$log.info(params);
            //var parties = params.reqID + "$" + params.userID + "$" + params.price + "$" + params.quotation + "$" + params.quotationName + "$" + params.sessionID;
            //requirementHub.invoke('MakeBid', params, function (req) {
            auctionsService.makeabid(params).then(function (req) {
                if (req.errorMessage == '') {
                    swal("Thanks !", "Your bidding process has been successfully updated", "success");
                    $("#quotationamount").val("");
                    //$(".removeattachedquotes").trigger('click');
                    $scope.quotationStatus = true;
                    $scope.auctionStarted = false;
                    $scope.getData();
                } else {
                    swal("Error!", req.errorMessage, "error");
                }
            });

            // auctionsService.makeabid(params).then(function(response){
            //     $log.info(response);
            //     if(response.errorMessage == ''){
            //         swal("Thanks !","Your bidding process has been successfully updated", "success"); 
            //          $("#makebidvalue").val("");
            //          $(".removeattachedquotes").trigger('click');
            //     } else {
            //         swal("Error!",response.errorMessage,"error");
            //     }
            //     $scope.getData();   
            // });
        }

        $scope.$on('timer-tick', function (event, args) {
            $scope.countdownVal = event.targetScope.countdown;
            if (event.targetScope.countdown < 61 && !$scope.disableStopBids && !$scope.disablereduceTime) {
                $timeout($scope.disableButtons(), 1000);
            }
            if (event.targetScope.countdown > 60) {
                $timeout($scope.enableButtons(), 1000);
            }

            if (event.targetScope.countdown == 0 && $scope.auctionStarted && ($scope.auctionItem.status == "CLOSED" || $scope.auctionItem.status == "STARTED")) {
                $scope.auctionItem.minPrice = $scope.auctionItem.auctionVendors[0].runningPrice;
                if (($scope.auctionItem.customerID != userService.getUserId() && $scope.auctionItem.superUserID != userService.getUserId()) && $scope.vendorRank == 1) {
                    swal("Negotiation Completed!", "Congratulations! you are the least bidder for this requirement. Your price is : " + $scope.auctionItem.minPrice + "\n Customer would be reaching out you shortly. All the best!", "success");
                } else if (($scope.auctionItem.customerID != userService.getUserId() && $scope.auctionItem.superUserID != userService.getUserId()) && $scope.vendorRank != 1) {
                    swal("Negotiation Completed!", "Bidding Completed.\n Thank You for your interest on this requirement. You ranked " + $scope.vendorRank + " in this requirement. Thank you for your participation.", "success");
                } else if (($scope.auctionItem.customerID == userService.getUserId() || $scope.auctionItem.superUserID == userService.getUserId())) {
                    var params = {};
                    params.reqID = id;
                    params.sessionID = userService.getUserToken();
                    params.userID = userService.getUserId();
                    params.newTicks = 0;
                    // auctionsService.updatebidtime(params);
                    // swal("Done!", "Auction time reduced to oneminute.", "success");
                    var parties = params.reqID + "$" + params.userID + "$" + params.newTicks + "$" + params.sessionID;
                    requirementHub.invoke('UpdateTime', parties, function (req) {
                        $scope.$broadcast('timer-set-countdown-seconds', 0);
                        $scope.disableButtons();
                        swal("Negotiation Completed!", "Congratulations! you procurement process is now completed. " + $scope.toprankerName + " is the least bider with the value " + $scope.auctionItem.minPrice + " \n Your savings through PRM :" + ($scope.vendorInitialPrice - $scope.auctionItem.minPrice), "success");
                    });
                    requirementHub.invoke('completeNegotiation')
                        .then(function (response) {

                        })

                }
                $scope.getData();
            }
            if (event.targetScope.countdown <= 120) {
                $scope.timerStyle = { 'color': '#f00' };
            }
            if (event.targetScope.countdown > 120) {
                $scope.timerStyle = { 'color': '#000' };
            }
            if (event.targetScope.countdown == 0) {
                $scope.showTimer = false;
                $scope.getData();
            }
            if (event.targetScope.countdown == 1) {
                //$scope.getData();
            }
        });

        $scope.stopBids = function () {
            swal({
                title: "Are you sure?",
                text: "The Negotiation will be stopped after one minute.",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#F44336",
                confirmButtonText: "Yes, Stop Bids!",
                closeOnConfirm: true
            }, function () {
                var params = {};
                params.reqID = id;
                params.sessionID = userService.getUserToken();
                params.userID = userService.getUserId();
                params.newTicks = 60;
                // auctionsService.updatebidtime(params);
                // swal("Done!", "Auction time reduced to oneminute.", "success");
                var parties = params.reqID + "$" + params.userID + "$" + params.newTicks + "$" + params.sessionID;
                requirementHub.invoke('UpdateTime', parties, function (req) {
                    $scope.$broadcast('timer-set-countdown-seconds', 60);
                    $scope.disableButtons();
                    swal("Done!", "Negotiation time reduced to one minute.", "success");
                });
            });
        };

        $scope.disableButtons = function () {
            $scope.buttonsDisabled = true;
        }

        $scope.enableButtons = function () {
            $scope.buttonsDisabled = false;
        }

        $scope.editRequirement = function () {
            $log.info('in edit' + $stateParams.Id);
            $state.go('form.addnewrequirement', { 'Id': $stateParams.Id });
        }

        $scope.generatePOforUser = function () {
            $state.go('generate-po', { 'Id': $stateParams.Id });
        }



        $scope.deleteRequirement = function () {
            swal({
                title: "Are you sure?",
                text: "The Negotiation will be deleted and an email will be sent out to all vendors involved.",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#F44336",
                confirmButtonText: "Yes, I am sure",
                closeOnConfirm: true
            }, function () {
                var params = {};
                params.reqID = id;
                params.sessionID = userService.getUserToken();
                params.userID = userService.getUserId();
                // auctionsService.updatebidtime(params);
                // swal("Done!", "Auction time reduced to oneminute.", "success");
                var parties = params.reqID + "$" + params.userID + "$" + params.sessionID + "$" + $scope.reason;
                requirementHub.invoke('DeleteRequirement', parties, function (req) {
                    $scope.$broadcast('timer-set-countdown-seconds', 60);
                    $scope.disableButtons();
                    swal("Done!", "Requirement has been cancelled", "success");
                });
            });
        }

        $scope.vendorsFromPRM = 1;
        $scope.vendorsFromSelf = 2;

        // $scope.resetTimer = function () {
        //     $scope.auctionItem.TimeLeft.seconds = 1800;
        // }

    });prmApp
    //=================================================
    // LOGIN
    //=================================================

    .controller('loginCtrl', function ($timeout, $state, $scope, growlService, userService, authService, auctionsService, $http, domain, $rootScope, fileReader, ngDialog, $filter) {

        //Status
        $scope.login = 1;
        $scope.register = 0;
        $scope.otp = 0;
        $scope.otpvalue = 0;
        $scope.verification = 0;
        $scope.verificationObj = {};
        $scope.otpobj = {};
        $scope.register_vendor = 0;
        $scope.forgot = 0;
        $scope.forgotpassword = {};
        $scope.loggedIn = userService.isLoggedIn();
        $scope.userError = {};
        $scope.user = {};
        $scope.checkEmailUniqueResult = false;
        $scope.checkPhoneUniqueResult = false;
        $scope.checkCompanyUniqueResult = false;
        $scope.checkPANUniqueResult = false;
        $scope.checkTINUniqueResult = false;
        $scope.checkSTNUniqueResult = false;
        $scope.vendorregisterobj = $scope.registerobj = {};
        $scope.otpvalueValidation = false;
        $scope.otpvalueValidationEmpty = false;
        $scope.otpvalueValidationError = false;
        $scope.categories = [];
        $scope.subcategories = [];
        $scope.categoriesdata = [];
        $scope.selectedSubcategories = [];
        $scope.selectedCurrency = {};
        $scope.selectedTimezone = {};
        $scope.currencies = [];
        $scope.timezones = [];

        $scope.isMobile = (typeof window.orientation !== 'undefined' ? true : false);

        $scope.registrationbtn = function () {
            $scope.register = 1;
            $scope.registerobj = {};
            $scope.otp = $scope.forgot = $scope.register_vendor = $scope.login = 0;
            $scope.checkEmailUniqueResult = false;
            $scope.checkPhoneUniqueResult = false;
            $scope.checkCompanyUniqueResult = false;
            $scope.checkPANUniqueResult = false;
            $scope.checkTINUniqueResult = false;
            $scope.checkSTNUniqueResult = false;
        };
        $scope.loginbtn = function () {
            $scope.login = 1;
            $scope.user = {};
            $scope.otp = $scope.forgot = $scope.register_vendor = $scope.register = 0;
        };
        $scope.forgotbtn = function () {
            $scope.otp = $scope.login = $scope.register_vendor = $scope.register = 0;
            $scope.forgot = 1;
        };

        $scope.sendOTPagain = function () {
            userService.resendotp(userService.getUserId());
        };
        $scope.vendorregistrationbtn = function () {
            $scope.otp = $scope.login = $scope.forgot = $scope.register = 0;
            $scope.vendorregisterobj = {};
            $scope.register_vendor = 1;
            $scope.checkEmailUniqueResult = false;
            $scope.checkPhoneUniqueResult = false;
            $scope.checkCompanyUniqueResult = false;
            $scope.checkPANUniqueResult = false;
            $scope.checkTINUniqueResult = false;
            $scope.checkSTNUniqueResult = false;
        };

        $scope.PhoneValidate = function () {
            //console.log("siva  1111111---->"); 
            var phoneno = /^\d{10}$/;
            var input = $scope.registerobj.phoneNum;
            if (input = phoneno) {
                //console.log("siva  2222222---->");
                return true
                //console.log("siva  2222222---->");

            }

        };

        function validate(evt) {
            var theEvent = evt || window.event;
            var key = theEvent.keyCode || theEvent.which;
            key = String.fromCharCode(key);
            var regex = /[0-9]|\./;
            if (!regex.test(key)) {
                theEvent.returnValue = false;
                if (theEvent.preventDefault) theEvent.preventDefault();
            }
        }


        $scope.PhoneValidate1 = function () {
            if ($scope.vendorregisterobj.phoneNum != "" && isNaN($scope.vendorregisterobj.phoneNum)) {
                return false;
            }
        };


        $scope.EmailValidate = function () {
            var re = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/
            var result = re.test($scope.registerobj.email);
            if (!result) {
                $scope.showMessage = true;
                $scope.msg = $scope.getErrMsg("Please enter a valid Email Address");
            } else {
                $scope.showMessage = false;
                $scope.msg = '';
            }
        }


        $scope.EmailValidateVendor = function () {
            var re = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/
            var result = re.test($scope.vendorregisterobj.email);
            if (!result) {
                $scope.showMessage = true;
                $scope.msg = $scope.getErrMsg("Please enter a valid Email Address");
            } else {
                $scope.showMessage = false;
                $scope.msg = '';
            }
        }


        $scope.userregistration = function () {
            var re = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/
            var result = re.test($scope.registerobj.email);
            if (isNaN($scope.registerobj.phoneNum)) {
                $scope.showMessage = true;
                $scope.msg = $scope.getErrMsg("Please enter a valid Phone Number");
            } else if (!result) {
                $scope.showMessage = true;
                $scope.msg = $scope.getErrMsg("Please enter a valid Email Address");
            } else {
                $scope.showMessage = false;
                $scope.msg = '';
            }
            if (!$scope.showMessage) {
                $scope.registerobj.username = $scope.registerobj.email;

                var sCurrency = $("#currency").val();
                var sCurrency = $("#currency").val();

                $scope.registerobj.currency = $scope.selectedCurrency.key;
                $scope.registerobj.timeZone = $scope.selectedTimezone.key;
                userService.userregistration($scope.registerobj).then(function (error) {
                    $scope.loggedIn = userService.isLoggedIn();
                    if (error.errorMessage != "") {
                        $scope.showMessage = true;
                        $scope.msg = $scope.getErrMsg(error);
                    } else {
                        $scope.otp = 1;
                        $scope.forgot = $scope.login = $scope.register_vendor = $scope.register = 0;
                    }
                });
            }
        };

        $scope.getCategories = function () {
            auctionsService.getCategories()
                .then(function (response) {
                    $scope.categories = _.uniq(_.map(response, 'category'));
                    //$scope.subcategories = response;
                    $scope.categoriesdata = response;

                })
        }



        $scope.getKeyValuePairs = function (parameter) {

            auctionsService.getKeyValuePairs(parameter)
                .then(function (response) {
                    if (parameter == "CURRENCY") {
                        $scope.currencies = response;

                    } else if (parameter == "TIMEZONES") {
                        $scope.timezones = response;

                    }

                    $scope.selectedCurrency = $filter('filter')($scope.currencies, { value: response.currency });
                    $scope.selectedCurrency = $scope.selectedCurrency[0];
                    $scope.selectedTimezone = $filter('filter')($scope.timezones, { value: response.timeZone });
                    $scope.selectedTimezone = $scope.selectedTimezone[0];
                })

        }

        $scope.getKeyValuePairs('CURRENCY');
        $scope.getKeyValuePairs('TIMEZONES');

        $scope.loadSubCategories = function () {
            $scope.subcategories = _.filter($scope.categoriesdata, { category: $scope.vendorregisterobj.category });
            /*$scope.subcategories = _.map($scope.subcategories, 'subcategory');*/
        }

        $scope.getCategories();

        $scope.docsVerification = function () {
            var params = { 'userID': userService.getUserId(), 'files': $scope.CredentialUpload, 'sessionID': userService.getUserToken() };
            $http({
                method: 'POST',
                url: domain + 'updatecredentials',
                encodeURI: true,
                headers: { 'Content-Type': 'application/json' },
                data: params
            }).then(function (response) {
                //console.log(response);
                if (response && response.data && response.data.errorMessage == "") {
                    $state.go('pages.profile.profile-about');
                    growlService.growl('Welcome to PRM360! Your credentials are being verified. Our associates will contact you as soon as it is done.', 'inverse');
                } else {
                    //console.log(response.data[0].errorMessage);
                }
            }, function (result) {
                //console.log("there is no current auctions");
            });
        }

        $scope.verifyOTP = function () {
            $scope.otpvalue = $scope.otpobj.otp;
            $scope.otpvalue.phone = $scope.registerobj.phoneNum;
            if ($scope.otpvalue == "") {
                $scope.otpvalueValidation = true;
                $scope.otpvalueValidationEmpty = true;
            } else {
                $scope.otpvalueValidationEmpty = false;
                $scope.otpvalueValidation = false;
            }
            if (isNaN($scope.otpvalue)) {
                $scope.otpvalueValidationError = true;
                $scope.otpvalueValidation = true;
            } else {
                $scope.otpvalueValidationError = false;
                $scope.otpvalueValidation = false;
            }
            if (!$scope.otpvalueValidation) {
                userService.verifyOTP($scope.otpvalue)
                    .then(function (response) {
                        if (response.errorMessage == "") {
                            if (response.userInfo.isOTPVerified == 1) {
                                $scope.isOTPVerified = 1;

                                swal("Done!", "Mobile OTP Verified successfully.", "success");
                            } else {
                                $scope.isOTPVerified = 0;
                                swal("Warning", "Please enter valid OTP", "warning");
                            }
                        } else {
                            swal("Warning", response.errorMessage, "warning");
                        }
                    });
            }
        };

        $scope.getFile1 = function (id, doctype, ext) {
            $scope.progress = 0;
            $scope.file = $("#" + id)[0].files[0];
            $scope.docType = doctype + "." + ext;
            fileReader.readAsDataUrl($scope.file, $scope)
                .then(function (result) {
                    var bytearray = new Uint8Array(result);
                    var fileobj = {};
                    fileobj.fileStream = $.makeArray(bytearray);
                    fileobj.fileType = $scope.docType;
                    fileobj.isVerified = 0;
                    //$scope.verificationObj.attachmentName=$scope.file.name;
                    $scope.CredentialUpload.push(fileobj);
                });
            ////console.log($scope.CredentialUpload);
        };

        $scope.ischangePhoneNumber = 0;

        $scope.changePhoneNumber = function () {
            $scope.newphonenumber = $("#newphonenumber").val();
            //console.log($scope.newphonenumber);
            if ($scope.newphonenumber == "") {
                $scope.newphonenumber_errors = true;
                $scope.newphonenumber_required_error = true;
                return false;
            } else {
                $scope.newphonenumber_errors = false;
                $scope.newphonenumber_required_error = false;
            }
            if (isNaN($scope.newphonenumber)) {
                $scope.newphonenumber_errors = true;
                $scope.newphonenumber_validation_error = true;
                return false;
            } else {
                $scope.newphonenumber_errors = false;
                $scope.newphonenumber_validation_error = false;
            }
            if (!$scope.newphonenumber_errors) {
                var userinfo = userService.getUserObj();
                userinfo.subcategories = [];
                userinfo.phoneNum = $scope.newphonenumber;
                userinfo.sessionID = userService.getUserToken();
                userinfo.emailAddress = userinfo.email;
                userinfo.aboutUs = "";
                userinfo.companyName = "";
                userinfo.logoFile = { "fileName": '', 'fileStream': "" };
                userinfo.ischangePhoneNumber = 1;
                userService.updateUser(userinfo)
                    .then(function (response) {
                        if (response == '') {
                            $scope.otp = 0;
                            $scope.login = 1;
                        }
                    });
                $scope.changepasswordstatus = false;
            }
        };

        $scope.vendorregistration = function () {
            if (isNaN($scope.vendorregisterobj.phoneNum)) {
                $scope.showMessage = true;
                $scope.msg = $scope.getErrMsg("Please enter a valid Phone Number");
            } else {
                $scope.showMessage = false;
                $scope.msg = '';
            }
            $scope.vendorregisterobj.username = $scope.vendorregisterobj.email;
            userService.vendorregistration($scope.vendorregisterobj).then(function (error) {
                $scope.loggedIn = userService.isLoggedIn();
                /*if (error) {
                    $scope.showMessage = true;
                    $scope.msg = $scope.getErrMsg(error);
                }else{
                   $scope.otp =1;  
                   $scope.forgot = $scope.login = $scope.register_vendor = $scope.register = 0;                     
                }*/
                if (error.errorMessage != "") {
                    $scope.showMessage = true;
                    $scope.msg = $scope.getErrMsg(error);
                } else {
                    $scope.otp = 1;
                    $scope.forgot = $scope.login = $scope.register_vendor = $scope.register = 0;
                }
            });
        };

        $scope.vendorregisterobj.panno = '';
        $scope.vendorregisterobj.vatno = '';
        $scope.vendorregisterobj.taxno = '';

        $scope.RegisterVendor = function () {
            //console.log($scope.vendorregisterobj);
            if (isNaN($scope.vendorregisterobj.phoneNum)) {
                $scope.showMessage = true;
                $scope.msg = $scope.getErrMsg("Please enter a valid Phone Number");
            } else {
                $scope.showMessage = false;
                $scope.msg = '';
            }
            if (!$scope.showMessage) {
                $scope.vendorregisterobj.username = $scope.vendorregisterobj.email;
                userService.vendorregistration($scope.vendorregisterobj).then(function (error) {
                    $scope.loggedIn = userService.isLoggedIn();
                    if (error.errorMessage != "") {
                        $scope.showMessage = true;
                        $scope.msg = $scope.getErrMsg(error);
                    } else {
                        $scope.otp = 1;
                        $scope.forgot = $scope.login = $scope.register_vendor = $scope.register = 0;
                    }
                });
            }
        }

        $scope.sub = {
            selectedSubcategories: []
        }

        $scope.vendorregisterobj.panno = '';
        $scope.vendorregisterobj.vatno = '';
        $scope.vendorregisterobj.taxno = '';

        $scope.RegisterVendor1 = function () {
            //console.log("select sub categories" ,$scope.sub.selectedSubcategories);
            //console.log($scope.vendorregisterobj);
            if (isNaN($scope.vendorregisterobj.phoneNum)) {
                $scope.showMessage = true;
                $scope.msg = $scope.getErrMsg("Please enter a valid Phone Number");
            } else {
                $scope.showMessage = false;
                $scope.msg = '';
            }
            if (!$scope.showMessage) {
                $scope.vendorregisterobj.username = $scope.vendorregisterobj.email;

                $scope.vendorregisterobj.currency = $scope.vendorregisterobj.currency.key;
                $scope.vendorregisterobj.timeZone = $scope.vendorregisterobj.timeZone.key;

                //console.log($scope.selectedCurrency.key + "Currency Key");
                //console.log($scope.selectedTimezone.key + "TimeZone Key");
                $scope.vendorregisterobj.subcategories = _.map($scope.sub.selectedSubcategories, 'id');
                if (!$scope.vendorregisterobj.subcategories) {
                    $scope.vendorregisterobj.subcategories = [];
                }
                if ($scope.vendorregisterobj.vatno == null) {
                    $scope.vendorregisterobj.vatno = '';
                }
                userService.vendorregistration1($scope.vendorregisterobj).then(function (error) {
                    $scope.loggedIn = userService.isLoggedIn();
                    if (error.errorMessage != "") {
                        $scope.showMessage = true;
                        $scope.msg = $scope.getErrMsg(error);
                    } else {
                        $scope.otp = 1;
                        $scope.forgot = $scope.login = $scope.register_vendor = $scope.register = 0;
                    }
                });
            }
        }



        $scope.checkUserUniqueResult = function (idtype, inputvalue) {
            if (inputvalue == "" || inputvalue == undefined) {
                return false;
            }
            $scope.checkPhoneUniqueResult = false;
            $scope.checkEmailUniqueResult = false;
            $scope.checkCompanyUniqueResult = false;
            $scope.checkPANUniqueResult = false;
            $scope.checkTINUniqueResult = false;
            $scope.checkSTNUniqueResult = false;
            userService.checkUserUniqueResult(inputvalue, idtype).then(function (response) {
                if (idtype == "PHONE") {
                    $scope.checkPhoneUniqueResult = !response;
                } else if (idtype == "EMAIL") {
                    $scope.checkEmailUniqueResult = !response;
                } else if (idtype == "COMPANY") {
                    $scope.checkCompanyUniqueResult = !response;
                }
                else if (idtype == "PAN") {
                    $scope.checkPANUniqueResult = !response;
                }
                else if (idtype == "TIN") {
                    $scope.checkTINUniqueResult = !response;
                }
                else if (idtype == "STN") {
                    $scope.checkSTNUniqueResult = !response;
                }
            });
        };
        $scope.forgotpasswordfunction = function () {
            userService.forgotpassword($scope.forgotpassword)
                .then(function (response) {
                    if (response.data.errorMessage == "") {
                        $scope.forgotpassword = {};
                        swal("Done!", "Password reset link sent to your registerd Email and Mobile Number.", "success");
                        $scope.login = 1;
                        $scope.user = {};
                        $scope.forgot = $scope.register_vendor = $scope.register = 0;
                    } else {
                        swal("Warning", "Please check the Email/Phone you have entered.", "warning");
                    }
                });
        };



        $scope.clickToOpen = function () {
            ngDialog.open({ template: 'login/termsConditions.html', width: 1000, height: 500 });
        };


        $scope.closeMsg = function () {
            $scope.showMessage = false;
        }


        $scope.loginSubmit = function () {
            userService.login($scope.user).then(function (error) {
                $scope.loggedIn = userService.isLoggedIn();
                if (error.errorMessage != "") {
                    $scope.showMessage = true;
                    $scope.msg = $scope.getErrMsg(error);
                } else if (error.userInfo.credentialsVerified == 0 || error.userInfo.isOTPVerified == 0) {
                    $state.go('pages.profile.profile-about');
                    //$scope.otp =1;  
                    //$scope.verification =$scope.forgot = $scope.login = $scope.register_vendor = $scope.register = 0; 
                }
                //else if(error.userInfo.credentialsVerified==0){
                //     $scope.verification =1;  
                //     $scope.otp =$scope.forgot = $scope.login = $scope.register_vendor = $scope.register = 0; 
                // }
            });
        };
    });prmApp
    //=================================================
    // LOGIN
    //=================================================

    .controller('loginCtrl', function ($timeout, $state, $scope, growlService, userService, auctionsService, $http, domain, $rootScope, fileReader) {

        //Status
        $scope.login = 1;
        $scope.register = 0;
        $scope.otp = 0;
        $scope.otpvalue = 0;
        $scope.verification = 0;
        $scope.verificationObj = {};
        $scope.otpobj = {};
        $scope.register_vendor = 0;
        $scope.forgot = 0;
        $scope.forgotpassword = {};
        $scope.loggedIn = userService.isLoggedIn();
        $scope.userError = {};
        $scope.user = {};
        $scope.checkEmailUniqueResult = false;
        $scope.checkPhoneUniqueResult = false;
        $scope.vendorregisterobj = $scope.registerobj = {};
        $scope.otpvalueValidation = false;
        $scope.otpvalueValidationEmpty = false;
        $scope.otpvalueValidationError = false;

        $scope.registrationbtn = function () {
            $scope.register = 1;
            $scope.registerobj = {};
            $scope.otp = $scope.forgot = $scope.register_vendor = $scope.login = 0;
            $scope.checkEmailUniqueResult = false;
            $scope.checkPhoneUniqueResult = false;
        };
        $scope.loginbtn = function () {
            $scope.login = 1;
            $scope.user = {};
            $scope.otp = $scope.forgot = $scope.register_vendor = $scope.register = 0;
        };
        $scope.forgotbtn = function () {
            $scope.otp = $scope.login = $scope.register_vendor = $scope.register = 0;
            $scope.forgot = 1;
        };

        $scope.sendOTPagain = function () {
            userService.resendotp(userService.getUserId());
        };
        $scope.vendorregistrationbtn = function () {
            $scope.otp = $scope.login = $scope.forgot = $scope.register = 0;
            $scope.vendorregisterobj = {};
            $scope.register_vendor = 1;
            $scope.checkEmailUniqueResult = false;
            $scope.checkPhoneUniqueResult = false;
        };

        $scope.PhoneValidate = function () {
            //console.log("siva  1111111---->"); 
            var phoneno = /^\d{10}$/;
            var input = $scope.registerobj.phoneNum;
            if (input = phoneno) {
                //console.log("siva  2222222---->");
                return true
                //console.log("siva  2222222---->");

            }

        };

        function validate(evt) {
            var theEvent = evt || window.event;
            var key = theEvent.keyCode || theEvent.which;
            key = String.fromCharCode(key);
            var regex = /[0-9]|\./;
            if (!regex.test(key)) {
                theEvent.returnValue = false;
                if (theEvent.preventDefault) theEvent.preventDefault();
            }
        }


        $scope.PhoneValidate1 = function () {
            if ($scope.vendorregisterobj.phoneNum != "" && isNaN($scope.vendorregisterobj.phoneNum)) {
                return false;
            }
        };


        $scope.EmailValidate = function () {
            var re = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/
            var result = re.test($scope.registerobj.email);
            if (!result) {
                $scope.showMessage = true;
                $scope.msg = $scope.getErrMsg("Please enter a valid Email Address");
            } else {
                $scope.showMessage = false;
                $scope.msg = '';
            }
        }


        $scope.EmailValidateVendor = function () {
            var re = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/
            var result = re.test($scope.vendorregisterobj.email);
            if (!result) {
                $scope.showMessage = true;
                $scope.msg = $scope.getErrMsg("Please enter a valid Email Address");
            } else {
                $scope.showMessage = false;
                $scope.msg = '';
            }
        }


        $scope.userregistration = function () {
            var re = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/
            var result = re.test($scope.registerobj.email);
            if (isNaN($scope.registerobj.phoneNum)) {
                $scope.showMessage = true;
                $scope.msg = $scope.getErrMsg("Please enter a valid Phone Number");
            } else if (!result) {
                $scope.showMessage = true;
                $scope.msg = $scope.getErrMsg("Please enter a valid Email Address");
            } else {
                $scope.showMessage = false;
                $scope.msg = '';
            }
            if (!$scope.showMessage) {
                $scope.registerobj.username = $scope.registerobj.email;
                userService.userregistration($scope.registerobj).then(function (error) {
                    $scope.loggedIn = userService.isLoggedIn();
                    if (error.errorMessage != "") {
                        $scope.showMessage = true;
                        $scope.msg = $scope.getErrMsg(error);
                    } else {
                        $scope.otp = 1;
                        $scope.forgot = $scope.login = $scope.register_vendor = $scope.register = 0;
                    }
                });
            }
        };

        $scope.docsVerification = function () {
            var params = { 'userID': userService.getUserId(), 'files': $scope.CredentialUpload, 'sessionID': userService.getUserToken() };
            $http({
                method: 'POST',
                url: domain + 'updatecredentials',
                encodeURI: true,
                headers: { 'Content-Type': 'application/json' },
                data: params
            }).then(function (response) {
                //console.log(response);
                if (response && response.data && response.data.errorMessage == "") {
                    $state.go('pages.profile.profile-about');
                    growlService.growl('Welcome to PRM360! Your credentials are being verified. Our associates will contact you as soon as it is done.', 'inverse');
                } else {
                    //console.log(response.data[0].errorMessage);
                }
            }, function (result) {
                //console.log("there is no current auctions");
            });
        }

        $scope.verifyOTP = function () {
            $scope.otpvalue = $scope.otpobj.otp;
            $scope.otpvalue.phone = $scope.registerobj.phoneNum;
            if ($scope.otpvalue == "") {
                $scope.otpvalueValidation = true;
                $scope.otpvalueValidationEmpty = true;
            } else {
                $scope.otpvalueValidationEmpty = false;
                $scope.otpvalueValidation = false;
            }
            if (isNaN($scope.otpvalue)) {
                $scope.otpvalueValidationError = true;
                $scope.otpvalueValidation = true;
            } else {
                $scope.otpvalueValidationError = false;
                $scope.otpvalueValidation = false;
            }
            if (!$scope.otpvalueValidation) {
                userService.verifyOTP($scope.otpvalue)
                    .then(function (response) {
                        if (response.errorMessage == "") {
                            if (response.userInfo.isOTPVerified == 1) {
                                $scope.isOTPVerified = 1;

                                swal("Done!", "Mobile OTP Verified successfully.", "success");
                            } else {
                                $scope.isOTPVerified = 0;
                                swal("Warning", "Please enter valid OTP", "warning");
                            }
                        } else {
                            swal("Warning", response.errorMessage, "warning");
                        }
                    });
            }
        };

        $scope.getFile1 = function (id, doctype, ext) {
            $scope.progress = 0;
            $scope.file = $("#" + id)[0].files[0];
            $scope.docType = doctype + "." + ext;
            fileReader.readAsDataUrl($scope.file, $scope)
                .then(function (result) {
                    var bytearray = new Uint8Array(result);
                    var fileobj = {};
                    fileobj.fileStream = $.makeArray(bytearray);
                    fileobj.fileType = $scope.docType;
                    fileobj.isVerified = 0;
                    //$scope.verificationObj.attachmentName=$scope.file.name;
                    $scope.CredentialUpload.push(fileobj);
                });
            ////console.log($scope.CredentialUpload);
        };

        $scope.changePhoneNumber = function () {
            $scope.newphonenumber = $("#newphonenumber").val();
            //console.log($scope.newphonenumber);
            if ($scope.newphonenumber == "") {
                $scope.newphonenumber_errors = true;
                $scope.newphonenumber_required_error = true;
                return false;
            } else {
                $scope.newphonenumber_errors = false;
                $scope.newphonenumber_required_error = false;
            }
            if (isNaN($scope.newphonenumber)) {
                $scope.newphonenumber_errors = true;
                $scope.newphonenumber_validation_error = true;
                return false;
            } else {
                $scope.newphonenumber_errors = false;
                $scope.newphonenumber_validation_error = false;
            }
            if (!$scope.newphonenumber_errors) {
                var userinfo = userService.getUserObj();
                userinfo.phoneNum = $scope.newphonenumber;
                userinfo.sessionID = userService.getUserToken();
                userinfo.emailAddress = userinfo.email;
                userinfo.aboutUs = "";
                userService.updateUser(userinfo);
                $scope.changepasswordstatus = false;
            }
        };

        $scope.vendorregistration = function () {
            if (isNaN($scope.vendorregisterobj.phoneNum)) {
                $scope.showMessage = true;
                $scope.msg = $scope.getErrMsg("Please enter a valid Phone Number");
            } else {
                $scope.showMessage = false;
                $scope.msg = '';
            }
            $scope.vendorregisterobj.username = $scope.vendorregisterobj.email;
            userService.vendorregistration($scope.vendorregisterobj).then(function (error) {
                $scope.loggedIn = userService.isLoggedIn();
                /*if (error) {
                    $scope.showMessage = true;
                    $scope.msg = $scope.getErrMsg(error);
                }else{
                   $scope.otp =1;  
                   $scope.forgot = $scope.login = $scope.register_vendor = $scope.register = 0;                     
                }*/
                if (error.errorMessage != "") {
                    $scope.showMessage = true;
                    $scope.msg = $scope.getErrMsg(error);
                } else {
                    $scope.otp = 1;
                    $scope.forgot = $scope.login = $scope.register_vendor = $scope.register = 0;
                }
            });
        };

        $scope.RegisterVendor = function () {
            //console.log($scope.vendorregisterobj);
            if (isNaN($scope.vendorregisterobj.phoneNum)) {
                $scope.showMessage = true;
                $scope.msg = $scope.getErrMsg("Please enter a valid Phone Number");
            } else {
                $scope.showMessage = false;
                $scope.msg = '';
            }
            if (!$scope.showMessage) {
                $scope.vendorregisterobj.username = $scope.vendorregisterobj.email;
                userService.vendorregistration1($scope.vendorregisterobj).then(function (error) {
                    $scope.loggedIn = userService.isLoggedIn();
                    if (error.errorMessage != "") {
                        $scope.showMessage = true;
                        $scope.msg = $scope.getErrMsg(error);
                    } else {
                        $scope.otp = 1;
                        $scope.forgot = $scope.login = $scope.register_vendor = $scope.register = 0;
                    }
                });
            }
        }


        $scope.RegisterVendor1 = function () {
            //console.log($scope.vendorregisterobj);
            if (isNaN($scope.vendorregisterobj.phoneNum)) {
                $scope.showMessage = true;
                $scope.msg = $scope.getErrMsg("Please enter a valid Phone Number");
            } else {
                $scope.showMessage = false;
                $scope.msg = '';
            }
            if (!$scope.showMessage) {
                $scope.vendorregisterobj.username = $scope.vendorregisterobj.email;
                userService.vendorregistration1($scope.vendorregisterobj).then(function (error) {
                    $scope.loggedIn = userService.isLoggedIn();
                    if (error.errorMessage != "") {
                        $scope.showMessage = true;
                        $scope.msg = $scope.getErrMsg(error);
                    } else {
                        $scope.otp = 1;
                        $scope.forgot = $scope.login = $scope.register_vendor = $scope.register = 0;
                    }
                });
            }
        }



        $scope.checkUserUniqueResult = function (idtype, inputvalue) {
            if (inputvalue == "" || inputvalue == undefined) {
                return false;
            }
            $scope.checkPhoneUniqueResult = false;
            $scope.checkEmailUniqueResult = false;
            userService.checkUserUniqueResult(inputvalue, idtype).then(function (response) {
                if (idtype == "PHONE") {
                    $scope.checkPhoneUniqueResult = !response;
                } else if (idtype == "EMAIL") {
                    $scope.checkEmailUniqueResult = !response;
                }
            });
        };
        $scope.forgotpasswordfunction = function () {
            userService.forgotpassword($scope.forgotpassword)
                .then(function (response) {
                    if (response.data.errorMessage != "No user found") {
                        $scope.forgotpassword = {};
                        swal("Done!", "Email sent to your registered email.", "success");
                        $scope.login = 1;
                        $scope.user = {};
                        $scope.forgot = $scope.register_vendor = $scope.register = 0;
                    } else {
                        swal("Warning", "Please check the email address you have entered.", "warning");
                    }
                });
        };


        $scope.closeMsg = function () {
            $scope.showMessage = false;
        }
        $scope.loginSubmit = function () {
            userService.login($scope.user).then(function (error) {
                $scope.loggedIn = userService.isLoggedIn();
                if (error.errorMessage != "") {
                    $scope.showMessage = true;
                    $scope.msg = $scope.getErrMsg(error);
                } else if (error.userInfo.credentialsVerified == 0 || error.userInfo.isOTPVerified == 0) {
                    $state.go('pages.profile.profile-about');
                    //$scope.otp =1;  
                    //$scope.verification =$scope.forgot = $scope.login = $scope.register_vendor = $scope.register = 0; 
                }
                //else if(error.userInfo.credentialsVerified==0){
                //     $scope.verification =1;  
                //     $scope.otp =$scope.forgot = $scope.login = $scope.register_vendor = $scope.register = 0; 
                // }
            });
        };
    });prmApp
    // =========================================================================
    // Base controller for common functions
    // =========================================================================

    .controller('prmAppCtrl', function ($timeout, $state, $scope, growlService, userService, auctionsService, $http, $rootScope, SignalRFactory) {
        $scope.loggedIn = userService.isLoggedIn();
        $scope.user = {};
        $scope.addrequirementaccess = true;

        $scope.showAddNewReq = function () {
            if (window.location.hash != "#/login" && userService.getUserType() == "CUSTOMER") {
                return true;
            } else {
                return false;
            }
        }





        $scope.showIsUserVendor = function () {
            if (window.location.hash != "#/login" && userService.getUserType() == "VENDOR") {
                return true;
            } else {
                return false;
            }
        }








        $scope.showAddButtons = function () {

        }

        $scope.registerobj = {};
        var options = {
            loop: true,
            margin: 10,
            dots: false,
            autoplay: true,
            autoplayTimeout: 3000,
            autoplayHoverPause: true,
            nav: false,
            responsive: {
                0: {
                    items: 1,
                },
                600: {
                    items: 3,
                },
                1000: {
                    items: 4
                }
            }
        }
        // $scope.formRequest = {};
        $scope.userError = {};
        $scope.msg = userService.getMessage();
        $scope.showMessage = true;
        $scope.successshowMessage = true;
        $scope.userData = userService.userData;
        if ($scope.userData.currentUser != null) {
            $scope.fullName = userService.userData.currentUser.firstName + " " + userService.userData.currentUser.lastName;
        } else {
            $scope.fullName = "";
        }

        $scope.closeMsg = function () {
            $scope.showMessage = false;
        }
        $scope.array = [1, 2, 3, 4];

        $scope.logout = function () {
            userService.logout().then(function () {
                $scope.loggedIn = userService.isLoggedIn();
            });
        }

        $scope.getErrMsg = function (message) {
            if (typeof message == 'string') {
                return message;
            } else if (typeof message == 'object') {
                var result = "";
                angular.forEach(message, function (value, key) {
                    result += value[0] + '<br />';
                });
                return result;
            }
        }
        // Detact Mobile Browser
        if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
            angular.element('html').addClass('ismobile');
        }

        // By default Sidbars are hidden in boxed layout and in wide layout only the right sidebar is hidden.
        this.sidebarToggle = {
            left: false,
            right: false
        }

        // By default template has a boxed layout
        this.layoutType = localStorage.getItem('ma-layout-status');

        // For Mainmenu Active Class
        this.$state = $state;

        //Close sidebar on click
        this.sidebarStat = function (event) {
            if (!angular.element(event.target).parent().hasClass('active')) {
                this.sidebarToggle.left = false;
            }
        }

        //Listview Search (Check listview pages)
        this.listviewSearchStat = false;

        this.lvSearch = function () {
            this.listviewSearchStat = true;
        }

        //Listview menu toggle in small screens
        this.lvMenuStat = false;

        //Blog
        this.wallCommenting = [];

        this.wallImage = false;
        this.wallVideo = false;
        this.wallLink = false;

        //Skin Switch
        this.currentSkin = 'blue';

        this.skinList = [
            'lightblue',
            'bluegray',
            'cyan',
            'teal',
            'green',
            'orange',
            'blue',
            'purple'
        ]

        this.skinSwitch = function (color) {
            this.currentSkin = color;
        };

       
        $scope.reduceTime = function (timerId, time) {
            addCDSeconds(timerId, time);
        }

        $scope.stopBid = function (item) {
            $scope.myHotAuctions[0].TimeLeft = 60;
        }
    });prmApp
    .controller('meterialDispatchmentCtrl', function ($scope, $state, $stateParams, userService, auctionsService) {
        $scope.id = $stateParams.Id;
        $scope.formRequest = {};

        //console.log("ABCD===========");

        $scope.getData = function () {
            auctionsService.getrequirementdata({ "reqid": $stateParams.Id, "sessionid": userService.getUserToken(), 'userid': userService.getUserId() })
                .then(function (response) {
                    $scope.auctionItem = response;
                    $scope.formRequest.selectedVendor = $scope.auctionItem.auctionVendors[0];

                    //console.log("ABCD===========");
                })
        }

        $scope.getData();

        $scope.postRequest = function () {
            var ts = moment($scope.formRequest.expectedDelivery, "DD-MM-yyyy HH:mm").valueOf();
            var m = moment(ts);
            var deliveryDate = new Date(m);
            var milliseconds = parseInt(deliveryDate.getTime() / 1000.0);
            $scope.formRequest.expectedDelivery = "/Date(" + milliseconds + "000+0530)/";

            /*var ts1 = moment($scope.formRequest.paymentScheduleDate, "DD-MM-yyyy HH:mm").valueOf();
            var m1 = moment(ts1);
            var deliveryDate1 = new Date(m1);
            var milliseconds1 = parseInt(deliveryDate1.getTime() / 1000.0);
            $scope.formRequest.paymentScheduleDate = "/Date(" + milliseconds1 + "000+0530)/";*/

            $scope.formRequest.sessionID = userService.getUserToken();
            $scope.formRequest.requirementID = $scope.auctionItem.requirementID;
            $scope.formRequest.userID = userService.getUserId();
            auctionsService.materialdispatch($scope.formRequest)
                .then(function (response) {
                    if (response.objectID != 0) {
                        swal({
                            title: "Done!",
                            text: "Data Saved Successfully.",
                            type: "success",
                            showCancelButton: false,
                            confirmButtonColor: "#DD6B55",
                            confirmButtonText: "Ok",
                            closeOnConfirm: true
                        },
                            function () {
                                //$state.go('view-requirement');
                                $state.go('view-requirement', { 'Id': response.objectID });
                            });
                    }
                })
        }
    });prmApp


    .controller('modalInstanceCtrlOTP', function ($scope, $uibModalInstance, userService, $rootScope) {

        //$scope.modalContent = content;
        $scope.otpvalueValidation = false;
        $scope.otpvalueValidationEmpty = false;
        $scope.otpvalueValidationError = false;
        $scope.modalotpvalue = "";
        $scope.ok = function () {
            $uibModalInstance.close();
        };
        $scope.afterOTPVerification = function () {
            $rootScope.$emit("CallProfileMethod");
        }
        $scope.verifyOTP = function () {
            if ($scope.modalotpvalue == "") {
                $scope.otpvalueValidation = true;
                $scope.otpvalueValidationEmpty = true;
            } else {
                $scope.otpvalueValidationEmpty = false;
                $scope.otpvalueValidation = false;
            }
            if (isNaN($scope.modalotpvalue)) {
                $scope.otpvalueValidationError = true;
                $scope.otpvalueValidation = true;
            } else {
                $scope.otpvalueValidationError = false;
                $scope.otpvalueValidation = false;
            }
            if (!$scope.otpvalueValidation) {
                userService.verifyOTP($scope.modalotpvalue)
                    .then(function (response) {
                        if (response.errorMessage == "") {
                            if (response.userInfo.isOTPVerified == 1) {
                                swal("Done!", "Mobile OTP Verified successfully.", "success");
                                $uibModalInstance.dismiss('cancel');
                                $scope.afterOTPVerification();
                            }
                        } else {
                            swal("Warning", response.errorMessage, "warning");
                        }
                    });
            }
        };

        $scope.sendOTPagain = function () {

            userService.resendotp(userService.getUserId());
        };


        $scope.sendEmailOTPagain = function () {

            userService.resendemailotp(userService.getUserId());
        };



        $scope.verifyEmailOTP = function () {
            if ($scope.modalotpvalue == "") {
                $scope.otpvalueValidation = true;
                $scope.otpvalueValidationEmpty = true;
            } else {
                $scope.otpvalueValidationEmpty = false;
                $scope.otpvalueValidation = false;
            }
            if (isNaN($scope.modalotpvalue)) {
                $scope.otpvalueValidationError = true;
                $scope.otpvalueValidation = true;
            } else {
                $scope.otpvalueValidationError = false;
                $scope.otpvalueValidation = false;
            }
            if (!$scope.otpvalueValidation) {
                userService.verifyEmailOTP($scope.modalotpvalue)
                    .then(function (response) {
                        if (response.errorMessage == "") {
                            if (response.userInfo.isEmailOTPVerified == 1) {
                                swal("Done!", "Email OTP Verified successfully.", "success");
                                $uibModalInstance.dismiss('cancel');
                                $scope.afterOTPVerification();
                            }
                        } else {
                            swal("Warning", response.errorMessage, "warning");
                        }
                    });
            }
        };





        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };
    });prmApp
    .controller('paymentCtrl', function ($scope, $state, $stateParams, userService, auctionsService) {
        $scope.id = $stateParams.Id;
        $scope.formRequest = {};

        //console.log("ABCD===========");

        $scope.getData = function () {
            auctionsService.getrequirementdata({ "reqid": $stateParams.Id, "sessionid": userService.getUserToken(), 'userid': userService.getUserId() })
                .then(function (response) {
                    $scope.auctionItem = response;
                    $scope.formRequest.selectedVendor = $scope.auctionItem.auctionVendors[0];

                    //console.log("ABCD===========");
                })
        }

        $scope.getData();

        $scope.postRequest = function () {
            var ts = moment($scope.formRequest.paymentDate, "DD-MM-yyyy HH:mm").valueOf();
            var m = moment(ts);
            var deliveryDate = new Date(m);
            var milliseconds = parseInt(deliveryDate.getTime() / 1000.0);
            $scope.formRequest.paymentDate = "/Date(" + milliseconds + "000+0530)/";

            /*var ts1 = moment($scope.formRequest.paymentScheduleDate, "DD-MM-yyyy HH:mm").valueOf();
            var m1 = moment(ts1);
            var deliveryDate1 = new Date(m1);
            var milliseconds1 = parseInt(deliveryDate1.getTime() / 1000.0);
            $scope.formRequest.paymentScheduleDate = "/Date(" + milliseconds1 + "000+0530)/";*/

            $scope.formRequest.sessionID = userService.getUserToken();
            $scope.formRequest.requirementID = $scope.auctionItem.requirementID;
            $scope.formRequest.userID = userService.getUserId();
            auctionsService.paymentdetails($scope.formRequest)
                .then(function (response) {
                    if (response.objectID != 0) {
                        swal({
                            title: "Done!",
                            text: "Data Saved Successfully.",
                            type: "success",
                            showCancelButton: false,
                            confirmButtonColor: "#DD6B55",
                            confirmButtonText: "Ok",
                            closeOnConfirm: true
                        },
                            function () {
                                //$state.go('view-requirement');
                                $state.go('view-requirement', { 'Id': response.objectID });
                            });
                    }
                })
        }
    });prmApp
    .controller('POCtrl', function ($scope, $state, $stateParams, userService, auctionsService, fileReader) {
        $scope.id = $stateParams.Id;
        $scope.formRequest = {};
        $scope.formRequest1 = {};


        $scope.getData = function () {
            auctionsService.getrequirementdata({ "reqid": $stateParams.Id, "sessionid": userService.getUserToken(), 'userid': userService.getUserId() })
                .then(function (response) {
                    $scope.auctionItem = response;
                    $scope.formRequest1.selectedVendor = $scope.auctionItem.selectedVendor;
                    $scope.formRequest1.deliveryAddress = $scope.auctionItem.deliveryLocation;

                    $scope.formRequest.selectedVendor = $scope.formRequest1.selectedVendor;
                    $scope.formRequest.deliveryAddress = $scope.formRequest1.deliveryAddress;

                    if ($scope.auctionItem.poLink != '') {

                        auctionsService.getpodetails({ "reqid": $stateParams.Id, "sessionid": userService.getUserToken(), 'userid': userService.getUserId() })
                            .then(function (response) {
                                $scope.formRequest = response;
                                if ($scope.formRequest.requirementID > 0) {
                                    //var date = $scope.auctionItem.deliveryTime.split('+')[0].split('(')[1];
                                    //var date1 = moment($scope.auctionItem.deliveryTime).format('DD/MM/YYYY');
                                    //console.log(date1);
                                    //var newDate = new Date(parseInt(parseInt(date)));
                                    ////$scope.formRequest.expectedDelivery = newDate.toString().replace('Z', '');
                                    //$scope.formRequest.expectedDelivery = date1;
                                } else {
                                    $scope.formRequest.expectedDelivery = '';
                                }
                                $scope.formRequest.selectedVendor = $scope.formRequest1.selectedVendor;
                                if (!$scope.formRequest.deliveryAddress || $scope.formRequest.deliveryAddress == "" || $scope.formRequest.deliveryAddress == null) {
                                    $scope.formRequest.deliveryAddress = $scope.formRequest1.deliveryAddress;
                                }

                            })
                    }
                    //console.log("ABCD===========");
                })
        }

        $scope.getData();

        this.UploadPO = 0;




        $scope.getFile1 = function (id, doctype, ext) {
            $scope.progress = 0;
            $scope.file = $("#" + id)[0].files[0];
            $scope.docType = doctype + "." + ext;
            fileReader.readAsDataUrl($scope.file, $scope)
                .then(function (result) {
                    if (id == "poFile") {
                        $scope.formRequest.poFile = { "fileName": '', 'fileStream': null };
                        var bytearray = new Uint8Array(result);
                        $scope.formRequest.poFile.fileStream = $.makeArray(bytearray);
                        $scope.formRequest.poFile.fileName = $scope.file.name;
                    }

                });
        };

        $scope.postRequest = function () {
            if (!$scope.formRequest.POID || $scope.formRequest.POID == "") {
                $scope.formRequest.POID = $scope.formRequest.requirementID + "_" + $scope.formRequest.customerID;
            }
            if (!$scope.formRequest.comments) {
                $scope.formRequest.comments = "";
            }
            var ts = moment($scope.formRequest.expectedDelivery, "DD-MM-yyyy HH:mm").valueOf();
            var m = moment(ts);
            var deliveryDate = new Date(m);
            var milliseconds = parseInt(deliveryDate.getTime() / 1000.0);
            $scope.formRequest.expectedDelivery = "/Date(" + milliseconds + "000+0530)/";

            /*var ts1 = moment($scope.formRequest.paymentScheduleDate, "DD-MM-yyyy HH:mm").valueOf();
            var m1 = moment(ts1);
            var deliveryDate1 = new Date(m1);
            var milliseconds1 = parseInt(deliveryDate1.getTime() / 1000.0); 
            $scope.formRequest.paymentScheduleDate = "/Date(" + milliseconds1 + "000+0530)/";*/

            if ($scope.formRequest.poFile == null) {
                $scope.formRequest.poFile = { "fileName": '', 'fileStream': null };
            }

            $scope.formRequest.sessionID = userService.getUserToken();
            $scope.formRequest.requirementID = $scope.auctionItem.requirementID;
            $scope.formRequest.customerID = userService.getUserId();
            $scope.formRequest.selectedVendor = $scope.auctionItem.selectedVendor;
            if (!$scope.formRequest.deliveryAddress || $scope.formRequest.deliveryAddress == "" || $scope.formRequest.deliveryAddress == null) {
                $scope.formRequest.deliveryAddress = $scope.auctionItem.deliveryLocation;
            }

            auctionsService.generatepo($scope.formRequest)
                .then(function (response) {
                    if (response.objectID != 0) {
                        swal({
                            title: "Done!",
                            text: "Purchase Order Generated Successfully.",
                            type: "success",
                            showCancelButton: false,
                            confirmButtonColor: "#DD6B55",
                            confirmButtonText: "Ok",
                            closeOnConfirm: true
                        },
                            function () {
                                //$state.go('view-requirement');
                                $state.go('view-requirement', { 'Id': response.objectID });
                            });
                    }
                })
        }
    });prmApp

    //=================================================
    // Profile
    //=================================================

    //.controller('profileCtrl', function ($scope,growlService,$http,domain,auctionsService,userService) {
    .controller('profileCtrl', function ($timeout, $uibModal, $state, $scope, growlService, userService, auctionsService, $http, domain, $rootScope, fileReader, $filter, $log) {
        $scope.isCustomer = userService.getUserType();
        $scope.sessionid = userService.getUserToken();
        //Get Profile Information from profileService Service
        $scope.userObj = {};
        userService.getUserDataNoCache()
            .then(function (response) {
                $scope.userObj = response;
            })
        $scope.newVendor = {};
        $scope.newVendor.panno = "";
        $scope.newVendor.vatNum = "";
        $scope.newVendor.serviceTaxNo = "";


        $scope.basicinfoCollapse = $scope.contactinfo = $scope.docsverification = $scope.professionalinfo = true;
        $scope.pwdmng = true;
        $scope.isnegotiationrunning = '';
        $scope.subcategories = '';
        userService.isnegotiationrunning()
            .then(function (response) {
                $scope.isnegotiationrunning = response.data.IsNegotationRunningResult;
                ////console.log($scope.isnegotiationrunning);
            })
        //User
        $scope.editPwd = 0;

        $scope.isPhoneModifies = 0;
        $scope.isEmailModifies = 0;
        $scope.totalSubcats = [];
        $scope.selectedCurrency = {};
        $scope.currencies = [];

        $scope.days = 0;
        $scope.hours = 0;
        $scope.mins = 0;

        $scope.showIsSuperUser = function () {
            if (userService.getUserObj().isSuperUser) {
                return true;
            } else {
                return false;
            }
        }

        $scope.addVendorShow = false;

        $scope.checkVendorUniqueResult = function (idtype, inputvalue) {
            if (inputvalue == "" || inputvalue == undefined) {
                return false;
            }
            /*$scope.checkVendorPhoneUniqueResult=false;
            $scope.checkVendorEmailUniqueResult=false;*/
            //$scope.checkPANUniqueResult = false;
            //$scope.checkTINUniqueResult = false;
            //$scope.checkSTNUniqueResult = false;
            userService.checkUserUniqueResult(inputvalue, idtype).then(function (response) {
                if (idtype == "PHONE") {
                    $scope.checkVendorPhoneUniqueResult = !response;
                } else if (idtype == "EMAIL") {
                    $scope.checkVendorEmailUniqueResult = !response;
                }
                else if (idtype == "PAN") {
                    $scope.checkPANUniqueResult = !response;
                }
                else if (idtype == "TIN") {
                    $scope.checkTINUniqueResult = !response;
                }
                else if (idtype == "STN") {
                    $scope.checkSTNUniqueResult = !response;
                }
            });
        };


        $scope.pwdObj = {
            username: userService.getUserObj().username,
            oldPass: '',
            newPass: ''
        };

        $scope.categories = [];

        $scope.myDepartments = [];


        $scope.getCategories = function () {



            auctionsService.getCategories(userService.getUserId())
                .then(function (response) {
                    $scope.categories = response;
                    $scope.addVendorCats = _.uniq(_.map(response, 'category'));
                })
        }



        $scope.getKeyValuePairs = function (parameter) {

            auctionsService.getKeyValuePairs(parameter)
                .then(function (response) {
                    if (parameter == "CURRENCY") {
                        $scope.currencies = response;
                    } else if (parameter == "TIMEZONES") {
                        $scope.timezones = response;

                    }

                    $scope.selectedCurrency = $filter('filter')($scope.currencies, { value: response.currency });
                    $scope.selectedCurrency = $scope.selectedCurrency[0];
                    //$scope.selectedTimezone = $filter('filter')($scope.timezones, { value: response.timeZone });
                    //$scope.selectedTimezone = $scope.selectedTimezone[0];
                })

        }

        $scope.getKeyValuePairs('CURRENCY');
        $scope.getKeyValuePairs('TIMEZONES');



        /*pagination code*/
        $scope.totalItems = 0;
        $scope.totalLeads = 0;
        $scope.currentPage = 1;
        $scope.itemsPerPage = 5;
        $scope.maxSize = 5; //Number of pager buttons to show

        $scope.setPage = function (pageNo) {
            $scope.currentPage = pageNo;
        };

        $scope.pageChanged = function () {
            ////console.log('Page changed to: ' + $scope.currentPage);
        };

        $scope.getCategories();

        $scope.userDetails = {
            achievements: "",
            assocWithOEM: false,
            clients: "",
            establishedDate: "01-01-1970",
            aboutUs: "",
            logoFile: "",
            logoURL: "",
            products: "",
            strengths: "",
            responseTime: "",
            oemCompanyName: "",
            oemKnownSince: "",
            workingHours: "",
            files: [],
            directors: "",
            address: "",
            dateshow: 0

        };

        $scope.userObj = userService.getUserObj();
        $scope.uId = userService.getUserId();

        this.updatePwd = function () {
            if ($scope.pwdObj.newPass.length < 6) {
                growlService.growl("Password should be at least 6 characters long.", "inverse");
                return false;
            } else if ($scope.pwdObj.newPass != $scope.pwdObj.confirmNewPass) {
                growlService.growl('Passwords do not match, please enter the same password in both new and Confirm Password fields', 'inverse');
                return false;
            }
            $scope.pwdObj.userID = parseInt(userService.getUserId());
            userService.updatePassword($scope.pwdObj)
                .then(function (response) {
                    if (response.errorMessage == "") {
                        $scope.pwdObj = {
                            username: userService.getUserObj().username
                        };
                        swal("Done!", 'Your password has been successfully updated.', 'success');
                    } else {
                        swal("Error!", response.errorMessage, 'error');
                    }
                })
        }

        $scope.getSubCats = function () {

        }

        $scope.getSubCats();

        $scope.callGetUserDetails = function () {
            $log.info("IN GET USER DETAILS");
            userService.getProfileDetails({ "userid": userService.getUserId(), "sessionid": userService.getUserToken() })
                .then(function (response) {
                    $scope.userStatus = "registered";
                    if (response != undefined) {
                        $scope.userDetails = response;

                        $scope.NegotiationSettings = $scope.userDetails.NegotiationSettings;
                        var duration = $scope.NegotiationSettings.negotiationDuration.split(" ", 4);
                        $scope.days = parseInt(duration[0]);
                        duration = duration[1];
                        duration = duration.split(":", 4);
                        $scope.hours = parseInt(duration[0]);
                        $scope.mins = parseInt(duration[1]);

                        $http({
                            method: 'GET',
                            url: domain + 'getcategories?userid=' + userService.getUserId() + '&sessionid=' + userService.getUserToken(),
                            encodeURI: true,
                            headers: { 'Content-Type': 'application/json' }
                        }).then(function (response) {
                            if (response && response.data) {
                                if (response.data.length > 0) {
                                    $scope.totalSubcats = $filter('filter')(response.data, { category: $scope.userDetails.category });
                                    $scope.selectedCurrency = $filter('filter')($scope.currencies, { value: response.currency });
                                    $scope.selectedCurrency = $scope.selectedCurrency[0];
                                    if ($scope.userDetails.subcategories && $scope.userDetails.subcategories.length > 0) {
                                        for (i = 0; i < $scope.userDetails.subcategories.length; i++) {
                                            for (j = 0; j < $scope.totalSubcats.length; j++) {
                                                if ($scope.userDetails.subcategories[i].id == $scope.totalSubcats[j].id) {
                                                    $scope.totalSubcats[j].ticked = true;
                                                }
                                            }
                                        }
                                    }
                                }
                            } else {
                                ////console.log(response.data[0].errorMessage);
                            }
                        }, function (result) {
                            ////console.log("there is no current auctions");
                        });
                        if ($scope.userDetails.subcategories && $scope.userDetails.subcategories.length > 0) {
                            for (i = 0; i < $scope.userDetails.subcategories.length; i++) {
                                $scope.subcategories += $scope.userDetails.subcategories[i].subcategory + ";";
                            }
                        }
                        var data = response.establishedDate;
                        var date = new Date(parseInt(data.substr(6)));
                        $scope.userDetails.establishedDate = date.getDate() + '/' + (date.getMonth() + 1) + '/' + date.getFullYear();

                        var today = new Date();
                        var todayDate = today.getDate() + '/' + (today.getMonth() + 1) + '/' + today.getFullYear();
                        $scope.userDetails.dateshow = 0;
                        if ($scope.userDetails.establishedDate == todayDate) {
                            $scope.userDetails.dateshow = 1;
                        }

                        if (response.registrationScore > 89) {

                            $scope.userStatus = "Authorised";

                        }
                    }

                    $log.info($scope.userDetails);
                });
        }



        $scope.subUsers = [];
        $scope.inactiveSubUsers = [];

        $scope.getSubUserData = function () {
            userService.getSubUsersData({ "userid": userService.getUserId(), "sessionid": userService.getUserToken() })
                .then(function (response) {
                    $scope.subUsers = $filter('filter')(response, { isValid: true });
                    $scope.inactiveSubUsers = $filter('filter')(response, { isValid: false });
                });
        }

        $scope.deleteUser = function (userid) {
            userService.deleteUser({ "userID": userid, "referringUserID": userService.getUserId() })
                .then(function (response) {
                    if (response.errorMessage != "") {
                        growlService.growl(response.errorMessage, "inverse");
                    } else {
                        growlService.growl("User deleted Successfully", "inverse");
                        $scope.getSubUserData();
                    }
                });
        }

        $scope.activateUser = function (userid) {
            userService.activateUser({ "userID": userid, "referringUserID": userService.getUserId() })
                .then(function (response) {
                    if (response.errorMessage != "") {
                        growlService.growl(response.errorMessage, "inverse");
                    } else {
                        growlService.growl("User Addes Successfully", "success");
                        $scope.getSubUserData();
                    }
                });
        }

        $scope.getSubUserData();


        this.profileSummary = "";

        var loginUserData = userService.getUserObj();
        $scope.userObj.fullName = loginUserData.firstName + " " + loginUserData.lastName;
        $scope.userObj.firstName = loginUserData.firstName;
        $scope.userObj.lastName = loginUserData.lastName;
        $scope.userObj.gender = "male";
        $scope.userObj.birthDay = "23/06/1988";
        $scope.userObj.martialStatus = "Single";
        $scope.userObj.phoneNum = loginUserData.phoneNum;
        $scope.userObj.email = loginUserData.email;
        $scope.oldPhoneNum = loginUserData.phoneNum;
        $scope.oldemail = loginUserData.email;
        /*this.aboutUs="";*/
        $scope.userObj.email = loginUserData.email;

        $scope.userObj.addressSuite = loginUserData.addressLine1 + " " + loginUserData.addressLine2 + " " + loginUserData.addressLine3;
        $scope.userObj.addressCity = loginUserData.city;
        $scope.userObj.addressCountry = loginUserData.country;
        this.userId = userService.getUserId();
        $scope.isOTPVerified = loginUserData.isOTPVerified;
        $scope.isEmailOTPVerified = loginUserData.isEmailOTPVerified;
        $rootScope.$on("CallProfileMethod", function () {
            $scope.updateUserDataFromService();
        });
        this.imagefilesonlyforlogo = false;
        this.editSummary = 0;
        this.editInfo = 0;
        this.editPic = 0;
        this.editDocVerification = 0;
        this.editContact = 0;
        this.editPro = 0;
        this.editPwd = 0;
        this.addUser = 0;
        this.addVendor = 0;
        $scope.myAuctionsLoaded = false;
        $scope.myAuctions = [];
        $scope.myActiveLeads = [];
        var date = new Date();

        $scope.getFile = function () {
            $scope.progress = 0;
            $scope.file = $("#tindoc")[0].files[0];
            fileReader.readAsDataUrl($scope.file, $scope)
                .then(function (result) {

                });
        };

        this.changepasswordstatus = this.newphonenumber_validation_error = this.newphonenumber_required_error = false;
        this.newphonenumber = "";
        $scope.CredentialUpload = [];
        $scope.uploadedCredentials = [{ "credentialID": '', "fileType": 'PAN', 'fileLink': "", 'isVerified': 0 }, { "credentialID": '', "fileType": 'TIN', 'fileLink': "", 'isVerified': 0 }, { "credentialID": '', "fileType": 'STN', 'fileLink': "", 'isVerified': 0 }];
        $scope.logoFile = { "fileName": '', 'fileStream': "" };
        this.newphonenumber_errors = false;
        $scope.otpvalue = "";

        $scope.getUserCredentials = function () {
            $http({
                method: 'GET',
                url: domain + 'getusercredentials?sessionid=' + userService.getUserToken() + "&userid=" + userService.getUserId(),
                encodeURI: true,
                headers: { 'Content-Type': 'application/json' }
            }).then(function (response) {

                $scope.CredentialsResponce = response.data;
                //var pan = $filter('filter')($scope.CredentialsResponce, 'pan');


                if (response && response.data && response.data.length > 0) {
                    if (response.data[0].errorMessage == "") {
                        var panObj = _.filter($scope.CredentialsResponce, ['fileType', 'PAN']);
                        $scope.pannumber = panObj[0].credentialID;
                        $scope.vatnumber = _.filter($scope.CredentialsResponce, ['fileType', 'TIN'])[0].credentialID;
                        $scope.taxnumber = _.filter($scope.CredentialsResponce, ['fileType', 'STN'])[0].credentialID;
                        var verifiedDocsCount = 0;
                        $.each(response.data, function (key, value) {
                            $scope.PAN = value.credentialID;
                            if (value.isVerified == 1) {
                                verifiedDocsCount++;
                            }
                        });
                        if (response.data.length == verifiedDocsCount + 1 || response.data.length == verifiedDocsCount) {
                            userService.updateVerified(1);
                        } else {
                            userService.updateVerified(0);
                        }
                        $scope.uploadedCredentials = response.data;
                        if (response.data.length > 0) {
                            this.editDocVerification = 0;
                            $scope.editDocVerification = 0;
                        }
                    }
                }
            }, function (result) {
                $log.error("error in request service");
            });
        };
        $scope.updateUserDataFromService = function (msg) {
            loginUserData = userService.getUserObj();
            $scope.oldPhoneNum = loginUserData.phoneNum;
            $scope.oldemail = loginUserData.email;
            $scope.userObj.fullName = loginUserData.firstName + " " + loginUserData.lastName;
            $scope.userObj.firstName = loginUserData.firstName;
            $scope.userObj.lastName = loginUserData.lastName;
            $scope.userObj.gender = "male";
            $scope.userObj.birthDay = "23/06/1988";
            $scope.userObj.martialStatus = "Single";
            $scope.userObj.phoneNum = loginUserData.phoneNum;
            $scope.userObj.email = loginUserData.email;
            $scope.userObj.addressSuite = loginUserData.addressLine1 + " " + loginUserData.addressLine2 + " " + loginUserData.addressLine3;
            $scope.userObj.addressCity = loginUserData.city;
            $scope.userObj.addressCountry = loginUserData.country;
            this.userId = userService.getUserId();
            $scope.isOTPVerified = loginUserData.isOTPVerified;
            $scope.isEmailOTPVerified = loginUserData.isEmailOTPVerified;
            $scope.credentialsVerified = loginUserData.credentialsVerified;
            $scope.callGetUserDetails();
            $scope.getUserCredentials();
            if ($scope.isOTPVerified && $scope.isEmailOTPVerified) {
                auctionsService.getmyAuctions({ "userid": userService.getUserId(), "sessionid": userService.getUserToken() })
                    .then(function (response) {
                        $scope.myAuctions = response;
                        if ($scope.myAuctions.length > 0) {
                            $scope.myAuctionsLoaded = true;
                            $scope.totalItems = $scope.myAuctions.length;
                        } else {
                            $scope.myAuctionsLoaded = false;
                            $scope.totalItems = 0;
                            $scope.myAuctionsMessage = "There are no auctions running right now for you.";
                        }
                    });


                auctionsService.getactiveleads({ "userid": userService.getUserId(), "sessionid": userService.getUserToken() })
                    .then(function (response) {
                        $scope.myActiveLeads = response;
                        if ($scope.myActiveLeads.length > 0) {
                            $scope.myAuctionsLoaded = true;
                            $scope.totalLeads = $scope.myActiveLeads.length;
                        } else {
                            $scope.totalLeads = 0;
                            $scope.myAuctionsLoaded = false;
                            $scope.myAuctionsMessage = "There are no auctions running right now for you.";
                        }
                    });




            }
        }

        // var intervalPromise = window.setInterval(function () {
        //     if (window.location.hash.indexOf("#/pages/profile/profile-timeline") > -1) {
        //         $scope.updateUserDataFromService();
        //     }
        // }, 100000);
        $scope.updateUserDataFromService('default call');



        //$scope.updateUserDataFromService(userService);
        this.editMode = function () {
            this.editPro = 1;

        }



        /* $scope.sendOTPagain=function(){
             userService.resendotp(userService.getUserId());
         };*/
        $scope.generatePDF = function () {
            var doc = new jsPDF();

            // We'll make our own renderer to skip this editor
            var specialElementHandlers = {
                '#editor': function (element, renderer) {
                    return true;
                }
            };

            // All units are in the set measurement for the document
            // This can be changed to "pt" (points), "mm" (Default), "cm", "in"
            doc.fromHTML($('.myTables').get(0), 15, 15, {
                'width': 170,
                'elementHandlers': specialElementHandlers
            });
            doc.save("test.pdf");
        }

        $scope.otpModalInstances = function () {
            //userService.resendotp(userService.getUserId());
            return $uibModal.open({
                animation: true,
                templateUrl: 'verifyOTPModal.html',
                controller: 'modalInstanceCtrlOTP',
                size: 'sm',
                backdrop: 'static',
                keyboard: false
            });
        }

        $scope.emailModalInstances = function () {
            // userService.resendemailotp(userService.getUserId());
            return $uibModal.open({
                animation: true,
                templateUrl: 'verifyEmailOTPModal.html',
                controller: 'modalInstanceCtrlOTP',
                size: 'sm',
                backdrop: 'static',
                keyboard: false
            });
        }

        $scope.addVendorValidation = function () {
            $scope.emailRegx = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            $scope.mobileRegx = /^\+?([0-9]{2})\)?[-. ]?([0-9]{4})[-. ]?([0-9]{4})$/;
            $scope.panregx = /^([a-zA-Z]{5})(\d{4})([a-zA-Z]{1})$/;

            $scope.addVendorValidationStatus = false;
            $scope.firstvalidation = $scope.companyvalidation = $scope.lastvalidation = $scope.contactvalidation = $scope.emailvalidation = $scope.categoryvalidation = $scope.emailregxvalidation = $scope.contactvalidationlength = $scope.panregxvalidation = $scope.tinvalidation = $scope.stnvalidation = $scope.currencyvalidation = $scope.knownSincevalidation = false;

            if ($scope.newVendor.firstName == "" || $scope.newVendor.firstName === undefined) {
                $scope.firstvalidation = true;
                $scope.addVendorValidationStatus = true;
                return;
            }
            if ($scope.newVendor.lastName == "" || $scope.newVendor.lastName === undefined) {
                $scope.lastvalidation = true;
                $scope.addVendorValidationStatus = true;
                return;
            }
            if ($scope.newVendor.contactNum == "" || $scope.newVendor.contactNum === undefined || isNaN($scope.newVendor.contactNum)) {
                $scope.contactvalidation = true;
                $scope.addVendorValidationStatus = true;
                return;
            } else if (!$scope.mobileRegx.test($scope.newVendor.contactNum)) {
                $scope.contactvalidationlength = true;
                $scope.addVendorValidationStatus = true;
                return;
            }
            if ($scope.newVendor.email == "" || $scope.newVendor.email === undefined) {
                $scope.emailvalidation = true;
                $scope.addVendorValidationStatus = true;
                return;
            } else if (!$scope.emailRegx.test($scope.newVendor.email)) {
                $scope.emailregxvalidation = true;
                $scope.addVendorValidationStatus = true;
                return;
            }
            if ($scope.checkVendorEmailUniqueResult || $scope.checkVendorEmailUniqueResult) {
                $scope.addVendorValidationStatus = true;
                return;
            }
            if ($scope.newVendor.knownSince == "" || $scope.newVendor.knownSince === undefined) {
                $scope.knownSincevalidation = true;
                $scope.addVendorValidationStatus = true;
                return;
            }
            if ($scope.newVendor.companyName == "" || $scope.newVendor.companyName === undefined) {
                $scope.companyvalidation = true;
                $scope.addVendorValidationStatus = true;
                return;
            }



            if ($scope.newVendor.category == "" || $scope.newVendor.category === undefined) {
                $scope.categoryvalidation = true;
                $scope.addVendorValidationStatus = true;
                return;
            }
            if ($scope.newVendor.currency == "" || $scope.newVendor.currency === undefined) {
                $scope.currencyvalidation = true;
                $scope.addVendorValidationStatus = true;
                return;
            }

        }

        $scope.addVendor = function () {
            $scope.addVendorValidation();
            if ($scope.addVendorValidationStatus) {
                return false;
            }
            var vendCAtegories = [];
            $scope.newVendor.category = $scope.newVendor.category;
            vendCAtegories.push($scope.newVendor.category);
            var params = {
                /*"vendorInfo": {
                    "firstName": $scope.newVendor.firstName,
                    "lastName": $scope.newVendor.lastName,
                    "email": $scope.newVendor.email,
                    "contactNum": $scope.newVendor.contactNum,
                    "username": $scope.newVendor.contactNum,
                    "password": $scope.newVendor.contactNum,
                    "rating": 1,
                    "category": vendCAtegories,
                    "panNum": ("panno" in $scope.newVendor) ? $scope.newVendor.panno : "",
                    "serviceTaxNum": ("serviceTaxNo" in $scope.newVendor) ? $scope.newVendor.serviceTaxNo : "",
                    "vatNum": ("vatNum" in $scope.newVendor) ? $scope.newVendor.vatNum : "",
                    "referringUserID": parseInt(userService.getUserId()),
                    "knownSince": ("knownSince" in $scope.newVendor) ? $scope.newVendor.knownSince : "",
                    "errorMessage": "",
                    "sessionID": userService.getUserToken(),
                    "institution": ""
                }*/
                "register": {
                    "firstName": $scope.newVendor.firstName,
                    "lastName": $scope.newVendor.lastName,
                    "email": $scope.newVendor.email,
                    "phoneNum": $scope.newVendor.contactNum,
                    "username": $scope.newVendor.contactNum,
                    "password": $scope.newVendor.contactNum,
                    "companyName": $scope.newVendor.companyName ? $scope.newVendor.companyName : "",
                    "isOTPVerified": 0,
                    "category": $scope.newVendor.category,
                    "userType": "VENDOR",
                    "panNumber": ("panno" in $scope.newVendor) ? $scope.newVendor.panno : "",
                    "stnNumber": ("serviceTaxNo" in $scope.newVendor) ? $scope.newVendor.serviceTaxNo : "",
                    "vatNumber": ("vatNum" in $scope.newVendor) ? $scope.newVendor.vatNum : "",
                    "referringUserID": userService.getUserId(),
                    "knownSince": ("knownSince" in $scope.newVendor) ? $scope.newVendor.knownSince : "",
                    "errorMessage": "",
                    "sessionID": "",
                    "userID": 0,
                    "department": "",
                    "currency": $scope.newVendor.currency.key
                }
            };
            $http({
                method: 'POST',
                url: domain + 'register',
                encodeURI: true,
                headers: { 'Content-Type': 'application/json' },
                data: params
            }).then(function (response) {
                if (response && response.data && response.data.errorMessage == "") {
                    //$scope.formRequest.auctionVendors.push({ vendorName: $scope.newVendor.firstName + " " + $scope.newVendor.lastName, companyName: $scope.newVendor.companyName, vendorID: response.data.objectID });
                    $scope.newVendor = null;
                    $scope.newVendor = {};
                    //$scope.addVendorForm.$setPristine();
                    $scope.addVendorShow = false;
                    growlService.growl("Vendor Added Successfully.", 'inverse');
                    location.reload();
                    this.addVendor = 0;
                } else if (response && response.data && response.data.errorMessage) {
                    growlService.growl(response.data.errorMessage, 'inverse');
                } else {
                    growlService.growl('Unexpected Error Occurred', 'inverse');
                }
            });
        }


        this.updateUserInfo = function () {
            ////console.log('JJJJJJJJJ');
            $log.info("IN UPDATE");
            if ($scope.oldPhoneNum != $scope.userObj.phoneNum) {
                $scope.isPhoneModifies = 1;
            }

            var params = {};
            if ($scope.userDetails.assocWithOEM) {
                if ($scope.userDetails.oemCompanyName == "" || $scope.userDetails.oemKnownSince == "" || $scope.userDetails.assocWithOEMFileName == "") {
                    growlService.growl("If Associated with OEM, please provide further details.", "inverse");
                    return false;
                }
            }
            if ($scope.userObj.firstName.toString() == "") {
                growlService.growl("Name cannot be empty.", "inverse");
                return false;
            }
            if ($scope.userObj.lastName.toString() == "") {
                growlService.growl("Name cannot be empty.", "inverse");
                return false;
            }
            if ($scope.userObj.phoneNum.toString() == "") {
                growlService.growl("Phone Number cannot be empty.", "inverse");
                return false;
            }
            if (isNaN($scope.userObj.phoneNum)) {
                growlService.growl("Please Enter correct Mobile number.", "inverse");
                return false;
            }
            if ($scope.userObj.phoneNum.toString().length != 10) {
                growlService.growl("Phone Number Must be 10 digits.", "inverse");
                return false;
            }
            if ($scope.userObj.email.toString() == "") {
                growlService.growl("Email cannot be empty.", "inverse");
                return false;
            }
            if (this.editPro == 1 && $scope.userDetails.aboutUs == "") {
                growlService.growl("Please update your about us section", "inverse");
                return false;
            }

            var ts = moment($scope.userDetails.establishedDate, "DD-MM-YYYY").valueOf();
            var m = moment(ts);
            var auctionStartDate = new Date(m);
            var milliseconds = parseInt(auctionStartDate.getTime() / 1000.0);
            $scope.userDetails.establishedDate = "/Date(" + milliseconds + "000+0530)/";

            params = $scope.userDetails;


            params.firstName = $scope.userObj.firstName;
            params.lastName = $scope.userObj.lastName;
            params.phoneNum = $scope.userObj.phoneNum;
            if ($scope.logoFile != '' && $scope.logoFile != null) {
                params.logoFile = $scope.logoFile;
                params.logoURL += $scope.logoFile.fileName;
            }
            params.isOTPVerified = $scope.isOTPVerified;
            params.credentialsVerified = $scope.credentialsVerified;
            if (params.phoneNum != $scope.userObj.phoneNum) {
                params.isOTPVerified = 0;
            }
            params.email = $scope.userObj.email;
            params.isEmailOTPVerified = $scope.isEmailOTPVerified;
            if (params.email != $scope.userObj.email) {
                params.isEmailOTPVerified = 0;
            }
            params.sessionID = userService.getUserToken();
            params.userID = userService.getUserId();
            params.errorMessage = "";
            params.subcategories = $scope.userDetails.subcategories;
            userService.updateUser(params)
                .then(function (response) {
                    if (response.toLowerCase().indexOf('already exists') > 0) {
                        userService.getUserDataNoCache().then(function (response) {

                            var loginUserData = userService.getUserObj();
                            $scope.userObj.fullName = loginUserData.firstName + " " + loginUserData.lastName;
                            $scope.userObj.firstName = loginUserData.firstName;
                            $scope.userObj.lastName = loginUserData.lastName;
                            $scope.userObj.gender = "male";
                            $scope.userObj.birthDay = "23/06/1988";
                            $scope.userObj.martialStatus = "Single";
                            $scope.userObj.phoneNum = loginUserData.phoneNum;
                            $scope.userObj.email = loginUserData.email;
                            $scope.oldPhoneNum = loginUserData.phoneNum;
                            $scope.oldemail = loginUserData.email;
                            $scope.userObj = loginUserData;
                            $scope.callGetUserDetails();
                            $scope.updateUserDataFromService();
                            $state.go('pages.profile.profile-about');
                            $state.reload();
                        });
                    }

                    $state.go('pages.profile.profile-about');
                    $state.reload();
                });

            this.editSummary = 0;
            this.editInfo = 0;
            this.editPic = 0;
            this.editContact = 0;
            this.editDocVerification = 0;
            this.editPro = 0;
            this.imagefilesonlyforlogo = false;
            this.addUser = 0;
            $scope.callGetUserDetails();
        };
        $scope.taxnumberalpha = false;
        $scope.taxnumberlengthvalidation = false;
        $scope.taxnumberrequired = false;
        $scope.pannumberalpha = false;
        $scope.pannumberlengthvalidation = false;
        $scope.pannumberrequired = false;
        $scope.tinnumberrequired = false;
        $scope.pannumber = "";
        $scope.vatnumber = "";
        $scope.taxnumber = "";
        $scope.panregx = new RegExp("/^([a-zA-Z]{5})(\d{4})([a-zA-Z]{1})$/");
        $scope.panValidations = function (pannumber) {
            /*var panvalue="";
            panvalue=pannumber;*/
            //$log.debug($scope.panregx.test(pannumber));
            if (pannumber != "" && !$scope.panregx.test(pannumber)) {
                $scope.pannumberalpha = false;
                $scope.pannumberlengthvalidation = false;
                $scope.pannumberrequired = true;
            } else if (('' + pannumber).length < 10) {
                $scope.pannumberalpha = false;
                $scope.pannumberlengthvalidation = true;
                $scope.pannumberrequired = false;
            } else if (!(/^[a-zA-Z0-9]*$/.test(pannumber))) {
                $scope.pannumberalpha = true;
                $scope.pannumberlengthvalidation = false;
                $scope.pannumberrequired = false;
            } else {
                $scope.pannumberalpha = false;
                $scope.pannumberlengthvalidation = false;
                $scope.pannumberrequired = false;
            }
        }
        $scope.tinValidations = function (vatnumber) {
            if (vatnumber == "") {
                $scope.tinnumberrequired = true;
            } else {
                $scope.tinnumberrequired = false;
            }
        }
        $scope.taxValidations = function (taxnumber) {
            if (taxnumber == "") {
                $scope.taxnumberalpha = false;
                $scope.taxnumberlengthvalidation = false;
                $scope.taxnumberrequired = true;
            } else if (("" + taxnumber).length < 15) {
                $scope.taxnumberalpha = false;
                $scope.taxnumberlengthvalidation = true;
                $scope.taxnumberrequired = false;
            } else if (!(/^[a-zA-Z0-9]*$/.test(taxnumber))) {
                $scope.taxnumberalpha = true;
                $scope.taxnumberlengthvalidation = false;
                $scope.taxnumberrequired = false;
            } else {
                $scope.taxnumberalpha = false;
                $scope.taxnumberlengthvalidation = false;
                $scope.taxnumberrequired = false;
            }
        }
        this.updateVerficationInfo = function (pannumber, vatnumber, taxnumber) {

            var panObj = _.filter($scope.CredentialsResponce, ['fileType', 'PAN'])[0];
            var tinObj = _.filter($scope.CredentialsResponce, ['fileType', 'TIN'])[0];
            var stnObj = _.filter($scope.CredentialsResponce, ['fileType', 'STN'])[0];

            if ((!$scope.panObject.fileType || pannumber === "" || !(/^[A-Z]{5}\d{4}[A-Z]{1}$/.test(pannumber.toUpperCase()))) && panObj.credentialID != pannumber) {
                $scope.pannumberalpha = false;
                $scope.pannumberlengthvalidation = false;
                $scope.pannumberrequired = false;

                if (pannumber === "") {
                    $scope.pannumberrequired = true;
                }
                else {
                    if (!(/^[A-Z]{5}\d{4}[A-Z]{1}$/.test(pannumber.toUpperCase()))) {
                        $scope.pannumberlengthvalidation = true;
                    }
                }

                return false;
            }
            else {
                $scope.pannumberalpha = false;
                $scope.pannumberlengthvalidation = false;
                $scope.pannumberrequired = false;
            }
            if ((!$scope.tinObject.fileType || vatnumber === "" || !(/^\d{11}$/.test(vatnumber))) && tinObj.credentialID != vatnumber) {
                $scope.tinnumberrequired = false;
                $scope.tinnumberlengthvalidation = false;
                if (vatnumber === "") {
                    $scope.tinnumberrequired = true;
                }
                else {
                    if (!(/^\d{11}$/.test(vatnumber.toUpperCase()))) {
                        $scope.tinnumberlengthvalidation = true;
                    }
                }

                return false;
            }
            else {
                $scope.tinnumberrequired = false;
                $scope.tinnumberlengthvalidation = false;
            }
            if ((!$scope.stnObject.fileType || taxnumber === "" || !(/^[a-zA-Z0-9]{15}$/.test(taxnumber))) && stnObj.credentialID != taxnumber) {
                $scope.taxnumberalpha = false;
                $scope.taxnumberlengthvalidation = false;
                $scope.taxnumberrequired = false;
                if (taxnumber === "") {
                    $scope.taxnumberrequired = true;
                }
                else {
                    if (!(/^[a-zA-Z0-9]{15}$/.test(taxnumber.toUpperCase()))) {
                        $scope.taxnumberlengthvalidation = true;
                    }
                }


                return false;
            }
            else {
                $scope.taxnumberalpha = false;
                $scope.taxnumberlengthvalidation = false;
                $scope.taxnumberrequired = false;
            }


            if (pannumber !== "" && (panObj.credentialID != pannumber || $scope.panObject.fileStream)) {
                $scope.panObject.credentialID = pannumber;
                if ($scope.panObject.fileStream) {
                    $scope.CredentialUpload.push($scope.panObject);
                }
            }

            if (vatnumber !== "" && (tinObj.credentialID != vatnumber || $scope.tinObject.fileStream)) {
                $scope.tinObject.credentialID = vatnumber;
                if ($scope.tinObject.fileStream) {
                    $scope.CredentialUpload.push($scope.tinObject);
                }
            }
            if (taxnumber !== "" && (stnObj.credentialID != taxnumber || $scope.stnObject.fileStream)) {
                $scope.stnObject.credentialID = taxnumber;
                if ($scope.stnObject.fileStream) {
                    $scope.CredentialUpload.push($scope.stnObject);
                }
            }

            if ($scope.CredentialUpload.length == 0) {
                growlService.growl("Please enter at least one credential for upload", "inverse");
                return false;
            }
            var params = { 'userID': userService.getUserId(), 'files': $scope.CredentialUpload, 'sessionID': userService.getUserToken() };
            $http({
                method: 'POST',
                url: domain + 'updatecredentials',
                encodeURI: true,
                headers: { 'Content-Type': 'application/json' },
                data: params
            }).then(function (response) {
                if (response && response.data && response.data.errorMessage == "") {
                    this.editSummary = 0;
                    this.editInfo = 0;
                    this.editPic = 0;
                    this.editContact = 0;
                    this.editDocVerification = 0;
                    this.editPro = 0;
                    this.addUser = 0;
                    $scope.pannumberalpha = false;
                    $scope.pannumberlengthvalidation = false;
                    $scope.pannumberrequired = false;
                    $scope.taxnumberalpha = false;
                    $scope.taxnumberlengthvalidation = false;
                    $scope.taxnumberrequired = false;
                    $scope.tinnumberrequired = false;
                    $scope.getUserCredentials();
                    //userService.updateVerified(1);
                    swal("Done!", 'Your credentials are being verified. Our associates will contact you as soon as it is done.', 'success');
                    this.editDocVerification = 0;
                } else {
                    growlService.growl(response.data.errorMessage, "inverse");
                    $log.info(response.data.errorMessage);
                }
            }, function (result) {
                $log.info(result);
            });
            this.editDocVerification = 0;
        };

        $scope.panObject = {};
        $scope.tinObject = {};
        $scope.stnObject = {};

        $scope.ImportEntity = {};

        $scope.getFile1 = function (id, doctype, ext) {
            $scope.progress = 0;
            $scope.file = $("#" + id)[0].files[0];
            $scope.docType = doctype + "." + ext;
            fileReader.readAsDataUrl($scope.file, $scope)
                .then(function (result) {
                    if (id != "assocWithOEMFile" && id != 'storeLogo') {
                        var bytearray = new Uint8Array(result);
                        var fileobj = {};
                        fileobj.fileStream = $.makeArray(bytearray);
                        fileobj.fileType = $scope.docType;
                        fileobj.isVerified = 0;
                        //$scope.CredentialUpload.push(fileobj);
                        if (doctype == "PAN") {
                            fileobj.credentialID = $scope.pannumber;
                            $scope.panObject = fileobj;
                        }
                        else if (doctype == "TIN") {
                            fileobj.credentialID = $scope.vatnumber;
                            $scope.tinObject = fileobj;
                        }
                        else if (doctype == "STN") {
                            fileobj.credentialID = $scope.taxnumber;
                            $scope.stnObject = fileobj;
                        }
                    }

                    if (id == "storeLogo") {
                        var bytearray = new Uint8Array(result);
                        $scope.logoFile.fileStream = $.makeArray(bytearray);
                        //$scope.bidAttachementName=$scope.file.name;
                        //$scope.formRequest.attachment=$scope.file.name;
                        $scope.logoFile.fileName = $scope.file.name;
                    }

                    if (id == "profileFile") {
                        var bytearray = new Uint8Array(result);
                        $scope.userDetails.profileFile = $.makeArray(bytearray);
                        $scope.userDetails.profileFileName = $scope.file.name;
                    }


                    if (id == "vendorsAttachment") {
                        if (ext != "xlsx") {
                            swal("Error!", "File type should be XSLX. Please download the template and  fill values accordingly.", "error");
                            return;
                        }
                        var bytearray = new Uint8Array(result);
                        $scope.ImportEntity.attachment = $.makeArray(bytearray);
                        $scope.ImportEntity.attachmentFileName = $scope.file.name;
                        $scope.AddVendorsExcel();
                    }

                    else {
                        var bytearray = new Uint8Array(result);
                        $scope.userDetails.assocWithOEMFile = $.makeArray(bytearray);
                        //$scope.bidAttachementName=$scope.file.name;
                        //$scope.formRequest.attachment=$scope.file.name;
                        $scope.userDetails.assocWithOEMFileName = $scope.file.name;
                    }

                });
        };

        $scope.getFile2 = function () {
            $scope.progress = 0;
            $scope.file = $("#storeLogo")[0].files[0];
            if ($scope.file.type == "image/jpeg" || $scope.file.type == "image/jpg" || $scope.file.type == "image/png") {
                fileReader.readAsDataUrl($scope.file, $scope)
                    .then(function (result) {
                        var bytearray = new Uint8Array(result);
                        $scope.logoFile.fileStream = $.makeArray(bytearray);
                        $scope.logoFile.fileName = $scope.file.name;
                        this.imagefilesonlyforlogo = false;
                    });
            } else {
                this.imagefilesonlyforlogo = true;
            }
        };

        $scope.addnewuserobj = {};

        this.AddNewUser = function () {

            if (isNaN($scope.addnewuserobj.phoneNum)) {
                $scope.showMessage = true;
                $scope.msg = $scope.getErrMsg("Please enter a valid Phone Number");
            } else {
                $scope.showMessage = false;
                $scope.msg = '';
            }
            $scope.addnewuserobj.userType = userService.getUserType();
            $scope.addnewuserobj.username = $scope.addnewuserobj.email;
            $scope.addnewuserobj.companyName = $scope.userObj.institution;
            $scope.addnewuserobj.currency = $scope.addnewuserobj.currency.key;
            ////console.log($scope.addnewuserobj.currency.key + "  ---  Key Selected");
            userService.addnewuser($scope.addnewuserobj)
                .then(function (response) {
                    if (response.errorMessage != "") {
                        growlService.growl(response.errorMessage, "inverse");
                    } else {
                        growlService.growl("User added successfully.", "inverse");
                        this.addUser = 0;
                        $state.reload();
                    }
                });

        };



        $scope.vendorsList = [];
        $scope.inactiveVendorsList = [];

        $scope.GetCompanyVendors = function () {
            $scope.isOTPVerified = loginUserData.isOTPVerified;
            $scope.isEmailOTPVerified = loginUserData.isEmailOTPVerified;
            $scope.params = { "userID": userService.getUserId(), "sessionID": userService.getUserToken() }

            if ($scope.isOTPVerified && $scope.isEmailOTPVerified) {
                userService.GetCompanyVendors($scope.params)
                    .then(function (response) {
                        $scope.vendorsList = $filter('filter')(response, { isValid: true });
                        $scope.inactiveVendorsList = $filter('filter')(response, { isValid: false });
                    });
            }
        }

        $scope.GetCompanyVendors();


        $scope.activatecompanyvendor = function (vendorID, isValid) {
            userService.activatecompanyvendor({ "customerID": userService.getUserId(), "vendorID": vendorID, "isValid": isValid })
                .then(function (response) {
                    if (response.errorMessage != "") {
                        growlService.growl(response.errorMessage, "inverse");
                    }
                    else if (response.errorMessage == "") {
                        $scope.GetCompanyVendors();
                        if (isValid == 0) {
                            growlService.growl("Vendor deleted Successfully", "inverse");
                        }
                        else if (isValid == 1) {
                            growlService.growl("Vendor Activated Successfully", "success");
                        }
                    }
                });
        }



        $scope.NegotiationSettingsValidationMessage = '';

        $scope.NegotiationTimeValidation = function (days, hours, mins, minReduction, rankComparision) {
            $scope.NegotiationSettingsValidationMessage = '';

            if (minReduction < 10 || minReduction == undefined) {
                $scope.NegotiationSettingsValidationMessage = 'Set Min 10 for Min.Amount reduction';
                return;
            }

            if (rankComparision < 10 || rankComparision == undefined) {
                $scope.NegotiationSettingsValidationMessage = 'Set Min 10 for Rank Comparision price';
                return;
            }

            if (minReduction >= 10 && rankComparision > minReduction) {
                $scope.NegotiationSettingsValidationMessage = 'Please enter Valid Rank Comparision price less than Min. Amount reduction';
                return;
            }
            if (days == undefined || days < 0) {
                $scope.NegotiationSettingsValidationMessage = 'Please Enter Valid Days';
                return;
            }
            if (hours < 0) {
                $scope.NegotiationSettingsValidationMessage = 'Please Enter Valid Hours';
                return;
            }
            if (mins < 0) {
                $scope.NegotiationSettingsValidationMessage = 'Please Enter Valid Minunts';
                return;
            }
            if (mins >= 60 || mins == undefined) {
                $scope.NegotiationSettingsValidationMessage = 'Please Enter Valid Minunts';
                return;
            }
            if (hours > 24 || hours == undefined) {
                $scope.NegotiationSettingsValidationMessage = 'Please Enter Valid Hours';
                return;
            }
            if (hours == 24 && mins > 0) {
                $scope.NegotiationSettingsValidationMessage = 'Please Enter Valid Hours & Minuts';
                return;
            }
            if (mins < 5 && hours == 0 && days == 0) {
                $scope.NegotiationSettingsValidationMessage = 'Please Enter Min 5 Minutes';
                return;
            }
        }


        $scope.NegotiationSettings = [];

        $scope.NegotiationSettings.negotiationDuration = $scope.days + ' ' + $scope.hours + ':' + $scope.mins + ':00.0';

        $scope.saveNegotiationSettings = function () {
            $scope.NegotiationTimeValidation($scope.days, $scope.hours, $scope.mins, $scope.NegotiationSettings.minReductionAmount, $scope.NegotiationSettings.rankComparision);

            if ($scope.NegotiationSettingsValidationMessage == '') {
                $scope.NegotiationSettings.negotiationDuration = $scope.days + ' ' + $scope.hours + ':' + $scope.mins + ':00.0';
                var params = {
                    "NegotiationSettings": {
                        "userID": userService.getUserId(),
                        "minReductionAmount": $scope.NegotiationSettings.minReductionAmount,
                        "rankComparision": $scope.NegotiationSettings.rankComparision,
                        "negotiationDuration": $scope.NegotiationSettings.negotiationDuration,
                        "sessionID": userService.getUserToken()
                    }
                };
                $http({
                    method: 'POST',
                    url: domain + 'savenegotiationsettings',
                    encodeURI: true,
                    headers: { 'Content-Type': 'application/json' },
                    data: params
                }).then(function (response) {
                    if (response && response.data && (response.data.errorMessage == "" || response.data.errorMessage == null)) {
                        $scope.NegotiationSettings = response.data;
                        growlService.growl('Successfully Saved', 'success');
                    }
                    else {
                        growlService.growl('Not Saved', 'inverse');
                    }
                });
            }
        }


        $scope.AddVendorsExcel = function () {
            var params = {
                "entity": {
                    attachment: $scope.ImportEntity.attachment,
                    userid: parseInt(userService.getUserId()),
                    entityName: 'AddVendor',
                    sessionid: userService.getUserToken()
                }
            }

            userService.AddVendorsExcel(params)
                .then(function (response) {
                    if (response.errorMessage == "") {
                        swal({
                            title: "Thanks!",
                            text: "Vendors Added Successfully",
                            type: "success",
                            showCancelButton: false,
                            confirmButtonColor: "#DD6B55",
                            confirmButtonText: "Ok",
                            closeOnConfirm: true
                        },
                            function () {
                                location.reload();
                            });


                    } else {
                        swal({
                            title: "Failed!",
                            text: response.errorMessage,
                            type: "error",
                            showCancelButton: false,
                            confirmButtonColor: "#DD6B55",
                            confirmButtonText: "Ok",
                            closeOnConfirm: true
                        },
                            function () {
                                location.reload();
                            });
                    }
                })
        }

        $scope.showAccess = false;
        $scope.UserName = '';

        $scope.loadAccess = function (action, userID, userName) {
            $scope.subuserentitlements = [];
            $scope.showAccess = action;
            $scope.UserName = userName;
            if (action) {
                userService.getuseraccess(userID, userService.getUserToken())
                    .then(function (responseObj) {
                        if (responseObj && responseObj.length > 0) {
                            $scope.subuserentitlements = responseObj;
                        }
                    })
            }
        }



        $scope.validateAccess = function (isEntitled, accessType) {
            if (accessType == 'View Requirement' && isEntitled == false) {
                $scope.subuserentitlements.forEach(function (item, index) {
                    if (item.accessType == 'Requirement Posting') {
                        item.isEntitled = false;
                    }
                    if (item.accessType == 'Quotations Verification') {
                        item.isEntitled = false;
                    }
                    if (item.accessType == 'Negotiation Schedule') {
                        item.isEntitled = false;
                    }
                    if (item.accessType == 'Live Negotiation (Bidding Process)') {
                        item.isEntitled = false;
                    }
                    if (item.accessType == 'Po Generation') {
                        item.isEntitled = false;
                    }
                })
            }


            if (accessType == 'Requirement Posting' && isEntitled == true) {
                $scope.subuserentitlements.forEach(function (item, index) {
                    if (item.accessType == 'View Requirement') {
                        item.isEntitled = true;
                    }
                })
            }


            if (accessType == 'Quotations Verification' && isEntitled == true) {
                $scope.subuserentitlements.forEach(function (item, index) {
                    if (item.accessType == 'View Requirement') {
                        item.isEntitled = true;
                    }
                    if (item.accessType == 'Requirement Posting') {
                        item.isEntitled = true;
                    }
                })
            }


            if (accessType == 'Negotiation Schedule' && isEntitled == true) {
                $scope.subuserentitlements.forEach(function (item, index) {
                    if (item.accessType == 'View Requirement') {
                        item.isEntitled = true;
                    }
                    if (item.accessType == 'Requirement Posting') {
                        item.isEntitled = true;
                    }

                    if (item.accessType == 'Quotations Verification') {
                        item.isEntitled = true;
                    }
                })
            }


            if (accessType == 'Live Negotiation (Bidding Process)' && isEntitled == true) {
                $scope.subuserentitlements.forEach(function (item, index) {
                    if (item.accessType == 'View Requirement') {
                        item.isEntitled = true;
                    }
                    if (item.accessType == 'Requirement Posting') {
                        item.isEntitled = true;
                    }

                    if (item.accessType == 'Quotations Verification') {
                        item.isEntitled = true;
                    }
                    if (item.accessType == 'Negotiation Schedule') {
                        item.isEntitled = true;
                    }
                })
            }


            if (accessType == 'Po Generation' && isEntitled == true) {
                $scope.subuserentitlements.forEach(function (item, index) {
                    if (item.accessType == 'View Requirement') {
                        item.isEntitled = true;
                    }
                    if (item.accessType == 'Requirement Posting') {
                        item.isEntitled = true;
                    }

                    if (item.accessType == 'Quotations Verification') {
                        item.isEntitled = true;
                    }
                    if (item.accessType == 'Negotiation Schedule') {
                        item.isEntitled = true;
                    }
                    if (item.accessType == 'Live Negotiation (Bidding Process)') {
                        item.isEntitled = true;
                    }
                })
            }
        }


        $scope.saveUserAccess = function () {
            var params = {
                "listUserAccess": $scope.subuserentitlements,
                "sessionID": userService.getUserToken()
            };

            userService.saveUserAccess(params)
                .then(function (response) {
                    if (response.errorMessage != '') {
                        //swal("Error!", 'Not Saved', 'error');

                        growlService.growl(response.errorMessage, "inverse");
                    }
                    else {

                        growlService.growl("Access Levels Defined Successfully.", "success");
                        //swal("Done!", 'Access Levels Defined Successfully.', 'success');
                        $scope.showAccess = false;
                    }
                })
        };



        $scope.showUserDepartments = false;
        $scope.UserName = '';

        $scope.loadUserDepartments = function (action, userID, userName) {
            $scope.subUserDepartments = [];
            $scope.showUserDepartments = action;
            $scope.UserName = userName;
            if (action) {
                auctionsService.GetUserDepartments(userID, userService.getUserToken())
                    .then(function (response) {
                        if (response && response.length > 0) {
                            $scope.subUserDepartments = response;
                        }
                    })
            }
        }


        $scope.SaveUserDepartments = function () {

            var params = {
                "listUserDepartments": $scope.subUserDepartments,
                "sessionID": userService.getUserToken()
            };

            auctionsService.SaveUserDepartments(params)
                .then(function (response) {
                    if (response.errorMessage != '') {
                        //swal("Error!", 'Not Saved', 'error');

                        growlService.growl(response.errorMessage, "inverse");
                    }
                    else {

                        growlService.growl("Access Levels Defined Successfully.", "success");
                        //swal("Done!", 'Access Levels Defined Successfully.', 'success');
                        $scope.showUserDepartments = false;
                    }
                })
        };





        $scope.goToEvaluation = function (auctionDetails) {
            if ($scope.isCustomer == "CUSTOMER") {
                $state.go("techeval", { "reqID": auctionDetails.requirementID, "evalID": 0 });
            }
            else {
                $state.go("techevalresponses", { "reqID": auctionDetails.requirementID, "vendorID": userService.getUserId() });
            }

        };



        $scope.GetMyDepartments = function () {
            
            auctionsService.GetUserDepartments(userService.getUserId(), userService.getUserToken())
                    .then(function (response) {
                        if (response && response.length > 0) {
                            $scope.myDepartments = response;
                            $scope.myDepartments = _.filter($scope.myDepartments, function (x) { return x.isValid == true; });
                            $log.info('This |||||||||||');
                            $log.info($scope.myDepartments);
                        }
                    })
        }
        //deptCode

        $scope.GetMyDepartments();


    });prmApp

    //=================================================
    // Profile
    //=================================================

    //.controller('profileCtrl', function ($scope,growlService,$http,domain,auctionsService,userService) {
    .controller('profileCtrl', function ($timeout, $uibModal, $state, $scope, growlService, userService, auctionsService, $http, domain, $rootScope, fileReader, $filter, $log) {

        //Get Profile Information from profileService Service
        $scope.userObj = {};
        userService.getUserDataNoCache()
            .then(function (response) {
                $scope.userObj = response;
            })

        $scope.isnegotiationrunning = '';
        userService.isnegotiationrunning()
            .then(function (response) {
                $scope.isnegotiationrunning = response.data.IsNegotationRunningResult;
                //console.log($scope.isnegotiationrunning);
            })
        //User
        $scope.editPwd = 0;

        $scope.isPhoneModifies = 0;
        $scope.isEmailModifies = 0;

        $scope.showIsSuperUser = function () {
            if (userService.getUserObj().isSuperUser) {
                return true;
            } else {
                return false;
            }
        }


        $scope.pwdObj = {
            username: userService.getUserObj().username,
            oldPass: '',
            newPass: ''
        };

        $scope.categories = [];

        $scope.getCategories = function () {
            auctionsService.getCategories(userService.getUserId())
                .then(function (response) {
                    $scope.categories = response;
                })
        }


        /*$scope.currencyFormat = function(amount)
          {
            x=amount;
            x=x.toString();
            var afterPoint = '';
            if(x.indexOf('.') > 0)
               afterPoint = x.substring(x.indexOf('.'),x.length);
            x = Math.floor(x);
            x=x.toString();
            var lastThree = x.substring(x.length-3);
            var otherNumbers = x.substring(0,x.length-3);
            if(otherNumbers != '')
              lastThree = ',' + lastThree;
            var res = otherNumbers.replace(/\B(?=(\d{2})+(?!\d))/g, ",") + lastThree + afterPoint;
            return "&#8377; " + res;
            //return amount.toLocaleString('en-in', { style: 'currency', currency: 'INR' });
          }*/



        $scope.getCategories();

        $scope.userDetails = {
            achievements: "",
            assocWithOEM: false,
            clients: "",
            establishedDate: "",
            aboutUs: "",
            logoFile: "",
            logoURL: "",
            products: "",
            strengths: "",
            responseTime: "",
            oemCompanyName: "",
            oemKnownSince: "",
            workingHours: "",
            files: [],
            directors: "",
            address: ""

        };

        $scope.userObj = userService.getUserObj();

        this.updatePwd = function () {
            if ($scope.pwdObj.newPass.length < 6) {
                growlService.growl("Password should be at least 6 characters long.", "inverse");
                return false;
            } else if ($scope.pwdObj.newPass != $scope.pwdObj.confirmNewPass) {
                growlService.growl('Passwords do not match, please enter the same password in both new and Confirm Password fields', 'inverse');
                return false;
            }
            $scope.pwdObj.userID = parseInt(userService.getUserId());
            userService.updatePassword($scope.pwdObj)
                .then(function (response) {
                    if (response.errorMessage == "") {
                        $scope.pwdObj = {
                            username: userService.getUserObj().username
                        };
                        swal("Done!", 'Your password has been successfully updated.', 'success');
                    } else {
                        swal("Error!", response.errorMessage, 'error');
                    }
                })
        }

        $scope.callGetUserDetails = function () {
            userService.getProfileDetails({ "userid": userService.getUserId(), "sessionid": userService.getUserToken() })
                .then(function (response) {
                    $scope.userStatus = "registered";
                    if (response != undefined) {
                        $scope.userDetails = response;
                        var data = response.establishedDate;
                        var date = new Date(parseInt(data.substr(6)));
                        $scope.userDetails.establishedDate = date.getDate() + '/' + (date.getMonth() + 1) + '/' + date.getFullYear();

                        if (response.registrationScore > 89) {

                            $scope.userStatus = "Authorised";

                        }
                    }
                });
        }



        $scope.subUsers = [];
        $scope.inactiveSubUsers = [];

        $scope.getSubUserData = function () {
            userService.getSubUsersData({ "userid": userService.getUserId(), "sessionid": userService.getUserToken() })
                .then(function (response) {
                    $scope.subUsers = $filter('filter')(response, { isValid: true });
                    $scope.inactiveSubUsers = $filter('filter')(response, { isValid: false });
                });
        }

        $scope.deleteUser = function (userid) {
            userService.deleteUser({ "userID": userid, "referringUserID": userService.getUserId() })
                .then(function (response) {
                    if (response.errorMessage != "") {
                        growlService.growl(response.errorMessage, "inverse");
                    } else {
                        growlService.growl("User deleted Successfully", "inverse");
                        $scope.getSubUserData();
                    }
                });
        }

        $scope.activateUser = function (userid) {
            userService.activateUser({ "userID": userid, "referringUserID": userService.getUserId() })
                .then(function (response) {
                    if (response.errorMessage != "") {
                        growlService.growl(response.errorMessage, "inverse");
                    } else {
                        growlService.growl("User deleted Successfully", "inverse");
                        $scope.getSubUserData();
                    }
                });
        }

        $scope.getSubUserData();


        this.profileSummary = "";

        var loginUserData = userService.getUserObj();
        $scope.userObj.fullName = loginUserData.firstName + " " + loginUserData.lastName;
        $scope.userObj.firstName = loginUserData.firstName;
        $scope.userObj.lastName = loginUserData.lastName;
        $scope.userObj.gender = "male";
        $scope.userObj.birthDay = "23/06/1988";
        $scope.userObj.martialStatus = "Single";
        $scope.userObj.phoneNum = loginUserData.phoneNum;
        $scope.userObj.email = loginUserData.email;
        $scope.oldPhoneNum = loginUserData.phoneNum;
        $scope.oldemail = loginUserData.email;
        /*this.aboutUs="";*/
        $scope.userObj.email = loginUserData.email;

        $scope.userObj.addressSuite = loginUserData.addressLine1 + " " + loginUserData.addressLine2 + " " + loginUserData.addressLine3;
        $scope.userObj.addressCity = loginUserData.city;
        $scope.userObj.addressCountry = loginUserData.country;
        this.userId = userService.getUserId();
        $scope.isOTPVerified = loginUserData.isOTPVerified;
        $scope.isEmailOTPVerified = loginUserData.isEmailOTPVerified;
        $rootScope.$on("CallProfileMethod", function () {
            $scope.updateUserDataFromService();
        });
        this.imagefilesonlyforlogo = false;
        this.editSummary = 0;
        this.editInfo = 0;
        this.editPic = 0;
        this.editDocVerification = 0;
        this.editContact = 0;
        this.editPro = 0;
        this.addUser = 0;
        $scope.myAuctionsLoaded = false;
        $scope.myAuctions = [];
        $scope.myActiveLeads = [];
        var date = new Date();

        $scope.getFile = function () {
            $scope.progress = 0;
            $scope.file = $("#tindoc")[0].files[0];
            fileReader.readAsDataUrl($scope.file, $scope)
                .then(function (result) {

                });
        };

        this.changepasswordstatus = this.newphonenumber_validation_error = this.newphonenumber_required_error = false;
        this.newphonenumber = "";
        $scope.CredentialUpload = [];
        $scope.uploadedCredentials = [{ "credentialID": '', "fileType": 'PAN', 'fileLink': "", 'isVerified': 0 }, { "credentialID": '', "fileType": 'TIN', 'fileLink': "", 'isVerified': 0 }, { "credentialID": '', "fileType": 'STN', 'fileLink': "", 'isVerified': 0 }];
        $scope.logoFile = { "fileName": '', 'fileStream': "" };
        this.newphonenumber_errors = false;
        $scope.otpvalue = "";

        $scope.getUserCredentials = function () {
            $http({
                method: 'GET',
                url: domain + 'getusercredentials?sessionid=' + userService.getUserToken() + "&userid=" + userService.getUserId(),
                encodeURI: true,
                headers: { 'Content-Type': 'application/json' }
            }).then(function (response) {

                $scope.CredentialsResponce = response.data;
                //var pan = $filter('filter')($scope.CredentialsResponce, 'pan');


                if (response && response.data && response.data.length > 0) {
                    if (response.data[0].errorMessage == "") {
                        var panObj = _.filter($scope.CredentialsResponce, ['fileType', 'PAN']);
                        $scope.pannumber = panObj[0].credentialID;
                        $scope.vatnumber = _.filter($scope.CredentialsResponce, ['fileType', 'TIN'])[0].credentialID;
                        $scope.taxnumber = _.filter($scope.CredentialsResponce, ['fileType', 'STN'])[0].credentialID;
                        var verifiedDocsCount = 0;
                        $.each(response.data, function (key, value) {
                            $scope.PAN = value.credentialID;
                            if (value.isVerified == 1) {
                                verifiedDocsCount++;
                            }
                        });
                        if (response.data.length == verifiedDocsCount) {
                            userService.updateVerified(1);
                        }
                        $scope.uploadedCredentials = response.data;
                        if (response.data.length > 0) {
                            this.editDocVerification = 0;
                            $scope.editDocVerification = 0;
                        }
                    }
                }





            }, function (result) {
                $log.error("error in request service");
            });
        };
        $scope.updateUserDataFromService = function (msg) {
            loginUserData = userService.getUserObj();
            $scope.oldPhoneNum = loginUserData.phoneNum;
            $scope.oldemail = loginUserData.email;
            $scope.userObj.fullName = loginUserData.firstName + " " + loginUserData.lastName;
            $scope.userObj.firstName = loginUserData.firstName;
            $scope.userObj.lastName = loginUserData.lastName;
            $scope.userObj.gender = "male";
            $scope.userObj.birthDay = "23/06/1988";
            $scope.userObj.martialStatus = "Single";
            $scope.userObj.phoneNum = loginUserData.phoneNum;
            $scope.userObj.email = loginUserData.email;
            $scope.userObj.addressSuite = loginUserData.addressLine1 + " " + loginUserData.addressLine2 + " " + loginUserData.addressLine3;
            $scope.userObj.addressCity = loginUserData.city;
            $scope.userObj.addressCountry = loginUserData.country;
            this.userId = userService.getUserId();
            $scope.isOTPVerified = loginUserData.isOTPVerified;
            $scope.isEmailOTPVerified = loginUserData.isEmailOTPVerified;
            $scope.credentialsVerified = loginUserData.credentialsVerified;
            $scope.callGetUserDetails();
            $scope.getUserCredentials();
            if ($scope.isOTPVerified && $scope.isEmailOTPVerified) {
                auctionsService.getmyAuctions({ "userid": userService.getUserId(), "sessionid": userService.getUserToken() })
                    .then(function (response) {
                        $scope.myAuctions = response;
                        if ($scope.myAuctions.length > 0) {
                            $scope.myAuctionsLoaded = true;
                        } else {
                            $scope.myAuctionsLoaded = false;
                            $scope.myAuctionsMessage = "There are no auctions running right now for you.";
                        }
                    });


                auctionsService.getactiveleads({ "userid": userService.getUserId(), "sessionid": userService.getUserToken() })
                    .then(function (response) {
                        $scope.myActiveLeads = response;
                        if ($scope.myActiveLeads.length > 0) {
                            $scope.myAuctionsLoaded = true;
                        } else {
                            $scope.myAuctionsLoaded = false;
                            $scope.myAuctionsMessage = "There are no auctions running right now for you.";
                        }
                    });




            }
        }

        // var intervalPromise = window.setInterval(function () {
        //     if (window.location.hash.indexOf("#/pages/profile/profile-timeline") > -1) {
        //         $scope.updateUserDataFromService();
        //     }
        // }, 100000);
        $scope.updateUserDataFromService('default call');



        //$scope.updateUserDataFromService(userService);
        this.editMode = function () {
            this.editPro = 1;

        }



        /* $scope.sendOTPagain=function(){
             userService.resendotp(userService.getUserId());
         };*/
        $scope.generatePDF = function () {
            var doc = new jsPDF();

            // We'll make our own renderer to skip this editor
            var specialElementHandlers = {
                '#editor': function (element, renderer) {
                    return true;
                }
            };

            // All units are in the set measurement for the document
            // This can be changed to "pt" (points), "mm" (Default), "cm", "in"
            doc.fromHTML($('.myTables').get(0), 15, 15, {
                'width': 170,
                'elementHandlers': specialElementHandlers
            });
            doc.save("test.pdf");
        }

        $scope.otpModalInstances = function () {
            //userService.resendotp(userService.getUserId());
            return $uibModal.open({
                animation: true,
                templateUrl: 'verifyOTPModal.html',
                controller: 'modalInstanceCtrlOTP',
                size: 'sm',
                backdrop: 'static',
                keyboard: false
            });
        }

        $scope.emailModalInstances = function () {
            userService.resendemailotp(userService.getUserId());
            return $uibModal.open({
                animation: true,
                templateUrl: 'verifyEmailOTPModal.html',
                controller: 'modalInstanceCtrlOTP',
                size: 'sm',
                backdrop: 'static',
                keyboard: false
            });
        }


        this.updateUserInfo = function () {
            //console.log('JJJJJJJJJ');
            if ($scope.oldPhoneNum != $scope.userObj.phoneNum) {
                $scope.isPhoneModifies = 1;
            }
            var ts = moment($scope.userDetails.establishedDate, "DD-MM-yyyy HH:mm").valueOf();
            var m = moment(ts);
            var auctionStartDate = new Date(m);
            var milliseconds = parseInt(auctionStartDate.getTime() / 1000.0);
            $scope.userDetails.establishedDate = "/Date(" + milliseconds + "000+0530)/";
            var params = {};
            params = $scope.userDetails;

            params.firstName = $scope.userObj.firstName;
            params.lastName = $scope.userObj.lastName;
            params.phoneNum = $scope.userObj.phoneNum;
            if ($scope.logoFile != '' && $scope.logoFile != null) {
                params.logoFile = $scope.logoFile;
                params.logoURL += $scope.logoFile.fileName;
            }
            params.isOTPVerified = $scope.isOTPVerified;
            params.credentialsVerified = $scope.credentialsVerified;
            if (params.phoneNum != $scope.userObj.phoneNum) {
                params.isOTPVerified = 0;
            }
            params.email = $scope.userObj.email;
            params.isEmailOTPVerified = $scope.isEmailOTPVerified;
            if (params.email != $scope.userObj.email) {
                params.isEmailOTPVerified = 0;
            }
            params.sessionID = userService.getUserToken();
            params.userID = userService.getUserId();
            params.errorMessage = "";
            userService.updateUser(params)
                .then(function (response) {
                    if (response.toLowerCase().indexOf('already exists') > 0) {
                        userService.getUserDataNoCache().then(function (response) {

                            var loginUserData = userService.getUserObj();
                            $scope.userObj.fullName = loginUserData.firstName + " " + loginUserData.lastName;
                            $scope.userObj.firstName = loginUserData.firstName;
                            $scope.userObj.lastName = loginUserData.lastName;
                            $scope.userObj.gender = "male";
                            $scope.userObj.birthDay = "23/06/1988";
                            $scope.userObj.martialStatus = "Single";
                            $scope.userObj.phoneNum = loginUserData.phoneNum;
                            $scope.userObj.email = loginUserData.email;
                            $scope.oldPhoneNum = loginUserData.phoneNum;
                            $scope.oldemail = loginUserData.email;
                            $scope.userObj = loginUserData;
                            //$scope.callGetUserDetails();
                            $scope.callGetUserDetails();
                            $scope.updateUserDataFromService();
                            $state.go('pages.profile.profile-about');
                            $state.reload();


                        });
                    }
                    $state.go('pages.profile.profile-about');
                    $state.reload();
                });

            this.editSummary = 0;
            this.editInfo = 0;
            this.editPic = 0;
            this.editContact = 0;
            this.editDocVerification = 0;
            this.editPro = 0;
            this.imagefilesonlyforlogo = false;
            this.addUser = 0;
            $scope.callGetUserDetails();
        };
        $scope.taxnumberalpha = false;
        $scope.taxnumberlengthvalidation = false;
        $scope.taxnumberrequired = false;
        $scope.pannumberalpha = false;
        $scope.pannumberlengthvalidation = false;
        $scope.pannumberrequired = false;
        $scope.tinnumberrequired = false;
        $scope.pannumber = "";
        $scope.vatnumber = "";
        $scope.taxnumber = "";
        $scope.panValidations = function (pannumber) {
            /*var panvalue="";
            panvalue=pannumber;*/
            if (pannumber == "") {
                $scope.pannumberalpha = false;
                $scope.pannumberlengthvalidation = false;
                $scope.pannumberrequired = true;
            } else if (('' + pannumber).length < 10) {
                $scope.pannumberalpha = false;
                $scope.pannumberlengthvalidation = true;
                $scope.pannumberrequired = false;
            } else if (!(/^[a-zA-Z0-9]*$/.test(pannumber))) {
                $scope.pannumberalpha = true;
                $scope.pannumberlengthvalidation = false;
                $scope.pannumberrequired = false;
            } else {
                $scope.pannumberalpha = false;
                $scope.pannumberlengthvalidation = false;
                $scope.pannumberrequired = false;
            }
        }
        $scope.tinValidations = function (vatnumber) {
            if (vatnumber == "") {
                $scope.tinnumberrequired = true;
            } else {
                $scope.tinnumberrequired = false;
            }
        }
        $scope.taxValidations = function (taxnumber) {
            if (taxnumber == "") {
                $scope.taxnumberalpha = false;
                $scope.taxnumberlengthvalidation = false;
                $scope.taxnumberrequired = true;
            } else if (("" + taxnumber).length < 15) {
                $scope.taxnumberalpha = false;
                $scope.taxnumberlengthvalidation = true;
                $scope.taxnumberrequired = false;
            } else if (!(/^[a-zA-Z0-9]*$/.test(taxnumber))) {
                $scope.taxnumberalpha = true;
                $scope.taxnumberlengthvalidation = false;
                $scope.taxnumberrequired = false;
            } else {
                $scope.taxnumberalpha = false;
                $scope.taxnumberlengthvalidation = false;
                $scope.taxnumberrequired = false;
            }
        }
        this.updateVerficationInfo = function (pannumber, vatnumber, taxnumber) {
            /*if (pannumber == "") {
                $scope.pannumberalpha = false;
                $scope.pannumberlengthvalidation = false;
                $scope.pannumberrequired = true;
                return false;
            } else if (('' + pannumber).length < 10) {
                $scope.pannumberalpha = false;
                $scope.pannumberlengthvalidation = true;
                $scope.pannumberrequired = false;
                return false;
            } else if (!(/^[a-zA-Z0-9]*$/.test(pannumber))) {
                $scope.pannumberalpha = true;
                $scope.pannumberlengthvalidation = false;
                $scope.pannumberrequired = false;
                return false;
            } else {
                $scope.pannumberalpha = false;
                $scope.pannumberlengthvalidation = false;
                $scope.pannumberrequired = false;
            }
            if (vatnumber == "") {
                $scope.tinnumberrequired = true;
                return false;
            } else {
                $scope.tinnumberrequired = false;
            }
            if (taxnumber == "") {
                $scope.taxnumberalpha = false;
                $scope.taxnumberlengthvalidation = false;
                $scope.taxnumberrequired = true;
                return false;
            } else if (("" + taxnumber).length < 15) {
                $scope.taxnumberalpha = false;
                $scope.taxnumberlengthvalidation = true;
                $scope.taxnumberrequired = false;
                return false;
            } else if (!(/^[a-zA-Z0-9]*$/.test(taxnumber))) {
                $scope.taxnumberalpha = true;
                $scope.taxnumberlengthvalidation = false;
                $scope.taxnumberrequired = false;
                return false;
            } else {
                $scope.taxnumberalpha = false;
                $scope.taxnumberlengthvalidation = false;
                $scope.taxnumberrequired = false;
            }*/
            var panObj = _.filter($scope.CredentialsResponce, ['fileType', 'PAN'])[0];
            var tinObj = _.filter($scope.CredentialsResponce, ['fileType', 'TIN'])[0];
            var stnObj = _.filter($scope.CredentialsResponce, ['fileType', 'STN'])[0];

            if ((!panObj.fileType || panObj.credentialID == "" || $scope.panObject.credentialID != panObj.credentialID) && ((pannumber !== "" && !$scope.panObject.fileType) || (pannumber === "" && $scope.panObject.fileType))) {
                $scope.pannumberalpha = false;
                $scope.pannumberlengthvalidation = false;
                $scope.pannumberrequired = true;
                return false;
            }
            if ((!tinObj.fileType || tinObj.credentialID == "" || $scope.tinObject.credentialID != tinObj.credentialID) && ((vatnumber !== "" && !$scope.tinObject.fileType) || (vatnumber === "" && $scope.tinObject.fileType))) {
                $scope.tinnumberrequired = true;
                return false;
            }
            if ((!stnObj.fileType || stnObj.credentialID == "" || $scope.stnObject.credentialID != stnObj.credentialID) && ((taxnumber !== "" && !$scope.stnObject.fileType) || (taxnumber === "" && $scope.stnObject.fileType))) {
                $scope.taxnumberalpha = false;
                $scope.taxnumberlengthvalidation = false;
                $scope.taxnumberrequired = true;
                return false;
            }

            if ((!panObj.fileType || panObj.credentialID == "" || $scope.panObject.credentialID != panObj.credentialID) && pannumber !== "") {
                $scope.panObject.credentialID = pannumber;
                $scope.CredentialUpload.push($scope.panObject);
            }

            if ((!tinObj.fileType || tinObj.credentialID == "" || $scope.tinObject.credentialID != tinObj.credentialID) && vatnumber !== "") {
                $scope.tinObject.credentialID = vatnumber;
                $scope.CredentialUpload.push($scope.tinObject);
            }
            if ((!stnObj.fileType || stnObj.credentialID == "" || $scope.stnObject.credentialID != stnObj.credentialID) && taxnumber !== "") {
                $scope.stnObject.credentialID = taxnumber;
                $scope.CredentialUpload.push($scope.stnObject);
            }

            /*angular.forEach($scope.CredentialUpload, function (value, key) {
                if (value.fileType.indexOf('PAN') > -1) { value.credentialID = pannumber; }
                if (value.fileType.indexOf('TIN') > -1) { value.credentialID = vatnumber; }
                if (value.fileType.indexOf('STN') > -1) { value.credentialID = taxnumber; }
                if(pannumber !== '' && value.fileType.indexOf('PAN') < 1){
                    $scope.pannumberalpha = false;
                    $scope.pannumberlengthvalidation = false;
                    $scope.pannumberrequired = true;
                    return false;
                }
                if(vatnumber !== '' && value.fileType.indexOf('TIN') < 1){
                    $scope.tinnumberrequired = true;
                    return false;
                }
                if(taxnumber !== '' && value.fileType.indexOf('STN') < 1){
                    $scope.taxnumberalpha = false;
                    $scope.taxnumberlengthvalidation = false;
                    $scope.taxnumberrequired = true;
                    return false;
                }
            });*/
            if ($scope.CredentialUpload.length == 0) {
                growlService.growl("Please enter at least one credential for upload", "inverse");
                return false;
            }
            var params = { 'userID': userService.getUserId(), 'files': $scope.CredentialUpload, 'sessionID': userService.getUserToken() };
            $http({
                method: 'POST',
                url: domain + 'updatecredentials',
                encodeURI: true,
                headers: { 'Content-Type': 'application/json' },
                data: params
            }).then(function (response) {
                if (response && response.data && response.data.errorMessage == "") {
                    this.editSummary = 0;
                    this.editInfo = 0;
                    this.editPic = 0;
                    this.editContact = 0;
                    this.editDocVerification = 0;
                    this.editPro = 0;
                    this.addUser = 0;
                    $scope.pannumberalpha = false;
                    $scope.pannumberlengthvalidation = false;
                    $scope.pannumberrequired = false;
                    $scope.taxnumberalpha = false;
                    $scope.taxnumberlengthvalidation = false;
                    $scope.taxnumberrequired = false;
                    $scope.tinnumberrequired = false;
                    $scope.getUserCredentials();
                    //userService.updateVerified(1);
                    swal("Done!", 'Your credentials are being verified. Our associates will contact you as soon as it is done.', 'success');
                    this.editDocVerification = 0;
                } else {
                    $log.info(response.data.errorMessage);
                }
            }, function (result) {
                $log.info(result);
            });
            this.editDocVerification = 0;
        };

        $scope.panObject = {};
        $scope.tinObject = {};
        $scope.stnObject = {};

        $scope.getFile1 = function (id, doctype, ext) {
            $scope.progress = 0;
            $scope.file = $("#" + id)[0].files[0];
            $scope.docType = doctype + "." + ext;
            fileReader.readAsDataUrl($scope.file, $scope)
                .then(function (result) {
                    if (id != "assocWithOEMFile" && id != 'storeLogo') {
                        var bytearray = new Uint8Array(result);
                        var fileobj = {};
                        fileobj.fileStream = $.makeArray(bytearray);
                        fileobj.fileType = $scope.docType;
                        fileobj.isVerified = 0;
                        //$scope.CredentialUpload.push(fileobj);
                        if (doctype == "PAN") {
                            $scope.panObject = fileobj;
                        }
                        else if (doctype == "TIN") {
                            $scope.tinObject = fileobj;
                        }
                        else if (doctype == "STN") {
                            $scope.stnObject = fileobj;
                        }
                    } if (id == "storeLogo") {
                        var bytearray = new Uint8Array(result);
                        $scope.logoFile.fileStream = $.makeArray(bytearray);
                        //$scope.bidAttachementName=$scope.file.name;
                        //$scope.formRequest.attachment=$scope.file.name;
                        $scope.logoFile.fileName = $scope.file.name;
                    } if (id == "profileFile") {
                        var bytearray = new Uint8Array(result);
                        $scope.userDetails.profileFile = $.makeArray(bytearray);
                        $scope.userDetails.profileFileName = $scope.file.name;
                    } else {
                        var bytearray = new Uint8Array(result);
                        $scope.userDetails.assocWithOEMFile = $.makeArray(bytearray);
                        //$scope.bidAttachementName=$scope.file.name;
                        //$scope.formRequest.attachment=$scope.file.name;
                        $scope.userDetails.assocWithOEMFileName = $scope.file.name;
                    }

                });
        };

        $scope.getFile2 = function () {
            $scope.progress = 0;
            $scope.file = $("#storeLogo")[0].files[0];
            if ($scope.file.type == "image/jpeg" || $scope.file.type == "image/jpg" || $scope.file.type == "image/png") {
                fileReader.readAsDataUrl($scope.file, $scope)
                    .then(function (result) {
                        var bytearray = new Uint8Array(result);
                        $scope.logoFile.fileStream = $.makeArray(bytearray);
                        $scope.logoFile.fileName = $scope.file.name;
                        this.imagefilesonlyforlogo = false;
                    });
            } else {
                this.imagefilesonlyforlogo = true;
            }
        };

        $scope.addnewuserobj = {};

        this.AddNewUser = function () {

            if (isNaN($scope.addnewuserobj.phoneNum)) {
                $scope.showMessage = true;
                $scope.msg = $scope.getErrMsg("Please enter a valid Phone Number");
            } else {
                $scope.showMessage = false;
                $scope.msg = '';
            }
            $scope.addnewuserobj.userType = userService.getUserType();
            $scope.addnewuserobj.username = $scope.addnewuserobj.email;
            $scope.addnewuserobj.companyName = $scope.userObj.institution;
            userService.addnewuser($scope.addnewuserobj)
                .then(function (response) {
                    if (response.errorMessage != "") {
                        growlService.growl(response.errorMessage, "inverse");
                    } else {
                        growlService.growl("User added successfully.", "inverse");
                        this.addUser = 0;
                        $state.reload();
                    }
                });

        };







    });prmApp
    .controller('remindersCtrl', function ($scope, $state, $stateParams, userService, auctionsService, fileReader) {
        $scope.id = $stateParams.Id;
        $scope.formRequest = {};

        $scope.sendMessageTo = '';

        $scope.listSelectedVendors = [
        ];

        $scope.selectedVendors = {
            vendorName: '',
            vendorIDs: 0
        };

        $scope.vendorIDs = [];
        $scope.reminderMessage = '';

        $scope.reminderMessageValidation = false;
        $scope.reminderVendorsValidation = true;

        $scope.getData = function () {
            auctionsService.getrequirementdata({ "reqid": $stateParams.Id, "sessionid": userService.getUserToken(), 'userid': userService.getUserId() })
                .then(function (response) {
                    $scope.auctionItem = response;

                    if ($scope.auctionItem.poLink != '') {
                        auctionsService.getpodetails({ "reqid": $stateParams.Id, "sessionid": userService.getUserToken(), 'userid': userService.getUserId() })
                            .then(function (response) {
                                $scope.formRequest = response;
                            })
                    }
                })
        }

        $scope.getData();

        $scope.getVendors = function () {
            $scope.reminderVendorsValidation = false;
            $scope.listSelectedVendors = [];
            $scope.vendorIDs = [];
            $scope.vendorCompanyNames = [];

            if ($scope.sendMessageTo == 'approved') {
                $scope.auctionItem.auctionVendors.forEach(function (item, index) {
                    $scope.selectedVendors = {};
                    if (item.quotationUrl != '' && item.isQuotationRejected == 0) {
                        $scope.vendorIDs.push(item.vendorID);
                        $scope.vendorCompanyNames.push(item.companyName);
                        $scope.selectedVendors.vendorID = item.vendorID;
                        $scope.selectedVendors.vendorName = item.companyName;
                        $scope.listSelectedVendors.push($scope.selectedVendors);
                    }
                })
            }

            if ($scope.sendMessageTo == 'rejected') {
                $scope.auctionItem.auctionVendors.forEach(function (item, index) {
                    $scope.selectedVendors = {};
                    if (item.quotationUrl != '' && item.isQuotationRejected == 1) {
                        $scope.vendorIDs.push(item.vendorID);
                        $scope.vendorCompanyNames.push(item.companyName);
                        $scope.selectedVendors.vendorID = item.vendorID;
                        $scope.selectedVendors.vendorName = item.companyName;
                        $scope.listSelectedVendors.push($scope.selectedVendors);
                    }
                })
            }

            if ($scope.sendMessageTo == 'specific') {
                $scope.auctionItem.auctionVendors.forEach(function (item, index) {
                    $scope.selectedVendors = {};
                    $scope.selectedVendors.vendorID = item.vendorID;
                    $scope.selectedVendors.vendorName = item.companyName;
                    $scope.listSelectedVendors.push($scope.selectedVendors);
                })
                $scope.vendorIDs = []
                $scope.vendorCompanyNames = []
            }

            if ($scope.sendMessageTo == 'notuploaded') {
                $scope.auctionItem.auctionVendors.forEach(function (item, index) {
                    $scope.selectedVendors = {};
                    if (item.quotationUrl == '' || item.quotationUrl == 0 || item.quotationUrl == undefined) {
                        $scope.vendorIDs.push(item.vendorID);
                        $scope.vendorCompanyNames.push(item.companyName);
                        $scope.selectedVendors.vendorID = item.vendorID;
                        $scope.selectedVendors.vendorName = item.companyName;
                        $scope.listSelectedVendors.push($scope.selectedVendors);
                    }
                })
            }

            if ($scope.sendMessageTo == 'all') {
                $scope.auctionItem.auctionVendors.forEach(function (item, index) {
                    $scope.selectedVendors = {};
                    $scope.vendorIDs.push(item.vendorID);
                    $scope.vendorCompanyNames.push(item.companyName);
                    $scope.selectedVendors.vendorID = item.vendorID;
                    $scope.selectedVendors.vendorName = item.companyName;
                    $scope.listSelectedVendors.push($scope.selectedVendors);
                })
            }
            //console.log($scope.vendorIDs);

            if ($scope.vendorIDs.length == 0) {
                $scope.reminderVendorsValidation = true;
            }
        }


        $scope.selectVendor = function (vendorID, companyName) {
            $scope.reminderVendorsValidation = false;

            var index = $scope.vendorIDs.indexOf(vendorID);
            var index = $scope.vendorCompanyNames.indexOf(companyName);

            if (index > -1) {
                $scope.vendorIDs.splice(index, 1);
                $scope.vendorCompanyNames.splice(index, 1);
            }
            else {
                $scope.vendorIDs.push(vendorID);
                $scope.vendorCompanyNames.push(companyName);
            }
            //console.log($scope.vendorIDs);

            if ($scope.vendorIDs.length == 0) {
                $scope.reminderVendorsValidation = true;
            }
        }


        $scope.vendorreminders = function () {
            $scope.reminderMessageValidation = false;
            $scope.reminderVendorsValidation = false;
            if ($scope.reminderMessage == '') {
                $scope.reminderMessageValidation = true;
            }
            if ($scope.vendorIDs.length == 0) {
                $scope.reminderVendorsValidation = true;
            }
            if ($scope.reminderMessageValidation || $scope.reminderVendorsValidation) {
                return;
            }
            var params = {
                reqID: $scope.auctionItem.requirementID,
                userID: userService.getUserId(),
                sessionID: userService.getUserToken(),
                message: $scope.reminderMessage,
                vendorIDs: $scope.vendorIDs,
                vendorCompanyNames: $scope.vendorCompanyNames
            }
            auctionsService.vendorreminders(params)
                .then(function (response) {
                    if (response.errorMessage == '') {
                        swal({
                            title: "Done!",
                            text: "Reminders sent Successfully.",
                            type: "success",
                            showCancelButton: false,
                            confirmButtonColor: "#DD6B55",
                            confirmButtonText: "Ok",
                            closeOnConfirm: true
                        },
                            function () {
                                //$state.go('reminders', { 'Id': response.objectID });
                                location.reload();
                            });
                    }
                    else {
                        swal("Error!", response.errorMessage, "error");
                    }
                })
        }

        $scope.getHistory = function () {
            auctionsService.GetVendorReminders({ "reqid": $stateParams.Id, "sessionid": userService.getUserToken(), 'userid': userService.getUserId() })
                .then(function (response) {
                    $scope.ListReminders = response;
                    $scope.ListReminders.forEach(function (item, index) {
                        item.sentOn = new moment(item.sentOn).format("DD-MM-YYYY HH:mm");
                    })
                })
        }

        $scope.getHistory();
    });﻿prmApp
    .controller('reqSearchCtrl', function ($scope, $state, $stateParams, userService, auctionsService) {


        $scope.CompanyLeads = {};

        $scope.searchstring = '';
        $scope.searchstype = 'TITLE';
        $scope.message = '';

        $scope.GetCompanyLeads = function () {
            $scope.message = '';
            if ($scope.searchstring.length < 3) {
                $scope.CompanyLeads = {};
                $scope.message = 'No Results';
            }
            if ($scope.searchstring.length > 2) {
                var params = { "userid": userService.getUserId(), "searchstring": $scope.searchstring, "searchtype": $scope.searchstype, "sessionid": userService.getUserToken() };
                auctionsService.GetCompanyLeads(params)
                    .then(function (response) {
                        $scope.CompanyLeads = response;

                        if (!$scope.CompanyLeads.length > 0) {
                            $scope.message = 'No Results';
                        }

                        $scope.CompanyLeads.forEach(function (item, index) {
                            item.postedOn = new moment(item.postedOn).format("DD-MM-YYYY HH:mm");
                        })
                    });
            }
        }

        $scope.GetCompanyLeads();

        if (!$scope.CompanyLeads.length > 0) {
            $scope.message = 'No Results';
        }

    });prmApp
    .controller('reqTechSupportCtrl', function ($scope, $state, $stateParams, $log, userService, auctionsService, fileReader, growlService) {


        $scope.message = 'Team, We have posted the following requirement in the system. Please train the vendors and get the quotations on time.'

        auctionsService.getrequirementdata({ "reqid": $stateParams.reqId, "sessionid": userService.getUserToken(), 'userid': userService.getUserId() })
            .then(function (response) {
                $scope.auctionItem = response;

                $scope.reqShare = {
                    requirement: $scope.auctionItem,
                    message: $scope.message,
                    expectedBidding: '',
                    minReductionAmount: $scope.auctionItem.minBidAmount,
                    vendorRankComparision: $scope.auctionItem.minVendorComparision,
                    negotiationDuration: $scope.auctionItem.negotiationDuration,
                    quotationFreezTime: $scope.auctionItem.quotationFreezTime
                };

                $scope.quotationFreezTime = moment($scope.auctionItem.quotationFreezTime).format('DD/MM/YYYY hh:mm');
        });

        

        $scope.reqTechSupport = function () {

            $scope.reqShare.sessionID = userService.getUserToken();

            var params = {
                reqShare: $scope.reqShare
            };

            auctionsService.reqTechSupport(params)                          
                .then(function (response) {
                    if (response.errorMessage == "") {
                        growlService.growl("Requirement Details have been sent Successfully to PRM360 Tech Support Team.", "success");;
                    }
                });

        };
        
    });prmApp

    .controller('resetPassCtrl', function ($scope, $http, $state, domain, $filter, $stateParams, $timeout, auctionsService, userService, SignalRFactory, fileReader, growlService) {
        $scope.resetpass = {};
        $scope.resetpass.email = $stateParams.email;
        $scope.resetpass.sessionid = $stateParams.sessionid;
        $scope.resetpass.OTP = '';
        $scope.resetpass.NewPass = '';
        $scope.resetpass.ConfNewPass = '';

        $scope.resetpassword = function () {
            userService.resetpassword($scope.resetpass)
                .then(function (response) {
                    if (response.data.errorMessage == '') {
                        swal("Done!", "Password reset successfully.", "success");
                    } else {
                        swal("Warning", response.data.errorMessage, "warning");
                        return;
                    }
                    //$state.go('login');
                    //return;
                });

        };
    });﻿prmApp
    .controller('savingsCtrl', function ($scope, $state, $stateParams, userService, auctionsService) {
        $scope.id = $stateParams.Id;
        $scope.totalItemsMinPrice = 0;
        $scope.totalLeastBidderPrice = 0;
        $scope.totalInitialPrice = 0;
        $scope.sessionid = userService.getUserToken();

        $scope.getData = function () {
            auctionsService.getpricecomparison({ "reqid": $stateParams.Id, "sessionid": userService.getUserToken(), 'userid': userService.getUserId() })
                .then(function (response) {
                    $scope.auctionItem = response.requirement;
                    $scope.priceCompObj = response;
                    //for (var i = 0; i < $scope.auctionItem.auctionVendors.length; i++) {
                    //    $scope.totalInitialPrice += $scope.auctionItem.auctionVendors[i].totalInitialPrice;
                    //}
                    //for (var i = 0; i < $scope.priceCompObj.priceCompareObject.length; i++) {
                    //    $scope.totalItemsMinPrice += $scope.priceCompObj.priceCompareObject[i].minPriceBidder;
                    //    $scope.totalLeastBidderPrice += $scope.priceCompObj.priceCompareObject[i].leastBidderPrice;
                    //}
                    $scope.totalItemsL1Price = 0;
                    $scope.totalItemsMinimunPrice = 0;
                    $scope.negotiationSavings = 0;
                    $scope.savingsByLeastBidder = 0;
                    $scope.savingsByItemMinPrice = 0;

                    $scope.L1CompanyName = '';
                    $scope.L1CompanyName = $scope.auctionItem.auctionVendors[0].companyName;


                    for (var i = 0; i < $scope.priceCompObj.priceCompareObject.length; i++) {
                        $scope.totalItemsL1Price += $scope.priceCompObj.priceCompareObject[i].leastBidderPrice;
                        $scope.totalItemsMinimunPrice += $scope.priceCompObj.priceCompareObject[i].minPrice;
                    }

                    $scope.negotiationSavings = $scope.auctionItem.savings;
                    $scope.savingsByLeastBidder = $scope.priceCompObj.minQuotationPrice - $scope.totalItemsL1Price;
                    $scope.savingsByItemMinPrice = $scope.priceCompObj.minQuotationPrice - $scope.totalItemsMinimunPrice;

                    $scope.additionalSavings = 0;

                    $scope.additionalSavings = $scope.totalItemsL1Price - $scope.totalItemsMinimunPrice;

                })
        }


        $scope.isReportGenerated = 0;

        $scope.GetReportsRequirement = function () {
            auctionsService.GetReportsRequirement({ "reqid": $stateParams.Id, "sessionid": userService.getUserToken(), 'userid': userService.getUserId() })
                .then(function (response) {
                    $scope.reports = response;
                    $scope.getData();
                    $scope.isReportGenerated = 1;
                });
        }

        $scope.getData();

    });﻿prmApp
    .controller('savingsPreNegotiationCtrl', function ($scope, $state, $stateParams, userService, auctionsService) {
        $scope.id = $stateParams.Id;
        $scope.totalItemsMinPrice = 0;
        $scope.totalLeastBidderPrice = 0;
        $scope.totalInitialPrice = 0;
        $scope.sessionid = userService.getUserToken();

        $scope.getData = function () {
            auctionsService.GetPriceComparisonPreNegotiation({ "reqid": $stateParams.Id, "sessionid": userService.getUserToken(), 'userid': userService.getUserId() })
                .then(function (response) {
                    $scope.auctionItem = response.requirement;
                    $scope.priceCompObj = response;
                    //for (var i = 0; i < $scope.auctionItem.auctionVendors.length; i++) {
                    //    $scope.totalInitialPrice += $scope.auctionItem.auctionVendors[i].totalInitialPrice;
                    //}
                    //for (var i = 0; i < $scope.priceCompObj.priceCompareObject.length; i++) {
                    //    $scope.totalItemsMinPrice += $scope.priceCompObj.priceCompareObject[i].minPriceBidder;
                    //    $scope.totalLeastBidderPrice += $scope.priceCompObj.priceCompareObject[i].leastBidderPrice;
                    //}
                    $scope.totalItemsL1Price = 0;
                    $scope.totalItemsMinimunPrice = 0;
                    $scope.negotiationSavings = 0;
                    $scope.savingsByLeastBidder = 0;
                    $scope.savingsByItemMinPrice = 0;

                    $scope.L1CompanyName = '';
                    $scope.L1CompanyName = $scope.auctionItem.auctionVendors[0].companyName;

                    if ($scope.auctionItem.auctionVendors[0].companyName == "PRICE_CAP") {
                        $scope.L1CompanyName = $scope.auctionItem.auctionVendors[1].companyName;
                    }

                    for (var i = 0; i < $scope.priceCompObj.priceCompareObject.length; i++) {
                        $scope.totalItemsL1Price += $scope.priceCompObj.priceCompareObject[i].leastBidderPrice;
                        $scope.totalItemsMinimunPrice += $scope.priceCompObj.priceCompareObject[i].minPrice;
                    }

                    $scope.negotiationSavings = $scope.auctionItem.savings;
                    $scope.savingsByLeastBidder = $scope.priceCompObj.minQuotationPrice - $scope.totalItemsL1Price;
                    $scope.savingsByItemMinPrice = $scope.priceCompObj.minQuotationPrice - $scope.totalItemsMinimunPrice;

                    $scope.additionalSavings = 0;

                    $scope.additionalSavings = $scope.totalItemsL1Price - $scope.totalItemsMinimunPrice;

                })
        }


        $scope.isReportGenerated = 0;

        $scope.GetReportsRequirement = function () {
            auctionsService.GetReportsRequirement({ "reqid": $stateParams.Id, "sessionid": userService.getUserToken(), 'userid': userService.getUserId() })
                .then(function (response) {
                    $scope.reports = response;
                    $scope.getData();
                    $scope.isReportGenerated = 1;
                });
        }

        $scope.getData();

    });﻿prmApp.controller('pipeCtrl', ['Resource', function (service) {

    var ctrl = this;

    this.displayed = [];

    this.callServer = function callServer(tableState) {

        ctrl.isLoading = true;

        var pagination = tableState.pagination;

        var start = pagination.start || 0;     // This is NOT the page number, but the index of item in the list that you want to use to display the table.
        var number = pagination.number || 10;  // Number of entries showed per page.

        service.getPage(start, number, tableState).then(function (result) {
            ctrl.displayed = result.data;
            tableState.pagination.numberOfPages = result.numberOfPages; //set the number of pages so the pagination can update
            ctrl.isLoading = false;
        });
    };

}]);prmApp
    .controller('tableCtrl', function ($filter, $sce, ngTableParams, tableService) {
        var data = tableService.data;

        //Basic Example
        this.tableBasic = new ngTableParams({
            page: 1,            // show first page
            count: 10           // count per page
        }, {
                total: data.length, // length of data
                getData: function ($defer, params) {
                    $defer.resolve(data.slice((params.page() - 1) * params.count(), params.page() * params.count()));
                }
            })

        //Sorting
        this.tableSorting = new ngTableParams({
            page: 1,            // show first page
            count: 10,           // count per page
            sorting: {
                name: 'asc'     // initial sorting
            }
        }, {
                total: data.length, // length of data
                getData: function ($defer, params) {
                    // use build-in angular filter
                    var orderedData = params.sorting() ? $filter('orderBy')(data, params.orderBy()) : data;

                    $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
                }
            })

        //Filtering
        this.tableFilter = new ngTableParams({
            page: 1,            // show first page
            count: 10
        }, {
                total: data.length, // length of data
                getData: function ($defer, params) {
                    // use build-in angular filter
                    var orderedData = params.filter() ? $filter('filter')(data, params.filter()) : data;

                    this.id = orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count());
                    this.name = orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count());
                    this.email = orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count());
                    this.username = orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count());
                    this.contact = orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count());

                    params.total(orderedData.length); // set total for recalc pagination
                    $defer.resolve(this.id, this.name, this.email, this.username, this.contact);
                }
            })

        //Editable
        this.tableEdit = new ngTableParams({
            page: 1,            // show first page
            count: 10           // count per page
        }, {
                total: data.length, // length of data
                getData: function ($defer, params) {
                    $defer.resolve(data.slice((params.page() - 1) * params.count(), params.page() * params.count()));
                }
            });
    });prmApp

    //====================================
    // ALERT
    //====================================

    .controller('AlertDemoCtrl', function ($scope) {
        $scope.alerts = [
            { type: 'info', msg: "Well done! You successfully read this important alert message." },
            { type: 'success', msg: "Well done! You successfully read this important alert message." },
            { type: 'warning', msg: "Warning! Better check yourself, you're not looking too good." },
            { type: 'danger', msg: "Oh snap! Change a few things up and try submitting again." }
        ];

        $scope.closeAlert = function (index) {
            $scope.alerts.splice(index, 1);
        };
    })


    //====================================
    // BUTTONS
    //====================================

    .controller('ButtonsDemoCtrl', function ($scope) {
        $scope.singleModel = 1;

        $scope.radioModel = 'Middle';

        $scope.checkModel = {
            left: false,
            middle: true,
            right: false
        };
    })


    //====================================
    // CAROUSEL
    //====================================

    .controller('CarouselDemoCtrl', function ($scope) {
        $scope.myInterval = 0;
        $scope.slides = [
            {
                img: 'c-1.jpg',
                title: 'First Slide Label',
                text: 'Some sample text goes here...'
            },
            {
                img: 'c-2.jpg',
                title: 'Second Slide Label',
                text: 'Some sample text goes here...'
            },
            {
                img: 'c-3.jpg'
            }
        ];

    })


    //====================================
    // CAROUSEL
    //====================================

    .controller('CollapseDemoCtrl', function ($scope) {
        $scope.isCollapsed = false;
    })


    //====================================
    // DROPDOWN
    //====================================

    .controller('UibDropdownDemoCtrl', function ($scope) {
        $scope.items = [
            { name: 'The first choice!', icon: 'home' },
            { name: 'And another choice', icon: 'account' },
            { name: 'But wait! A third!', icon: 'email' },
            { name: 'And fourth on here', icon: 'pin' }
        ];
    })


    //====================================
    // MODAL
    //====================================
    .controller('ModalDemoCtrl', function ($scope, $uibModal, $log) {

        $scope.modalContent = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin sodales orci ante, sed ornare eros vestibulum ut. Ut accumsan vitae eros sit amet tristique. Nullam scelerisque nunc enim, non dignissim nibh faucibus ullamcorper. Fusce pulvinar libero vel ligula iaculis ullamcorper. Integer dapibus, mi ac tempor varius, purus nibh mattis erat, vitae porta nunc nisi non tellus. Vivamus mollis ante non massa egestas fringilla. Vestibulum egestas consectetur nunc at ultricies. Morbi quis consectetur nunc.';

        //Create Modal
        function modalInstances(animation, size, backdrop, keyboard) {
            var modalInstance = $uibModal.open({
                animation: animation,
                templateUrl: 'myModalContent.html',
                controller: 'ModalInstanceCtrl',
                size: size,
                backdrop: backdrop,
                keyboard: keyboard,
                resolve: {
                    content: function () {
                        return $scope.modalContent;
                    }
                }

            });
        }

        //Custom Sizes
        $scope.open = function (size) {
            modalInstances(true, size, true, true)
        }

        //Without Animation
        $scope.openWithoutAnimation = function () {
            modalInstances(false, '', true, true)
        }

        //Prevent Outside Click
        $scope.openStatic = function () {
            modalInstances(true, '', 'static', true)
        };

        //Disable Keyboard
        $scope.openKeyboard = function () {
            modalInstances(true, '', true, false)
        };

    })

    // Please note that $uibModalInstance represents a modal window (instance) dependency.
    // It is not the same as the $modal service used above.

    .controller('ModalInstanceCtrl', function ($scope, $uibModalInstance, content) {

        $scope.modalContent = content;

        $scope.ok = function () {
            $uibModalInstance.close();
        };

        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };
    })


    //====================================
    // PAGINATION
    //====================================

    .controller('PaginationDemoCtrl', function ($scope, $log) {
        $scope.totalItems = 64;
        $scope.currentPage = 4;

        $scope.setPage = function (pageNo) {
            $scope.currentPage = pageNo;
        };

        $scope.maxSize = 5;
        $scope.bigTotalItems = 175;
        $scope.bigCurrentPage = 1;
    })


    //====================================
    // POPOVER
    //====================================

    .controller('PopoverDemoCtrl', function ($scope) {
        $scope.dynamicPopover = {
            templateUrl: 'myPopoverTemplate.html',
        };
    })

    //====================================
    // PROGRESSBAR
    //====================================

    .controller('ProgressDemoCtrl', function ($scope) {
        $scope.max = 200;

        $scope.random = function () {
            var value = Math.floor((Math.random() * 100) + 1);
            var type;

            if (value < 25) {
                type = 'success';
            }
            else if (value < 50) {
                type = 'info';
            }
            else if (value < 75) {
                type = 'warning';
            }
            else {
                type = 'danger';
            }

            $scope.showWarning = (type === 'danger' || type === 'warning');

            $scope.dynamic = value;
            $scope.type = type;
        };

        $scope.random();

        $scope.randomStacked = function () {
            $scope.stacked = [];
            var types = ['success', 'info', 'warning', 'danger'];

            for (var i = 0, n = Math.floor((Math.random() * 4) + 1); i < n; i++) {
                var index = Math.floor((Math.random() * 4));
                $scope.stacked.push({
                    value: Math.floor((Math.random() * 30) + 1),
                    type: types[index]
                });
            }
        };

        $scope.randomStacked();
    })


    //====================================
    // TABS
    //====================================

    .controller('TabsDemoCtrl', function ($scope, $window) {
        $scope.tabs = [
            {
                title: 'Category-1',
                content: 'In hac habitasse platea dictumst. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos hymenaeos. Nam eget dui. In ac felis quis tortor malesuada pretium. Phasellus consectetuer vestibulum elit. Duis lobortis massa imperdiet quam. Pellentesque commodo eros a enim. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; In ac dui quis mi consectetuer lacinia. Phasellus a est. Pellentesque commodo eros a enim. Cras ultricies mi eu turpis hendrerit fringilla. Donec mollis hendrerit risus. Vestibulum turpis sem, aliquet eget, lobortis pellentesque, rutrum eu, nisl. Praesent egestas neque eu enim. In hac habitasse platea dictumst.'
            },
            {
                title: 'Category-2',
                content: 'Duis arcu tortor, suscipit eget, imperdiet nec, imperdiet iaculis, ipsum. Vestibulum purus quam, scelerisque ut, mollis sed, nonummy id, metus. Nulla sit amet est. Praesent ac massa at ligula laoreet iaculis. Vivamus aliquet elit ac nisl. Nulla porta dolor. Cras dapibus. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus.',
            },
            {
                title: 'Category-3',
                content: 'Etiam rhoncus. Phasellus leo dolor, tempus non, auctor et, hendrerit quis, nisi. Cras id dui. Curabitur turpis. Etiam ut purus mattis mauris sodales aliquam. Aenean viverra rhoncus pede. Nulla sit amet est. Donec mi odio, faucibus at, scelerisque quis, convallis in, nisi. Praesent ac sem eget est egestas volutpat. Cras varius. Morbi mollis tellus ac sapien. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nam ipsum risus, rutrum vitae, vestibulum eu, molestie vel, lacus. Fusce vel dui.Morbi mattis ullamcorper velit. Etiam rhoncus. Phasellus leo dolor, tempus non, auctor et, hendrerit quis, nisi. Cras id dui. Curabitur turpis. Etiam ut purus mattis mauris sodales aliquam. Aenean viverra rhoncus pede. Nulla sit amet est. Donec mi odio, faucibus at, scelerisque quis, convallis in, nisi. Praesent ac sem eget est egestas volutpat. Cras varius. Morbi mollis tellus ac sapien. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nam ipsum risus, rutrum vitae, vestibulum eu, molestie vel, lacus. Fusce vel dui.',
            },
            {
                title: 'Category-4',
                content: 'Praesent turpis. Phasellus magna. Fusce vulputate eleifend sapien. Duis arcu tortor, suscipit eget, imperdiet nec, imperdiet iaculis, ipsum. Donec elit libero, sodales nec, volutpat a, suscipit non, turpis.',
            }
        ];

    })


    //====================================
    // TOOLTIPS
    //====================================

    .controller('TooltipDemoCtrl', function ($scope, $sce) {
        $scope.htmlTooltip = $sce.trustAsHtml('I\'ve been made <b>bold</b>!');
    })


    //====================================
    // DATE PICKER
    //====================================
    .controller('DatepickerDemoCtrl', function ($scope) {
        $scope.today = function () {
            $scope.dt = new Date();
        };
        $scope.today();


        $scope.toggleMin = function () {
            $scope.minDate = $scope.minDate ? null : new Date();
        };
        $scope.toggleMin();

        $scope.open = function ($event, opened) {
            $event.preventDefault();
            $event.stopPropagation();

            $scope[opened] = true;
        };

        $scope.dateOptions = {
            formatYear: 'yy',
            startingDay: 1
        };

        $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
        $scope.format = $scope.formats[0];
    })



    //====================================
    // TYPEAHEAD
    //====================================
    .controller('TypeaheadCtrl', function ($scope, $http) {

        $scope.selected = undefined;
        $scope.states = [
            'Alabama',
            'Alaska',
            'Arizona',
            'Arkansas',
            'California',
            'Colorado',
            'Connecticut',
            'Delaware',
            'Florida',
            'Georgia',
            'Hawaii',
            'Idaho',
            'Illinois',
            'Indiana',
            'Iowa',
            'Kansas',
            'Kentucky',
            'Louisiana',
            'Maine',
            'Maryland',
            'Massachusetts',
            'Michigan',
            'Minnesota',
            'Mississippi',
            'Missouri',
            'Montana',
            'Nebraska',
            'Nevada',
            'New Hampshire',
            'New Jersey',
            'New Mexico',
            'New York',
            'North Dakota',
            'North Carolina',
            'Ohio',
            'Oklahoma',
            'Oregon',
            'Pennsylvania',
            'Rhode Island',
            'South Carolina',
            'South Dakota',
            'Tennessee',
            'Texas',
            'Utah',
            'Vermont',
            'Virginia',
            'Washington',
            'West Virginia',
            'Wisconsin',
            'Wyoming'
        ];

        // Any function returning a promise object can be used to load values asynchronously
        $scope.getLocation = function (val) {
            return $http.get('//maps.googleapis.com/maps/api/geocode/json', {
                params: {
                    address: val,
                    sensor: false
                }
            }).then(function (response) {
                return response.data.results.map(function (item) {
                    return item.formatted_address;
                });
            });
        }
    });prmApp

    .controller('viewprofileCtrl', function ($scope, $http, $state, domain, $filter, $stateParams, $timeout, auctionsService, userService, SignalRFactory, fileReader, growlService) {
        $scope.userId = $stateParams.Id;
        $scope.userObj = {};
        this.userdetails = {};

        $scope.subcategories = '';

        $scope.callGetUserDetails = function () {
            userService.getProfileDetails({ "userid": userService.getUserId(), "sessionid": userService.getUserToken() })
                .then(function (response) {
                    $scope.userStatus = "registered";
                    if (response != undefined) {
                        $scope.userDetails = response;
                        $http({
                            method: 'GET',
                            url: domain + 'getcategories?userid=' + userService.getUserId() + '&sessionid=' + userService.getUserToken(),
                            encodeURI: true,
                            headers: { 'Content-Type': 'application/json' }
                        }).then(function (response) {
                            if (response && response.data) {
                                if (response.data.length > 0) {
                                    $scope.totalSubcats = $filter('filter')(response.data, { category: $scope.userDetails.category });
                                    for (i = 0; i < $scope.userDetails.subcategories.length; i++) {
                                        for (j = 0; j < $scope.totalSubcats.length; j++) {
                                            if ($scope.userDetails.subcategories[i].id == $scope.totalSubcats[j].id) {
                                                $scope.totalSubcats[j].ticked = true;
                                            }
                                        }
                                    }
                                }
                            } else {
                                //console.log(response.data[0].errorMessage);
                            }
                        }, function (result) {
                            //console.log("there is no current auctions");
                        });
                        for (i = 0; i < $scope.userDetails.subcategories.length; i++) {
                            $scope.subcategories += $scope.userDetails.subcategories[i].subcategory + ";";
                        }
                        var data = response.establishedDate;
                        var date = new Date(parseInt(data.substr(6)));
                        $scope.userDetails.establishedDate = date.getDate() + '/' + (date.getMonth() + 1) + '/' + date.getFullYear();

                        var today = new Date();
                        var todayDate = today.getDate() + '/' + (today.getMonth() + 1) + '/' + today.getFullYear();
                        $scope.userDetails.dateshow = 0;
                        if ($scope.userDetails.establishedDate == todayDate) {
                            $scope.userDetails.dateshow = 1;
                        }

                        if (response.registrationScore > 89) {

                            $scope.userStatus = "Authorised";

                        }
                    }
                });
        }


        $scope.callGetUserDetails();
        /*this.getuserdetails = function () {*/

        /*getuserdetails*/
        userService.getProfileDetails({ "userid": $scope.userId, "sessionid": userService.getUserToken() })
            .then(function (response) {
                $scope.userObj = response;
                var data = response.establishedDate;
                var date = new Date(parseInt(data.substr(6)));
                $scope.userObj.establishedDate = date.getDate() + '/' + (date.getMonth() + 1) + '/' + date.getFullYear();
                for (i = 0; i < $scope.userObj.subcategories.length; i++) {
                    $scope.subcategories += $scope.userObj.subcategories[i].subcategory + ";";
                }
            }
            );

        /*getuserinfo*/
        userService.getUserDataNoCache()
            .then(function (response) {
                $scope.userdetails = response;
            })
    });﻿prmApp
    .controller('addnewstoreCtrl', function ($scope, $stateParams, $state, $window, $log, userService, storeService, growlService) {
        $scope.storeID = $stateParams.storeID == "" ? 0 : $stateParams.storeID;
        $scope.compID = userService.getUserCompanyId();
        $scope.companyStores = [];
        $scope.isValidToSubmit = true;

        $log.info($scope.companyStores);
        storeService.getcompanystores($scope.compID, 0)
            .then(function (response) {
                $scope.companyStores = response;
            });

        $scope.companyID = userService.getUserCompanyId();
        $scope.sessionID = userService.getUserToken();

        $scope.storeObj = {
            storeID: $scope.storeID,
            companyID: $scope.companyID,
            //isMainBranch: 0,
            mainStoreID: 0,
            //storeCode: '',
            //storeDescription: '',
            //storeInCharge: '',
            //storeDetails: '',
            //isValid: 0,
            //storeAddress: '',
            //storeBranches: 0,
            sessionID: $scope.sessionID
        };

        $scope.goToBranches = function (store) {
            var url = $state.href('branches', { "storeID": store.storeID });
            $window.open(url, '_blank');
            //$state.go("branches", { "storeID": store.storeID });
        }

        if ($scope.storeID > 0) {            
            if (!$stateParams.storeObj) {
                storeService.getstores($scope.storeID, $scope.companyID)
                    .then(function (response) {
                        $scope.storeObj = response[0];
                    });
            }
            else {
                $scope.storeObj = $stateParams.storeObj;
            }            
        }

        


        $scope.saveStore = function () {
            validateData($scope.storeObj);
            if (!$scope.isValidToSubmit) {
                growlService.growl("Please fill all required fields marked (*).", "inverse");
                return false;
            }

            $scope.storeObj.isValid = 1;

            var params = {
                "store": $scope.storeObj
            };

            storeService.savestore(params)
               .then(function (response) {
                   if (response.errorMessage != '') {
                       growlService.growl(response.errorMessage, "inverse");
                   }
                   else {
                       growlService.growl("Store Saved Successfully.", "success");
                       $window.history.back();
                   }
               })
        }

        function validateData(storeObj) {
            if (storeObj.storeCode == null || storeObj.storeCode == "") {
                $scope.isValidToSubmit = false;
            }
            else if (storeObj.storeInCharge == null || storeObj.storeInCharge == "") {
                $scope.isValidToSubmit = false;
            }
            else if(storeObj.storeDescription == null || storeObj.storeDescription == "") {
                $scope.isValidToSubmit = false;
            }
            else if (storeObj.storeDetails == null || storeObj.storeDetails == "") {
                $scope.isValidToSubmit = false;
            }
            else if (storeObj.storeAddress == null || storeObj.storeAddress == "") {
                $scope.isValidToSubmit = false;
            }
            else {
                $scope.isValidToSubmit = true;
            }
        }

});﻿prmApp
    .controller('addnewstoreitemCtrl', function ($scope, $stateParams, $window, $log, userService, storeService, growlService) {
        $scope.storeID = $stateParams.storeID;
        $scope.itemID = $stateParams.itemID;
        $scope.companyID = userService.getUserCompanyId();
        $scope.sessionID = userService.getUserToken();
        $scope.uniqueItemCodes = [];
        $scope.uniqueItemNames = [];
        $scope.initialInStock = 0;
        $scope.initialOnOrder = 0;
        $scope.initialTotalStock = 0;
        $scope.isValidToSubmit = true;
       
        $scope.storeObj = {
            storeID: $scope.storeID,
            companyID: $scope.companyID,
            sessionID: $scope.sessionID
        };

        $scope.showIsSuperUser = function () {
            if (userService.getUserObj().isSuperUser) {
                return true;
            } else {
                return false;
            }
        }

        $scope.storeItemsObj = {
            itemID: $scope.itemID,
            inStock: 0,
            onOrder: 0,
            itemPrice: 0,
            totalStock : 0,
            storeDetails: $scope.storeObj,
            doAlert: 0,
            threshold: 0,
            iGST:0,
            sessionID: $scope.sessionID
        };

        $scope.getCompanyUniqueItems = function (itemName) {
            storeService.getuniqueitems($scope.companyID, itemName)
                .then(function (response) {
                    if (itemName == '')
                    {
                        $scope.uniqueItemNames = response;
                    }
                    else
                    {
                        $scope.uniqueItemCodes = response;
                    }                    
                });
        };

        $scope.getCompanyUniqueItems('');

        $scope.selectItemCode = function (uniqueItem, currentItemObj) {
            currentItemObj.itemCode = uniqueItem.itemCode;
        }

        $scope.selectItemName = function (uniqueItemName, currentItemObj) {
            currentItemObj.itemName = uniqueItemName.itemName;
            //$log.info(uniqueItemName);
            $scope.getCompanyUniqueItems(uniqueItemName.itemName);
        }

        $scope.taxClassification = [
                   { name: 'Cement - 28 %', tax: 28 },
                   { name: 'Steel - 18 %', tax: 18 },
                   { name: 'River Sand - 5 %', tax: 5 },
                   { name: 'Crush Sandg - 5 %', tax: 5 },
                   { name: 'Aggregate - 5 %', tax: 5 },
                   { name: 'Marble and Granite- 12 - 28 %', tax: 28 },
                   { name: 'Stone - 5 %', tax: 5 },
                   { name: 'Red Bricks- 5 %', tax: 5 },
                   { name: 'Fly Ash Bricks- 12 %', tax: 12 },
                   { name: 'Cement Block - 18 %', tax: 18 },
                   { name: 'Glass - 28 %', tax: 28 },
                   { name: 'Shahabad - 5 %', tax: 5 },
                   { name: 'Tiles - 18 - 28 %', tax: 28 },
                   { name: 'Wire and Cable - 28 %', tax: 28 },
                   { name: 'Paint and Varnish - 28 %', tax: 28 },
                   { name: 'Sanatary Fittings - 28 %', tax: 28 },
                   { name: 'GI Fittings - 18 %', tax: 18 },
                   { name: 'CP Fittings - 18 - 28 % (Depends on Base Material)', tax: 28 },
                   { name: 'Wallpapers - 28 %', tax: 28 },
                   { name: 'Key Locks - 18 %', tax: 18 },
                   { name: 'Wooden Door and window - 28 %', tax: 28 },
                   { name: 'Aluminium Windows - 18 - 28 %', tax: 28 },
        ];


        if ($scope.itemID > 0)
        {
            if (!$stateParams.itemObj) {
                storeService.getcompanystoreitems($scope.storeID, $scope.itemID)
              .then(function (response) {
                  $scope.storeItemsObj = response[0];
                  $scope.storeItemsObj.sessionID = $scope.sessionID;
              });
            }
            else {
                $scope.storeItemsObj = $stateParams.itemObj;
                $scope.storeItemsObj.sessionID = $scope.sessionID;
            }

            //$scope.initialInStock = $scope.storeItemsObj.inStock;
            //$scope.initialOnOrder = $scope.storeItemsObj.onOrder;
            //$scope.initialTotalStock = $scope.storeItemsObj.totalStock;
        }
        else {
            $scope.storeItemsObj.totalStock = $scope.inStock + $scope.onOrder;
        }

        $scope.$watch('storeItemsObj.inStock', function (newValue, oldValue, scope) {
            $scope.storeItemsObj.totalStock = $scope.storeItemsObj.inStock + $scope.storeItemsObj.onOrder;
            //if (!$scope.storeItemsObj.totalStock || isNaN($scope.storeItemsObj.totalStock))
            //{
            //    $scope.storeItemsObj.totalStock = 0;
            //}

            //$scope.storeItemsObj.totalStock = $scope.initialTotalStock + (newValue - $scope.initialInStock) + $scope.storeItemsObj.onOrder;
        }, true);

        $scope.$watch('storeItemsObj.onOrder', function (newValue, oldValue, scope) {
            $scope.storeItemsObj.totalStock = $scope.storeItemsObj.inStock + $scope.storeItemsObj.onOrder;
            //if (!$scope.storeItemsObj.totalStock || isNaN($scope.storeItemsObj.totalStock)) {
            //    $scope.storeItemsObj.totalStock = 0;
            //}

            //$scope.storeItemsObj.totalStock = $scope.initialTotalStock + (newValue - $scope.initialOnOrder) + $scope.storeItemsObj.inStock;
        }, true);
        


        $scope.saveStoreItem = function () {
            validateData($scope.storeItemsObj);
            if (!$scope.isValidToSubmit)
            {
                growlService.growl("Please fill all required fields marked (*).", "inverse");
                return false;
            }

            if (!$scope.storeItemsObj.itemCode);
            {
                $scope.storeItemsObj.itemCode = "";
            }

            $scope.storeItemsObj.isValid = 1;

            var params = {
                "item": $scope.storeItemsObj
            };

            storeService.savestoreitem(params)
               .then(function (response) {
                   if (response.errorMessage != '') {
                       growlService.growl(response.errorMessage, "inverse");
                   }
                   else {
                       growlService.growl("Item Details Saved Successfully.", "success");
                       $window.history.back();
                   }
               })
        }

        $scope.itemTax = 0;

        $scope.getTaxes = function(tax)
        {            
            $scope.storeItemsObj.cGST = tax / 2;
            $scope.storeItemsObj.sGST = tax / 2
        }

        function validateData(storeItemsObj)
        {
            //if (storeItemsObj.itemCode == null || storeItemsObj.itemCode == ""){
            //    $scope.isValidToSubmit = false;
            //}
            if (storeItemsObj.itemName == null || storeItemsObj.itemName == "") {
                $scope.isValidToSubmit = false;
            }
            else if (storeItemsObj.itemType == null || storeItemsObj.itemType == "") {
                $scope.isValidToSubmit = false;
            }
            else if (storeItemsObj.itemPrice == null || storeItemsObj.itemPrice == "") {
                $scope.isValidToSubmit = false;
            }
            else if (storeItemsObj.cGST == null || storeItemsObj.cGST == "") {
                $scope.isValidToSubmit = false;
            }
            else if (storeItemsObj.sGST == null || storeItemsObj.sGST == "") {
                $scope.isValidToSubmit = false;
            }
            //else if (storeItemsObj.iGST == null || storeItemsObj.iGST == "") {
            //    $scope.isValidToSubmit = false;
            //}
            else if (storeItemsObj.inStock == null || storeItemsObj.inStock == "" || storeItemsObj.inStock == "0") {
                $scope.isValidToSubmit = false;
            }
            else if (storeItemsObj.onOrder == null || storeItemsObj.onOrder == "" || storeItemsObj.onOrder == "0") {
                $scope.isValidToSubmit = false;
            }
            else if (storeItemsObj.itemUnits == null || storeItemsObj.itemUnits == "") {
                $scope.isValidToSubmit = false;
            }
            else if (storeItemsObj.itemDescription == null || storeItemsObj.itemDescription == "") {
                $scope.isValidToSubmit = false;
            }
            else
            {
                $scope.isValidToSubmit = true;
            }
        }

    });﻿prmApp
    .controller('storeginCtrl', function ($scope, $state, $stateParams, $window, $log, userService, storeService, fileReader, growlService) {
        $scope.storeID = $stateParams.storeID;
        $scope.selectedStoreItems = [];
        $scope.sessionID = userService.getUserToken();
        $scope.companyStoreItems = [];
        $scope.companyStoreItemsTemp = [];
        $scope.storeItemSerialNos = [];
        $scope.storeItemSerialNosSelected = {};
        $scope.currentStoreItemID = 0;

        $scope.phoneError = false;
        $scope.generateGinError = false;
        $scope.issueDateError = false;

        var GenRandom = {

            Stored: [],

            Job: function () {
                var newId = Date.now().toString().substr(6); // or use any method that you want to achieve this string

                if (!this.Check(newId)) {
                    this.Stored.push(newId);
                    return newId;
                }

                return this.Job();
            },

            Check: function (id) {
                for (var i = 0; i < this.Stored.length; i++) {
                    if (this.Stored[i] == id) return true;
                }
                return false;
            }

        };

        $scope.ginObject = {
            ginID: 0,
            ginCode : "GIN-" + GenRandom.Job(),
            shipState: "",
            shipCountry: "",
            shipZip: "",
            orderQty: 0,
            shipQty: 0,
            sessionID: $scope.sessionID,
            modifiedBy: userService.getUserId()
        };

        $scope.searchKey = '';

        if ($stateParams.itemsArr) {
            $scope.companyStoreItems = $stateParams.itemsArr;
            if ($scope.companyStoreItems.length > 0) {
                $scope.ginObject.storeItemDetails = $scope.companyStoreItems;
            }
        }

        $scope.cancelClick = function () {
            $window.history.back();
        }

        $scope.generateGin = function () {

            $scope.generateGinValidations();

            if (!$scope.generateGinError) {

                if ($scope.companyStoreItems.length > 0) {
                    
                    
                    var savedItems = 0;
                    $.each($scope.ginObject.storeItemDetails, function (key, item) {
                        item.inStock = item.shipQty;
                        item.totalStock = item.orderQty;
                    });

                    var ts = moment($scope.ginObject.issueDate, "DD-MM-YYYY HH:mm").valueOf();
                    var m = moment(ts);
                    var issueDate = new Date(m);
                    var milliseconds = parseInt(issueDate.getTime() / 1000.0);
                    $scope.ginObject.issueDate = "/Date(" + milliseconds + "000+0530)/";

                    var params = {
                        "item": $scope.ginObject
                    };

                    storeService.saveStoreItemGIN(params)
                        .then(function (response) {
                            if (response.errorMessage != '') {
                                growlService.growl(response.errorMessage, "inverse");
                            }
                            else {
                                growlService.growl("GIN details saved successfully.", "success");
                                $window.history.back();
                            }
                        });
                }
            }
        }

        $scope.itemDetailsSelect = function (itemDetails) {
            $scope.storeItemSerialNosSelected[itemDetails.itemID] = _.filter($scope.storeItemSerialNos[itemDetails.itemID], function (item) {
                return item.isSelected == true;
            });

            $.each($scope.ginObject.storeItemDetails, function (key, item) {
                if (item.itemID == itemDetails.itemID)
                {
                    item.itemDetails = $scope.storeItemSerialNosSelected[item.itemID];
                }
            });
        };

        $scope.handleDate = function (date) {
            return new moment(date).format("DD-MMM-YYYY");
        }

        $scope.selectSerialNo = function (storeItemID) {
            $scope.currentStoreItemID = storeItemID;
            if ($scope.storeItemSerialNos[storeItemID] === undefined || $scope.storeItemSerialNos[storeItemID].length <= 0) {
                storeService.getStoreItemDetails(storeItemID, 0)
                    .then(function (response) {
                        $scope.storeItemSerialNos[storeItemID] = response;
                    });
            }
        }

        $scope.search = function () {
            $scope.companyStoreItemsTemp = _.filter($scope.companyStoreItems, function (item) {
                return item.itemCode.indexOf($scope.searchKey) > -1;
            });
        }

        $scope.companyStoreItem = {};

        $scope.getCompanyStoreItem = function (itemID) {
            $scope.companyStoreItem = _.filter($scope.companyStoreItems, function (x) { return x.itemID == itemID; });
            $scope.singleItemDetails = $scope.companyStoreItem[0];
            return $scope.singleItemDetails;
        }
        
        $scope.generateGinValidations = function () {

            $scope.phoneError = false;
            $scope.generateGinError = false;
            $scope.issueDateError = false;

            if ($scope.ginObject.phone == undefined) {
                $scope.phoneError = true;
                $scope.generateGinError = true;
            }
            if ($scope.ginObject.issueDate == undefined || $scope.ginObject.issueDate == '') {
                $scope.issueDateError = true;
                $scope.generateGinError = true;
            }
            
        };

    }); 

﻿prmApp
    .controller('storegindetailsCtrl', function ($scope, $state, $stateParams, $window, $log, $timeout, userService, storeService, fileReader, growlService) {
        $scope.storeID = $stateParams.storeID;
        $scope.ginCode = $stateParams.ginCode;
        $scope.ginObject = $stateParams.ginObj;
        $scope.sessionID = userService.getUserToken();
        $scope.storeItemGINItemDetails = [];
        $scope.indentItemDetails = [];
        $scope.indentDetails = [];
        $scope.doPrint = false;

        $scope.handleDate = function (date) {
            return new moment(date).format("DD-MMM-YYYY");
        }

        $scope.cancelClick = function () {
            $window.history.back();
        }

        $scope.printReport = function () {
            $scope.doPrint = true;
            $timeout(function () {
                $window.print();
                $scope.doPrint = false
            }, 1000);
        }

        $scope.selectGinItemDetails = function (ginDetails) {
            if ($scope.indentDetails.length <= 0)
            {
                storeService.getIndentItemDetails($scope.storeID, $scope.ginCode, '')
                .then(function (response) {
                    $scope.indentDetails = response;
                });
            }
            else {
                $scope.indentItemDetails = _.filter($scope.indentDetails, function (item) {
                    return item.itemID == ginDetails.itemID;
                });
            }
        }

        $scope.getCompanyStoreItemsGIN = function () {
            storeService.getStoreItemGIN($scope.ginCode, $scope.storeID)
                .then(function (response) {
                    $scope.storeItemGINItemDetails = response[0];
                });

            storeService.getIndentItemDetails($scope.storeID, $scope.ginCode, '')
                .then(function (response) {
                    $scope.indentDetails = response;
                });
        };

        $scope.getCompanyStoreItemsGIN();
    });

﻿prmApp
    .controller('storeginhistoryCtrl', function ($scope, $state, $stateParams, $window, $log, userService, storeService, fileReader, growlService) {
        $scope.storeID = $stateParams.storeID;
        $scope.sessionID = userService.getUserToken();
        $scope.storeItemGINArray = [];

        $scope.handleDate = function (date) {
            return new moment(date).format("DD-MMM-YYYY");
        }

        $scope.cancelClick = function () {
            $window.history.back();
        }

        $scope.selectGinDetails = function (ginDetails) {
            $state.go("storegindetails", { "storeID": $scope.storeID, "ginCode": ginDetails.ginCode, "ginObj": ginDetails });            
        }

        $scope.getCompanyStoreItemsGIN = function () {
            storeService.getStoreItemGIN('', $scope.storeID)
                .then(function (response) {
                    $scope.storeItemGINArray = response;
                });
        };

        $scope.getCompanyStoreItemsGIN();
    });

﻿prmApp
    .controller('storegrnCtrl', function ($scope, $state, $stateParams, $window, $log, userService, storeService, fileReader, growlService) {
        $scope.storeID = $stateParams.storeID;
        $scope.selectedStoreItems = [];
        $scope.sessionID = userService.getUserToken();
        $scope.companyStoreItems = [];
        $scope.companyStoreItemsTemp = [];
        $scope.storeItemSerialNos = [];
        $scope.storeItemSerialNosSelected = {};
        $scope.currentStoreItemID = 0;

        $scope.generateGrnError = false;
        $scope.receiveDateError = false;

        var GenRandom = {

            Stored: [],

            Job: function () {
                var newId = Date.now().toString().substr(6); // or use any method that you want to achieve this string

                if (!this.Check(newId)) {
                    this.Stored.push(newId);
                    return newId;
                }

                return this.Job();
            },

            Check: function (id) {
                for (var i = 0; i < this.Stored.length; i++) {
                    if (this.Stored[i] == id) return true;
                }
                return false;
            }

        };

        $scope.grnObject = {
            grnID: 0,
            grnCode : "GRN-" + GenRandom.Job(),
            orderQty: 0,
            receivedQty: 0,
            sessionID: $scope.sessionID,
            modifiedBy: userService.getUserId()
        };

        $scope.searchKey = '';

        if ($stateParams.itemsArr) {
            $scope.companyStoreItems = $stateParams.itemsArr;
            if ($scope.companyStoreItems.length > 0) {
                $scope.grnObject.storeItems = $scope.companyStoreItems;
            }
        }

        $scope.cancelClick = function () {
            $window.history.back();
        }

        $scope.JsonDate = function (dateVal)
        {
            //return dateVal;
            var ts = moment(dateVal, "DD-MM-YYYY HH:mm").valueOf();
            var m = moment(ts);
            var tempDate = new Date(m);
            var milliseconds = parseInt(tempDate.getTime() / 1000.0);
            return "/Date(" + milliseconds + "000+0530)/";
        }

        $scope.generateGrn = function () {
            $scope.generateGrnValidations();
            if (!$scope.generateGrnError) {

                if ($scope.companyStoreItems.length > 0) {
                    var savedItems = 0;
                    $.each($scope.grnObject.storeItems, function (key, item) {
                        item.inStock = item.receivedQty;
                        item.totalStock = item.orderQty;
                    });

                    var ts = moment($scope.grnObject.receiveDate, "DD-MM-YYYY HH:mm").valueOf();
                    var m = moment(ts);
                    var receiveDate = new Date(m);
                    var milliseconds = parseInt(receiveDate.getTime() / 1000.0);
                    $scope.grnObject.receiveDate = "/Date(" + milliseconds + "000+0530)/";
                    $.each($scope.grnObject.storeItems, function (key, item) {
                        $.each(item.itemDetails, function (key, details) {
                            details.warrantyDate = $scope.JsonDate(details.warrantyDate);
                        });
                    });

                    var params = {
                        "item": $scope.grnObject
                    };

                    storeService.saveStoreItemGRN(params)
                        .then(function (response) {
                            if (response.errorMessage != '') {
                                growlService.growl(response.errorMessage, "inverse");
                            }
                            else {
                                growlService.growl("GIN details saved successfully.", "success");
                                $window.history.back();
                            }
                        });
                }
            }
        }

        $scope.itemDetailsSelect = function (itemDetails) {
            $scope.storeItemSerialNosSelected[itemDetails.itemID] = _.filter($scope.storeItemSerialNos[itemDetails.itemID], function (item) {
                return item.isSelected == true;
            });

            $.each($scope.grnObject.storeItems, function (key, item) {
                if (item.itemID == itemDetails.itemID)
                {
                    item.itemDetails = $scope.storeItemSerialNosSelected[item.itemID];
                }
            });
        };

        $scope.handleDate = function (date) {
            return new moment(date).format("DD-MMM-YYYY");
        }

        $scope.selectSerialNo = function (storeItem) {
            $scope.currentStoreItemID = storeItem.itemID;
            if (!$scope.storeItemSerialNos[storeItem.itemID] || $scope.storeItemSerialNos[storeItem.itemID].length <= 0)
            {
                $scope.storeItemSerialNos[storeItem.itemID] = [];
                
                for (i = 0; i <= storeItem.receivedQty - 1; i++)
                {
                    var storeDetailsObj = {
                        warrantyDate: "01-01-2020",
                        warrantyNo: "",
                        serialNo: ""
                    };

                    $scope.storeItemSerialNos[storeItem.itemID].push(storeDetailsObj);
                }

                $.each($scope.grnObject.storeItems, function (key, item) {
                    if (item.itemID == storeItem.itemID) {
                        item.itemDetails = $scope.storeItemSerialNos[item.itemID];
                    }
                });
            }
        }

        $scope.search = function () {
            $scope.companyStoreItemsTemp = _.filter($scope.companyStoreItems, function (item) {
                return item.itemCode.indexOf($scope.searchKey) > -1;
            });
        }

        $scope.generateGrnValidations = function () {

            $scope.generateGrnError = false;
            $scope.receiveDateError = false;

            
            if ($scope.grnObject.receiveDate == undefined || $scope.grnObject.receiveDate == '') {
                $scope.receiveDateError = true;
                $scope.generateGrnError = true;
            }
            
        };

    }); 

﻿prmApp
    .controller('storegrndetailsCtrl', function ($scope, $state, $stateParams, $window, $log, $timeout, userService, storeService, fileReader, growlService) {
        $scope.storeID = $stateParams.storeID;
        $scope.grnCode = $stateParams.grnCode;
        $scope.grnObject = $stateParams.grnObj;
        $scope.sessionID = userService.getUserToken();
        $scope.storeItemGRNItemDetails = [];
        $scope.indentItemDetails = [];
        $scope.indentDetails = [];
        $scope.doPrint = false;

        $scope.handleDate = function (date) {
            return new moment(date).format("DD-MMM-YYYY");
        }

        $scope.cancelClick = function () {
            $window.history.back();
        }

        $scope.printReport = function () {
            $scope.doPrint = true;
            $timeout(function () {
                $window.print();
                $scope.doPrint = false
            }, 1000);
        }

        $scope.selectGrnItemDetails = function (grnDetails) {
            if ($scope.indentDetails.length <= 0)
            {
                storeService.getIndentItemDetails($scope.storeID, $scope.grnCode, '')
                .then(function (response) {
                    $scope.indentDetails = response;
                });
            }
            else {
                $scope.indentItemDetails = _.filter($scope.indentDetails, function (item) {
                    return item.itemID == grnDetails.itemID;
                });
            }
        }

        $scope.getCompanyStoreItemsGRN = function () {
            storeService.getStoreItemGRN($scope.grnCode, $scope.storeID)
                .then(function (response) {
                    $scope.storeItemGRNItemDetails = response[0];
                });

            storeService.getIndentItemDetails($scope.storeID, '', $scope.grnCode)
                .then(function (response) {
                    $scope.indentDetails = response;
                });
        };

        $scope.getCompanyStoreItemsGRN();
    });

﻿prmApp
    .controller('storegrnhistoryCtrl', function ($scope, $state, $stateParams, $window, $log, userService, storeService, fileReader, growlService) {
        $scope.storeID = $stateParams.storeID;
        $scope.sessionID = userService.getUserToken();
        $scope.storeItemGRNArray = [];

        $scope.handleDate = function (date) {
            return new moment(date).format("DD-MMM-YYYY");
        }

        $scope.cancelClick = function () {
            $window.history.back();
        }

        $scope.selectGrnDetails = function (grnDetails) {
            $state.go("storegrndetails", { "storeID": $scope.storeID, "grnCode": grnDetails.grnCode, "grnObj": grnDetails });            
        }

        $scope.getCompanyStoreItemsGRN = function () {
            storeService.getStoreItemGRN('', $scope.storeID)
                .then(function (response) {
                    $scope.storeItemGRNArray = response;
                    $log.info($scope.storeItemGRNArray);
                });
        };

        $scope.getCompanyStoreItemsGRN();
    });

﻿prmApp
    .controller('storeitemdetailsCtrl', function ($scope, $state, $stateParams, $log, $window, userService, storeService, fileReader, growlService) {
        //$scope.storeID = $stateParams.storeID;
        $scope.itemID = $stateParams.itemID;
        $scope.notDispatched = 0;

        $scope.sessionID = userService.getUserToken();

        $scope.StoreItemDetailsList = [];

        $scope.getStoreItemDetails = function () {
            storeService.getStoreItemDetails($scope.itemID, $scope.notDispatched)
               .then(function (response) {
                   $scope.StoreItemDetailsList = response;                   
               })
        }

        $scope.handleDate = function(date)
        {
            return new moment(date).format("DD-MMM-YYYY");
        }

        $scope.getStoreItemDetails();

        $scope.cancelClick = function () {
            $window.history.back();
        };

        $scope.deletestoreitemdetail = function (itemDetails) {
            var params = {
                storeItemDetailsId: itemDetails.itemDetailID,
                sessionID: $scope.sessionID
            };

            storeService.deleteStoreItemDetails(params)
              .then(function (response) {
                  if (response.errorMessage != '') {
                      growlService.growl(response.errorMessage, "inverse");
                  }
                  else {
                      growlService.growl("Remove Item Successfully.", "success");
                      location.reload();
                  }
              });
        };

        $scope.entityObj = {
            entityName: 'StoreItemDetails',
            attachment: [],
            userid: userService.getUserId(),
            sessionID: $scope.sessionID
        };

        $scope.getFile1 = function (id, itemid, ext) {
            $scope.file = $("#" + id)[0].files[0];
            fileReader.readAsDataUrl($scope.file, $scope)
                .then(function (result) {

                    if (id == "excelItemDetails") {
                        var bytearray = new Uint8Array(result);
                        $scope.entityObj.attachment = $.makeArray(bytearray);
                        $scope.importEntity();
                    }
                });
        };

        $scope.importEntity = function () {

            var params = {
                "entity": $scope.entityObj
            };

            storeService.importentity(params)
               .then(function (response) {
                   if (response.errorMessage != '' || response.message != '') {
                       growlService.growl(response.errorMessage, "inverse");
                       growlService.growl(response.message, "inverse");
                   }
                   else {
                       growlService.growl("Saved Successfully.", "success");
                       location.reload();

                   }
               });
        };

        $scope.exportItemsToExcel = function () {
            var mystyle = {
                sheetid: 'StoreItemDetails',
                headers: true,
                column: {
                    style: 'font-size:15px;background:#233646;color:#FFF;'
                }
            };

            alasql.fn.handleDate = function (date) {
                return new moment(date).format("MM/DD/YYYY");
            }

            alasql('SELECT itemDetailID as [ITEM_DETAIL_ID],  itemID as [ITEM_ID], serialNo as [SERIAL_NO], warrantyNo as [WARRANTY_NO], handleDate(warrantyDate) as [WARRANTY_DATE], ginID as [GIN_ID], grnID as [GRN_ID], poID as [PO_ID] INTO XLSX(?,{headers:true,sheetid: "StoreItemDetails", style: "font-size:15px;background:#233646;color:#FFF;"}) FROM ?', ["StoreItemDetails.xlsx", $scope.StoreItemDetailsList]);
        };

    }); 

﻿prmApp
    .controller('storeitempropertiesCtrl', function ($scope, $state, $stateParams, $window, fileReader, userService, storeService, growlService) {
        $scope.compID = $stateParams.compID;
        $scope.itemID = $stateParams.itemID;

        $scope.sessionID = userService.getUserToken();

        $scope.storeItemProperties = [];
        $scope.storeItemProperty = {
            custPropID: 0,
            companyID: $scope.compID,
            custPropCode: '',
            custPropDesc: '',
            custPropType: '',
            custPropDefault: '',
            itemID: $scope.itemID,
            custPropValue: '',
            sessionID: $scope.sessionID
        };
       
        storeService.storesitempropvalues($scope.itemID)
            .then(function (response) {
                $scope.storeItemProperties = response;                  
            });

        $scope.showSaveProperty = false;

        $scope.showSavePropertyFunction = function (val) {
            $scope.storeItemProperty = {
                custPropID: 0,
                companyID: $scope.compID,
                custPropCode: '',
                custPropDesc: '',
                custPropType: '',
                custPropDefault: '',
                itemID: $scope.itemID,
                custPropValue: '',
                sessionID: $scope.sessionID
            };
            $scope.showSaveProperty = val;
        };

        $scope.savestoreitemprop = function (itemProperty, isEdit) {
            itemProperty.sessionID = $scope.sessionID;
            var params = {
                "storeItemProp": itemProperty
            };
            storeService.savestoreitemprop(params)
               .then(function (response) {
                   if (response.errorMessage != '') {
                       growlService.growl(response.errorMessage, "inverse");
                   }
                   else {
                       // growlService.growl("Store Item Property Saved Successfully.", "success");
                       $scope.storeItemProperty.custPropID = response.objectID;
                       $scope.savestoreitempropvalue(itemProperty);
                   }
               })
        };

        $scope.savestoreitempropvalue = function (storeItemProperty) {
            storeItemProperty.sessionID = $scope.sessionID;
            storeItemProperty.itemID = $scope.itemID;
            var params = {
                storeItemProp: storeItemProperty
            };

            storeService.savestoreitempropvalue(params)
               .then(function (response) {
                   if (response.errorMessage != '') {
                       growlService.growl(response.errorMessage, "inverse");
                       storeItemProperty.custPropID = response.objectID;
                   }
                   else {
                       growlService.growl("Store Item Property Value Saved Successfully.", "success");
                       $scope.showSaveProperty = false;

                       var index = _.find($scope.storeItemProperties, function (o) { return o.custPropID == storeItemProperty.custPropID; });                       
                       if (!index) {
                           $scope.storeItemProperties.push(storeItemProperty);
                       }
                   }
               })
        };
           
        $scope.cancelClick = function () {
            $window.history.back();
        };

        $scope.exportItemsToExcel = function () {
            var mystyle = {
                sheetid: 'StoreItemsPropValues',
                headers: true,
                column: {
                    style: 'font-size:15px;background:#233646;color:#FFF;'
                }
            };

            alasql.fn.getstoreid = function (x) {
                return x.storeID;
            }

            alasql('SELECT custPropID as [CUST_PROP_ID], companyID as [COMP_ID], custPropCode as [CUST_PROP_CODE], custPropDesc as [CUST_PROP_DESC], custPropType as [CUST_PROP_TYPE], custPropDefault as [CUST_PROP_DEFAULT], itemID as [ITEM_ID], custPropValue as [CUST_PROP_VALUE] INTO XLSX(?,{headers:true,sheetid: "StoreItemsPropValues", style: "font-size:15px;background:#233646;color:#FFF;"}) FROM ?', ["StoreItemsPropValues.xlsx", $scope.storeItemProperties]);
        };

        $scope.entityObj = {
            entityName: 'StoreItemsPropValues',
            attachment: [],
            userid: userService.getUserId(),
            sessionID: $scope.sessionID
        };

        $scope.getFile1 = function (id, itemid, ext) {
            $scope.file = $("#" + id)[0].files[0];
            fileReader.readAsDataUrl($scope.file, $scope)
                .then(function (result) {

                    if (id == "excelItemProperties") {
                        var bytearray = new Uint8Array(result);
                        $scope.entityObj.attachment = $.makeArray(bytearray);
                        $scope.importEntity();
                    }
                });
        };

        $scope.importEntity = function () {

            var params = {
                "entity": $scope.entityObj
            };

            storeService.importentity(params)
               .then(function (response) {
                   if (response.errorMessage != '') {
                       growlService.growl(response.errorMessage, "inverse");
                   }
                   else {
                       growlService.growl("Saved Successfully.", "success");
                       location.reload();

                   }
               })
        };


        $scope.editstoreitemprop = function (storeItemProperty, valu) {
            $scope.storeItemProperty = storeItemProperty;
            $scope.showSaveProperty = true;

            document.body.scrollTop = 0; // For Chrome, Safari and Opera 
            document.documentElement.scrollTop = 0; // For IE and Firefox

        };


    });﻿prmApp
    .controller('storeitemsCtrl', function ($scope, $state, $stateParams, $log, $window, userService, storeService, fileReader, growlService) {
        $scope.storeID = $stateParams.storeID;
        $scope.itemID = $stateParams.itemID;
        $scope.storeObj = $stateParams.storeObj

        $scope.sessionID = userService.getUserToken();

        $scope.companyStoreItems = [];
        $scope.selectedStoreItems = [];
        $scope.companyStoreItemsTemp = [];

        $scope.searchKey = '';

        $scope.showIsSuperUser = function () {
            if (userService.getUserObj().isSuperUser) {
                return true;
            } else {
                return false;
            }
        }

        if (!$scope.storeObj)
        {
            $scope.storeObj = {
                storeID: $scope.storeID,
                companyID: $scope.companyID,
                sessionID: $scope.sessionID
            };
        }
        else {
            $scope.storeObj.sessionID = $scope.sessionID;
        }
        
        $scope.goToStores = function () {
            $state.go("stores");
        }

        $scope.storeItemsObj = {
            storeDetails: $scope.storeObj
        };

        $scope.itemSelect = function (storeItem) {
            //if (storeItem.isSelected)
            //{
            //    $scope.selectedStoreItems.push(storeItem);
            //}
            //else
            //{
            //    $scope.selectedStoreItems = _.filter($scope.selectedStoreItems, function (item) {
            //        return item.itemID !== storeItem.itemID
            //    });
            //}

            $scope.selectedStoreItems = _.filter($scope.companyStoreItemsTemp, function (item) {
                return item.isSelected == true;
            });
        };

        $scope.entityObj = {
            entityName: 'StoreItems',
            attachment: [],
            userid: userService.getUserId(),
            sessionID: $scope.sessionID
        };

        $scope.getCompanyStoreItems = function () {
            storeService.getcompanystoreitems($scope.storeID, $scope.itemID)
               .then(function (response) {
                   $scope.companyStoreItems = response;
                   $scope.companyStoreItemsTemp = _.filter($scope.companyStoreItems, function (item) {
                       return item.itemCode.indexOf($scope.searchKey) > -1;
                   });

               })
        }

        $scope.getCompanyStoreItems();

        $scope.search = function ()
        {
            $scope.companyStoreItemsTemp = _.filter($scope.companyStoreItems, function (item) {
                return (item.itemCode.toLowerCase().indexOf($scope.searchKey.toLowerCase()) > -1 || item.itemType.toLowerCase().indexOf($scope.searchKey.toLowerCase()) > -1 || item.itemDescription.toLowerCase().indexOf($scope.searchKey.toLowerCase()) > -1 || item.itemName.toLowerCase().indexOf($scope.searchKey.toLowerCase()) > -1);
            });
        }

        $scope.exportItemsToExcel = function () {
            var mystyle = {
                sheetid: 'StoreItems',
                headers: true,
                column: {
                    style: 'font-size:15px;background:#233646;color:#FFF;'
                }
            };

            alasql.fn.getstoreid = function (x) {
                return x.storeID;
            }

            alasql('SELECT itemID as [ITEM_ID], getstoreid(storeDetails) as [STORE_ID], itemCode as [HSN_CODE], itemName as [SUB_CODE], itemType as [ITEM_TYPE], itemDescription as [ITEM_DESC], inStock as [IN_STOCK], onOrder as [ON_ORDER], itemPrice as [ITEM_PRICE], doAlert as [DO_ALERT], threshold as [THRESHOLD], iGST as [IGST], cGST as [CGST], sGST as [SGST], itemUnits as [ITEM_UNITS] INTO XLSX(?,{headers:true,sheetid: "StoreItems", style: "font-size:15px;background:#233646;color:#FFF;"}) FROM ?', ["StoreItems.xlsx", $scope.companyStoreItems]);
        };

        $scope.downloadTemplate = function () {
            var mystyle = {
                sheetid: 'StoreItems',
                headers: true,
                column: {
                    style: 'font-size:15px;background:#233646;color:#FFF;'
                }
            };

            alasql.fn.getstoreid = function () {                                
                return $scope.storeID;
            }

            alasql('SELECT itemID as [ITEM_ID], getstoreid() as [STORE_ID], itemCode as [HSN_CODE], itemName as [SUB_CODE], itemType as [ITEM_TYPE], itemDescription as [ITEM_DESC], inStock as [IN_STOCK], onOrder as [ON_ORDER], itemPrice as [ITEM_PRICE], doAlert as [DO_ALERT], threshold as [THRESHOLD], iGST as [IGST], cGST as [CGST], sGST as [SGST], itemUnits as [ITEM_UNITS] INTO XLSX(?,{headers:true,sheetid: "StoreItems", style: "font-size:15px;background:#233646;color:#FFF;"}) FROM ?', ["StoreItems.xlsx", $scope.storeItemsObj]);
        }

        
        $scope.importEntity = function () {

            var params = {
                "entity": $scope.entityObj
            };

            storeService.importentity(params)
               .then(function (response) {
                   if (response.errorMessage != '') {
                       growlService.growl(response.errorMessage, "inverse");
                   }
                   else {
                       growlService.growl("Items Saved Successfully.", "success");
                       location.reload();

                   }
               })
        }

        $scope.getFile1 = function (id, itemid, ext) {
            $scope.file = $("#" + id)[0].files[0];
            fileReader.readAsDataUrl($scope.file, $scope)
                .then(function (result) {

                    if (id == "excelquotation") {
                        var bytearray = new Uint8Array(result);
                        $scope.entityObj.attachment = $.makeArray(bytearray);
                        $scope.importEntity();
                    }                    
                });
        }



        $scope.companyStoreItem = {};

        $scope.getCompanyStoreItem = function (itemID) {
            $scope.companyStoreItem = _.filter($scope.companyStoreItems, function (x) { return x.itemID == itemID; });
            $scope.singleItemDetails = $scope.companyStoreItem[0];
            return $scope.singleItemDetails;
        }
        

        $scope.goToItemEdit = function (storeitem) {
            $state.go("addnewstoreitem", { "storeID": storeitem.storeDetails.storeID, "itemID": storeitem.itemID, "itemObj": storeitem });
        }

        $scope.generategin = function () {
            if($scope.selectedStoreItems.length > 0)
            {
                $state.go("storegin", { "storeID": $scope.storeID, "itemsArr": $scope.selectedStoreItems });
            }
            else {
                swal("Warning", "Please select items to create GIN", "warning"); 
            }
        }

        $scope.generategrn = function () {
            if ($scope.selectedStoreItems.length > 0) {
                $state.go("storegrn", { "storeID": $scope.storeID, "itemsArr": $scope.selectedStoreItems });
            }
            else {
                swal("Warning", "Please select items to create GIN", "warning");
            }
        }

        $scope.grnhistory = function () {
            $state.go("storegrnhistory", { "storeID": $scope.storeID });
        }

        $scope.ginhistory = function () {
            $state.go("storeginhistory", { "storeID": $scope.storeID });
        }


        $scope.deletestoreitem = function (storeItemId) {

            var params = {
                storeItemId: storeItemId,
                sessionID: $scope.sessionID
            };

            storeService.deletestoreitem(params)
               .then(function (response) {
                   if (response.errorMessage != '') {
                       growlService.growl(response.errorMessage, "inverse");
                   }
                   else {
                       growlService.growl("Item Deleted Successfully.", "success");
                       location.reload();
                   }
               })
        }


    }); 

﻿prmApp
    .controller('storesCtrl', function ($scope, $state, $log, $stateParams, userService, storeService, growlService) {
        $scope.compID = userService.getUserCompanyId();
        $scope.storeID = 0;
        if ($stateParams.storeID)
        {
            $scope.storeID = $stateParams.storeID;
        }

        $scope.showIsSuperUser = function () {
            if (userService.getUserObj().isSuperUser) {
                return true;
            } else {
                return false;
            }
        }

        $log.info($scope.storeID);
        $scope.companyStores = [];

        $scope.sessionID = userService.getUserToken();

        $scope.isCompanyStoresLoaded = false;

        $scope.getCompanyStores = function () {
            storeService.getcompanystores($scope.compID, $scope.storeID)
                .then(function (response) {
                    $scope.companyStores = response;
                    $scope.isCompanyStoresLoaded = true;
                });
        }

        $scope.getCompanyStores();

        $scope.goToStoreEdit = function (store) {
            $state.go("addnewstore", { "storeID": store.storeID, "storeObj": store, "companyStores": $scope.companyStores });
        }

        $scope.goToStoreInventory = function (store) {
            $state.go("storeitems", { "storeID": store.storeID, "storeObj": store, "itemID": 0 });
        }

        $scope.goToBranches = function (store) {
            var url = $state.href('branches', { "storeID": store.storeID });
            window.open(url, '_blank');
            //$state.go("branches", { "storeID": store.storeID });
        }

        $scope.deletestore = function (storeId) {

            var params = {
                "storeId": storeId,
                sessionID: $scope.sessionID
            };

            storeService.deletestore(params)
                .then(function (response) {
                    if (response.errorMessage != '') {
                        growlService.growl(response.errorMessage, "inverse");
                    }
                    else {
                        growlService.growl("Store Deleted Successfully.", "success");
                        location.reload();
                    }
                })
        }


    });﻿prmApp
    .controller('managetechevalCtrl', function ($scope, $state, $stateParams, userService, growlService, techevalService, fileReader, $log) {
        $scope.compID = userService.getUserCompanyId();
        $scope.sessionID = userService.getUserToken();

        $scope.questionnaire = {
            evalID: 0,
            reqID: 0,
            userID: userService.getUserId(),
            title: '',
            description: '',
            startDate: '',
            endDate: '',
            questions: [],
            sessionID: $scope.sessionID
        };

        $scope.questionnaireList = [
            questionnaire = $scope.questionnaire
        ];

        $scope.question = {
            evalID: 0,
            questionID: 0,
            attachmentID: 0,
            userID: userService.getUserId(),
            type: '',
            text: '',
            options: '',
            marks: 0,
            isValid: 0,
            sessionID: $scope.sessionID
        };


        $scope.entity = {
            entityName: 'Questions',
            userid: userService.getUserId(),
            attachment: [],
            sessionID: $scope.sessionID
        };

        techevalService.getquestionnairelist(0)
            .then(function (response) {
                $scope.questionnaireList = response;

                $scope.questionnaireList.forEach(function (item, index) {
                    item.startDate = new moment(item.startDate).format("DD-MM-YYYY");
                    item.endDate = new moment(item.endDate).format("DD-MM-YYYY");
                })                
        })
       
        $scope.goToQuestionnaireEdit = function (compID, questionnaireID) {
            $state.go("savequestionnaire", { "compID": compID, "questionnaireID": questionnaireID });
        }        

        $scope.gotechevalquestions = function (evalID) {
            $state.go("techevalresponses", { "evalID": evalID });
        }

        $scope.downloadTemplate = function (evalIDValue) {
            var mystyle = {
                sheetid: 'Questions',
                headers: true,
                column: {
                    style: 'font-size:15px;background:#233646;color:#FFF;'
                }
            };           
            alasql.fn.getevalID = function () {
                return evalIDValue;
            }
            alasql.fn.getqueID = function () {
                return 0;
            }
            alasql('SELECT getevalID() as [EVAL_ID], getqueID() as [Q_ID], attachmentID as [Q_ATTACH_ID], type as [Q_TYPE], text as [Q_TEXT], options as [Q_OPTIONS], marks as [Q_MARKS] INTO XLSX(?,{headers:true,sheetid: "Questions", style: "font-size:15px;background:#233646;color:#FFF;"}) FROM ?', ["Questions.xlsx", $scope.question]);
        }


        $scope.getFile1 = function (id, itemid, ext) {
            $scope.file = $("#" + id)[0].files[0];
            fileReader.readAsDataUrl($scope.file, $scope)
                .then(function (result) {

                    if (id == "excelquotation") {
                        var bytearray = new Uint8Array(result);
                        $scope.entity.attachment = $.makeArray(bytearray);
                        $scope.importEntity();
                    }
                });
        };

        $scope.importEntity = function () {
            var params = {
                "entity": $scope.entity
            };
            techevalService.importentity(params)
               .then(function (response) {
                   if (response.errorMessage != '') {
                       growlService.growl(response.errorMessage, "inverse");
                   }
                   else {
                       growlService.growl("Quotions Saved Successfully.", "success");
                       location.reload();
                   }
               })
        };

        $scope.goToAddQuestions = function (questionnaire) {
           $state.go("questions", { "reqID": questionnaire.reqID, "evalID": questionnaire.evalID });
        }

        $scope.goToEditQuestionaire = function (questionnaire) {
            $state.go("savequestionnaire", { "compID": $scope.compID, "questionnaireID": questionnaire.evalID });
        }

        $scope.goToCloneQuestionaire = function (questionnaire) {
            $state.go("clonequestionnaire", { "compID": $scope.compID, "questionnaireID": questionnaire.evalID, "cloneData": true });
        }

    });  ﻿prmApp
    .controller('savequestionnaireCtrl', function ($scope, $stateParams, $state, $window, $log, $filter, userService, growlService, techevalService) {        
        $scope.companyID = userService.getUserCompanyId();
        $scope.sessionID = userService.getUserToken();
        $scope.evalID = $stateParams.questionnaireID == "" || $stateParams.questionnaireID == null ? 0 : $stateParams.questionnaireID;
        $scope.Vendors = [];
        $scope.selectedA = [];
        $scope.selectedB = [];
        $scope.formRequest = {
            auctionVendors: []
        }
        $scope.questionnaire = {
            evalID: 0,
            reqID: 0,
            createdBy: userService.getUserId(),
            title: '',
            description: '',
            startDate: '',
            endDate: '',
            questions: []
        };

        if ($scope.evalID > 0) {
            techevalService.getquestionnaire($scope.evalID, 1)
                .then(function (response) {
                    if (response) {
                        $scope.questionnaire = response;
                        $scope.questionnaire.startDate = new moment(response.startDate).format("DD-MM-YYYY");
                        $scope.questionnaire.endDate = new moment(response.endDate).format("DD-MM-YYYY");
                        if ($stateParams.cloneData) {
                            $scope.questionnaire.evalID = 0;
                            $scope.questionnaire.reqID = 0;
                            $scope.questionnaire.questions.forEach(function (item, index) {
                                item.questionID = 0;
                                item.isValid = 1;
                            });
                        }
                    }
                })
        }

        $scope.savequestionnaire = function () {

            var ts = moment($scope.questionnaire.startDate, "DD-MM-YYYY HH:mm").valueOf();
            var m = moment(ts);
            var startDate = new Date(m);
            var milliseconds = parseInt(startDate.getTime() / 1000.0);
            $scope.questionnaire.startDate = "/Date(" + milliseconds + "000+0530)/";

            var ts = moment($scope.questionnaire.endDate, "DD-MM-YYYY HH:mm").valueOf();
            var m = moment(ts);
            var endDate = new Date(m);
            var milliseconds = parseInt(endDate.getTime() / 1000.0);
            $scope.questionnaire.endDate = "/Date(" + milliseconds + "000+0530)/";

            $scope.questionnaire.createdBy = userService.getUserId();
            $scope.questionnaire.sessionID = $scope.sessionID
            var params = {
                questionnaire: $scope.questionnaire
            };
            
            techevalService.savequestionnaire(params)
               .then(function (response) {
                   if (response.errorMessage != '') {
                       growlService.growl(response.errorMessage, "inverse");
                   }
                   else {
                       growlService.growl("Questionnaire Saved Successfully.", "success");
                       $state.go("managetecheval");
                   }
               })
        };

        $scope.selectForA = function (item) {
            var index = $scope.selectedA.indexOf(item);
            if (index > -1) {
                $scope.selectedA.splice(index, 1);
            } else {
                $scope.selectedA.splice($scope.selectedA.length, 0, item);
            }
            for (i = 0; i < $scope.selectedA.length; i++) {
                $scope.formRequest.auctionVendors.push($scope.selectedA[i]);
                $scope.Vendors.splice($scope.Vendors.indexOf($scope.selectedA[i]), 1);
            }
            $scope.reset();
        }

        $scope.selectForB = function (item) {
            var index = $scope.selectedB.indexOf(item);
            if (index > -1) {
                $scope.selectedB.splice(index, 1);
            } else {
                $scope.selectedB.splice($scope.selectedA.length, 0, item);
            }
            for (i = 0; i < $scope.selectedB.length; i++) {
                $scope.Vendors.push($scope.selectedB[i]);
                $scope.formRequest.auctionVendors.splice($scope.formRequest.auctionVendors.indexOf($scope.selectedB[i]), 1);
            }
            $scope.reset();
        }

        $scope.AtoB = function () {

        }

        $scope.BtoA = function () {

        }

        $scope.reset = function () {
            $scope.selectedA = [];
            $scope.selectedB = [];
        }

        $scope.GetCompanyVendors = function () {
            $scope.params = { "userID": userService.getUserId(), "sessionID": userService.getUserToken() }
            userService.GetCompanyVendors($scope.params)
            .then(function (response) {
                if (response.length > 0) {
                    $scope.Vendors = response;
                    $scope.vendorsLoaded = true;
                    for (var j in $scope.formRequest.auctionVendors) {
                        for (var i in $scope.Vendors) {
                            if ($scope.Vendors[i].userID == $scope.formRequest.auctionVendors[j].userID) {
                                $scope.Vendors.splice(i, 1);
                            }
                        }
                    }
                }
            });
        }

        $scope.GetVendorsForTechEval = function () {
            techevalService.GetVendorsForTechEval($scope.evalID)
                .then(function (response) {
                    if (response.length > 0) {
                        $scope.formRequest.auctionVendors = response;
                        $scope.GetCompanyVendors();
                    }
                });
        }

        $scope.GetVendorsForTechEval();

        $scope.saveVendorsForQuestionnaire = function () {
            var params = {
                evalID: $scope.evalID,
                vendors: $scope.formRequest.auctionVendors,
                sessionID: userService.getUserToken()
            };

            techevalService.saveVendorsForQuestionnaire(params)
                .then(function (response) {
                    if (response.errorMessage != '') {
                        growlService.growl(response.errorMessage, "inverse");
                    }
                    else {
                        growlService.growl("Questionnaire will be sent to the selected Vendors.", "success");
                    }
                })
        }


});﻿prmApp
    .controller('techevalquestionsCtrl', function ($scope, $state, $stateParams, $log, $window, userService, growlService, techevalService, fileReader) {
        $scope.reqID = $stateParams.reqID == "" ? 0 : $stateParams.reqID;
        $scope.evalID = $stateParams.evalID == "" || !$stateParams.evalID ? 0 : $stateParams.evalID;
        $scope.configValue = "";
        $scope.showPopUp = true;
        $scope.currentOption = "";
        $scope.questionChoices = [];
        $scope.isValidToSubmit = true;

        $scope.compID = userService.getUserCompanyId();
        $scope.sessionID = userService.getUserToken();

        $scope.questionDetails = {
            options: "",
            text: "",
            type: "DESC",
            sessionID: $scope.sessionID,
            questionID: 0,
            marks: 0,
            isValid: 1,
            evalID: $scope.evalID
        }
        
        $scope.questionnaire = {};
        $scope.load = false;      
        $scope.totalMarks = 0;

        $scope.answers = [];
        $scope.responses = [];
        $scope.userResponse = '';

        $scope.showPopUpClick = function (display) {
            $scope.showPopUp = display;
        };

        $scope.handleOption = function (value, doAddOption) {
            var contains = $.inArray(value, $scope.questionChoices);
            if (doAddOption)
            {
                if (contains < 0)
                {
                    $scope.questionChoices.push(value);
                }
            }
            else {
                if (contains >= 0) {
                    $scope.questionChoices.splice(contains, 1);
                }                
            }

            $scope.currentOption = "";
        };

        $scope.deleteQuestion = function (questionItem) {
            var params = {
                question: questionItem
            };

            techevalService.deletequestion(params)
                .then(function (response) {
                    if (response.errorMessage != '') {
                        growlService.growl(response.errorMessage, "inverse");
                    }
                    else {
                        growlService.growl("Question Deleted Successfully.", "success");
                        $scope.getResponses();
                    }
                });
        }

        $scope.editQuestion = function (questionItem) {
            if (questionItem.type == 'CHECK_BOX' || questionItem.type == 'RADIO_BUTTON') {
                $scope.questionChoices = questionItem.options.split("$~");
            }
            else {
                $scope.questionChoices = [];
            }

            $scope.questionDetails = questionItem;
            $scope.showPopUp = false;
        }

        $scope.saveQuestionDetails = function () {
            var options = "";
            if ($scope.questionChoices && $scope.questionChoices.length > 0)
            {
                $scope.questionChoices.forEach(function (item2, index2) {
                    options += item2 + '$~';
                });

                options = options.slice(0, -2);
            }

            if (!$scope.questionDetails.type || $scope.questionDetails.type == "")
            {
                $scope.questionDetails.type = "DESC";
            }

            $scope.questionDetails.options = options;
            
            var params = {
                question: $scope.questionDetails
            };

            validateData($scope.questionDetails);
            if (!$scope.isValidToSubmit) {
                growlService.growl("Please fill all required fields marked (*).", "inverse");
                return false;
            }

            techevalService.savequestion(params)
                .then(function (response) {
                    if (response.errorMessage != '') {
                        growlService.growl(response.errorMessage, "inverse");
                    }
                    else {
                        growlService.growl("Question Saved Successfully.", "success");
                        $scope.getResponses();
                        $scope.showPopUp = true;
                    }
                });

            
            $scope.questionDetails = {
                options: "",
                text: "",
                type: "DESC",
                sessionID: $scope.sessionID,
                questionID: 0,
                marks: 0,
                isValid: 1,
                evalID: $scope.evalID
            }

            $scope.currentOption = "";
            $scope.questionChoices = [];
        };

        $scope.getResponses = function () {
            techevalService.getquestionnaire($scope.evalID, 1)
                .then(function (response) {
                    $scope.questionnaire = response;
                    $scope.totalMarks = 0;
                    $scope.questionnaire.questions.forEach(function (item, index) {
                        
                        item.sessionID = $scope.sessionID;

                        $scope.totalMarks = $scope.totalMarks + item.marks;
                        item.answerArray = [];
                        if (item.type == 'CHECK_BOX' || item.type == 'RADIO_BUTTON') {
                            item.optionsArray = item.options.split("$~");                            
                        }
                        else {
                            item.optionsArray = [];
                        }
                    });
                    
                $scope.load = true;
            });            
        };

        $scope.getResponses();
        
        $scope.cancelResponse = function (isApproved) {
            $window.history.back();
        };

        function validateData(questionObj) {
            if (questionObj.text == null || questionObj.text == "") {
                $scope.isValidToSubmit = false;
            }
            else if (questionObj.type == null || questionObj.type == "") {
                $scope.isValidToSubmit = false;
            }
            else if (questionObj.type != 'DESC' && (questionObj.options == null || questionObj.options == "")) {
                $scope.isValidToSubmit = false;
            }
            else {
                $scope.isValidToSubmit = true;
            }
        }




        $scope.getFile = function () {

            $scope.file = $("#QuestionAttachement")[0].files[0];

            fileReader.readAsDataUrl($scope.file, $scope)
                .then(function (result) {
                    var bytearray = new Uint8Array(result);
                    $scope.questionDetails.attachment = $.makeArray(bytearray);
                    $scope.questionDetails.attachmentName = $scope.file.name;
                });
        };


    });﻿prmApp
    .controller('techevalresponsesCtrl', function ($scope, $state, $stateParams, $log, $window, userService, growlService, techevalService, fileReader) {
        $scope.reqID = $stateParams.reqID == "" ? 0 : $stateParams.reqID;
        $scope.evalID = $stateParams.evalID == "" || !$stateParams.evalID ? 0 : $stateParams.evalID;
        $scope.vendorID = $stateParams.vendorID == "" ? 0 : $stateParams.vendorID;
        

        $scope.compID = userService.getUserCompanyId();
        $scope.sessionID = userService.getUserToken();
        $scope.userID = userService.getUserId();

        $scope.isCustomer = userService.getUserType() == "CUSTOMER"? true : false;
        
        $scope.load = false;      

        $scope.answers = [];
        $scope.responses = [];
        $scope.userResponse = '';

        $scope.getResponses = function () {
            techevalService.getresponses($scope.reqID, $scope.evalID, $scope.vendorID)
            .then(function (response) {
                $scope.answers = response;                 
                $scope.answers.forEach(function (item, index) {
                    item.vendor.userID = $scope.userID;
                    item.answerArray = [];
                    if (item.questionDetails.type == 'CHECK_BOX' || item.questionDetails.type == 'RADIO_BUTTON') {
                        item.questionDetails.optionsArray = item.questionDetails.options.split("$~");
                        if (item.answerText != "")
                        {
                            item.answerArray = item.answerText.split("$~");
                        }                        
                    }
                    else {
                        item.questionDetails.optionsArray = [];                        
                    }
                });

                $scope.load = true;
            });            
        };

        $scope.getResponses();
        
        $scope.toggle = function (option, list, qid) {
            $scope.answers.forEach(function (item, index) {
                if (item.questionDetails.questionID == qid) {
                    if (item.answerArray.indexOf(option) > -1) {
                        item.answerArray.splice(item.answerArray.indexOf(option), 1);
                    }
                    else {
                        item.answerArray.push(option);
                    }
                }
            });
        };


        $scope.radio = function (option, qid) {
            $scope.answers.forEach(function (item, index) {
                if (item.questionDetails.questionID == qid) {
                    item.answerText = option;
                }
            });
        };

        $scope.saveresponse = function () {
            $scope.answers.forEach(function (item, index) {
                if (item.questionDetails.type == 'CHECK_BOX') {
                    item.answerText = '';
                    item.answerArray.forEach(function (item2, index2) {
                        item.answerText += item2 + '$~';
                    });

                    item.answerText = item.answerText.slice(0, -2);
                    var tem = item.answerText;
                }
            });

            var params = {
                answers: $scope.answers
            };

            techevalService.saveresponse(params)
               .then(function (response) {
                   if (response.errorMessage != '') {
                       growlService.growl(response.errorMessage, "inverse");
                   }
                   else {
                       growlService.growl("Answers Saved Successfully.", "success");
                       $scope.getResponses();
                   }
               })
        };

        $scope.saveRequestDetails = function (isApproved) {
           
            $scope.techEvalObj = $stateParams.techEvalObj;
            if ($scope.techEvalObj) {
                $scope.techEvalObj.isApproved = isApproved;
                $scope.techEvalObj.modifiedBy = $scope.userID;

                var params = {
                    techeval: $scope.techEvalObj
            };

            techevalService.saveTechApproval(params)
               .then(function (response) {
                   if (response.errorMessage != '') {
                       growlService.growl(response.errorMessage, "inverse");
                   }
                   else {
                       growlService.growl("Saved Successfully.", "success");
                       $window.history.back();
                   }
               })
            }            
        };

        $scope.cancelResponse = function (isApproved) {
            $window.history.back();
        };
    });﻿prmApp
    .controller('techevaluationCtrl', function ($scope, $state, $stateParams, userService, growlService, techevalService, fileReader) {
        $scope.evalID = $stateParams.evalID == "" ? 0 : $stateParams.evalID;
        $scope.reqID = $stateParams.reqID == "" ? 0 : $stateParams.reqID;

        $scope.compID = userService.getUserCompanyId();
        $scope.sessionID = userService.getUserToken();

        $scope.techEvalutionList = [];
       
        techevalService.GetTechEvalution($scope.evalID, $scope.reqID)
            .then(function (response) {
                $scope.techEvalutionList = response;
            });

        $scope.goToVendorEvaluation = function (techEvaluation) {
            $state.go("techevalresponses", { "reqID": techEvaluation.reqID, "vendorID": techEvaluation.vendor.userID, "techEvalObj": techEvaluation });
        };
    });