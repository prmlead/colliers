﻿prmApp
    .config(["$stateProvider", "$urlRouterProvider", "$httpProvider", "$provide", "domain", "version",
        function ($stateProvider, $urlRouterProvider, $httpProvider, $provide, domain, version) {
            $stateProvider

                //////////////
                //  CATALOG MGMT   //
                //////////////
            .state('catalogMgmtMainPage', {
                url: '/catalogMgmtMainPage',
                templateUrl: 'views/CatalogMgmt/catalogMgmtMainPage.html'
            })
            .state('productAttribute', {
                url: '/productAttribute',
                templateUrl: 'views/CatalogMgmt/productAttribute.html'
            })
             .state('general', {
                 url: '/general',
                 templateUrl: 'views/CatalogMgmt/general.html'
             })

            .state('catalogMgmt', {
                url: '/catalogMgmt',
                templateUrl: 'views/CatalogMgmt/catalogMgmt.html'
            })
            .state('Attribute', {
                url: '/Attribute',
                templateUrl: 'views/CatalogMgmt/Attribute.html'
                })
                .state('CategoryProducts', {
                url: '/CategoryProducts',
                    templateUrl: 'views/CatalogMgmt/CategoryProducts.html'
                })
                .state('products', {
                    url: '/products',
                    templateUrl: 'views/CatalogMgmt/products.html'
                })
                .state('productAdd', {
                    url: '/product/add',
                    templateUrl: 'views/CatalogMgmt/product.html'
                })
                .state('productView', {
                    url: '/product/:viewId/:productId',
                    templateUrl: 'views/CatalogMgmt/productEdit.html'
                })
                .state('productAlalysis', {
                    url: '/productAlalysis/:productId/:callID',
                    templateUrl: 'views/CatalogMgmt/productAnalysis.html'
                })
                .state('productEdit', {
                    url: '/product/:viewId/:productId',
                    templateUrl: 'views/CatalogMgmt/productEdit.html'
                })


            .state('CatalogCompleteView', {
                url: '/CatalogCompleteView',
                templateUrl: 'views/CatalogReports/CatalogCompleteView.html'
            })

     
        }]);