prmApp

    .service('PRMPRServices', ["PRMPRServicesDomain", "userService", "httpServices", function (PRMPRServicesDomain,userService, httpServices) {

        var PRMPRServices = this;


        PRMPRServices.GetSeries = function (series, seriestype, deptid) {
            var url = PRMPRServicesDomain + 'getseries?series=' + series + '&seriestype=' + seriestype + '&compid=' + userService.getUserCompanyId() + '&deptid=' + deptid + '&sessionid=' + userService.getUserToken();
            return httpServices.get(url);
        };
        
        PRMPRServices.getprdetails = function (params) {
            let url = PRMPRServicesDomain + 'getprdetails?prid=' + params.prid+ '&sessionid=' + userService.getUserToken();
             return httpServices.get(url);
        };

        PRMPRServices.getprlist = function (params) {
            let url = PRMPRServicesDomain + 'getprlist?userid=' + params.userid + '&sessionid=' + params.sessionid + '&deptid=' + params.deptid + '&desigid=' + params.desigid + '&depttypeid=' + params.depttypeid + '&fromdate=' + params.fromDate + '&todate=' + params.toDate;
            return httpServices.get(url);
        };

        PRMPRServices.savePrDetails = function (params) {
            let url = PRMPRServicesDomain + 'saveprdetails';
            return httpServices.post(url, params);
        };

        PRMPRServices.getreqprlist = function (params) {
            let url = PRMPRServicesDomain + 'getreqprlist?userid=' + params.userid + '&sessionid=' + userService.getUserToken();
            return httpServices.get(url);
        };

        PRMPRServices.GetCompanyRFQCreators = function (params) {
            let url = PRMPRServicesDomain + 'getcompanyrfqcreators?u_id=' + userService.getUserId() + '&pr_id=' + params.pr_id + '&sessionid=' + userService.getUserToken();// + '&dept_id=' + params.dept_id
            return httpServices.get(url);
        };

        PRMPRServices.SaveCompanyRFQCreators = function (params) {
            let url = PRMPRServicesDomain + 'savecompanyrfqcreators';
            return httpServices.post(url, params);
        };

        PRMPRServices.getpritemslist = function (params) {
            let url = PRMPRServicesDomain + 'getpritemslist?prid=' + params.prid + '&sessionid=' + userService.getUserToken();
            return httpServices.get(url);
        };


        return PRMPRServices;
}]);