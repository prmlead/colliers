angular.module("prm.user").service('userService', userService);


function userService($http, store, $state, $rootScope, $q, version, domain, growlService, vendorDomian, httpServices, auctionsService) {
    //var domain = 'http://182.18.169.32/services/';
    var self = this;
    self.myCatalog = [];
    var successMessage = '';
    self.companyFieldTemplates = [];
    $rootScope.featureCodeHide = true;

    self.userData = { currentUser: null };

    self.getUserObj = function () {
        if (!self.userData.currentUser || !self.userData.currentUser.id) {
            self.userData.currentUser = store.get('currentUser');
        }

        $rootScope.companyRoundingDecimalSetting = (self.userData.currentUser || {}).companyRoundingDecimalSetting;
        $rootScope.companyRoundingDecimalSetting = ($rootScope.companyRoundingDecimalSetting || 3);
        return self.userData.currentUser || {};
    };

    self.getUserToken = function () {
        return store.get('sessionid');
    };

    self.getUserHashToken = function () {
        return store.get('userhashtoken');
    };

    self.getJWTToken = function () {
        return store.get('authentication');
    };

    self.getLocalEntitlement = function () {
        return store.get('localEntitlement');
    };

    self.removeLocalEntitlement = function () {
        return store.remove('localEntitlement');
    };

    self.getLocalDeptDesigt = function () {
        return store.get('localDeptDesig');
    };

    self.removeLocalDeptDesig = function () {
        return store.remove('localDeptDesig');
    };

    self.getListUserDepartmentDesignations = function () {
        return store.get('ListUserDepartmentDesignations');
    };

    self.removeListUserDepartmentDesignations = function () {
        return store.remove('ListUserDepartmentDesignations');
    };

    self.getSelectedUserDepartmentDesignation = function () {
        return store.get('SelectedUserDepartmentDesignation');
    };

    self.removeSelectedUserDepartmentDesignation = function () {
        return store.remove('SelectedUserDepartmentDesignation');
    };

    self.getSelectedUserDeptID = function () {
        return store.get('SelectedUserDepartmentDesignation').deptID;
    };

    self.getSelectedUserDesigID = function () {

        var desigs = store.get('SelectedUserDepartmentDesignation').listDesignation;

        desigs = desigs.filter(function (d) {
            return (d.isAssignedToUser == true && d.isValid == true)
        });

        return desigs[0].desigID;

    };


    self.getOtpVerified = function () {
        return self.getUserObj().isOTPVerified;
    };

    self.getEmailOtpVerified = function () {
        return self.getUserObj().isEmailOTPVerified;
    };

    self.reloadProfileObj = function () {

    };

    self.getDecimalRoundingSetting = function () {
        return self.getUserObj().companyRoundingDecimalSetting;
    };

    self.getDocsVerified = function () {
        return self.getUserObj().credentialsVerified;
    };
    self.removeUserToken = function () {
        store.remove('sessionid');
    };
    self.removeUserHashToken = function () {
        store.remove('userhashtoken');
    };
    self.removeJWTToken = function () {
        store.remove('authentication');
    };    
    self.getRememberMeToken = function () {
        return store.get('rememberMeToken');
    };
    self.removeRememberMeToken = function () {
        store.remove('rememberMeToken');
    };

    self.removeUser = function () {
        store.remove('currentUser');
    };

    self.getUsername = function () {
        return self.getUserObj().username;
    };

    self.getFirstname = function () {
        console.log(self.getUserObj());
        return self.getUserObj().firstName;
    };

    self.getLastname = function () {
        return self.getUserObj().lastName;
    };

    self.getUserId = function () {
        return self.getUserObj().userID;
    };

    self.getSAPUserId = function () {
        return self.getUserObj().sapUserId;
    };

    self.setProjectID = function (id) {
        self.projectID = id;
    };

    self.getProjectID = function () {
        return self.projectID;
    };

    self.setSelectedSubPackages = function (ids) {
        self.selectedSubPackages = ids;
    };

    self.getSelectedSubPackages = function () {
        return self.selectedSubPackages;
    };

    self.getUserCompanyId = function () {
        return self.getUserObj().companyId;
    };

    self.getCustomerCompanyId = function () {
        return self.getUserObj().COMP_ID_CUSTOMER;
    };

    self.getUserCatalogCompanyId = function () {
        return self.getUserObj().catalogueCompanyId;
    };

    self.setMessage = function (msg) {
        successMessage = msg;
    };

    self.getMessage = function () {
        return successMessage;
    };

    self.getUserType = function () {
        return self.getUserObj().userType;
    };


    self.getCostCentres = function ()
    {
        var costCentreArr =
            [
                {
                    display: 'DB_south',
                    value: 'DB_south'
                },
                {
                    display: 'DB_north',
                    value: 'DB_north'
                },
                {
                    display: 'DB_mid',
                    value: 'DB_mid'
                },
                {
                    display: 'PMC_North',
                    value: 'PMC_North'
                },
                {
                    display: 'PMC_Mid',
                    value: 'PMC_Mid'
                },
                {
                    display: 'PMC_South',
                    value: 'PMC_South'
                },
                {
                    display: 'Fitout-General',
                    value: 'Fitout-General'
                }
            ];

        return costCentreArr;
    };


    self.setCurrentUser = function () {
        self.userData.sessionid = self.getUserToken();
        self.userData.currentUser = self.getUserObj();
    };

    self.setCurrentUser();

    self.checkUniqueValue21 = function (type, currentvalue) {
        var data = {
            "type": type,
            "currentvalue": currentvalue
        };
        //console.log(data);
        return false;
        // return $http.post("", data).then( function(res) {
        //   return res.data.isUnique;
        // });
    };

    self.checkUniqueValue = function (type, currentvalue) {
        var data = {
            "phone": currentvalue,
            "idtype": type
        };
        //console.log(data);
        //return false;
        return $http({
            method: 'POST',
            url: domain + 'checkuserifexists',
            headers: { 'Content-Type': 'application/json' },
            encodeURI: true,
            data: data
        }).then(function (response) {
            //console.log(response);
            return response.data.CheckUserIfExistsResult;
        });
    };

    self.forgotpassword = function (forgot) {
        var data = {
            "email": forgot.email,
            "sessionid": ''
        };
        //data=forgot.email;
        return $http({
            method: 'POST',
            url: domain + 'forgotpassword',
            headers: { 'Content-Type': 'application/json' },
            encodeURI: true,
            data: data
        }).then(function (response) {
            return response;
        });
        //  $http.post(domain+"forgotassword", data).then( function(res) {
        //   return res;
        // });
    };

    self.resetpassword = function (resetpass) {
        var data = {
            "email": resetpass.email,
            "sessionid": resetpass.sessionid,
            "NewPass": resetpass.NewPass,
            "ConfNewPass": resetpass.ConfNewPass
        };
        //data=forgot.email;
        return $http({
            method: 'POST',
            url: domain + 'resetpassword',
            headers: { 'Content-Type': 'application/json' },
            encodeURI: true,
            data: data
        }).then(function (response) {
            $state.go('login');
            return response;
        });
        //  $http.post(domain+"forgotassword", data).then( function(res) {
        //   return res;
        // });
    };

    self.resendotp = function (userid) {
        var data = {
            "userID": userid
        };
        return $http({
            method: 'POST',
            url: domain + 'sendotp',
            headers: { 'Content-Type': 'application/json' },
            encodeURI: true,
            data: data
        }).then(function (response) {
            swal("Done!", "OTP send to your registered Mobile No.", "success");
        });
    };


    self.isnegotiationrunning = function () {
        var data = {
            "userID": self.getUserId(),
            "sessionID": self.getUserToken()
        };
        return $http({
            method: 'POST',
            url: domain + 'isnegotiationrunning',
            headers: { 'Content-Type': 'application/json' },
            encodeURI: true,
            data: data
        }).then(function (response) {
            //console.log('isnegotiationrunning');
            return response;
        });
    };
    
    self.resendemailotp = function (userid) {
        var data = {
            "userID": userid
        };
        return $http({
            method: 'POST',
            url: domain + 'sendotpforemail',
            headers: { 'Content-Type': 'application/json' },
            encodeURI: true,
            data: data
        }).then(function (response) {
            swal("Done!", "OTP send to your registered Email.", "success");
        });
    };


    self.getUserDataNoCache = function () {
        var params = {
            userid: self.getUserId(),
            sessionid: self.getUserToken()
        }
        return $http({
            method: 'GET',
            url: domain + 'getuserinfo?userid=' + params.userid + '&sessionid=' + params.sessionid,
            encodeURI: true,
            headers: { 'Content-Type': 'application/json' }
            //data: params
        }).then(function (response) {

            store.set('currentUser', response.data);
            self.setCurrentUser();
            var list = {};
            if (response && response.data) {
                list = response.data;
                return list;
            } else {
                //console.log(response.data.errorMessage);
            }
        }, function (result) {
            //console.log(result);
        });
    }

    self.verifyOTP = function (otpobj) {
        var data = {
            "OTP": otpobj,
            "userID": self.getUserId(),
            "phone": otpobj.phone ? otpobj.phone : ""
        };
        return $http({
            method: 'POST',
            url: domain + 'verifyotp',
            headers: { 'Content-Type': 'application/json' },
            encodeURI: true,
            data: data
        }).then(function (response) {
            if (response.data.sessionID != "" || response.data.userInfo.sessionID != "") {
                store.set('sessionid', response.data.sessionID != "" ? response.data.sessionID : response.data.userInfo.sessionID);
                if (response.data.userInfo.isOTPVerified == 1 && response.data.userInfo.credentialsVerified == 1) {
                    store.set('verified', 1);
                } else {
                    store.set('verified', 0);
                }
                if (response.data.userInfo.isEmailOTPVerified == 1) {
                    store.set('emailverified', 1);
                } else {
                    store.set('emailverified', 0);
                }
                if (response.data.userInfo.credentialsVerified == 1) {
                    store.set('credverified', 1);
                } else {
                    store.set('credverified', 0);
                }
                store.set('currentUser', response.data.userInfo);
                self.setCurrentUser();
                $state.go('pages.profile.profile-about');
            } else {
                swal('Warning', "The OTP is not valid. Please enter the valid OTP sent to your mobile number", "warning");
            }
           
            return response.data;
        });
    };





    self.verifyEmailOTP = function (otpobj) {
        var data = {
            "OTP": otpobj,
            "userID": self.getUserId(),
            "email": otpobj.email ? otpobj.email : ""
        };
        return $http({
            method: 'POST',
            url: domain + 'verifyemailotp',
            headers: { 'Content-Type': 'application/json' },
            encodeURI: true,
            data: data
        }).then(function (response) {
            if (response.data.sessionID != "" || response.data.userInfo.sessionID != "") {
                store.set('sessionid', response.data.sessionID != "" ? response.data.sessionID : response.data.userInfo.sessionID);
                if (response.data.userInfo.isEmailOTPVerified == 1) {
                    store.set('emailverified', 1);
                } else {
                    store.set('emailverified', 0);
                }
                if (response.data.userInfo.isOTPVerified == 1) {
                    store.set('verified', 1);
                } else {
                    store.set('verified', 0);
                }
                if (response.data.userInfo.credentialsVerified == 1) {
                    store.set('credverified', 1);
                } else {
                    store.set('credverified', 0);
                }
                store.set('currentUser', response.data.userInfo);
                self.setCurrentUser();
                $state.go('pages.profile.profile-about');
            } else {
                swal('Warning', "The OTP is not valid. Please enter the valid OTP sent to your Email", "warning");
            }
           
            return response.data;
        });
    };


    self.GetVendorRatings = function (userid) {
        var params = {
            userid: userid,
            sessionid: self.getUserToken()
        }

        if (!self.getUserId()) {
            params.userid = store.get('userid');
        }

        return $http({
            method: 'GET',
            url: domain + 'getvendorratings?userid=' + params.userid + '&sessionid=' + params.sessionid,
            encodeURI: true,
            headers: { 'Content-Type': 'application/json' }
        }).then(function (response) {
            return response.data;
        }, function (result) {
        });
    }

    self.SaveVendorRating = function (vendorRating) {
        var data = {
            "rId": vendorRating.rId,
            "reqId": vendorRating.reqId,
            "reviewerId": self.getUserId(),
            "revieweeId": vendorRating.revieweeId,
            "delivery": vendorRating.rating.deliveryValue,
            "quality": vendorRating.rating.qualityValue,
            "emergency": vendorRating.rating.emergencyValue,
            "service": vendorRating.rating.serviceValue,
            "response": vendorRating.rating.responseValue,
            "comments": vendorRating.comments,
            "seesionId": self.getUserToken()
        };

        return $http({
            method: 'POST',
            url: domain + 'savevendorrating',
            headers: { 'Content-Type': 'application/json' },
            encodeURI: true,
            data: data
        }).then(function (response) {
            return response.data;
        });
    };



    self.getCompanyName = function () {

        let url = domain + 'getcompanyname';
        return httpServices.get(url);
    }


    self.isLoggedIn = function () {
        return (self.getUserToken() && self.getOtpVerified() && self.getDocsVerified()) ? true : false;
    };

    self.updateUser = function (params) {


        var params1 = {
            "user": {
                "userID": params.userID,
                "firstName": params.firstName,
                "lastName": params.lastName,
                "email": params.email,
                "phoneNum": params.phoneNum,
                "sessionID": params.sessionID,
                "isOTPVerified": params.isOTPVerified,
                "credentialsVerified": params.credentialsVerified,
                "errorMessage": params.errorMessage,
                "logoFile": params.logoFile ? params.logoFile : null,
                "logoURL": params.logoURL ? params.logoURL : "",
                "aboutUs": params.aboutUs ? params.aboutUs : "",
                "achievements": params.achievements ? params.achievements : "",
                "assocWithOEM": params.assocWithOEM ? params.assocWithOEM : false,
                "assocWithOEMFile": params.assocWithOEMFile ? params.assocWithOEMFile : null,
                "assocWithOEMFileName": params.assocWithOEMFileName ? params.assocWithOEMFileName : "",
                "clients": params.clients ? params.clients : "",
                "establishedDate": ("establishedDate" in params) ? params.establishedDate : '/Date(634334872000+0000)/',
                "products": params.products ? params.products : "",
                "strengths": params.strengths ? params.strengths : "",
                "responseTime": params.responseTime ? params.responseTime : "",
                "oemCompanyName": params.oemCompanyName ? params.oemCompanyName : "",
                "oemKnownSince": params.oemKnownSince ? params.oemKnownSince : "",
                "files": [],
                "profileFile": params.profileFile ? params.profileFile : null,
                "profileFileName": params.profileFileName ? params.profileFileName : "",
                "workingHours": params.workingHours ? params.workingHours : "",
                "directors": params.directors ? params.directors : "",
                "address": params.address ? params.address : "",
                "altEmail": params.altEmail,
                "altPhoneNum": params.altPhoneNum,
                "subcategories": params.subcategories,
                "Street1": params.Street1,
                "Street2": params.Street2,
                "Street3": params.Street3,
                "Country": params.Country,
                "State": params.State,
                "City": params.City,
                "PinCode": params.PinCode,
                "altMobile": params.altMobile,
                "landLine": params.landLine,
                "salesPerson": params.salesPerson,
                "salesContact": params.salesContact,
                "BankName": params.BankName,
                "BankAddress": params.BankAddress,
                "accntNumber": params.accntNumber,
                "Ifsc": params.Ifsc,
                "BussinessDet": params.BussinessDet,

                "gstAttachments": params.gstAttachments,
                "gstAttachmentName": params.gstAttachmentName,
                "panAttachments": params.panAttachments,
                "panAttachmentName": params.panAttachmentsName,
                "businessAttachments": params.businessAttachments,
                "businessAttachmentName": params.businessAttachmentName,
                "cancelledchequeattachments": params.cancelledchequeattachments,
                "cancelledchequeattachmentName": params.cancelledchequeattachmentName,
                "businessAttach": params.businessAttach,
                "panAttach": params.panAttach,
                "gstAttach": params.gstAttach,
                "cancelledCheque": params.cancelledCheque,
                "msmeAttach": params.msmeAttach,
                "msmeAttachments": params.msmeAttachments,
                "msmeAttachmentName": params.msmeAttachmentName,
                "additionalVendorInformation": params.additionalVendorInformation,
                "customerCopyLetterAttach": params.customerCopyLetterAttach,
                "customerCopyLetterAttachments": params.copyLetterAttachments,
                "customerCopyLetterAttachmentName": params.copyLetterAttachmentName,
                "PERSONAL_INFO_VALID": params.PERSONAL_INFO_VALID,
                "CONTACT_INFO_VALID": params.CONTACT_INFO_VALID,
                "BANK_INFO_VALID": params.BANK_INFO_VALID,
                "BUSINESS_INFO_VALID": params.BUSINESS_INFO_VALID,
                "CONFLICT_INFO_VALID": params.CONFLICT_INFO_VALID
            }
        }

        return $http({
            method: 'POST',
            url: domain + 'updateuserinfo',
            headers: { 'Content-Type': 'application/json' },
            encodeURI: true,
            data: params1
        }).then(function (response) {
            if (response && response.data && response.data.errorMessage == "" && response.data.objectID != 0) {
                store.set('sessionid', response.data.sessionID);
                store.set('verified', 0);
                store.set('emailverified', 0);
                store.set('credverified', 0);
                store.set('currentUser', response.data.userInfo);
                self.setCurrentUser();
                if (response.data.userInfo.isOTPVerified == 1 && response.data.userInfo.credentialsVerified == 1) {
                    store.set('verified', 1);
                } if (response.data.userInfo.isEmailOTPVerified == 1) {
                    store.set('emailverified', 1);
                }
                if (response.data.userInfo.credentialsVerified == 1) {
                    store.set('credverified', 1);
                } else {
                    store.set('credverified', 0);
                }
                if (response.data.objectID > 0) {

                    growlService.growl('User data has updated Successfully!', 'inverse');
                    //self.getProfileDetails();
                    //$state.go('pages.profile.profile-about');
                    if (params.ischangePhoneNumber == 1) {
                        //self.logout();
                        $state.go('login');
                        return responce.data;
                    } else {
                        $state.reload();
                    }
                    return "true";
                } else {
                    return "false";
                }
            } else if (response && response.data && response.data.errorMessage != "") {
                swal('Error', response.data.errorMessage, 'error');
                //store.set('emailverified', 1);
                return response.data.errorMessage;

            } else {
                return "Update failed";
            }
        }, function (result) {
            return "Update failed";
        });
    }







    self.UpdateUserProfileInfo = function (params) {
        return $http({
            method: 'POST',
            url: domain + 'updateuserprofileinfo',
            headers: { 'Content-Type': 'application/json' },
            encodeURI: true,
            data: params
        }).then(function (response) {
            return response.data;
        });
    }










    self.getProfileDetails = function (params) {
        return $http({
            method: 'GET',
            url: domain + 'getuserdetails?userid=' + params.userid + '&sessionid=' + params.sessionid,
            encodeURI: true,
            headers: { 'Content-Type': 'application/json' }
            //data: params
        }).then(function (response) {
            var list = {};
            if (response && response.data) {
                list = response.data;
                return list;
            } else {
                //console.log(response.data.errorMessage);
            }
        }, function (result) {
            //console.log(result);
        });
        $state.go('pages.profile.profile-about');
    };

    self.getGSTDetails = function (params) {
        let url = domain + 'searchgstin?gstin=' + params.gstin + "&year=" + params.year + "&sessionid=" + params.sessionid;
        return httpServices.get(url);
    };

    self.getUserDeptDesig = function (userid, sessionid) {
        return $http({
            method: 'GET',
            url: domain + 'getuserdeptdesig?userid=' + params.userid + '&sessionid=' + params.sessionid,
            encodeURI: true,
            headers: { 'Content-Type': 'application/json' }
            //data: params
        }).then(function (response) {
            var list = {};
            if (response && response.data) {
                list = response.data;
                return list;
            } else {
                //console.log(response.data.errorMessage);
            }
        }, function (result) {
            //console.log(result);
        });
    }

    self.updatePassword = function (params) {
        return $http({
            method: 'POST',
            url: domain + 'changepassword',
            headers: { 'Content-Type': 'application/json' },
            encodeURI: true,
            data: params
        }).then(function (response) {
            return response.data;
        });
    }


    self.AddVendorsExcel = function (params) {
        return $http({
            method: 'POST',
            url: domain + 'importentity',
            headers: { 'Content-Type': 'application/json' },
            encodeURI: true,
            data: params
        }).then(function (response) {
            return response.data;
        });
    }

    self.getuseraccess = function (userid, sessionid) {
        return $http({
            method: 'GET',
            url: domain + 'getuseraccess?userid=' + userid + '&sessionid=' + sessionid,
            encodeURI: true,
            headers: { 'Content-Type': 'application/json' }
        }).then(function (response) {
            return response.data;
        }, function (result) {
            console.log("date error");
        });
    }





    self.saveUserAccess = function (params) {
        return $http({
            method: 'POST',
            url: domain + 'saveuseraccess',
            headers: { 'Content-Type': 'application/json' },
            encodeURI: true,
            data: params
        }).then(function (response) {
            return response.data;
        });
    }




    self.getSubUsersData = function (params) {
        return $http({
            method: 'GET',
            url: domain + 'getsubuserdata?userid=' + params.userid + '&sessionid=' + params.sessionid,
            encodeURI: true,
            headers: { 'Content-Type': 'application/json' }
            //data: params
        }).then(function (response) {
            return response.data;
        });
    };

    self.getCompanyUsers = function (params) {
        return $http({
            method: 'GET',
            url: domain + 'getcompanyuserdata?compId=' + params.compId + '&sessionId=' + params.sessionId,
            encodeURI: true,
            headers: { 'Content-Type': 'application/json' }
            //data: params
        }).then(function (response) {
            return response.data;
        });
    };



    self.updateVerified = function (verified) {
        store.set('credverified', verified);
    }

    self.GetPRMFieldMappingDetails = function (template) {
        var objList = [];
        var def = $q.defer();
        if (self.companyFieldTemplates && self.companyFieldTemplates.length > 0) {
            objList = _.filter(self.companyFieldTemplates, function (o) {
                return o.TEMPLATE_NAME === template;
            });

            def.resolve(objList);
        } else {
            var tempParams = {
                compid: self.getUserCompanyId(),
                templatename: '',
                sessionId: self.getUserToken()
            };
            let url = 'custom-fields/svc/PRMCustomFieldService.svc/REST/getprmfieldmappingdetails?compid=' + self.getUserCompanyId() + '&templatename=' + tempParams.templatename
                + '&sessionid=' + self.getUserToken();
            httpServices.get(url)
                .then(function (response) {
                    if (response && response.length > 0) {
                        self.companyFieldTemplates = response;
                        objList = _.filter(self.companyFieldTemplates, function (o) {
                            return o.TEMPLATE_NAME === template;
                        });
                        def.resolve(objList);
                    }
                });
        }

        return def.promise;
    };

    self.checkUserUniqueResult = function (uniquevalue, idtype, onlyvendor, onlycustomer) {
        var data = {
            "phone": uniquevalue,
            "idtype": idtype,
            'onlyvendor': onlyvendor,
            'onlycustomer': onlycustomer
        };
        return $http({
            method: 'POST',
            url: domain + 'checkuserifexists',
            headers: { 'Content-Type': 'application/json' },
            encodeURI: true,
            data: data
        }).then(function (response) {
            return response.data.CheckUserIfExistsResult;
        });
    };

    self.login = function (user) {
        var loginMethod = "loginuser";
        if (user && user.userhashtoken && user.source == "DESKTOP_CONTAINER") {
            loginMethod = "loginuserbyhash";
        }

        if (user && user.ssotoken && user.ssoType === 'JWT') {
            loginMethod = "loginjwtsso";
        }
        if (user && user.ssotoken && user.ssoType === 'AD') {
            loginMethod = "loginadsso";
        }

        return $http({
            method: 'POST',
            url: domain + loginMethod,
            headers: { 'Content-Type': 'application/json' },
            encodeURI: true,
            data: user
        }).then(function (response) {
            if (response && response.data && response.data.errorMessage == "" && response.data.objectID != 0) {
                store.set('sessionid', response.data.sessionID);
                store.set('userhashtoken', response.data.userHashToken);
                store.set('authentication', response.data.jwtToken);
                store.set('verified', 0);
                store.set('emailverified', 0);
                store.set('currentUser', response.data.userInfo);
                self.setCurrentUser();
                if (response.data.userInfo.isOTPVerified == 1) {
                    store.set('verified', 1);
                }
                else if (response.data.userInfo.isOTPVerified == 0) {
                    self.resendotp(response.data.objectID);
                }
                if (response.data.userInfo.credentialsVerified == 1) {
                    store.set('credverified', 1);
                } else {
                    store.set('credverified', 0);
                }
                if (response.data.userInfo.isEmailOTPVerified == 1) {
                    store.set('emailverified', 1);
                }
                else if (response.data.userInfo.isEmailOTPVerified == 0) {
                    self.resendemailotp(response.data.objectID);
                }

                $rootScope.entitlements = [];
                self.getuseraccess(response.data.objectID, response.data.sessionID)
                    .then(function (responseObj) {
                        if (responseObj && responseObj.length > 0) {
                            $rootScope.entitlements = responseObj;
                            store.set('localEntitlement', $rootScope.entitlements);
                        }

                    });

                $rootScope.rootDeptDesig = [];

                $rootScope.rootDetails = {
                    userId: response.data.objectID,
                    sessionId: response.data.sessionID,
                    deptId: 0,
                    deptCode: '',
                    deptTypeID: 0,
                    desigId: 0,
                    desigCode: '',
                    desigTypeID: 0
                };

                if (self.getUserType() == "CUSTOMER") {
                    //Load company templates
                    var tempParams = {
                        compid: self.getUserCompanyId(),
                        templatename: '',
                        sessionId: self.getUserToken()
                    };
                    let url = 'custom-fields/svc/PRMCustomFieldService.svc/REST/getprmfieldmappingdetails?compid=' + self.getUserCompanyId() + '&templatename=' + tempParams.templatename
                        + '&sessionid=' + self.getUserToken();
                    httpServices.get(url)
                        .then(function (response) {
                            if (response && response.length > 0) {
                                self.companyFieldTemplates = response;
                            }
                        });
                    //^load company templates
                    auctionsService.GetUserDepartmentDesignations(response.data.objectID, response.data.sessionID)
                        .then(function (responseDeptDesig) {
                            if (responseDeptDesig && responseDeptDesig.length > 0) {
                                var ListUserDepartmentDesignations = responseDeptDesig;

                                ListUserDepartmentDesignations = ListUserDepartmentDesignations.filter(function (udd) {
                                    return (udd.isAssignedToUser == true);
                                });

                                store.set('localDeptDesig', ListUserDepartmentDesignations);
                                store.set('ListUserDepartmentDesignations', ListUserDepartmentDesignations);

                                if (ListUserDepartmentDesignations && ListUserDepartmentDesignations.length > 0) {

                                    ListUserDepartmentDesignations = ListUserDepartmentDesignations.filter(function (d) {
                                        return (d.isAssignedToUser == true && d.isValid == true)
                                    });

                                    store.set('SelectedUserDepartmentDesignation', ListUserDepartmentDesignations[0]);
                                }
                                else {
                                    growlService.growl('You are not assigned to any Departments and designation.', 'inverse');
                                }


                                console.log('ListUserDepartmentDesignations-------------------------->START');
                                console.log(ListUserDepartmentDesignations);
                                console.log('ListUserDepartmentDesignations-------------------------->END');

                            }

                            $state.go('home');
                        });
                }
                else {
                    $state.go('home');
                }
               

                return response.data;
            } else if (response && response.data && response.data.errorMessage != "") {
                return response.data;
            } else {
                return "Login failed";
            }
        }, function (result) {
            return "Login failed";
        });
    };

    self.logout = function () {
        return $http({
            method: 'POST',
            data: { "userID": self.getUserId(), "sessionID": self.userData.sessionid },
            url: domain + 'logoutuser',
            headers: { 'Content-Type': 'application/json' }
        }).then(function (response) {
            self.removeUserHashToken();
            self.removeJWTToken();
            self.removeUserToken();
            self.removeLocalEntitlement();
            self.removeUser();
            self.removeLocalDeptDesig();
            self.removeListUserDepartmentDesignations();
            self.removeSelectedUserDepartmentDesignation();
            delete self.userData.sessionid;
            delete self.userData.currentUser;
            self.setMessage("Successfully Logged Out");
            $state.go('login');
        });
    };

    self.deleteUser = function (params) {
        params.sessionID = self.getUserToken();
        return $http({
            method: 'POST',
            data: params,
            url: domain + 'deleteuser',
            headers: { 'Content-Type': 'application/json' }
        }).then(function (response) {
            return response.data;
        });
    }


    self.resetPwd = function (params) {
        params.sessionID = self.getUserToken();
        return $http({
            method: 'POST',
            data: params,
            url: domain + 'resetpwd',
            headers: { 'Content-Type': 'application/json' }
        }).then(function (response) {
            return response.data;
        });
    }

    self.activateUser = function (params) {
        params.sessionID = self.getUserToken();
        return $http({
            method: 'POST',
            data: params,
            url: domain + 'activateuser',
            headers: { 'Content-Type': 'application/json' }
        }).then(function (response) {
            return response.data;
        });
    }

    self.activatecompanyvendor = function (params) {
        params.sessionID = self.getUserToken();
        return $http({
            method: 'POST',
            data: params,
            url: domain + 'activatecompanyvendor',
            headers: { 'Content-Type': 'application/json' }
        }).then(function (response) {
            return response.data;
        });
    }


    self.approveVendorThroughWorkflow = function (params) {
        params.sessionID = self.getUserToken();
        return $http({
            method: 'POST',
            data: params,
            url: domain + 'approveVendorThroughWorkflow',
            headers: { 'Content-Type': 'application/json' }
        }).then(function (response) {
            return response.data;
        });
    }

    self.blockUnBlockVendor = function (params) {
        params.sessionID = self.getUserToken();
        return $http({
            method: 'POST',
            data: params,
            url: domain + 'blockUnBlockVendor',
            headers: { 'Content-Type': 'application/json' }
        }).then(function (response) {
            return response.data;
        });
    }


    self.acceptRegistration = function (params) {
        params.sessionID = self.getUserToken();
        return $http({
            method: 'POST',
            data: params,
            url: domain + 'acceptRegistration',
            headers: { 'Content-Type': 'application/json' }
        }).then(function (response) {
            return response.data;
        });
    }

    self.updateCompanyStatus = function (data) {
        data.sessionID = self.getUserToken();
        return $http({
            method: 'POST',
            data: data,
            url: domain + 'VendorStatusUpdate',
            headers: { 'Content-Type': 'application/json' }
        }).then(function (response) {
            return response.data;
        });
    }

    self.updateVendorScore = function (data) {
        data.sessionID = self.getUserToken();
        return $http({
            method: 'POST',
            data: data,
            url: vendorDomian + 'vendor/save_score',
            headers: { 'Content-Type': 'application/json' }
        }).then(function (response) {
            return response.data;
        });
    }

    self.calculateScore = function (userId) {
        return httpServices.get(vendorDomian + "vendor/calculate_score/" + userId);
    }

    self.getVendorScore = function (userId) {
        return httpServices.get(vendorDomian + "vendor/get_score/" + userId);
    }

    self.getVendorSubCategories = function (userId) {
        return httpServices.get(vendorDomian + "vendor/get_vendor_subcategory/" + userId);
    }

    self.saveVendorStatus = function (data) {
        return httpServices.post(vendorDomian + "vendor/save_status/",data);
    }

    self.getVendorPreQualificationDetails = function (params) {
        return httpServices.get(vendorDomian + "getVendorPreQualificationDetails?vendorId=" + params.vendorId + "&compId=" + params.compId + "&sessionId=" + store.get('sessionid'));
    }

    self.userregistration = function (register) {
        var params = {
            "userInfo": {
                "userID": "0",
                "firstName": register.firstName,
                "lastName": register.lastName,
                "email": register.email,
                "phoneNum": register.phoneNum,
                "username": register.username,
                "password": register.password,
                "birthday": '/Date(634334872000+0000)/',
                "userType": "CUSTOMER",
                "isOTPVerified": 0,
                "category": "",
                "institution": ("institution" in register) ? register.institution : "",
                "addressLine1": "",
                "addressLine2": "",
                "addressLine3": "",
                "city": "",
                "state": "",
                "country": "",
                "zipCode": "",
                "addressPhoneNum": "",
                "extension1": "",
                "extension2": "",
                "userData1": "",
                "registrationScore": 0,
                "errorMessage": "",
                "sessionID": "",
            }
        };

        return $http({
            method: 'POST',
            url: domain + 'registeruser',
            encodeURI: true,
            headers: { 'Content-Type': 'application/json' },
            data: params
        }).then(function (response) {
            if (response && response.data && response.data.errorMessage == "" && response.data.objectID != 0) {
                store.set('sessionid', response.data.sessionID);
                store.set('verified', 0);
                store.set('emailverified', 0);
                store.set('credverified', 0);
                if (response.data.userInfo.credentialsVerified == 1) {
                    store.set('credverified', 1);
                } else {
                    store.set('credverified', 0);
                }
                store.set('currentUser', response.data.userInfo);
                self.setCurrentUser();
                return response.data;
                //growlService.growl('Welcome to PRM360, To add new requirement Please use "+" at the Bottom.', 'inverse');
            } else if (response && response.data && response.data.errorMessage != "") {
                return response.data.errorMessage;
            } else {
                return "Registeration failed";
            }
        }, function (result) {
            return "Registeration failed";
        });
    };

    self.vendorregistration = function (vendorregisterobj) {
        var vendorcat = [];
        vendorcat.push(vendorregisterobj.category);

        var params = {
            "vendorInfo": {
                "firstName": vendorregisterobj.firstName,
                "lastName": vendorregisterobj.lastName,
                "email": vendorregisterobj.email,
                "contactNum": vendorregisterobj.phoneNum,
                "username": vendorregisterobj.username,
                "password": vendorregisterobj.password,
                "institution": vendorregisterobj.institution ? vendorregisterobj.institution : "",
                "rating": 1,
                "isOTPVerified": 0,
                "category": vendorcat,
                "panNum": ("panno" in vendorregisterobj) ? vendorregisterobj.panno : "",
                "serviceTaxNum": ("taxno" in vendorregisterobj) ? vendorregisterobj.taxno : "",
                "vatNum": ("vatno" in vendorregisterobj) ? vendorregisterobj.vatno : "",
                "referringUserID": 0,
                "knownSince": "",
                "errorMessage": "",
                "sessionID": ""
            }
        };
        return $http({
            method: 'POST',
            url: domain + 'addnewvendor',
            encodeURI: true,
            headers: { 'Content-Type': 'application/json' },
            data: params
        }).then(function (response) {
            if (response && response.data && response.data.errorMessage == "") {
                store.set('sessionid', response.data.sessionID);
                store.set('currentUser', response.data);
                store.set('verified', 0);
                store.set('emailverified', 0);
                store.set('credverified', 0);
                if (response.data.credentialsVerified == 1) {
                    store.set('credverified', 1);
                } else {
                    store.set('credverified', 0);
                }
                self.setCurrentUser();
                return response.data;
                //growlService.growl('Welcome to PRM360, Please go thourgh with your Auctions', 'inverse');
                //return "Vendor Registration Successfully Completed.";
            } else if (response && response.data && response.data.errorMessage != "") {
                return response.data.errorMessage;
            } else {
                return "Vendor Registration Failed.";
            }
        });
    };


    self.vendorregistration1 = function (vendorregisterobj) {
        var vendorcat = [];
        vendorcat.push(vendorregisterobj.category);
        //console.log(vendorregisterobj.role);



        var companyId = 0;
        if (vendorregisterobj.role == 'VENDOR') {
            if (store.get('currentUser') != undefined) {
                companyId = store.get('currentUser').companyId;
            } else {
                companyId = 1443;
            }
        }


        var params = {
            "register": {
                "firstName": vendorregisterobj.firstName,
                "lastName": vendorregisterobj.lastName,
                "email": vendorregisterobj.email,
                "phoneNum": vendorregisterobj.phoneNum,
                "username": vendorregisterobj.username,
                "password": vendorregisterobj.password,
                "companyName": vendorregisterobj.institution ? vendorregisterobj.institution : "",
                "isOTPVerified": 0,
                "category": vendorregisterobj.category ? vendorregisterobj.category : "",
                "userType": vendorregisterobj.role,
                "panNumber": ("panno" in vendorregisterobj) ? vendorregisterobj.panno : "",
                "stnNumber": ("taxno" in vendorregisterobj) ? vendorregisterobj.taxno : "",
                "vatNumber": ("vatno" in vendorregisterobj) ? vendorregisterobj.vatno : "",
                "referringUserID": companyId,
                "knownSince": "",
                "errorMessage": "",
                "sessionID": "",
                "userID": 0,
                "department": "",
                "currency": vendorregisterobj.currency,
                "timeZone": vendorregisterobj.timeZone,
                "subcategories": vendorregisterobj.subcategories ? vendorregisterobj.subcategories : [],
                "additionalData": JSON.stringify(vendorregisterobj.additionalData),
                "attachments": JSON.stringify(vendorregisterobj.attachments)

            }
        };
        return $http({
            method: 'POST',
            url: domain + 'register',
            encodeURI: true,
            headers: { 'Content-Type': 'application/json' },
            data: params
        }).then(function (response) {
            if (response && response.data && response.data.errorMessage == "") {
                store.set('sessionid', response.data.sessionID);
                store.set('currentUser', response.data.userInfo);
                store.set('verified', 0);
                store.set('emailverified', 0);
                store.set('credverified', 0);
                if (response.data.credentialsVerified == 1) {
                    store.set('credverified', 1);
                } else {
                    store.set('credverified', 0);
                }
                self.setCurrentUser();
                return response.data;
                //growlService.growl('Welcome to PRM360, Please go thourgh with your Auctions', 'inverse');
                //return "Vendor Registration Successfully Completed.";
            } else if (response && response.data && response.data.errorMessage != "") {
                return response.data.errorMessage;
            } else {
                return "Vendor Registration Failed.";
            }
        });
    };




    self.addnewuser = function (addnewuserobj) {
        var referringUserID = 0;
        referringUserID = self.getUserId();
        var params = {
            "register": {
                "firstName": addnewuserobj.firstName,
                "lastName": addnewuserobj.lastName,
                "email": addnewuserobj.email,
                "phoneNum": addnewuserobj.phoneNum,
                "username": addnewuserobj.email,
                "password": addnewuserobj.phoneNum,
                "companyName": addnewuserobj.companyName ? addnewuserobj.companyName : "",
                "isOTPVerified": 0,
                "category": addnewuserobj.category ? addnewuserobj.category : "",
                "userType": addnewuserobj.userType,
                "panNumber": "",
                "stnNumber": "",
                "vatNumber": "",
                "referringUserID": referringUserID,
                "knownSince": "",
                "errorMessage": "",
                "sessionID": addnewuserobj.isEditUser ? self.getUserToken() : "",
                "userID": addnewuserobj.isEditUser ? addnewuserobj.userID : 0,
                "department": "",
                "currency": addnewuserobj.currency,
                "userLoginID": addnewuserobj.loginid,
                "userPassword": addnewuserobj.password1,
                "isSubUser": addnewuserobj.isSubUser,
                "isEditUser": addnewuserobj.isEditUser,
                "sapUserId": addnewuserobj.sapUserId,
                "validityFrom": addnewuserobj.validityFrom,
                "validityTo": addnewuserobj.validityTo,
                "oldEmail": addnewuserobj.oldEmail,
                "oldPhone": addnewuserobj.oldPhone,
                "accessAllProjects": addnewuserobj.accessAllProjects
            }
        };

        return $http({
            method: 'POST',
            url: domain + 'register',
            encodeURI: true,
            headers: { 'Content-Type': 'application/json' },
            data: params
        }).then(function (response) {
            return response.data;
        });
    };


    self.GetCompanyVendors = function (params) {
        if (!params.PRODUCT_ID) {
            params.PRODUCT_ID = 0;
        }
        if (!params.IS_FROM) {
            params.IS_FROM = 'VENDORS';
        }
        if (!params.CHECKED_VENDORS) {
            params.CHECKED_VENDORS = '';
        }
        return $http({
            method: 'GET',
            url: domain + 'getcompanyvendors?userid=' + params.userID + '&sessionid=' + params.sessionID + '&PageSize=' + params.PageSize + '&NumberOfRecords=' + params.NumberOfRecords + '&searchString=' + params.searchString
                + '&PRODUCT_ID=' + params.PRODUCT_ID + '&IS_FROM=' + params.IS_FROM + '&CHECKED_VENDORS=' + params.CHECKED_VENDORS ,
            encodeURI: true,
            headers: { 'Content-Type': 'application/json' }
            //data: params
            //  + '&Categories=' + params.Categories
        }).then(function (response) {
            var list = {};
            if (response && response.data) {
                list = response.data;
                return list;
            } else {
                //console.log(response.data.errorMessage);
            }
        }, function (result) {
            //console.log(result);
        });
    }


    self.toUTCTicks = function (local) {
        var d = new Date();
        var n = d.getTimezoneOffset();
        n = n + 330;
        n = n * 60000;
        var ticks = moment(moment.utc(moment(local, "DD-MM-YYYY HH:mm")).format("MM-DD-YYYY HH:mm").valueOf(), "MM-DD-YYYY HH:mm").valueOf();
        ticks = parseInt(ticks) - n;
        return ticks;
    }

    self.toLocalDate = function (utc) {
        var utcTemp = moment.tz(utc, "UTC").add(330, 'minutes');
        return moment.utc(new moment(utcTemp).format("DD-MM-YYYY HH:mm"), "DD-MM-YYYY HH:mm").local().format('DD-MM-YYYY HH:mm');
    }
    self.toLocalDateFormat1 = function (utc) {
        var utcTemp = moment.tz(utc, "UTC").add(330, 'minutes');
        return moment.utc(new moment(utcTemp).format("DD-MMM-YYYY hh:mm A"), "DD-MMM-YYYY hh:mm A").local().format('DD-MMM-YYYY hh:mm A');
    }

    self.toLocalDateOnly = function (utc) {
        var utcTemp = moment.tz(utc, "UTC").add(330, 'minutes');
        return moment.utc(new moment(utcTemp).format("DD-MM-YYYY"), "DD-MM-YYYY").local().format('DD-MM-YYYY');
    }

    self.toOnlyDate = function (utc) {
        var utcTemp = moment.tz(utc, "UTC").add(330, 'minutes');
        return moment.utc(new moment(utcTemp).format("DD-MM-YYYY HH:mm"), "DD-MM-YYYY HH:mm").local().format('DD-MM-YYYY');
    }

    self.GetUserBranches = function (params) {
        return $http({
            method: 'GET',
            url: PRMCompanyDomain + 'userbranches?uid=' + params.userid + '&sessionid=' + params.sessionid,
            encodeURI: true,
            headers: { 'Content-Type': 'application/json' }
            //data: params
        }).then(function (response) {
            var list = {};
            if (response && response.data) {
                list = response.data;
                return list;
            } else {
                //console.log(response.data.errorMessage);
            }
        }, function (result) {
            //console.log(result);
        });
    }

    self.isCatalogueEnabled = function () {
        return self.getUserObj().isCatalogueEnabled;
    };

    self.getCatalogueCompanyId = function () {
        return self.getUserObj().catalogueCompanyId;
    };

    self.getUserSelectedBranch = function () {
        var currentBranchId = 0;
        if (store.get('selecteduserbranch')) {
            currentBranchId = store.get('selecteduserbranch');
        }

        return currentBranchId;
    };

    self.setUserSelectedBranch = function (branchId) {
        store.set('selecteduserbranch', branchId);
    };

    self.getToDoData = function (params) {
        let url = domain + 'getToDoData?myDate=' + params.myDate + '&userId=' + params.userId + '&userType=' + params.userType + '&sessionid=' + self.getUserToken();
        
        return httpServices.get(url);
    };


    self.NegotiationStatus = function (type, status) {

        if (type == 'CUSTOMER') {
            if (status == 'UNCONFIRMED') {
                status = 'Quotations pending';
            } else if (status == 'NOTSTARTED') {
                status = 'Negotiation scheduled';
            } else if (status == 'STARTED') {
                status = 'Negotiation started';
            } else if (status == 'Negotiation Ended') {
                status = 'Negotiation Closed';
            } else if (status == 'DELETED') {
                status = 'Negotiation Cancelled';
            } else if (status == 'Qcs Pending') {
                status = 'QCS Pending';
            } else if (status == 'Qcs Created') {
                status = 'QCS Created';
            } else if (status == 'Qcs Approved') {
                status = 'QCS Approved';
            } else if (status == 'Qcs Approval Pending') {
                status = 'QCS Approval Pending';
            } else if (status == 'Quotations Received') {
                status = 'Quotations Received';
            } else if (status == 'Saved As Draft') {
                status = 'Saved As Draft';
            } else if (status == 'Closed') {
                status = 'Closed';
            }
        } else if (type == 'VENDOR') {
            if (status == 'NEW') {
                status = 'NEW'; // / WIP
            } else if (status == 'NOTSTARTED') {
                status = 'Negotiation scheduled';
            } else if (status == 'STARTED') {
                status = 'Negotiation started';
            } else if (status == 'Negotiation Ended') {
                status = 'Negotiation Closed';
            } else if (status == 'DELETED') {
                status = 'Negotiation Cancelled';
            } else if (status == 'WIP') {
                status = 'WIP';
            } else if (status == 'Closed') {
                status = 'Closed';
            }
        }

        return status;
    };


    self.getUserNotifications = function () {
        var params = {
            userid: self.getUserId(),
            sessionid: self.getUserToken()
        };

        let url = domain + 'getuseralerts?uid=' + params.userid + '&sessionid=' + self.getUserToken();
        return httpServices.get(url);
    };

    self.validateUserLogin = function (params) {
        var data = {
            'userLoginId': (params.userLoginId || ''),
            'email': (params.email || ''),
            'phone': (params.phone || '')
        };

        let url = domain + 'validateuserlogin';
        return httpServices.post(url, data);
    };

    self.getUserSettings = function (params) {
        if (!self.userSettings || self.userSettings.length <= 0) {
            return $http({
                method: 'GET',
                url: domain + 'getusersetings?userid=' + params.userid + '&sessionid=' + params.sessionid,
                encodeURI: true,
                headers: { 'Content-Type': 'application/json' }
            });
        } else {
            return $q(function (resolve, reject) {
                resolve({ 'data': self.userSettings });
            });
        }
    };

};