prmApp
    .service('itemTableServices', ["domain", "userService", "httpServices",
        function (domain, userService, httpServices) {
            var itemTableService = this;
            var customerTable = {
                unconfirmed: {
                    companyName: "Company Name"
                }
            }

            var vendorDiscountBiddingTable = {
                "unconfirmed": {
                    "productNameOrID": {
                        "displayName": "Product Name",
                        "type": "string",
                        "disabled": false
                    },
                    "productNo": {
                        "displayName": "Product No.",
                        "type": "string",
                        "disabled": false
                    },
                    "productBrand": {
                        "displayName": "Brand",
                        "type": "string",
                        "disabled": "auctionItem.auctionVendors[0].isQuotationRejected == 0"
                    },
                    "productQuantity": {
                        "displayName": "Quantity",
                        "type": "number",
                        "disabled": true
                    },
                    "productQuantityIn": {
                        "displayName": "Units",
                        "type": "string",
                        "disabled": true
                    },
                    "Gst": {
                        "displayName": "GST",
                        "type": "percentage",
                        "disabled": "auctionItem.auctionVendors[0].isQuotationRejected == 0"
                    },
                    "unitPrice": {
                        "displayName": "Unit Price",
                        "type": "currency",
                        "disabled": "auctionItem.auctionVendors[0].isQuotationRejected == 0"
                    },
                    "unitDiscount": {
                        "displayName": "Discount (%)",
                        "type": "percentage",
                        "disabled": "auctionItem.auctionVendors[0].isQuotationRejected == 0"
                    },
                    "totalItemPrice": {
                        "displayName": "Total Item Price",
                        "type": "currency",
                        "disabled": true
                    },
                    "revUnitDiscount": {
                        "displayName": "Rev. Discount (%)",
                        "type": "percentage",
                        "disabled": true
                    },
                    "revItemPrice": {
                        "displayName": "Revised Item Price",
                        "type": "currency",
                        "disabled": true
                    }
                }
            }

            var vendorMarginBiddingTable = {
                unconfirmed: {
                    "productNameOrID": "Product Name",
                    "productNo": "HSN Code",
                    "productBrand": "Brand",
                    "productQuantity": "Quantity",
                    "costPrice": "Cost Price",
                    "Gst": "GST",
                    "unitMRP": "MRP",
                    "netPrice": "Net Price",
                    "marginAmount": "Margin Amount",
                    "unitDiscount": "Margin (%)",
                    "totalItemPrice": "Total Item Price",
                    "revUnitDiscount": "Rev. Discount (%)",
                    "revItemPrice": "Revised Item Price"
                }
            }

            itemTableService.getTableColumns = function (status, biddingType) {
                if (userService.getUserType() == "CUSTOMER") {
                    return customerTable[status];
                } else {
                    if (biddingType == 1) {
                        return vendorDiscountBiddingTable[status];
                    } else if (biddingType == 2) {
                        return vendorMarginBiddingTable[status];
                    } else if (biddingType == 0) {
                        return vendorNonUnitBiddingTable[status];
                    }

                }
            };

            return itemTableService;
        }]);