prmApp
    .service('reportingService', ["reportingDomain", "userService", "httpServices", "$window", function (reportingDomain, userService, httpServices, $window) {
        //var domain = 'http://182.18.169.32/services/';
        var reportingService = this;

        reportingService.getLiveBiddingReport = function (reqID) {
            let url = reportingDomain + 'getlivebiddingreport?reqid=' + reqID + '&sessionid=' + userService.getUserToken();
            return httpServices.get(url);
        };

        reportingService.getItemWiseReport = function (reqID) {
            let url = reportingDomain + 'getitemwisereport?reqid=' + reqID + '&sessionid=' + userService.getUserToken();
            return httpServices.get(url);
        };

        reportingService.getDeliveryDateReport = function (reqID) {
            let url = reportingDomain + 'deliverytimelinereport?reqid=' + reqID + '&sessionid=' + userService.getUserToken();
            return httpServices.get(url);
        };

        reportingService.getPaymentTermsReport = function (reqID) {
            let url = reportingDomain + 'paymenttermsreport?reqid=' + reqID + '&sessionid=' + userService.getUserToken();
            return httpServices.get(url);
        };

        reportingService.GetReqDetails = function (reqID) {
            let url = reportingDomain + 'getreqdetails?reqid=' + reqID + '&sessionid=' + userService.getUserToken();
            return httpServices.get(url);
        };

        reportingService.GetNewQuotations = function (params) {
            let url = reportingDomain + 'GetNewQuotations';
            return httpServices.post(url, params);
        };

        reportingService.DeleteQcs = function (params) {
            let url = reportingDomain + 'DeleteQcs';
            return httpServices.post(url, params);
        };

        reportingService.ActivateQcs = function (params) {
            let url = reportingDomain + 'ActivateQcs';
            return httpServices.post(url, params);
        };

        reportingService.downloadTemplate = function (template, userid, reqid, templateid, subPackageIds, projectId) {
            if (!templateid) {
                templateid = 0;
            }

            if (!subPackageIds) {
                subPackageIds = 0;
            }

            if (!projectId) {
                projectId = 0;
            }

            let url = reportingDomain + 'gettemplates?template=' + template + '&userid=' + userid + '&reqid=' + reqid + '&compID=' + userService.getUserCompanyId() + '&templateid=' + templateid + '&sessionid=' + userService.getUserToken() + '&subPackageIds=' + subPackageIds + '&projectId=' + projectId;
            let data = httpServices.get(url).then(function (response) {
                if (response) {
                    response = b64toBlob(response, "application/ vnd.openxmlformats - officedocument.spreadsheetml.sheet");
                    var linkElement = document.createElement('a');
                    try {
                        //var blob = b64toBlob(response, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");//new Blob([response], { type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" });
                        var url = $window.URL.createObjectURL(response);

                        linkElement.setAttribute('href', url);
                        linkElement.setAttribute("download", template + ".xlsx");

                        var clickEvent = new MouseEvent("click", {
                            "view": window,
                            "bubbles": true,
                            "cancelable": false
                        });
                        linkElement.dispatchEvent(clickEvent);
                    }
                    catch (ex) { }
                    return response;
                }
            }, function (result) {
                //console.log("date error");
            });
        };

        reportingService.downloadConsolidatedTemplate = function (template, from, to, userid) {
            let url = reportingDomain + 'getconsolidatedtemplates?template=' + template + '&from=' + from + '&to=' + to + '&userid=' + userid + '&sessionid=' + userService.getUserToken();
            let data = httpServices.get(url).then(function (response) {
                if (response) {
                    response = b64toBlob(response, "application/ vnd.openxmlformats - officedocument.spreadsheetml.sheet");
                    var linkElement = document.createElement('a');
                    try {
                        //var blob = b64toBlob(response, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");//new Blob([response], { type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" });
                        var url = $window.URL.createObjectURL(response);

                        linkElement.setAttribute('href', url);
                        linkElement.setAttribute("download", template + ".xlsx");

                        var clickEvent = new MouseEvent("click", {
                            "view": window,
                            "bubbles": true,
                            "cancelable": false
                        });
                        linkElement.dispatchEvent(clickEvent);
                    }
                    catch (ex) { }
                    return response;
                }
            }, function (result) {
                //console.log("date error");
            });
        };

        reportingService.GetUserLoginTemplates = function (template, from, to, userid) {
            let url = reportingDomain + 'GetUserLoginTemplates?template=' + template + '&from=' + from + '&to=' + to + '&userid=' + userid + '&sessionid=' + userService.getUserToken();
            let data = httpServices.get(url).then(function (response) {
                if (response) {
                    response = b64toBlob(response, "application/ vnd.openxmlformats - officedocument.spreadsheetml.sheet");
                    var linkElement = document.createElement('a');
                    try {
                        //var blob = b64toBlob(response, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");//new Blob([response], { type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" });
                        var url = $window.URL.createObjectURL(response);

                        linkElement.setAttribute('href', url);
                        linkElement.setAttribute("download", template + ".xlsx");

                        var clickEvent = new MouseEvent("click", {
                            "view": window,
                            "bubbles": true,
                            "cancelable": false
                        });
                        linkElement.dispatchEvent(clickEvent);
                    }
                    catch (ex) { }
                    return response;
                }
            }, function (result) {
                //console.log("date error");
            });
        };

        reportingService.GetLogsTemplates = function (template, from, to, companyID) {
            let url = reportingDomain + 'GetLogsTemplates?template=' + template + '&from=' + from + '&to=' + to + '&companyID=' + companyID + '&sessionid=' + userService.getUserToken();
            let data = httpServices.get(url).then(function (response) {
                if (response) {
                    response = b64toBlob(response, "application/ vnd.openxmlformats - officedocument.spreadsheetml.sheet");
                    var linkElement = document.createElement('a');
                    try {
                        //var blob = b64toBlob(response, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");//new Blob([response], { type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" });
                        var url = $window.URL.createObjectURL(response);

                        linkElement.setAttribute('href', url);
                        linkElement.setAttribute("download", template + ".xlsx");

                        var clickEvent = new MouseEvent("click", {
                            "view": window,
                            "bubbles": true,
                            "cancelable": false
                        });
                        linkElement.dispatchEvent(clickEvent);
                    }
                    catch (ex) { }
                    return response;
                }
            }, function (result) {
                //console.log("date error");
            });
        };

        function b64toBlob(b64Data, contentType, sliceSize) {
            contentType = contentType || '';
            sliceSize = sliceSize || 512;

            var byteCharacters = atob(b64Data);
            var byteArrays = [];

            for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
                var slice = byteCharacters.slice(offset, offset + sliceSize);

                var byteNumbers = new Array(slice.length);
                for (var i = 0; i < slice.length; i++) {
                    byteNumbers[i] = slice.charCodeAt(i);
                }

                var byteArray = new Uint8Array(byteNumbers);

                byteArrays.push(byteArray);
            }

            var blob = new Blob(byteArrays, { type: contentType });
            return blob;
        }

        reportingService.GetReqReportForExcel = function (reqid, sessionid) {
            let url = reportingDomain + 'getreqreportforexcel?reqid=' + reqid + '&sessionid=' + sessionid;
            return httpServices.get(url);
        };

        reportingService.GetReqItemWiseVendors = function (reqid, sessionid) {
            let url = reportingDomain + 'getReqItemWiseVendors?reqid=' + reqid + '&sessionid=' + sessionid;
            return httpServices.get(url);
        };

        reportingService.getConsolidatedReport = function (fromDate, toDate) {
            let url = reportingDomain + 'getconsolidatedreports?from=' + fromDate + '&to=' + toDate + '&userid=' + userService.getUserId() + '&sessionid=' + userService.getUserToken();
            return httpServices.get(url);
        };

        reportingService.getLogisticConsalidatedReport = function (fromDate, toDate) {
            let url = reportingDomain + 'getLogisticConsolidatedreports?from=' + fromDate + '&to=' + toDate + '&userid=' + userService.getUserId() + '&sessionid=' + userService.getUserToken();
            return httpServices.get(url);
        };

        reportingService.getconsolidatedbasepricereport = function (fromDate, toDate) {
            let url = reportingDomain + 'getconsolidatedbasepricereports?from=' + fromDate + '&to=' + toDate + '&userid=' + userService.getUserId() + '&sessionid=' + userService.getUserToken();
            return httpServices.get(url);
        };

        reportingService.GetAccountingConsolidatedReports = function (fromDate, toDate) {
            let url = reportingDomain + 'getAccountingConsolidatedreports?from=' + fromDate + '&to=' + toDate + '&userid=' + userService.getUserId() + '&sessionid=' + userService.getUserToken();
            return httpServices.get(url);
        };

        //#region QCS

        reportingService.GetQCSDetails = function (params) {
            let url = reportingDomain + 'qcsdetails?qcsid=' + params.qcsid + '&sessionid=' + userService.getUserToken();
            return httpServices.get(url);
        };

        reportingService.GetQcsPRDetails = function (params) {
            let url = reportingDomain + 'GetQcsPRDetails?reqID=' + params.reqID + '&sessionid=' + userService.getUserToken();
            return httpServices.get(url);
        };

        reportingService.GetQCSList = function (params) {
            let url = reportingDomain + 'getqcslist?uid=' + params.uid + '&reqid=' + params.reqid + '&sessionid=' + userService.getUserToken();
            return httpServices.get(url);
        };

        reportingService.GetQCSIDS = function (params) {
            let url = reportingDomain + 'GetQCSIDS?reqid=' + params.reqid + '&sessionid=' + userService.getUserToken();
            return httpServices.get(url);
        };

        reportingService.SaveQCSDetails = function (params) {
            let url = reportingDomain + 'saveqcsdetails';
            return httpServices.post(url, params);
        };

        //#endregion QCS

        //#region RM QCS

        reportingService.GetRMQCSReportForExcel = function (reqid, itemID, sessionid) {
            let url = reportingDomain + 'getrmqcsreportforexcel?reqid=' + reqid + '&itemID=' + itemID + '&sessionid=' + sessionid;
            return httpServices.get(url);
        };

        reportingService.GetRMQCSDetails = function (params) {
            let url = reportingDomain + 'getrmqcsdetails?qcsid=' + params.qcsid + '&sessionid=' + userService.getUserToken();
            return httpServices.get(url);
        };

        reportingService.SaveRMQCSDetails = function (params) {
            let url = reportingDomain + 'savermqcsdetails';
            return httpServices.post(url, params);
        };

        //#endregion RM QCS

        //# OPEN PO AND OPEN PR START 

        reportingService.GetOpenPR = function (compid, plant, purchase, exclusion) {
            let url = reportingDomain + 'openpr?compid=' + compid + '&plant=' + plant + '&purchase=' + purchase + '&sessionid=' + userService.getUserToken() + '&exclusion=' + exclusion;
            return httpServices.get(url);
        };

        reportingService.GetOpenPRPivot = function (compid, plant, purchase, exclusion) {
            let url = reportingDomain + 'openprpivot?compid=' + compid + '&plant=' + plant + '&purchase=' + purchase + '&sessionid=' + userService.getUserToken() + '&exclusion=' + exclusion;
            return httpServices.get(url);
        };

        reportingService.GetOpenPO = function (compid, pono, plant, purchase, exclude) {
            let url = reportingDomain + 'openpo?compid=' + compid + '&plant=' + plant + '&purchase=' + purchase + '&pono=' + pono + '&sessionid=' + userService.getUserToken() + '&exclude=' + exclude;
            return httpServices.get(url);
        };

        reportingService.GetOpenPOPivot = function (compid, plant, purchase) {
            let url = reportingDomain + 'openpopivot?compid=' + compid + '&plant=' + plant + '&purchase=' + purchase + '&sessionid=' + userService.getUserToken();
            return httpServices.get(url);
        };

        reportingService.GetPoComments = function (pono, item, type) {
            let url = reportingDomain + 'openpocomments?pono=' + pono + '&itemno=' + item + '&type=' + type + '&sessionid=' + userService.getUserToken();
            return httpServices.get(url);
        };

        reportingService.UpdatePoStatus = function (params) {
            var url = reportingDomain + 'updatepostatus';
            return httpServices.post(url, params);
        };

        reportingService.SavePoComments = function (params) {
            var url = reportingDomain + 'savepocomments';
            return httpServices.post(url, params);
        };

        reportingService.GetSapAccess = function (userid) {
            let url = reportingDomain + 'sapaccess?userid=' + userid + '&sessionid=' + userService.getUserToken();
            return httpServices.get(url);
        };

        reportingService.GetAvdAccess = function (userid) {
            let url = reportingDomain + 'avdaccess?userid=' + userid + '&sessionid=' + userService.getUserToken();
            return httpServices.get(url);
        };

        reportingService.GetLastUpdatedDate = function (table) {
            let url = reportingDomain + 'lastupdatedate?table=' + table + '&sessionid=' + userService.getUserToken();
            return httpServices.get(url);
        };

        reportingService.GetOpenPOReport = function (compid) {
            let url = reportingDomain + 'openporeport?compid=' + compid + '&sessionid=' + userService.getUserToken();
            return httpServices.get(url);
        };

        reportingService.GetOpenPRReport = function (compid) {
            let url = reportingDomain + 'openprreport?compid=' + compid + '&sessionid=' + userService.getUserToken();
            return httpServices.get(url);
        };


        reportingService.GetOpenPOShortageReport = function (compid, pono, plant, purchase, exclude) {
            let url = reportingDomain + 'getopenposhortagereport?compid=' + compid + '&plant=' + plant + '&purchase=' + purchase + '&pono=' + pono + '&sessionid=' + userService.getUserToken() + '&exclude=' + exclude;
            return httpServices.get(url);
        };

        reportingService.GetOpenPRShortageReport = function (compid, plant, purchase, exclusion) {
            let url = reportingDomain + 'getopenprshortagereport?compid=' + compid + '&plant=' + plant + '&purchase=' + purchase + '&sessionid=' + userService.getUserToken() + '&exclusion=' + exclusion;
            return httpServices.get(url);
        };

        reportingService.GetCompanySavingStats = function (params) {
            let url = reportingDomain + 'getcompanysavingstats?compid=' + params.compid + '&fromdate=' + params.fromDate + '&todate=' + params.toDate + '&sessionid=' + userService.getUserToken();
            return httpServices.get(url);
        };

        reportingService.UpdateQCSSavings = function (params) {
            let url = reportingDomain + 'UpdateQCSSavings?dbname=' + params.dbname + '&compid=' + params.compid + '&sessionid=' + params.sessionid + '&type=' + params.type;
            return httpServices.get(url);
        };

        reportingService.saveQCSSAVINGS = function (params) {
            var url = reportingDomain + 'saveQCSSAVINGS';
            return httpServices.post(url, params);
        };

        reportingService.getRfqtoQcsReport = function (params) {
            let url = reportingDomain + 'getrfqtoqcsreport?userid=' + userService.getUserId() + '&sessionid=' + userService.getUserToken() + '&compID=' + params.compID + '&material=' + params.material + '&companyName=' + params.companyName + '&buyer=' + params.buyer + '&fromdate=' + params.fromdate + '&todate=' + params.todate + '&PageSize=' + params.PageSize + '&NumberOfRecords=' + params.NumberOfRecords;
            return httpServices.get(url);
        };
        
        //# OPEN PO AND OPEN PR END
        reportingService.getFilterValues = function (params) {
            let url = reportingDomain + 'getFilterValues?compid=' + params.compID + '&reqid=' + params.reqid +'&sessionid=' + userService.getUserToken() + '&fromdate=' + params.fromdate + '&todate=' + params.todate;
            return httpServices.get(url);
        };

        reportingService.GetBidHistoryComparatives = function (params) {
            let url = reportingDomain + 'getBidHistoryComparatives?REQ_ID=' + params.REQ_ID + '&sessionid=' + userService.getUserToken();
            return httpServices.get(url);
        };

        return reportingService;
    }]);