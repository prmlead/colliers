﻿prmApp
    .service('apmcService', ["apmcDomain", "userService", "httpServices",
        function (apmcDomain, userService, httpServices) {
            //var domain = 'http://182.18.169.32/services/';
            var apmcService = this;

            apmcService.getapmclist = function () {
                var url = apmcDomain + 'getapmclist?userid=' + userService.getUserId() + '&sessionid=' + userService.getUserToken();
                return httpServices.get(url);
            };

            apmcService.getapmcvendorlist = function () {
                var url = apmcDomain + 'getapmcvendorlist?userid=' + userService.getUserId() + '&sessionid=' + userService.getUserToken();
                return httpServices.get(url);
            };

            apmcService.getApmc = function (apmcid) {
                var url = apmcDomain + 'getapmc?apmcid='+ apmcid + '&userid=' + userService.getUserId() + '&sessionid=' + userService.getUserToken();
                return httpServices.get(url);
            };

            apmcService.getapmcnegotiation = function (apmcnegid) {
                let url = apmcDomain + 'getapmcnegotiation?apmcnegid=' + apmcnegid + '&userid=' + userService.getUserId() + '&sessionid=' + userService.getUserToken();
                return httpServices.get(url);
            };

            apmcService.getnegotiation = function (apmcnegid) {
                let url = apmcDomain + 'getnegotiation?apmcnegid=' + apmcnegid + '&userid=' + userService.getUserId() + '&sessionid=' + userService.getUserToken();
                return httpServices.get(url);
            }

            apmcService.getapmcaudit = function (apmcnegid, vendorid) {
                let url = apmcDomain + 'getapmcaudit?apmcnegid=' + apmcnegid + '&vendorid=' + vendorid + '&userid=' + userService.getUserId() + '&sessionid=' + userService.getUserToken();
                return httpServices.get(url);
            }

            apmcService.getapmcnegotiationlist = function (fromdate, todate) {
                let url = apmcDomain + 'getapmcnegotiationlist?userid=' + userService.getUserId() + '&sessionid=' + userService.getUserToken() + '&fromdate=' + fromdate + '&todate=' + todate;
                return httpServices.get(url);
            }

            apmcService.getnegotiation = function (apmcnegid) {
                let url = apmcDomain + 'getnegotiation?apmcnegid=' + apmcnegid + '&userid=' + userService.getUserId() + '&sessionid=' + userService.getUserToken();
                return httpServices.get(url);
            }


            apmcService.getapmcvendorquantitylist = function (apmcnegid, vendorid) {
                let url = apmcDomain + 'getapmcvendorquantitylist?apmcnegid=' + apmcnegid + '&vendorid=' + vendorid + '&userid=' + userService.getUserId() + '&sessionid=' + userService.getUserToken();
                return httpServices.get(url);
            }


            apmcService.saveApmc = function (params) {
                let url = apmcDomain + 'saveapmc';
                return httpServices.post(url, params);
            };  

            apmcService.savevendorquantitylist = function (params) {
                let url = apmcDomain + 'saveapmcvendorquantitylist';
                return httpServices.post(url, params);
            };

            apmcService.saveapmcinput = function (params) {
                let url = apmcDomain + 'saveapmcinput';
                return httpServices.post(url, params);
            };

            apmcService.saveapmcnegotiation = function (params) {
                let url = apmcDomain + 'saveapmcnegotiation';
                return httpServices.post(url, params);
            };

            apmcService.saveapmcnegotiationlist = function (params) {
                let url = apmcDomain + 'saveapmcnegotiationlist';
                return httpServices.post(url, params);
            };

            apmcService.importentity = function (params) {
                let url = apmcDomain + 'importentity';
                return httpServices.post(url, params);
            };

            apmcService.CheckUniqueIfExists = function (params) {
                let url = apmcDomain + 'checkuniqueifexists';
                return httpServices.post(url, params);
            };
            
            return apmcService;
        }]);