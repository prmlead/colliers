﻿prmApp.controller("ReqConfigController", ['$uibModal', 'SettingService', 'store', 'growlService', function ($uibModal, SettingService, store, growlService) {
    var _reqConfigCtrl = this;
    _reqConfigCtrl.FieldList = []
    _reqConfigCtrl.addField = function () {
        $uibModal.open({
            ariaLabelledBy: 'modal-title',
            ariaDescribedBy: 'modal-body',
            templateUrl: 'ReqConfigModal.html',
            controller: 'ReqConfigModalCtrl',
            controllerAs: '_modalCtrl',
            appendTo: 'body',
            resolve: {
                options: {
                    editMode: false
                }
            }
        }).result.then(function (data) {
            _reqConfigCtrl.getFields();

        });
    }

    _reqConfigCtrl.editField = function (item) {
        $uibModal.open({
            ariaLabelledBy: 'modal-title',
            ariaDescribedBy: 'modal-body',
            templateUrl: 'ReqConfigModal.html',
            controller: 'ReqConfigModalCtrl',
            controllerAs: '_modalCtrl',
            appendTo: 'body',
            resolve: {
                options: {
                    editMode: true,
                    field: item
                }
            }
        }).result.then(function (data) {
            _reqConfigCtrl.getFields();
        });
    }

    _reqConfigCtrl.deleteField = function (item, index) {

        swal({
            title: "Are you sure?",
            text: "You will not be able to recover this field!",
            icon: "warning",
            buttons: [
              'No, cancel it!',
              'Yes, I am sure!'
            ],
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, I am sure!",
            closeOnConfirm: true,
            dangerMode: true,
        }, function (isConfirm) {
            console.log(isConfirm)
            if (isConfirm) {
                SettingService.deleteRequirementField(item.Id).then(function (response) {
                    if (response.errorMessage == "") {
                        _reqConfigCtrl.FieldList.splice(index, 1);
                        growlService.growl(response.message, "success", {
                            from: "bottom",
                            align: "left"
                        })
                    }
                    else {
                        growlService.growl(response.errorMessage, "error")

                    }
                })
            } else {
                swal("Cancelled", "Your imaginary file is safe :)", "error");
            }
        })



    }

    _reqConfigCtrl.getFields = function () {


        SettingService.getRequirementFields().then(function (response) {
            if (response != null && response.length > 0) {
                _reqConfigCtrl.FieldList = response
            }
        })
    }
    _reqConfigCtrl.dragControlListeners = {
        containment: '#fieldList',
        accept: function (sourceItemHandleScope, destSortableScope) {
            return sourceItemHandleScope.itemScope.sortableScope.$id === destSortableScope.$id;
        }
    };

}]);

prmApp.controller("ReqConfigModalCtrl", ['$uibModalInstance', 'options', 'SettingService', 'store', 'growlService', function ($uibModalInstance, options, SettingService, store, growlService) {
    var _modalCtrl = this;
    _modalCtrl.Model = {
        Type: "",
        Required: true
    };
    _modalCtrl.options = options;
    _modalCtrl.TypeList = ["text", "number", "checkbox","long text"]
    if (options.editMode) {
        _modalCtrl.Model = options.field
    }

    _modalCtrl.Create = function (form) {
        form.$submitted = true;
        if (form.$valid) {
            SettingService.insertRequirementField(_modalCtrl.Model).then(function (response) {
                if (response.errorMessage == "") {
                    $uibModalInstance.close(_modalCtrl.Model);
                    growlService.growl(response.message, "inverse")
                }
                else {
                    growlService.growl(response.errorMessage, "inverse")

                }
            })

        }

    };
    _modalCtrl.Update = function (form) {
        form.$submitted = true;
        if (form.$valid) {
            SettingService.updateRequirementField(_modalCtrl.Model).then(function (response) {
                if (response.errorMessage == "") {
                    $uibModalInstance.close(_modalCtrl.Model);
                    growlService.growl(response.message, "inverse")
                } else {
                    growlService.growl(response.errorMessage, "inverse")

                }
            })

        }
    };

    _modalCtrl.close = function () {
        $uibModalInstance.close();
    };

}]);