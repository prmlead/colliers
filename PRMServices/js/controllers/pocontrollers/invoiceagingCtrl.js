﻿prmApp
    .controller('invoiceagingCtrl', function ($scope, $state, $stateParams, userService, growlService, auctionsService,
        workflowService,
        auctionsService, fileReader, $log, $window,poService) {
        
        $scope.MyPendingInvoices = [];
        $scope.MyPendingInvoices1 = [];
        $scope.MyPendingInvoices2 = [];
        //$scope.agingStatus = 'ALL';
        //$scope.categoryStatus = 'ALL';
        //$scope.departmentStatus = 'ALL';
        $scope.filters = {
            categoryStatus: '',
            departmentStatus:''
        };

        $scope.isVendor = (userService.getUserType() == "VENDOR") ? true : false;


        $scope.compID = userService.getUserCompanyId();
        /*PAGINATION CODE*/
        $scope.totalItems = 0;
        $scope.currentPage = 1;
        $scope.itemsPerPage = 10;
        $scope.maxSize = 5; //Number of pager buttons to show
        
        $scope.setPage = function (pageNo) {
            $scope.currentPage = pageNo;
        };

        $scope.pageChanged = function () {
            ////console.log('Page changed to: ' + $scope.currentPage);
        };
    /*PAGINATION CODE*/

        $scope.DeptCategories = [];
        $scope.catArr = [];
        $scope.deptArr = [];

        $scope.GetFiltersOnLoadData = function () {
            poService.GetFiltersOnLoadData()
                .then(function (response) {
                    $scope.DeptCategories = response;

                    //$scope.catArr = $scope.catArr.concat(response.filter(function (item) { return item.type === 'CATEGORY' }));
                    //$scope.deptArr = $scope.deptArr.concat(response.filter(function (item) { return item.type === 'DEPARTMENT' }));

                    $scope.DeptCategories.forEach(function (deptCatItem, deptCatIndex) {
                       if (deptCatItem.type === 'CATEGORY') {
                            $scope.catArr.push(deptCatItem);
                        } else if (deptCatItem.type === 'DEPARTMENT') {
                           $scope.deptArr.push(deptCatItem);
                       } 
                    });
                    $scope.filters.categoryStatus = '';
                    $scope.filters.departmentStatus = '';

                    $scope.GetMyPendingApprovals();

                });
        };



        $scope.GetMyPendingApprovals = function () {
            poService.GetPendingPayments()
                .then(function (response) {
                    $scope.MyPendingInvoices = response;
                    $scope.categories = [];
                    $scope.departments = [];
                    $scope.pendingInvoices = [];
                    $scope.pendingPayments = [];
                    $scope.pendingAmount = 0;
                    $scope.allInvoices = [];
                    $scope.scheduledPayments = 0;


                    $scope.MyPendingInvoices.forEach(function (item, index) {

                        console.log(item.department);
                        if ($scope.categories.indexOf(item.category)== -1) {
                            $scope.categories.push(item.category);

                        }
                        var departmentArray = item.department.split(',');
                        departmentArray.forEach(function (dept) {
                            if ($scope.departments.indexOf(dept) == -1) {
                                $scope.departments.push(dept);
                            }
                        })

                        if (isNaN(item.invoiceAmount)) {
                            item.invoiceAmount = 0
                        }
                        else {
                            item.invoiceAmount = parseFloat(item.invoiceAmount)
                         }
                        
                       
                        item.pendingAmount = Math.round((item.invoiceAmount)-(item.paymentAmount));
                        console.log(item.pendingAmount);

                        var curDate = moment();
                        item.aggingDays = curDate.diff(moment(item.invoiceDate), 'days');
                        console.log(item.invoiceDate);


                        if ($scope.pendingPayments.indexOf(item.paymentCode) == -1) {
                            if (item.paymentStatus == 'PENDING') {
                                $scope.pendingPayments.push(item.paymentCode);
                                $scope.pendingAmount += item.pendingAmount;
                            }
                            if (item.invoiceDate > curDate) {
                                $scope.scheduledPayments++;
                            }
                        }

                        item.paymentDate = userService.toLocalDate(item.paymentDate);
                        item.invoiceDate = userService.toLocalDate(item.invoiceDate);
                        item.receivedDate = userService.toLocalDate(item.receivedDate);
                        console.log(item.invoiceDate);

                        if ($scope.allInvoices.indexOf(item.invoiceNumber) == -1) {
                            $scope.allInvoices.push(item.invoiceNumber);
                        }

                        if ($scope.pendingInvoices.indexOf(item.invoiceNumber) == -1 && item.paymentStatus=='PENDING') {
                            $scope.pendingInvoices.push(item.invoiceNumber);
                        }
                        
                    

                        

                        //const date1 = item.paymentDate;
                        //const date2 = item.invoiceDate;
                        //const diffTime = Math.abs(date2 - date1);
                        //const diffDays = Math.ceil(diffTime / (1000 * 60 * 60 * 24)); 
                    
  
                    });
                    $scope.MyPendingInvoices = $scope.MyPendingInvoices.filter(function (item) {
                       return item.paymentStatus == 'PENDING';
                    })
                    $scope.MyPendingInvoices1 = response.filter(function (item) {
                      return item.paymentStatus == 'PENDING';
                    });
                   console.log($scope.MyPendingInvoices1);
                    $scope.MyPendingInvoices2 = response.filter(function (item) {
                       return item.paymentStatus == 'PENDING';
                   });
                   // $scope.MyPendingInvoices1 = response;
                   
                    /*PAGINATION CODE START*/
                    $scope.totalItems = $scope.MyPendingInvoices.length;
                    /*PAGINATION CODE END*/
                });
        };

        //$scope.GetMyPendingApprovals();
        $scope.GetFiltersOnLoadData();
        
        $scope.goToPO = function (reqID,poID,vendorID) {
            var url = $state.href("po-list", { "reqID": reqID, "vendorID": vendorID, "poID": 0 });
            window.open(url, '_blank');
        }
       

        $scope.setFilters = function () {
            $scope.filterArray = [];
            $scope.MyPendingInvoices = $scope.MyPendingInvoices1;
            if ($scope.searchKeyword) {
                $scope.filterArray = $scope.MyPendingInvoices.filter(function (item) {
                    return item.itemName.toLowerCase().indexOf($scope.searchKeyword.toLowerCase()) >= 0;
                });
                $scope.MyPendingInvoices = $scope.filterArray;
            }
            if ($scope.filters.departmentStatus) {
                console.log($scope.filters.departmentStatus);
                
                    $scope.filterArray = $scope.MyPendingInvoices.filter(function (item) {
                        return item.department.split(',').indexOf($scope.filters.departmentStatus) >= 0;
                    });
                    $scope.MyPendingInvoices = $scope.filterArray;
                
                

            }
            if ($scope.filters.categoryStatus) {
                console.log($scope.filters.categoryStatus);
                     $scope.filterArray = $scope.MyPendingInvoices.filter(function (item) {
                            return item.category == $scope.filters.categoryStatus;
                     });
                     $scope.MyPendingInvoices = $scope.filterArray;
                
            }
            if ($scope.agingStatus) {
                if ($scope.agingStatus == "<10") {
                    $scope.filterArray = $scope.MyPendingInvoices.filter(function (item) {
                        return item.aggingDays <= 10;
                    });
                    $scope.MyPendingInvoices = $scope.filterArray;
                }
                else if ($scope.agingStatus == "10-20") {
                    $scope.filterArray = $scope.MyPendingInvoices.filter(function (item) {
                        return item.aggingDays > 10 && item.aggingDays <= 20;
                    });
                    $scope.MyPendingInvoices = $scope.filterArray;
                }
                else if ($scope.agingStatus == "20-45") {
                    $scope.filterArray = $scope.MyPendingInvoices.filter(function (item) {
                        return item.aggingDays > 20 && item.aggingDays <= 45;
                    });
                    $scope.MyPendingInvoices = $scope.filterArray;
                }
                else {
                    $scope.filterArray = $scope.MyPendingInvoices.filter(function (item) {
                        return item.aggingDays > 45;
                    });
                    $scope.MyPendingInvoices = $scope.filterArray;
                }
            }
            //else {
            //    $scope.MyPendingInvoices = $scope.MyPendingInvoices1;
            //}
            
        }
    });