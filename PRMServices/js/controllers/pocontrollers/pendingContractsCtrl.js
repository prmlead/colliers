﻿prmApp
    .controller('pendingContractsCtrl', function ($scope, $rootScope, $state, $stateParams, userService, catalogService) {

        $scope.sessionid = userService.getUserToken();  

        $scope.MyPendingContracts = [];
        $scope.MyPendingContracts1 = [];
        $scope.MyPendingContracts2 = [];
        $scope.openContracts = 0;
        $scope.totalContracts = 0;
        $scope.contractValue = 0;
        $scope.quantity = 0;
        $scope.availedQuantity = 0;
        $scope.utility = 0;

        $scope.isVendor = (userService.getUserType() == "VENDOR") ? true : false;
        let requirementPRContacts = $rootScope.PRPendingContracts;
        console.log(requirementPRContacts);
        $scope.isModalWindow = true;

        $scope.filters = {
            categoryStatus: '',
            departmentStatus: '',
            productName: '',
            supplierName: '',
            status:'active'

        };

        $scope.compID = userService.getUserCompanyId();
        /*PAGINATION CODE*/
        $scope.totalItems = 0;
        $scope.currentPage = 1;
        $scope.itemsPerPage = 10;
        $scope.maxSize = 5; //Number of pager buttons to show
        
        $scope.setPage = function (pageNo) {
            $scope.currentPage = pageNo;
        };

        $scope.pageChanged = function () {
            ////console.log('Page changed to: ' + $scope.currentPage);
        };
    /*PAGINATION CODE*/

        $scope.supplierArr = [];
        $scope.itemArr = [];
       
        $scope.catArr = [];

        $scope.contractEndDate = ''
        $scope.contractStartDate = ''
       

        $scope.GetMyPendingContracts = function () {
            $scope.MyPendingContracts = [];
            
            catalogService.GetMyPendingContracts()
                .then(function (response) {
                    if (response && response.length > 0) {
                        response.forEach(function (item, index) {

                            $scope.contractValue += item.contractValue;
                            $scope.quantity += item.quantity;
                            $scope.availedQuantity += item.availedQuantity;
                            

                            if ($scope.supplierArr.indexOf(item.supplierName) == -1) {
                                $scope.supplierArr.push(item.supplierName);

                            }
                            
                            if ($scope.catArr.indexOf(item.categories) == -1) {
                                $scope.catArr.push(item.categories);

                            }
                            if ($scope.itemArr.indexOf(item.ProductName) == -1) {
                                $scope.itemArr.push(item.ProductName);

                            }

                            let isContractItem = false;
                            if (requirementPRContacts && requirementPRContacts.length > 0) {
                                let anyContractItem = _.filter(requirementPRContacts, function (o) {
                                    return o.PRODUCT_ID === item.ProductId;
                                });

                                if (anyContractItem && anyContractItem.length > 0) {
                                    isContractItem = true;
                                }
                            } else {
                                isContractItem = true;
                            }


                            item.startTime = userService.toLocalDate(item.startTime);
                            item.endTime = userService.toLocalDate(item.endTime);
                            if (isContractItem) {
                                if (item.quantity != item.availedQuantity) {
                                    $scope.openContracts+=1;
                                }
                                $scope.MyPendingContracts.push(item);

                            }

                            isContractItem = true;
                            
                        });
                        $scope.utility = (($scope.availedQuantity / $scope.quantity) * 100).toFixed(2);
                        

                        $scope.totalContracts = $scope.MyPendingContracts.length
                        $scope.MyPendingContracts1 = $scope.MyPendingContracts;
                        $scope.MyPendingContracts2 = $scope.MyPendingContracts;
                        /*PAGINATION CODE START*/
                        $scope.totalItems = $scope.MyPendingContracts.length;
                    /*PAGINATION CODE END*/
                        $scope.setFilters();

                    }
                });
        };

        $scope.GetMyPendingContracts();

        $scope.setFilters = function () {
            console.log(1)
            $scope.filterArray = [];
            $scope.MyPendingContracts = $scope.MyPendingContracts1;

            if ($scope.filters.status == 'active') {
                $scope.filterArray = $scope.MyPendingContracts.filter(function (item) {
                    return item.quantity != item.availedQuantity;
                })
                $scope.MyPendingContracts = $scope.filterArray;
            } else {
                $scope.filterArray = $scope.MyPendingContracts.filter(function (item) {
                    return item.quantity == item.availedQuantity;
                })
                $scope.MyPendingContracts = $scope.filterArray
            }
            
            
            if ($scope.filters.categoryStatus) {
                
                $scope.filterArray = $scope.MyPendingContracts.filter(function (item) {
                    return item.categories == $scope.filters.categoryStatus
                });
                $scope.MyPendingContracts = $scope.filterArray;

            }
            if ($scope.filters.productName) {
                $scope.filterArray = $scope.MyPendingContracts.filter(function (item) {
                    return item.ProductName == $scope.filters.productName
                });
                $scope.MyPendingContracts = $scope.filterArray;
            }

            if ($scope.filters.supplierName) {
                $scope.filterArray = $scope.MyPendingContracts.filter(function (item) {
                    return item.supplierName == $scope.filters.supplierName;
                });
                $scope.MyPendingContracts = $scope.filterArray;
            }
            if ($scope.contractStartDate) {
                $scope.filterArray = $scope.MyPendingContracts.filter(function (item) {
                    return moment(item.startTime, 'DD-MM-YYYY') > moment($scope.contractStartDate, 'DD-MM-YYYY');
                });
                $scope.MyPendingContracts = $scope.filterArray;
            }
            if ($scope.contractEndDate) {
                $scope.filterArray = $scope.MyPendingContracts.filter(function (item) {
                    return moment(item.endTime, 'DD-MM-YYYY') < moment($scope.contractEndDate, 'DD-MM-YYYY');
                });
                $scope.MyPendingContracts = $scope.filterArray;
            }
           

           

        }
        
    });