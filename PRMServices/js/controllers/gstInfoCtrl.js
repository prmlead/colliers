﻿prmApp

    .controller('gstInfoCtrl', ["$scope", "$http", "$state", "domain", "$filter", "$stateParams", "$timeout", "auctionsService", "userService", "SignalRFactory", "fileReader", "growlService",
        function ($scope, $http, $state, domain, $filter, $stateParams, $timeout, auctionsService, userService, SignalRFactory, fileReader, growlService) {
           
            $scope.vendorGstDetailsObj = {
                general: {},
                filing: {},
                isError: false,
                error: 'test'
            };

            
            $scope.sessionid = userService.getUserToken();
            
            $scope.isCustomer = userService.getUserType() == "CUSTOMER" ? true : false;

            

            $scope.searchGst = function () {
                if ($scope.searchKeyword && $scope.year) {
                    $scope.getGSTFilingDetails($scope.searchKeyword);
                    $scope.getGSTCompanyDetails($scope.searchKeyword);
                } else {
                        $scope.vendorGstDetailsObj.isError = true;
                        $scope.vendorGstDetailsObj.error = 'Vendor GST number missing.';
                    }
            }


            

            $scope.GetDateconverted = function (dateBefore) {
                if (dateBefore) {
                    return new moment(dateBefore).format("DD-MM-YYYY HH:mm");
                }
            };


            $scope.getGSTFilingDetails = function (gst) {
                $scope.vendorGstDetailsObj.isError = false;
                $scope.vendorGstDetailsObj.filing = null;
                var params = {
                    "gstin": gst,
                    "year": $scope.year,
                    "sessionid": userService.getUserToken()
                }
                userService.getGSTDetails(params)
                    .then(function (response) {

                        var vendorGSTFilingDetailsJson = response;//'{"error":false,"data":{"EFiledlist":[{"valid":"Y","mof":"ONLINE","dof":"21-12-2019","ret_prd":"112019","rtntype":"GSTR3B","arn":"AA3611192835414","status":"Filed"},{"valid":"Y","mof":"ONLINE","dof":"10-12-2019","ret_prd":"102019","rtntype":"GSTR3B","arn":"AA361019397525T","status":"Filed"},{"valid":"Y","mof":"ONLINE","dof":"09-12-2019","ret_prd":"092019","rtntype":"GSTR1","arn":"AA360919514416U","status":"Filed"},{"valid":"Y","mof":"ONLINE","dof":"09-12-2019","ret_prd":"092019","rtntype":"GSTR3B","arn":"AA3609195140138","status":"Filed"},{"valid":"Y","mof":"ONLINE","dof":"23-09-2019","ret_prd":"082019","rtntype":"GSTR3B","arn":"AA360819275807D","status":"Filed"},{"valid":"Y","mof":"ONLINE","dof":"17-09-2019","ret_prd":"072019","rtntype":"GSTR3B","arn":"AA3607193931202","status":"Filed"},{"valid":"Y","mof":"ONLINE","dof":"27-07-2019","ret_prd":"062019","rtntype":"GSTR1","arn":"AA360619397611P","status":"Filed"},{"valid":"Y","mof":"ONLINE","dof":"27-07-2019","ret_prd":"042019","rtntype":"GSTR3B","arn":"AA360419423368X","status":"Filed"},{"valid":"Y","mof":"ONLINE","dof":"27-07-2019","ret_prd":"052019","rtntype":"GSTR3B","arn":"AA360519397330X","status":"Filed"},{"valid":"Y","mof":"ONLINE","dof":"27-07-2019","ret_prd":"062019","rtntype":"GSTR3B","arn":"AA360619397246K","status":"Filed"},{"valid":"Y","mof":"ONLINE","dof":"25-05-2019","ret_prd":"032019","rtntype":"GSTR1","arn":"AA360319530586X","status":"Filed"},{"valid":"Y","mof":"ONLINE","dof":"22-05-2019","ret_prd":"032019","rtntype":"GSTR3B","arn":"AA360319525963U","status":"Filed"},{"valid":"Y","mof":"ONLINE","dof":"25-04-2019","ret_prd":"022019","rtntype":"GSTR3B","arn":"AA3602194018174","status":"Filed"},{"valid":"Y","mof":"ONLINE","dof":"22-04-2019","ret_prd":"012019","rtntype":"GSTR3B","arn":"AA360119415081L","status":"Filed"}]}}';
                        console.log(JSON.parse(vendorGSTFilingDetailsJson));
                        if (vendorGSTFilingDetailsJson) {
                            let tempObj = JSON.parse(vendorGSTFilingDetailsJson);
                            if (!tempObj.error) {
                                $scope.vendorGstDetailsObj.filing = tempObj.data.EFiledlist;
                            } else {
                                $scope.vendorGstDetailsObj.isError = true;
                                $scope.vendorGstDetailsObj.error = tempObj.data.error.message;
                            }
                        }
                    });
            };

            $scope.getGSTCompanyDetails = function (gst) {
                $scope.vendorGstDetailsObj.isError = false;
                $scope.vendorGstDetailsObj.general = null;
                userService.getGSTDetails({ "gstin": gst, "year": '', "sessionid": userService.getUserToken() })
                    .then(function (response) {
                        var vendorGSTDetailsJson = response;//'{"error":false,"data":{"stjCd":"TG137","lgnm":"ACADS360 INDIA PRIVATE LIMITED","stj":"RAJENDRANAGAR - II","dty":"Regular","adadr":[],"cxdt":"","gstin":"36AANCA9052E1ZK","nba":["Service Provision"],"lstupdt":"16/09/2019","rgdt":"01/07/2017","ctb":"Private Limited Company","pradr":{"addr":{"bnm":"RADHAKRISHANA NAGAR","st":"HYDERGUDA","loc":"ATTAPUR","bno":"3-4-174/21/2","dst":"Hyderabad","stcd":"Telangana","city":"","flno":"","lt":"","pncd":"500048","lg":""},"ntr":"Service Provision"},"tradeNam":"M/S ACADS360 INDIA RIVATE LIMITED","sts":"Active","ctjCd":"YN0401","ctj":"ATTAPUR"}}';
                        console.log(JSON.parse(vendorGSTDetailsJson));
                        if (vendorGSTDetailsJson) {
                            let tempObj = JSON.parse(vendorGSTDetailsJson);
                            if (!tempObj.error) {
                                $scope.vendorGstDetailsObj.general = tempObj.data;
                            } else {
                                $scope.vendorGstDetailsObj.isError = true;
                                $scope.vendorGstDetailsObj.error = tempObj.message;
                            }
                        }
                    });
            };

        }]);