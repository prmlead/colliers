﻿prmApp
    .controller('storeginhistoryCtrl', ["$scope", "$state", "$stateParams", "$window", "$log", "userService", "storeService", "fileReader", "growlService",
        function ($scope, $state, $stateParams, $window, $log, userService, storeService, fileReader, growlService) {
        $scope.storeID = $stateParams.storeID;
        $scope.sessionID = userService.getUserToken();
        $scope.storeItemGINArray = [];

        $scope.handleDate = function (date) {
            return new moment(date).format("DD-MMM-YYYY");
        }

        $scope.cancelClick = function () {
            $window.history.back();
        }

        $scope.selectGinDetails = function (ginDetails) {
            $state.go("storegindetails", { "storeID": $scope.storeID, "ginCode": ginDetails.ginCode, "ginObj": ginDetails });            
        }

        $scope.getCompanyStoreItemsGIN = function () {
            storeService.getStoreItemGIN('', $scope.storeID)
                .then(function (response) {
                    $scope.storeItemGINArray = response;
                });
        };

        $scope.getCompanyStoreItemsGIN();
    }]);