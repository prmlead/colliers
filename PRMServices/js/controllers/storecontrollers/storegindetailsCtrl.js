﻿prmApp
    .controller('storegindetailsCtrl', ["$scope", "$state", "$stateParams", "$window", "$log", "$timeout", "userService", "storeService", "fileReader", "growlService",
        function ($scope, $state, $stateParams, $window, $log, $timeout, userService, storeService, fileReader, growlService) {
            $scope.storeID = $stateParams.storeID;
            $scope.ginCode = $stateParams.ginCode;
            $scope.ginObject = $stateParams.ginObj;
            $scope.sessionID = userService.getUserToken();
            $scope.storeItemGINItemDetails = [];
            $scope.indentItemDetails = [];
            $scope.indentDetails = [];
            $scope.doPrint = false;
            $scope.selectedStoreItems = [];

            $scope.handleDate = function (date) {
                return new moment(date).format("DD-MMM-YYYY");
            }

            $scope.cancelClick = function () {
                $window.history.back();
            }

            $scope.itemSelect = function (storeItem) {
                $scope.selectedStoreItems = _.filter($scope.storeItemGINItemDetails.storeItemDetails, function (item) {
                    return item.isSelected == true;
                });
            };

            $scope.generategrn = function () {
                if ($scope.selectedStoreItems.length > 0) {
                    $state.go("storegrn", { "storeID": $scope.storeID, "itemsArr": $scope.selectedStoreItems, "linkgin": $scope.ginCode });
                }
                else {
                    swal("Warning", "Please select items to create GRN", "warning");
                }
            }

            $scope.printReport = function () {
                $scope.doPrint = true;
                $timeout(function () {
                    $window.print();
                    $scope.doPrint = false
                }, 1000);
            }

            $scope.selectGinItemDetails = function (ginDetails) {
                if ($scope.indentDetails.length <= 0) {
                    storeService.getIndentItemDetails($scope.storeID, $scope.ginCode, '')
                        .then(function (response) {
                            $scope.indentDetails = response;
                        });
                }
                else {
                    $scope.indentItemDetails = _.filter($scope.indentDetails, function (item) {
                        return item.itemID == ginDetails.itemID;
                    });
                }
            }

            $scope.getCompanyStoreItemsGIN = function () {
                storeService.getStoreItemGIN($scope.ginCode, $scope.storeID)
                    .then(function (response) {
                        $scope.storeItemGINItemDetails = response[0];
                    });

                storeService.getIndentItemDetails($scope.storeID, $scope.ginCode, '')
                    .then(function (response) {
                        $scope.indentDetails = response;
                    });
            };

            $scope.getCompanyStoreItemsGIN();
        }]);