﻿prmApp
    .controller('addnewstoreitemCtrl', ["$scope", "$stateParams", "$window", "$log", "userService", "storeService", "growlService", "auctionsService",
        function ($scope, $stateParams, $window, $log, userService, storeService, growlService, auctionsService) {
            $scope.storeID = $stateParams.storeID;
            $scope.itemID = $stateParams.itemID;
            $scope.companyID = userService.getUserCompanyId();
            $scope.sessionID = userService.getUserToken();
            $scope.uniqueItemCodes = [];
            $scope.uniqueItemNames = [];
            $scope.initialInStock = 0;
            $scope.initialOnOrder = 0;
            $scope.initialTotalStock = 0;
            $scope.isValidToSubmit = true;
            $scope.categories = [];
            $scope.categoriesdata = [];
            $scope.selcCategory = '';
            $scope.selcSubCategory = '';
            $scope.subCategories = [];

           auctionsService.getCategories(userService.getUserId()).then(function (response) {
                    if (response) {
                        $scope.categories = _.uniq(_.map(response, 'category'));
                        $scope.categoriesdata = response;
                    } else { }
                }, function (result) {
            });
            
            $scope.storeObj = {
                storeID: $scope.storeID,
                companyID: $scope.companyID,
                sessionID: $scope.sessionID
            };

            $scope.showIsSuperUser = function () {
                if (userService.getUserObj().isSuperUser) {
                    return true;
                } else {
                    return false;
                }
            }

            $scope.storeItemsObj = {
                itemID: $scope.itemID,
                inStock: 0,
                onOrder: 0,
                itemPrice: 0,
                totalStock: 0,
                storeDetails: $scope.storeObj,
                doAlert: 0,
                threshold: 0,
                iGST: 0,
                itemSubType:'',
                sessionID: $scope.sessionID
            };

            $scope.getCompanyUniqueItems = function (itemName) {
                storeService.getuniqueitems($scope.companyID, itemName)
                    .then(function (response) {
                        if (itemName == '') {
                            $scope.uniqueItemNames = response;
                        }
                        else {
                            $scope.uniqueItemCodes = response;
                        }
                    });
            };

            $scope.getCompanyUniqueItems('');

            $scope.selectItemCode = function (uniqueItem, currentItemObj) {
                currentItemObj.itemCode = uniqueItem.itemCode;
            }

            $scope.selectItemName = function (uniqueItemName, currentItemObj) {
                currentItemObj.itemName = uniqueItemName.itemName;
                $scope.getCompanyUniqueItems(uniqueItemName.itemName);
            }

            $scope.taxClassification = [
                { name: 'Cement - 28 %', tax: 28 },
                { name: 'Steel - 18 %', tax: 18 },
                { name: 'River Sand - 5 %', tax: 5 },
                { name: 'Crush Sandg - 5 %', tax: 5 },
                { name: 'Aggregate - 5 %', tax: 5 },
                { name: 'Marble and Granite- 12 - 28 %', tax: 28 },
                { name: 'Stone - 5 %', tax: 5 },
                { name: 'Red Bricks- 5 %', tax: 5 },
                { name: 'Fly Ash Bricks- 12 %', tax: 12 },
                { name: 'Cement Block - 18 %', tax: 18 },
                { name: 'Glass - 28 %', tax: 28 },
                { name: 'Shahabad - 5 %', tax: 5 },
                { name: 'Tiles - 18 - 28 %', tax: 28 },
                { name: 'Wire and Cable - 28 %', tax: 28 },
                { name: 'Paint and Varnish - 28 %', tax: 28 },
                { name: 'Sanatary Fittings - 28 %', tax: 28 },
                { name: 'GI Fittings - 18 %', tax: 18 },
                { name: 'CP Fittings - 18 - 28 % (Depends on Base Material)', tax: 28 },
                { name: 'Wallpapers - 28 %', tax: 28 },
                { name: 'Key Locks - 18 %', tax: 18 },
                { name: 'Wooden Door and window - 28 %', tax: 28 },
                { name: 'Aluminium Windows - 18 - 28 %', tax: 28 },
            ];


            if ($scope.itemID > 0) {
                if (!$stateParams.itemObj) {
                    storeService.getcompanystoreitems($scope.storeID, $scope.itemID)
                        .then(function (response) {
                            $scope.storeItemsObj = response[0];
                            //$log.info($scope.storeItemsObj);
                            $scope.storeItemsObj.sessionID = $scope.sessionID;
                        });
                }
                else {
                    $scope.storeItemsObj = $stateParams.itemObj;
                    $scope.storeItemsObj.sessionID = $scope.sessionID;
                }

                //$scope.initialInStock = $scope.storeItemsObj.inStock;
                //$scope.initialOnOrder = $scope.storeItemsObj.onOrder;
                //$scope.initialTotalStock = $scope.storeItemsObj.totalStock;
            }
            else {
                $scope.storeItemsObj.totalStock = $scope.inStock + $scope.onOrder;
            }

            $scope.$watch('storeItemsObj.itemType', function (newValue, oldValue, scope) {
                $scope.subCategories = _.filter($scope.categoriesdata, { category: $scope.storeItemsObj.itemType });
            }, true);

            $scope.$watch('storeItemsObj.inStock', function (newValue, oldValue, scope) {
                $scope.storeItemsObj.totalStock = $scope.storeItemsObj.inStock + $scope.storeItemsObj.onOrder;
            }, true);

            $scope.$watch('storeItemsObj.onOrder', function (newValue, oldValue, scope) {
                $scope.storeItemsObj.totalStock = $scope.storeItemsObj.inStock + $scope.storeItemsObj.onOrder;
            }, true);



            $scope.saveStoreItem = function () {
                validateData($scope.storeItemsObj);
                if (!$scope.isValidToSubmit) {
                    growlService.growl("Please fill all required fields marked (*).", "inverse");
                    return false;
                }

                //if (!$scope.storeItemsObj.itemCode);
                //{
                //    $scope.storeItemsObj.itemCode = "";
                //}

                $scope.storeItemsObj.isValid = 1;

                var params = {
                    "item": $scope.storeItemsObj
                };

                storeService.savestoreitem(params)
                    .then(function (response) {
                        if (response.errorMessage != '') {
                            growlService.growl(response.errorMessage, "inverse");
                        }
                        else {
                            growlService.growl("Item Details Saved Successfully.", "success");
                            $window.history.back();
                        }
                    })
            }

            $scope.itemTax = 0;

            $scope.getTaxes = function (tax) {
                $scope.storeItemsObj.cGST = tax / 2;
                $scope.storeItemsObj.sGST = tax / 2
            }

            $scope.getSubCategories = function (category) {
                $scope.storeItemsObj.itemType = category;
                $scope.subCategories = _.filter($scope.categoriesdata, { category: $scope.storeItemsObj.itemType });
            }

            function validateData(storeItemsObj) {
                //let subCategories = _.filter($scope.categoriesdata, { id: Number($scope.selcSubCategory) });
                //$scope.storeItemsObj.itemSubType = subCategories[0].subcategory;
                if (storeItemsObj.itemCode == null || storeItemsObj.itemCode == ""){
                    $scope.isValidToSubmit = false;
                }
                if (storeItemsObj.itemName == null || storeItemsObj.itemName == "") {
                    $scope.isValidToSubmit = false;
                }
                else if (storeItemsObj.itemType == null || storeItemsObj.itemType == "") {
                    $scope.isValidToSubmit = false;
                }
                else if (storeItemsObj.itemSubType == null || storeItemsObj.itemSubType == "") {
                    $scope.isValidToSubmit = false;
                }
                else if (storeItemsObj.itemPrice == null || storeItemsObj.itemPrice == "") {
                    $scope.isValidToSubmit = true;
                }
                else if (storeItemsObj.cGST == null || storeItemsObj.cGST == "") {
                    $scope.isValidToSubmit = false;
                }
                else if (storeItemsObj.sGST == null || storeItemsObj.sGST == "") {
                    $scope.isValidToSubmit = false;
                }
                //else if (storeItemsObj.iGST == null || storeItemsObj.iGST == "") {
                //    $scope.isValidToSubmit = false;
                //}
                else if (storeItemsObj.inStock == null || storeItemsObj.inStock == "" || storeItemsObj.inStock == "0") {
                    $scope.isValidToSubmit = true;
                }
                else if (storeItemsObj.onOrder == null || storeItemsObj.onOrder == "" || storeItemsObj.onOrder == "0") {
                    $scope.isValidToSubmit = true;
                }
                else if (storeItemsObj.itemUnits == null || storeItemsObj.itemUnits == "") {
                    $scope.isValidToSubmit = true;
                }
                else if (storeItemsObj.itemDescription == null || storeItemsObj.itemDescription == "") {
                    $scope.isValidToSubmit = false;
                }
                else {
                    $scope.isValidToSubmit = true;
                }
            }

        }]);