prmApp
    .controller('lgstcsRemindersCtrl', ["$scope", "$state", "$stateParams", "userService", "auctionsService", "fileReader", "logisticServices",
        function ($scope, $state, $stateParams, userService, auctionsService, fileReader, logisticServices) {
            $scope.id = $stateParams.Id;
            $scope.formRequest = {};

            $scope.sendMessageTo = '';

            $scope.listSelectedVendors = [
            ];

            $scope.selectedVendors = {
                vendorName: '',
                vendorIDs: 0
            };

            $scope.vendorIDs = [];
            $scope.reminderMessage = '';

            $scope.reminderMessageValidation = false;
            $scope.reminderVendorsValidation = true;

            $scope.requestType = 'REMINDER';



            $scope.getData = function () {

                var params = {
                    reqid: $stateParams.Id,
                    sessionid: userService.getUserToken(),
                    userid: userService.getUserId()
                }

                logisticServices.GetRequirementData(params)
                    .then(function (response) {
                        $scope.auctionItem = response;

                        $scope.auctionItem.auctionVendors = $scope.auctionItem.auctionVendors.filter(function (el) { return el.companyName != "PRICE_CAP"; });

                    })
            }

            $scope.getData();

            $scope.getVendors = function () {
                $scope.reminderVendorsValidation = false;
                $scope.listSelectedVendors = [];
                $scope.vendorIDs = [];
                $scope.vendorCompanyNames = [];

                if ($scope.sendMessageTo == 'approved') {
                    $scope.auctionItem.auctionVendors.forEach(function (item, index) {
                        $scope.selectedVendors = {};
                        if (item.quotationUrl != '' && item.isQuotationRejected == 0) {
                            $scope.vendorIDs.push(item.vendorID);
                            $scope.vendorCompanyNames.push(item.companyName);
                            $scope.selectedVendors.vendorID = item.vendorID;
                            $scope.selectedVendors.vendorName = item.companyName;
                            $scope.listSelectedVendors.push($scope.selectedVendors);
                        }
                    })
                }

                if ($scope.sendMessageTo == 'rejected') {
                    $scope.auctionItem.auctionVendors.forEach(function (item, index) {
                        $scope.selectedVendors = {};
                        if (item.quotationUrl != '' && item.isQuotationRejected == 1) {
                            $scope.vendorIDs.push(item.vendorID);
                            $scope.vendorCompanyNames.push(item.companyName);
                            $scope.selectedVendors.vendorID = item.vendorID;
                            $scope.selectedVendors.vendorName = item.companyName;
                            $scope.listSelectedVendors.push($scope.selectedVendors);
                        }
                    })
                }

                if ($scope.sendMessageTo == 'specific') {
                    $scope.auctionItem.auctionVendors.forEach(function (item, index) {
                        $scope.selectedVendors = {};
                        $scope.selectedVendors.vendorID = item.vendorID;
                        $scope.selectedVendors.vendorName = item.companyName;
                        $scope.listSelectedVendors.push($scope.selectedVendors);
                    })
                    $scope.vendorIDs = []
                    $scope.vendorCompanyNames = []
                }

                if ($scope.sendMessageTo == 'notuploaded') {
                    $scope.auctionItem.auctionVendors.forEach(function (item, index) {
                        $scope.selectedVendors = {};
                        if (item.quotationUrl == '' || item.quotationUrl == 0 || item.quotationUrl == undefined) {
                            $scope.vendorIDs.push(item.vendorID);
                            $scope.vendorCompanyNames.push(item.companyName);
                            $scope.selectedVendors.vendorID = item.vendorID;
                            $scope.selectedVendors.vendorName = item.companyName;
                            $scope.listSelectedVendors.push($scope.selectedVendors);
                        }
                    })
                }

                if ($scope.sendMessageTo == 'all') {
                    $scope.auctionItem.auctionVendors.forEach(function (item, index) {
                        $scope.selectedVendors = {};
                        $scope.vendorIDs.push(item.vendorID);
                        $scope.vendorCompanyNames.push(item.companyName);
                        $scope.selectedVendors.vendorID = item.vendorID;
                        $scope.selectedVendors.vendorName = item.companyName;
                        $scope.listSelectedVendors.push($scope.selectedVendors);
                    })
                }
                //console.log($scope.vendorIDs);

                if ($scope.vendorIDs.length == 0) {
                    $scope.reminderVendorsValidation = true;
                }
            }


            $scope.selectVendor = function (vendorID, companyName) {
                $scope.reminderVendorsValidation = false;

                var index = $scope.vendorIDs.indexOf(vendorID);
                var index = $scope.vendorCompanyNames.indexOf(companyName);

                if (index > -1) {
                    $scope.vendorIDs.splice(index, 1);
                    $scope.vendorCompanyNames.splice(index, 1);
                }
                else {
                    $scope.vendorIDs.push(vendorID);
                    $scope.vendorCompanyNames.push(companyName);
                }
                //console.log($scope.vendorIDs);

                if ($scope.vendorIDs.length == 0) {
                    $scope.reminderVendorsValidation = true;
                }
            }


            $scope.vendorreminders = function () {
                $scope.reminderMessageValidation = false;
                $scope.reminderVendorsValidation = false;
                if ($scope.reminderMessage == '') {
                    $scope.reminderMessageValidation = true;
                }
                if ($scope.vendorIDs.length == 0) {
                    $scope.reminderVendorsValidation = true;
                }
                if ($scope.reminderMessageValidation || $scope.reminderVendorsValidation) {
                    return;
                }
                var params = {
                    reqID: $scope.auctionItem.requirementID,
                    userID: userService.getUserId(),
                    sessionID: userService.getUserToken(),
                    message: $scope.reminderMessage,
                    vendorIDs: $scope.vendorIDs,
                    vendorCompanyNames: $scope.vendorCompanyNames,
                    requestType: $scope.requestType
                }
                logisticServices.vendorreminders(params)
                    .then(function (response) {
                        if (response.errorMessage == '') {
                            swal({
                                title: "Done!",
                                text: "Reminders sent Successfully.",
                                type: "success",
                                showCancelButton: false,
                                confirmButtonColor: "#DD6B55",
                                confirmButtonText: "Ok",
                                closeOnConfirm: true
                            },
                                function () {
                                    //$state.go('reminders', { 'Id': response.objectID });
                                    location.reload();
                                });
                        }
                        else {
                            swal("Error!", response.errorMessage, "error");
                        }
                    })
            }

            $scope.getHistory = function () {

                var params = {
                    reqid: $stateParams.Id,
                    sessionid: userService.getUserToken(),
                    userid: userService.getUserId()
                }

                logisticServices.GetVendorReminders(params)
                    .then(function (response) {
                        $scope.ListReminders = response;
                        $scope.ListReminders.forEach(function (item, index) {
                            item.sentOn = new moment(item.sentOn).format("DD-MM-YYYY HH:mm");
                        })
                    })
            }

            $scope.getHistory();

            $scope.reminderMessage = '';
            //$scope.reminderMessage = "We are using an online tool -PRM360 for our procurement process (Enquiry, Receiving Quotes, Negotiations and releasing POs. Just now we have floated an enquiry request you to please use the link you received in your mailbox and on Mobile for sending your offers." + "\n" + "Initially, Ms. Megha Mehta and Ms. Hannah  will help and train you in using the tool." + "\n" + "Request you to please coordinate with them. You may reach them at" + "\n" + "Email: megha@prm360.com ; hannah@prm360.com" + "\n" + "Megha   :  9989389385" + "\n" + "Hannah :  9160258358"

            $scope.bindHtml = function (val) {
                $scope.bindreminderMessage = val.replace(/\u000a/g, "</br>");
            }


        }]);