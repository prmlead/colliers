﻿prmApp
    .controller('termsconditionsCtrl', ["$scope", "$stateParams", "$log", "$state", "$window", "userService", "auctionsService", "storeService", "growlService",
        function ($scope, $stateParams, $log, $state, $window, userService, auctionsService, storeService, growlService) {

            $scope.userID = userService.getUserId();
            $scope.compId = userService.getUserCompanyId();
            $scope.sessionID = userService.getUserToken();


            $scope.companyDesignations = [];
            $scope.companyDesignations1 = [];

            $scope.CompanyDeptDesigTypes = [];

            /*PAGINATION CODE*/
            $scope.totalItems = 0;
            $scope.currentPage = 1;
            $scope.itemsPerPage = 10;
            $scope.maxSize = 5;


            $scope.setPage = function (pageNo) {
                $scope.currentPage = pageNo;
            };

            $scope.pageChanged = function () {
                ////console.log('Page changed to: ' + $scope.currentPage);
            };
            /*PAGINATION CODE*/

            $scope.terms = {
                U_ID: $scope.userID,
                COMP_ID: $scope.compId,
                TERM_ID: 0,
                PO_TYPE: '',
                TITLE: '',
                DESCRIPTION: '',
                sessionID: $scope.sessionID,
            };

            $scope.addnewtermsView = false;


            $scope.TermsConditions = [];
            $scope.GetTermsandConditions = function () {
                auctionsService.GetTermsandConditions(0, $scope.compId, $scope.sessionID)
                    .then(function (response) {
                        $scope.addnewtermsView = false;
                        $scope.TermsConditions = response;
                        $scope.totalItems = $scope.TermsConditions.length;
                    })
            }

            $scope.GetTermsandConditions();

            $scope.SaveTermsandConditions = function () {
                var params = {
                    "TermsConditions": $scope.terms,
                    "sessionID": $scope.sessionID

                };
                auctionsService.SaveTermsandConditions(params)
                    .then(function (response) {
                        if (response.errorMessage) {
                            growlService.growl(response.errorMessage, "inverse");
                        }
                        else {
                            growlService.growl("Terms and Conditions Saved Successfully.", "success");
                            $scope.GetTermsandConditions();
                            $scope.addnewtermsView = false;
                            $scope.terms = {
                                U_ID: $scope.userID,
                                COMP_ID: $scope.compId,
                                TERM_ID: 0,
                                "sessionID": $scope.sessionID
                            };
                        }
                    });
            };

            $scope.addTermsandConditons = function () {
                $scope.terms = {};
                $scope.disabledEdit = false;
                $scope.addnewtermsView = true;
                $scope.terms.COMP_ID = $scope.compId;
                $scope.terms.sessionID = $scope.sessionID;
                $scope.terms.U_ID = $scope.userID;
                $scope.terms.IS_REQUIRED = 'Y';
                document.body.scrollTop = 0; // For Chrome, Safari and Opera 
                document.documentElement.scrollTop = 0; // For IE and Firefox
            };

            $scope.editTerms = function (termsCondition) {
                $scope.addnewtermsView = true;
                $scope.condition = termsCondition;
                $scope.disabledEdit = false;

                $scope.condition.TERM_ID = termsCondition.TERM_ID;

                $scope.terms = {
                    U_ID: $scope.userID,
                    TERM_ID: $scope.condition.TERM_ID,
                    COMP_ID: $scope.compId,
                    PO_TYPE: $scope.condition.PO_TYPE,
                    TITLE: $scope.condition.TITLE,
                    DESCRIPTION: $scope.condition.DESCRIPTION,
                    sessionID: $scope.sessionID,
                    IS_REQUIRED: $scope.condition.IS_REQUIRED
                }
            }

            $scope.disabledEdit = false;

            $scope.ViewTerms = function (termsCondition) {
                $scope.addnewtermsView = true;
                $scope.condition = termsCondition;
                $scope.disabledEdit = true;

                $scope.condition.TERM_ID = termsCondition.TERM_ID;

                $scope.terms = {

                    U_ID: $scope.userID,
                    TERM_ID: $scope.condition.TERM_ID,
                    COMP_ID: $scope.compId,
                    PO_TYPE: $scope.condition.PO_TYPE,
                    TITLE: $scope.condition.TITLE,
                    DESCRIPTION: $scope.condition.DESCRIPTION,
                }

            }

            $scope.closeEditterms = function () {
                $scope.addnewtermsView = false;
                $stae.reload();
                $scope.terms = {
                    userID: $scope.userID,
                    sessionID: $scope.sessionID
                };
            };
        }]);