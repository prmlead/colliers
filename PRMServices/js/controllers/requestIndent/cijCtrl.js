﻿prmApp

    .controller('cijCtrl', function ($scope, $http, $state, domain, $filter, $window, $log, $stateParams, $timeout, auctionsService,
        fwdauctionsService, userService, SignalRFactory, fileReader, growlService, workflowService, $rootScope, cijIndentService) {

        $scope.sessionID = userService.getUserToken();
        $scope.currentID = userService.getUserId();

        $scope.cijID = 0;

        if ($stateParams.cijID > 0) {
            $scope.cijID = $stateParams.cijID;

        }

        $scope.isEdit = false;

        $scope.isEditFunction = function (val) {
            $scope.isEdit = val;
        }

        $scope.date = new Date();
        $scope.isError = false;

        $scope.EditCijIndentDetails = function (val, type) {
            var params = {
                id: $scope.cijID,
                type: type,
                val: val,
                sessionID: $scope.sessionID
            }

            auctionsService.EditCijIndentDetails(params)
                .then(function (response) {
                    if (response.errorMessage != '') {
                        growlService.growl(response.errorMessage, "inverse");
                    }
                    else {
                        growlService.growl("Saved Successfully.", "success");
                    }
                })
        };




        $scope.isEditDisable = false;
        $scope.isFinanceDisable = true;
        $scope.isPurchaseDisable = true;
        $scope.isSaveDisable = false;


        $scope.date = new Date();

        $scope.date = new moment($scope.date).format("DD-MM-YYYY");

        $scope.seriesDB = 0;
        $scope.seriesDBDeptCode = '';
        $scope.isHOD = false;

        $scope.GetSeries = function (series, deptID) {

            cijIndentService.GetSeries(series, 'CIJ', deptID)
                .then(function (response) {
                    $scope.seriesDB = response.objectID;
                    $scope.seriesDBDeptCode = response.message;

                    if ($scope.seriesDB <= 0) {
                        $scope.seriesDB = 1;
                    }

                    if ($scope.cij.code == '' || $scope.cij.code == null || $scope.cij.code == undefined) {
                        if ($scope.seriesDB < 10) {
                            $scope.cij.code = 'KIMS/' + $scope.seriesDBDeptCode + '/CIJ/0000' + $scope.seriesDB;
                        }
                        if ($scope.seriesDB > 9 && $scope.seriesDB < 100) {
                            $scope.cij.code = 'KIMS/' + $scope.seriesDBDeptCode + '/CIJ/000' + $scope.seriesDB;
                        }
                        if ($scope.seriesDB > 99 && $scope.seriesDB < 1000) {
                            $scope.cij.code = 'KIMS/' + $scope.seriesDBDeptCode + '/CIJ/00' + $scope.seriesDB;
                        }
                        if ($scope.seriesDB > 999 && $scope.seriesDB < 10000) {
                            $scope.cij.code = 'KIMS/' + $scope.seriesDBDeptCode + '/CIJ/0' + $scope.seriesDB;
                        }
                    }
                })
        }





        /////////////
        // ITEMS
        ////////////




        $scope.indentDetails = {
            cij: {},
            listDeleteItems: [],
            isMedical: 1
        };

        $scope.indentDetails.cij.cijID = 0;

        $scope.indentDetails.listIndentItems = [];
        $scope.indentDetails.listDeleteItems = [];
        $scope.indentDetails.userDetailsList = [];

        $scope.item = {
            sNO: 1,
            materialDescription: '',
            makeModel: '',
            existFunctional: 0,
            existNonFunctional: 0,
            requiredQuantity: 0,
            units: '',
            purposeOfMaterial: '',
            fileName: {
                fileStream: [],
                fileName: ''
            },
            IndentItemID: 0
        };

        $scope.indentDetails.listIndentItems.push($scope.item);
        console.log($scope.item);
        console.log($scope.indentDetails.listIndentItems);

        $scope.addItem = function () {

            $scope.item = {
                sNO: 0,
                materialDescription: '',
                existFunctional: 0,
                existNonFunctional: 0,
                requiredQuantity: 0,
                purposeOfMaterial: '',
                IndentItemID: 0,
                fileName: {
                    fileStream: [],
                    fileName: ''
                }
            };

            $scope.indentDetails.listIndentItems.push($scope.item);

            var i = 0;

            $scope.indentDetails.listIndentItems.forEach(function (item, index) {
                i = i + 1;
                item.sNO = i;
            });

        };


        $scope.delItem = function (val, id) {

            if ($scope.indentDetails.listIndentItems.length > 1) {

                if (id > 0) {
                    $scope.indentDetails.listDeleteItems.push(id);
                }

                $scope.indentDetails.listIndentItems.splice(val, 1);


                var i = 0;

                $scope.indentDetails.listIndentItems.forEach(function (item, index) {
                    i = i + 1;
                    item.sNO = i;
                });
            }

        };


        /////////////
        // ITEMS
        ////////////



        ////////////////////////
        // WORK FLOW  
        ///////////////////////
        $scope.workflowList = [];
        $scope.itemWorkflow = [];
        $scope.workflowObj = {};
        $scope.workflowObj.workflowID = 0;
        $scope.currentStep = 0;
        ////////////////////////
        // WORK FLOW  
        ///////////////////////



        if ($stateParams.cijID) {
            $scope.cijID = $stateParams.cijID;
        } else {
            $scope.cijID = 0;
        };

        $scope.cijString = {
            cijType: 1,
            isValid: 1
        };

        $scope.sessionid = userService.getUserToken();

        $scope.cij = {

            assetType: 0,
            assetCategory: 0,
            departmentName: '',
            deptID: '',
            hodName: '',
            purpose: {
                Text: '',
                Link: 0,
                LinkName: ''
            },
            specifications: {
                Text: '',
                Link: 0,
                LinkName: ''
            },
            need: {
                Text: '',
                Link: 0,
                LinkName: ''
            },
            proposedSuppliers: {
                supplier1: {
                    name: '',
                    id: 0
                },
                supplier2: {
                    name: '',
                    id: 0
                },
                supplier3: {
                    name: '',
                    id: 0
                },
                supplier4: {
                    name: '',
                    id: 0
                }
            },
            noOfExixstingEquipment: 0,
            approximateCostRange: 0,


            proposedTotallInvestment: {
                baicEquipmentCost: 0,
                accessoriesCost: 0,
                installationCost: 0,
                anyOtherCost: 0,
                totalCost: 0
            },


            paybackPeriodAnanysis: {
                expectedNoOfProceduresPerMonth: 0,
                proposedTarrifPerProcedure: 0,
                approximateRunningCostPerMonth: 0,
                approximateRunningCostPerMonth1: 0,
                paybackPeriod: 0
            },

            valueAdditionToThePatient: {
                patientOutcome: '',
                patiencySafety: '',
                operationalEfficiency: ''
            },

            valueAdditionNonMedicalEquipment: {
                costReduction: 0,
                operationalEfficiency: '',
                increaseInEmployee: '',
                requestedBy: {
                    name: '',
                    signature: {
                        Text: '',
                        Link: 0,
                        LinkName: ''
                    },
                    date: $scope.date
                },
                recommendedBy: {
                    name: '',
                    signature: {
                        Text: '',
                        Link: 0,
                        LinkName: ''
                    },
                    date: $scope.date
                }

            },

            agingAnalysis: {
                dateOfInstallation: '',
                dateOfDeInstallation: '',
                noOfYearsServed: 0
            },

            expenditureAnalysis: {
                equipmentCost: 0,
                service: 0,
                sparesCost: 0
            },

            revenueAnalysis: {
                noOfProceduresDone: 0,
                incomeFromProcedures: 0,
                requestedBy: {
                    name: '',
                    signature: {
                        Text: '',
                        Link: 0,
                        LinkName: ''
                    },
                    date: $scope.date
                },
                recommendedBy: {
                    name: '',
                    signature: {
                        Text: '',
                        Link: 0,
                        LinkName: ''
                    },
                    date: $scope.date
                }
            },

            assessedAndApprovedBy: {
                name: '',
                mobile: '',
                signature: {
                    Text: '',
                    Link: 0,
                    LinkName: ''
                },
                date: $scope.date
            },

            budgetApprovedBy: {
                name: '',
                mobile: '',
                signature: {
                    Text: '',
                    Link: 0,
                    LinkName: ''
                },
                date: $scope.date
            },

            purchaseDepartmentUse: {
                name: '',
                poNo: '',
                poDate: '',
                supplierName: '',
                otherInfo: '',
                mobile: '',
                signature: {
                    Text: '',
                    Link: 0,
                    LinkName: ''
                },
            },

            expectedDelivery: '',
            userStatus: '',
            userPriority: 'MEDIUM'

        };

        $scope.isEndUser == false;

        if ($scope.cijID > 0) {

        } else {
            $scope.cij.departmentName = $rootScope.rootDetails.deptCode;
            $scope.cij.deptID = $rootScope.rootDetails.deptId;
        };

        if ($rootScope.rootDetails.desigCode.toUpperCase() == 'END USER') {
            $scope.isEndUser == true;
        };

        if ($scope.cij && $scope.cij.valueAdditionNonMedicalEquipment) {
            if ($scope.cij.valueAdditionNonMedicalEquipment.requestedBy && $scope.cij.valueAdditionNonMedicalEquipment.requestedBy.date == '') {
                $scope.cij.valueAdditionNonMedicalEquipment.requestedBy.date == $scope.date;
            }

            if ($scope.cij.valueAdditionNonMedicalEquipment.recommendedBy && $scope.cij.valueAdditionNonMedicalEquipment.recommendedBy.date) {
                $scope.cij.valueAdditionNonMedicalEquipment.recommendedBy.date == $scope.date;
            }
        }

        if ($scope.cij.revenueAnalysis) {
            if ($scope.cij.revenueAnalysis.requestedBy && $scope.cij.revenueAnalysis.requestedBy.date) {
                $scope.cij.revenueAnalysis.requestedBy.date == $scope.date;
            }

            if ($scope.cij.revenueAnalysis.recommendedBy && $scope.cij.revenueAnalysis.recommendedBy.date == '') {
                $scope.cij.revenueAnalysis.recommendedBy.date == $scope.date;
            }
        }

        if ($scope.cij.assessedAndApprovedBy.date == '') {
            $scope.cij.assessedAndApprovedBy.date == $scope.date;
        }

        if ($scope.cij.budgetApprovedBy.date == '') {
            $scope.cij.budgetApprovedBy.date == $scope.date;
        }

        if ($scope.cij.assessedAndApprovedBy.date == '') {
            $scope.cij.assessedAndApprovedBy.date == $scope.date;
        }



        $scope.BudgetCodes = [];

        $scope.GetBudgetCodes = function () {
            auctionsService.GetBudgetCodes($scope.currentID, $scope.sessionID)
                .then(function (response) {
                    $scope.BudgetCodes = response;
                    if ($rootScope.rootDetails.deptTypeID == 6 || $rootScope.rootDetails.deptTypeID == 5) {
                        $scope.BudgetCodes = $filter('filter')($scope.BudgetCodes, function (item) {
                            return item.deptID === $rootScope.rootDetails.deptId;
                        })
                    }
                })
        }

        $scope.GetBudgetCodes();


        $scope.GetCIJ = function () {

            cijIndentService.GetCIJ($scope.cijID, userService.getUserToken())
                .then(function (response) {
                    $scope.cijString = response;

                    var cijObj = $.parseJSON($scope.cijString.cij);

                    $scope.cij = cijObj;

                    if ($scope.cij && $scope.cij.valueAdditionNonMedicalEquipment) {
                        if ($scope.cij.valueAdditionNonMedicalEquipment.requestedBy && $scope.cij.valueAdditionNonMedicalEquipment.requestedBy.date == '') {
                            $scope.cij.valueAdditionNonMedicalEquipment.requestedBy.date == $scope.date;
                        }

                        if ($scope.cij.valueAdditionNonMedicalEquipment.recommendedBy && $scope.cij.valueAdditionNonMedicalEquipment.recommendedBy.date) {
                            $scope.cij.valueAdditionNonMedicalEquipment.recommendedBy.date == $scope.date;
                        }
                    }

                    if ($scope.cij.revenueAnalysis) {
                        if ($scope.cij.revenueAnalysis.requestedBy && $scope.cij.revenueAnalysis.requestedBy.date) {
                            $scope.cij.revenueAnalysis.requestedBy.date == $scope.date;
                        }

                        if ($scope.cij.revenueAnalysis.recommendedBy && $scope.cij.revenueAnalysis.recommendedBy.date == '') {
                            $scope.cij.revenueAnalysis.recommendedBy.date == $scope.date;
                        }
                    }


                    if ($scope.cij.assessedAndApprovedBy.date == '') {
                        $scope.cij.assessedAndApprovedBy.date == $scope.date;
                    }


                    if ($scope.cij.assessedAndApprovedBy.date == '') {
                        $scope.cij.assessedAndApprovedBy.date == $scope.date;
                    }


                    $log.info($scope.cij.purchaseDepartmentUse.poDate);


                })
        }


        $scope.GetCijItems = function () {

            cijIndentService.GetCijItems($scope.cijID, userService.getUserToken())
                .then(function (response) {
                    $scope.indentDetails.listIndentItems = response;


                    var i = 0;

                    $scope.indentDetails.listIndentItems.forEach(function (item, index) {
                        i = i + 1;
                        item.sNO = i;
                    });

                })
        }


        if ($scope.cijID > 0) {


            $scope.isEditDisable = true;
            $scope.isFinanceDisable = true;
            $scope.isPurchaseDisable = true;

            $scope.GetCIJ();
            $scope.GetCijItems();


        }
        else {

            $scope.isEditDisable = false;
            $scope.isFinanceDisable = true;
            $scope.isPurchaseDisable = true;

            $scope.GetSeries('', $rootScope.rootDetails.deptId);


        };

        $scope.totalPrice = function () {

            totalPrice = (parseFloat($scope.cij.proposedTotallInvestment.baicEquipmentCost) + parseFloat($scope.cij.proposedTotallInvestment.accessoriesCost) + parseFloat($scope.cij.proposedTotallInvestment.installationCost) + parseFloat($scope.cij.proposedTotallInvestment.anyOtherCost));
            $scope.cij.proposedTotallInvestment.totalCost = totalPrice;
        }



        $scope.priceChange = function (val) {
            if (val == 'APPROX') {
                $scope.cij.proposedTotallInvestment.baicEquipmentCost = parseInt($scope.cij.approximateCostRange)
                $scope.totalPrice();
            }
            if (val == 'BASIC') {
                $scope.cij.approximateCostRange = parseInt($scope.cij.proposedTotallInvestment.baicEquipmentCost)
                $scope.totalPrice();
            }
        }




        $scope.saveCIJ = function (isDraft) {

            $scope.isSaveDisable = true;
            $scope.validate($scope.cij, isDraft);

            if ($scope.isError == false) {
                var cijObjStr = JSON.stringify($scope.cij);
                var params = {
                    stringcij: {
                        cijCode: $scope.cij.code,
                        cij: cijObjStr,
                        compID: userService.getUserCompanyId(),
                        cijType: $scope.cijString.cijType,
                        cijID: $scope.cijID,
                        userID: $scope.currentID,
                        deptID: $scope.cij.deptID,
                        assetType: $scope.cij.assetType,
                        userPriority: $scope.cij.userPriority,
                        userStatus: $scope.cij.userStatus,
                        expectedDelivery: $scope.cij.expectedDelivery

                    },
                    indent: $scope.indentDetails
                };


                cijIndentService.saveCIJ(params)
                    .then(function (response) {

                        if ($scope.cijID > 0 && $scope.itemWorkflow.length > 0) {
                            $scope.workflowObj.workflowID = $scope.itemWorkflow[0].workflowID;
                        }

                        if ($scope.workflowObj.workflowID > 0) {
                            if (response.errorMessage != '') {
                                growlService.growl(response.errorMessage, "inverse");
                                $scope.isSaveDisable = false;
                            }
                            else {
                                if ($scope.itemWorkflow && $scope.itemWorkflow.length > 0
                                    && $scope.itemWorkflow[0].WorkflowTracks && $scope.itemWorkflow[0].WorkflowTracks.length > 0
                                    && $scope.cijString && $scope.cijString.createdBy != $scope.currentID) {
                                    growlService.growl("Saved Successfully.", "success");
                                    $scope.isSaveDisable = false;
                                    $scope.goToCIJ();
                                }
                                else {
                                    if (!isDraft) {
                                        $scope.assignWorkflow(response.objectID);
                                    } else {
                                        growlService.growl("Saved Successfully.", "success");
                                        $scope.goToCIJ();
                                        $scope.isSaveDisable = false;
                                    }
                                }
                            }
                        } else {
                            if (response.errorMessage != '') {
                                growlService.growl(response.errorMessage, "inverse");
                                $scope.isSaveDisable = false;
                            }
                            else {
                                growlService.growl("Saved Successfully.", "success");
                                $scope.goToCIJ();
                            }
                        }

                    });

            }
        }





        $scope.cancelClick = function () {
            var url = $state.href('cijList', {});
            window.open(url, '_self');
        }


        $scope.goToCIJ = function (id) {
            var url = $state.href('cijList', {});
            window.open(url, '_self');
        }



        $scope.fileUpload = {
            fileStream: [],
            fileName: ''
        }




        $scope.getFile1 = function (id, itemid, ext) {
            $scope.file = $("#" + id)[0].files[0];
            fileReader.readAsDataUrl($scope.file, $scope)
                .then(function (result) {


                    $scope.fileUpload = {
                        fileStream: [],
                        fileName: ''
                    };


                    if (id == "purposequotation") {
                        var bytearray = new Uint8Array(result);
                        $scope.fileUpload.fileStream = $.makeArray(bytearray);
                        $scope.fileUpload.fileName = $scope.file.name;
                        $scope.cij.purpose.LinkName = $scope.file.name;
                        $scope.saveFileUpload(id);
                    }

                    else
                        if (id == "needquotation") {
                            var bytearray = new Uint8Array(result);
                            $scope.fileUpload.fileStream = $.makeArray(bytearray);
                            $scope.fileUpload.fileName = $scope.file.name;
                            $scope.cij.need.LinkName = $scope.file.name;

                            $scope.saveFileUpload(id);
                        }

                        else
                            if (id == "specificationsquotation") {
                                var bytearray = new Uint8Array(result);
                                $scope.fileUpload.fileStream = $.makeArray(bytearray);
                                $scope.fileUpload.fileName = $scope.file.name;
                                $scope.cij.specifications.LinkName = $scope.file.name;

                                $scope.saveFileUpload(id);
                            }

                            else
                                if (id == "reqsignquotation") {
                                    var bytearray = new Uint8Array(result);
                                    $scope.fileUpload.fileStream = $.makeArray(bytearray);
                                    $scope.fileUpload.fileName = $scope.file.name;
                                    $scope.cij.valueAdditionNonMedicalEquipment.requestedBy.signature.LinkName = $scope.file.name;

                                    $scope.saveFileUpload(id);
                                }

                                else
                                    if (id == "recsignquotation") {
                                        var bytearray = new Uint8Array(result);
                                        $scope.fileUpload.fileStream = $.makeArray(bytearray);
                                        $scope.fileUpload.fileName = $scope.file.name;
                                        $scope.cij.valueAdditionNonMedicalEquipment.recommendedBy.signature.LinkName = $scope.file.name;

                                        $scope.saveFileUpload(id);
                                    }

                                    else
                                        if (id == "reqstmsignquotation") {
                                            var bytearray = new Uint8Array(result);
                                            $scope.fileUpload.fileStream = $.makeArray(bytearray);
                                            $scope.fileUpload.fileName = $scope.file.name;
                                            $scope.cij.revenueAnalysis.requestedBy.signature.LinkName = $scope.file.name;

                                            $scope.saveFileUpload(id);
                                        }


                                        else
                                            if (id == "recmsignquotation") {
                                                var bytearray = new Uint8Array(result);
                                                $scope.fileUpload.fileStream = $.makeArray(bytearray);
                                                $scope.fileUpload.fileName = $scope.file.name;
                                                $scope.cij.revenueAnalysis.recommendedBy.signature.LinkName = $scope.file.name;

                                                $scope.saveFileUpload(id);
                                            }

                                            else
                                                if (id == "signquotation") {
                                                    var bytearray = new Uint8Array(result);
                                                    $scope.fileUpload.fileStream = $.makeArray(bytearray);
                                                    $scope.fileUpload.fileName = $scope.file.name;
                                                    $scope.cij.assessedAndApprovedBy.signature.LinkName = $scope.file.name;

                                                    $scope.saveFileUpload(id);
                                                }

                                                else
                                                    if (id == "budgtsignquotation") {
                                                        var bytearray = new Uint8Array(result);
                                                        $scope.fileUpload.fileStream = $.makeArray(bytearray);
                                                        $scope.fileUpload.fileName = $scope.file.name;
                                                        $scope.cij.budgetApprovedBy.signature.LinkName = $scope.file.name;

                                                        $scope.saveFileUpload(id);
                                                    }

                                                    else
                                                        if (id == "deprtsignquotation") {
                                                            var bytearray = new Uint8Array(result);
                                                            $scope.fileUpload.fileStream = $.makeArray(bytearray);
                                                            $scope.fileUpload.fileName = $scope.file.name;
                                                            $scope.cij.purchaseDepartmentUse.signature.LinkName = $scope.file.name;

                                                            $scope.saveFileUpload(id);
                                                        }

                                                        else
                                                            if (id == "deprtsignquotation") {
                                                                var bytearray = new Uint8Array(result);
                                                                $scope.fileUpload.fileStream = $.makeArray(bytearray);
                                                                $scope.fileUpload.fileName = $scope.file.name;
                                                                $scope.cij.purchaseDepartmentUse.signature.LinkName = $scope.file.name;

                                                                $scope.saveFileUpload(id);
                                                            }
                                                            else
                                                                if (id == "itemFileUploadquotation") {
                                                                    var bytearray = new Uint8Array(result);
                                                                    $scope.fileUpload.fileStream = $.makeArray(bytearray);
                                                                    $scope.fileUpload.fileName = $scope.file.name;

                                                                    $scope.indentDetails.listIndentItems[itemid].fileName.fileName = $scope.file.name;
                                                                    $scope.saveFileUpload(id, itemid);
                                                                }


                });

        }


        $scope.saveFileUpload = function (id, itemid) {

            var params = {
                fileupload: $scope.fileUpload
            };

            cijIndentService.saveFileUpload(params)
                .then(function (response) {
                    if (response.errorMessage != '') {
                    }
                    else {

                        if (id == 'purposequotation') {
                            $scope.cij.purpose.Link = response.objectID;
                        }
                        else if (id == "needquotation") {
                            $scope.cij.need.Link = response.objectID;
                        }
                        else if (id == "specificationsquotation") {
                            $scope.cij.specifications.Link = response.objectID;
                        }
                        else if (id == "reqsignquotation") {
                            $scope.cij.valueAdditionNonMedicalEquipment.requestedBy.signature.Link = response.objectID;
                        }
                        else if (id == "recsignquotation") {
                            $scope.cij.valueAdditionNonMedicalEquipment.recommendedBy.signature.Link = response.objectID;
                        }
                        else if (id == "reqstmsignquotation") {
                            $scope.cij.revenueAnalysis.requestedBy.signature.Link = response.objectID;
                        }
                        else if (id == "recmsignquotation") {
                            $scope.cij.revenueAnalysis.recommendedBy.signature.Link = response.objectID;
                        }
                        else if (id == "signquotation") {
                            $scope.cij.assessedAndApprovedBy.signature.Link = response.objectID;
                        }
                        else if (id == "budgtsignquotation") {
                            $scope.cij.budgetApprovedBy.signature.Link = response.objectID;
                        }
                        else if (id == "deprtsignquotation") {
                            $scope.cij.purchaseDepartmentUse.signature.Link = response.objectID;
                        }
                        else if (id == "itemFileUploadquotation") {
                            $scope.indentDetails.listIndentItems[itemid].fileName.fileName = response.objectID;
                        }
                    }
                })
        }




        $scope.convertDate = function (inputdate) {
            var ts = moment(inputdate, "DD-MM-YYYY").valueOf();
            var m = moment(ts);
            var quotationDate = new Date(m);
            var milliseconds = parseInt(quotationDate.getTime() / 1000.0);
            inputdate = "/Date(" + milliseconds + "000+0530)/";
            return inputdate;
        }

        $scope.convertToReadableDate = function (inputdate) {
            inputdate = new moment(inputdate).format("DD-MM-YYYY");
            if (inputdate.indexOf('-9999') > -1) {
                inputdate = "";
            }
            return inputdate;
        };








        $scope.checkflow = function () {

            if ($scope.currentStep != 0) {
                $scope.isEditDisable = true;
            }
            if ($scope.currentStep == 1 && $scope.currentStepStatus == 'REJECTED' && $scope.cijString.createdBy == $scope.currentID) {
                $scope.isEditDisable = false;
            } else if ($scope.currentStep == 1 && $scope.currentStepStatus == 'REJECTED' && $scope.cijString.createdBy != $scope.currentID) {
                $scope.isEditDisable = true;
            }
            if ($scope.currentStep == 1 && ($scope.currentStepStatus == 'PENDING' || $scope.currentStepStatus == 'HOLD') && $scope.HODdesigIDApprover == $rootScope.rootDetails.desigId && $scope.HODdeptIDApprover == $rootScope.rootDetails.deptId) {
                $scope.isEditDisable = false;
                $scope.isFinanceDisable = true;
                $scope.isPurchaseDisable = true;
            }
            if ($scope.currentStep == 2 && ($scope.currentStepStatus == 'PENDING' || $scope.currentStepStatus == 'HOLD') && $scope.PURCHASEdesigIDApprover == $rootScope.rootDetails.desigId && $scope.PURCHASEdeptIDApprover == $rootScope.rootDetails.deptId) {
                $scope.isEditDisable = true;
                $scope.isFinanceDisable = true;
                $scope.isPurchaseDisable = false;
            }
            if ($scope.currentStep == 4 && ($scope.currentStepStatus == 'PENDING' || $scope.currentStepStatus == 'HOLD') && $scope.FINANCEdesigIDApprover == $rootScope.rootDetails.desigId && $scope.FINANCEdeptIDApprover == $rootScope.rootDetails.deptId) {
                $scope.isEditDisable = true;
                $scope.isFinanceDisable = false;
                $scope.isPurchaseDisable = true;
            }
        };







        $scope.checkflow();

        ////////////////////////
        // WORK FLOW  
        ///////////////////////


        $scope.getWorkflows = function () {
            workflowService.getWorkflowList()
                .then(function (response) {
                    $scope.workflowListTemp = response;

                    $scope.workflowListTemp.forEach(function (item, index) {

                        if (item.WorkflowModule == 'CIJ') {
                            $scope.workflowList.push(item);
                        }

                    });

                    $log.info($scope.workflowList);

                    $scope.workflowList = $scope.workflowList.filter(function (item) {
                        return item.deptID === $rootScope.rootDetails.deptId;
                    });



                });
        };


        $scope.getItemWorkflow = function () {
            workflowService.getItemWorkflow(0, $scope.cijID, 'CIJ')
                .then(function (response) {
                    $scope.itemWorkflow = response;
                    //console.log(itemWorkflow[0].WorkflowTracks);
                    if ($scope.itemWorkflow && $scope.itemWorkflow.length > 0 && $scope.itemWorkflow[0].WorkflowTracks.length > 0) {


                        $scope.currentStep = 0;

                        var count = 0;
                        $scope.itemWorkflow[0].WorkflowTracks.forEach(function (track) {
                            $scope.workflowObj.workflowID = $scope.itemWorkflow[0].workflowID;

                            if (track.status == 'REJECTED' && count == 0) {
                                count = count + 1;
                            }

                            if ((track.status == 'PENDING' || track.status == 'HOLD') && count == 0) {
                                count = count + 1;
                                $scope.IsUserApproverForStage(track.approverID);
                                $scope.currentAccess = track.order;
                            }

                            if ((track.status == 'PENDING' || track.status == 'HOLD' || track.status == 'REJECTED') && $scope.currentStep == 0) {
                                $scope.currentStep = track.order;
                                $scope.currentStepStatus = track.status;
                                if ($scope.currentStep != 1) {
                                }
                                return false;
                            }
                        });


                        if ($scope.itemWorkflow[0].WorkflowTracks.length > 0) {
                            $scope.HODdesigIDApprover = $scope.itemWorkflow[0].WorkflowTracks[0].approver.desigID;
                            $scope.HODdeptIDApprover = $scope.itemWorkflow[0].WorkflowTracks[0].department.deptID;
                        }

                        if ($scope.itemWorkflow[0].WorkflowTracks.length > 1) {
                            $scope.PURCHASEdesigIDApprover = $scope.itemWorkflow[0].WorkflowTracks[1].approver.desigID;
                            $scope.PURCHASEdeptIDApprover = $scope.itemWorkflow[0].WorkflowTracks[1].department.deptID;
                        }

                        if ($scope.itemWorkflow[0].WorkflowTracks.length > 3) {
                            $scope.FINANCEdesigIDApprover = $scope.itemWorkflow[0].WorkflowTracks[3].approver.desigID;
                            $scope.FINANCEdeptIDApprover = $scope.itemWorkflow[0].WorkflowTracks[3].department.deptID;
                        }

                        //$scope.GetUserDeptDesig();


                    }
                    else {
                        $scope.isEditDisable = false;
                    }
                });
        };


        if ($scope.cijID > 0) {
            $scope.getItemWorkflow();
        }


        $scope.isSaveEnabled = function () {
            if ($scope.itemWorkflow && $scope.itemWorkflow.length > 0 && $scope.itemWorkflow[0].WorkflowTracks.length > 0) {
                if ($scope.itemWorkflow[0].WorkflowTracks[0].status == 'PENDING' || $scope.itemWorkflow[0].WorkflowTracks[0].status == 'HOLD' || $scope.itemWorkflow[0].WorkflowTracks[0].status == 'REJECTED') {
                    return false;
                }
            }
            else {
                return false;
            }

            return true;
        };




        $scope.updateTrack = function (step, status) {


            $scope.commentsError = '';
            if (status == 'REJECTED' && (step.comments == null || step.comments == "")) {
                $scope.commentsError = 'Please enter comments';
                return false;
            }

            step.status = status;
            step.sessionID = $scope.sessionID;
            step.modifiedBy = $scope.currentID;
            step.moduleCode = $scope.cij.code;
            step.moduleName = 'CIJ';

            workflowService.SaveWorkflowTrack(step)
                .then(function (response) {
                    if (response.errorMessage != '') {
                        growlService.growl(response.errorMessage, "inverse");
                    }
                    else {
                        $scope.saveCIJ();
                        //growlService.growl("Saved Successfully.", "success");
                        $scope.getItemWorkflow();
                        location.reload();
                    }
                })
        };


        $scope.assignWorkflow = function (moduleID) {
            workflowService.assignWorkflow(({ wID: $scope.workflowObj.workflowID, moduleID: moduleID, user: $scope.currentID, sessionID: $scope.sessionID }))
                .then(function (response) {
                    if (response.errorMessage != '') {
                        growlService.growl(response.errorMessage, "inverse");
                        $scope.isSaveDisable = false;
                    }
                    else {
                        $scope.goToCIJ();
                    }
                })
        };

        $scope.getWorkflows();

        $scope.IsUserApprover = false;

        $scope.functionResponse = false;
        $scope.IsUserApproverForStage = function (approverID) {
            workflowService.IsUserApproverForStage(approverID, $scope.currentID)
                .then(function (response) {
                    $scope.IsUserApprover = response;
                });
        };




        ////////////////////////
        // WORK FLOW  
        ///////////////////////

        $scope.dateFormat = function (date) {
            return new moment(date).format("DD-MM-YYYY HH:mm");
        }

        $scope.validate = function (cij, isDraft) {

            $scope.isValidToSubmit = false;
            $scope.errorMessage = '';

            $scope.isError = false;

            $scope.isErrorDepartmentName = $scope.isErrorHodName = $scope.isErrorPurposeText = $scope.isErrorSpecificationsText = $scope.isErrorNeedText =
                $scope.isErrorSupplierName1 = $scope.isErrorSupplierName2 = $scope.isErrorSupplierName3 = $scope.isErrorSupplierName4 = $scope.isErrorExixstingEquipment = $scope.isApproxErrorTotalCost = $scope.isErrorTotalCost = $scope.isErrorYearsServed =
                $scope.isErrorEquipmentcost = $scope.isErrorServiceCost = $scope.isErrorSparesCost = $scope.isErrorProceduresDone = $scope.isErrorIncomeFromProcedures =
                $scope.isErrorRequestedByName = $scope.isErrorRecommendedByName = $scope.isErrorRequestedBySignature = $scope.isErrorRecommendedBySignature =
                $scope.isErrorApprovedByName = $scope.isErrorApprovedBySignature = $scope.isErrorBudgetApprovedByName =
                $scope.isErrorBudgetApprovedBySignature = $scope.isErrorPurchaseDepartmentUse = $scope.isErrorSupplierName = $scope.isErrorOtherInfo = $scope.isErrorSignatureInfo =
                $scope.isErrorProceduresPerMonth = $scope.isErrorTarrifPerProcedure = $scope.isErrorRunningCostPerMonth = $scope.isErrorApproxRunningCostPerMonth = $scope.isErrorPaybackPeriod =
                $scope.isErrorworkflowID = $scope.isErrorMaterialDescription = $scope.isErrorRequiredQuantity = $scope.isErrorPurposeOfMaterial
                = false;



            if ($scope.cij.departmentName == null || $scope.cij.departmentName == "" || $scope.cij.departmentName == undefined) {
                $scope.isErrorDepartmentName = true;
                $scope.isError = true;
                $scope.errorMessage = 'Department Name';
                $scope.isSaveDisable = false;

            }

            if ($scope.cij.hodName == null || $scope.cij.hodName == "" || $scope.cij.hodName == undefined) {
                $scope.isErrorHodName = true;
                $scope.isError = true;
                $scope.errorMessage = 'HOD Name';
                $scope.isSaveDisable = false;
            }

            if (($scope.cij.specifications.Text == undefined || $scope.cij.specifications.Text == null || $scope.cij.specifications.Text == "") && ($scope.cij.specifications.Link <= 0)) {
                $scope.isErrorSpecificationsText = true;
                $scope.isError = true;
                $scope.errorMessage = 'Text specifications is missing';
                $scope.isSaveDisable = false;
            }

            if (($scope.cij.need.Text == undefined || $scope.cij.need.Text == null || $scope.cij.need.Text == "") && ($scope.cij.need.Link <= 0)) {
                $scope.isErrorNeedText = true;
                $scope.isError = true;
                $scope.errorMessage = 'Text Need is missing';
                $scope.isSaveDisable = false;
            }

            if ($scope.cij.approximateCostRange <= 0 || $scope.cij.approximateCostRange == "") {
                $scope.isApproxErrorTotalCost = true;
                $scope.isError = true;
                $scope.errorMessage = 'Approximate Cost Range is Missing';
                $scope.isSaveDisable = false;
            }


            if ($scope.cijID == 0 && !isDraft && ($scope.workflowObj.workflowID == null || $scope.workflowObj.workflowID == "" || $scope.workflowObj.workflowID == undefined)) {
                $scope.isErrorworkflowID = true;
                $scope.isError = true;
                $scope.errorMessage = 'workflow ID is Missing';
                $scope.isSaveDisable = false;
            }
            else if ($scope.cijID > 0 && !isDraft && ($scope.workflowObj.workflowID == null || $scope.workflowObj.workflowID == "" || $scope.workflowObj.workflowID == undefined)) {
                $scope.isErrorworkflowID = true;
                $scope.isError = true;
                $scope.errorMessage = 'workflow ID is Missing';
                $scope.isSaveDisable = false;
            }

            $scope.tableValidation = false;

            if (1 == 1) {
                $scope.indentDetails.listIndentItems.forEach(function (item, index) {
                    item.isErrorRequiredQuantity = false;
                    item.isErrorBudgetCode = false;
                    item.isErrorMaterialDescription = false;
                    item.isErrorPurposeOfMaterial = false;
                    item.isErrorMaterialUnits = false;

                    item.style = {};

                    if (item.units == null || item.units == '' || item.units == undefined) {
                        $scope.isError = true;
                        $scope.errorMessage = 'Material Units is Missing';
                        $scope.units = true;
                        $scope.tableValidation = true;
                        item.isErrorMaterialUnits = true;
                        $scope.isSaveDisable = false;
                    }

                    if (item.budgetID == null || item.budgetID == '' || item.budgetID == undefined || item.budgetID <= 0) {
                        $scope.isError = true;
                        $scope.errorMessage = 'Budget code is Missing';
                        $scope.isErrorBudgetCode = true;
                        $scope.tableValidation = true;

                        item.isErrorBudgetCode = true;
                        $scope.isSaveDisable = false;
                        item.style = { "border": "1px solid #ff0000" };
                    }

                    if (item.materialDescription == null || item.materialDescription == '' || item.materialDescription == undefined) {
                        $scope.isError = true;
                        $scope.errorMessage = 'Material Description is Missing';
                        $scope.isErrorMaterialDescription = true;
                        $scope.tableValidation = true;
                        item.isErrorMaterialDescription = true;
                        $scope.isSaveDisable = false;
                    }
                    if (item.requiredQuantity <= 0 || item.requiredQuantity == undefined || item.requiredQuantity == '') {
                        $scope.isError = true;
                        $scope.errorMessage = 'Required Quantity is Missing';
                        $scope.isErrorRequiredQuantity = true;
                        $scope.tableValidation = true;
                        item.isErrorRequiredQuantity = true;
                        $scope.isSaveDisable = false;
                    }
                    if (item.purposeOfMaterial == null || item.purposeOfMaterial == '' || item.purposeOfMaterial == undefined) {
                        $scope.isError = true;
                        $scope.errorMessage = 'Purpose Of Material is Missing';
                        $scope.isErrorPurposeOfMaterial = true;
                        $scope.tableValidation = true;
                        item.isErrorPurposeOfMaterial = true;
                        $scope.isSaveDisable = false;
                    }
                });

                if ($scope.tableValidation) {
                    return false;
                }
            }

        };


        $scope.getDateDiff = function (date2, date1) {

            $scope.cij.agingAnalysis.noOfYearsServed = 0;

            var date2 = date2.split("-", 4);
            var year2 = 0;

            if (date2 && date2.length > 2) {
                year2 = parseInt(date2[2]);
            }

            var date1 = date1.split("-", 4);
            var year1 = 0;

            if (date1 && date1.length > 2) {
                year1 = parseInt(date1[2]);
            }


            if (year2 > 999 && year2 < 10000 && year1 > 999 && year1 < 10000 && year2 > year1) {
                $scope.cij.agingAnalysis.noOfYearsServed = year2 - year1;
            } else {
                $scope.cij.agingAnalysis.noOfYearsServed = 0;
            }

        }

        $scope.deleteFile = function (val) {
            if (val == 'specifications') {
                $scope.cij.specifications.Link = 0;
                $scope.cij.specifications.LinkName = '';
            }
            else if (val == 'need') {
                $scope.cij.need.Link = 0;
                $scope.cij.need.LinkName = '';
            }
            else if (val == 'revenueAnalysis.requestedBy.signature') {
                $scope.cij.revenueAnalysis.requestedBy.signature.Link = 0;
                $scope.cij.revenueAnalysis.requestedBy.signature.LinkName = '';
            }
            else if (val == 'revenueAnalysis.recommendedBy.signature') {
                $scope.cij.revenueAnalysis.recommendedBy.signature.Link = 0;
                $scope.cij.revenueAnalysis.recommendedBy.signature.LinkName = '';
            }
            else if (val == 'assessedAndApprovedBy.signature') {
                $scope.cij.assessedAndApprovedBy.signature.Link = 0;
                $scope.cij.assessedAndApprovedBy.signature.LinkName = '';
            }
            else if (val == 'budgetApprovedBy.signature') {
                $scope.cij.budgetApprovedBy.signature.Link = 0;
                $scope.cij.budgetApprovedBy.signature.LinkName = '';
            }
            else if (val == 'purchaseDepartmentUse.signature') {
                $scope.cij.purchaseDepartmentUse.signature.Link = 0;
                $scope.cij.purchaseDepartmentUse.signature.LinkName = '';
            }

            else if (val == 'valueAdditionNonMedicalEquipment.requestedBy.signature') {
                $scope.cij.valueAdditionNonMedicalEquipment.requestedBy.signature.Link = 0;
                $scope.cij.valueAdditionNonMedicalEquipment.requestedBy.signature.LinkName = '';
            }

            else if (val == 'valueAdditionNonMedicalEquipment.recommendedBy.signature') {
                $scope.cij.valueAdditionNonMedicalEquipment.recommendedBy.signature.Link = 0;
                $scope.cij.valueAdditionNonMedicalEquipment.recommendedBy.signature.LinkName = '';
            }
            else if (val == '') {
                $scope.item.itemFileUpload.Link = 0;
                $scope.item.itemFileUpload.LinkName = '';
            }
        };

        $scope.myDeptUser = [];
        $scope.itDeptDesig = [];
        $scope.BudgetDept = [];
        $scope.PurchaseDept = [];
        $scope.currentDept = [];

        $scope.getSubUserData = function () {

            var params = { "userid": $scope.currentID, "sessionid": $scope.sessionID }

            userService.getSubUsersData(params)
                .then(function (response) {
                    $scope.activeUserObj = response;

                    $scope.activeUserObj.forEach(function (item, index) {

                        if (item.isValid == true) {

                            auctionsService.GetUserDeptDesig(item.userID, userService.getUserToken())
                                .then(function (response) {
                                    item.UserDeptDesig = response;

                                    item.UserDeptDesig.forEach(function (item2, index2) {
                                        if (item2.isValid == true) {

                                            if (item2.companyDepartments.deptID == $rootScope.rootDetails.deptId) {
                                                $scope.currentDept.push(item);
                                            }

                                            if (item2.companyDepartments.deptCode.toUpperCase() == 'DIRECTOR') {
                                                $scope.myDeptUser.push(item)
                                            }
                                            if (item2.companyDepartments.deptCode.toUpperCase() == 'BUDGET' || item2.companyDepartments.deptCode.toUpperCase() == 'FINANCE' || item2.companyDepartments.deptCode.toUpperCase() == 'ACCOUNT DEPARTMENT') {
                                                $scope.BudgetDept.push(item);

                                                $scope.BudgetDept.forEach(function (item3, index3) {
                                                    if (item3.isValid == true) {
                                                        item3.UserDeptDesig.forEach(function (item4, index4) {
                                                            if (item4.isValid == true) {
                                                                item4.listUserDesignations.forEach(function (item5, index5) {
                                                                    if (item5.isValid == true) {
                                                                        if (item5.companyDesignations.desigCode.toUpperCase() == 'HOD' || item5.companyDesignations.desigCode.toUpperCase() == 'FINANCE HEAD' || item5.companyDesignations.desigCode.toUpperCase() == 'FINANCE') {
                                                                            $scope.HODBudgetName = item3.firstName + ' ' + item3.lastName;
                                                                            $scope.HODBudgetPhone = item3.phoneNum
                                                                            if ($scope.cijID > 0) {
                                                                            } else {
                                                                                $scope.cij.budgetApprovedBy.name = $scope.HODBudgetName;
                                                                                $scope.cij.budgetApprovedBy.mobile = $scope.HODBudgetPhone;
                                                                            }
                                                                        }
                                                                    }
                                                                })
                                                            }
                                                        })
                                                    }
                                                })
                                            }
                                            if (item2.companyDepartments.deptCode.toUpperCase() == 'PURCHASE' || item2.companyDepartments.deptCode.toUpperCase() == 'PURCHASE DEPARTMENT') {
                                                $scope.PurchaseDept.push(item);

                                                $scope.PurchaseDept.forEach(function (item3, index3) {
                                                    if (item3.isValid == true) {
                                                        item3.UserDeptDesig.forEach(function (item4, index4) {
                                                            if (item4.isValid == true) {
                                                                item4.listUserDesignations.forEach(function (item5, index5) {
                                                                    if (item5.isValid == true) {
                                                                        if (item5.companyDesignations.desigCode.toUpperCase() == 'HOD' || item5.companyDesignations.desigCode.toUpperCase() == 'HEAD OF PROCUREMENT' || item5.companyDesignations.desigCode.toUpperCase() == 'PURCHASE HEAD') {
                                                                            $scope.purchaseName = item3.firstName + ' ' + item3.lastName;
                                                                            $scope.purchasePhone = item3.phoneNum
                                                                            if ($scope.cijID > 0) {
                                                                            } else {
                                                                                $scope.cij.purchaseDepartmentUse.name = $scope.purchaseName;
                                                                                $scope.cij.purchaseDepartmentUse.mobile = $scope.purchasePhone;

                                                                            }
                                                                        }
                                                                    }
                                                                })
                                                            }
                                                        })
                                                    }
                                                })

                                            }
                                            if (item2.companyDepartments.deptCode.toUpperCase() == 'IT') {
                                                $scope.itDeptDesig.push(item);
                                            }
                                        }
                                    })

                                    $scope.myDeptUser.forEach(function (item3, index3) {
                                        if (item3.isValid == true) {
                                            item3.UserDeptDesig.forEach(function (item4, index4) {
                                                if (item4.isValid == true) {
                                                    item4.listUserDesignations.forEach(function (item5, index5) {
                                                        if (item5.isValid == true) {
                                                            if (item5.companyDesignations.desigCode.toUpperCase() == 'DIRECTOR') {

                                                                $scope.HODName = item3.firstName + ' ' + item3.lastName;
                                                                $scope.HODPhone = item3.phoneNum
                                                                if ($scope.cijID > 0) {
                                                                } else {
                                                                    $scope.cij.assessedAndApprovedBy.name = $scope.HODName;
                                                                    $scope.cij.assessedAndApprovedBy.mobile = $scope.HODPhone;

                                                                    $scope.cij.revenueAnalysis.recommendedBy.name = $scope.HODName;
                                                                    $scope.cij.revenueAnalysis.recommendedBy.signature.Text = $scope.HODName;

                                                                    $scope.cij.valueAdditionNonMedicalEquipment.recommendedBy.name = $scope.HODName;
                                                                    $scope.cij.valueAdditionNonMedicalEquipment.recommendedBy.signature.Text = $scope.HODName;
                                                                }
                                                            }
                                                        }
                                                    })
                                                }
                                            })
                                        }
                                    })


                                    $scope.currentDept.forEach(function (item31, index3) {
                                        if (item31.isValid == true) {
                                            item31.UserDeptDesig.forEach(function (item4, index4) {
                                                if (item4.isValid == true) {
                                                    item4.listUserDesignations.forEach(function (item5, index5) {
                                                        if (item5.isValid == true) {
                                                            if (item5.companyDesignations.desigCode.toUpperCase() == 'HOD') {



                                                                $scope.HODName = item31.firstName + ' ' + item31.lastName;
                                                                $scope.HODPhone = item31.phoneNum
                                                                if ($scope.cijID > 0) {
                                                                } else {
                                                                    ///DEPT HOD NAME////

                                                                    $scope.cij.hodName = $scope.HODName;
                                                                    $scope.cij.revenueAnalysis.recommendedBy.name = $scope.HODName;
                                                                    $scope.cij.revenueAnalysis.recommendedBy.signature.Text = $scope.HODName;

                                                                    $scope.cij.valueAdditionNonMedicalEquipment.recommendedBy.name = $scope.HODName;
                                                                    $scope.cij.valueAdditionNonMedicalEquipment.recommendedBy.signature.Text = $scope.HODName;
                                                                }
                                                            }
                                                        }
                                                    })
                                                }
                                            })
                                        }
                                    })


                                    $scope.itDeptDesig.forEach(function (item3, index3) {
                                        if (item3.isValid == true) {
                                            item3.UserDeptDesig.forEach(function (item4, index4) {
                                                if (item4.isValid == true) {
                                                    item4.listUserDesignations.forEach(function (item5, index5) {
                                                        if (item5.isValid == true) {
                                                            if (item5.companyDesignations.desigCode.toUpperCase() == 'HOD') {
                                                                $scope.ITHODName = item3.firstName + ' ' + item3.lastName;
                                                                if ($scope.cijID > 0) {
                                                                } else {
                                                                }
                                                            }
                                                        }
                                                    })
                                                }
                                            })
                                        }
                                    })

                                    $scope.BudgetDept.forEach(function (item3, index3) {
                                        if (item3.isValid == true) {
                                            item3.UserDeptDesig.forEach(function (item4, index4) {
                                                if (item4.isValid == true) {
                                                    item4.listUserDesignations.forEach(function (item5, index5) {
                                                        if (item5.isValid == true) {
                                                            if (item5.companyDesignations.desigCode.toUpperCase() == 'HOD' || item5.companyDesignations.desigCode.toUpperCase() == 'FINANCE HEAD' || item5.companyDesignations.desigCode.toUpperCase() == 'FINANCE') {
                                                                $scope.HODBudgetName = item3.firstName + ' ' + item3.lastName;
                                                                $scope.HODBudgetPhone = item3.phoneNum
                                                                if ($scope.cijID > 0) {
                                                                } else {
                                                                    $scope.cij.budgetApprovedBy.name = $scope.HODBudgetName;
                                                                    $scope.cij.budgetApprovedBy.mobile = $scope.HODBudgetPhone;
                                                                }
                                                            }
                                                        }
                                                    })
                                                }
                                            })
                                        }
                                    })

                                    $scope.PurchaseDept.forEach(function (item3, index3) {
                                        if (item3.isValid == true) {
                                            item3.UserDeptDesig.forEach(function (item4, index4) {
                                                if (item4.isValid == true) {
                                                    item4.listUserDesignations.forEach(function (item5, index5) {
                                                        if (item5.isValid == true) {
                                                            if (item5.companyDesignations.desigCode.toUpperCase() == 'HOD' || item5.companyDesignations.desigCode.toUpperCase() == 'HEAD OF PROCUREMENT') {
                                                                $scope.purchaseName = item3.firstName + ' ' + item3.lastName;
                                                                $scope.purchasePhone = item3.phoneNum
                                                                if ($scope.cijID > 0) {
                                                                } else {
                                                                    $scope.cij.purchaseDepartmentUse.name = $scope.purchaseName;
                                                                    $scope.cij.purchaseDepartmentUse.mobile = $scope.purchasePhone;

                                                                }
                                                            }
                                                        }
                                                    })
                                                }
                                            })
                                        }
                                    })
                                })
                        }
                        //$scope.GetUserDeptDesig();

                    });

                });
        }

        $scope.getSubUserData();

        $scope.Getuserinfo = function () {
            auctionsService.GetUserDetails(userService.getUserId(), userService.getUserToken())
                .then(function (response) {
                    $scope.GetUserDetailsList = response;
                    if ($scope.cijID == 0) {
                        $scope.cij.revenueAnalysis.requestedBy.name = $scope.GetUserDetailsList.firstName + ' ' + $scope.GetUserDetailsList.lastName;
                        $scope.cij.revenueAnalysis.requestedBy.signature.Text = $scope.GetUserDetailsList.firstName + ' ' + $scope.GetUserDetailsList.lastName;

                        $scope.cij.valueAdditionNonMedicalEquipment.requestedBy.name = $scope.GetUserDetailsList.firstName + ' ' + $scope.GetUserDetailsList.lastName;
                        $scope.cij.valueAdditionNonMedicalEquipment.requestedBy.signature.Text = $scope.GetUserDetailsList.firstName + ' ' + $scope.GetUserDetailsList.lastName;
                    }
                });
        };

        $scope.Getuserinfo();









        $scope.UserDetailsByDeptDesigTypeID = {};

        $scope.GetUserDetailsByDeptDesigTypeID = function () {
            auctionsService.GetUserDetailsByDeptDesigTypeID(userService.getUserCompanyId(), $rootScope.rootDetails.deptTypeID, $rootScope.rootDetails.desigTypeID, userService.getUserToken())
                .then(function (response) {
                    $scope.UserDetailsByDeptDesigTypeID = response;
                });
        };

        $scope.GetUserDetailsByDeptDesigTypeID();



    });