﻿prmApp
    .controller('billToCtrl', ["$scope", "$stateParams", "$log", "$state", "$window", "userService", "auctionsService", "storeService", "growlService", "PRMProjectServices",
        function ($scope, $stateParams, $log, $state, $window, userService, auctionsService, storeService, growlService, PRMProjectServices) {

            $scope.userID = userService.getUserId();
            $scope.compId = userService.getUserCompanyId();
            $scope.sessionID = userService.getUserToken();

            $scope.ProjectBillToDetailsList = [];

            $scope.addnewBillTo = false;

            /*PAGINATION CODE*/
            $scope.totalItems = 0;
            $scope.currentPage = 1;
            $scope.itemsPerPage = 10;
            $scope.maxSize = 5;


            $scope.setPage = function (pageNo) {
                $scope.currentPage = pageNo;
            };

            $scope.pageChanged = function () {
                ////console.log('Page changed to: ' + $scope.currentPage);
            };
            /*PAGINATION CODE*/


            $scope.BILL_TO =
            {
                BILL_TO_ID: 0,
                COMP_ID: +$scope.compId,
                U_ID: +$scope.userID,
                LEGAL_ENTITY_NAME: '',
                ADDRESS: '',
                SPOC_EMAIL_ADDRESS: '',
                GSTIN: '',
                PAN : ''
            };

            $scope.GetProjectBillToDetails = function ()
            {
                $scope.ProjectBillToDetailsList = [];
                var params = {
                    "U_ID": $scope.userID,
                    "COMP_ID": $scope.compId,
                    "searchString": '',
                    "sessionID": $scope.sessionID
                };
                PRMProjectServices.GetProjectBillToDetails(params)
                    .then(function (response) {
                        if (response)
                        {
                            $scope.ProjectBillToDetailsList = JSON.parse(response).Table;
                            $scope.ProjectBillToDetailsListTemp = $scope.ProjectBillToDetailsList;
                            $scope.totalItems = $scope.ProjectBillToDetailsList.length;
                        }
                    });
            };

            $scope.GetProjectBillToDetails();


            $scope.saveProjectBillToDetails = function ()
            {
                $scope.LEGAL_ENTITY_NAME_VALIDATION = $scope.ADDRESS_VALIDATION = $scope.SPOC_EMAIL_ADDRESS_VALIDATION = $scope.GSTIN_VALIDATION = $scope.PAN_VALIDATION = false;

                if (!$scope.BILL_TO.LEGAL_ENTITY_NAME) {
                    $scope.LEGAL_ENTITY_NAME_VALIDATION = true;
                    return false;
                }

                if (!$scope.BILL_TO.ADDRESS) {
                    $scope.ADDRESS_VALIDATION = true;
                    return false;
                }

                if (!$scope.BILL_TO.SPOC_EMAIL_ADDRESS) {
                    $scope.SPOC_EMAIL_ADDRESS_VALIDATION = true;
                    return false;
                }

                if (!$scope.BILL_TO.GSTIN) {
                    $scope.GSTIN_VALIDATION = true;
                    return false;
                }

                if (!$scope.BILL_TO.PAN) {
                    $scope.PAN_VALIDATION = true;
                    return false;
                }


                var params =
                {
                    "billTo": $scope.BILL_TO,
                    "sessionid": $scope.sessionID
                };

                PRMProjectServices.saveProjectBillToDetails(params)
                    .then(function (response) {
                        if (response.errorMessage) {
                            swal("Error!", response.errorMessage, "error");
                        } else {
                            growlService.growl('Bill To Created Successfully.', "success");
                            location.reload();
                        };
                    });
            };

            $scope.createShow = function () {
                $scope.addnewBillTo = true;
                $scope.showSaveButton = true;
            };

            $scope.showSaveButton = true;

            $scope.cancelProjectBillToDetails = function () {
                $scope.addnewBillTo = false;
            };
            $scope.editBillTo = function (billToObj,type) {
                $scope.addnewBillTo = true;
                $scope.showSaveButton = false;
                if (type === 'EDIT') {
                    $scope.showSaveButton = true;
                } else {
                    $scope.showSaveButton = false;
                }

                $scope.BILL_TO =
                {
                    BILL_TO_ID: billToObj.BILL_TO_ID,
                    COMP_ID: +$scope.compId,
                    U_ID: +$scope.userID,
                    LEGAL_ENTITY_NAME: billToObj.LEGAL_ENTITY_NAME,
                    ADDRESS: billToObj.ADDRESS,
                    SPOC_EMAIL_ADDRESS: billToObj.SPOC_EMAIL_ADDRESS,
                    GSTIN: billToObj.GSTIN,
                    PAN: billToObj.PAN
                };

            };


            $scope.searchTable = function (str)
            {
                if (str) {
                    str = str.toLowerCase();
                    $scope.ProjectBillToDetailsList = $scope.ProjectBillToDetailsListTemp.filter(function (billTo) {
                        return (String(billTo.LEGAL_ENTITY_NAME.toLowerCase()).includes(str) == true
                            || String(billTo.ADDRESS.toLowerCase()).includes(str) == true
                            || String(billTo.SPOC_EMAIL_ADDRESS.toLowerCase()).includes(str) == true
                            || String(billTo.GSTIN.toLowerCase()).includes(str) == true
                            || String(billTo.PAN.toLowerCase()).includes(str) == true);
                    });
                } else {
                    $scope.ProjectBillToDetailsList = $scope.ProjectBillToDetailsListTemp;
                }

                $scope.totalItems = $scope.ProjectBillToDetailsList.length;
            };

        }]);