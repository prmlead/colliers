﻿prmApp

    //=================================================
    // ProfileAbout
    //=================================================

    .controller('profieAboutCtrl', function ($timeout, $uibModal, $state, $window, $scope, growlService, userService, auctionsService,
        fwdauctionsService, $http, domain, $rootScope, fileReader, $filter, $log, reportingService, workflowService) {
        $scope.isCustomer = userService.getUserType();
        $scope.sessionid = userService.getUserToken();

        var self = this;

        $scope.userObj = {};

        $scope.compID = userService.getUserCompanyId();
        $scope.userId = userService.getUserId();
        $scope.pwdObj = {};

        $scope.isCustomer = userService.getUserType() == "CUSTOMER" ? true : false;

        if ($scope.compID == 0) {
            location.reload();
        }
        

        $scope.supplierType = ['Dealer', 'Importer', 'SSI', 'PSU', 'Govt. Agency', 'Others'];


        $scope.firstNameErr = $scope.lastNameErr = $scope.AddressErr = $scope.streetErr = $scope.countryErr = $scope.stateErr = $scope.pinCodeErr = $scope.cityErr = $scope.altMobileErr = "";
        $scope.bankAddressErr = $scope.accntNumberErr = $scope.ifscErr = $scope.cancelledChequeErr = $scope.taxnumberErr = $scope.taxnumberLengErr = "" ;
        $scope.pannumberErr = $scope.pannumberLengErr = $scope.panObjectErr = $scope.stnObjectErr = salesPersonErr = salesContactErr = "";
        

        $scope.companyConfigStr = '';
        $scope.companyConfigurationArr = ['BANK_CODES', 'STATE_CODE', 'COUNTRY', 'COUNTRY_CODE'];
        $scope.companyConfigStr = $scope.companyConfigurationArr.join(',').toString();
        $scope.companyConfigList = [];
        $scope.companyBankUnits = [];
        $scope.companyStateUnits = [];
        $scope.companyCountryUnits = [];
        $scope.companyCountryCodeUnits = [];

        if ($scope.companyConfigStr != null || $scope.companyConfigStr != '') {
            auctionsService.GetCompanyConfigurationForLogin(0, $scope.companyConfigStr, userService.getUserToken())
                .then(function (unitResponse) {
                    $scope.companyConfigList = unitResponse;
                    $scope.getUserDataNoCache();
                    $scope.companyConfigList.forEach(function (item, index) {
                        if (item.configKey == 'BANK_CODES') {
                            $scope.companyBankUnits.push(item);
                        } else if (item.configKey == 'STATE_CODE') {
                            $scope.companyStateUnits.push(item);
                        } else if (item.configKey == 'COUNTRY') {
                            $scope.companyCountryUnits.push(item);
                        } else if (item.configKey == 'COUNTRY_CODE') {
                            $scope.companyCountryCodeUnits.push(item);
                        }
                    })
                });
        }


        $scope.getUserDataNoCache = function () {
            userService.getUserDataNoCache()
                .then(function (response) {
                    $scope.userObj = response;

                    if ($scope.userObj.userType == 'VENDOR' && $scope.userObj.vendorDetails.additionalVendorInformation != "") {
                        $scope.userObj.vendorDetails.additionalVendorInformation = angular.fromJson($scope.userObj.vendorDetails.additionalVendorInformation);
                        $scope.userObj.vendorDetails.additionalVendorInformation.customerPhoneNumber = (+$scope.userObj.vendorDetails.additionalVendorInformation.customerPhoneNumber);
                    }

                    $scope.userObj.phoneNum = parseInt($scope.userObj.phoneNum);
                    $scope.userObj.vendorDetails.accntNumber = parseInt($scope.userObj.vendorDetails.accntNumber);
                    $scope.userObj.vendorDetails.altMobile = parseInt($scope.userObj.vendorDetails.altMobile);
                    $scope.userObj.vendorDetails.landLine = parseInt($scope.userObj.vendorDetails.landLine);
                    $scope.userObj.vendorDetails.PinCode = parseInt($scope.userObj.vendorDetails.PinCode);
                    $scope.userObj.vendorDetails.salesContact = parseInt($scope.userObj.vendorDetails.salesContact);
                    //if ($scope.userObj.vendorDetails.Country!=null) {
                    //    $scope.populateCCCode($scope.userObj.vendorDetails.Country);
                    //}
                    $scope.companyCountryUnits.filter(function (item) {
                        if (item.configValue == $scope.userObj.vendorDetails.Country) {
                            return $scope.userObj.vendorDetails.Country = item;
                        }
                    });
                    if ($scope.userObj.vendorDetails.Country != null) {
                        $scope.populateCCCode($scope.userObj.vendorDetails.Country);
                    }

                    if ($scope.userObj.vendorDetails.Country && $scope.userObj.vendorDetails.Country.configText == 'IN') {
                        $scope.populateState($scope.userObj.vendorDetails.Country);
                    }
                    
                });
        };

        this.editPro = 0;
        this.editInfo = 0;
        this.editBankInfo = 0;
        this.editBusinessInfo = 0;
        this.editContact = 0;
        this.editConflictOfInterestInfo = 0;
        this.editDocVerification = 0;
        this.editPwd=0
        $scope.isnegotiationrunning = false;
        //$scope.proinfo=0;
        $scope.contactinfo = 0;

        $scope.isPhoneModifies = 0;
        $scope.isEmailModifies = 0;

        $scope.showIsSuperUser = function () {
            if (userService.getUserObj().isSuperUser) {
                return true;
            } else {
                return false;
            }
        }

        $scope.addVendorShow = false;

        $scope.CredentialUpload = [];
        $scope.uploadedCredentials = [{ "credentialID": '', "fileType": 'PAN', 'fileLink': "", 'isVerified': 1 }, { "credentialID": '', "fileType": 'TIN', 'fileLink': "", 'isVerified': 1 }, { "credentialID": '', "fileType": 'STN', 'fileLink': "", 'isVerified': 1 }];

        userService.isnegotiationrunning()
            .then(function (response) {
                $scope.isnegotiationrunning = response.data.IsNegotationRunningResult;
            })


        $scope.sendUniqueCall = true;

        $scope.temporaryObj = {
            pan: '',
            tin: '',
            stn: ''
        }
            
        $scope.checkVendorUniqueResult = function (idtype, inputvalue) {

            if (idtype == "PHONE") {
                $scope.checkVendorPhoneUniqueResult = false;
            } else if (idtype == "EMAIL") {
                $scope.checkVendorEmailUniqueResult = false;
            }
            else if (idtype == "PAN") {
                $scope.checkPANUniqueResult = false;
            }
            else if (idtype == "TIN") {
                $scope.checkVendorTinUniqueResult = false;
            }
            else if (idtype == "STN") {
                $scope.checkSTNUniqueResult = false;
            }


            if (idtype == "PAN") {
                if ($scope.temporaryObj.pan != inputvalue) {
                    $scope.sendUniqueCall = true;
                } else {
                    $scope.sendUniqueCall = false;
                }
            }

            if (idtype == "TIN") {
                if ($scope.temporaryObj.tin != inputvalue) {
                    $scope.sendUniqueCall = true;
                } else {
                    $scope.sendUniqueCall = false;
                }
            }

            if (idtype == "STN") {
                if ($scope.temporaryObj.stn != inputvalue) {
                    $scope.sendUniqueCall = true;
                } else {
                    $scope.sendUniqueCall = false;
                }
            }
            
            if (inputvalue == "" || inputvalue == undefined) {
                return false;
            }



            if ($scope.sendUniqueCall) {
                userService.checkUserUniqueResult(inputvalue, idtype).then(function (response) {
                    if (idtype == "PHONE") {
                        $scope.checkVendorPhoneUniqueResult = !response;
                    } else if (idtype == "EMAIL") {
                        $scope.checkVendorEmailUniqueResult = !response;
                    }
                    else if (idtype == "PAN") {
                        $scope.checkPANUniqueResult = !response;
                    }
                    else if (idtype == "TIN") {
                        $scope.checkTINUniqueResult = !response;
                    }
                    else if (idtype == "STN") {
                        $scope.checkSTNUniqueResult = !response;
                    }
                    else if (idtype == "USER_NAME") {
                        $scope.checkUserNameUniqueResult = !response;
                    }
                });
            }


        };

        $scope.callGetUserDetails = function () {
            userService.getProfileDetails({ "userid": userService.getUserId(), "sessionid": userService.getUserToken() })
                .then(function (response) {
                    $scope.userStatus = "registered";
                    if (response != undefined) {
                        $scope.userDetails = response;
                    }

                });
        }


        this.updateUserInfo = function (contact) {

            $scope.firstNameErr = $scope.lastNameErr = $scope.AddressErr = $scope.streetErr = $scope.countryErr = $scope.stateErr = $scope.pinCodeErr = $scope.cityErr = $scope.altMobileErr = "";
            $scope.bankAddressErr = $scope.accntNumberErr = $scope.ifscErr = $scope.cancelledChequeErr = $scope.taxnumberErr = $scope.taxnumberLengErr = "";
            $scope.pannumberErr = $scope.pannumberLengErr = $scope.panObjectErr = $scope.stnObjectErr = $scope.salesPersonErr = $scope.salesContactErr = $scope.bankNameErr = "";
            $scope.primaryContactNameErr = $scope.primaryContactDesignationErr = $scope.primaryContactMobileErr = $scope.primaryContactLandLineErr = $scope.primaryContactEmailIdErr = "";
            $scope.businessTypeErr = $scope.ownerDirectorNameErr = $scope.ownerDirectorAadharErr = $scope.payeeNameErr = $scope.bankAccountTypeErr = $scope.businessIncoDateTypeErr = "";
            $scope.supplierErr = $scope.materialOrServiceNameErr = $scope.materialOrServiceErr = $scope.importFromErr = $scope.customerNameAndLocationErr = $scope.customerNatureOfBusinessErr =
            $scope.customerContactPersonNameErr = $scope.nameContactDetailsErr = $scope.clientDealtErr = "";


            if ($scope.oldPhoneNum != $scope.userObj.phoneNum) {
                $scope.isPhoneModifies = 1;
            }

            var params = {};
            //$scope.userObj.vendorDetails.additionalVendorInformation = JSON.stringify($scope.userObj.vendorDetails.additionalVendorInformation);
            if (!$scope.userObj.institution) {
                growlService.growl("Company Name cannot be empty.", "inverse");
                
                return false;

            }

            if (!$scope.userObj.firstName) {
                growlService.growl("First Name cannot be empty.", "inverse");
                $scope.firstNameErr = "First Name cannot be empty.";
                return false;
            }
            if (!$scope.userObj.lastName) {
                growlService.growl("Last Name cannot be empty.", "inverse");
                $scope.lastNameErr = "Last Name cannot be empty.";
                return false;
            }

            if (this.editInfo == 1 && ($scope.userObj.isSuperUser != false && $scope.userObj.userType != "CUSTOMER")) {
                if (!$scope.userObj.vendorDetails.Address1) {
                    growlService.growl("Address cannot be empty.", "inverse");
                    $scope.AddressErr = "Address cannot be empty.";
                    return false;
                }
                if (!$scope.userObj.vendorDetails.Street1) {
                    growlService.growl("Street1 cannot be empty.", "inverse");
                    $scope.streetErr = "Street1 cannot be empty.";
                    return false;
                }

                if (!$scope.userObj.vendorDetails.Country.configValue) {
                    growlService.growl("Country cannot be empty.", "inverse");
                    $scope.countryErr = "Country cannot be empty.";
                    return false;
                }


                if (!$scope.displayStateVal) {
                    if (!$scope.userObj.vendorDetails.State.configValue) {
                        growlService.growl("State cannot be empty.", "inverse");
                        $scope.stateErr = "State cannot be empty.";
                        return false;
                    }

                } else {
                    if (!$scope.userObj.vendorDetails.State) {
                        growlService.growl("State cannot be empty.", "inverse");
                        $scope.stateErr = "State cannot be empty.";
                        return false;
                    }
                }
                
                if (!$scope.userObj.vendorDetails.PinCode) {
                    growlService.growl("PinCode cannot be empty.", "inverse");
                    $scope.pinCodeErr = "PinCode cannot be empty.";
                    return false;
                }
                if (!$scope.userObj.vendorDetails.City) {
                    growlService.growl("City cannot be empty.", "inverse");
                    $scope.cityErr = "City cannot be empty.";
                    return false;
                }


                $scope.userObj.PERSONAL_INFO_VALID = 1;
                
            }

            if (this.editContact == 1) {
                if (!$scope.userObj.phoneNum) {
                    growlService.growl("Phone Number cannot be empty.", "inverse");
                    return false;
                }
                if (isNaN($scope.userObj.phoneNum)) {
                    growlService.growl("Please Enter correct Mobile number.", "inverse");
                    return false;
                }
                if ($scope.userObj.phoneNum.toString().length != 10) {
                    growlService.growl("Phone Number Must be 10 digits.", "inverse");
                    return false;
                }
                if (!$scope.userObj.email) {
                    growlService.growl("Email cannot be empty.", "inverse");
                    return false;
                }

                if ($scope.userObj.vendorDetails.altMobile) {
                    if ($scope.userObj.vendorDetails.altMobile.toString().length != 10) {
                        growlService.growl("Alt Phone Number Must be 10 digits.", "inverse");
                        $scope.altMobileErr = "Alt Phone Number Must be 10 digits";
                        return false;
                    }
                }

                if (!$scope.userObj.vendorDetails.additionalVendorInformation.primaryContactName) {
                    growlService.growl("Primary Contact Name cannot be empty.", "inverse");
                    $scope.primaryContactNameErr = "Primary Contact Person Name Cannot be Empty.";
                    return false;
                }

                if (!$scope.userObj.vendorDetails.additionalVendorInformation.primaryContactDesignation) {
                    growlService.growl("Primary Contact Designation cannot be empty.", "inverse");
                    $scope.primaryContactDesignationErr = "Primary Contact Designation Cannot be Empty.";
                    return false;
                }

                if (!$scope.userObj.vendorDetails.additionalVendorInformation.primaryContactMobile) {
                    growlService.growl("Primary contact Mobile number cannot be empty.", "inverse");
                    $scope.primaryContactMobileErr = "Primary contact Mobile number Cannot be Empty.";
                    return false;
                }
                if (!$scope.userObj.vendorDetails.additionalVendorInformation.primaryContactLandLine) {
                    growlService.growl("Primary LandLine cannot be empty.", "inverse");
                    $scope.primaryContactLandLineErr = "Primary Landline Cannot be Empty.";
                    return false;
                }
                if (!$scope.userObj.vendorDetails.additionalVendorInformation.primaryContactEmailId) {
                    growlService.growl("Primary contact Email cannot be empty.", "inverse");
                    $scope.primaryContactEmailIdErr = "Primary contact Email Cannot be Empty.";
                    return false;
                }

                $scope.userObj.CONTACT_INFO_VALID = 1;
                
            }

            if (this.editBankInfo == 1) {

                if (!$scope.userObj.vendorDetails.BankName) {
                    growlService.growl("Bank Name cannot be empty.", "inverse");
                    $scope.bankNameErr = "Bank Name cannot be empty.";
                    return false;
                }

                if (!$scope.userObj.vendorDetails.BankAddress) {
                    growlService.growl("Bank Address cannot be empty.", "inverse");
                    $scope.bankAddressErr = "Bank Address cannot be empty.";
                    return false;
                }
                if (isNaN($scope.userObj.vendorDetails.accntNumber) || $scope.userObj.vendorDetails.accntNumber == null || $scope.userObj.vendorDetails.accntNumber =="") {
                    growlService.growl("Bank Account Number cannot be empty.", "inverse");
                    $scope.accntNumberErr = "Bank Account Number cannot be empty.";
                    return false;
                }
                if (!$scope.userObj.vendorDetails.Ifsc) {
                    growlService.growl("Ifsc Details cannot be empty.", "inverse");
                    $scope.ifscErr = "Ifsc Details cannot be empty.";
                    return false;
                }

                if (!$scope.checkObject.fileStream && !$scope.userObj.vendorDetails.cancelledCheque) {
                    growlService.growl("Cancelled Cheque cannot be empty.", "inverse");
                    $scope.cancelledChequeErr = "Cancelled Cheque cannot be empty.";
                    return false;
                }

                if (!$scope.userObj.vendorDetails.additionalVendorInformation.payeeName) {
                    growlService.growl("Payee Name cannot be empty.", "inverse");
                    $scope.payeeNameErr = "Payee Name cannot be Empty.";
                    return false;
                }

                if (!$scope.userObj.vendorDetails.additionalVendorInformation.bankAccountType) {
                    growlService.growl("Bank Account Type cannot be empty.", "inverse");
                    $scope.bankAccountTypeErr = "Bank Account Type Cannot be Empty.";
                    return false;
                }

                $scope.userObj.BANK_INFO_VALID = 1;

                //
            }

            if (this.editBusinessInfo == 1) {


                if (!taxnumber.value) {
                    growlService.growl("GST cannot be empty.", "inverse");
                    $scope.taxnumberErr = "GST cannot be empty.";
                    return false;
                }
                if (taxnumber.value.length < 15 || taxnumber.value.length > 15) {
                    growlService.growl("GST Number must be 15 characters.", "inverse");
                    $scope.taxnumberLengErr = "GST Number must be 15 characters.";
                    return false;
                }

                if (!pannumber.value) {
                    growlService.growl("PAN cannot be empty.", "inverse");
                    $scope.pannumberErr = "PAN cannot be empty.";
                    return false;
                }

                if (pannumber.value.length < 10 || pannumber.value.length > 10) {
                    growlService.growl("PAN Number must be 10 characters.", "inverse");
                    $scope.pannumberLengErr = "PAN Number must be 10 characters.";
                    return false;
                }

                if (!$scope.stnObject.fileStream && !$scope.userObj.vendorDetails.gstAttach) {
                    growlService.growl("Please Upload GST Document.", "inverse");
                    $scope.stnObjectErr = "Please Upload GST Document.";
                    return false;
                }

                if (!$scope.panObject.fileStream && !$scope.userObj.vendorDetails.panAttach) {
                    growlService.growl("Please Upload Pan Document.", "inverse");
                    $scope.panObjectErr = "Please Upload Pan Document.";
                    return false;
                }
                
                if ($scope.checkSTNUniqueResult) {
                    return false;
                }

                if ($scope.checkPANUniqueResult) {
                    return false;
                }

                self.updateVerficationInfo(pannumber.value, "", taxnumber.value);


                if (!$scope.userObj.vendorDetails.additionalVendorInformation.busineesType)
                {
                    growlService.growl("Business Type Cannot be Empty", "inverse");
                    $scope.businessTypeErr = "Business Type Cannot be Empty";
                    return false;
                }

                if (!$scope.userObj.vendorDetails.additionalVendorInformation.businessIncoDate) {
                    growlService.growl("Business Incorporation Date Cannot be Empty", "inverse");
                    $scope.businessIncoDateTypeErr = "Business Incorporation Date Cannot be Empty";
                    return false;
                }

                if (!$scope.userObj.vendorDetails.additionalVendorInformation.ownerDirectorName) {
                    growlService.growl("Owner / Director name Cannot be Empty", "inverse");
                    $scope.ownerDirectorNameErr= "Owner / Director name Cannot be Empty";
                    return false;
                }

                if (!$scope.userObj.vendorDetails.additionalVendorInformation.ownerDirectorAadharCard) {
                    growlService.growl("Owner / Director Aadhar Card Cannot be Empty", "inverse");
                    $scope.ownerDirectorAadharCardErr = "Owner / Director Aadhar CardCannot be Empty";
                    return false;
                }


                $scope.userObj.BUSINESS_INFO_VALID = 1;
                
            }



            if (this.editConflictOfInterestInfo == 1) {
                
                if (!$scope.userObj.vendorDetails.additionalVendorInformation.nameContactDetails) {
                    growlService.growl("Please fill out this field", "inverse");
                    $scope.nameContactDetailsErr = "Please fill out this field Cannot be Empty";
                    return false;
                }

                if (!$scope.userObj.vendorDetails.additionalVendorInformation.clientDealt) {
                    growlService.growl("Have you ever dealt with Century Ply Before Cannot be Empty", "inverse");
                    $scope.clientDealtErr = "Have you ever dealt with Century Ply Before Cannot be Empty";
                    return false;
                }
               
                if (!$scope.userObj.vendorDetails.additionalVendorInformation.supplier) {
                    growlService.growl("Supplier Type Cannot be Empty", "inverse");
                    $scope.supplierErr = "Supplier Type Cannot be Empty";
                    return false;
                }

                if (!$scope.userObj.vendorDetails.additionalVendorInformation.materialOrServiceName) {
                    growlService.growl("Material/Service Name Cannot be Empty", "inverse");
                    $scope.materialOrServiceNameErr = "Material/Service Name Cannot be Empty";
                    return false;
                }

                if (!$scope.userObj.vendorDetails.additionalVendorInformation.materialOrService) {
                    growlService.growl("Category of Material/Service Cannot be Empty", "inverse");
                    $scope.materialOrServiceErr = "Category of Material/Service Name Cannot be Empty";
                    return false;
                }


                if ((!$scope.userObj.vendorDetails.additionalVendorInformation.importFrom && $scope.userObj.vendorDetails.additionalVendorInformation.supplier == 'Importer')) {
                    growlService.growl("Import from Cannot be Empty", "inverse");
                    $scope.importFromErr = "Import from Cannot be Empty";
                    return false;
                }


                $scope.userObj.CONFLICT_INFO_VALID = 1;
                
            }
            
            params.institution = $scope.userObj.institution;
            params.firstName = $scope.userObj.firstName;
            params.lastName = $scope.userObj.lastName;
            params.phoneNum = $scope.userObj.phoneNum;

            params.isOTPVerified = $scope.isOTPVerified;
            params.credentialsVerified = $scope.credentialsVerified;
            if (params.phoneNum != $scope.userObj.phoneNum) {
                params.isOTPVerified = 0;
            }
            params.email = $scope.userObj.email;
            params.isEmailOTPVerified = $scope.isEmailOTPVerified;
            if (params.email != $scope.userObj.email) {
                params.isEmailOTPVerified = 0;
            }
            params.sessionID = userService.getUserToken();
            params.userID = userService.getUserId();
            params.errorMessage = "";

            params.altPhoneNum = $scope.userObj.altPhoneNum;
            params.altEmail = $scope.userObj.altEmail;
            params.address1 = $scope.userObj.vendorDetails.Address1;

            params.address = $scope.userObj.vendorDetails.Address1;
            params.Street1 = $scope.userObj.vendorDetails.Street1;
            params.Street2 = $scope.userObj.vendorDetails.Street2;
            params.Street3 = $scope.userObj.vendorDetails.Street3;
            params.Country = $scope.userObj.vendorDetails.Country.configValue;
            if ($scope.displayStateVal) {
                params.State = $scope.userObj.vendorDetails.State;
            } else {
                params.State = $scope.userObj.vendorDetails.State.configValue;
            }
            params.City = $scope.userObj.vendorDetails.City;
            params.PinCode = $scope.userObj.vendorDetails.PinCode;
            params.altMobile = $scope.userObj.vendorDetails.altMobile;
            params.landLine = $scope.userObj.vendorDetails.landLine;
            params.salesPerson = $scope.userObj.vendorDetails.salesPerson;
            params.salesContact = $scope.userObj.vendorDetails.salesContact;
            params.BankName = $scope.userObj.vendorDetails.BankName;
            params.BankAddress = $scope.userObj.vendorDetails.BankAddress;
            params.accntNumber = $scope.userObj.vendorDetails.accntNumber;
            params.Ifsc = $scope.userObj.vendorDetails.Ifsc;
            params.cancelledCheque = $scope.userObj.vendorDetails.cancelledCheque;
            params.BussinessDet = $scope.userObj.vendorDetails.BussinessDet;


            params.gstAttachments = $scope.stnObject.fileStream;
            params.gstAttachmentName = $scope.stnObject.name;
            params.panAttachments = $scope.panObject.fileStream;
            params.panAttachmentsName = $scope.panObject.name;
            params.businessAttachments = $scope.businessObject.fileStream;
            params.businessAttachmentName = $scope.businessObject.name;
            params.cancelledchequeattachments = $scope.checkObject.fileStream;
            params.cancelledchequeattachmentName = $scope.checkObject.name;
            params.msmeAttachments = $scope.msmeObject.fileStream;
            params.msmeAttachmentName = $scope.msmeObject.name;
            params.additionalVendorInformation = JSON.stringify($scope.userObj.vendorDetails.additionalVendorInformation);
            params.copyLetterAttachments = $scope.copyLetterObject.fileStream;
            params.copyLetterAttachmentName = $scope.copyLetterObject.name;

            params.PERSONAL_INFO_VALID = $scope.userObj.PERSONAL_INFO_VALID;
            params.CONTACT_INFO_VALID = $scope.userObj.CONTACT_INFO_VALID;
            params.BANK_INFO_VALID = $scope.userObj.BANK_INFO_VALID;
            params.BUSINESS_INFO_VALID = $scope.userObj.BUSINESS_INFO_VALID;
            params.CONFLICT_INFO_VALID = $scope.userObj.CONFLICT_INFO_VALID;



            /*Attachments while update */
            if (params.gstAttachments == null || params.gstAttachments == undefined || params.gstAttachments == "") {
                params.gstAttach = $scope.userObj.vendorDetails.gstAttach;
            } else {
                params.gstAttach = "";
            }
            if (params.businessAttachments == null || params.businessAttachments == undefined || params.businessAttachments == "") {
                params.businessAttach = $scope.userObj.vendorDetails.businessAttach;
            } else {
                params.businessAttach = "";
            }
            if (params.panAttachments == null || params.panAttachments == undefined || params.panAttachments == "") {
                params.panAttach = $scope.userObj.vendorDetails.panAttach;
            } else {
                params.panAttach = "";
            }
            if (params.cancelledchequeattachments == null || params.cancelledchequeattachments == undefined || params.cancelledchequeattachments == "") {
                params.cancelledCheque = $scope.userObj.vendorDetails.cancelledCheque;
            } else {
                params.cancelledCheque = "";
            }
            if (params.msmeAttachments == null || params.msmeAttachments == undefined || params.msmeAttachments == "") {
                params.msmeAttach = $scope.userObj.vendorDetails.msmeAttach;
            } else {
                params.msmeAttach = "";
            }
            if (params.copyLetterAttachments == null || params.copyLetterAttachments == undefined || params.copyLetterAttachments == "") {
                params.customerCopyLetterAttach = $scope.userObj.vendorDetails.copyLetterAttachments;
            } else {
                params.customerCopyLetterAttach = "";
            }
            /*Attachments while update */


            //$scope.panObject = $scope.file.name;

            userService.updateUser(params)
                .then(function (response) {
                    if (response.toLowerCase().indexOf('already exists') > 0) {
                        userService.getUserDataNoCache().then(function (response) {

                            var loginUserData = userService.getUserObj();
                            $scope.userObj.institution = loginUserData.institution;
                            $scope.userObj.fullName = loginUserData.firstName + " " + loginUserData.lastName;
                            $scope.userObj.firstName = loginUserData.firstName;
                            $scope.userObj.lastName = loginUserData.lastName;
                            $scope.userObj.altPhoneNum = loginUserData.altPhoneNum;
                            $scope.userObj.altEmail = loginUserData.altEmail;
                            $scope.userObj.gender = "male";
                            $scope.userObj.birthDay = "23/06/1988";
                            $scope.userObj.martialStatus = "Single";
                            $scope.userObj.phoneNum = loginUserData.phoneNum;
                            $scope.userObj.email = loginUserData.email;
                            $scope.userObj.username = loginUserData.username;
                            $scope.oldPhoneNum = loginUserData.phoneNum;
                            $scope.oldemail = loginUserData.email;
                            $scope.userObj = loginUserData;
                            $scope.callGetUserDetails();
                            $scope.updateUserDataFromService();
                            $state.go('pages.profile.profile-about');
                            $state.reload();
                        });
                    }

                    $state.go('pages.profile.profile-about');
                    $state.reload();
                });

            this.editSummary = 0;

            this.editInfo = 0;
            this.editBankInfo = 0;
            this.editBusinessInfo = 0;
            this.editPic = 0;
            this.editConflictOfInterestInfo = 0;
            this.editContact = 0;
            this.editDocVerification = 0;
            this.editPro = 0;
            this.imagefilesonlyforlogo = false;
            this.addUser = 0;
            $scope.callGetUserDetails();
        };





        ///////////////////
        // FILE UPLOAD
        //////////////////

        $scope.panObject = {};
        $scope.tinObject = {};
        $scope.stnObject = {};

        $scope.checkObject = {};
        $scope.businessObject = {};
        $scope.msmeObject = {};
        $scope.copyLetterObject = {};

        $scope.ImportEntity = {};

        $scope.getFile1 = function (id, doctype, ext) {
            $scope.progress = 0;
            $scope.file = $("#" + id)[0].files[0];
            $scope.docType = doctype + "." + ext;
            fileReader.readAsDataUrl($scope.file, $scope)
                .then(function (result) {

                    var bytearray = new Uint8Array(result);
                    var fileobj = {};
                    fileobj.fileStream = $.makeArray(bytearray);
                    fileobj.fileType = $scope.docType;
                    fileobj.isVerified = 1;
                    if (doctype == "PAN") {
                        fileobj.credentialID = $scope.pannumber;
                        $scope.panObject = fileobj;
                        $scope.panObject.name = $scope.file.name;
                    }
                    else if (doctype == "TIN") {
                        fileobj.credentialID = $scope.vatnumber;
                        $scope.tinObject.name = fileobj;
                    }
                    else if (doctype == "STN") {
                        fileobj.credentialID = $scope.taxnumber;
                        $scope.stnObject = fileobj;
                        $scope.stnObject.name = $scope.file.name;
                    }
                    else if (doctype == "CHEQUE") {
                        $scope.checkObject = fileobj;
                        $scope.checkObject.name = $scope.file.name;
                    } 
                    else if (doctype == "BUSINESSDET") {
                        $scope.businessObject = fileobj;
                        $scope.businessObject.name = $scope.file.name;
                    }
                    else if (doctype == "MSME") {
                        $scope.msmeObject = fileobj;
                        $scope.msmeObject.name = $scope.file.name;
                    }
                    else if (doctype == "COPYLETTER") {
                        $scope.copyLetterObject = fileobj;
                        $scope.copyLetterObject.name = $scope.file.name;
                    }
                    //$scope.copyLetterObject

                });
        };

        ///////////////////
        // FILE UPLOAD
        //////////////////






        ///////////////////
        // ALTERNATE PHONE && EMAIL
        //////////////////

        $scope.addPhone = function () {
            $scope.phonesLength = $scope.multiplePhones.length;
            $scope.singlePhone =
                {
                    contactNo: '',
                    phoneSNo: $scope.phoneSNo++
                }
            $scope.multiplePhones.push($scope.singlePhone);
        };

        $scope.deletePhone = function (phoneSNo) {
            $scope.phonesLength = $scope.multiplePhones.length;
            //if ($scope.phonesLength > 1) {
            $scope.multiplePhones = _.filter($scope.multiplePhones, function (x) { return x.phoneSNo !== phoneSNo; });
            //}
        };

        $scope.phoneSNo = 1;
        $scope.multiplePhones = [];
        $scope.singlePhone =
            {
                contactNo: '',
                phoneSNo: $scope.phoneSNo++
            }
        $scope.phones = [];
        $scope.editPhones = function (altPhoneNum) {
            $scope.phones = [];
            $scope.multiplePhones = [];
            $scope.phones = altPhoneNum.split(",");

            if ($scope.phones.length > 0) {
                $scope.phones.forEach(function (item, index) {
                    $scope.singlePhone =
                        {
                            contactNo: item,
                            phoneSNo: $scope.phoneSNo++
                        }
                    $scope.multiplePhones.push($scope.singlePhone);
                });
            }
        }





        $scope.SaveMultiPhones = function () {

            $scope.phoneError = false;

            $scope.multiplePhones.forEach(function (item, index) {
                item.error = "";
                if (item.contactNo.toString() == "") {
                    item.error = "Phone Number cannot be empty.";
                    $scope.phoneError = true;
                }
                else if (isNaN(item.contactNo)) {
                    item.error = "Please Enter correct Mobile number.";
                    $scope.phoneError = true;
                }
                else if (item.contactNo.toString().length != 10) {
                    item.error = "Phone Number Must be 10 digits.";
                    $scope.phoneError = true;
                }
            });

            if (!$scope.phoneError) {
                var phone = "";
                if ($scope.multiplePhones && $scope.multiplePhones.length > 0) {
                    $scope.multiplePhones.forEach(function (item, index) {
                        if (item.contactNo == undefined) {
                            item.contactNo = '';
                        };
                        phone += item.contactNo + ',';
                    });
                    phone = phone.slice(0, -1);
                };
                $scope.userObj.altPhoneNum = phone;
                $(function () {
                    $('#multiplePhones').modal('toggle');
                });
                self.updateUserInfo();

            }
        };



        $scope.addEmail = function () {
            $scope.emailsLength = $scope.multipleEmails.length;
            $scope.singleEmail =
                {
                    email: '',
                    emailSNo: $scope.emailSNo++
                }
            $scope.multipleEmails.push($scope.singleEmail);
        };

        $scope.deleteEmail = function (emailSNo) {
            $scope.emailsLength = $scope.multipleEmails.length;
            //if ($scope.emailsLength > 1) {
            $scope.multipleEmails = _.filter($scope.multipleEmails, function (x) { return x.emailSNo !== emailSNo; });
            //}
        };

        $scope.emailSNo = 1;
        $scope.multipleEmails = [];
        $scope.singleEmail =
            {
                email: '',
                emailSNo: $scope.emailSNo++
            }
        $scope.emails = [];

        $scope.editEmails = function (altEmail) {
            $scope.emails = [];
            $scope.multipleEmails = [];
            $scope.emails = altEmail.split(",");

            if ($scope.emails.length > 0) {
                $scope.emails.forEach(function (item, index) {
                    $scope.singleEmail =
                        {
                            email: item,
                            emailSNo: $scope.emailSNo++
                        }
                    $scope.multipleEmails.push($scope.singleEmail);
                });
            }
        }

        $scope.SaveMultiEmails = function () {

            $scope.emailError = false;

            $scope.multipleEmails.forEach(function (item, index) {
                item.error = "";
                if (item.email.toString() == "") {
                    item.error = "Email cannot be empty.";
                    $scope.emailError = true;
                }
            });

            if (!$scope.emailError) {

                var email = "";
                if ($scope.multipleEmails && $scope.multipleEmails.length > 0) {
                    $scope.multipleEmails.forEach(function (item, index) {
                        if (item.email == undefined) {
                            item.email = '';
                        };
                        email += item.email + ',';
                    });
                    email = email.slice(0, -1);
                };
                $scope.userObj.altEmail = email;
                $(function () {
                    $('#multipleEmails').modal('toggle');
                });
                self.updateUserInfo();
            }
        };

        ///////////////////
        // ALTERNATE PHONE && EMAIL
        //////////////////








        ///////////////////
        // VERIFY OTP
        //////////////////

        $scope.otpModalInstances = function () {
            return $uibModal.open({
                animation: true,
                templateUrl: 'verifyOTPModal.html',
                controller: 'modalInstanceCtrlOTP',
                size: 'sm',
                backdrop: 'static',
                keyboard: false
            });
        }

        $scope.emailModalInstances = function () {
            return $uibModal.open({
                animation: true,
                templateUrl: 'verifyEmailOTPModal.html',
                controller: 'modalInstanceCtrlOTP',
                size: 'sm',
                backdrop: 'static',
                keyboard: false
            });
        }

        ///////////////////
        // VERIFY OTP
        //////////////////



        ///////////////////
        // CREDENTIALS
        //////////////////

        $scope.taxnumberalpha = false;
        $scope.taxnumberlengthvalidation = false;
        $scope.taxnumberrequired = false;
        $scope.pannumberalpha = false;
        $scope.pannumberlengthvalidation = false;
        $scope.pannumberrequired = false;
        $scope.tinnumberrequired = false;
        $scope.pannumber = "";
        $scope.vatnumber = "";
        $scope.taxnumber = "";
        $scope.panregx = new RegExp("/^([a-zA-Z]{5})(\d{4})([a-zA-Z]{1})$/");
        $scope.panValidations = function (pannumber) {
            if (pannumber != "" && !$scope.panregx.test(pannumber)) {
                $scope.pannumberalpha = false;
                $scope.pannumberlengthvalidation = false;
                $scope.pannumberrequired = true;
            } else if (('' + pannumber).length < 10) {
                $scope.pannumberalpha = false;
                $scope.pannumberlengthvalidation = true;
                $scope.pannumberrequired = false;
            } else if (!(/^[a-zA-Z0-9]*$/.test(pannumber))) {
                $scope.pannumberalpha = true;
                $scope.pannumberlengthvalidation = false;
                $scope.pannumberrequired = false;
            } else {
                $scope.pannumberalpha = false;
                $scope.pannumberlengthvalidation = false;
                $scope.pannumberrequired = false;
            }
        }
        $scope.tinValidations = function (vatnumber) {
            if (vatnumber == "") {
                $scope.tinnumberrequired = true;
            } else {
                $scope.tinnumberrequired = false;
            }
        }
        $scope.taxValidations = function (taxnumber) {
            if (taxnumber == "") {
                $scope.taxnumberalpha = false;
                $scope.taxnumberlengthvalidation = false;
                $scope.taxnumberrequired = true;
            } else if (("" + taxnumber).length < 15) {
                $scope.taxnumberalpha = false;
                $scope.taxnumberlengthvalidation = true;
                $scope.taxnumberrequired = false;
            } else if (!(/^[a-zA-Z0-9]*$/.test(taxnumber))) {
                $scope.taxnumberalpha = true;
                $scope.taxnumberlengthvalidation = false;
                $scope.taxnumberrequired = false;
            } else {
                $scope.taxnumberalpha = false;
                $scope.taxnumberlengthvalidation = false;
                $scope.taxnumberrequired = false;
            }
        }


        $scope.getUserCredentials = function () {
            $http({
                method: 'GET',
                url: domain + 'getusercredentials?sessionid=' + userService.getUserToken() + "&userid=" + userService.getUserId(),
                encodeURI: true,
                headers: { 'Content-Type': 'application/json' }
            }).then(function (response) {

                $scope.CredentialsResponce = response.data;


                if (response && response.data && response.data.length > 0) {
                    if (response.data[0].errorMessage == "") {
                        //var panObj = ;

                        $scope.pannumber = _.filter($scope.CredentialsResponce, ['fileType', 'PAN'])[0].credentialID;
                        $scope.vatnumber = _.filter($scope.CredentialsResponce, ['fileType', 'TIN'])[0].credentialID;
                        $scope.taxnumber = _.filter($scope.CredentialsResponce, ['fileType', 'STN'])[0].credentialID;

                        $scope.temporaryObj.pan = $scope.pannumber;
                        $scope.temporaryObj.tin = $scope.vatnumber;
                        $scope.temporaryObj.stn = $scope.taxnumber;

                        //console.log("$scope.temporaryObj.pan>>>" + $scope.temporaryObj.pan + "$scope.temporaryObj.tin>>>>" + $scope.temporaryObj.tin + "$scope.temporaryObj.stn>>>" + $scope.temporaryObj.stn);

                        var verifiedDocsCount = 0;
                        $.each(response.data, function (key, value) {
                            $scope.PAN = value.credentialID;
                            if (value.isVerified == 1) {
                                verifiedDocsCount++;
                            }
                        });
                        if (response.data.length == verifiedDocsCount + 1 || response.data.length == verifiedDocsCount) {
                            userService.updateVerified(1);
                        } else {
                            userService.updateVerified(0);
                        }
                        $scope.uploadedCredentials = response.data;
                        if (response.data.length > 0) {
                            this.editDocVerification = 0;
                            $scope.editDocVerification = 0;
                        }
                    }
                }
            }, function (result) {
            });
        };

        $scope.getUserCredentials();



        this.updateVerficationInfo = function (pannumber, vatnumber, taxnumber) {

            var panObj = _.filter($scope.CredentialsResponce, ['fileType', 'PAN'])[0];
            var tinObj = _.filter($scope.CredentialsResponce, ['fileType', 'TIN'])[0];
            var stnObj = _.filter($scope.CredentialsResponce, ['fileType', 'STN'])[0];

            //if ((!$scope.panObject.fileType || pannumber === "" || !(/^[A-Z]{5}\d{4}[A-Z]{1}$/.test(pannumber.toUpperCase()))) && panObj.credentialID != pannumber) {
            //    $scope.pannumberalpha = false;
            //    $scope.pannumberlengthvalidation = false;
            //    $scope.pannumberrequired = false;

            //    if (pannumber === "") {
            //        $scope.pannumberrequired = true;
            //    }
            //    else {
            //        if (!(/^[A-Z]{5}\d{4}[A-Z]{1}$/.test(pannumber.toUpperCase()))) {
            //            $scope.pannumberlengthvalidation = true;
            //        }
            //    }

            //    return false;
            //}
            //else {
            //    $scope.pannumberalpha = false;
            //    $scope.pannumberlengthvalidation = false;
            //    $scope.pannumberrequired = false;
            //}






            //if ((!$scope.tinObject.fileType || vatnumber === "" || !(/^\d{11}$/.test(vatnumber))) && tinObj.credentialID != vatnumber) {
            //    $scope.tinnumberrequired = false;
            //    $scope.tinnumberlengthvalidation = false;
            //    if (vatnumber === "") {
            //        $scope.tinnumberrequired = true;
            //    }
            //    else {
            //        if (!(/^\d{11}$/.test(vatnumber.toUpperCase()))) {
            //            $scope.tinnumberlengthvalidation = true;
            //        }
            //    }

            //    return false;
            //}
            //else {
            //    $scope.tinnumberrequired = false;
            //    $scope.tinnumberlengthvalidation = false;
            //}
            //if ((!true || taxnumber === "" || !(/^[a-zA-Z0-9]{15}$/.test(taxnumber))) && stnObj.credentialID != taxnumber) {
            //    $scope.taxnumberalpha = false;
            //    $scope.taxnumberlengthvalidation = false;
            //    $scope.taxnumberrequired = false;
            //    if (taxnumber === "") {
            //        $scope.taxnumberrequired = true;
            //    }
            //    else {
            //        if (!(/^[a-zA-Z0-9]{15}$/.test(taxnumber.toUpperCase()))) {
            //            $scope.taxnumberlengthvalidation = true;
            //        }
            //    }


            //    return false;
            //}
            //else {
            //    $scope.taxnumberalpha = false;
            //    $scope.taxnumberlengthvalidation = false;
            //    $scope.taxnumberrequired = false;
            //}


            if (pannumber !== "" && (panObj.credentialID != pannumber || true)) {
                $scope.panObject.credentialID = pannumber;
                if ($scope.panObject.fileStream) {
                    $scope.CredentialUpload.push($scope.panObject);
                } else {
                    $scope.panObject.fileStream = null;
                    $scope.panObject.fileType = 'PAN.png';
                    $scope.panObject.isVerified = 1;
                    $scope.CredentialUpload.push($scope.panObject);
                }
            } else {
                $scope.panObject.fileStream = null;
                $scope.panObject.fileType = 'PAN.png';
                $scope.panObject.isVerified = 1;
                $scope.CredentialUpload.push($scope.panObject);
            }

            if (vatnumber !== "" && (tinObj.credentialID != vatnumber || $scope.tinObject.fileStream)) {
                $scope.tinObject.credentialID = vatnumber;
                if ($scope.tinObject.fileStream) {
                    $scope.CredentialUpload.push($scope.tinObject);
                }
            }
            if (taxnumber !== "" && (stnObj.credentialID != taxnumber || true)) {
                $scope.stnObject.credentialID = taxnumber;
                if ($scope.stnObject.fileStream) {
                    $scope.CredentialUpload.push($scope.stnObject);
                }
                else {
                    $scope.stnObject.fileStream = null;
                    $scope.stnObject.fileType = 'STN.png';
                    $scope.stnObject.isVerified = 1;
                    $scope.CredentialUpload.push($scope.stnObject);
                }
            } else {
                $scope.stnObject.fileStream = null;
                $scope.stnObject.fileType = 'STN.png';
                $scope.stnObject.isVerified = 1;
                $scope.CredentialUpload.push($scope.stnObject);
            }

            if ($scope.CredentialUpload.length == 0 && false) {
                growlService.growl("Please enter at least one credential for upload", "inverse");
                return false;
            }
            var params = { 'userID': userService.getUserId(), 'files': $scope.CredentialUpload, 'sessionID': userService.getUserToken() };
            $http({
                method: 'POST',
                url: domain + 'updatecredentials',
                encodeURI: true,
                headers: { 'Content-Type': 'application/json' },
                data: params
            }).then(function (response) {
                if (response && response.data && response.data.errorMessage == "") {
                    $scope.CredentialUpload = [];
                    this.editSummary = 0;
                    this.editInfo = 0;
                    this.editBankInfo = 0;
                    this.editConflictOfInterestInfo = 0;
                    this.editBusinessInfo = 0;
                    this.editPic = 0;
                    this.editContact = 0;
                    this.editDocVerification = 0;
                    this.editPro = 0;
                    this.addUser = 0;
                    $scope.pannumberalpha = false;
                    $scope.pannumberlengthvalidation = false;
                    $scope.pannumberrequired = false;
                    $scope.taxnumberalpha = false;
                    $scope.taxnumberlengthvalidation = false;
                    $scope.taxnumberrequired = false;
                    $scope.tinnumberrequired = false;
                    $scope.getUserCredentials();
                    //swal("Done!", '', 'success');
                    this.editDocVerification = 0;
                } else {
                    growlService.growl(response.data.errorMessage, "inverse");
                }
            }, function (result) {
            });
            this.editDocVerification = 0;
        };


        ///////////////////
        // CREDENTIALS
        //////////////////

        $scope.cCode = '';

        $scope.populateCCCode = function (value) {
            $scope.cCode = '';
            $scope.companyCountryCodeUnits.forEach(function (item, index) {
                if (value.configText == item.configText) {
                    $scope.cCode = item.configValue;
                    return item;
                }
            })
        }


        $scope.companyStateUnitsTemp = [];
        $scope.displayStateVal = true;

        $scope.populateState = function (value) {
            if (value.configText == 'IN') {
                $scope.displayStateVal = false;
                $scope.companyStateUnitsTemp = $scope.companyStateUnits;
                $scope.companyStateUnitsTemp.forEach(function (item, index) {
                    item.configValue = item.configValue.split(':')[0];
                    if (typeof ($scope.userObj.vendorDetails.State) != 'object') {
                        if (($scope.userObj.vendorDetails.State).toLowerCase() == (item.configValue).toLowerCase()) {
                            return $scope.userObj.vendorDetails.State = item;
                        }
                    }
                });
            } else {
                $scope.displayStateVal = true;
                $scope.userObj.vendorDetails.State = "";
            }
        }


        $scope.acceptRegistration = function (wfid) {
            

            if ($scope.userObj.PERSONAL_INFO_VALID == 0 || $scope.userObj.CONTACT_INFO_VALID == 0 ||
                $scope.userObj.BANK_INFO_VALID == 0 || $scope.userObj.BUSINESS_INFO_VALID == 0 ||
                $scope.userObj.CONFLICT_INFO_VALID == 0)
            {
                var validationErrorMessage = '';
                if ($scope.userObj.PERSONAL_INFO_VALID == 0)
                {
                    validationErrorMessage = 'Personal Details';
                } else if ($scope.userObj.CONTACT_INFO_VALID == 0) {
                    validationErrorMessage = 'Contact Details';
                } else if ($scope.userObj.BANK_INFO_VALID == 0) {
                    validationErrorMessage = 'Bank Details';
                } else if ($scope.userObj.BUSINESS_INFO_VALID == 0) {
                    validationErrorMessage = 'Business Details';
                } else {
                    validationErrorMessage = 'Conflict Of Interest';
                }


                swal("Cancelled", "", "error");
                $(".sweet-alert h2").html("<br> Some mandatory fileds in <h3 style='color:red'>" + validationErrorMessage + " </h3> section are missing.");


                return;
            }

            userService.acceptRegistration({ "vendorID": userService.getUserId(), "wfID": wfid, "compID":userService.getUserCompanyId()})
                .then(function (response) {
                    if (response.errorMessage != "") {
                        growlService.growl(response.errorMessage, "inverse");
                    }
                    else if (response.errorMessage == "") {
                        swal("Thanks !", "Your registration process has been successfully completed", "success");
                    }
                });
        };


        this.updatePwd = function () {
            if ($scope.pwdObj.newPass.length < 6) {
                growlService.growl("Password should be at least 6 characters long.", "inverse");
                return false;
            } else if ($scope.pwdObj.newPass != $scope.pwdObj.confirmNewPass) {
                growlService.growl('Passwords do not match, please enter the same password in both new and Confirm Password fields', 'inverse');
                return false;
            }


            if ($scope.userObj.subUserPassword != $scope.pwdObj.oldPass) {
                $scope.ispassword = true;
                $scope.pwdObj.oldPass = '';
                swal("Warning!", 'Old Password Doesn\'t match .');
                return false;
            }

            $scope.pwdObj.userID = parseInt(userService.getUserId());
            $scope.pwdObj.username = $scope.userObj.phoneNum;
            userService.updatePassword($scope.pwdObj)
                .then(function (response) {
                    if (response.errorMessage == "") {
                        $scope.pwdObj = {
                            username: userService.getUserObj().username
                        };
                        swal("Done!", 'Your password has been successfully updated.', 'success');
                        location.reload();

                    } else {
                        swal("Error!", response.errorMessage, 'error');
                    }
                })
        }

        $scope.ispassword = false;

        $scope.checkpassword = function () {

            if ($scope.userObj.phoneNum == $scope.pwdObj.newPass) {
                $scope.ispassword = true;
                $scope.pwdObj.newPass = '';
                swal("Warning!", 'Phone and Password can not be same.');
            }
            else {
                $scope.ispassword = false;
            }
        };

        $scope.openPreQualificationForm = function (vendor) {
            $state.go('vendorRegistration1', { 'vendorId': $scope.userId });
        };

        $(".toggle-password").click(function () {
                $(this).toggleClass("fa-eye fa-eye-slash");
                var input = $($(this).attr("toggle"));
                if (input.attr("type") === "password") {
                    input.attr("type", "text");
                } else {
                    input.attr("type", "password");
            }
        });

    });