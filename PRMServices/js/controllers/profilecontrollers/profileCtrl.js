prmApp

//    //=================================================
//    // Profile
//    //=================================================

    .controller('profileCtrl', function ($timeout, $uibModal, $state, $window, $scope, growlService, userService, auctionsService, fwdauctionsService, $http, domain, $rootScope, fileReader, $filter, $log, reportingService) {



        $scope.showIsSuperUser = function () {
                    if (userService.getUserObj().isSuperUser) {
                        return true;
                    } else {
                        return false;
                    }
                }





//        $scope.isCustomer = userService.getUserType();
//        $scope.sessionid = userService.getUserToken();
//        $scope.userObj = {};
//        userService.getUserDataNoCache()
//            .then(function (response) {
//                $scope.userObj = response;
//            })
//        $scope.newVendor = {};
//        $scope.newVendor.panno = "";
//        $scope.newVendor.vatNum = "";
//        $scope.newVendor.serviceTaxNo = "";
//        $scope.formRequest = {};
//        $scope.formRequest.isForwardBidding = false;


//        $scope.basicinfoCollapse = $scope.contactinfo = $scope.docsverification = $scope.professionalinfo = true;
//        $scope.pwdmng = true;
//        $scope.isnegotiationrunning = '';
//        $scope.subcategories = '';
//        userService.isnegotiationrunning()
//            .then(function (response) {
//                $scope.isnegotiationrunning = response.data.IsNegotationRunningResult;
//            })
//        $scope.editPwd = 0;

//        $scope.isPhoneModifies = 0;
//        $scope.isEmailModifies = 0;
//        $scope.totalSubcats = [];
//        $scope.selectedCurrency = {};
//        $scope.currencies = [];

//        $scope.days = 0;
//        $scope.hours = 0;
//        $scope.mins = 0;

//        

//        $scope.addVendorShow = false;

//        $scope.checkVendorUniqueResult = function (idtype, inputvalue) {


//            if (idtype == "PHONE") {
//                $scope.checkVendorPhoneUniqueResult = false;
//            } else if (idtype == "EMAIL") {
//                $scope.checkVendorEmailUniqueResult = false;
//            }
//            else if (idtype == "PAN") {
//                $scope.checkVendorPanUniqueResult = false;
//            }
//            else if (idtype == "TIN") {
//                $scope.checkVendorTinUniqueResult = false;
//            }
//            else if (idtype == "STN") {
//                $scope.checkVendorStnUniqueResult = false;
//            }

//            if (inputvalue == "" || inputvalue == undefined) {
//                return false;
//            }



//            userService.checkUserUniqueResult(inputvalue, idtype).then(function (response) {
//                if (idtype == "PHONE") {
//                    $scope.checkVendorPhoneUniqueResult = !response;
//                } else if (idtype == "EMAIL") {
//                    $scope.checkVendorEmailUniqueResult = !response;
//                }
//                else if (idtype == "PAN") {
//                    $scope.checkPANUniqueResult = !response;
//                }
//                else if (idtype == "TIN") {
//                    $scope.checkTINUniqueResult = !response;
//                }
//                else if (idtype == "STN") {
//                    $scope.checkSTNUniqueResult = !response;
//                }
//                else if (idtype == "USER_NAME") {
//                    $scope.checkUserNameUniqueResult = !response;
//                }
//            });
//        };


//        $scope.pwdObj = {
//            username: userService.getUserObj().username,
//            oldPass: '',
//            newPass: ''
//        };

//        $scope.categories = [];

//        $scope.subcategories = [];
//        $scope.sub = {
//            selectedSubcategories: [],
//        }

//        $scope.selectedSubcategories = [];


//        $scope.changeCategory = function () {

//            $scope.selectedSubCategoriesList = [];
//            $scope.loadSubCategories();
//        }

//        $scope.loadSubCategories = function () {
//            $scope.subcategories = _.filter($scope.categoriesdata, { category: $scope.newVendor.category });
//        }


//        $scope.selectedSubCategoriesList = [];

//        $scope.selectSubcat = function () {
            
//            $scope.selectedSubCategoriesList = [];

//            $scope.vendorsLoaded = false;
//            var category = [];
//            var count = 0;
//            var succategory = "";
//            $scope.sub.selectedSubcategories = $filter('filter')($scope.subcategories, { ticked: true });
//            selectedcount = $scope.sub.selectedSubcategories.length;
//            if (selectedcount > 0) {
//                succategory = _.map($scope.sub.selectedSubcategories, 'id');
//                category.push(succategory);

//                $scope.selectedSubCategoriesList = succategory;

//                console.log($scope.selectedSubCategoriesList);
                
//            } else {
//                console.log($scope.selectedSubCategoriesList);
//            }

//        }

//        $scope.selectSubcat();













//        $scope.myDepartments = [];


//        $scope.getCategories = function () {
//            auctionsService.getCategories(userService.getUserId())
//                .then(function (response) {
//                    $scope.categories = response;
//                    $scope.addVendorCats = _.uniq(_.map(response, 'category'));
//                    $scope.categoriesdata = response;
//                    $scope.showCategoryDropdown = true;
//                })
//        }



//        $scope.getKeyValuePairs = function (parameter) {

//            auctionsService.getKeyValuePairs(parameter)
//                .then(function (response) {
//                    if (parameter == "CURRENCY") {
//                        $scope.currencies = response;
//                    } else if (parameter == "TIMEZONES") {
//                        $scope.timezones = response;

//                    }

//                    $scope.selectedCurrency = $filter('filter')($scope.currencies, { value: response.currency });
//                    $scope.selectedCurrency = $scope.selectedCurrency[0];
//                })

//        }

//        $scope.getKeyValuePairs('CURRENCY');
//        $scope.getKeyValuePairs('TIMEZONES');



//        /*pagination code*/
//        $scope.totalItems = 0;
//        $scope.totalVendors = 0;
//        $scope.totalSubuser = 0;
//        $scope.totalInactiveVendors = 0;
//        $scope.totalLeads = 0;
//        $scope.currentPage = 1;
//        $scope.currentPage2 = 1;
//        $scope.itemsPerPage = 5;
//        $scope.itemsPerPage2 = 5;
//        $scope.maxSize = 5; 

//        $scope.setPage = function (pageNo) {
//            $scope.currentPage = pageNo;
//        };

//        $scope.pageChanged = function () {
//        };

//        $scope.getCategories();

//        $scope.userDetails = {
//            achievements: "",
//            assocWithOEM: false,
//            clients: "",
//            establishedDate: "01-01-1970",
//            aboutUs: "",
//            logoFile: "",
//            logoURL: "",
//            products: "",
//            strengths: "",
//            responseTime: "",
//            oemCompanyName: "",
//            oemKnownSince: "",
//            workingHours: "",
//            files: [],
//            directors: "",
//            address: "",
//            dateshow: 0

//        };

//        $scope.userObj = userService.getUserObj();
//        $scope.uId = userService.getUserId();

//        this.updatePwd = function () {
//            if ($scope.pwdObj.newPass.length < 6) {
//                growlService.growl("Password should be at least 6 characters long.", "inverse");
//                return false;
//            } else if ($scope.pwdObj.newPass != $scope.pwdObj.confirmNewPass) {
//                growlService.growl('Passwords do not match, please enter the same password in both new and Confirm Password fields', 'inverse');
//                return false;
//            }
//            $scope.pwdObj.userID = parseInt(userService.getUserId());
//            userService.updatePassword($scope.pwdObj)
//                .then(function (response) {
//                    if (response.errorMessage == "") {
//                        $scope.pwdObj = {
//                            username: userService.getUserObj().username
//                        };
//                        swal("Done!", 'Your password has been successfully updated.', 'success');
//                    } else {
//                        swal("Error!", response.errorMessage, 'error');
//                    }
//                })
//        }

//        $scope.getSubCats = function () {

//        }

//        $scope.getSubCats();

//        //$scope.callGetUserDetails = function () {
//        //    $log.info("IN GET USER DETAILS");
//        //    userService.getProfileDetails({ "userid": userService.getUserId(), "sessionid": userService.getUserToken() })
//        //        .then(function (response) {
//        //            $scope.userStatus = "registered";
//        //            if (response != undefined) {
//        //                $scope.userDetails = response;

//        //                $scope.NegotiationSettings = $scope.userDetails.NegotiationSettings;
//        //                var duration = $scope.NegotiationSettings.negotiationDuration.split(" ", 4);
//        //                $scope.days = parseInt(duration[0]);
//        //                duration = duration[1];
//        //                duration = duration.split(":", 4);
//        //                $scope.hours = parseInt(duration[0]);
//        //                $scope.mins = parseInt(duration[1]);

//        //                $http({
//        //                    method: 'GET',
//        //                    url: domain + 'getcategories?userid=' + userService.getUserId() + '&sessionid=' + userService.getUserToken(),
//        //                    encodeURI: true,
//        //                    headers: { 'Content-Type': 'application/json' }
//        //                }).then(function (response) {
//        //                    if (response && response.data) {
//        //                        if (response.data.length > 0) {
//        //                            $scope.totalSubcats = $filter('filter')(response.data, { category: $scope.userDetails.category });
//        //                            $scope.selectedCurrency = $filter('filter')($scope.currencies, { value: response.currency });
//        //                            $scope.selectedCurrency = $scope.selectedCurrency[0];
//        //                            if ($scope.userDetails.subcategories && $scope.userDetails.subcategories.length > 0) {
//        //                                for (i = 0; i < $scope.userDetails.subcategories.length; i++) {
//        //                                    for (j = 0; j < $scope.totalSubcats.length; j++) {
//        //                                        if ($scope.userDetails.subcategories[i].id == $scope.totalSubcats[j].id) {
//        //                                            $scope.totalSubcats[j].ticked = true;
//        //                                        }
//        //                                    }
//        //                                }
//        //                            }
//        //                        }
//        //                    } else {
//        //                    }
//        //                }, function (result) {
//        //                });
//        //                if ($scope.userDetails.subcategories && $scope.userDetails.subcategories.length > 0) {
//        //                    for (i = 0; i < $scope.userDetails.subcategories.length; i++) {
//        //                        $scope.subcategories += $scope.userDetails.subcategories[i].subcategory + ";";
//        //                    }
//        //                }
//        //                var data = response.establishedDate;
//        //                var date = new Date(parseInt(data.substr(6)));
//        //                $scope.userDetails.establishedDate = date.getDate() + '/' + (date.getMonth() + 1) + '/' + date.getFullYear();

//        //                var today = new Date();
//        //                var todayDate = today.getDate() + '/' + (today.getMonth() + 1) + '/' + today.getFullYear();
//        //                $scope.userDetails.dateshow = 0;
//        //                if ($scope.userDetails.establishedDate == todayDate) {
//        //                    $scope.userDetails.dateshow = 1;
//        //                }

//        //                if (response.registrationScore > 89) {

//        //                    $scope.userStatus = "Authorised";

//        //                }
//        //            }

//        //            $log.info($scope.userDetails);
//        //        });
//        //}



//        $scope.subUsers = [];
//        $scope.inactiveSubUsers = [];

//        $scope.getSubUserData = function () {
//            userService.getSubUsersData({ "userid": userService.getUserId(), "sessionid": userService.getUserToken() })
//                .then(function (response) {
//                    $scope.subUsers = $filter('filter')(response, { isValid: true });
//                    $scope.inactiveSubUsers = $filter('filter')(response, { isValid: false });
//                });
//        }

//        $scope.deleteUser = function (userid) {
//            userService.deleteUser({ "userID": userid, "referringUserID": userService.getUserId() })
//                .then(function (response) {
//                    if (response.errorMessage != "") {
//                        growlService.growl(response.errorMessage, "inverse");
//                    } else {
//                        growlService.growl("User deleted Successfully", "inverse");
//                        $scope.getSubUserData();
//                    }
//                });
//        }

//        $scope.activateUser = function (userid) {
//            userService.activateUser({ "userID": userid, "referringUserID": userService.getUserId() })
//                .then(function (response) {
//                    if (response.errorMessage != "") {
//                        growlService.growl(response.errorMessage, "inverse");
//                    } else {
//                        growlService.growl("User Addes Successfully", "success");
//                        $scope.getSubUserData();
//                    }
//                });
//        }

//        $scope.getSubUserData();


//        this.profileSummary = "";

//        var loginUserData = userService.getUserObj();
//        $scope.userObj.fullName = loginUserData.firstName + " " + loginUserData.lastName;
//        $scope.userObj.firstName = loginUserData.firstName;
//        $scope.userObj.lastName = loginUserData.lastName;
//        $scope.userObj.gender = "male";
//        $scope.userObj.birthDay = "23/06/1988";
//        $scope.userObj.martialStatus = "Single";
//        $scope.userObj.phoneNum = loginUserData.phoneNum;
//        $scope.userObj.email = loginUserData.email;
//        $scope.oldPhoneNum = loginUserData.phoneNum;
//        $scope.oldemail = loginUserData.email;
//        $scope.userObj.email = loginUserData.email;

//        $scope.userObj.addressSuite = loginUserData.addressLine1 + " " + loginUserData.addressLine2 + " " + loginUserData.addressLine3;
//        $scope.userObj.addressCity = loginUserData.city;
//        $scope.userObj.addressCountry = loginUserData.country;
//        this.userId = userService.getUserId();
//        $scope.isOTPVerified = loginUserData.isOTPVerified;
//        $scope.isEmailOTPVerified = loginUserData.isEmailOTPVerified;
//        $rootScope.$on("CallProfileMethod", function () {
//            $scope.updateUserDataFromService();
//        });
//        this.imagefilesonlyforlogo = false;
//        this.editSummary = 0;
//        this.editInfo = 0;
//        this.editPic = 0;
//        this.editDocVerification = 0;
//        this.editContact = 0;
//        this.editPro = 0;
//        this.editPwd = 0;
//        this.addUser = 0;
//        this.addVendor = 0;
//        $scope.myAuctionsLoaded = false;
//        $scope.myAuctions = [];
//        $scope.myActiveLeads = [];
//        var date = new Date();

//        $scope.getFile = function () {
//            $scope.progress = 0;
//            $scope.file = $("#tindoc")[0].files[0];
//            fileReader.readAsDataUrl($scope.file, $scope)
//                .then(function (result) {

//                });
//        };

//        this.changepasswordstatus = this.newphonenumber_validation_error = this.newphonenumber_required_error = false;
//        this.newphonenumber = "";
//        $scope.CredentialUpload = [];
//        $scope.uploadedCredentials = [{ "credentialID": '', "fileType": 'PAN', 'fileLink': "", 'isVerified': 0 }, { "credentialID": '', "fileType": 'TIN', 'fileLink': "", 'isVerified': 0 }, { "credentialID": '', "fileType": 'STN', 'fileLink': "", 'isVerified': 0 }];
//        $scope.logoFile = { "fileName": '', 'fileStream': "" };
//        this.newphonenumber_errors = false;
//        $scope.otpvalue = "";

//        $scope.getUserCredentials = function () {
//            $http({
//                method: 'GET',
//                url: domain + 'getusercredentials?sessionid=' + userService.getUserToken() + "&userid=" + userService.getUserId(),
//                encodeURI: true,
//                headers: { 'Content-Type': 'application/json' }
//            }).then(function (response) {

//                $scope.CredentialsResponce = response.data;


//                if (response && response.data && response.data.length > 0) {
//                    if (response.data[0].errorMessage == "") {
//                        var panObj = _.filter($scope.CredentialsResponce, ['fileType', 'PAN']);
//                        $scope.pannumber = panObj[0].credentialID;
//                        $scope.vatnumber = _.filter($scope.CredentialsResponce, ['fileType', 'TIN'])[0].credentialID;
//                        $scope.taxnumber = _.filter($scope.CredentialsResponce, ['fileType', 'STN'])[0].credentialID;
//                        var verifiedDocsCount = 0;
//                        $.each(response.data, function (key, value) {
//                            $scope.PAN = value.credentialID;
//                            if (value.isVerified == 1) {
//                                verifiedDocsCount++;
//                            }
//                        });
//                        if (response.data.length == verifiedDocsCount + 1 || response.data.length == verifiedDocsCount) {
//                            userService.updateVerified(1);
//                        } else {
//                            userService.updateVerified(0);
//                        }
//                        $scope.uploadedCredentials = response.data;
//                        if (response.data.length > 0) {
//                            this.editDocVerification = 0;
//                            $scope.editDocVerification = 0;
//                        }
//                    }
//                }
//            }, function (result) {
//                $log.error("error in request service");
//            });
//        };
//        $scope.updateUserDataFromService = function (msg) {
//            loginUserData = userService.getUserObj();
//            $scope.oldPhoneNum = loginUserData.phoneNum;
//            $scope.oldemail = loginUserData.email;
//            $scope.userObj.fullName = loginUserData.firstName + " " + loginUserData.lastName;
//            $scope.userObj.firstName = loginUserData.firstName;
//            $scope.userObj.lastName = loginUserData.lastName;
//            $scope.userObj.gender = "male";
//            $scope.userObj.birthDay = "23/06/1988";
//            $scope.userObj.martialStatus = "Single";
//            $scope.userObj.phoneNum = loginUserData.phoneNum;
//            $scope.userObj.email = loginUserData.email;
//            $scope.userObj.addressSuite = loginUserData.addressLine1 + " " + loginUserData.addressLine2 + " " + loginUserData.addressLine3;
//            $scope.userObj.addressCity = loginUserData.city;
//            $scope.userObj.addressCountry = loginUserData.country;

//            this.userId = userService.getUserId();
//            $scope.isOTPVerified = loginUserData.isOTPVerified;
//            $scope.isEmailOTPVerified = loginUserData.isEmailOTPVerified;
//            $scope.credentialsVerified = loginUserData.credentialsVerified;
//            //$scope.callGetUserDetails();
//            $scope.getUserCredentials();
//            if ($scope.isOTPVerified && $scope.isEmailOTPVerified) {
//                $scope.getAuctions();


//                auctionsService.getactiveleads({ "userid": userService.getUserId(), "sessionid": userService.getUserToken() })
//                    .then(function (response) {
//                        $scope.myActiveLeads = response;
//                        if ($scope.myActiveLeads.length > 0) {
//                            $scope.myAuctionsLoaded = true; 
//                            $scope.totalLeads = $scope.myActiveLeads.length;
//                        } else {
//                            $scope.totalLeads = 0;
//                            $scope.myAuctionsLoaded = false;
//                            $scope.myAuctionsMessage = "There are no auctions running right now for you.";
//                        }
//                    });
//            }
//        }

//        $scope.getAuctions = function () {
//            $log.info($scope.formRequest.isForwardBidding);
//            if (!$scope.formRequest.isForwardBidding) {
//                auctionsService.getmyAuctions({ "userid": userService.getUserId(), "sessionid": userService.getUserToken() })
//                    .then(function (response) {
//                        $scope.myAuctions = response;
//                        if ($scope.myAuctions.length > 0) {
//                            $scope.myAuctionsLoaded = true;
//                            $scope.totalItems = $scope.myAuctions.length;
//                        } else {
//                            $scope.myAuctionsLoaded = false;
//                            $scope.totalItems = 0;
//                            $scope.myAuctionsMessage = "There are no auctions running right now for you.";
//                        }
//                    });
//            }
//            else {
//                fwdauctionsService.getmyAuctions({ "userid": userService.getUserId(), "sessionid": userService.getUserToken() })
//                    .then(function (response) {
//                        $scope.myAuctions = response;
//                        if ($scope.myAuctions.length > 0) {
//                            $scope.myAuctionsLoaded = true;
//                            $scope.totalItems = $scope.myAuctions.length;
//                        } else {
//                            $scope.myAuctionsLoaded = false;
//                            $scope.totalItems = 0;
//                            $scope.myAuctionsMessage = "There are no auctions running right now for you.";
//                        }
//                    });
//            }
            
//        }

//        $scope.updateUserDataFromService('default call');
//        this.editMode = function () {
//            this.editPro = 1;

//        }

//        $scope.generatePDF = function () {
//            var doc = new jsPDF();
//            var specialElementHandlers = {
//                '#editor': function (element, renderer) {
//                    return true;
//                }
//            };
//            doc.fromHTML($('.myTables').get(0), 15, 15, {
//                'width': 170,
//                'elementHandlers': specialElementHandlers
//            });
//            doc.save("test.pdf");
//        }

//        $scope.otpModalInstances = function () {
//            return $uibModal.open({
//                animation: true,
//                templateUrl: 'verifyOTPModal.html',
//                controller: 'modalInstanceCtrlOTP',
//                size: 'sm',
//                backdrop: 'static',
//                keyboard: false
//            });
//        }

//        $scope.emailModalInstances = function () {
//            return $uibModal.open({
//                animation: true,
//                templateUrl: 'verifyEmailOTPModal.html',
//                controller: 'modalInstanceCtrlOTP',
//                size: 'sm',
//                backdrop: 'static',
//                keyboard: false
//            });
//        }

//        $scope.addVendorValidation = function () {
//            $scope.emailRegx = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
//            $scope.mobileRegx = /^\+?([0-9]{2})\)?[-. ]?([0-9]{4})[-. ]?([0-9]{4})$/;
//            $scope.panregx = /^([a-zA-Z]{5})(\d{4})([a-zA-Z]{1})$/;

//            $scope.addVendorValidationStatus = false;
//            $scope.firstvalidation = $scope.companyvalidation = $scope.lastvalidation = $scope.contactvalidation = $scope.emailvalidation = $scope.categoryvalidation = $scope.emailregxvalidation = $scope.contactvalidationlength = $scope.panregxvalidation = $scope.tinvalidation = $scope.stnvalidation = $scope.currencyvalidation = $scope.knownSincevalidation = false;

//            if ($scope.newVendor.firstName == "" || $scope.newVendor.firstName === undefined) {
//                $scope.firstvalidation = true;
//                $scope.addVendorValidationStatus = true;
//                return;
//            }
//            if ($scope.newVendor.lastName == "" || $scope.newVendor.lastName === undefined) {
//                $scope.lastvalidation = true;
//                $scope.addVendorValidationStatus = true;
//                return;
//            }
//            if ($scope.newVendor.contactNum == "" || $scope.newVendor.contactNum === undefined || isNaN($scope.newVendor.contactNum)) {
//                $scope.contactvalidation = true;
//                $scope.addVendorValidationStatus = true;
//                return;
//            } else if (!$scope.mobileRegx.test($scope.newVendor.contactNum)) {
//                $scope.contactvalidationlength = true;
//                $scope.addVendorValidationStatus = true;
//                return;
//            }
//            if ($scope.newVendor.email == "" || $scope.newVendor.email === undefined) {
//                $scope.emailvalidation = true;
//                $scope.addVendorValidationStatus = true;
//                return;
//            } else if (!$scope.emailRegx.test($scope.newVendor.email)) {
//                $scope.emailregxvalidation = true;
//                $scope.addVendorValidationStatus = true;
//                return;
//            }
//            if ($scope.checkVendorEmailUniqueResult || $scope.checkVendorEmailUniqueResult) {
//                $scope.addVendorValidationStatus = true;
//                return;
//            }
//            //if ($scope.newVendor.knownSince == "" || $scope.newVendor.knownSince === undefined) {
//            //    $scope.knownSincevalidation = true;
//            //    $scope.addVendorValidationStatus = true;
//            //    return;
//            //}
//            if ($scope.newVendor.companyName == "" || $scope.newVendor.companyName === undefined) {
//                $scope.companyvalidation = true;
//                $scope.addVendorValidationStatus = true;
//                return;
//            }



//            if ($scope.newVendor.category == "" || $scope.newVendor.category === undefined) {
//                $scope.categoryvalidation = true;
//                $scope.addVendorValidationStatus = true;
//                return;
//            }
//            if ($scope.newVendor.currency == "" || $scope.newVendor.currency === undefined) {
//                $scope.currencyvalidation = true;
//                $scope.addVendorValidationStatus = true;
//                return;
//            }

//        }

//        $scope.addVendor = function () {
//            $scope.addVendorValidation();
//            if ($scope.addVendorValidationStatus) {
//                return false;
//            }
//            var vendCAtegories = [];
//            $scope.newVendor.category = $scope.newVendor.category;
//            vendCAtegories.push($scope.newVendor.category);
//            var params = {
//                "register": {
//                    "firstName": $scope.newVendor.firstName,
//                    "lastName": $scope.newVendor.lastName,
//                    "email": $scope.newVendor.email,
//                    "phoneNum": $scope.newVendor.contactNum,
//                    "altEmail": $scope.newVendor.altEmail,
//                    "altPhoneNum": $scope.newVendor.altPhoneNum,
//                    "username": $scope.newVendor.contactNum,
//                    "password": $scope.newVendor.contactNum,
//                    "companyName": $scope.newVendor.companyName ? $scope.newVendor.companyName : "",
//                    "isOTPVerified": 0,
//                    "category": $scope.newVendor.category,
//                    "userType": "VENDOR",
//                    "panNumber": ("panno" in $scope.newVendor) ? $scope.newVendor.panno : "",
//                    "stnNumber": ("serviceTaxNo" in $scope.newVendor) ? $scope.newVendor.serviceTaxNo : "",
//                    "vatNumber": ("vatNum" in $scope.newVendor) ? $scope.newVendor.vatNum : "",
//                    "referringUserID": userService.getUserId(),
//                    "knownSince": ("knownSince" in $scope.newVendor) ? $scope.newVendor.knownSince : "",
//                    "errorMessage": "",
//                    "sessionID": "",
//                    "userID": 0,
//                    "department": "",
//                    "currency": $scope.newVendor.currency.key,
//                    "subcategories": $scope.selectedSubCategoriesList
//                }
//            };
//            $http({
//                method: 'POST',
//                url: domain + 'register',
//                encodeURI: true,
//                headers: { 'Content-Type': 'application/json' },
//                data: params
//            }).then(function (response) {
//                if (response && response.data && response.data.errorMessage == "") {
//                    $scope.newVendor = null;
//                    $scope.newVendor = {};
//                    $scope.addVendorShow = false;
//                    growlService.growl("Vendor Added Successfully.", 'inverse');
//                    location.reload();
//                    this.addVendor = 0;
//                } else if (response && response.data && response.data.errorMessage) {
//                    growlService.growl(response.data.errorMessage, 'inverse');
//                    location.reload();
//                } else {
//                    growlService.growl('Unexpected Error Occurred', 'inverse');
//                }
//            });
//        }


//        this.updateUserInfo = function (contact) {
//            $log.info("IN UPDATE");
//            if ($scope.oldPhoneNum != $scope.userObj.phoneNum) {
//                $scope.isPhoneModifies = 1;
//            }

//            var params = {};
//            if ($scope.userDetails.assocWithOEM) {
//                if ($scope.userDetails.oemCompanyName == "" || $scope.userDetails.oemKnownSince == "" || $scope.userDetails.assocWithOEMFileName == "") {
//                    growlService.growl("If Associated with OEM, please provide further details.", "inverse");
//                    return false;
//                }
//            }
//            if ($scope.userObj.firstName.toString() == "") {
//                growlService.growl("Name cannot be empty.", "inverse");
//                return false;
//            }
//            if ($scope.userObj.lastName.toString() == "") {
//                growlService.growl("Name cannot be empty.", "inverse");
//                return false;
//            }

//            if (contact = 'contact')
//            {
//                if ($scope.userObj.phoneNum.toString() == "") {
//                    growlService.growl("Phone Number cannot be empty.", "inverse");
//                    return false;
//                }
//                if (isNaN($scope.userObj.phoneNum)) {
//                    growlService.growl("Please Enter correct Mobile number.", "inverse");
//                    return false;
//                }
//                if ($scope.userObj.phoneNum.toString().length != 10) {
//                    growlService.growl("Phone Number Must be 10 digits.", "inverse");
//                    return false;
//                }
//                if ($scope.userObj.email.toString() == "") {
//                    growlService.growl("Email cannot be empty.", "inverse");
//                    return false;
//                }
//            }
            


//            if (this.editPro == 1 && $scope.userDetails.aboutUs == "") {
//                growlService.growl("Please update your about us section", "inverse");
//                return false;
//            }

//            var ts = moment($scope.userDetails.establishedDate, "DD-MM-YYYY").valueOf();
//            var m = moment(ts);
//            var auctionStartDate = new Date(m);
//            var milliseconds = parseInt(auctionStartDate.getTime() / 1000.0);
//            $scope.userDetails.establishedDate = "/Date(" + milliseconds + "000+0530)/";

//            params = $scope.userDetails;


//            params.firstName = $scope.userObj.firstName;
//            params.lastName = $scope.userObj.lastName;
//            params.phoneNum = $scope.userObj.phoneNum;
//            if ($scope.logoFile != '' && $scope.logoFile != null) {
//                params.logoFile = $scope.logoFile;
//                params.logoURL += $scope.logoFile.fileName;
//            }
//            params.isOTPVerified = $scope.isOTPVerified;
//            params.credentialsVerified = $scope.credentialsVerified;
//            if (params.phoneNum != $scope.userObj.phoneNum) {
//                params.isOTPVerified = 0;
//            }
//            params.email = $scope.userObj.email;
//            params.isEmailOTPVerified = $scope.isEmailOTPVerified;
//            if (params.email != $scope.userObj.email) {
//                params.isEmailOTPVerified = 0;
//            }
//            params.sessionID = userService.getUserToken();
//            params.userID = userService.getUserId();
//            params.errorMessage = "";
//            params.subcategories = $scope.userDetails.subcategories;

//            params.altPhoneNum = $scope.userObj.altPhoneNum;
//            params.altEmail = $scope.userObj.altEmail;

//            userService.updateUser(params)
//                .then(function (response) {
//                    if (response.toLowerCase().indexOf('already exists') > 0) {
//                        userService.getUserDataNoCache().then(function (response) {

//                            var loginUserData = userService.getUserObj();
//                            $scope.userObj.fullName = loginUserData.firstName + " " + loginUserData.lastName;
//                            $scope.userObj.firstName = loginUserData.firstName;
//                            $scope.userObj.lastName = loginUserData.lastName;
//                            $scope.userObj.altPhoneNum = loginUserData.altPhoneNum;
//                            $scope.userObj.altEmail = loginUserData.altEmail;
//                            $scope.userObj.gender = "male";
//                            $scope.userObj.birthDay = "23/06/1988";
//                            $scope.userObj.martialStatus = "Single";
//                            $scope.userObj.phoneNum = loginUserData.phoneNum;
//                            $scope.userObj.email = loginUserData.email;
//                            $scope.userObj.username = loginUserData.username;
//                            $scope.oldPhoneNum = loginUserData.phoneNum;
//                            $scope.oldemail = loginUserData.email;
//                            $scope.userObj = loginUserData;
//                            //$scope.callGetUserDetails();
//                            $scope.updateUserDataFromService();
//                            $state.go('pages.profile.profile-about');
//                            $state.reload();
//                        });
//                    }
                    
//                    $state.go('pages.profile.profile-about');
//                    $state.reload();
//                });

//            this.editSummary = 0;
//            this.editInfo = 0;
//            this.editPic = 0;
//            this.editContact = 0;
//            this.editDocVerification = 0;
//            this.editPro = 0;
//            this.imagefilesonlyforlogo = false;
//            this.addUser = 0;
//            //$scope.callGetUserDetails();
//        };
//        $scope.taxnumberalpha = false;
//        $scope.taxnumberlengthvalidation = false;
//        $scope.taxnumberrequired = false;
//        $scope.pannumberalpha = false;
//        $scope.pannumberlengthvalidation = false;
//        $scope.pannumberrequired = false;
//        $scope.tinnumberrequired = false;
//        $scope.pannumber = "";
//        $scope.vatnumber = "";
//        $scope.taxnumber = "";
//        $scope.panregx = new RegExp("/^([a-zA-Z]{5})(\d{4})([a-zA-Z]{1})$/");
//        $scope.panValidations = function (pannumber) {
//            if (pannumber != "" && !$scope.panregx.test(pannumber)) {
//                $scope.pannumberalpha = false;
//                $scope.pannumberlengthvalidation = false;
//                $scope.pannumberrequired = true;
//            } else if (('' + pannumber).length < 10) {
//                $scope.pannumberalpha = false;
//                $scope.pannumberlengthvalidation = true;
//                $scope.pannumberrequired = false;
//            } else if (!(/^[a-zA-Z0-9]*$/.test(pannumber))) {
//                $scope.pannumberalpha = true;
//                $scope.pannumberlengthvalidation = false;
//                $scope.pannumberrequired = false;
//            } else {
//                $scope.pannumberalpha = false;
//                $scope.pannumberlengthvalidation = false;
//                $scope.pannumberrequired = false;
//            }
//        }
//        $scope.tinValidations = function (vatnumber) {
//            if (vatnumber == "") {
//                $scope.tinnumberrequired = true;
//            } else {
//                $scope.tinnumberrequired = false;
//            }
//        }
//        $scope.taxValidations = function (taxnumber) {
//            if (taxnumber == "") {
//                $scope.taxnumberalpha = false;
//                $scope.taxnumberlengthvalidation = false;
//                $scope.taxnumberrequired = true;
//            } else if (("" + taxnumber).length < 15) {
//                $scope.taxnumberalpha = false;
//                $scope.taxnumberlengthvalidation = true;
//                $scope.taxnumberrequired = false;
//            } else if (!(/^[a-zA-Z0-9]*$/.test(taxnumber))) {
//                $scope.taxnumberalpha = true;
//                $scope.taxnumberlengthvalidation = false;
//                $scope.taxnumberrequired = false;
//            } else {
//                $scope.taxnumberalpha = false;
//                $scope.taxnumberlengthvalidation = false;
//                $scope.taxnumberrequired = false;
//            }
//        }
//        this.updateVerficationInfo = function (pannumber, vatnumber, taxnumber) {

//            var panObj = _.filter($scope.CredentialsResponce, ['fileType', 'PAN'])[0];
//            var tinObj = _.filter($scope.CredentialsResponce, ['fileType', 'TIN'])[0];
//            var stnObj = _.filter($scope.CredentialsResponce, ['fileType', 'STN'])[0];

//            if ((!$scope.panObject.fileType || pannumber === "" || !(/^[A-Z]{5}\d{4}[A-Z]{1}$/.test(pannumber.toUpperCase()))) && panObj.credentialID != pannumber) {
//                $scope.pannumberalpha = false;
//                $scope.pannumberlengthvalidation = false;
//                $scope.pannumberrequired = false;

//                if (pannumber === "") {
//                    $scope.pannumberrequired = true;
//                }
//                else {
//                    if (!(/^[A-Z]{5}\d{4}[A-Z]{1}$/.test(pannumber.toUpperCase()))) {
//                        $scope.pannumberlengthvalidation = true;
//                    }
//                }

//                return false;
//            }
//            else {
//                $scope.pannumberalpha = false;
//                $scope.pannumberlengthvalidation = false;
//                $scope.pannumberrequired = false;
//            }
//            if ((!$scope.tinObject.fileType || vatnumber === "" || !(/^\d{11}$/.test(vatnumber))) && tinObj.credentialID != vatnumber) {
//                $scope.tinnumberrequired = false;
//                $scope.tinnumberlengthvalidation = false;
//                if (vatnumber === "") {
//                    $scope.tinnumberrequired = true;
//                }
//                else {
//                    if (!(/^\d{11}$/.test(vatnumber.toUpperCase()))) {
//                        $scope.tinnumberlengthvalidation = true;
//                    }
//                }

//                return false;
//            }
//            else {
//                $scope.tinnumberrequired = false;
//                $scope.tinnumberlengthvalidation = false;
//            }
//            if ((!$scope.stnObject.fileType || taxnumber === "" || !(/^[a-zA-Z0-9]{15}$/.test(taxnumber))) && stnObj.credentialID != taxnumber) {
//                $scope.taxnumberalpha = false;
//                $scope.taxnumberlengthvalidation = false;
//                $scope.taxnumberrequired = false;
//                if (taxnumber === "") {
//                    $scope.taxnumberrequired = true;
//                }
//                else {
//                    if (!(/^[a-zA-Z0-9]{15}$/.test(taxnumber.toUpperCase()))) {
//                        $scope.taxnumberlengthvalidation = true;
//                    }
//                }


//                return false;
//            }
//            else {
//                $scope.taxnumberalpha = false;
//                $scope.taxnumberlengthvalidation = false;
//                $scope.taxnumberrequired = false;
//            }


//            if (pannumber !== "" && (panObj.credentialID != pannumber || $scope.panObject.fileStream)) {
//                $scope.panObject.credentialID = pannumber;
//                if ($scope.panObject.fileStream) {
//                    $scope.CredentialUpload.push($scope.panObject);
//                }
//            }

//            if (vatnumber !== "" && (tinObj.credentialID != vatnumber || $scope.tinObject.fileStream)) {
//                $scope.tinObject.credentialID = vatnumber;
//                if ($scope.tinObject.fileStream) {
//                    $scope.CredentialUpload.push($scope.tinObject);
//                }
//            }
//            if (taxnumber !== "" && (stnObj.credentialID != taxnumber || $scope.stnObject.fileStream)) {
//                $scope.stnObject.credentialID = taxnumber;
//                if ($scope.stnObject.fileStream) {
//                    $scope.CredentialUpload.push($scope.stnObject);
//                }
//            }

//            if ($scope.CredentialUpload.length == 0) {
//                growlService.growl("Please enter at least one credential for upload", "inverse");
//                return false;
//            }
//            var params = { 'userID': userService.getUserId(), 'files': $scope.CredentialUpload, 'sessionID': userService.getUserToken() };
//            $http({
//                method: 'POST',
//                url: domain + 'updatecredentials',
//                encodeURI: true,
//                headers: { 'Content-Type': 'application/json' },
//                data: params
//            }).then(function (response) {
//                if (response && response.data && response.data.errorMessage == "") {
//                    this.editSummary = 0;
//                    this.editInfo = 0;
//                    this.editPic = 0;
//                    this.editContact = 0;
//                    this.editDocVerification = 0;
//                    this.editPro = 0;
//                    this.addUser = 0;
//                    $scope.pannumberalpha = false;
//                    $scope.pannumberlengthvalidation = false;
//                    $scope.pannumberrequired = false;
//                    $scope.taxnumberalpha = false;
//                    $scope.taxnumberlengthvalidation = false;
//                    $scope.taxnumberrequired = false;
//                    $scope.tinnumberrequired = false;
//                    $scope.getUserCredentials();
//                    swal("Done!", 'Your credentials are being verified. Our associates will contact you as soon as it is done.', 'success');
//                    this.editDocVerification = 0;
//                } else {
//                    growlService.growl(response.data.errorMessage, "inverse");
//                    $log.info(response.data.errorMessage);
//                }
//            }, function (result) {
//                $log.info(result);
//            });
//            this.editDocVerification = 0;
//        };

//        $scope.panObject = {};
//        $scope.tinObject = {};
//        $scope.stnObject = {};

//        $scope.ImportEntity = {};

//        $scope.getFile1 = function (id, doctype, ext) {
//            $scope.progress = 0;
//            $scope.file = $("#" + id)[0].files[0];
//            $scope.docType = doctype + "." + ext;
//            fileReader.readAsDataUrl($scope.file, $scope)
//                .then(function (result) {
//                    if (id != "assocWithOEMFile" && id != 'storeLogo') {
//                        var bytearray = new Uint8Array(result);
//                        var fileobj = {};
//                        fileobj.fileStream = $.makeArray(bytearray);
//                        fileobj.fileType = $scope.docType;
//                        fileobj.isVerified = 0;
//                        if (doctype == "PAN") {
//                            fileobj.credentialID = $scope.pannumber;
//                            $scope.panObject = fileobj;
//                        }
//                        else if (doctype == "TIN") {
//                            fileobj.credentialID = $scope.vatnumber;
//                            $scope.tinObject = fileobj;
//                        }
//                        else if (doctype == "STN") {
//                            fileobj.credentialID = $scope.taxnumber;
//                            $scope.stnObject = fileobj;
//                        }
//                    }

//                    if (id == "storeLogo") {
//                        var bytearray = new Uint8Array(result);
//                        $scope.logoFile.fileStream = $.makeArray(bytearray);
//                        $scope.logoFile.fileName = $scope.file.name;
//                    }

//                    if (id == "profileFile") {
//                        var bytearray = new Uint8Array(result);
//                        $scope.userDetails.profileFile = $.makeArray(bytearray);
//                        $scope.userDetails.profileFileName = $scope.file.name;
//                    }


//                    if (id == "vendorsAttachment") {
//                        if (ext != "xlsx") {
//                            swal("Error!", "File type should be XSLX. Please download the template and  fill values accordingly.", "error");
//                            return;
//                        }
//                        var bytearray = new Uint8Array(result);
//                        $scope.ImportEntity.attachment = $.makeArray(bytearray);
//                        $scope.ImportEntity.attachmentFileName = $scope.file.name;
//                        $scope.AddVendorsExcel();
//                    }

//                    else {
//                        var bytearray = new Uint8Array(result);
//                        $scope.userDetails.assocWithOEMFile = $.makeArray(bytearray);
//                        $scope.userDetails.assocWithOEMFileName = $scope.file.name;
//                    }

//                });
//        };

//        $scope.getFile2 = function () {
//            $scope.progress = 0;
//            $scope.file = $("#storeLogo")[0].files[0];
//            if ($scope.file.type == "image/jpeg" || $scope.file.type == "image/jpg" || $scope.file.type == "image/png") {
//                fileReader.readAsDataUrl($scope.file, $scope)
//                    .then(function (result) {
//                        var bytearray = new Uint8Array(result);
//                        $scope.logoFile.fileStream = $.makeArray(bytearray);
//                        $scope.logoFile.fileName = $scope.file.name;
//                        this.imagefilesonlyforlogo = false;
//                    });
//            } else {
//                this.imagefilesonlyforlogo = true;
//            }
//        };

//        $scope.addnewuserobj = {};

//        this.AddNewUser = function () {

//            if (isNaN($scope.addnewuserobj.phoneNum)) {
//                $scope.showMessage = true;
//                $scope.msg = $scope.getErrMsg("Please enter a valid Phone Number");
//            } else {
//                $scope.showMessage = false;
//                $scope.msg = '';
//            }
//            $scope.addnewuserobj.userType = userService.getUserType();
//            $scope.addnewuserobj.username = $scope.addnewuserobj.email;
//            $scope.addnewuserobj.companyName = $scope.userObj.institution;
//            $scope.addnewuserobj.currency = $scope.addnewuserobj.currency.key;
//            userService.addnewuser($scope.addnewuserobj)
//                .then(function (response) {
//                    if (response.errorMessage != "") {
//                        growlService.growl(response.errorMessage, "inverse");
//                    } else {
//                        growlService.growl("User added successfully.", "inverse");
//                        this.addUser = 0;
//                        $state.reload();
//                    }
//                });

//        };



//        $scope.vendorsList = [];
//        $scope.inactiveVendorsList = [];

//        $scope.GetCompanyVendors = function () {
//            $scope.isOTPVerified = loginUserData.isOTPVerified;
//            $scope.isEmailOTPVerified = loginUserData.isEmailOTPVerified;
//            $scope.params = { "userID": userService.getUserId(), "sessionID": userService.getUserToken() }

//            if ($scope.isOTPVerified && $scope.isEmailOTPVerified) {
//                userService.GetCompanyVendors($scope.params)
//                    .then(function (response) { 
//                        $scope.vendorsList = $filter('filter')(response, { isValid: true });
//                        $scope.totalVendors = $scope.vendorsList.length;
//                        $scope.inactiveVendorsList = $filter('filter')(response, { isValid: false });
//                        $scope.totalInactiveVendors = $scope.inactiveVendorsList.length;
//                    });
//            }
//        }

//        $scope.GetCompanyVendors();


//        $scope.activatecompanyvendor = function (vendorID, isValid) {
//            userService.activatecompanyvendor({ "customerID": userService.getUserId(), "vendorID": vendorID, "isValid": isValid })
//                .then(function (response) {
//                    if (response.errorMessage != "") {
//                        growlService.growl(response.errorMessage, "inverse");
//                    }
//                    else if (response.errorMessage == "") {
//                        $scope.GetCompanyVendors();
//                        if (isValid == 0) {
//                            growlService.growl("Vendor deleted Successfully", "inverse");
//                        }
//                        else if (isValid == 1) {
//                            growlService.growl("Vendor Activated Successfully", "success");
//                        }
//                    }
//                });
//        }



//        //$scope.NegotiationSettingsValidationMessage = '';

//        //$scope.NegotiationTimeValidation = function (days, hours, mins, minReduction, rankComparision) {
//        //    $scope.NegotiationSettingsValidationMessage = '';

//        //    if (minReduction < 10 || minReduction == undefined) {
//        //        $scope.NegotiationSettingsValidationMessage = 'Set Min 10 for Min.Amount reduction';
//        //        return;
//        //    }

//        //    if (rankComparision < 10 || rankComparision == undefined) {
//        //        $scope.NegotiationSettingsValidationMessage = 'Set Min 10 for Rank Comparision price';
//        //        return;
//        //    }

//        //    if (minReduction >= 10 && rankComparision > minReduction) {
//        //        $scope.NegotiationSettingsValidationMessage = 'Please enter Valid Rank Comparision price less than Min. Amount reduction';
//        //        return;
//        //    }
//        //    if (days == undefined || days < 0) {
//        //        $scope.NegotiationSettingsValidationMessage = 'Please Enter Valid Days';
//        //        return;
//        //    }
//        //    if (hours < 0) {
//        //        $scope.NegotiationSettingsValidationMessage = 'Please Enter Valid Hours';
//        //        return;
//        //    }
//        //    if (mins < 0) {
//        //        $scope.NegotiationSettingsValidationMessage = 'Please Enter Valid Minunts';
//        //        return;
//        //    }
//        //    if (mins >= 60 || mins == undefined) {
//        //        $scope.NegotiationSettingsValidationMessage = 'Please Enter Valid Minunts';
//        //        return;
//        //    }
//        //    if (hours > 24 || hours == undefined) {
//        //        $scope.NegotiationSettingsValidationMessage = 'Please Enter Valid Hours';
//        //        return;
//        //    }
//        //    if (hours == 24 && mins > 0) {
//        //        $scope.NegotiationSettingsValidationMessage = 'Please Enter Valid Hours & Minuts';
//        //        return;
//        //    }
//        //    if (mins < 5 && hours == 0 && days == 0) {
//        //        $scope.NegotiationSettingsValidationMessage = 'Please Enter Min 5 Minutes';
//        //        return;
//        //    }
//        //}


//        //$scope.NegotiationSettings = [];

//        //$scope.NegotiationSettings.negotiationDuration = $scope.days + ' ' + $scope.hours + ':' + $scope.mins + ':00.0';

//        //$scope.saveNegotiationSettings = function () {
//        //    $scope.NegotiationTimeValidation($scope.days, $scope.hours, $scope.mins, $scope.NegotiationSettings.minReductionAmount, $scope.NegotiationSettings.rankComparision);

//        //    if ($scope.NegotiationSettingsValidationMessage == '') {
//        //        $scope.NegotiationSettings.negotiationDuration = $scope.days + ' ' + $scope.hours + ':' + $scope.mins + ':00.0';
//        //        var params = {
//        //            "NegotiationSettings": {
//        //                "userID": userService.getUserId(),
//        //                "minReductionAmount": $scope.NegotiationSettings.minReductionAmount,
//        //                "rankComparision": $scope.NegotiationSettings.rankComparision,
//        //                "negotiationDuration": $scope.NegotiationSettings.negotiationDuration,
//        //                "sessionID": userService.getUserToken(),
//        //                "compInterest": $scope.NegotiationSettings.compInterest,
//        //            }
//        //        };
//        //        $http({
//        //            method: 'POST',
//        //            url: domain + 'savenegotiationsettings',
//        //            encodeURI: true,
//        //            headers: { 'Content-Type': 'application/json' },
//        //            data: params
//        //        }).then(function (response) {
//        //            if (response && response.data && (response.data.errorMessage == "" || response.data.errorMessage == null)) {
//        //                $scope.NegotiationSettings = response.data;
//        //                growlService.growl('Successfully Saved', 'success');
//        //            }
//        //            else {
//        //                growlService.growl('Not Saved', 'inverse');
//        //            }
//        //        });
//        //    }
//        //}


//        $scope.AddVendorsExcel = function () {
//            var params = {
//                "entity": {
//                    attachment: $scope.ImportEntity.attachment,
//                    userid: parseInt(userService.getUserId()),
//                    entityName: 'AddVendor',
//                    sessionid: userService.getUserToken()
//                }
//            }

//            userService.AddVendorsExcel(params)
//                .then(function (response) {
//                    if (response.errorMessage == "") {
//                        swal({
//                            title: "Thanks!",
//                            text: "Vendors Added Successfully",
//                            type: "success",
//                            showCancelButton: false,
//                            confirmButtonColor: "#DD6B55",
//                            confirmButtonText: "Ok",
//                            closeOnConfirm: true
//                        },
//                            function () {
//                                location.reload();
//                            });


//                    } else {
//                        swal({
//                            title: "Failed!",
//                            text: response.errorMessage,
//                            type: "error",
//                            showCancelButton: false,
//                            confirmButtonColor: "#DD6B55",
//                            confirmButtonText: "Ok",
//                            closeOnConfirm: true
//                        },
//                            function () {
//                                location.reload();
//                            });
//                    }
//                })
//        }

//        $scope.showAccess = false;
//        $scope.UserName = '';

//        $scope.loadAccess = function (action, userID, userName) {
//            $scope.subuserentitlements = [];
//            $scope.showAccess = action;
//            $scope.UserName = userName;
//            if (action) {
//                userService.getuseraccess(userID, userService.getUserToken())
//                    .then(function (responseObj) {
//                        if (responseObj && responseObj.length > 0) {
//                            $scope.subuserentitlements = responseObj;
//                        }
//                    })
//            }
//        }



//        $scope.validateAccess = function (isEntitled, accessType) {
//            if (accessType == 'View Requirement' && isEntitled == false) {
//                $scope.subuserentitlements.forEach(function (item, index) {
//                    if (item.accessType == 'Requirement Posting') {
//                        item.isEntitled = false;
//                    }
//                    if (item.accessType == 'Quotations Verification') {
//                        item.isEntitled = false;
//                    }
//                    if (item.accessType == 'Negotiation Schedule') {
//                        item.isEntitled = false;
//                    }
//                    if (item.accessType == 'Live Negotiation (Bidding Process)') {
//                        item.isEntitled = false;
//                    }
//                    if (item.accessType == 'Po Generation') {
//                        item.isEntitled = false;
//                    }
//                })
//            }


//            if (accessType == 'Requirement Posting' && isEntitled == true) {
//                $scope.subuserentitlements.forEach(function (item, index) {
//                    if (item.accessType == 'View Requirement') {
//                        item.isEntitled = true;
//                    }
//                })
//            }


//            if (accessType == 'Quotations Verification' && isEntitled == true) {
//                $scope.subuserentitlements.forEach(function (item, index) {
//                    if (item.accessType == 'View Requirement') {
//                        item.isEntitled = true;
//                    }
//                    if (item.accessType == 'Requirement Posting') {
//                        item.isEntitled = true;
//                    }
//                })
//            }


//            if (accessType == 'Negotiation Schedule' && isEntitled == true) {
//                $scope.subuserentitlements.forEach(function (item, index) {
//                    if (item.accessType == 'View Requirement') {
//                        item.isEntitled = true;
//                    }
//                    if (item.accessType == 'Requirement Posting') {
//                        item.isEntitled = true;
//                    }

//                    if (item.accessType == 'Quotations Verification') {
//                        item.isEntitled = true;
//                    }
//                })
//            }


//            if (accessType == 'Live Negotiation (Bidding Process)' && isEntitled == true) {
//                $scope.subuserentitlements.forEach(function (item, index) {
//                    if (item.accessType == 'View Requirement') {
//                        item.isEntitled = true;
//                    }
//                    if (item.accessType == 'Requirement Posting') {
//                        item.isEntitled = true;
//                    }

//                    if (item.accessType == 'Quotations Verification') {
//                        item.isEntitled = true;
//                    }
//                    if (item.accessType == 'Negotiation Schedule') {
//                        item.isEntitled = true;
//                    }
//                })
//            }


//            if (accessType == 'Po Generation' && isEntitled == true) {
//                $scope.subuserentitlements.forEach(function (item, index) {
//                    if (item.accessType == 'View Requirement') {
//                        item.isEntitled = true;
//                    }
//                    if (item.accessType == 'Requirement Posting') {
//                        item.isEntitled = true;
//                    }

//                    if (item.accessType == 'Quotations Verification') {
//                        item.isEntitled = true;
//                    }
//                    if (item.accessType == 'Negotiation Schedule') {
//                        item.isEntitled = true;
//                    }
//                    if (item.accessType == 'Live Negotiation (Bidding Process)') {
//                        item.isEntitled = true;
//                    }
//                })
//            }
//        }


//        $scope.saveUserAccess = function () {
//            var params = {
//                "listUserAccess": $scope.subuserentitlements,
//                "sessionID": userService.getUserToken()
//            };

//            userService.saveUserAccess(params)
//                .then(function (response) {
//                    if (response.errorMessage != '') {
//                        growlService.growl(response.errorMessage, "inverse");
//                    }
//                    else {

//                        growlService.growl("Access Levels Defined Successfully.", "success");
//                        $scope.showAccess = false;
//                    }
//                })
//        };



//        $scope.showUserDepartments = false;
//        $scope.UserName = '';

//        $scope.loadUserDepartments = function (action, userID, userName) {
//            $scope.subUserDepartments = [];
//            $scope.showUserDepartments = action;
//            $scope.UserName = userName;
//            if (action) {
//                auctionsService.GetUserDepartments(userID, userService.getUserToken())
//                    .then(function (response) {
//                        if (response && response.length > 0) {
//                            $scope.subUserDepartments = response;
//                        }
//                    })
//            }
//        }


//        $scope.SaveUserDepartments = function () {

//            var params = {
//                "listUserDepartments": $scope.subUserDepartments,
//                "sessionID": userService.getUserToken()
//            };

//            auctionsService.SaveUserDepartments(params)
//                .then(function (response) {
//                    if (response.errorMessage != '') {
//                        growlService.growl(response.errorMessage, "inverse");
//                    }
//                    else {

//                        growlService.growl("Access Levels Defined Successfully.", "success");
//                        $scope.showUserDepartments = false;
//                    }
//                })
//        };




















//        $scope.goToEvaluation = function (auctionDetails) {
//            if ($scope.isCustomer == "CUSTOMER") {
//                $state.go("techeval", { "reqID": auctionDetails.requirementID, "evalID": 0 });
//            }
//            else {
//                $state.go("techevalresponses", { "reqID": auctionDetails.requirementID, "vendorID": userService.getUserId() });
//            }

//        };



//        $scope.GetMyDepartments = function () {
            
//            auctionsService.GetUserDepartments(userService.getUserId(), userService.getUserToken())
//                    .then(function (response) {
//                        if (response && response.length > 0) {
//                            $scope.myDepartments = response;
//                            $scope.myDepartments = _.filter($scope.myDepartments, function (x) { return x.isValid == true; });                            
//                        }
//                    })
//        }

//        $scope.GetMyDepartments();

















        

        


















//        var self = this;
       
//        $scope.addPhone = function () {
//            $scope.phonesLength = $scope.multiplePhones.length;
//            $scope.singlePhone =
//                {
//                    contactNo: '',
//                    phoneSNo: $scope.phoneSNo++
//                }
//            $scope.multiplePhones.push($scope.singlePhone);
//        };

//        $scope.deletePhone = function (phoneSNo) {
//            $scope.phonesLength = $scope.multiplePhones.length;
//            //if ($scope.phonesLength > 1) {
//                $scope.multiplePhones = _.filter($scope.multiplePhones, function (x) { return x.phoneSNo !== phoneSNo; });
//            //}
//        };

//        $scope.phoneSNo = 1;
//        $scope.multiplePhones = [];
//        $scope.singlePhone =
//            {
//                contactNo: '',
//                phoneSNo: $scope.phoneSNo++
//            }
//        $scope.phones = [];
//        $scope.editPhones = function (altPhoneNum) {
//            $scope.phones = [];
//            $scope.multiplePhones = [];
//            $scope.phones = altPhoneNum.split(",");

//            if ($scope.phones.length > 0) {
//                $scope.phones.forEach(function (item, index) {
//                    $scope.singlePhone =
//                       {
//                           contactNo: item,
//                           phoneSNo: $scope.phoneSNo++
//                       }                    
//                    $scope.multiplePhones.push($scope.singlePhone);
//                });
//            }
//        }

//        $scope.SaveMultiPhones = function () {

//            $scope.phoneError = false;

//            $scope.multiplePhones.forEach(function (item, index) {
//                item.error = "";
//                if (item.contactNo.toString() == "") {
//                    item.error = "Phone Number cannot be empty.";
//                    $scope.phoneError = true;
//                }
//                else if (isNaN(item.contactNo)) {
//                    item.error = "Please Enter correct Mobile number.";
//                    $scope.phoneError = true;
//                }
//                else if (item.contactNo.toString().length != 10) {
//                    item.error = "Phone Number Must be 10 digits.";
//                    $scope.phoneError = true;
//                }
//            });

//            if (!$scope.phoneError){
//                var phone = "";
//                if ($scope.multiplePhones && $scope.multiplePhones.length > 0) {
//                    $scope.multiplePhones.forEach(function (item, index) {
//                        if (item.contactNo == undefined) {
//                            item.contactNo = '';
//                        };
//                        phone += item.contactNo + ',';
//                    });
//                    phone = phone.slice(0, -1);
//                };
//                $scope.userObj.altPhoneNum = phone;
//                $(function () {
//                    $('#multiplePhones').modal('toggle');
//                });
//                self.updateUserInfo();
                
//            }
//        };







//        $scope.addEmail = function () {
//            $scope.emailsLength = $scope.multipleEmails.length;
//            $scope.singleEmail =
//                       {
//                           email: '',
//                           emailSNo: $scope.emailSNo++
//                       }
//            $scope.multipleEmails.push($scope.singleEmail);
//        };

//        $scope.deleteEmail = function (emailSNo) {
//            $scope.emailsLength = $scope.multipleEmails.length;
//            //if ($scope.emailsLength > 1) {
//                $scope.multipleEmails = _.filter($scope.multipleEmails, function (x) { return x.emailSNo !== emailSNo; });
//            //}
//        };

//        $scope.emailSNo = 1;
//        $scope.multipleEmails = [];
//        $scope.singleEmail =
//            {
//                email: '',
//                emailSNo: $scope.emailSNo++
//            }
//        $scope.emails = [];

//        $scope.editEmails = function (altEmail) {
//            $scope.emails = [];
//            $scope.multipleEmails = [];
//            $scope.emails = altEmail.split(",");

//            if ($scope.emails.length > 0) {
//                $scope.emails.forEach(function (item, index) {
//                    $scope.singleEmail =
//                       {
//                           email: item,
//                           emailSNo: $scope.emailSNo++
//                       }
//                    $scope.multipleEmails.push($scope.singleEmail);
//                });
//            }
//        }

//        $scope.SaveMultiEmails = function () {

//            $scope.emailError = false;

//            $scope.multipleEmails.forEach(function (item, index) {
//                item.error = "";
//                if (item.email.toString() == "") {
//                    item.error = "Email cannot be empty.";
//                    $scope.emailError = true;
//                }                
//            });

//            if (!$scope.emailError) {

//                var email = "";
//                if ($scope.multipleEmails && $scope.multipleEmails.length > 0) {
//                    $scope.multipleEmails.forEach(function (item, index) {
//                        if (item.email == undefined) {
//                            item.email = '';
//                        };
//                        email += item.email + ',';
//                    });
//                    email = email.slice(0, -1);
//                };
//                $scope.userObj.altEmail = email;
//                $(function () {
//                    $('#multipleEmails').modal('toggle');
//                });
//                self.updateUserInfo();
//            }
//        };

        

//        $scope.AssignVendorToCompany = function (vendorPhone, vendorEmail) {

//            if ($scope.newVendor.altPhoneNum == undefined)
//            {
//                $scope.newVendor.altPhoneNum = '';
//            }
//            if ($scope.newVendor.altEmail == undefined)
//            {
//                $scope.newVendor.altEmail = '';
//            }

//            var params = {
//                userID: userService.getUserId(),
//                vendorPhone: vendorPhone,
//                vendorEmail: vendorEmail,
//                sessionID: userService.getUserToken(),
//                category: $scope.newVendor.category,
//                subCategory: $scope.selectedSubCategoriesList,
//                altPhoneNum: $scope.newVendor.altPhoneNum,
//                altEmail: $scope.newVendor.altEmail
//            };

//            auctionsService.AssignVendorToCompany(params)
//                .then(function (response) {

//                    $scope.vendor = response;

//                    if ($scope.vendor.errorMessage == '') {                                                
//                        swal({
//                            title: "Done!",
//                            text: "Vendor Added Successfully",
//                            type: "success",
//                            showCancelButton: false,
//                            confirmButtonColor: "#DD6B55",
//                            confirmButtonText: "Ok",
//                            closeOnConfirm: true
//                        },
//                        function () {
//                            location.reload();
//                        });
//                    }
//                    else {                        
//                        swal({
//                            title: "Done!",
//                            text: $scope.vendor.errorMessage,
//                            type: "warning",
//                            showCancelButton: false,
//                            confirmButtonColor: "#DD6B55",
//                            confirmButtonText: "Ok",
//                            closeOnConfirm: true
//                        },
//                        function () {
//                            location.reload();
//                        });
//                    }
//                });
//        };






//        $scope.goToReqReport = function (reqID) {
//            if ($scope.isCustomer == "CUSTOMER") {
//                //$state.go("reports", { "reqID": reqID });

//                var url = $state.href('reports', { "reqID": reqID });
//                $window.open(url, '_blank');

//            }
//        };



//        $scope.categoriesList = [];

//        $scope.categoriesListLength = $scope.categoriesList.length;

//        $scope.categoryObj = {
//            id: 0,
//            category: '',
//            subcategory: ''
//        };


//        $scope.editCategoriesFunction = function (category, value) {
//            category.editCategories = value;
//        };
        
//        $scope.addCatToArray = function (categoryObj)
//        {
//            categoryObj.compID = userService.getUserCompanyId();
//            categoryObj.sessionID = userService.getUserToken();

//            $scope.categoriesList.push(categoryObj);

//            $scope.categoriesListLength = $scope.categoriesList.length;


//            auctionsService.SaveCompanyCategories(categoryObj)
//                .then(function (response) {

//                    $scope.catResponse = response;

//                    if ($scope.catResponse.errorMessage == '') {
//                        swal({
//                            title: "Done!",
//                            text: "Category Saved Successfully",
//                            type: "success",
//                            showCancelButton: false,
//                            confirmButtonColor: "#DD6B55",
//                            confirmButtonText: "Ok",
//                            closeOnConfirm: true
//                        },
//                        function () {
//                            //location.reload();
//                        });
//                    }
//                    else {
//                        swal({
//                            title: "Done!",
//                            text: $scope.catResponse.errorMessage,
//                            type: "warning",
//                            showCancelButton: false,
//                            confirmButtonColor: "#DD6B55",
//                            confirmButtonText: "Ok",
//                            closeOnConfirm: true
//                        },
//                        function () {
//                            //location.reload();
//                        });
//                    }
//                });


//            $scope.categoryObj = {
//                id: 0,
//                category: categoryObj.categoryName,
//                subcategory: ''
//            };
//        }




//        $scope.GetCompanyCategories = function () {

//            auctionsService.GetCompanyCategories(userService.getUserCompanyId(), userService.getUserToken())
//                    .then(function (response) {
//                        if (response && response.length > 0) {
//                            $scope.categoriesList = response;
                            
//                        }
//                    })
//        }

//        $scope.GetCompanyCategories();



//        $scope.goTocatergory = function () {
            
//            var url = $state.href('category', {});
//                $window.open(url, '_blank');
//        };

//        $scope.goToVendorDetails = function (vendorID) {
//            var url = $state.href('viewProfile', { "Id": vendorID });
//            $window.open(url, '_blank');
//        }



//        $scope.goToworkflows = function () {
//            var url = $state.href('workflow', {});
//            $window.open(url, '_blank');
//        };







//        $scope.downloadTemplate = function () {
//            reportingService.downloadTemplate('addvendors', userService.getUserId())
//                .then(function (response) {
//                    if (response) {
//                        var linkElement = document.createElement('a');
//                        try {
//                            //var blob = b64toBlob(response, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");//new Blob([response], { type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" });
//                            var url = $window.URL.createObjectURL(response);

//                            linkElement.setAttribute('href', url);
//                            linkElement.setAttribute("download", "AddVendor.xlsx");

//                            var clickEvent = new MouseEvent("click", {
//                                "view": window,
//                                "bubbles": true,
//                                "cancelable": false
//                            });
//                            linkElement.dispatchEvent(clickEvent);
//                        }
//                        catch (ex) { }
//                    }
//                });
//        }













//        $scope.showUserDesignations = false;
//        $scope.UserName = '';

//        $scope.loadUserDesignations = function (action, userID, userName) {
//            $scope.subUserDesignations = [];
//            $scope.showUserDesignations = action;
//            $scope.UserName = userName;
//            if (action) {
//                auctionsService.GetUserDesignations(userID, userService.getUserToken())
//                    .then(function (response) {
//                        if (response && response.length > 0) {
//                            $scope.subUserDesignations = response;
//                        }
//                    })
//            }
//        }



//        $scope.SaveUserDesignations = function () {

//            var params = {
//                "listUserDesignations": $scope.subUserDesignations,
//                "sessionID": userService.getUserToken()
//            };

//            auctionsService.SaveUserDesignations(params)
//                .then(function (response) {
//                    if (response.errorMessage != '') {
//                        growlService.growl(response.errorMessage, "inverse");
//                    }
//                    else {

//                        growlService.growl("Access Levels Defined Successfully.", "success");
//                        $scope.showUserDesignations = false;
//                    }
//                })
//        };



//        $scope.GetMyDesignations = function () {

//            auctionsService.GetUserDesignations(userService.getUserId(), userService.getUserToken())
//                    .then(function (response) {
//                        if (response && response.length > 0) {
//                            $scope.myDesignations = response;
//                            $scope.myDesignations = _.filter($scope.myDesignations, function (x) { return x.isValid == true; });
//                        }
//                    })
//        }

//        //$scope.GetMyDesignations();











//        $scope.UserDeptDesig = [];

//        $scope.GetUserDeptDesig = function (action, userID, userName) {

//            $scope.vendorID = userID;

//            $scope.subUserDeptDesig = [];
//            $scope.showUserDeptDesig = action;
//            $scope.UserName = userName;
//            if (action) {
//                auctionsService.GetUserDeptDesig(userID, userService.getUserToken())
//                    .then(function (response) {
//                        if (response && response.length > 0) {
//                            $scope.UserDeptDesig = response;

//                            console.log($scope.UserDeptDesig);

//                        }
//                    })
//            }
//        }



//        $scope.GetUserDeptDesig();

        $scope.GetUserDepartmentDesignations = function (action, userID, userName) {

            $scope.vendorID = userID;

            $scope.subUserDeptDesig = [];
            $scope.showUserDeptDesig = action;
            $scope.UserName = userName;
            if (action) {
                auctionsService.GetUserDepartmentDesignations(userID, userService.getUserToken())
                    .then(function (response) {
                        if (response && response.length > 0) {
                            $scope.UserDeptDesig = response;
                        }
                    })
            }
        };
       

//        $scope.SaveUserDeptDesig = function (UserDeptDesig) {

//            var params = {
//                "listUserDeptDesig": UserDeptDesig,
//                "sessionID": userService.getUserToken()
//            };

//            auctionsService.SaveUserDeptDesig(params)
//                .then(function (response) {
//                    if (response.errorMessage != '') {
//                        growlService.growl(response.errorMessage, "inverse");
//                    }
//                    else {

//                        growlService.growl("Saved Successfully.", "success");
//                        $scope.showUserDeptDesig = false;
//                    }
//                })
//        };


//        $scope.showUserDeptDesig = false;
//        $scope.UserName = '';

//        $scope.loadUserDeptDesig = function (action, userID, userName) {
//            $scope.subUserDeptDesig = [];
//            $scope.showUserDeptDesig = action;
//            $scope.UserName = userName;
//            if (action) {
//                auctionsService.GetUserDeptDesig(userID, userService.getUserToken())
//                    .then(function (response) {
//                        if (response && response.length > 0) {
//                            $scope.subUserDeptDesig = response;
//                        }
//                    })
//            }
//        }

    });
