﻿prmApp
 .controller('profilePwdCtrl', ["$timeout", "$uibModal", "$state", "$window", "$scope", "growlService", "userService", "auctionsService", "fwdauctionsService", "$http", "domain", "$rootScope", "fileReader", "$filter", "$log", "reportingService",
        function ($timeout, $uibModal, $state, $window, $scope, growlService, userService, auctionsService, fwdauctionsService, $http, domain, $rootScope, fileReader, $filter, $log, reportingService) {
            $scope.isCustomer = userService.getUserType();
            $scope.sessionid = userService.getUserToken();
            $scope.userObj = {};
            userService.getUserDataNoCache()
                .then(function (response) {
                    $scope.userObj = response;
                })

            $scope.userObj = userService.getUserObj();
            $scope.uId = userService.getUserId();

            this.updatePwd = function () {
                if ($scope.pwdObj.newPass.length < 6) {
                    growlService.growl("Password should be at least 6 characters long.", "inverse");
                    return false;
                } else if ($scope.pwdObj.newPass != $scope.pwdObj.confirmNewPass) {
                    growlService.growl('Passwords do not match, please enter the same password in both new and Confirm Password fields', 'inverse');
                    return false;
                }


                if ($scope.userObj.subUserPassword !== $scope.pwdObj.oldPass) {
                    $scope.ispassword = true;
                    $scope.pwdObj.oldPass = '';
                    swal("Warning!", 'Old Password Doesn\'t match .');
                    return false;
                }

                $scope.pwdObj.userID = parseInt(userService.getUserId());
                $scope.pwdObj.username = $scope.userObj.phoneNum;
                userService.updatePassword($scope.pwdObj)
                    .then(function (response) {
                        if (!response.errorMessage) {
                            $scope.pwdObj = {
                                username: userService.getUserObj().username
                            };
                            swal("Done!", 'Your password has been successfully updated, please re-login with new credentials.', 'success');
                            userService.logout();
                            //location.reload();

                        } else {
                            swal("Error!", response.errorMessage, 'error');
                        }
                    });
            };



            $scope.ispassword = false;
            $scope.checkpassword = function () {

                if ($scope.userObj.phoneNum == $scope.pwdObj.newPass) {
                    $scope.ispassword = true;
                    $scope.pwdObj.newPass = '';
                    swal("Warning!", 'Phone and Password can not be same.');
                }
                else {
                    $scope.ispassword = false;
                }
            };

            $(".toggle-password").click(function () {
                $(this).toggleClass("fa-eye fa-eye-slash");
                var input = $($(this).attr("toggle"));
                if (input.attr("type") == "password") {
                    input.attr("type", "text");
                } else {
                    input.attr("type", "password");
                }
            });





        }
  ]);