﻿prmApp.
    controller('incoTermsCtrl', function ($scope, growlService, userService, $filter, $log, auctionsService, $state) {
        $scope.isCustomer = userService.getUserType();
        $scope.sessionid = userService.getUserToken();
        $scope.companyID = userService.getUserCompanyId();


        /*pagination code*/
        $scope.totalItems = 0;
        $scope.totalVendors = 0;
        $scope.totalincoTerm = 0;
        $scope.totalInactiveVendors = 0;
        $scope.totalLeads = 0;
        $scope.currentPage = 1;
        $scope.currentPage2 = 1;
        $scope.itemsPerPage = 10;
        $scope.itemsPerPage2 = 10;
        $scope.maxSize = 10;

        $scope.incoTermObj = {};
        $scope.incoTerms = [];
        $scope.inactiveincoTerms = [];
        $scope.editIncoTermForm = false;

        $scope.incoTerm = {
            userID: $scope.userID,
            sessionID: $scope.sessionID,
            freight: '',
            packaging: '',
            mislaneous: '',
            insurance: ''

        }


        //togetincoTermdata
        $scope.getIncoTermData = function () {
            auctionsService.getIncoTermsData({ "sessionid": userService.getUserToken() })
                .then(function (response) {
                    $scope.incoTerms = $filter('filter')(response, { isValid: true });

                    $scope.inactiveIncoTerms = $filter('filter')(response, { isValid: false });

                    
                    $scope.totalItems = $scope.incoTerms.length;
                    $scope.incoTerms1 = $filter('filter')(response, { isValid: true });
                });
        };

        //$scope.getincoTermData();

        
        $scope.deleteIncoTerm = function (incoTermID) {
            userService.deleteIncoTerm({ "incoTermID": incoTermID })
                .then(function (response) {
                    if (response.errorMessage != "") {
                        growlService.growl(response.errorMessage, "inverse");
                    } else {
                        growlService.growl("incoTerm deactivated Successfully", "inverse");
                        $scope.getIncoTermData();
                    }
                });
        };


        $scope.activateIncoTerm = function (incoTermID) {
            userService.activateIncoTerm({ "incoTermID": incoTermID })
                .then(function (response) {
                    if (response.errorMessage != "") {
                        growlService.growl(response.errorMessage, "inverse");
                    } else {
                        growlService.growl("incoTerm added Successfully", "success");
                        $scope.getIncoTermData();
                    }
                });
        };

        $scope.editIncoTerm = function (incoTerm) {
            $scope.incoTerm = incoTerm;
            $scope.editIncoTermForm = true;
        };

        


        $scope.saveIncoTerms = function () {

            var params = {
                "incoTerm": $scope.incoTerm
            };

            auctionsService.saveIncoterm(params)
                .then(function (response) {
                    if (response.errorMessage != '') {
                        growlService.growl(response.errorMessage, "inverse");
                    }
                    else {
                        growlService.growl("Incoterm Saved Successfully.", "success");
                        $scope.getIncoTermData();
                        $scope.editIncoTermForm = false;
                        $scope.incoTerm = {
                            userID: $scope.userID,
                            sessionID: $scope.sessionID,
                            freight: '',
                            packaging: '',
                            mislaneous: '',
                            insurance: '',
                        };
                        //$window.history.back();
                    }
                });

        };

        $scope.closeIncoTerm = function () {
            $scope.editIncoTermForm = false;
            $scope.incoTerm = {
                userID: $scope.userID,
                sessionID: $scope.sessionID,
                freight: '',
                packaging: '',
                mislaneous: '',
                insurance: ''

            }
        }



    });