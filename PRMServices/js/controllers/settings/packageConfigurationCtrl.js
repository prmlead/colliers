﻿prmApp
    .controller('packageConfigurationCtrl', ["$scope", "userService", "auctionsService", "growlService", "$http", "domain",
        "$location", "$anchorScroll",
        function ($scope, userService, auctionsService, growlService, $http, domain,  $location, $anchorScroll) {

            $scope.userID = userService.getUserId();
            $scope.sessionID = userService.getUserToken();
            $scope.compID = userService.getUserCompanyId();
            $scope.currencyFactors = [];
            $scope.currencyFactorAudits = [];
            $scope.displayCard = 0;
            $scope.minDateMoment = moment().subtract(0, 'day');
            $scope.currency = {
                userID: $scope.userID,
                sessionID: $scope.sessionID,
                currencyFactorID: 0,
                currencyCode: '',
                compID: $scope.compID,
                currencyFactor: '',
                startDate: '',
                endDate: '',
                fromDate: '',
                toDate: ''
            };

            $scope.expand = false;

            /*PAGINATION CODE*/
            $scope.totalItems = 0;
            $scope.totalItemAudits = 0;
            $scope.currentPage = 1;
            $scope.currentPage1 = 1;
            $scope.itemsPerPage = 10;
            $scope.maxSize = 5;

            $scope.isCurrEdit = {
                isEdit: 0
            }

            $scope.setPage = function (pageNo) {
                $scope.currentPage = pageNo;
            };


            $scope.pageChanged = function () {
                ////console.log('Page changed to: ' + $scope.currentPage);
            };
            /*PAGINATION CODE*/


            $scope.currencies = [];
            $scope.addNewView = false;


            $scope.data = [{
                'nodes': [
                    {
                        ID: 1,
                        PACKAGE: "CIVIL INTERIORS",
                    },
                    {
                        ID: 2,
                        PACKAGE: "ELECTRICAL",
                    },
                    {
                        ID: 3,
                        PACKAGE: "HVAC",
                    },
                    {
                        ID: 4,
                        PACKAGE: "PAC",
                    },
                    {
                        ID: 5,
                        PACKAGE: "FDAPA",
                    },
                    {
                        ID: 6,
                        PACKAGE: "CCTV & ACS",
                    }
                ]
            }];


            $scope.editPackageConfiguration = function (packageConfig,isEdit) {
                $scope.addNewView = true;
                if (isEdit == 1) {
                    $scope.packageName = packageConfig;                   
                } else if (isEdit == 0) {
                    $scope.addNewView = false;
                    growlService.growl("Package Saved Successfully.", "success");
                }
                document.body.scrollTop = 0; // For Chrome, Safari and Opera 
                document.documentElement.scrollTop = 0; // For IE and Firefox
            };

            $scope.closePackage = function () {
                $scope.addNewView = false;

            };

            $scope.showPackage = function (index, visible) {
                var IDD = $scope.packageLists[index].ID;
                var PACKAGES = $scope.packageLists[index].PACKAGE;
                if (visible == 0 && IDD == index + 1 ) {
                    $scope.expand = true;
                } if (visible == 0 && IDD != index + 1) {
                    $scope.expand = false;
                } else if (visible == 1 && IDD == index + 1) {
                    $scope.expand = false;
                }

                /*alert("Id: " + IDD + "\nPackage: " + PACKAGES);*/
            }
            function scrollToElement(elementId) {
                $location.hash(elementId);
                $anchorScroll();
            }






        }]);

