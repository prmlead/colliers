prmApp
    // =========================================================================
    // Header
    // =========================================================================
    .controller('headerCtrl', ["$timeout", "messageService", "$scope", "userService", "$rootScope", "$interval",
        function ($timeout, messageService, $scope, userService, $rootScope, $event, $interval) {

            //$scope.currentUserDisplayName = function () {
            //    return userService.getUserObj().firstName + ' ' + userService.getUserObj().lastName;
            //};

            $scope.currentUserDisplayName = function () {
                return userService.getUserObj().firstName + ' ' + userService.getUserObj().lastName;
            };
            $scope.currentUserDisplayNameLetter = function () {
                if (userService.getUserObj().firstName && userService.getUserObj().lastName) {
                    return userService.getUserObj().firstName.charAt(0) + userService.getUserObj().lastName.charAt(0);
                } else {
                    return '';
                }
            };
            $scope.currentUserEmail = function () {
                return userService.getUserObj().email;
            };

            $scope.companyDecimalRound = function () {
                return userService.getUserObj().companyRoundingDecimalSetting;
            };

            this.openSearch = function () {
                angular.element('#header').addClass('search-toggled');
                angular.element('#top-search-wrap').find('input').focus();
            };

            $scope.showAddNewReq = function () {
                if (window.location.hash != "#/login" && userService.getUserType() == "CUSTOMER") {
                    return true;
                } else {
                    return false;
                }
            };

            $scope.isCatalogueEnabled = function () {
                if (userService.getUserObj().isCatalogueEnabled) {
                    return true;
                } else {
                    return false;
                }
            };

            $scope.showIsUserVendor = function () {
                if (window.location.hash != "#/login" && userService.getUserType() == "VENDOR") {
                    return true;
                } else {
                    return false;
                }
            };

            $scope.showIsSuperUser = function () {
                if (userService.getUserObj().isSuperUser) {
                    return true;
                } else {
                    return false;
                }
            }



            this.closeSearch = function () {
                angular.element('#header').removeClass('search-toggled');
            }

            // Get messages and notification for header
            this.img = messageService.img;
            this.user = messageService.user;
            this.user = messageService.text;

            /* this.messageResult = messageService.getMessage(this.img, this.user, this.text);*/


            //Clear Notification
            this.clearNotification = function ($event) {
                $event.preventDefault();

                var x = angular.element($event.target).closest('.listview');
                var y = x.find('.lv-item');
                var z = y.size();

                angular.element($event.target).parent().fadeOut();

                x.find('.list-group').prepend('<i class="grid-loading hide-it"></i>');
                x.find('.grid-loading').fadeIn(1500);
                var w = 0;

                y.each(function () {
                    var z = $(this);
                    $timeout(function () {
                        z.addClass('animated fadeOutRightBig').delay(1000).queue(function () {
                            z.remove();
                        });
                    }, w += 150);
                })

                $timeout(function () {
                    angular.element('#notifications').addClass('empty');
                }, (z * 150) + 200);
            }

            // Clear Local Storage
            this.clearLocalStorage = function () {

                //Get confirmation, if confirmed clear the localStorage
                swal({
                    title: "Are you sure?",
                    text: "All your saved localStorage values will be removed",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#F44336",
                    confirmButtonText: "Yes, delete it!",
                    closeOnConfirm: false
                }, function () {
                    localStorage.clear();
                    swal("Done!", "localStorage is cleared", "success");
                });

            }

            //Fullscreen View
            this.fullScreen = function () {
                //Launch
                function launchIntoFullscreen(element) {
                    if (element.requestFullscreen) {
                        element.requestFullscreen();
                    } else if (element.mozRequestFullScreen) {
                        element.mozRequestFullScreen();
                    } else if (element.webkitRequestFullscreen) {
                        element.webkitRequestFullscreen();
                    } else if (element.msRequestFullscreen) {
                        element.msRequestFullscreen();
                    }
                }

                //Exit
                function exitFullscreen() {
                    if (document.exitFullscreen) {
                        document.exitFullscreen();
                    } else if (document.mozCancelFullScreen) {
                        document.mozCancelFullScreen();
                    } else if (document.webkitExitFullscreen) {
                        document.webkitExitFullscreen();
                    }
                }

                if (exitFullscreen()) {
                    launchIntoFullscreen(document.documentElement);
                }
                else {
                    launchIntoFullscreen(document.documentElement);
                }
            }


            $scope.tConvert = function (time) {
                // Check correct time format and split into components
                time = time.toString().match(/^([01]\d|2[0-3])(:)([0-5]\d)(:[0-5]\d)?$/) || [time];

                if (time.length > 1) { // If time format correct
                    time = time.slice(1);  // Remove full string match value
                    time[5] = +time[0] < 12 ? 'AM' : 'PM'; // Set AM/PM
                    time[0] = +time[0] % 12 || 12; // Adjust hours
                    time[0] = +time[0] < 10 ? "0" + time[0] : time[0]
                }
                return time.join(' '); // return adjusted time or original string
            }

            $(".open-close").click(function () {
                $("body").toggleClass("show-sidebar");
            });

            $scope.sideBarClick = function () {
                $("body").toggleClass("show-sidebar");
            }

            $scope.toDoCalander = false;
            $scope.myStoryBoard = false;

            $scope.myDate = function (data) {
                data = data / 1000.0;
                var thisDate = "/Date(" + data + "000+0530)/";
                var todayDateTime = new Date();
                var timeDateStamp = new moment(thisDate).format("DD-MM-YYYY") + " " + todayDateTime.getHours() + ":" + todayDateTime.getMinutes() + ":00";
                timeDateStamp = userService.toUTCTicks(timeDateStamp) / 1000.0;
                timeDateStamp = "/Date(" + timeDateStamp + "000+0530)/";

                //console.log(new moment(timeDateStamp).format("DD-MM-YYYY H:mm"));
                //return false;
                var params = {
                    myDate: timeDateStamp,
                    userId: userService.getUserId(),
                    userType: userService.getUserType()
                }
                params.myDate = new moment(params.myDate).format("YYYY-MM-DD");
                userService.getToDoData(params)
                    .then(function (response) {
                        if (response) {


                            response = response.filter(function (item) {
                                return !String(item.startTime).includes('9999');
                            })
                            response.forEach(function (item, index) {

                                //var quotationFreezTime = new moment(item.quotationFreezTime).format("DD-MM-YYYY");
                                //quotationFreezTime = userService.toLocalDate(quotationFreezTime);
                                //quotationFreezTime = new moment(quotationFreezTime).format("DD-MM-YYYY");


                                //var startTime = new moment(item.startTime).format("DD-MM-YYYY");
                                //startTime = userService.toLocalDate(startTime);
                                //startTime = new moment(startTime).format("DD-MM-YYYY");


                                //var todayDate = new moment(timeDateStamp).format("DD-MM-YYYY");
                                //todayDate = userService.toLocalDate(todayDate);
                                //todayDate = new moment(todayDate).format("DD-MM-YYYY");

                                var quotationFreezTime = userService.toLocalDate(item.quotationFreezTime).split(' ')[0];
                                var startTime = userService.toLocalDate(item.startTime).split(' ')[0];
                                var todayDate = userService.toLocalDate(timeDateStamp).split(' ')[0];


                                //console.log(quotationFreezTime + "------" + startTime + "-------" + todayDate);
                                if (quotationFreezTime == todayDate) {
                                    item.quotationFreezTime = userService.toLocalDate(item.quotationFreezTime);
                                    myTime = item.quotationFreezTime.split(" ");
                                    item.quotationFreezTime = $scope.tConvert(myTime[1]);

                                } else {
                                    item.quotationFreezTime = "";
                                }

                                if (startTime == todayDate) {
                                    item.startTime = userService.toLocalDate(item.startTime);
                                    myTime = item.startTime.split(" ");
                                    item.startTime = $scope.tConvert(myTime[1]);
                                } else {
                                    item.startTime = "";
                                }
                                if (item.status == 'UNCONFIRMED') {
                                    item.status = 'Quotations pending';
                                    item.statusColor = 'text-custom';
                                }

                                else if (item.status == 'Negotiation Ended') {
                                    item.status = 'Negotiation Closed';
                                    item.statusColor = 'text-success';
                                }
                                else if (item.status == 'NOTSTARTED') {
                                    item.status = 'Negotiation scheduled';
                                    item.statusColor = 'text-warning';
                                }
                                else if (item.status == 'STARTED') {
                                    item.status = 'Negotiation Started';
                                    item.statusColor = 'text-danger';
                                }
                                else if (item.status == 'DELETED') {
                                    item.status = 'Negotiation Cancelled';
                                    item.statusColor = 'text-dark';
                                }
                                //else if (item.status == 'Qcs Pending') {
                                //    item.status = 'QCS Pending';
                                //    item.statusColor = 'text-dark';
                                //}
                                //else if (item.status == 'Qcs Created') {
                                //    item.status = 'QCS Created';
                                //    item.statusColor = 'text-dark';
                                //}
                                //else if (item.status == 'Qcs Approved') {
                                //    item.status = 'QCS Approved';
                                //    item.statusColor = 'text-dark';
                                //}
                                //else if (item.status == 'Qcs Approval Pending') {
                                //    item.status = 'QCS Approval Pending';
                                //    item.statusColor = 'text-dark';
                                //}
                                //else if (item.status == 'Quotations Received') {
                                //    item.status = 'Quotations Received';
                                //    item.statusColor = 'text-dark';
                                //}
                                //else if (item.status == 'Saved As Draft') {
                                //    item.status = 'Saved As Draft';
                                //    item.statusColor = 'text-dark';
                                //}
                                //else if (item.status == 'NEW') {
                                //    item.status = 'NEW'; // / WIP
                                //    item.statusColor = 'text-dark';
                                //}
                                //else if (item.status == 'WIP') {
                                //    item.status = 'WIP';
                                //    item.statusColor = 'text-dark';
                                //}
                                else {
                                    item.statusColor = '';
                                }
                            })
                            $scope.records = response;
                            console.log($scope.records.length)
                        }
                        else {
                            console.log(response.errorMessage, "inverse");
                        }

                    })
            }

            $scope.linkify = function (text) {
                var url = '';
                var urlRegex = /(\b(https?|ftp|file):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])/ig;
                text.replace(urlRegex, function (urlVal) {
                    url = '<br/>More details: <a target="_blank" href="' + urlVal + '">Click here</a>';
                    return '<br/>URL: <a target="_blank" href="' + urlVal + '">Click here for details</a>';
                });

                return url;
            };

            $scope.showUserNotifications = function () {
                $scope.userNotifications = [];
                $scope.notificationsCount = 0;
                console.log($scope.userNotifications)
                userService.getUserNotifications().then(function (response) {
                    if (response && response.length > 0) {
                        $scope.userNotifications = response;
                        console.log($scope.userNotifications)
                        $scope.notificationsCount = $scope.userNotifications.length;
                        $scope.userNotifications.forEach(function (item, index) {
                            item.messageDate = userService.toLocalDateFormat1(item.messageDate);
                            item.displayMessage = item.subject;
                            item.displayMessage += '<br/>Date & Time:<strong>' + item.messageDate + '</strong>';
                            item.displayMessage += $scope.linkify(item.message);

                        });
                        console.log($scope.userNotifacations)
                    } else {
                        var noNotification = {
                            displayMessage: 'No notifications to display.'
                        };

                        $scope.userNotifications.push(noNotification);
                        console.log($scope.userNotifications);
                        console.log($scope.notificationsCount);
                    }
                });
            };


            $scope.calanderData = function () {
                $scope.myStoryBoard = false;
                $scope.toDoCalander = $scope.toDoCalander ? false : true;
                $rootScope.isCalanderLoadFirstTime++;
                Date.prototype.getUnixTime = function () { return this.getTime() / 1000 | 0 };
                if ($scope.toDoCalander == true) {
                    $scope.myDate((new Date()).getUnixTime()*1000);
                }
            }

            angular.element(document).on('click', function () {
                //$scope.toDoCalander = false;
                //$scope.$apply();
            });

            if ($rootScope.isCalanderLoadFirstTime == 0 || $rootScope.isCalanderLoadFirstTime == 1) {
                //$scope.calanderData();
            }

            $scope.myPanel = parseInt(window.innerHeight - 110);

            $scope.timesRun = 0;
            $scope.myVar = "";
            $scope.barWidth = 0;
            $scope.countNewStry = 0;
            $scope.bgImg;
            $scope.createStory = false;
            $scope.viewStory = false;

            $scope.stryArrCnt = 0;

            $scope.dataRecords = [
                { "US_ID": "1", "U_NAME": "Rathan Reddy", "US_DESCRIPTION": "Hai this is Rathan", "US_ATTACHMENT": "rabbit.jpg", "DATE_CREATED": "2019-06-11 11:30:27", "status": "0" },
                { "US_ID": "2", "U_NAME": "Jay Krishna Chari", "US_DESCRIPTION": "Hai this is Jay KrishnaHai this is Jay Krishna Hai this is Jay KrishnaHai this is Jay KrishnaHai this is Jay KrishnaHai this is Jay KrishnaHai this is Jay KrishnaHai this is Jay KrishnaHai this is Jay KrishnaHai this is Jay KrishnaHai this is Jay KrishnaHai this is Jay KrishnaHai this is Jay KrishnaHai this is Jay KrishnaHai this is Jay KrishnaHai this is Jay KrishnaHai this is Jay KrishnaHai this is Jay KrishnaHai this is Jay KrishnaHai this is Jay KrishnaHai this is Jay Krishna", "US_ATTACHMENT": "dog.jpg", "DATE_CREATED": "2019-06-11 10:30:27", "status": "0" },
                { "US_ID": "3", "U_NAME": "Chetan Puranam", "US_DESCRIPTION": "Hay this is me", "US_ATTACHMENT": "download.jpg", "DATE_CREATED": "2019-06-10 13:30:27", "status": "0" },
                { "US_ID": "4", "U_NAME": "Meghana Reddy", "US_DESCRIPTION": "Hey this is meghana reddy", "US_ATTACHMENT": "gif.png", "DATE_CREATED": "2019-06-11 16:30:27", "status": "0" },
                { "US_ID": "5", "U_NAME": "Akshara Reddy", "US_DESCRIPTION": "Hai this is akshara reddy", "US_ATTACHMENT": "apple.png", "DATE_CREATED": "2019-06-12 11:30:27", "status": "0" },
                { "US_ID": "6", "U_NAME": "Srija", "US_DESCRIPTION": "", "US_ATTACHMENT": "fishpond.jpg", "DATE_CREATED": "2019-06-11 11:30:27", "status": "0" }
            ]

            $scope.stryArrCnt = $scope.dataRecords.length;

            $scope.zeroArrCnt = $scope.stryArrCnt;


            $scope.openNewStory = function (a) {

                $scope.myStoryBoard = true;
                $scope.toDoCalander = false;
                if (a == 1) {
                    $scope.viewStory = true;
                    $scope.createStory = false;
                    $scope.timesRun = 0;
                    $scope.barWidth = 0;
                    $scope.storyRecord = $scope.dataRecords[$scope.countNewStry];
                    $scope.dataRecords[$scope.countNewStry].status = 1;
                    $scope.bgImg = '/img/story/' + $scope.storyRecord["US_ATTACHMENT"];
                    $scope.myVar = setInterval(storyTimer, 100);
                    $scope.zeroArrCnt = 0;
                    $scope.countNewStry++;
                } else {
                    $scope.viewStory = false;
                    $scope.createStory = true;
                    clearTimeout($scope.myVar);
                }
            }

            function storyTimer() {
                $scope.timesRun++;
                $scope.barWidth = (100 / 100) * $scope.timesRun + '%';
                //console.log($scope.timesRun);
                $scope.$apply();
                if ($scope.timesRun === 101) {
                    clearTimeout($scope.myVar);
                    //console.log($scope.stryArrCnt + '--------' + $scope.countNewStry + '/////' + $scope.timesRun);
                    if ($scope.stryArrCnt === $scope.countNewStry) {
                        $scope.countNewStry = 0;
                        $scope.closeStory();

                    } else {
                        $scope.barWidth = 0;
                        $scope.$apply();
                        setTimeout(function () { $scope.openNewStory(1); }, 100);
                    }
                }
            }

            $scope.closeStory = function () {
                clearTimeout($scope.myVar);
                if ($scope.countNewStry > 0) {
                    $scope.countNewStry = $scope.countNewStry - 1;
                }
                $scope.storyRecord = "";
                $scope.bgImg = "";

                $scope.toDoCalander = false;
                $scope.myStoryBoard = false;
                $scope.barWidth = 0;
                $scope.$apply();

            }

            $scope.openSpecificStory = function (id) {
                clearTimeout($scope.myVar);

                $scope.countNewStry = id;
                $scope.timesRun = 0;
                $scope.barWidth = 0;
                $scope.openNewStory(1);
            }
            $scope.showNavbar = function () {
                angular.element('#sidebar').toggleClass('show_sidebar');
                angular.element('#page-wrapper').toggleClass('page-wrapper-ml');
            }

            $scope.loremIpsum = "";
            $timeout(expand, 0);

            $scope.autoExpand = function (e) {
                var element = typeof e === 'object' ? e.target : document.getElementById(e);
                var scrollHeight = (element && element.scrollHeight) ? element.scrollHeight - 20 : 0; // replace 40 by the sum of padding-top and padding-bottom
                if (scrollHeight <= 310 && element) {
                    element.style.height = scrollHeight + "px";
                }
            };

            function expand() {
                $scope.autoExpand('TextArea');
            }



        }]);



prmApp.directive('dateAsMs', function () {
    return {
        restrict: 'A',
        require: 'ngModel',
        link: function (scope, elem, attrs, ngModelCtrl) {
            ngModelCtrl.$parsers.push(function (value) {
                if (value && value.getTime) {
                    return value.getTime();
                } else {
                    return value;
                }
            });
        }
    };
});