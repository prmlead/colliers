prmApp
    // =========================================================================
    // Auction Tiles
    // =========================================================================
    .controller('freshdeskTicketsCtrl', ["$timeout", "$state", "$scope", "$log", "growlService", "userService", "auctionsService", "$http", "$rootScope",
        "SignalRFactory", "signalRHubName", "logisticServices", "$filter", "store", "poService", "workflowService",
        function ($timeout, $state, $scope, $log, growlService, userService, auctionsService, $http, $rootScope, $filter, SignalRFactory, signalRHubName, logisticServices, store, poService,
            workflowService) {


            $scope.sessionID = userService.getUserToken();


            $scope.totalItems = 0;
            $scope.currentPage = 1;
            $scope.itemsPerPage = 10;
            $scope.maxSize = 8;

            $scope.setPage = function (pageNo) {
                $scope.currentPage = pageNo;
            };

            $scope.pageChanged = function () {
            };

            $scope.showTicketsTable = false;

            $scope.trainer = 'All';
            $scope.source = 'All';

            $scope.filterList = {
                tagsList: [],
                issueTypeList: []

            }
            $scope.responderTicket = [];
            $scope.responderTicketTemp = [];

            $scope.selectedStatus = [];
            $scope.selectedTags = [];
            $scope.selectedtype = [];
            $scope.filterDataByDates = 'created';


            $scope.clientFromDate = moment().add('days', -30).format('YYYY-MM-DD');
            $scope.clientToDate = moment().format('YYYY-MM-DD');

            $scope.statusList = ['Open', 'Pending', 'Resolved', 'Closed', 'Awaiting response', 'Trainings Scheduled', 'Pending Training Confirmation','Suspended Training'];
            $scope.supportFilter = '';

            $scope.transformData = function () {
                $scope.filterList.tagsList = [];
                $scope.filterList.issueTypeList = [];

                $scope.trainerCount = {};
                $scope.sourceCount = {};
                $scope.responderTicket.forEach(function (item) {

                    item.responderName = '';
                    item.sourceName = '';
                    item.groupName = '';
                    if (item.created_at.includes("T") || item.created_at.includes("Z")) {
                        item.created_at = new moment(item.created_at).format("DD-MM-YYYY HH:mm");
                    } else {
                        item.created_at = item.created_at;
                    }

                    if (item.updated_at.includes("T") || item.updated_at.includes("Z")) {
                        item.updated_at = new moment(item.updated_at).format("DD-MM-YYYY HH:mm");
                    } else {
                        item.updated_at = item.updated_at;
                    }

                    if (item.status == 2) {
                        item.status = 'Open'
                    } else if (item.status == 3) {
                        item.status = 'Pending'
                    } else if (item.status == 4) {
                        item.status = 'Resolved'
                    } else if (item.status == 5) {
                        item.status = 'Closed'
                    } else if (item.status == 6) {
                        item.status = 'Pending Training Confirmation'
                    } else if (item.status == 7) {
                        item.status = 'Trainings Scheduled'
                    } else if (item.status == 8) {
                        item.status = 'Awaiting response'
                    } else if (item.status == 9) {
                        item.status = 'Suspended Training'
                    }
                    else {
                        item.status = item.status;
                    }


                    if (item.priority == 1) {
                        item.priority = 'Low'
                    } else if (item.priority == 2) {
                        item.priority = 'Medium'
                    } else if (item.priority == 3) {
                        item.priority = 'High'
                    } else if (item.priority == 4) {
                        item.priority = 'Urgent'
                    } else {
                        item.priority = ''
                    }

                    if ($scope.sourceCount[item.source]) {
                        $scope.sourceCount[item.source]++;
                    } else {
                        $scope.sourceCount[item.source] = 1;
                    }


                    if (item.source == 1) {
                        item.sourceName = 'Email'
                    } else if (item.source == 2) {
                        item.sourceName = 'Portal'
                    } else if (item.source == 3) {
                        item.sourceName = 'Phone'
                    } else if (item.source == 7) {
                        item.sourceName = 'Chat'
                    } else if (item.source == 9) {
                        item.sourceName = 'Feedback Widget'
                    } else if (item.source == 10) {
                        item.sourceName = 'Outbound Email'
                    } else {
                        item.sourceName = ''
                    }


                    if (item.group_id == 81000193970) {
                        item.groupName = 'Customer Support'
                    } else if (item.group_id == 81000274957) {
                        item.groupName = 'Vendor Support'
                    }


                    if (item.responder_id == 81024420267) {
                        item.responderName = 'Karan'
                    } else if (item.responder_id == 81024420713) {
                        item.responderName = 'Srilekha'
                    } else if (item.responder_id == 81025035308) {
                        item.responderName = 'Sanjana'
                    } else if (item.responder_id == 81024420107) {
                        item.responderName = 'Jasjeet'
                    } else if (item.responder_id == 81024420183) {
                        item.responderName = 'Kabita'
                    } else if (item.responder_id == 81024420620) {
                        item.responderName = 'Pavan'
                    } else if (item.responder_id == 81024420515) {
                        item.responderName = 'Manoj'
                    } else if (item.responder_id == 81024420378) {
                        item.responderName = 'Kshithija'
                    }

                    //if (item.tags) {
                    //    item.tags.split(',').forEach(function (tag) {
                    //        if ($scope.filterList.tagsList.indexOf(tag) < 0) {
                    //            $scope.filterList.tagsList.push(tag);
                    //        }
                    //    });
                    //}

                    if (item.type) {
                        item.type.split(',').forEach(function (issueType) {
                            if ($scope.filterList.issueTypeList.indexOf(issueType) < 0) {
                                $scope.filterList.issueTypeList.push(issueType);
                            }
                        });
                    }


                })

                // $scope.getTicket(page + 1);
                
                $scope.responderTicketTemp = $scope.responderTicket;
                $scope.totalItems = $scope.responderTicket.length;
                $scope.setFilters();
            }


            //$scope.getTicket = function (page) {
            //    $scope.updateTime = moment($scope.clientFromDate).utc().format("YYYY-MM-DDTHH:mm:ss[Z]");
            //    $http({
            //        method: 'GET',
            //        //url: "https://prm360.freshdesk.com/api/v2/tickets?per_page=100&page=" + page,
            //        url: "https://prm360.freshdesk.com/api/v2/tickets?updated_since=" + $scope.updateTime + "&per_page=100&page=" + page,

            //        headers: {
            //            'Content-Type': 'application/json',
            //            "Authorization": "Basic " + window.btoa('oLRuHDsQvII8LrfthZAj' + ":x")
            //        },
            //        dataType: "json"
            //    }).then(function (response) {
            //        if (response.data && response.data.length > 0) {
            //            response.data.forEach(function (item) {
            //                if (item.tags.join('').toLowerCase().indexOf($scope.companyName.toLowerCase()) > -1 || (item.custom_fields.cf_client && item.custom_fields.cf_client.toLowerCase().indexOf($scope.companyName.toLowerCase()) > -1)) {
            //                    $scope.responderTicket.push(item);
            //                }
            //                if ($scope.clientToDate) {
            //                    $scope.responderTicket = $scope.responderTicket.filter(function (item) {
            //                        var temp = moment($scope.clientToDate, 'YYYY-MM-DD').add('days', 1).format('YYYY-MM-DD')
            //                        return moment(item.updated_at, 'YYYY-MM-DDTHH:mm:ss[Z]') <= moment(temp, 'YYYY-MM-DD')
            //                    });
            //                }

            //            });
            //            if (response.data.length < 100) {
            //                $scope.transformData();

            //                $scope.showTicketsTable = true;
            //                return
            //            } else {
            //                $scope.getTicket(page + 1);
            //            }
            //        }
            //        //location.reload();
            //    });
            //};
            //userService.getCompanyName().then(function (response) {
            //    $scope.companyName = response;
            //    $scope.getTicket(1);
            //})

         

            $scope.getTicket = function (page) {

                $scope.responderTicketAllData = [];
                $scope.responderTicket = [];
              
                auctionsService.GetAssignetTickets($scope.sessionID).then(function (response) {

                    $scope.responderTicket = response;

                    $scope.transformData()
                })

            }
            $scope.getTicket();

          
            $scope.dateFilter = '';
            $scope.selectedStatus = [];
            $scope.filteredList = [];



            $scope.setFilters = function () {
             
                $scope.filterArray = [];
                $scope.trainerCount = {};
                $scope.sourceCount = {};
                //$scope.responderTicket = $scope.responderTicketTemp;

                $scope.responderTicket = $scope.responderTicketTemp.filter(function (item) {
                    if ($scope.filterDataByDates === 'created') {
                        if (item.created_at) {
                            var temp = moment($scope.clientToDate, 'YYYY-MM-DD').add('days', 1).format('YYYY-MM-DD')
                            var temp1 = moment($scope.clientFromDate, 'YYYY-MM-DD')

                            return moment(item.created_at, 'DD-MM-YYYY HH:mm') >= moment(temp1, 'YYYY-MM-DD') && moment(item.created_at, 'DD-MM-YYYY HH:mm') <= moment(temp, 'YYYY-MM-DD')
                        }
                    } else {
                        if (item.updated_at) {
                            var temp2 = moment($scope.clientToDate, 'YYYY-MM-DD').add('days', 1).format('YYYY-MM-DD')
                            var temp3 = moment($scope.clientFromDate, 'YYYY-MM-DD')

                            return moment(item.updated_at, 'DD-MM-YYYY HH:mm') >= moment(temp3, 'YYYY-MM-DD') && moment(item.updated_at, 'DD-MM-YYYY HH:mm') <= moment(temp2, 'YYYY-MM-DD')
                        }
                    }


                });

                if ($scope.filterDataByDates === 'created') {
                    if ($scope.dateFilter == 'today') {
                        $scope.responderTicket = $scope.responderTicket.filter(function (item) {
                            return moment(item.created_at, 'DD-MM-YYYY').format('DD-MM-YYYY') == moment().format('DD-MM-YYYY');
                        });

                    } else if ($scope.dateFilter == 'yesterday') {
                        $scope.responderTicket = $scope.responderTicket.filter(function (item) {
                            return moment(item.created_at, 'DD-MM-YYYY').format('DD-MM-YYYY') == moment().add('days', -1).format('DD-MM-YYYY');
                        });

                    } else if ($scope.dateFilter == 'lastWeek') {
                        $scope.responderTicket = $scope.responderTicket.filter(function (item) {
                            return moment(item.created_at, 'DD-MM-YYYY') >= moment().add('days', -7)
                        });
                    }
                } else {
                    if ($scope.dateFilter == 'today') {
                        $scope.responderTicket = $scope.responderTicket.filter(function (item) {
                            return moment(item.updated_at, 'DD-MM-YYYY').format('DD-MM-YYYY') == moment().format('DD-MM-YYYY');
                        });

                    } else if ($scope.dateFilter == 'yesterday') {
                        $scope.responderTicket = $scope.responderTicket.filter(function (item) {
                            return moment(item.updated_at, 'DD-MM-YYYY').format('DD-MM-YYYY') == moment().add('days', -1).format('DD-MM-YYYY');
                        });

                    } else if ($scope.dateFilter == 'lastWeek') {
                        $scope.responderTicket = $scope.responderTicket.filter(function (item) {
                            return moment(item.updated_at, 'DD-MM-YYYY') >= moment().add('days', -7)
                        });
                    }
                }

                if ($scope.supportFilter) {
                    if ($scope.supportFilter == 'vendor') {
                        $scope.responderTicket = $scope.responderTicket.filter(function (item) {
                            return item.groupName == 'Vendor Support';
                        });
                    } else if ($scope.supportFilter == 'customer') {
                        $scope.responderTicket = $scope.responderTicket.filter(function (item) {
                            return item.groupName == 'Customer Support';
                        });
                    }

                }

                if ($scope.selectedStatus.length > 0) {
                    $scope.responderTicket = $scope.responderTicket.filter(function (item) {
                        return $scope.selectedStatus.indexOf(item.status) > -1;
                    });

                }

                if ($scope.selectedTags.length > 0) {
                    $scope.responderTicket = $scope.responderTicket.filter(function (item) {
                        return _.intersection($scope.selectedTags, item.tags.split(',')).length > 0;
                    });

                }
                if ($scope.selectedtype.length > 0) {
                    $scope.responderTicket = $scope.responderTicket.filter(function (item) {
                        return _.intersection($scope.selectedtype, item.type.split(',')).length > 0;
                    });

                }


                if ($scope.responderTicket.length > 0) {

                    $scope.responderTicket.forEach(function (item, index) {

                        if ($scope.trainerCount[item.responder_id]) {
                            $scope.trainerCount[item.responder_id]++;
                        } else {
                            $scope.trainerCount[item.responder_id] = 1;
                        }

                        if ($scope.sourceCount[item.source]) {
                            $scope.sourceCount[item.source]++;
                        } else {
                            $scope.sourceCount[item.source] = 1;
                        }
                    });

                }
                if ($scope.trainer !== 'All') {
                    $scope.responderTicket = $scope.responderTicket.filter(function (item) {

                        return item.responderName == $scope.trainer;

                    });
                }
                if ($scope.source !== 'All') {
                    $scope.responderTicket = $scope.responderTicket.filter(function (item) {

                        return item.sourceName == $scope.source;

                    });
                }
                $scope.totalItems = $scope.responderTicket.length;

                $scope.filteredList = [$scope.dateFilter, $scope.trainer, $scope.source]
                $scope.selectedStatus.forEach(function (item) {
                    $scope.filteredList.push(item)
                })
                $scope.selectedTags.forEach(function (item) {
                    $scope.filteredList.push(item)
                })
                $scope.selectedtype.forEach(function (item) {
                    $scope.filteredList.push(item)
                })




            }

        }]);