﻿//(function () { 
prmApp.controller('productViewCtrl', ['$scope', '$state', '$window', '$stateParams', '$filter', 'catalogService', 'userService', 'growlService',
    function ($scope, $state, $window, $stateParams, $filter, catalogService, userService, growlService) {

        $scope.productId = $stateParams.productId == "" || !$stateParams.productId ? 0 : $stateParams.productId;

        $scope.compId = userService.getUserCompanyId();
        $scope.catalogCompId = userService.getUserCatalogCompanyId();;

        $scope.GetDateconverted = function (dateBefore) {
            if (dateBefore) {
                return new moment(dateBefore).format("DD-MM-YYYY HH:mm");
            }
        };
        // in view ctrl
        $scope.productDetails = null;
        $scope.getProductDetails = function () {
            catalogService.getproductbyid($scope.catalogCompId, $scope.productId)
                .then(function (response) {
                    $scope.productDetails = {
                        prodId: response.prodId,
                        prodName: response.prodName,
                        prodCode: response.prodCode,
                        prodDesc: response.prodDesc,
                        prodHSNCode: response.prodHSNCode,
                        prodQty: response.prodQty,
                        prodNo: response.prodNo,
                        dateModified: $scope.GetDateconverted(response.dateModified)
                    };
                    catalogService.GetCompanyConfiguration($scope.catalogCompId, "ITEM_UNITS", userService.getUserToken())
                        .then(function (unitResponse) {
                            $scope.companyItemUnits = unitResponse;
                            $scope.prodEditDetails = response;
                        });
                    $scope.productName = response.prodName;
                    $scope.productDesc = response.prodDesc;
                });

        }

        var catNodes = [];
        $scope.nodes = [];
        $scope.selectedNodesView = [];
        $scope.getcategories = function () {
            //catalogService.getcategories($scope.compId)
            catalogService.GetProductSubCategories($scope.productId, 0, $scope.catalogCompId)
                .then(function (response) {
                    // console.log(response);
                    //$scope.companyCatalog = response;

                    //$scope.companyCatalog.forEach(function (item, index) {
                    //    var cat = {

                    //        "compId": item.compId,
                    //        "parentID": item.catParentId,
                    //        "id": item.catId,
                    //        "title": item.catName,
                    //        "catdesc": item.catDesc,
                    //        "subCatCount": item.subCatCount,
                    //        "childCollapsed": true,
                    //        "nodeChecked": item.catSelected > 0 ? true : false,
                    //        "nodes": []
                    //    }
                    //    catNodes.push(cat);
                    //    $scope.nodes = catNodes;
                    //})
                    $scope.nodes = response
                    $scope.getSelectedNodes();
                    //$scope.collapseAll();
                });

        }
        
        $scope.selectedNodes = '0';
        $scope.selectedCategories = '';
        $scope.selectChildNodes = function (childNode, ischecked) {
            childNode.nodeChecked = ischecked;
            childNode.nodes.forEach(function (item, index) {
                $scope.selectChildNodes(item, ischecked);
            });
        }

        $scope.selectParentNode = function (ischecked, selectedNodeScope) {
            if (selectedNodeScope) {
                var parentScope = selectedNodeScope.$parent;
                if (parentScope && parentScope.node && parentScope.node.catId > 0) {
                    if (ischecked) {
                        parentScope.node.nodeChecked = ischecked;
                    }
                    else {
                        if (parentScope.node.nodes && parentScope.node.nodes.length > 0) {
                            if ($filter('filter')(parentScope.node.nodes, { 'nodeChecked': true }).length <= 0) {
                                parentScope.node.nodeChecked = ischecked;
                            }
                        }
                        else {
                            parentScope.node.nodeChecked = ischecked;
                        }
                    }

                    $scope.selectParentNode(ischecked, parentScope);
                }
            }
        }
        $scope.checkChanged = function (selectedNode, selectedNodeScope) {
            $scope.selectChildNodes(selectedNode, selectedNode.nodeChecked);
            $scope.selectParentNode(selectedNode.nodeChecked, selectedNodeScope);
            $scope.getSelectedNodes();
        }
        $scope.toggle = function (scope) {
            scope.toggle()

        };

        $scope.getProductDetails();
        $scope.getcategories();

        $scope.editProduct = function (prodId) {

            $scope.productEditObj = {
                prodId: prodId,
                compId: $scope.catalogCompId,
                isValid: 0,
                ModifiedBy: userService.getUserId()
            };
            var param = {
                reqProduct: $scope.productEditObj,
                sessionID: userService.getUserToken()
            }

            catalogService.isProdEditAllowed(param)
                .then(function (response) {
                    if (response.errorMessage != '') {
                        growlService.growl(response.errorMessage, "inverse");
                    }
                    else {
                        $state.go("productEdit", { "productId": prodId });
                    }
                });
        }

        $scope.deleteProduct = function (prod) {

            swal({
                title: "Are You Sure!",
                text: 'Do You want to Delete the Item Permanently',
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes",
                closeOnConfirm: true
            }, function () {
                $scope.productdelObj = {
                    prodId: prod.prodId,
                    compId: $scope.catalogCompId,
                    prodCode: prod.prodCode,
                    prodName: prod.prodName,
                    isValid: 0,
                    ModifiedBy: userService.getUserId()
                };
                var param = {
                    reqProduct: $scope.productdelObj,
                    sessionID: userService.getUserToken()
                }

                catalogService.deleteProduct(param)
                    .then(function (response) {
                        if (response.errorMessage != '') {
                            growlService.growl(response.errorMessage, "inverse");
                        }
                        else {
                            growlService.growl("product has been deleted.", "success");
                            $scope.goToProducts();
                        }
                    });
            });

            

        }

        $scope.InactiveProduct = function (prod) {
            swal({
                title: "Are You Sure!",
                text: 'Do You want to Inactivate the Item',
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes",
                closeOnConfirm: true
            }, function () {
                $scope.productdelObj = {
                    prodId: prod.prodId,
                    compId: $scope.catalogCompId,
                    prodCode: prod.prodCode,
                    prodName: prod.prodName,
                    isValid: 2,
                    ModifiedBy: userService.getUserId()
                };
                var param = {
                    reqProduct: $scope.productdelObj,
                    sessionID: userService.getUserToken()
                }

                catalogService.deleteProduct(param)
                    .then(function (response) {
                        if (response.errorMessage != '') {
                            growlService.growl(response.errorMessage, "inverse");
                        }
                        else {
                            growlService.growl("product has been Inactivated.", "success");
                            $scope.goToProducts();
                        }
                    });
            });

        }

        $scope.goToProducts = function () {
            //var url = $state.href("productEdit", { "productId": prodId });
            $state.go("products");
            //window.open(url, '_blank');
        }

        $scope.productAlalysis = function (id) {
            var url = $state.href('productAlalysis', { "productId": id });
            $window.open(url, '_self');
        }

        $scope.getProperties = function () {
            $scope.Properties = [];
            catalogService.getProperties($scope.catalogCompId, $scope.productId, 0)
                .then(function (response) {

                    $scope.PropertiesRaw = response;

                    $scope.PropertiesRaw.forEach(function (item, index) {
                        var Property = {
                            "propId": item.propId,
                            "propName": item.propName,
                            "propDesc": item.propDesc,
                            "propDataType": item.propDataType,
                            "propChecked": item.propValue == null ? false : true,
                            "propOptions": item.propOptions,
                            "propValue": item.propDataType == "multi" ? (item.propValue == null ? item.propValue : item.propValue.split("^^")) : item.propValue,
                            "propIsValid": item.isValid,
                            "propModified": false
                        }
                        $scope.Properties.push(Property);

                    })

                    //$scope.collapseAll();
                });
            /*
            $scope.Attributes.push({ attrName: 'test Text', attrType: 'Short Text', attrId: 123, attrDefault: '', attrChecked: false, attrOptions: '', attrValue:'' });
            $scope.Attributes.push({ attrName: 'test Paragraph', attrType: 'Paragraph', attrId: 234, attrDefault: '', attrChecked: false, attrOptions: '', attrValue:'' });
            $scope.Attributes.push({ attrName: 'test dropdown', attrType: 'Dropdown', attrId: 235, attrDefault: '', attrChecked: false, attrOptions: 't1$$t2$$t3$$t4', attrValue:'' });
            $scope.Attributes.push({ attrName: 'test Multiselect', attrType: 'Multiselect', attrId: 235, attrDefault: '', attrChecked: false, attrOptions: 'p1$$p2$$p3$$p4', attrValue:'' });
            */
        }
        $scope.getProperties();
        $scope.propCheckChanged = function (selectedProp) {
            selectedProp.propModified = true;
            //selectedProp.propChecked = !selectedProp.propChecked;
            selectedProp.propIsValid = selectedProp.propChecked
        }

        $scope.propModified = function (selectedProp) {
            selectedProp.propModified = true;
            selectedProp.propIsValid = selectedProp.propChecked
        }
       
        $scope.getSelectedNodes = function () {
            $scope.selectedCategories = [];
            $scope.selectedNodes = '0';

            $scope.nodes.forEach(function (item, index) {
                $scope.getSelectedNodesIteration(item, $scope.selectedCategories);
            })
        }

        $scope.getSelectedNodesIteration = function (selectedNode, selectedCat) {
            var selectedItem = { "catName": selectedNode.catName, "nodes": [] };
            if (selectedNode.nodeChecked) {
                
                $scope.selectedNodes += ',' + selectedNode.catId;

                if (selectedNode.nodes != null) {
                    selectedNode.nodes.forEach(function (item, index) {
                        if (item.nodeChecked) {
                            $scope.getSelectedNodesIteration(item, selectedItem.nodes);
                        }

                    })
                }
                selectedCat.push(selectedItem);
            }
            
        }


        $scope.ProductVendors = [];
        $scope.count = 0;
        $scope.GetProductVendors = function () {
            $scope.params = { "userID": userService.getUserId(), "sessionID": userService.getUserToken() }
            catalogService.getproductVendors($scope.productId, userService.getUserToken())
                .then(function (response) {
                    $scope.ProductVendors = response;

                    $scope.vendorsList.forEach(function (vlItem, vlIndex) {
                        $scope.ProductVendors.forEach(function (plItem, plIIndex) {
                            //if (vlItem.userID == plItem.userID) {
                            //    vlItem.isAssignedToProduct = true;
                            //} else {
                            //    vlItem.isAssignedToProduct = false;
                            //}
                            if (vlItem.userID == plItem.userID) {
                                //$scope.count++;
                                //alert("1111111>>>>" + $scope.count);
                                vlItem.isAssignedToProduct = true;
                            }
                        })
                    })


                });
        };




        $scope.GetCompanyVendors = function () {
            $scope.params = { "userID": userService.getUserId(), "sessionID": userService.getUserToken() }
            userService.GetCompanyVendors($scope.params)
                .then(function (response) {
                    $scope.vendorsList = response;
                    $scope.VendorsTemp1 = $scope.vendorsList;
                    if ($scope.productId > 0) {
                        $scope.GetProductVendors();
                    }
                });
        };

        $scope.GetCompanyVendors();
        
    }]);
