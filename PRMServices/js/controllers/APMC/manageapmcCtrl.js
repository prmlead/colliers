﻿prmApp
    .controller('manageapmcCtrl', ["$scope", "$state", "$stateParams", "userService", "growlService", "apmcService", "fileReader", "$log",
        function ($scope, $state, $stateParams, userService, growlService, apmcService, fileReader, $log) {
            $scope.compID = userService.getUserCompanyId();
            $scope.sessionID = userService.getUserToken();
            $scope.currentID = userService.getUserId();
            $scope.isCustomer = userService.getUserType() == "CUSTOMER" ? true : false;
            
            $scope.apmcList = [];
            
            apmcService.getapmclist()
                .then(function (response) {
                    $scope.apmcList = response;
                })

            $scope.goToAPMCEdit = function (apmcID) {
                $state.go("saveapmc", { "apmcID": apmcID });
            }

            $scope.gotechevalquestions = function (evalID) {
                $log.info(userService.getUserType);
                if (userService.getUserType() == "VENDOR") {
                    $state.go("techevalresponses", { "evalID": evalID, "vendorID": $scope.currentID });
                } else {
                    $state.go("techevalresponses", { "evalID": evalID, "vendorID": 0 });
                }

            }

            $scope.deletequestionnaire = function (evalID) {

                var params = {
                    "evalID": evalID,
                    sessionID: $scope.sessionID
                };


                swal({
                    title: "Are You Sure!",
                    text: 'Do You want to Delete Questionnaire Permanently',
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Yes",
                    closeOnConfirm: true
                },
                    function () {
                        techevalService.deletequestionnaire(params)
                            .then(function (response) {
                                if (response.errorMessage != '') {
                                    growlService.growl(response.errorMessage, "inverse");
                                }
                                else {
                                    growlService.growl("Questionnaire Deleted Successfully.", "success");
                                    location.reload();
                                }
                            });
                    });



            }

        }]);