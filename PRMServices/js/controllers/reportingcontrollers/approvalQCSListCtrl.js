﻿prmApp
    .controller('approvalQCSListCtrl', ["$scope", "$stateParams", "$log", "$state", "$window", "userService", "auctionsService", "storeService", "growlService",
    "PRMPRServices", "reportingService","workflowService",
    function ($scope, $stateParams, $log, $state, $window, userService, auctionsService, storeService, growlService,
        PRMPRServices, reportingService, workflowService) {

        $scope.userID = userService.getUserId();
        $scope.sessionID = userService.getUserToken();
        
        $scope.qcsApprovalList = [];
        $scope.qcsApprovalList1 = [];
        $scope.qcsApprovalListTemporary = [];
        $scope.deptIDs = [];
        $scope.desigIDs = [];
        $scope.deptIDStr = '';
        $scope.desigIDStr = '';
        $scope.moduleName = '';

        /*PAGINATION CODE*/
        $scope.totalItems = 0;
        $scope.currentPage = 1;
        $scope.itemsPerPage = 10;
        $scope.maxSize = 8;

        $scope.setPage = function (pageNo) {
            $scope.currentPage = pageNo;
        };

        $scope.pageChanged = function () {
            ////console.log('Page changed to: ' + $scope.currentPage);
        };
        /*PAGINATION CODE*/


        $scope.modules = [
            {
                display: 'BUDGET',
                value: 'BUDGET'
            },
            {
                display: 'VENDOR SELECTION',
                value: 'VENDOR_SELECTION'
            },
            {
                display: 'PACKAGE APPROVAL',
                value: 'PACKAGE_APPROVAL'
            },
            {
                display: 'BILL CERTIFICATION',
                value: 'BILL_CERTIFICATION'
            },
            {
                display: 'PO_AMENDMENT',
                value: 'PO_AMENDMENT'
            },
            {
                display: 'PROJECT_AMENDMENT',
                value: 'PROJECT_AMENDMENT'
            },
            {
                display: 'VENDOR_FORM_WORKFLOW',
                value: 'VENDOR_FORM_WORKFLOW'
            }
        ];




        $scope.ListUserDepartmentDesignations = userService.getListUserDepartmentDesignations();
        if ($scope.ListUserDepartmentDesignations && $scope.ListUserDepartmentDesignations.length > 0) {
            $scope.ListUserDepartmentDesignations.forEach(function (item, index) {
                $scope.deptIDs.push(item.deptID);
                item.listDesignation.forEach(function (item1, index1) {
                    if (item1.isAssignedToUser && item1.isValid) {
                        $scope.desigIDs.push(item1.desigID);
                        $scope.desigIDs = _.union($scope.desigIDs, [item1.desigID]);
                    }
                });
            });
            $scope.deptIDStr = $scope.deptIDs.join(',');
            $scope.desigIDStr = $scope.desigIDs.join(',');
        }


        $scope.getQCSApprovalList = function () {

            var params = {
                "userid": $scope.userID,
                "deptid": $scope.deptIDStr,
                "desigid": $scope.desigIDStr,
                "type": "ALL"
            };
            workflowService.GetApprovalList(params)
                .then(function (response) {
                    $scope.qcsApprovalList = response;
                    //$scope.qcsApprovalList1 = response;
                    //$scope.qcsApprovalListTemporary = response;

                    $scope.qcsApprovalList = $scope.qcsApprovalList.filter(function (item, index) {
                        return item.USER_ID == $scope.userID;
                    });

                    $scope.qcsApprovalList.forEach(function (qcs, qcsIndex) {
                        qcs.APPROVAL_DATE = userService.toLocalDate(qcs.APPROVAL_DATE);
                    });

                    $scope.qcsApprovalList1 = $scope.qcsApprovalList;
                    $scope.qcsApprovalListTemporary = $scope.qcsApprovalList;

                    //$scope.qcsApprovalList1 = $scope.qcsApprovalList1.filter(function (item, index) {
                    //    return item.USER_ID == $scope.userID;
                    //});

                    //$scope.qcsApprovalListTemporary = $scope.qcsApprovalListTemporary.filter(function (item, index) {
                    //    return item.USER_ID == $scope.userID;
                    //});

                    //$scope.qcsApprovalList.forEach(function (qcs, qcsIndex) {
                    //    qcs.APPROVAL_DATE = userService.toLocalDate(qcs.APPROVAL_DATE);
                    //});

                    //$scope.qcsApprovalList1.forEach(function (qcs, qcsIndex) {
                    //    qcs.APPROVAL_DATE = userService.toLocalDate(qcs.APPROVAL_DATE);
                    //});

                    //$scope.qcsApprovalListTemporary.forEach(function (qcs, qcsIndex) {
                    //    qcs.APPROVAL_DATE = userService.toLocalDate(qcs.APPROVAL_DATE);
                    //});
                    
                    $scope.totalItems = $scope.qcsApprovalList.length;

                });
        };


        $scope.getQCSApprovalList();


        $scope.setFilters = function (status, searchKeyword) {
            if (searchKeyword == "") {
                searchKeyword = null;
            }
            if (status && searchKeyword) {

                $scope.qcsApprovalList = $scope.qcsApprovalList1.filter(function (item) {
                    if (item.MODULE_TYPE === status && ((String(item.MODULE_NAME).toUpperCase().includes(searchKeyword.toUpperCase()))
                        || (String(item.QCS_ID).toUpperCase().includes(searchKeyword.toUpperCase()))
                        || (String(item.REFER_MODULE_LINK_NAME).toUpperCase().includes(searchKeyword.toUpperCase()))
                        || (String(item.APPROVAL_BY).toUpperCase().includes(searchKeyword.toUpperCase())))) {
                        return item.MODULE_TYPE === status &&
                            (String(item.MODULE_TYPE).toUpperCase().includes(searchKeyword.toUpperCase())
                            || (String(item.MODULE_NAME).toUpperCase().includes(searchKeyword.toUpperCase()))
                            || (String(item.QCS_ID).toUpperCase().includes(searchKeyword.toUpperCase()))
                            || (String(item.REFER_MODULE_LINK_NAME).toUpperCase().includes(searchKeyword.toUpperCase()))
                            || (String(item.APPROVAL_BY).toUpperCase().includes(searchKeyword.toUpperCase())));
                    }

                });

            } else {
                if (!status && searchKeyword) {
                    $scope.qcsApprovalList = _.filter($scope.qcsApprovalList1, function (item) {
                        return (String(item.MODULE_TYPE).toUpperCase().includes(searchKeyword.toUpperCase())
                            || (String(item.QCS_ID).toUpperCase().includes(searchKeyword.toUpperCase()))
                            || (String(item.MODULE_NAME).toUpperCase().includes(searchKeyword.toUpperCase()))
                            || (String(item.REFER_MODULE_LINK_NAME).toUpperCase().includes(searchKeyword.toUpperCase()))
                            || (String(item.APPROVAL_BY).toUpperCase().includes(searchKeyword.toUpperCase()))) == true;
                    });
                } else {
                    $scope.qcsApprovalList = $scope.qcsApprovalList1.filter(function (item) {
                        return item.MODULE_TYPE == status;
                    });
                }
            };

            if (!status && !searchKeyword) {
                $scope.qcsApprovalList = $scope.qcsApprovalList1;
            }

            $scope.totalItems = $scope.qcsApprovalList.length;

        };

        //$scope.setFilters = function (search) {

        //    if (search) {
        //        $scope.qcsApprovalList = $scope.qcsApprovalList1.filter(function (qcs) {
        //            return (String(qcs.MODULE_ID).indexOf(search) >= 0
        //                    || qcs.MODULE_TYPE.toLowerCase().indexOf(search.toLowerCase()) >= 0
        //                    || String(qcs.REFER_MODULE_LINK_ID).indexOf(search) >= 0
        //                    || qcs.REFER_MODULE_LINK_NAME.toLowerCase().indexOf(search.toLowerCase()) >= 0);
        //        });
        //        $scope.totalItems = $scope.qcsApprovalList.length;
        //    } else {
        //        $scope.qcsApprovalList = [];
        //        $scope.qcsApprovalList = $scope.qcsApprovalListTemporary;
        //        $scope.totalItems = $scope.qcsApprovalListTemporary.length;
        //    }


        //};

        $scope.routeToModule = function (module,moduleID) {

            if (module && moduleID) {
                var approvalObj = { isFromApproval: true };

                return module == 'DOMESTIC' ? "cost-comparisions-qcs({ reqID: qcs.REFER_MODULE_LINK_ID, qcsID: qcs.MODULE_ID},'costcomparisonsObj':approvalObj)" : "import-qcs({ reqID: qcs.REFER_MODULE_LINK_ID, qcsID: qcs.MODULE_ID },'importObj':approvalObj)";
            }
        };

        $scope.navigateToQCS = function (qcs) {
            //let url1 = "";
            //if (qcs.MODULE_TYPE === 'DOMESTIC') {
            //    url = $state.href("cost-comparisions-qcs", { "reqID": qcs.REFER_MODULE_LINK_ID, "qcsID": qcs.MODULE_ID });
            //} else {
            //    url = $state.href("import-qcs", { "reqID": qcs.REFER_MODULE_LINK_ID, "qcsID": qcs.MODULE_ID });
            //}

            if (qcs.MODULE_TYPE == 'BUDGET' || qcs.MODULE_TYPE == 'VENDOR_SELECTION') {
              var url1 = $state.href('setupBudget', { "projectId": qcs.REFER_MODULE_LINK_ID, "budgetId": qcs.MODULE_ID });
                $window.open(url1, '_blank');
            }
            else if (qcs.MODULE_TYPE == 'PACKAGE_APPROVAL') {
                var url1 = $state.href('packageApproval', { "projectId": qcs.REFER_MODULE_LINK_ID, "budgetId": qcs.REFER_MODULE_LINK_ID_1, "packageApprovalId": qcs.MODULE_ID ,"qcsId": qcs.QCS_ID });
                $window.open(url1, '_blank');
            }
            else if (qcs.MODULE_TYPE == 'BILL_CERTIFICATION') {
                var url1 = $state.href('billRequestsList');
                $window.open(url1, '_blank');
            }
            else if (qcs.MODULE_TYPE == 'VENDOR') {
                var url2 = $state.href('pages.profile.new-vendor');
                $window.open(url2, '_blank');
            }
            else if (qcs.MODULE_TYPE == 'UNBLOCK_VENDOR') {
                var url3 = $state.href('pages.profile.new-vendor');
                $window.open(url3, '_blank');
            }
            else if (qcs.MODULE_TYPE == 'VENDOR_INVOICE') {
                var url4 = $state.href('uploadvendorinvoice', { "ID": qcs.REFER_MODULE_LINK_ID });
                $window.open(url4, '_blank');
            }
            else if (qcs.MODULE_TYPE == 'PROJECT_AMENDMENT') {
                var url4 = $state.href('projectAmendment', { "projectId": qcs.REFER_MODULE_LINK_ID, "budgetId": qcs.REFER_MODULE_LINK_ID_1, "amendmentId": qcs.MODULE_ID });
                $window.open(url4, '_blank');
            }
            else if (qcs.MODULE_TYPE == 'PO_AMENDMENT') {
                var url4 = $state.href('poAmendment', { "poID": qcs.REFER_MODULE_LINK_NAME, "PO_AMENDMENT_ID": qcs.MODULE_ID });
                $window.open(url4, '_blank');
            }
            else if (qcs.MODULE_TYPE == 'VENDOR_FORM_WORKFLOW') {
                var url4 = $state.href('vendorRegistration1', { "vendorId": qcs.REFER_MODULE_LINK_ID });
                $window.open(url4, '_blank');
            }

            window.open(url, '_blank');
        };


        $scope.goToSaveImportQCS = function (reqID, qcsID) {
            var url = $state.href("import-qcs", { "reqID": reqID, "qcsID": qcsID });
            window.open(url, '_blank');
        };

}]);