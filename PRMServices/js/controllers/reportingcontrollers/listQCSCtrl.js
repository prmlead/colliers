﻿prmApp
.controller('listQCSCtrl', ["$scope", "$stateParams", "$log", "$state", "$window", "userService", "auctionsService", "storeService", "growlService",
    "PRMPRServices", "reportingService", "workflowService","$filter",
    function ($scope, $stateParams, $log, $state, $window, userService, auctionsService, storeService, growlService,
        PRMPRServices, reportingService, workflowService, $filter) {

        $scope.userID = userService.getUserId();
        $scope.sessionID = userService.getUserToken();
        $scope.reqID = $stateParams.reqID;

        $scope.deptID = userService.getSelectedUserDepartmentDesignation().deptID;
        $scope.desigID = userService.getSelectedUserDesigID();

        $scope.enableQCSIfCreated = false;
        $scope.enableQCSDomestic = true;

        $scope.QCSList = [];
        $scope.QCSListTemp = [];
        $scope.InActiveQCSList = [];
        $scope.requirementDetails;
        $scope.getData = function () {
            reportingService.GetQcsPRDetails({ "reqID": $stateParams.reqID, "sessionid": $scope.sessionID })
                .then(function (response) {
                    if (response) {
                        $scope.requirementDetails = response;
                        // $scope.requirementDetails.VENDOR_DETAILS.forEach(function (details) {   })
                        var vendorDetails = _.filter($scope.requirementDetails.VENDOR_DETAILS, function (details) {
                            if ((!details.SELECTED_VENDOR_CURRENCY == "") || (!details.SELECTED_VENDOR_CURRENCY == null) || (!details.SELECTED_VENDOR_CURRENCY == undefined)) {
                                return details.SELECTED_VENDOR_CURRENCY != $scope.requirementDetails.REQ_CURRENCY;
                            }

                        });
                        if (vendorDetails.length > 0) {
                            if ($scope.requirementDetails.REQ_CURRENCY != vendorDetails[0].SELECTED_VENDOR_CURRENCY) {
                                $scope.enableQCSDomestic = false;
                            }
                        }

                        $scope.GetQCSList();
                    }
                });
        };

        $scope.getData();

        $scope.isValidToCreateQCS = function () {

            let isValid = true;
            isValid = _.some($scope.QCSList, function (item) {
                return (item.IS_SENT_FOR_APPROVAL === 1);
            });
            return !isValid;

        };

        $scope.CREATE_QCS = true;

        $scope.GetQCSList = function () {
            var params = {
                "uid": $scope.userID,
                "reqid": $scope.reqID,
                "sessionid": userService.getUserToken()
            };
            reportingService.GetQCSList(params)
                .then(function (response) {
                    $scope.CREATE_QCS = true;
                    $scope.QCSList = response;
                    $scope.QCSList.forEach(function (qcs, qcsIndex) {
                        qcs.CREATED_DATE = userService.toLocalDate(qcs.CREATED_DATE);

                        if (qcs.IS_PRIMARY_ID == 1) {
                            qcs.isPrimary = 'PRIMARY';
                        } else {
                            qcs.isPrimary = 'SECONDARY';
                        }
                    });
                    $scope.QCSListTemp = $scope.QCSList;
                    $scope.QCSList = $filter('filter')($scope.QCSListTemp, { IS_VALID: 1 });
                    $scope.QCSList.forEach(function (item, index) {
                        item.PURCHASE_GROUP = JSON.parse(item.REQ_JSON).PURCHASE_GROUP_CODES;
                    });


                    $scope.CREATE_QCS = $scope.isValidToCreateQCS();

                    $scope.InActiveQCSList = $filter('filter')($scope.QCSListTemp, { IS_VALID: 0 });

                    $scope.disableCreationOfQCS($scope.QCSList, $scope.reqID);
                });
        };

        // $scope.GetQCSList();

        $scope.DeleteQcs = function (reqID, qcsID) {

            swal({
                title: "Are you sure?",
                //text: "",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#F44336",
                confirmButtonText: "Yes",
                closeOnConfirm: true
            }, function () {
                var params = {
                    "reqID": reqID,
                    "qcsID": qcsID,
                    "sessionid": userService.getUserToken()
                };
                reportingService.DeleteQcs(params)
                    .then(function (response) {
                        if (response.errorMessage != '') {
                            growlService.growl(response.errorMessage, "inverse");
                        }
                        else {
                            growlService.growl("QCS Deleted successfully.", "success");
                            $scope.GetQCSList();
                        }

                    });
            });
        }

        $scope.ActivateQcs = function (reqID, qcsID) {

            swal({
                title: "Are you sure?",
                //text: "",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#F44336",
                confirmButtonText: "Yes",
                closeOnConfirm: true
            }, function () {
                var params = {
                    "reqID": reqID,
                    "qcsID": qcsID,
                    "sessionid": userService.getUserToken()
                };
                reportingService.ActivateQcs(params)
                    .then(function (response) {
                        if (response.errorMessage != '') {
                            growlService.growl(response.errorMessage, "inverse");
                        }
                        else {
                            growlService.growl("QCS Activated successfully.", "success");
                            $scope.GetQCSList();
                        }

                    });
            });
        }

        $scope.goToSaveDomesticQCS = function (reqID, qcsID) {
            var url = $state.href("cost-comparisions-qcs", { "reqID": reqID, "qcsID": qcsID });
            window.open(url, '_self');
        };

        $scope.goToSaveImportQCS = function (reqID, qcsID) {
            var url = $state.href("import-qcs", { "reqID": reqID, "qcsID": qcsID });
            window.open(url, '_self');
        };


        $scope.qcsApprovalList = [];

        $scope.goToApprovalQCS = function () {

            var url = $state.href("approval-qcs-list");
            window.open(url, '_blank');

        };

        $scope.disableCreationOfQCS = function (arr, value) {

            $scope.enableQCSIfCreated = true;

            if ($scope.requirementDetails.REQ_STATUS == "Negotiation Ended") {
                $scope.enableQCSIfCreated = true;
            } else {
                if (arr && arr.length > 0) {
                    var req_id = _.findIndex(arr, function (requirement) { return requirement.REQ_ID === +value; });
                    if (req_id > -1) {
                        $scope.enableQCSIfCreated = false;
                    }
                }
            }


            return $scope.enableQCSIfCreated;
        };

        $scope.deletedOn = function (date) {
            return userService.toLocalDate(date);
        };

        $scope.goToPackageApproval = function (item) {
            var url = $state.href("packageApproval", { "projectId": +item.PROJECT_ID, "budgetId": item.BUDGET_ID, "packageApprovalId": item.PACKAGE_APPROVAL_ID,"qcsId":item.QCS_ID});
            window.open(url, '_blank');
        };

    }]);