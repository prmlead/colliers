﻿prmApp

    .controller('sapShortageReportCtrl', ["$scope", "$http", "$state", "domain", "$filter", "$log", "$stateParams", "$timeout",
        "auctionsService", "reportingService", "userService", "SignalRFactory", "fileReader", "growlService",
        function ($scope, $http, $state, domain, $filter, $log, $stateParams, $timeout,
            auctionsService, reportingService, userService, SignalRFactory, fileReader, growlService) {

            $scope.deliveryMissed = 0;
            $scope.deliveryAlert = 0;
            $scope.timeAvailable = 0;

            $scope.minDateMoment = moment();
            //$scope.minDateMoment = moment().subtract(1, 'day');
            //$scope.minDateString = moment().subtract(1, 'day').format('YYYY-MM-DD');

            /*PAGINATION CODE*/
            $scope.totalItems = 0;
            $scope.currentPage = 1;
            $scope.itemsPerPage = 8;
            $scope.maxSize = 5; //Number of pager buttons to show
            $scope.errorMessage = '';
            $scope.color = '';
            $scope.setPage = function (pageNo) {
                $scope.currentPage = pageNo;
            };

            $scope.pageChanged = function () {
                ////console.log('Page changed to: ' + $scope.currentPage);
            };
            /*PAGINATION CODE*/

            $scope.openPOArr = [];
            $scope.openPOArrForSortig = $scope.openPOArr;
            $scope.openChildPOArr = [];
            $scope.openPOArrInitial = [];
            $scope.openPOPivot = [];
            $scope.openPOPivotData = [];
            $scope.filterModelPurchaseUser = '0';
            $scope.category = '0';
            $scope.filterDateRange = 0;
            $scope.poitemcomments = [];
            $scope.selectedPOItem;
            $scope.poquantity = 0;
            $scope.pocomments = '';
            $scope.selectedGraphFilter;
            $scope.monthWiseReport = {};
            $scope.tileFilter = '';
            $scope.filterModelPlant = '0';
            $scope.filterModelExclusion = '0';
            $scope.selectedMonth = '';
            $scope.displayDetails = false;
            $scope.tileFilterTextToDisplay = '';
            $scope.showDetailedView = false;
            $scope.accessTypes = [];
            $scope.permission = 'VIEW';
            $scope.lastUpdatedDate = "";
            $scope.commentType = "SHORTAGE";

            $scope.getLastUpdatedDate = function () {
                reportingService.GetLastUpdatedDate('sap_openpo')
                    .then(function (response) {
                        //$log.info(response);
                        $scope.lastUpdatedDate = response.message;
                    });
            }

            $scope.getUserAccess = function () {
                reportingService.GetSapAccess(userService.getUserId())
                    .then(function (response) {
                        //$log.info(response);
                        $scope.accessTypes = response;
                        if ($scope.accessTypes && $scope.accessTypes.length > 0) {
                            $scope.filterModelPurchaseUser = $scope.accessTypes[0].objectID;
                            $scope.permission = $scope.accessTypes[0].errorMessage;
                            $scope.getOpenPO(0, $scope.filterModelPurchaseUser,0);
                            //$scope.getOpenPRPivot(0, $scope.filterModelPurchaseUser);
                        }
                    });
            }

            $scope.getUserAccess();
            $scope.getLastUpdatedDate();
            
            // in po
            $scope.openPOArrInitialPageLoadData = [];
            $scope.getOpenPO = function (plant, purchase, exclusionList) {
                $scope.serchString = '';
                reportingService.GetOpenPOShortageReport(0, '', plant, purchase, exclusionList)
                    .then(function (response) {
                        $log.info(response);

                      //  if ((plant == 0 || plant == '' || plant == null || plant == undefined) && (purchase == 0 || purchase == '' || purchase == null || purchase == undefined))
                      //  {
                       //     $scope.openPOArrInitialPageLoadData = response;
                    //    }
                        if ($scope.openPOArrInitialPageLoadData.length == 0) {
                            $scope.openPOArrInitialPageLoadData = response;
                        }
                        $scope.openPOArr = response;
                        //$scope.openPOArrForSortig = $scope.openPOArr;
                        $scope.openPOArrInitial = response;

                        $scope.openPOArr.forEach(function (item) {
                            item.PO_DATE1 = moment(item.PO_DATE).format('DD-MM-YYYY');
                            item.PR_CREATE_DATE1 = moment(item.PR_CREATE_DATE).format('DD-MM-YYYY');
                            item.PO_DELV_DATE1 = moment(item.PO_DELV_DATE).format('DD-MM-YYYY');
                            item.IDEAL_DELIVERY_DATE1 = moment(item.IDEAL_DELIVERY_DATE).format('DD-MM-YYYY');
                            item.PR_DELV_DATE1 = moment(item.PR_DELV_DATE).format('DD-MM-YYYY');
                            if (item.NWSTATUS == 'REVIEW PENDING') {
                                item.color = 'Red';
                            } else if (item.NWSTATUS == 'REVIEWED BUT ACTION PENDING') {
                                item.color = 'Yellow';
                            } else if (item.NWSTATUS == 'REVIEWED AND ACTION TAKEN') {
                                item.color = 'GreenYellow';
                            }
                            if (item.NEW_DELIVERY_DATE_COMM == null) {
                                item.NEW_DELIVERY_DATE_COMM = '-';
                            } else {
                                item.NEW_DELIVERY_DATE_COMM = moment(item.NEW_DELIVERY_DATE_COMM).format('DD-MM-YYYY');
                            }
                        });
                        $scope.totalItems = $scope.openPOArr.length;
                    });
            }


            $scope.getShortageOpenPOFilter = function (category, idealFromDate, idealToDate, filterModelPurchaseUser) {
                if (filterModelPurchaseUser > 0 && category != "0") {
                    $scope.openPOArr = $scope.openPOArrInitial.filter(function (item) {
                        if (filterModelPurchaseUser == item.PURCHASING_GROUP && category == item.CATEGORY) {
                            return item;
                        }
                    })
                }
                else if (category != "0") {
                    $scope.openPOArr = $scope.openPOArrInitial.filter(function (item) {
                        if (category.toLowerCase() == item.CATEGORY.toLowerCase()) {
                            return item;
                        }
                    })
                }
                else if (filterModelPurchaseUser > 0) {
                     $scope.openPOArr = $scope.openPOArrInitial.filter(function (item) {
                        if (filterModelPurchaseUser == item.PURCHASING_GROUP) {
                            return item;
                        }
                    })
                }
                else {
                    $scope.openPOArr = $scope.openPOArrInitial;
                    //$scope.totalItems = $scope.openPOArr.length;
                }


                if (idealFromDate && idealToDate) {

                    var tsIdealFromDate = moment(idealFromDate, "DD-MM-YYYY HH:mm").valueOf();
                    tsIdealFromDate = moment(tsIdealFromDate);
                    tsIdealFromDate = new Date(tsIdealFromDate);

                    var tsIdealToDate = moment(idealToDate, "DD-MM-YYYY HH:mm").valueOf();
                    tsIdealToDate = moment(tsIdealToDate);
                    tsIdealToDate = new Date(tsIdealToDate);

                    $scope.openPOArr = $scope.openPOArr.filter(function (item) {
                        console.log('------------------------------>');
                        console.log(idealFromDate);
                        console.log(item.IDEAL_DELIVERY_DATE1);
                        console.log(idealToDate);
                        console.log(tsIdealFromDate <= new Date(moment(moment(item.IDEAL_DELIVERY_DATE1, "DD-MM-YYYY HH:mm").valueOf())));
                        console.log(tsIdealToDate >= new Date(moment(moment(item.IDEAL_DELIVERY_DATE1, "DD-MM-YYYY HH:mm").valueOf())));
                        console.log('------------------------------>');
                        if (tsIdealFromDate <= new Date(moment(moment(item.IDEAL_DELIVERY_DATE1, "DD-MM-YYYY HH:mm").valueOf())) &&
                            tsIdealToDate >= new Date(moment(moment(item.IDEAL_DELIVERY_DATE1, "DD-MM-YYYY HH:mm").valueOf()))) {
                            return item;
                        }
                    })
                    $scope.totalItems = $scope.openPOArr.length;
                } else
                {
                    $scope.openPOArr = $scope.openPOArr;
                    $scope.totalItems = $scope.openPOArr.length;
                }
            }


            $scope.exportShortageReportExcel = function () {
                var mystyle = {
                    sheetid: 'PO_Report',
                    headers: true,
                    column: {
                        style: 'font-size:15px;background:#233646;color:#FFF;'
                    }
                };
                alasql.fn.decimalRound = function (x) { return Math.round(x); };   
                alasql('SELECT PURCHASER as [LP], PLANT_NAME as [Plant], MATERIAL as [MATERIAL], SHORT_TEXT as [DESCRIPTION], API_NAME as [API], NAME_OF_SUPPLIER as [SUPPLIER], '+
                    ' PR_CREATE_DATE1 as [PR DATE],PO_DATE1 as [PO DATE],PR_DELV_DATE1 as [PR Delivery Date],IDEAL_DELIVERY_DATE1 as [Ideal Delivery Date], ORDER_UNIT as [ORDER UNIT], ' +
                    ' decimalRound(PR_QUAN) as [PR Qty],decimalRound(ORDER_QUANTITY) as [PO Qty], decimalRound(STILL_TO_BE_DELIVERED_QTY) as [Open Qty], ' +
                    ' PR_NUMBER as [PR Number], PR_ITM_NO as [PR LN], PURCHASING_DOCUMENT as [PO Number], ITEM as [PO LINE ITEM], USER_ID as [PO Creator], MULTIPLE_PR_TEXT as [Multiple PR], ' +
                    ' IMPORT_LOCAL as [Supp. Country], LEAD_TIME as [Lead Time],' +
                    ' CATEGORY as [Category], NWSTATUS as [Status], NEW_DELIVERY_DATE_COMM as [New Delivery Date], COMMENTS as [Comments] ' +
                    ' INTO XLSX(?, { headers: true, sheetid: "PO_Report", style: "font-size:15px;background:#233646;color:#FFF;" }) FROM ? ', ["PO_Report.xlsx", $scope.openPOArr]);
            }


            $scope.scrollWin = function (id, scrollPosition) {
                var elmnt = document.getElementById(id);
                elmnt.scrollIntoView();
                //'+=1500'
                //$(".innerWrapper").animate({ scrollLeft: 100 + 200 }, 800);

                if (scrollPosition == 'BOTTOM')
                {
                    window.scroll({ bottom: elmnt.offsetBottom });
                }
                else {
                    window.scroll({ top: elmnt.offsetTop });
                }
                //window.scroll({ bottom: elmnt.offsetBottom });
                //window.scroll({ top: 10 });

                //window.scrollTo(500, 0);
                //document.body.scrollRight = 0; // For Chrome, Safari and Opera 
                //document.documentElement.scrollRight = 0; // For IE and Firefox

                //var el = $("#" + id);
                //var position = el.position();
                //console.log("left: " + position.left + ", top: " + position.top);
                //window.scroll({ top: position.top, left: position.left });
            };

            document.body.scrollTop = 0; // For Chrome, Safari and Opera 
            document.documentElement.scrollTop = 0; // For IE and Firefox



        }]);