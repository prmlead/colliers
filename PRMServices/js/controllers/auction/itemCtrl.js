﻿prmApp

    // =========================================================================
    // AUCTION ITEM
    // =========================================================================

    .controller('itemCtrl', ["$scope", "$rootScope", "$filter", "$stateParams", "$http", "domain", "fileReader", "$state",
        "$timeout", "auctionsService", "userService", "SignalRFactory", "growlService", "$log", "signalRHubName", "ngDialog",
        "reportingService", "$window", "priceCapServices", "PRMLotReqService", "PRMCustomFieldService", "workflowService", "PRMSurrogateBidService",
        "PRMPRServices", "$q",
        function ($scope, $rootScope, $filter, $stateParams, $http, domain, fileReader, $state, $timeout, auctionsService,
            userService, SignalRFactory, growlService, $log, signalRHubName, ngDialog, reportingService, $window, priceCapServices,
            PRMLotReqService, PRMCustomFieldService, workflowService, PRMSurrogateBidService, PRMPRServices, $q) {
            var liveId = "";
            $scope.tempScopeVariables = {};
            $scope.selectedTemplate = {};
            $scope.prmFieldMappingDetails = {};
            $scope.companyItemUnits = [];
            $scope.requirementSettings = [];
            $scope.selectedcustomFieldList = [];
            var id = $stateParams.Id;
            $scope.reqId = $stateParams.Id;
            $scope.IS_CB_ENABLED = false;
            $scope.IS_CB_NO_REGRET = false;
            $scope.selectedVendorForItemLevelPrices = {};
            $scope.currentUserId = +userService.getUserId();
            $scope.subUsers = [];
            $scope.disableSubmitAllItemApproval = true;
            $scope.isSuperUser = userService.getUserObj().isSuperUser;
            $scope.vendorIncoTerms = {};
            $scope.requirementPRStatus = {
                MESSAGE: '',
                showStatus: false,
                validPRs: ''
            };
            $scope.companyINCOTerms = [];
            $scope.localGSTNumberCode = '36';
            $scope.disableLocalGSTFeature = false;
            $scope.overallPercentage = 0;
            $scope.itemCeilingVendor = 0;
            $scope.isAuctionScheduled = false;
            $scope.rankLevelChanged = false;
            $scope.reductionLevelChanged = false;
            $scope.isTaxesMandatory = true;
            $scope.anyPackagesApproved = false;

            if ($scope.reqId > 0) {
            }
            else {
                $scope.reqId = $stateParams.reqID;
                $stateParams.Id = $stateParams.reqID;
            }

            $scope.overAllValue = {
                overallPercentage: 0,
                overallDiscountPercentage: 0
            };

            /* CLIENT STATUS MAPPING TO PRM STATUS */
            $scope.customerType = userService.getUserType();
            $scope.prmStatus = function (type, status) {
                return userService.NegotiationStatus(type, status);
            };
            /* CLIENT STATUS MAPPING TO PRM STATUS */

            $scope.regretDetails = [];
            $scope.isCustomer = $scope.customerType == "CUSTOMER" ? true : false;
            $scope.listRequirementItemsTemp = [];
            //Pagination
            $scope.totalItems = 0;
            $scope.currentPage = 1;
            $scope.currentPage2 = 1;
            $scope.itemsPerPage = 5;
            $scope.itemsPerPage2 = 5;
            $scope.maxSize = 8;
            //^Pagination

            $scope.currentSessionId = userService.getUserToken();
            $scope.currentUserCompID = userService.getUserCompanyId();
            //#CB-0-2018-12-05
            $scope.goToCbVendor = function (vendorID) {
                $state.go('cb-vendor', { 'reqID': $stateParams.Id, 'vendorID': vendorID });
            };


            $scope.goToAudit = function () {
                var url = $state.href("audit", { "reqID": $scope.reqId });
                window.open(url, '_blank');
            };
            $scope.specificationData = [];
            $scope.numberOfItemDetails = 1;
            $scope.signalRCustomerAccess = false;
            $scope.Loding = false;
            $scope.makeaBidLoding = false;
            $scope.showTimer = false;
            $scope.userIsOwner = false;
            $scope.sessionid = $scope.currentSessionId;
            $scope.allItemsSelected = true;
            $scope.RevQuotationfirstvendor = "";
            $scope.disableBidButton = false;
            $scope.nonParticipatedMsg = '';
            $scope.quotationRejecteddMsg = '';
            $scope.quotationNotviewedMsg = '';
            $scope.revQuotationRejecteddMsg = '';
            $scope.revQuotationNotviewedMsg = '';
            $scope.quotationApprovedMsg = '';
            $scope.revQuotationApprovedMsg = '';
            $scope.reduceBidAmountNote = '';
            $scope.incTaxBidAmountNote = '';
            $scope.noteForBidValue = '';
            $scope.CSGSTFields = false;
            $scope.IGSTFilelds = false;
            $scope.disableDecreaseButtons = true;
            $scope.disableAddButton = true;
            $scope.NegotiationEnded = false;
            $scope.uploadQuotationButtonMsg = 'The Submit Quotation option would be available only if prices are entered above.';
            $scope.uploadQuotationTaxValidationMsg = 'Please check all Tax Fields';
            $scope.currentID = -1;
            $scope.timerStyle = { 'color': '#000' };
            $scope.savingsStyle = { 'color': '#228B22' };
            $scope.restartStyle = { 'color': '#f00' };
            $scope.bidAttachement = [];
            $scope.bidAttachementName = "";
            $scope.bidAttachementValidation = false;
            $scope.bidPriceEmpty = false;
            $scope.bidPriceValidation = false;
            $scope.showStatusDropDown = false;
            $scope.vendorInitialPrice = 0;
            $scope.showGeneratePOButton = false;
            $scope.isDeleted = false;
            $scope.ratingForVendor = 0;
            $scope.ratingForCustomer = 0;
            $scope.participatedVendors = [];
            $scope.vendorApprovals = [];
            $scope.companyGSTInfo = [];
            /*region start WORKFLOW*/
            $scope.workflowList = [];
            $scope.itemWorkflow = [];
            $scope.workflowObj = {};
            $scope.workflowObj.workflowID = 0;
            $scope.currentStep = 0;
            $scope.orderInfo = 0;
            $scope.assignToShow = '';
            $scope.isWorkflowCompleted = false;
            $scope.WorkflowModule = 'VENDOR';
            $scope.disableWFSelection = false;
            /*region end WORKFLOW*/
            $scope.surrogateObj = {};
            $scope.surrogateUsers = [];
            $scope.filteredSubUsers = [];
            $scope.selectedSurrogateVendor = {};
            $scope.showCurrencyRefresh = false;
            $scope.paymentPopUp = false;
            var requirementHub = SignalRFactory('', signalRHubName);
            $scope.auctionItem = {
                auctionVendors: [],
                listRequirementItems: [],
                listRequirementTaxes: []
            };

            $scope.auctionItemTemporary = {
                emailLinkVendors: []
            };

            $scope.checkConnection = function () {
                if (requirementHub) {
                    return requirementHub.getStatus();
                } else {
                    return 0;
                }
            };
            $scope.auctionItemVendor = [];
            $scope.reconnectHub = function () {
                if (requirementHub) {
                    if (requirementHub.getStatus() == 0) {
                        requirementHub.reconnect();
                        return true;
                    }
                } else {
                    requirementHub = SignalRFactory('', signalRHubName);
                }
            };

            $scope.invokeSignalR = function (methodName, params, callback) {
                if ($scope.checkConnection() == 1) {
                    requirementHub.invoke(methodName, params, function (req) {
                        if (callback) {
                            callback();
                        }
                    });
                } else {
                    $scope.reconnectHub();
                    requirementHub.invoke(methodName, params, function (req) {
                        if (callback) {
                            callback();
                        }
                    });
                }
            };

            /*Chat hub*/
            requirementHub.connection.start().done(function () {
                let signalRLotGroupName = '';
                let signalRGroupName = 'requirementGroup' + $stateParams.Id + '_' + $scope.currentUserId;
                signalRLotGroupName = 'requirementGroup_LOT_' + $scope.currentUserId;

                if ($scope.isCustomer) {
                    signalRGroupName = 'requirementGroup' + $stateParams.Id + '_CUSTOMER';
                    signalRLotGroupName = 'requirementGroup_LOT_' + 'CUSTOMER';
                }

                $scope.invokeSignalR('joinGroup', signalRGroupName);
                $scope.invokeSignalR('joinGroup', 'chatGroup' + $scope.reqId);

                if (signalRLotGroupName) {
                    $scope.invokeSignalR('joinGroup', signalRLotGroupName);
                }
            });

            requirementHub.on('chatUpdate', function (payLoad) {
                if (payLoad && !payLoad.IsCustomer) {
                    toastr.info(payLoad.FromName + ' left a message: ' + payLoad.Message);
                }
                if (payLoad && payLoad.IsCustomer && payLoad.ToId === $scope.currentUserId) {
                    toastr.info('Customer left a message: ' + payLoad.Message);
                }
            });
            /*^Chat Hub*/

            $scope.setOptions = function () {
                toastr.options.positionClass = "toast-top-right";
                toastr.options.closeButton = true;
                toastr.options.showMethod = 'slideDown';
                toastr.options.hideMethod = 'slideUp';
                //toastr.options.newestOnTop = false;
                toastr.options.progressBar = true;
            };

            $scope.setOptions();



            var requirementData = {};
            $scope.quotationAttachment = null;
            $scope.days = 0;
            $scope.hours = 0;
            $scope.mins = 0;
            $scope.NegotiationSettings = {
                negotiationDuration: ''
            };
            $scope.divfix = {};
            $scope.divfix3 = {};
            $scope.divfix1 = {};
            $scope.divfix2 = {};
            $scope.divfixMakeabid = {};
            $scope.divfixMakeabidError = {};
            $scope.boxfix = {};
            $scope.NegotiationSettings.negotiationDuration = $scope.days + ' ' + $scope.hours + ':' + $scope.mins + ':00.0';
            $scope.auctionItem.reqType = 'REGULAR';
            $scope.auctionItem.priceCapValue = 0;
            $scope.auctionItem.isUnitPriceBidding = 1;
            $scope.$on("$destroy", function () {
                $log.info('disconecting signalR');
                requirementHub.stop();
                $log.info('disconected signalR');
            });
            $scope.clickToOpen = function () {
                ngDialog.open({ template: 'login/termsUpdateStartTime.html', width: 1000, height: 500 });
            };

            $scope.updateTimeLeftSignalR = function () {
                var parties = id + "$" + $scope.currentUserId + "$" + "10000" + "$" + $scope.currentSessionId;
                $scope.invokeSignalR('UpdateTime', parties);
            };

            $scope.subIemsColumnsVisibility = {
                hideSPECIFICATION: true,
                hideQUANTITY: true,
                hideTAX: true,
                hidePRICE: true
            };

            $scope.newPriceToBeQuoted = 0;

            $scope.reduceBidAmount = function () {
                if (!isNaN($scope.reduceBidValue) && $scope.reduceBidValue != "" && $scope.reduceBidValue >= 0 && $scope.auctionItem.minPrice > $scope.reduceBidValue) {
                    $("#reduceBidValue").val($scope.reduceBidValue);


                    $("#makebidvalue").val($scope.precisionRound(parseFloat($scope.auctionItem.minPrice - $scope.reduceBidValue), $rootScope.companyRoundingDecimalSetting));
                    $("#reduceBidValue1").val($scope.precisionRound(parseFloat($scope.auctionItem.minPrice - $scope.reduceBidValue), $rootScope.companyRoundingDecimalSetting));

                    //console.log($scope.auctionItem.minPrice - $scope.reduceBidValue);

                    //angular.element($event.target).parent().addClass('fg-line fg-toggled');
                }
                else {
                    $("#makebidvalue").val($scope.auctionItem.minPrice);
                    //swal("Error!", "Invalid bid value.", "error");
                    $("#reduceBidValue1").val($scope.auctionItem.minPrice);
                    $scope.reduceBidValue = 0;
                    $("#reduceBidValue").val($scope.reduceBidValue);
                }


            };

            $scope.bidAmount = function () {
                if ($scope.vendorBidPrice != "" && $scope.vendorBidPrice >= 0 && $scope.auctionItem.minPrice > $scope.vendorBidPrice) {
                    $("#reduceBidValue").val($scope.auctionItem.minPrice - $scope.vendorBidPrice);
                    /*angular.element($event.target).parent().addClass('fg-line fg-toggled');*/
                }
                else {
                    $("#reduceBidValue").val(0);
                    swal("Error!", "Invalid bid value.", "error");
                    $scope.vendorBidPrice = 0;
                }
            };

            $scope.formRequest = {
                overallItemLevelComments: '',
                rankLevel: 'BOTH',
                reductionLevel: 'OVERALL',
                selectedVendor: {},
                priceCapValue: 0,
                priceCapValueMsg: ''
            };

            $scope.selectVendor = function () {
                var selVendID = $scope.formRequest.selectedVendor.vendorID;
                var winVendID = $scope.auctionItem.auctionVendors[0].vendorID;
                if (!$scope.formRequest.selectedVendor.reason && selVendID != winVendID) {
                    growlService.growl("Please enter the reason for choosing the particular vendor", "inverse");
                    return false;
                }
                var params = {
                    userID: $scope.currentUserId,
                    vendorID: $scope.formRequest.selectedVendor.vendorID,
                    reqID: $scope.auctionItem.requirementID,
                    reason: $scope.formRequest.selectedVendor.reason ? $scope.formRequest.selectedVendor.reason : (selVendID != winVendID ? $scope.formRequest.selectedVendor.reason : ""),
                    sessionID: $scope.currentSessionId
                };
                auctionsService.selectVendor(params)
                    .then(function (response) {
                        if (response.errorMessage == "") {
                            growlService.growl("Vendor " + response.userInfo.firstName + " " + response.userInfo.lastName + " has been selected for the final  ", "inverse");
                            $scope.getData();
                        }
                    });
            };

            $scope.exportItemsToExcel = function () {
                var mystyle = {
                    sheetid: 'QuotationDetails',
                    headers: true,
                    column: {
                        style: 'font-size:15px;background:#233646;color:#FFF;'
                    }
                };

                if ($scope.auctionItem.isDiscountQuotation == 0 || $scope.auctionItem.isDiscountQuotation == 1) {
                    var name = '';
                    if ($scope.auctionItem.status == 'UNCONFIRMED' || $scope.auctionItem.status == 'NOTSTARTED') {
                        name = 'UNIT_QUOTATION_' + $scope.reqId;
                    } else {
                        name = 'UNIT_BIDDING_' + $scope.reqId;
                    }

                    reportingService.downloadTemplate(name, $scope.currentUserId, $scope.reqId);
                }

                //if ($scope.auctionItem.isDiscountQuotation == 1) {
                //    alasql('SELECT itemID as [ItemID], productIDorName as [ProductName], productNo as [ProductNumber], productDescription as [Description], productQuantity as [Quantity], productBrand as Brand, productQuantityIn as [Units],unitMRP as UnitMRP, unitDiscount as UnitDiscount, cGst, sGst, iGst INTO XLSX(?,{headers:true,sheetid: "QuotationDetails", style: "font-size:15px;background:#233646;color:#FFF;"}) FROM ?', ["QuotationDetails.xlsx", $scope.auctionItemVendor.listRequirementItems]);
                //}
            };

            $scope.exportItemsToExcelForCeiling = function () {
                var name = 'CEILING_DETAILS_' + $scope.reqId;
                let userId = $scope.currentUserId;
                if ($scope.itemCeilingVendor && $scope.itemCeilingVendor.vendorID) {
                    userId = $scope.itemCeilingVendor.vendorID;
                }

                reportingService.downloadTemplate(name, userId, $scope.reqId);
            };

            $scope.rateVendor = function (vendorID) {
                var params = {
                    uID: $scope.currentUserId,
                    userID: vendorID,
                    rating: $scope.ratingForVendor,
                    sessionID: $scope.currentSessionId
                };
                auctionsService.rateVendor(params)
                    .then(function (response) {
                        if (response.errorMessage == "") {
                            growlService.growl("Rating saved successfully. You can edit your rating as well.", "inverse");
                        }
                    });
            };

            $scope.rateCustomer = function () {
                var params = {
                    uID: $scope.currentUserId,
                    userID: $scope.auctionItem.customerID,
                    rating: $scope.ratingForCustomer,
                    sessionID: $scope.currentSessionId
                };

                auctionsService.rateVendor(params)
                    .then(function (response) {
                        if (response.errorMessage == "") {
                            growlService.growl("Rating saved successfully. You can edit your rating as well.", "inverse");
                        }
                    });
            };

            $scope.makeABidDisable = false;

            $scope.makeaBid1 = function () {
                let currentVendorFactor = ($scope.auctionItem.auctionVendors[0].vendorCurrencyFactor);
                let isValidItemPrice = true;
                let isValidItemPriceReduction = true;
                $scope.makeABidDisable = true;
                var bidPrice = $("#makebidvalue").val();
                let containsItemsPriceMoreThanCeilingPrice = false;

                $scope.auctionItemVendor.listRequirementItems.forEach(function (vendorItem, itemIndexs) {
                    if (vendorItem.revUnitPrice) {
                        var requirementItem = $scope.auctionItem.listRequirementItems.filter(function (reqItem) {
                            return reqItem.itemID === vendorItem.itemID;
                        });

                        requirementItem[0].ceilingPriceFactor = (requirementItem[0].ceilingPrice / currentVendorFactor);
                        if (requirementItem && requirementItem.length > 0 && requirementItem[0].ceilingPrice && +vendorItem.revUnitPriceOld && +vendorItem.revUnitPrice > requirementItem[0].ceilingPriceFactor) {
                            vendorItem.revUnitPrice = vendorItem.revUnitPriceOld ? vendorItem.revUnitPriceOld : vendorItem.revUnitPrice;
                            isValidItemPrice = false;
                            $scope.makeABidDisable = false;
                            return false;
                        } else if (requirementItem && requirementItem.length > 0 && requirementItem[0].ceilingPrice && +vendorItem.revUnitPriceOld && +vendorItem.revUnitPriceOld > requirementItem[0].ceilingPrice && +vendorItem.revUnitPrice <= requirementItem[0].ceilingPriceFactor) {
                            if (vendorItem.revUnitPrice < (0.75 * requirementItem[0].ceilingPriceFactor)) {
                                vendorItem.revUnitPrice = vendorItem.revUnitPriceOld ? vendorItem.revUnitPriceOld : vendorItem.revUnitPrice;
                                isValidItemPriceReduction = false;
                                $scope.makeABidDisable = false;
                                return false;
                            }
                        }
                    }




                    if (requirementItem && requirementItem.length > 0 && requirementItem[0].ceilingPrice && +vendorItem.revUnitPriceOld > requirementItem[0].ceilingPriceFactor) {
                        containsItemsPriceMoreThanCeilingPrice = true;
                    }
                });

                if (!isValidItemPrice) {
                    swal("Error!", "Your item revised amount should be less than Item Ceiling amount", 'error');
                    $scope.makeABidDisable = false;
                    $("#makebidvalue").val("");
                    $("#reduceBidValue").val("");
                    $scope.getData();
                    return false;
                }

                if (!isValidItemPriceReduction) {
                    swal("Error!", "You are reducing more than 25% of current item amount. The Maximum reduction amount per item should not exceed more than 25% from item ceiling amount", 'error');
                    $scope.makeABidDisable = false;
                    $("#makebidvalue").val("");
                    $("#reduceBidValue").val("");
                    $scope.getData();
                    return false;
                }

                if (bidPrice == "" || Number(bidPrice) <= 0) {
                    $scope.bidPriceEmpty = true;
                    $scope.bidPriceValidation = false;
                    $("#makebidvalue").val("");
                    $scope.makeABidDisable = false;
                    return false;
                } else if (!isNaN($scope.auctionItem.minPrice) && $scope.auctionItem.minPrice > 0 && +bidPrice >= $scope.auctionItem.minPrice) {
                    $scope.bidPriceValidation = true;
                    $scope.bidPriceEmpty = false;
                    $scope.makeABidDisable = false;
                    $scope.makeABidDisable = false;
                    return false;
                } else {
                    $scope.bidPriceValidation = false;
                    $scope.bidPriceEmpty = false;
                }
                if (($scope.bidAttachementName == "" || $scope.bidAttachement.length == 0) && $scope.quotationStatus == false) {
                    $scope.bidAttachementValidation = true;
                    $scope.makeABidDisable = false;
                    return false;
                } else {
                    $scope.bidAttachementValidation = false;
                }
                if (bidPrice > $scope.auctionItem.auctionVendors[0].runningPrice - ($scope.auctionItem.minBidAmount / currentVendorFactor)) {
                    $scope.reduceBidValue = "";
                    $("#makebidvalue").val("");
                    $("#reduceBidValue").val("");
                    $scope.getData();
                    swal("Error!", "Your amount must be at least " + $scope.precisionRound(($scope.auctionItem.minBidAmount / currentVendorFactor), $rootScope.companyRoundingDecimalSetting) + " less than your previous bid.", 'error');
                    $scope.makeABidDisable = false;
                    return false;
                }
                if (bidPrice < (75 * $scope.auctionItem.auctionVendors[0].runningPrice / 100) && !containsItemsPriceMoreThanCeilingPrice) {
                    $scope.reduceBidValue = "";
                    $("#makebidvalue").val("");
                    $("#reduceBidValue").val("");
                    $scope.getData();
                    swal("Maximum Reduction Error!", " You are reducing more than 25% of current bid amount. The Maximum reduction amount per bid should not exceed more than 25% from current bid amount  " + $scope.auctionItem.minPrice + ". Incase if You want to Reduce more Please Do it in Multiple Bids", "error");
                    $scope.makeABidDisable = false;
                    return false;
                }
                else {
                    var params = {};
                    params.reqID = parseInt(id);
                    params.sessionID = $scope.currentSessionId;
                    params.userID = $scope.currentUserId;
                    params.price = $scope.precisionRound(parseFloat(bidPrice), $rootScope.companyRoundingDecimalSetting);
                    params.quotationName = $scope.bidAttachementName;
                    params.ignorevalidations = containsItemsPriceMoreThanCeilingPrice;
                    params.payment = validateStringWithoutSpecialCharacters($scope.payment);

                    if (params.payment == null || params.payment == '' || params.payment == undefined) {
                        params.payment = '';
                        swal({
                            title: "Error",
                            text: "Please enter the Payment Terms",
                            type: "error",
                            showCancelButton: false,
                            confirmButtonColor: "#DD6B55",
                            confirmButtonText: "Ok",
                            closeOnConfirm: true
                        });
                        $scope.makeABidDisable = false;
                        return false;
                    }

                    params.quotationObject = $scope.auctionItemVendor.listRequirementItems;
                    params.vendorBidPrice = $scope.revvendorBidPrice;
                    params.itemrevtotalprice = $scope.revtotalprice;

                    if ($scope.auctionItem.isUnitPriceBidding == 1) {
                        $scope.items = {
                            itemsList: $scope.auctionItemVendor.listRequirementItems,
                            userID: $scope.currentUserId,
                            reqID: params.reqID,
                            price: $scope.revtotalprice,
                            vendorBidPrice: $scope.revvendorBidPrice
                        };
                    }

                    requirementHub.invoke('MakeBid', params, function (req) {

                        if (req.errorMessage == '') {
                            $scope.makeABidDisable = false;
                            $scope.overAllValue.overallPercentage = 0;
                            $scope.overAllValue.overallDiscountPercentage = 0;
                            swal("Thanks !", "Your bidding process has been successfully updated", "success");
                            $scope.overAllValue.overallPercentage = 0;
                            $scope.overAllValue.overallDiscountPercentage = 0;

                            if ($scope.auctionItemVendor.listRequirementItems.length > 20) {
                                if ($scope.auctionItem.isUnitPriceBidding == 1) {
                                    if ($scope.items.revinstallationCharges == null || $scope.items.revinstallationCharges == '' || $scope.items.revinstallationCharges == undefined || isNaN($scope.items.revinstallationCharges)) {
                                        $scope.items.revinstallationCharges = 0;
                                    }
                                    $scope.items.itemsList.forEach(function (item, itemIndexs) {
                                        if (item.productQuotationTemplateArray && item.productQuotationTemplateArray.length > 0) {
                                            item.productQuotationTemplateArray.forEach(function (subItem, subItemIndex) {
                                                subItem.dateCreated = "/Date(1561000200000+0530)/";
                                                subItem.dateModified = "/Date(1561000200000+0530)/";
                                            });
                                        }
                                        item.productQuotationTemplateJson = JSON.stringify(item.productQuotationTemplateArray);
                                    });

                                    auctionsService.SaveRunningItemPrice($scope.items)
                                        .then(function (response) {
                                            if (response.errorMessage != "") {
                                                growlService.growl(response.errorMessage, "inverse");
                                            } else {
                                                $scope.recalculate('', 0, false);
                                            }
                                        });
                                }
                            }

                            $scope.reduceBidValue = "";
                            $("#makebidvalue").val("");
                            $("#reduceBidValue").val("");

                        } else {
                            if (req.errorMessage != '') {

                                $scope.getData();
                                $scope.makeABidDisable = false;
                                var parties = id + "$" + $scope.currentUserId + "$" + $scope.currentSessionId;
                                requirementHub.invoke('CheckRequirement', parties, function () {

                                    $scope.reduceBidValue = "";
                                    $("#makebidvalue").val("");
                                    $("#reduceBidValue").val("");

                                    var htmlContent = 'We have some one in this range <h3>' + req.errorMessage + ' </h3> Kindly Quote some other value.'

                                    swal("Cancelled", "", "error");
                                    $(".sweet-alert h2").html("oops...! Your Price is Too close to another Bid <br> We have some one in this range <h3 style='color:red'>" + req.errorMessage + " </h3> Kindly Quote some other value.");

                                });

                            } else {
                                $scope.makeABidDisable = false;
                                swal("Error!", req.errorMessage, "error");
                            }
                        }
                    });
                }
            };

            $scope.recalculate = function (subMethodName, receiverId, showswal) {

                if (subMethodName == null || subMethodName == '' || subMethodName == undefined) {
                    subMethodName = '';
                }

                if (receiverId < 0) {
                    receiverId = -1;
                }

                var params = {};
                params.reqID = id;
                params.sessionID = $scope.currentSessionId;
                params.userID = $scope.currentUserId;
                var parties = id + "$" + $scope.currentUserId + "$" + $scope.currentSessionId + "$" + subMethodName + "$" + receiverId;
                $scope.invokeSignalR('CheckRequirement', parties);
                if (showswal) {
                    swal("done!", "a refresh command has been sent to everyone.", "success");
                }
            };

            $scope.setFields = function () {
                if ($scope.auctionItem.status == "CLOSED") {
                    $scope.mactrl.skinSwitch('green');
                    if (($scope.auctionItem.customerID == $scope.currentUserId || $scope.auctionItem.superUserID == $scope.currentUserId || $scope.auctionItem.customerReqAccess)) {
                        $scope.errMsg = "Negotiation has been completed. You can generate the Purchase Order by pressing the button below.";
                    }
                    else {
                        $scope.errMsg = "Negotiation has completed.";
                    }
                    $scope.showStatusDropDown = false;
                    $scope.showGeneratePOButton = true;
                } else if ($scope.auctionItem.status == "UNCONFIRMED" || $scope.auctionItem.status == "NOTSTARTED") {
                    $scope.mactrl.skinSwitch('teal');
                    $scope.errMsg = "Negotiation has not started yet.";
                    $scope.showStatusDropDown = false;
                    $scope.auctionStarted = false;
                    $scope.isAuctionScheduled = true;
                    $scope.timeLeftMessage = "Negotiation Starts in: ";
                    $scope.startBtns = true;
                    $scope.customerBtns = false;
                } else if ($scope.auctionItem.status == "STARTED") {
                    $scope.mactrl.skinSwitch('orange');
                    $scope.errMsg = "Negotiation has started.";
                    $scope.showStatusDropDown = false;
                    $scope.auctionStarted = true;
                    $scope.isAuctionScheduled = false;
                    // Dont Change This Text "Negotiation Ends in: " it has dependency in ENDNEGOTIATION
                    $scope.timeLeftMessage = "Negotiation Ends in: ";
                    // Dont Change This Text "Negotiation Ends in: " it has dependency in ENDNEGOTIATION

                    $scope.startBtns = false;
                    $scope.customerBtns = true;
                } else if ($scope.auctionItem.status == "DELETED") {
                    $scope.mactrl.skinSwitch('bluegray');
                    $scope.errMsg = "This requirement has been cancelled.";
                    $scope.showStatusDropDown = false;
                    $scope.isDeleted = true;
                } else if ($scope.auctionItem.status == "Negotiation Ended") {
                    $scope.mactrl.skinSwitch('bluegray');
                    $scope.errMsg = "Negotiation has been completed.";
                    $scope.showStatusDropDown = false;
                } else if ($scope.auctionItem.status == "Vendor Selected") {
                    $scope.mactrl.skinSwitch('bluegray');
                    if ($scope.auctionItem.isTabular) {
                        $scope.errMsg = "Please select vendors for all items in order to provide Purchase Order Information.";
                    } else {
                        $scope.errMsg = "Please click the button below to provide the Purchase Order information.";
                    }
                    $scope.showStatusDropDown = false;
                } else if ($scope.auctionItem.status == "PO Processing") {
                    $scope.mactrl.skinSwitch('lightblue');
                    $scope.errMsg = "The PO has been generated. Please find the PO here: ";
                    $scope.showStatusDropDown = false;
                } else {
                    $scope.mactrl.skinSwitch('lightblue');
                    $scope.showStatusDropDown = true;
                }
                if (($scope.auctionItem.customerID == $scope.currentUserId || $scope.auctionItem.superUserID == $scope.currentUserId || $scope.auctionItem.customerReqAccess)) {
                    $scope.userIsOwner = true;
                    if ($scope.userIsOwner && !$rootScope.isUserEntitled(546654) && $scope.auctionItem.status == "STARTED") {
                        swal("Error!", "Live negotiation Access Denined", "error");
                        $state.go('home');
                    }
                    if ($scope.userIsOwner && !$rootScope.isUserEntitled(591159)) {
                        swal("Error!", "View Requirement Access Denied", "error");
                        $state.go('home');
                    }
                    $scope.options = ['PO Sent', 'Material Received', 'Payment Processing', 'Payment Released'];
                    $scope.options.push($scope.auctionItem.status);
                }
                var start = $scope.auctionItem.startTime.split('+')[0].split('(')[1];
                auctionsService.getdate()
                    .then(function (responseFromServer) {
                        var dateFromServer = new Date(parseInt(responseFromServer.substr(6)));
                        $log.debug(dateFromServer);
                        var curDate = dateFromServer;

                        var myEpoch = curDate.getTime();
                        $scope.timeLeftMessage = "";
                        if (start > myEpoch) {
                            $scope.auctionStarted = false;
                            $scope.isAuctionScheduled = true;
                            $scope.timeLeftMessage = "Negotiation Starts in: ";
                            $scope.startBtns = true;
                            $scope.customerBtns = false;
                        } else {
                            $scope.auctionStarted = true;
                            $scope.isAuctionScheduled = false;
                            // Dont Change This Text "Negotiation Ends in: " it has dependency in ENDNEGOTIATION
                            $scope.timeLeftMessage = "Negotiation Ends in: ";
                            // Dont Change This Text "Negotiation Ends in: " it has dependency in ENDNEGOTIATION

                            $scope.startBtns = false;
                            $scope.customerBtns = true;
                        }
                        if ($scope.auctionItem.customerID != $scope.currentUserId && $scope.auctionItem.superUserID != $scope.currentUserId && !$scope.auctionItem.customerReqAccess) {
                            $scope.startBtns = false;
                            $scope.customerBtns = false;
                        }
                        if ($scope.auctionItem.timeLeft == null || $scope.auctionItem.timeLeft < 1) {
                            $scope.showTimer = false;
                            $scope.disableButtons();
                        } else {
                            $scope.showTimer = true;
                        }
                        //$scope.auctionItem.postedOn = new Date(parseFloat($scope.auctionItem.postedOn.substr(6)));
                        //var date = $scope.auctionItem.postedOn.split('+')[0].split('(')[1];
                        //var newDate = new Date(parseInt(parseInt(date)));
                        //$scope.auctionItem.postedOn = newDate.toString().replace('Z', '');

                        //var date1 = $scope.auctionItem.deliveryTime.split('+')[0].split('(')[1];
                        //var newDate1 = new Date(parseInt(parseInt(date1)));
                        //$scope.auctionItem.deliveryTime = newDate1.toString().replace('Z', '');

                        //var date2 = $scope.auctionItem.quotationFreezTime.split('+')[0].split('(')[1];
                        //var newDate2 = new Date(parseInt(parseInt(date2)));
                        //$scope.auctionItem.quotationFreezTime = newDate2.toString().replace('Z', '');

                        // // #INTERNATIONALIZATION-0-2019-02-12
                        $scope.auctionItem.postedOn = userService.toLocalDate($scope.auctionItem.postedOn);

                        var minPrice = 0;
                        if ($scope.auctionItem.status == "NOTSTARTED") {
                            $scope.auctionItem.minPrice = $scope.auctionItem.auctionVendors[0] ? $scope.auctionItem.auctionVendors[0].initialPrice : 0;
                        } else {
                            $scope.auctionItem.minPrice = $scope.auctionItem.auctionVendors[0] ? $scope.auctionItem.auctionVendors[0].runningPrice : 0;
                        }
                        //$scope.auctionItem.deliveryTime = $scope.auctionItem.deliveryTime.split("GMT")[0];
                        for (var i in $scope.auctionItem.auctionVendors) {

                            $scope.nonParticipatedMsg = '';
                            $scope.quotationRejecteddMsg = '';
                            $scope.quotationNotviewedMsg = '';
                            $scope.revQuotationRejecteddMsg = '';
                            $scope.revQuotationNotviewedMsg = '';
                            $scope.quotationApprovedMsg = '';
                            $scope.revQuotationApprovedMsg = '';

                            var vendor = $scope.auctionItem.auctionVendors[i];
                            if (vendor.vendorID === $scope.currentUserId && vendor.quotationUrl === "") {
                                $scope.quotationStatus = false;
                                $scope.quotationUploaded = false;
                            } else {
                                $scope.quotationStatus = true;
                                if ($scope.auctionItem.customerID != $scope.currentUserId && $scope.auctionItem.superUserID != $scope.currentUserId && !$scope.auctionItem.customerReqAccess) {
                                    $scope.quotationUploaded = true;
                                }

                                $scope.quotationUrl = vendor.quotationUrl;
                                $scope.revquotationUrl = vendor.revquotationUrl;
                                if (!$scope.vendorQuotedPrice) {
                                    $scope.vendorQuotedPrice = vendor.runningPrice;
                                }
                            }

                            if (i == 0 && vendor.initialPrice != 0) {
                                minPrice = vendor.initialPrice;
                            } else {
                                if (vendor.initialPrice < minPrice && vendor.initialPrice != 0) {
                                    minPrice = vendor.initialPrice;
                                }
                            }
                            $scope.vendorInitialPrice = minPrice;
                            var runningMinPrice = 0;
                            if ($scope.auctionItem.auctionVendors[i].runningPrice > 0 && $scope.auctionItem.auctionVendors[i].runningPrice < $scope.vendorInitialPrice) {
                                runningMinPrice = $scope.auctionItem.auctionVendors[i].runningPrice;
                            }
                            //$scope.auctionItem.minPrice = runningMinPrice;
                            if ($scope.auctionItem.auctionVendors[i].runningPrice == 0) {
                                $scope.auctionItem.auctionVendors[i].runningPrice = 0.00;
                                $scope.auctionItem.auctionVendors[i].totalPriceIncl = 0.00;
                                $scope.auctionItem.auctionVendors[i].rank = 'NA';
                            } else {
                                $scope.vendorRank = vendor.rank;
                                if (vendor.rank == 1) {
                                    $scope.toprankerName = vendor.vendorName;
                                    if ($scope.currentUserId == vendor.vendorID) {
                                        $scope.options = ['PO Accepted', 'Material Dispatched', 'Payment Acknowledged'];
                                        $scope.options.push($scope.auctionItem.status);
                                        if ($scope.auctionItem.status == "STARTED") {
                                            $scope.enableMakeBids = true;
                                        }
                                    }
                                }
                                //$scope.auctionItem.auctionVendors[i].totalPriceIncl = $scope.auctionItem.auctionVendors[i].runningPrice + ($scope.auctionItem.auctionVendors[i].runningPrice * $scope.auctionItem.auctionVendors[i].taxes) / 100;
                                $scope.auctionItem.auctionVendors[i].totalPriceIncl = $scope.auctionItem.auctionVendors[i].runningPrice;
                            }
                            if ($scope.auctionItem.auctionVendors[i].initialPrice == 0) {
                                $scope.auctionItem.auctionVendors[i].initialPrice = 0.00;
                                $scope.nonParticipatedMsg = $scope.showMessageBasedOnBiddingType('You have missed an opportunity to participate in this Negotiation.', 'NEG_STARTED');
                            }

                            if ($scope.auctionItem.auctionVendors[i].isQuotationRejected == 1) {
                                $scope.quotationRejecteddMsg = 'Your quotation has not been shortlisted. Please re submit.';
                            }
                            else if ($scope.auctionItem.auctionVendors[i].isQuotationRejected == -1) {
                                $scope.quotationNotviewedMsg = 'Your quotation submitted to the customer.';
                            }
                            else if ($scope.auctionItem.auctionVendors[i].isRevQuotationRejected == 1 && $scope.auctionItem.status == "Negotiation Ended") {
                                $scope.revQuotationRejecteddMsg = 'Your Rev.Quotation has not been shortlisted. Please re submit.';
                            }
                            else if ($scope.auctionItem.auctionVendors[i].isRevQuotationRejected == -1 && $scope.auctionItem.status == 'Negotiation Ended') {
                                $scope.revQuotationNotviewedMsg = 'Your Rev.Quotation submitted to the customer.';
                            }

                            if ($scope.auctionItem.auctionVendors[i].isQuotationRejected == 0) {
                                $scope.quotationApprovedMsg = $scope.showMessageBasedOnBiddingType('Your quotation has been shortlisted for further process.', 'INITIAL_QUOTE');
                            }
                            if ($scope.auctionItem.auctionVendors[i].isQuotationRejected == 0 && $scope.itemQuotationsMessage()) {
                                $scope.quotationApprovedMsg = $scope.itemQuotationsMessage();
                            }
                            if ($scope.auctionItem.auctionVendors[i].isRevQuotationRejected == 0) {
                                $scope.revQuotationApprovedMsg = 'Your Rev.Quotation Approved by the customer.';
                            }

                        }
                        $scope.$broadcast('timer-set-countdown-seconds', $scope.auctionItem.timeLeft);
                        $('.datetimepicker').datetimepicker({
                            useCurrent: false,
                            icons: {
                                time: 'glyphicon glyphicon-time',
                                date: 'glyphicon glyphicon-calendar',
                                up: 'glyphicon glyphicon-chevron-up',
                                down: 'glyphicon glyphicon-chevron-down',
                                previous: 'glyphicon glyphicon-chevron-left',
                                next: 'glyphicon glyphicon-chevron-right',
                                today: 'glyphicon glyphicon-screenshot',
                                clear: 'glyphicon glyphicon-trash',
                                close: 'glyphicon glyphicon-remove'

                            },

                            minDate: curDate
                        });
                    })


                $scope.reduceBidAmountNote = 'Specify the value to be reduce from bid Amount.';
                //$scope.incTaxBidAmountNote = 'Please Enter the consolidate bid value including tax amount.';
                $scope.incTaxBidAmountNote = '';
                //$scope.noteForBidValue = 'NOTE : If you fill one field, other will be autocalculated.';
                $scope.noteForBidValue = '';

                if ($scope.auctionItem.status == "STARTED") {
                    liveId = "live";
                }
                else {
                    liveId = "";
                }

            };

            $scope.vendorBidPrice = 0;
            $scope.revvendorBidPrice = 0;
            $scope.auctionStarted = true;
            $scope.customerBtns = true;
            $scope.showVendorTable = true;
            $scope.quotationStatus = true;
            $scope.toprankerName = "";
            $scope.vendorRank = 0;
            $scope.quotationUrl = "";
            $scope.revquotationUrl = "";
            $scope.vendorBtns = false;
            $scope.vendorQuotedPrice = 0;
            $scope.startBtns = false;
            $scope.commentsvalidation = false;
            $scope.enableMakeBids = false;
            $scope.price = "";
            $scope.startTime = '';
            $scope.customerID = $scope.currentUserId;

            $scope.priceSwitch = 0;

            $scope.poDetails = {};

            $scope.bidHistory = {};

            $scope.GetBidHistory = function () {
                auctionsService.GetBidHistory({ "reqid": $stateParams.Id, "sessionid": $scope.currentSessionId, 'userid': $scope.currentUserId })
                    .then(function (response) {
                        $scope.bidHistory = response;
                    });
            };

            $scope.vendorID = 0;

            $scope.validity = '';


            //if ($scope.auctionItem.urgency == 'Critical (Will be Closed Today)') {
            //    $scope.validity = '1 Day';
            //}
            //if ($scope.auctionItem.urgency == 'High (Will be Closed in 2 Days)') {
            //    $scope.validity = '2 Days';
            //}
            //if ($scope.auctionItem.urgency == 'Medium (Will be Closed in 3 Days)') {
            //    $scope.validity = '3 Days';
            //}
            //if ($scope.auctionItem.urgency == 'Low (Will Take 7 Days Time)') {
            //    $scope.validity = '7 Days';
            //}

            /*$scope.warranty = 'As Per OEM';*/
            $scope.duration = $scope.auctionItem.deliveryTime;
            $scope.payment = $scope.auctionItem.paymentTerms;
            $scope.gstNumber = $scope.auctionItem.gstNumber;
            $scope.isQuotationRejected = -1;
            $scope.starttimecondition1 = 0;
            $scope.starttimecondition2 = 0;
            $scope.revQuotationUrl1 = 0;
            $scope.L1QuotationUrl = 0;
            $scope.notviewedcompanynames = '';
            $scope.starreturn = false;
            $scope.customerListRequirementTerms = [];
            $scope.customerDeliveryList = [];
            $scope.customerPaymentlist = [];
            $scope.listTerms = [];
            $scope.listRequirementTerms = [];
            $scope.deliveryRadio = false;
            $scope.deliveryList = [];
            $scope.paymentRadio = false;
            $scope.paymentlist = [];
            $scope.vendorsFromPRM = 1;
            $scope.vendorsFromSelf = 2;
            $scope.totalprice = 0;
            $scope.taxs = 0;
            $scope.vendorTaxes = 0;
            $scope.discountAmount = 0;
            $scope.totalpriceinctaxfreight = 0;
            $scope.vendorBidPriceWithoutDiscount = 0;
            $scope.revtotalprice = 0;
            $scope.revtaxs = 0;
            $scope.revvendorTaxes = $scope.vendorTaxes;
            $scope.revtotalpriceinctaxfreight = 0;
            $scope.priceValidationsVendor = '';
            $scope.calculatedSumOfAllTaxes = 0;
            $scope.taxValidation = false;
            $scope.ItemPriceValidation = false;
            $scope.discountfreightValidation = false;
            $scope.gstValidation = false;
            $scope.freightCharges = 0;
            $scope.revfreightCharges = 0;
            $scope.packingCharges = 0;
            $scope.revpackingCharges = 0;
            $scope.installationCharges = 0;
            $scope.revinstallationCharges = 0;

            $scope.additionalSavings = 0;

            $scope.getpricecomparison = function () {
                auctionsService.getpricecomparison({ "reqid": $scope.reqId, "sessionid": $scope.currentSessionId, 'userid': $scope.currentUserId })
                    .then(function (response) {
                        $scope.priceCompObj = response;

                        $scope.totalItemsL1Price = 0;
                        $scope.totalItemsMinimunPrice = 0;
                        $scope.negotiationSavings = 0;
                        $scope.savingsByLeastBidder = 0;
                        $scope.savingsByItemMinPrice = 0;

                        if ($scope.priceCompObj && $scope.priceCompObj.priceCompareObject && $scope.priceCompObj.priceCompareObject.length > 0) {
                            for (var i = 0; i < $scope.priceCompObj.priceCompareObject.length; i++) {
                                $scope.totalItemsL1Price += $scope.priceCompObj.priceCompareObject[i].leastBidderPrice;
                                $scope.totalItemsMinimunPrice += $scope.priceCompObj.priceCompareObject[i].minPrice;
                            }
                        }

                        $scope.negotiationSavings = $scope.priceCompObj.requirement.savings;
                        $scope.savingsByLeastBidder = $scope.priceCompObj.minQuotationPrice - $scope.totalItemsL1Price;
                        $scope.savingsByItemMinPrice = $scope.priceCompObj.minQuotationPrice - $scope.totalItemsMinimunPrice;

                        $scope.additionalSavings = 0;

                        $scope.additionalSavings = $scope.totalItemsL1Price - $scope.totalItemsMinimunPrice;

                    });
            };

            $scope.GetRequirementTerms = function () {
                auctionsService.GetRequirementTerms($scope.currentUserId, $scope.reqId, $scope.currentSessionId)
                    .then(function (response) {
                        if (response && response.length > 0) {
                            $scope.listRequirementTerms = response;
                            $scope.listRequirementTerms.forEach(function (item, index) {
                                if (item.reqTermsType == 'DELIVERY') {
                                    $scope.deliveryList.push(item);
                                    $scope.deliveryRadio = true;
                                }
                                else if (item.reqTermsType == 'PAYMENT') {

                                    if (item.reqTermsDays > 0) {
                                        item.paymentType = '+';
                                    }
                                    else if (item.reqTermsDays < 0) {
                                        item.paymentType = '-';
                                        item.reqTermsDays = -(item.reqTermsDays);
                                    }

                                    $scope.paymentlist.push(item);
                                    $scope.paymentRadio = true;
                                }
                            });
                        }
                        else {

                            $scope.customerListRequirementTerms.forEach(function (item, index) {

                                var obj = {
                                    currentTime: item.currentTime,
                                    errorMessage: item.errorMessage,
                                    isRevised: item.isRevised,
                                    refReqTermID: item.reqTermsID,
                                    reqID: item.reqID,
                                    reqTermsDays: item.reqTermsDays,
                                    reqTermsID: 0,
                                    reqTermsPercent: item.reqTermsPercent,
                                    reqTermsType: item.reqTermsType,
                                    sessionID: item.sessionID,
                                    userID: item.userID
                                };

                                $scope.listRequirementTerms.push(obj);

                            });

                            $scope.listRequirementTerms.forEach(function (item, index) {

                                if (item.reqTermsType == 'DELIVERY') {
                                    $scope.deliveryList.push(item);
                                    $scope.deliveryRadio = true;
                                }
                                else if (item.reqTermsType == 'PAYMENT') {

                                    if (item.reqTermsDays > 0) {
                                        item.paymentType = '+';
                                    }
                                    else if (item.reqTermsDays < 0) {
                                        item.paymentType = '-';
                                        item.reqTermsDays = -(item.reqTermsDays);
                                    }

                                    $scope.paymentlist.push(item);
                                    $scope.paymentRadio = true;
                                }
                            });
                        }

                    });
            };

            $scope.GetRequirementTermsCustomer = function () {
                auctionsService.GetRequirementTerms($scope.auctionItem.customerID, $scope.reqId, $scope.currentSessionId)
                    .then(function (response) {
                        if (response && response.length > 0) {

                            $scope.customerListRequirementTerms = [];
                            $scope.customerDeliveryList = [];
                            $scope.customerPaymentlist = [];

                            $scope.customerListRequirementTerms = response;

                            $scope.customerListRequirementTerms.forEach(function (item, index) {
                                if (item.reqTermsType == 'DELIVERY') {
                                    $scope.customerDeliveryList.push(item);
                                }
                                else if (item.reqTermsType == 'PAYMENT') {

                                    if (item.reqTermsDays > 0) {
                                        item.paymentType = '+';
                                    }
                                    else if (item.reqTermsDays < 0) {
                                        item.paymentType = '-';
                                        item.reqTermsDays = -(item.reqTermsDays);
                                    }
                                    else if (item.reqTermsDays == 0) {
                                        item.paymentType = '';
                                    }

                                    $scope.customerPaymentlist.push(item);
                                }
                            });
                        }


                        //$scope.GetRequirementTerms();


                    });
            };

            $scope.getUserCredentials = function () {
                $http({
                    method: 'GET',
                    url: domain + 'getusercredentials?sessionid=' + $scope.currentSessionId + "&userid=" + $scope.currentUserId,
                    encodeURI: true,
                    headers: { 'Content-Type': 'application/json' }
                }).then(function (response) {
                    $scope.CredentialsResponce = response.data;
                    $scope.CredentialsResponce.forEach(function (cred, credIndex) {
                        if (cred.fileType === "STN") {
                            $scope.gstNumber = cred.credentialID;
                        }
                    });

                    if (!$scope.gstNumber && $scope.companyGSTInfo && $scope.companyGSTInfo.length > 0) {
                        $scope.gstNumber = $scope.companyGSTInfo[0].gstNumber;
                    }

                }, function (result) {

                });
            };

            $scope.colspanDynamic = 16;
            $scope.getData = function (methodName, callerID) {
                auctionsService.getrequirementdata({ "reqid": $stateParams.Id, "sessionid": $scope.currentSessionId, 'userid': $scope.currentUserId })
                    .then(function (response) {
                        if (response) {
                            if ($scope.isValidJson(response.reqComments1)) {
                                $scope.attachmentList = JSON.parse(response.reqComments1);
                            } else {
                                $scope.reqComments1 = response.reqComments1;
                            }
                            $scope.setAuctionInitializer(response, methodName, callerID);
                            $scope.GetReqDataPriceCap();
                            //$scope.getCurrencyRateStatus();
                            $scope.isTaxAvailable();
                            auctionsService.GetCompanyConfiguration(response.custCompID, "ITEM_UNITS,GST_LOCAL_CODE", $scope.currentSessionId)
                                .then(function (configResponse) {
                                    if (configResponse && configResponse.length > 0) {
                                        $scope.companyItemUnits = configResponse.filter(function (config) {
                                            return config.configKey === 'ITEM_UNITS';
                                        });

                                        let gstLocalCodeArr = configResponse.filter(function (config) {
                                            return config.configKey === 'GST_LOCAL_CODE';
                                        });

                                        if (gstLocalCodeArr && gstLocalCodeArr.length > 0) {
                                            $scope.localGSTNumberCode = gstLocalCodeArr[0].configValue;
                                        }
                                    }
                                });
                        }
                    });
                //$scope.$broadcast('timer-start');
            };

            //Get Min reduction amount based on min quotation amount of 1%
            $scope.getMinReductionAmount = function () {
                if ($scope.auctionItem.minBidAmount > 0) {
                    $scope.NegotiationSettings.minReductionAmount = $scope.auctionItem.minBidAmount;
                    $scope.NegotiationSettings.rankComparision = $scope.auctionItem.minVendorComparision;
                }
                else {
                    var amount = $scope.NegotiationSettings.minReductionAmount;
                    if ($scope.auctionItem && $scope.auctionItem.auctionVendors && $scope.auctionItem.auctionVendors.length > 0) {
                        var tempAuctionVendors = $scope.auctionItem.auctionVendors.filter(function (vendor) {
                            return (vendor.runningPrice > 0 && vendor.initialPrice > 0 && vendor.companyName != 'PRICE_CAP' && vendor.isQuotationRejected == 0);
                        });

                        var temp = _.minBy(tempAuctionVendors, 'initialPrice');
                        if (temp && temp.initialPrice && temp.initialPrice > 0) {
                            amount = temp.initialPrice * 0.005;
                        }
                    }

                    if ($scope.formRequest.reductionLevel && $scope.formRequest.reductionLevel === 'OVERALL') {
                        $scope.NegotiationSettings.minReductionAmount = 0.5;
                    } else {
                        $scope.NegotiationSettings.minReductionAmount = amount;
                    }

                    $scope.NegotiationSettings.rankComparision = amount;
                }
            };


            $scope.CheckDisable = false;
            $scope.existingVendors = [];

            $scope.setAuctionInitializer = function (response, methodName, callerID) {
                $scope.GetReqPRItems();
                let requirementIncoTermsSettings = {
                    INCO_TERMS: ''
                };


                if ($scope.auctionItem && $scope.auctionItem.auctionVendors && $scope.auctionItem.auctionVendors.length > 0) {
                    requirementIncoTermsSettings.INCO_TERMS = $scope.auctionItem.auctionVendors[0].INCO_TERMS;
                    if ($scope.auctionItem.auctionVendors[0].isVendAckChecked) {
                        $scope.auctionItem.auctionVendors[0].isVendAckApprove = true;
                    }
                }

                $scope.auctionItem = response;
                $scope.auctionItem.paymentTerms = ($scope.auctionItem.paymentTerms || '');
                $scope.auctionItemTemporary = response;
                if ($scope.isCustomer) {
                    $scope.auctionItem.listRequirementItems = $scope.auctionItem.listRequirementItems.filter(function (reqItem) { return reqItem.isCoreProductCategory; });
                }
                $scope.auctionItem.listRequirementItems = _.orderBy($scope.auctionItem.listRequirementItems, ['isCoreProductCategory'], ['desc']);

                if (!$scope.isCustomer && $scope.companyINCOTerms.length <= 0) {
                    auctionsService.GetCompanyConfiguration($scope.currentUserCompID, "INCO", $scope.currentSessionId)
                        .then(function (unitResponse) {
                            $scope.companyINCOTerms = unitResponse;
                        });
                }

                if ($scope.auctionItem && $scope.auctionItem.auctionVendors && $scope.auctionItem.auctionVendors.length > 0) {
                    $scope.GetCompanyGSTInfo();
                    $scope.auctionItem.auctionVendors[0].INCO_TERMS = requirementIncoTermsSettings.INCO_TERMS ? requirementIncoTermsSettings.INCO_TERMS : $scope.auctionItem.auctionVendors[0].INCO_TERMS;
                }

                if ($scope.auctionItemTemporary && $scope.auctionItemTemporary && $scope.auctionItemTemporary.auctionVendors) {
                    $scope.auctionItemTemporary.emailLinkVendors = $scope.auctionItemTemporary.auctionVendors.filter(function (item) {
                        item.listWorkflows = [];
                        if (item.isEmailSent == undefined || item.isEmailSent == '') {
                            item.isEmailSent = false;
                        } else {
                            item.isEmailSent = true;
                        }

                        if (item.companyName != 'PRICE_CAP') {
                            $scope.existingVendors.push(item.vendorID);
                        }

                        return item.companyName != 'PRICE_CAP';
                    });

                    //$scope.getWorkflows();

                }


                $scope.CB_END_TIME = $scope.auctionItem.CB_END_TIME;
                //#CB-0-2018-12-05
                if (!$scope.isCustomer && $scope.auctionItem && $scope.auctionItem.auctionVendors &&
                    $scope.auctionItem.auctionVendors.length > 0 && $scope.auctionItem.auctionVendors[0].revVendorTotalPriceCB > 0 &&
                    //$scope.auctionItem.auctionVendors[0].FREEZE_CB == false &&
                    $scope.auctionItem.auctionVendors[0].isQuotationRejected == 0 &&
                    $scope.auctionItem.IS_CB_ENABLED == 1 &&
                    $scope.auctionItem.IS_CB_COMPLETED == false) {
                    $scope.goToCbVendor($scope.auctionItem.auctionVendors[0].vendorID);
                }

                if ($scope.auctionItem && $scope.auctionItem.auctionVendors && $scope.auctionItem.auctionVendors.length > 0
                    && $scope.auctionItem.auctionVendors[0].isQuotationRejected == 0) {
                    $scope.colspanDynamic = 21;
                } else {
                    $scope.colspanDynamic = 16;
                }
                //ack code start
                if ($scope.auctionItem && $scope.auctionItem.auctionVendors && $scope.auctionItem.auctionVendors.length > 0) {
                    if ($scope.auctionItem.auctionVendors[0].quotationUrl == '' || $scope.auctionItem.auctionVendors[0].quotationUrl == undefined) {
                        $scope.auctionItem.auctionVendors[0].isVendAckChecked = false;
                    } else {
                        $scope.auctionItem.auctionVendors[0].isVendAckChecked = true;
                        $scope.CheckDisable = true;
                    }
                }

                $scope.IS_CB_ENABLED = $scope.auctionItem.IS_CB_ENABLED;
                $scope.IS_CB_NO_REGRET = $scope.auctionItem.IS_CB_NO_REGRET;
                $scope.auctionItem.isUOMDifferent = false;
                if (callerID == $scope.currentUserId || callerID == undefined) {
                    $scope.auctionItemVendor = $scope.auctionItem;
                    if ($scope.auctionItemVendor.listRequirementItems != null) {
                        $scope.auctionItemVendor.listRequirementItems.forEach(function (item, index) {
                            item.isEdit = true;
                            if (item.cGst > 0 || item.sGst > 0) {
                                $scope.IGSTFields = true;
                                $scope.CSGSTFields = false;
                            } else if (item.iGst > 0) {
                                $scope.IGSTFields = false;
                                $scope.CSGSTFields = true;
                            }
                            item.oldRevUnitPrice = item.revUnitPrice;
                            item.TemperoryRevUnitPrice = 0;
                            item.TemperoryRevUnitPrice = item.revUnitPrice;

                            item.TemperoryUnitDiscount = 0;
                            item.TemperoryRevUnitDiscount = 0;
                            item.TemperoryUnitDiscount = item.unitDiscount;
                            item.TemperoryRevUnitDiscount = item.revUnitDiscount;

                            item.vendorUnits = (item.vendorUnits == '' || item.vendorUnits == undefined) ? item.productQuantityIn : item.vendorUnits;
                            if (item.productQuantityIn.toUpperCase() != item.vendorUnits.toUpperCase()) {
                                //$scope.auctionItem.isUOMDifferent = true;
                            }
                        });
                    }
                }

                $scope.isCurrency = false;
                if (!$scope.isCustomer) {
                    $scope.fieldValidation();
                }

                $scope.notviewedcompanynames = '';
                $scope.auctionItem.listRequirementItems.forEach(function (item, itemIndex) {
                    if (item.productQuotationTemplateJson && item.productQuotationTemplateJson !== '' && item.productQuotationTemplateJson) {
                        item.productQuotationTemplateArray = JSON.parse(item.productQuotationTemplateJson);
                        if (item.productQuotationTemplateArray) {
                            item.productQuotationTemplateArray = item.productQuotationTemplateArray.filter(function (prodObj) {
                                if (prodObj) {
                                    return prodObj;
                                }
                            });
                        }
                        $scope.handleSubItemsVisibility(item);
                    } else {
                        item.productQuotationTemplateArray = [];
                    }
                });

                $scope.auctionItemVendor.listRequirementItems.forEach(function (item, index) {
                    if ($scope.auctionItem.auctionVendors && $scope.auctionItem.auctionVendors.length > 0) {
                        $scope.auctionItem.auctionVendors[0].listRequirementItems.forEach(function (i, indx) {
                            if (item.itemID && i.itemID && item.itemID == i.itemID) {
                                //console.log('ItemId:' + item.itemID + '--Rank:' + i.itemRank);
                                item.itemRank = i.itemRank;
                            }
                        });
                    }
                });

                if ($scope.auctionItem && $scope.auctionItem.auctionVendors) {
                    $scope.auctionItem.auctionVendors.forEach(function (item, index) {
                        if (!item.bestPrice) {
                            item.bestPrice = 0;
                        }

                        item.quotationRejectedComment = item.quotationRejectedComment.replace("Approved-", "");
                        item.quotationRejectedComment = item.quotationRejectedComment.replace("Reject Comments-", "");
                        if (item.quotationRejectedComment) {
                            item.quotationRejectedComment = item.quotationRejectedComment.trim();
                        }

                        item.revQuotationRejectedComment = item.revQuotationRejectedComment.replace("Approved-", "");
                        item.revQuotationRejectedComment = item.revQuotationRejectedComment.replace("Reject Comments-", "");
                        if (item.revQuotationRejectedComment) {
                            item.revQuotationRejectedComment = item.revQuotationRejectedComment.trim();
                        }


                        if ($scope.auctionItem.LAST_BID_ID == item.vendorID &&
                            ($scope.auctionItem.status == 'STARTED' || $scope.auctionItem.status == 'NOTSTARTED' || $scope.auctionItem.status == 'UNCONFIRMED')) {
                            item.vendorColorStyle = { 'background-color': 'lightgreen' };
                        }
                        else if (item.FREEZE_CB == true) {
                            item.vendorColorStyle = { 'background-color': 'rgb(255, 228, 225)' };
                        }
                        else if (item.VEND_FREEZE_CB == true) {
                            item.vendorColorStyle = { 'background-color': 'rgb(176, 224, 230)' };
                            //rgb(176, 224, 230)
                            //#B0E0E6
                        }
                        else {
                            item.vendorColorStyle = {};
                        }

                        item.isQuotationRejectedDB = item.isQuotationRejected;
                        item.isRevQuotationRejectedDB = item.isRevQuotationRejected;

                        item.bestPrice = 0;
                        item.hasRegretItems = false;
                        item.listRequirementItems.forEach(function (item2, index) {
                            item2.itemQuotationRejectedComment = item2.itemQuotationRejectedComment.replace("Approved-", "").replace("Reject Comments-", "");
                            //Core products only
                            if (item2.isCoreProductCategory) {
                                item.bestPrice += (item2.revUnitPrice * item2.productQuantity);
                            }

                            if (item2.isRegret) {
                                item.hasRegretItems = true;
                            }

                            if (item2.productQuantityIn.toUpperCase() != item2.vendorUnits.toUpperCase()) {
                                //item.isUOMDifferent = true;
                            }

                            if (item2.productQuotationTemplateJson && item2.productQuotationTemplateJson !== '' && item2.productQuotationTemplateJson) {
                                item2.productQuotationTemplateArray = JSON.parse(item2.productQuotationTemplateJson);
                            }
                            else {
                                item2.productQuotationTemplateArray = [];
                            }

                        });

                        if (!$scope.multipleAttachmentsList) {
                            $scope.multipleAttachmentsList = [];
                        }
                        if (!item.multipleAttachmentsList) {
                            item.multipleAttachmentsList = [];
                        }

                        if (item.multipleAttachments != '' && item.multipleAttachments != null && item.multipleAttachments != undefined) {
                            var multipleAttachmentsList = item.multipleAttachments.split(',');
                            item.multipleAttachmentsList = [];
                            $scope.multipleAttachmentsList = [];
                            multipleAttachmentsList.forEach(function (att, index) {
                                var fileUpload = {
                                    fileStream: [],
                                    fileName: '',
                                    fileID: att
                                };

                                item.multipleAttachmentsList.push(fileUpload);
                                $scope.multipleAttachmentsList.push(fileUpload);
                            });
                        }

                        if (item.quotationUrl != '' && item.isQuotationRejected == -1) {
                            $scope.Loding = false;
                            $scope.notviewedcompanynames += item.companyName + ', ';
                            $scope.starreturn = true;
                        }

                        //if (!$scope.isCustomer && (item.quotationUrl == '' || item.quotationUrl == null || item.quotationUrl == undefined)) {
                        //    $scope.getUserCredentials();
                        //}
                    });
                }

                $scope.selectedcurr = '';
                if ($scope.auctionItem && $scope.auctionItem.auctionVendors && $scope.auctionItem.auctionVendors.length > 0) {
                    //$scope.auctionItem.auctionVendors[0].selectedVendorCurrency = $scope.auctionItem.currency; //Force Requirement Currency
                    $scope.selectedcurr = $scope.auctionItem.auctionVendors[0].selectedVendorCurrency;
                }

                if (!$scope.auctionItem.multipleAttachments) {
                    $scope.auctionItem.multipleAttachments = [];
                }

                if ($scope.auctionItem.multipleAttachments == null) {
                    $scope.auctionItem.attFile = response.attachmentName;
                    if ($scope.auctionItem.attFile != '' && $scope.auctionItem.attFile != null && $scope.auctionItem.attFile != undefined) {
                        var attchArray = $scope.auctionItem.attFile.split(',');
                        attchArray.forEach(function (att, index) {
                            var fileUpload = {
                                fileStream: [],
                                fileName: '',
                                fileID: att
                            };

                            $scope.auctionItem.multipleAttachments.push(fileUpload);
                        });
                    }
                }

                $scope.notviewedcompanynames = $scope.notviewedcompanynames.substring(0, $scope.notviewedcompanynames.length - 2);

                if ($scope.auctionItem && $scope.auctionItem.listRequirementTaxes && $scope.auctionItem.listRequirementTaxes.length == 0) {
                    $scope.auctionItem.listRequirementTaxes = $scope.listRequirementTaxes;
                }
                else {
                    $scope.listRequirementTaxes = $scope.auctionItem.listRequirementTaxes || [];
                }

                $scope.listRequirementTaxeslength = $scope.listRequirementTaxes.length;
                $scope.reqTaxSNo = $scope.auctionItem.taxSNoCount;
                $scope.NegotiationSettings = ($scope.auctionItem.NegotiationSettings || {});
                $scope.disableLocalGSTFeature = $scope.NegotiationSettings.disableLocalGSTFeature ? true : false;
                if ($scope.NegotiationSettings && $scope.NegotiationSettings.negotiationDuration) {
                    var duration = $scope.NegotiationSettings.negotiationDuration.split(" ", 4);
                    $scope.days = parseInt(duration[0]);
                    duration = duration[1];
                    duration = duration.split(":", 4);
                    $scope.hours = parseInt(duration[0]);
                    $scope.mins = parseInt(duration[1]);
                    $scope.NegotiationSettings.negotiationDuration = $scope.days + ' ' + $scope.hours + ':' + $scope.mins + ':00.0';
                }

                if (!response.auctionVendors || response.auctionVendors.length == 0) {
                    $scope.auctionItem.auctionVendors = [];
                    $scope.auctionItem.description = "";
                }

                // // #INTERNATIONALIZATION-0-2019-02-12
                $scope.auctionItem.quotationFreezTime = userService.toLocalDate($scope.auctionItem.quotationFreezTime);
                $scope.auctionItem.expStartTime = userService.toLocalDate($scope.auctionItem.expStartTime);




                if (($scope.auctionItem.auctionVendors && $scope.auctionItem.auctionVendors.length != 0) && (callerID == $scope.currentUserId || callerID == undefined)) {
                    $scope.totalprice = $scope.auctionItem.auctionVendors[0].initialPriceWithOutTaxFreight;
                    $scope.vendorTaxes = $scope.auctionItem.auctionVendors[0].taxes;
                    $scope.vendorBidPrice = $scope.auctionItem.auctionVendors[0].totalInitialPrice;

                    $scope.discountAmount = $scope.auctionItem.auctionVendors[0].discount;
                    $scope.vendorBidPriceWithoutDiscount = $scope.vendorBidPrice + $scope.discountAmount;
                    $scope.vendorID = $scope.auctionItem.auctionVendors[0].vendorID;
                    $scope.warranty = $scope.auctionItem.auctionVendors[0].warranty;
                    $scope.duration = $scope.auctionItem.auctionVendors[0].duration;
                    $scope.payment = $scope.auctionItem.auctionVendors[0].payment;
                    $scope.gstNumber = $scope.auctionItem.auctionVendors[0].gstNumber;
                    $scope.validity = $scope.auctionItem.auctionVendors[0].validity;
                    $scope.otherProperties = $scope.auctionItem.auctionVendors[0].otherProperties ? $scope.auctionItem.auctionVendors[0].otherProperties : $scope.auctionItem.auctionVendors[0].otherProperties;

                    $scope.revvendorTaxes = $scope.auctionItem.auctionVendors[0].taxes;
                    $scope.isQuotationRejected = $scope.auctionItem.auctionVendors[0].isQuotationRejected;
                    $scope.quotationRejectedComment = $scope.auctionItem.auctionVendors[0].quotationRejectedComment;
                    $scope.revQuotationRejectedComment = $scope.auctionItem.auctionVendors[0].revQuotationRejectedComment;

                    $scope.selectedVendorCurrency = $scope.auctionItem.auctionVendors[0].selectedVendorCurrency;

                    //if ($scope.auctionItem.urgency == 'Critical (Will be Closed Today)' && $scope.validity == '') {
                    //    $scope.validity = '1 Day';
                    //}
                    //else if ($scope.auctionItem.urgency == 'High (Will be Closed in 2 Days)' && $scope.validity == '') {
                    //    $scope.validity = '2 Days';
                    //}
                    //else if ($scope.auctionItem.urgency == 'Medium (Will be Closed in 3 Days)' && $scope.validity == '') {
                    //    $scope.validity = '3 Days';
                    //}
                    //else if ($scope.auctionItem.urgency == 'Low (Will Take 7 Days Time)' && $scope.validity == '') {
                    //    $scope.validity = '7 Days';
                    //}
                    //if ($scope.warranty == '') {
                    //    $scope.warranty = 'As Per OEM';
                    //}
                    if ($scope.duration == '') {
                        //$scope.duration = new moment($scope.auctionItem.deliveryTime).format("DD-MM-YYYY");
                        $scope.duration = $scope.auctionItem.deliveryTime;
                    }
                    if ($scope.payment == '') {
                        $scope.payment = $scope.auctionItem.paymentTerms;
                    }

                    $scope.auctionItemVendor.listRequirementItems.forEach(function (item, index) {

                        if (item.productQuantityIn.toUpperCase() != item.vendorUnits.toUpperCase()) {
                            //$scope.auctionItem.isUOMDifferent = true;
                        }

                        item.Gst = item.cGst + item.sGst + item.iGst;

                        if ($scope.auctionItem.isDiscountQuotation == 1) {
                            if ($scope.auctionItem.status == 'STARTED') {
                                $scope.overAllValue.overallDiscountPercentage = item.revUnitDiscount;
                                $scope.revDiscountPrice = item.revUnitDiscount;
                            } else {
                                $scope.overAllValue.overallDiscountPercentage = item.unitDiscount;
                                $scope.revDiscountPrice = item.revUnitDiscount;
                            }

                        }

                        if ($scope.auctionItem.isDiscountQuotation == 2) {
                            if ($scope.auctionItem.isTabular && ($scope.auctionItem.status == 'UNCONFIRMED' || $scope.auctionItem.status == 'NOTSTARTED')) {
                                item.revCostPrice = item.costPrice;
                                item.revmarginAmount = item.marginAmount;
                            }

                            item = $scope.handlePrecision(item, 2);
                            item.Gst = item.cGst + item.sGst;
                            item = $scope.handlePrecision(item, 2);
                            item.costPrice = (item.unitMRP * 100 * 100) / ((item.unitDiscount * 100) + 10000 + (item.unitDiscount * item.Gst) + (item.Gst * 100));
                            item.revCostPrice = (item.unitMRP * 100 * 100) / ((item.revUnitDiscount * 100) + 10000 + (item.revUnitDiscount * item.Gst) + (item.Gst * 100));
                            item.netPrice = item.costPrice * (1 + item.Gst / 100);
                            item.revnetPrice = item.revCostPrice * (1 + item.Gst / 100);
                            item.marginAmount = item.unitMRP - item.netPrice;
                            item.revmarginAmount = item.unitMRP - item.revnetPrice;
                            item = $scope.handlePrecision(item, 2);
                        }
                        if (!item.selectedVendorID || item.selectedVendorID == 0) {
                            $scope.allItemsSelected = false;
                        }
                        if ($scope.auctionItem.status == "Negotiation Ended" || $scope.auctionItem.status == "Vendor Selected" || $scope.auctionItem.status == "PO Processing") {
                            if (item.selectedVendorID > 0) {
                                var vendorArray = $filter('filter')($scope.auctionItem.auctionVendors, { vendorID: item.selectedVendorID });
                                if (vendorArray.length > 0) {
                                    item.selectedVendor = vendorArray[0];
                                }
                            } else {
                                item.selectedVendor = $scope.auctionItem.auctionVendors[0];
                            }
                        }

                        if (item.productQuotationTemplateArray && item.productQuotationTemplateArray.length > 0) {
                            item.productQuotationTemplateArray.forEach(function (subItem, subItemIndex) {
                                if (subItem.IS_VALID === 1 && (subItem.HAS_PRICE === 1 || subItem.HAS_SPECIFICATION === 1 || subItem.HAS_QUANTITY || subItem.HAS_TAX || subItem.DESCRIPTION !== null)) {
                                    item.expanded = true;
                                }
                            });
                        }

                    });

                    if (!$scope.auctionItem.isStopped && $scope.auctionItem.timeLeft > 0) {
                        $scope.disableAddButton = false;
                    }

                    if (callerID == $scope.currentUserId || callerID == undefined) {
                        $scope.revtotalprice = $scope.auctionItem.auctionVendors[0].revPrice;

                        $scope.revtotalprice = $scope.precisionRound(parseFloat($scope.revtotalprice), $rootScope.companyRoundingDecimalSetting);

                        $scope.revvendorBidPrice = $scope.auctionItem.auctionVendors[0].revVendorTotalPrice;
                    }

                    $scope.starttimecondition1 = $scope.auctionItem.auctionVendors[0].isQuotationRejected;


                    $scope.revQuotationUrl1 = $scope.auctionItem.auctionVendors[0].revquotationUrl;

                    if ($scope.auctionItem.auctionVendors[0].companyName == 'PRICE_CAP') {
                        $scope.revQuotationUrl1 = $scope.auctionItem.auctionVendors[1].revquotationUrl;
                    }

                    $scope.L1QuotationUrl = $scope.auctionItem.auctionVendors[0].quotationUrl;


                    //= $filter('filter')($scope.auctionItem.auctionVendors, {revquotationUrl > ''});
                    //$scope.RevQuotationfirstvendor 
                    // $scope.isRejectedPOEnable = true;
                    $scope.auctionItem.auctionVendors.forEach(function (item, index) {
                        if (item.revquotationUrl != '') {
                            $scope.RevQuotationfirstvendor = item.revquotationUrl;
                        }
                        //if (item.isRevQuotationRejected == 1) {
                        //    $scope.isRejectedPOEnable = false;
                        //} else {
                        //    $scope.isRejectedPOEnable = true;
                        //}

                    });
                }

                if (!$scope.isCustomer) {
                    if ($scope.auctionItem.biddingType != 'SPOT') {
                        $scope.auctionItem.auctionVendors[0].showIncoTerm = $scope.auctionItem.auctionVendors[0].isQuotationRejected != 0;
                    } else {
                        $scope.auctionItem.auctionVendors[0].showIncoTerm = $scope.auctionItem.auctionVendors[0].initialPrice > 0 ? ($scope.auctionItem.auctionVendors[0].isQuotationRejected == 0 ? false : true) : true;
                    }

                    $scope.auctionItem.auctionVendors[0].displayTax = $scope.evaluateBiddingTypeBasedOnStatus();
                    if (!$scope.auctionItem.auctionVendors[0].selectedVendorCurrency) {
                        $scope.auctionItem.auctionVendors[0].selectedVendorCurrency = 'INR';
                        $scope.currencyChangeEvent();
                    }
                    if (!$scope.auctionItem.auctionVendors[0].INCO_TERMS) {
                        $scope.auctionItem.auctionVendors[0].INCO_TERMS = 'NA';
                        $scope.fieldValidation();
                        $scope.clearNonCoreItemsValues()
                    }
                }

                if ($scope.auctionItem.auctionVendors && $scope.auctionItem.auctionVendors.length >= 2) {
                    $scope.starttimecondition2 = $scope.auctionItem.auctionVendors[1].isQuotationRejected;
                }
                //$scope.vendorID = $scope.auctionItem.auctionVendors[0].vendorID;

                $scope.participatedVendors = $scope.auctionItem.auctionVendors.filter(function (vendor) {
                    return (vendor.runningPrice > 0 && vendor.companyName != 'PRICE_CAP');
                });

                $scope.revisedParticipatedVendors = $scope.participatedVendors.filter(function (vendor) {
                    return (vendor.revquotationUrl != null && vendor.revquotationUrl != '' && vendor.companyName != 'PRICE_CAP');
                });

                $scope.revisedApprovedVendors = $scope.participatedVendors.filter(function (vendor) {
                    return (vendor.revquotationUrl != null && vendor.revquotationUrl != '' && vendor.companyName != 'PRICE_CAP' && vendor.isRevQuotationRejected == 0);
                });


                if ($scope.auctionItem.status != 'DELETED' && $scope.auctionItem.status != 'UNCONFIRMED' && $scope.auctionItem.status != 'STARTED' && $scope.auctionItem.status != 'NOTSTARTED' && $scope.auctionItem.status != 'Negotiation Ended') {
                    auctionsService.getpodetails({ "reqid": $stateParams.Id, "sessionid": $scope.currentSessionId, 'userid': $scope.currentUserId })
                        .then(function (response) {
                            $scope.poDetails = response;
                            //$scope.updatedeliverydateparams.date = $scope.poDetails.expectedDelivery;
                            if (response != undefined) {
                                //var date = $scope.poDetails.expectedDelivery.split('+')[0].split('(')[1];
                                var date1 = moment($scope.poDetails.expectedDelivery).format('DD/MM/YYYY  HH:mm');
                                $scope.updatedeliverydateparams.date = date1;

                                //var date1 = $scope.poDetails.paymentScheduleDate.split('+')[0].split('(')[1];
                                //var newDate1 = new Date(parseInt(parseInt(date1)));
                                var date2 = moment($scope.poDetails.expPaymentDate).format('DD/MM/YYYY  HH:mm');
                                $scope.updatepaymentdateparams.date = date2;
                                if ($scope.updatepaymentdateparams.date == '31/12/9999') {
                                    $scope.updatepaymentdateparams.date = 'Payment Date';
                                }
                            }
                        });
                }

                //  $scope.auctionItem.description = $scope.auctionItem.description.replace("\u000a", "\n")
                $scope.description = $scope.auctionItem.description.replace(/\u000a/g, "</br>");
                $scope.deliveryLocation = $scope.auctionItem.deliveryLocation.replace(/\u000a/g, "</br>");
                $scope.paymentTerms = $scope.auctionItem.paymentTerms.replace(/\u000a/g, "</br>");
                $scope.deliveryTime = $scope.auctionItem.deliveryTime.replace(/\u000a/g, "</br>");


                //var id = $scope.currentUserId;
                var result = $scope.auctionItem.auctionVendors.filter(function (obj) {
                    return obj.vendorID == $scope.currentUserId;
                });

                if ($scope.currentUserId != $scope.auctionItem.customerID && $scope.currentUserId != $scope.auctionItem.superUserID && !$scope.auctionItem.customerReqAccess && result.length == 0) {
                    swal("Access denied", "You do not have access to this requirement because you are not part of this requirements process.", 'error');
                    $state.go('home');
                } else {
                    $scope.setFields();
                    //auctionsService.getcomments({ "reqid": $stateParams.Id, "userid": $scope.currentUserId, "sessionid": $scope.currentSessionId })
                    //    .then(function (response) {
                    //        $scope.Comments = response;
                    //    });

                }


                $scope.quotationApprovedColor = {};
                $scope.quotationNotApprovedColor = {};

                if ($scope.auctionItem && $scope.auctionItem.auctionVendors[0] && $scope.auctionItem.auctionVendors[0].isQuotationRejected != 0 && $scope.auctionItem.isUnitPriceBidding == 1) {
                    $scope.quotationApprovedColor = {
                        'background-color': '#f5b2b2'
                    };
                    $scope.quotationNotApprovedColor = {};
                } else if ($scope.auctionItem.isUnitPriceBidding == 1) {
                    $scope.quotationNotApprovedColor = {
                        'background-color': '#f5b2b2'
                    };
                    $scope.quotationApprovedColor = {};
                }

                $scope.getMinReductionAmount();
                //$scope.overAllValue.overallPercentage = $scope.NegotiationSettings.minReductionAmount;
                //$scope.changeItemPercentage($scope.overAllValue.overallPercentage);
                if ($scope.isCustomer) {
                    // $scope.getpricecomparison(); // disable as of now, as UI is commented out.
                }

                if ($scope.isCustomer) {
                    $scope.listRequirementItemsTemp = $scope.auctionItem.listRequirementItems;
                } else {
                    $scope.listRequirementItemsTemp = $scope.auctionItemVendor.listRequirementItems;
                }

                $scope.totalItems = ($scope.listRequirementItemsTemp) ? $scope.listRequirementItemsTemp.length : 0;
            };



            if ($scope.isCustomer) {
                auctionsService.GetIsAuthorized($scope.currentUserId, $scope.reqId, $scope.currentSessionId)
                    .then(function (response) {
                        if (response.errorMessage != "") {
                            growlService.growl(response.errorMessage, "inverse");
                            $state.go('home');
                            return false;
                        }

                        $scope.getData();
                    });
            } else {
                $scope.getData();
            }

            //$scope.GetBidHistory();

            $scope.AmountSaved = 0;

            $scope.selectItemVendor = function (itemID, vendorID) {
                var params = {
                    reqID: $scope.auctionItem.requirementID,
                    userID: $scope.currentUserId,
                    itemID: itemID,
                    vendorID: vendorID,
                    sessionID: $scope.currentSessionId
                };
                auctionsService.itemwiseselectvendor(params)
                    .then(function (response) {
                        if (response.errorMessage == '') {
                            var index = -1;
                            for (var i = 0, len = $scope.auctionItemVendor.listRequirementItems.length; i < len; i++) {
                                if ($scope.auctionItemVendor.listRequirementItems[i].itemID === itemID) {
                                    index = i;
                                    $scope.auctionItemVendor.listRequirementItems[i].selectedVendorID = vendorID;
                                    break;
                                }
                                if ($scope.auctionItemVendor.listRequirementItems[i].selectedVendor == 0) {
                                    $scope.allItemsSelected = false;
                                } else {
                                    $scope.allItemsSelected = true;
                                }
                            }
                            swal("Success!", "This item has been assigned to Vendor", "success");
                            $scope.getData();
                        } else {
                            swal("Error", 'An Unexpected error has occurred. Please contact support@prm360.com for resolving this.', 'error');
                        }

                    })
            }

            $scope.handlePrecision = function (item, precision) {

                //item.sGst = $scope.precisionRound(parseFloat(item.sGst), precision);
                //item.cGst = $scope.precisionRound(parseFloat(item.cGst), precision);
                //item.Gst = $scope.precisionRound(parseFloat(item.Gst), precision);
                //item.unitMRP = $scope.precisionRound(parseFloat(item.unitMRP), precision);
                item.unitDiscount = $scope.precisionRound(parseFloat(item.unitDiscount), precision + 6);
                item.revUnitDiscount = $scope.precisionRound(parseFloat(item.revUnitDiscount), precision + 6);
                //item.costPrice = $scope.precisionRound(parseFloat(item.costPrice), precision);
                //item.revCostPrice = $scope.precisionRound(parseFloat(item.revCostPrice), precision);
                //item.netPrice = $scope.precisionRound(parseFloat(item.netPrice), precision);
                //item.revnetPrice = $scope.precisionRound(parseFloat(item.revnetPrice), precision);
                //item.marginAmount = $scope.precisionRound(parseFloat(item.marginAmount), precision);
                //item.revmarginAmount = $scope.precisionRound(parseFloat(item.revmarginAmount), precision);
                //$scope.totalprice = $scope.precisionRound(parseFloat($scope.totalprice), precision);
                //$scope.revtotalprice = $scope.precisionRound(parseFloat($scope.revtotalprice), precision);
                //return item;
                return item;
            };

            $scope.$watch('auctionItemVendor.listRequirementItems', function (oldVal, newVal) {
                $log.debug("items list has changed");
                if (!newVal) { return; }
                newVal.forEach(function (item, index) {
                    if (!item.selectedVendorID || item.selectedVendorID == 0) {
                        $scope.allItemsSelected = false;
                    }
                })
            })

            $scope.generatePDFonHTML = function () {
                auctionsService.getdate()
                    .then(function (response) {
                        var date = new Date(parseInt(response.substr(6)));
                        var obj = $scope.auctionItem;
                        $scope.POTemplate = "<div id='POTemplate' style='display:none;'><html><head><title>PRM360</title><style>.date{margin-left: 850px;}.to{margin-left: 250px;}.name{margin-left: 300px;}.sub{margin-left: 450px;}img{position: absolute; left: 750px; top:75px; z-index: -1;}</style></head><body><header><br><br><br><img src='acads360.jpg' width='50' height='50'><h1 align='center'>PRM360<img </h1></header><br><div class='date'><p><b>Date:</b> " + date + "</p><p><b>PO No:</b> " + obj.requirementID + "</p></div><div class='to'><p>To,</p><p><b>" + obj.CompanyName + ",</b></p><p><b>" + obj.deliveryLocation + ".</b></p></div><p class='name'><b>Hello </b> " + obj.auctionVendors[0].vendorName + "</p><p class='sub'><b>Sub:</b> " + obj.title + "</p><p align='center'><b>Bill of Material</b></p><table border='1' cellpadding='2' style='width:60%' align='center'><tr><th>Product Name</th><th>Description</th><th>Price</th></tr><tr><td>" + obj.title + "</td><td>" + obj.description + "</td><td>" + obj.price + "</td></tr></table><p class='to'><b>Terms & Conditions</b></p><div class='name'> <p>1. Payment : " + obj.paymentTerms + ".</p><p>2. Delivery : " + obj.deliveryLocation + ".</p><p>3. Tax : " + obj.taxes + ".</p></div><p class='to'><b>Billing and Shipping Address:</b></p><p class='to'>Savvy Associates, # 3-4-174/21/2, Radha Krishna Nagar, Attapur, Hyderguda, Hyderabad - 500048</p><p align=center>This is a system generated PO, henceforth sign and seal is not required.</p><br><footer class='to'>M/s. Savvy Associates, H.No: 3-4-174/21/2, Radha Krishna Nagar, Attapur, Hyderguda, Hyderabad – 48Contact,M: 91-9949245791.,<br>E: savvyassociates@gmail.com.<br><b>URL:</b> www.savvyassociates.com. </footer></body></html></div>";
                        var content = document.getElementById('content');
                        content.insertAdjacentHTML('beforeend', $scope.POTemplate);
                    });
            };

            $scope.generatePOasPDF = function (divName) {
                $scope.generatePDFonHTML();
                var printContents = document.getElementById(divName).innerHTML;
                var originalContents = document.body.innerHTML;

                if (navigator.userAgent.toLowerCase().indexOf('chrome') > -1) {
                    var popupWin = window.open('', '_blank', 'width=600,height=600,scrollbars=no,menubar=no,toolbar=no,location=no,status=no,titlebar=no');
                    popupWin.window.focus();
                    popupWin.document.write('<!DOCTYPE html><html><head>' +
                        '<link rel="stylesheet" type="text/css" href="style.css" />' +
                        '</head><body onload="window.print()"><div class="reward-body">' + printContents + '</div></html>');
                    popupWin.onbeforeunload = function (event) {
                        popupWin.close();
                        return '.\n';
                    };
                    popupWin.onabort = function (event) {
                        popupWin.document.close();
                        popupWin.close();
                    }
                } else {
                    var popupWin = window.open('', '_blank', 'width=800,height=600');
                    popupWin.document.open();
                    popupWin.document.write('<html><head><link rel="stylesheet" type="text/css" href="style.css" /></head><body onload="window.print()">' + printContents + '</html>');
                    popupWin.document.close();
                }
                popupWin.document.close();
                var params = {
                    reqid: $scope.auctionItem.requirementID,
                    userid: $scope.currentUserId,
                    status: 'PO Processing',
                    type: "WINVENDOR",
                    sessionID: $scope.currentSessionId
                };
                auctionsService.updateStatus(params)
                    .then(function (response) {
                        if (response.errorMessage == '') {
                            $scope.getData();
                            //doc.save("DOC.PDF");
                        } else {
                            swal("Error", 'An Unexpected error has occurred. Please contact support@prm360.com for resolving this.', 'error');
                        }

                    })

                return true;
            }

            $scope.generatePO = function () {
                var doc = new jsPDF();
                doc.setFontSize(40);
                //doc.text(40, 30, "Octocat loves jsPDF", 4);
                /*doc.fromHTML($("#POTemplate")[0], 15, 15, {
                    "width": 170,
                    function() {
                        $scope.POFile = $.makeArray(new Uint8Array(doc.output('arraybuffer')));
                    }
                })*/

                var params = {
                    reqid: $scope.auctionItem.requirementID,
                    userid: $scope.currentUserId,
                    status: 'PO Processing',
                    type: "WINVENDOR",
                    sessionID: $scope.currentSessionId
                };
                auctionsService.updateStatus(params)
                    .then(function (response) {
                        if (response.errorMessage == '') {
                            $scope.getData();
                            doc.save("DOC.PDF");
                        } else {
                            swal("Error", 'An Unexpected error has occurred. Please contact support@prm360.com for resolving this.', 'error');
                        }

                    })
            }

            $scope.generatePOinServer = function () {
                if ($scope.POTemplate == "") {
                    $scope.generatePDFonHTML();

                }
                var doc = new jsPDF('p', 'in', 'letter');
                var specialElementHandlers = {};
                var doc = new jsPDF();
                //doc.setFontSize(40);
                doc.fromHTML($scope.POTemplate, 0.5, 0.5, {
                    'width': 7.5, // max width of content on PDF
                });
                //doc.save("DOC.PDF");
                doc.output("dataurl");
                $scope.POFile = $.makeArray(new Uint8Array(doc.output('arraybuffer')));
                var params = {
                    POfile: $scope.POFile,
                    reqid: $scope.auctionItem.requirementID,
                    userid: $scope.currentUserId,
                    POfileName: 'PO_req_' + $scope.auctionItem.requirementID + '.pdf',
                    sessionID: $scope.currentSessionId
                }

                auctionsService.generatePOinServer(params)
                    .then(function (response) {
                        if (response.errorMessage == '') {
                            $scope.showStatusDropDown = true;
                            $scope.getData();
                        } else {
                            swal("Error", 'An Unexpected error has occurred. Please contact support@prm360.com for resolving this.', 'error');
                        }

                    });
            };

            $scope.updateStatus = function (status) {
                var params = {
                    reqid: $scope.auctionItem.requirementID,
                    userid: $scope.currentUserId,
                    status: status,
                    type: "ALLVENDORS",
                    sessionID: $scope.currentSessionId
                };
                auctionsService.updateStatus(params)
                    .then(function (response) {
                        if (response.errorMessage == '') {
                            $scope.getData();
                        } else {
                            swal("Error", 'An Unexpected error has occurred. Please contact support@prm360.com for resolving this.', 'error');
                        }

                    });
            };

            $scope.uploadquotationsfromexcel = function (status) {
                var params = {
                    reqID: $scope.auctionItem.requirementID,
                    userID: $scope.currentUserId,
                    sessionID: $scope.currentSessionId,
                    quotationAttachment: $scope.quotationAttachment
                };
                auctionsService.uploadquotationsfromexcel(params)
                    .then(function (response) {
                        if (response.errorMessage == '') {
                            location.reload();
                        } else {
                            swal("Error", response.errorMessage, 'error');
                        }
                    });
            };

            /*$scope.applyTaxtoAll = false;*/
            $scope.uploadclientsidequotation = function (type) {
                var params = {
                    reqID: $scope.auctionItem.requirementID,
                    userID: $scope.currentUserId,
                    sessionID: $scope.currentSessionId,
                    quotationAttachment: $scope.quotationAttachment
                };
                auctionsService.uploadclientsidequotation(params)
                    .then(function (response) {

                        if (response && response.length > 0 && !response[0].errorMessage) {
                            var requirementItem = [];

                            requirementItem = response.filter(function (I) {
                                if (I.unitPrice < 0 || I.unitDiscount < 0 || I.unitMRP < 0 || I.revUnitDiscount < 0) {
                                    return I;
                                }
                            });

                            requirementItem;
                            //if (requirementItem.length > 0) {
                            //    swal("Error", "Prices/Discounts should be greater than 0", "error");
                            //    return;
                            //}
                        }


                        if (response && response.length > 0 && !response[0].errorMessage) {
                            var validation = true;
                            var itemID = 0;
                            for (item in $scope.auctionItem.listRequirementItems) {
                                item = (+item);
                                var newItem = $filter('filter')(response, { itemID: $scope.auctionItem.listRequirementItems[item].itemID })[0];
                                if (newItem) {
                                    if ($scope.auctionItem.status == "STARTED" && $scope.auctionItem.listRequirementItems[item].revUnitPrice < newItem.revUnitPrice && $scope.auctionItem.isDiscountQuotation == 0) {
                                        validation = false;
                                        itemID = newItem.itemID;
                                    } else if ($scope.auctionItem.status == "STARTED" && $scope.auctionItem.listRequirementItems[item].unitDiscount > newItem.revUnitDiscount && $scope.auctionItem.isDiscountQuotation == 1) {
                                        validation = false;
                                        itemID = newItem.itemID;
                                    }
                                }
                            }
                            var isSCGST = false;
                            var isIGST = false;
                            if (validation) {
                                for (item in $scope.auctionItem.listRequirementItems) {
                                    item = (+item);
                                    newItem = $filter('filter')(response, { itemID: $scope.auctionItem.listRequirementItems[item].itemID })[0];
                                    if (newItem) {

                                        $scope.auctionItem.listRequirementItems[item].unitPrice = newItem.unitPrice;
                                        $scope.auctionItem.listRequirementItems[item].revUnitPrice = newItem.revUnitPrice;
                                        $scope.auctionItem.listRequirementItems[item].unitMRP = newItem.unitMRP;
                                        $scope.auctionItem.listRequirementItems[item].unitDiscount = newItem.unitDiscount;
                                        $scope.auctionItem.listRequirementItems[item].revUnitDiscount = newItem.revUnitDiscount;
                                        $scope.auctionItem.listRequirementItems[item].cGst = newItem.cGst;
                                        $scope.auctionItem.listRequirementItems[item].sGst = newItem.sGst;
                                        $scope.auctionItem.listRequirementItems[item].iGst = newItem.iGst;
                                        $scope.auctionItem.listRequirementItems[item].Gst = newItem.iGst + newItem.sGst + newItem.cGst;
                                        $scope.auctionItem.listRequirementItems[item].itemLevelInitialComments = newItem.itemLevelInitialComments;
                                        $scope.auctionItem.listRequirementItems[item].isRegret = (newItem.unitPrice > 0 && !newItem.isRegret) ? false : true;
                                        $scope.auctionItem.listRequirementItems[item].regretComments = (newItem.unitPrice > 0 && !newItem.isRegret) ? newItem.regretComments : 'Regretted';
                                        /*$scope.auctionItem.listRequirementItems[item].disableInput = true;*/

                                        if ($scope.auctionItem.auctionVendors[0].selectedVendorCurrency != 'INR') {
                                            $scope.auctionItem.listRequirementItems[item].cGst = 0;
                                            $scope.auctionItem.listRequirementItems[item].sGst = 0;
                                            $scope.auctionItem.listRequirementItems[item].iGst = 0;
                                        }

                                        if ($scope.gstNumber && $scope.gstNumber.startsWith($scope.localGSTNumberCode)) {
                                            $scope.auctionItem.listRequirementItems[item].cGst = newItem.cGst;
                                            $scope.auctionItem.listRequirementItems[item].sGst = newItem.sGst;
                                            $scope.auctionItem.listRequirementItems[item].iGst = 0;
                                        }
                                        if (newItem.unitPrice <= 0 || newItem.isRegret) {
                                            $scope.regretHandle($scope.auctionItem.listRequirementItems[item]);
                                        }
                                        if (newItem.unitPrice > 0 || !newItem.isRegret) {

                                            $scope.auctionItem.listRequirementItems[item].productQuotationTemplateArray = newItem.productQuotationTemplateJson ? JSON.parse(newItem.productQuotationTemplateJson) : newItem.productQuotationTemplateJson;

                                        }

                                        if (newItem.productQuotationTemplateJson && !newItem.isRegret) {
                                            $scope.auctionItem.listRequirementItems[item].productQuotationTemplateArray = JSON.parse(newItem.productQuotationTemplateJson);

                                            if ($scope.auctionItem.listRequirementItems[item].productQuotationTemplateArray) {

                                                $scope.GetItemUnitPrice(false, -1);
                                                $scope.mrpDiscountCalculation();
                                                $scope.unitPriceCalculation('PRC');
                                                $scope.getTotalPrice($scope.discountAmount, $scope.freightCharges, $scope.totalprice,
                                                    $scope.packingCharges, $scope.packingChargesTaxPercentage, $scope.packingChargesWithTax,
                                                    $scope.installationCharges, $scope.installationChargesTaxPercentage, $scope.installationChargesWithTax,
                                                    $scope.freightChargesTaxPercentage, $scope.freightChargesWithTax);
                                                $scope.getRevTotalPrice($scope.revfreightCharges, $scope.revtotalprice,
                                                    $scope.revpackingCharges, $scope.packingChargesTaxPercentage, $scope.revpackingChargesWithTax,
                                                    $scope.revinstallationCharges, $scope.installationChargesTaxPercentage, $scope.revinstallationChargesWithTax,
                                                    $scope.freightChargesTaxPercentage, $scope.revfreightChargesWithTax);
                                            }
                                        }
                                    }
                                }
                            } else {
                                swal("Error", 'New bid for item cannot be lower than the old bid. Please check the values in the excel sheet for item: ' + itemID, 'error');
                            }

                            $scope.auctionItemVendor.listRequirementItems = $scope.auctionItem.listRequirementItems;
                            $scope.listRequirementItemsTemp = $scope.auctionItemVendor.listRequirementItems;
                            $scope.totalItems = $scope.listRequirementItemsTemp.length;
                            $scope.unitPriceCalculation('IGST');
                            $scope.getTotalPrice($scope.discountAmount, 0, $scope.totalprice);
                            $scope.getRevTotalPrice(0, $scope.revtotalprice);
                            swal("Verify", "Please verify and Submit your prices!", "success");
                            $(".fileinput").fileinput("clear");
                            $scope.fieldValidation();
                            $scope.applyTaxtoAll = true;
                            $scope.gstSelect();
                            return;
                        } else if (response && response.length > 0) {
                            swal("Error!", response[0].errorMessage, "error");
                        }

                        $("#clientsideupload").val(null);
                        $("#quotationBulkUpload").val(null);                        
                    });
            };

            $scope.getrevisedquotations = function () {
                var params = {
                    reqID: $scope.auctionItem.requirementID,
                    userID: $scope.currentUserId,
                    sessionID: $scope.currentSessionId
                };
                auctionsService.getrevisedquotations(params)
                    .then(function (response) {
                        if (response.errorMessage == '') {
                            location.reload();
                        } else {
                            swal("Error", response.errorMessage, 'error');
                            console.log(response.errorMessage);
                        }
                    });
            };

            $scope.updatedeliverydateparams = {
                date: ''
            };

            $scope.updatedeliverdate = function () {
                var ts = moment($scope.updatedeliverydateparams.date, "DD-MM-YYYY HH:mm").valueOf();
                var m = moment(ts);
                var deliveryDate = new Date(m);
                var milliseconds = parseInt(deliveryDate.getTime() / 1000.0);
                $scope.updatedeliverydateparams.date = "/Date(" + milliseconds + "000+0530)/";

                var params = {
                    reqid: $scope.auctionItem.requirementID,
                    userid: $scope.currentUserId,
                    date: $scope.updatedeliverydateparams.date,
                    type: "DELIVERY",
                    sessionID: $scope.currentSessionId
                };
                auctionsService.updatedeliverdate(params)
                    .then(function (response) {
                        if (response.errorMessage == '') {
                            $scope.getData();
                        } else {
                            swal("Error", 'An Unexpected error has occurred. Please contact support@prm360.com for resolving this.', 'error');
                        }
                    });
            };

            $scope.updatepaymentdateparams = {
                date: ''
            };

            $scope.updatepaymentdate = function () {
                var ts = moment($scope.updatepaymentdateparams.date, "DD-MM-YYYY HH:mm").valueOf();
                var m = moment(ts);
                var deliveryDate = new Date(m);
                var milliseconds = parseInt(deliveryDate.getTime() / 1000.0);
                $scope.updatepaymentdateparams.date = "/Date(" + milliseconds + "000+0530)/";

                var params = {
                    reqid: $scope.auctionItem.requirementID,
                    userid: $scope.currentUserId,
                    date: $scope.updatepaymentdateparams.date,
                    type: "PAYMENT",
                    sessionID: $scope.currentSessionId
                };
                auctionsService.updatepaymentdate(params)
                    .then(function (response) {
                        if (response.errorMessage == '') {
                            $scope.getData();
                        } else {
                            swal("Error", 'An Unexpected error has occurred. Please contact support@prm360.com for resolving this.', 'error');
                        }

                    });
            };

            $scope.RestartNegotiation = function () {
                var params = {};
                params.reqID = id;
                params.sessionID = $scope.currentSessionId;
                params.userID = $scope.currentUserId;
                var parties = id + "$" + $scope.currentUserId + "$" + $scope.currentSessionId;
                $scope.isnegotiationended = false;
                $scope.NegotiationEnded = false;
                // $scope.invokeSignalR('RestartNegotiation', parties);


                auctionsService.checkForRestartNegotiation(params)
                    .then(function (response) {
                        if (response.errorMessage == '') {
                            $scope.invokeSignalR('RestartNegotiation', parties);
                        } else {
                            swal("Error", response.errorMessage, 'error');
                        }
                    });
            };

            $scope.saveComment = function () {
                var commentText = "";
                if ($scope.newComment && $scope.newComment != "") {
                    commentText = $socpe.newComment;
                } else if ($("#comment")[0].value != '') {
                    commentText = $("#comment")[0].value;
                } else {
                    $scope.commentsvalidation = true;
                }
                if (!$scope.commentsvalidation) {
                    var params = {};
                    auctionsService.getdate()
                        .then(function (response) {
                            var date = new Date(parseInt(response.substr(6)));
                            var myEpoch = date.getTime();
                            params.requirementID = id;
                            params.firstName = "";
                            params.lastName = "";
                            params.replyCommentID = -1;
                            params.commentID = -1;
                            params.errorMessage = "";
                            params.createdTime = "/Date(" + myEpoch + "+0000)/";
                            params.sessionID = $scope.currentSessionId;
                            params.userID = $scope.currentUserId;
                            params.commentText = commentText;
                            $scope.invokeSignalR('SaveComment', params);
                            //requirementHub.invoke('SaveComment', params, function (response) {
                            //	//auctionsService.savecomment(params).then(function (response) {
                            //	$scope.getData();
                            //	$scope.newComment = "";
                            //	$scope.commentsvalidation = false;
                            //});
                        });
                }
            };

            $scope.multipleAttachments = [];
            //$scope.multipleAttachmentsList = [];

            if (!$scope.multipleAttachmentsList) {
                $scope.multipleAttachmentsList = [];
            }

            $scope.getFile = function () {
                $scope.progress = 0;
                //var quotation = $("#quotation")[0].files[0];
                //if (quotation != undefined && quotation != '') {
                //    $scope.file = $("#quotation")[0].files[0];
                //    $log.info($("#quotation")[0].files[0]);
                //}

                //fileReader.readAsDataUrl($scope.file, $scope)
                //    .then(function (result) {
                //        var bytearray = new Uint8Array(result);
                //        if (quotation != undefined && quotation != '') {
                //            $scope.bidAttachement = $.makeArray(bytearray);
                //            $scope.bidAttachementName = $scope.file.name;
                //            $scope.enableMakeBids = true;
                //        }
                //    });

                $scope.multipleAttachments = $("#attachement")[0].files;
                $scope.multipleAttachments = Object.values($scope.multipleAttachments)

                $scope.multipleAttachments.forEach(function (item, index) {
                    fileReader.readAsDataUrl(item, $scope)
                        .then(function (result) {

                            var fileUpload = {
                                fileStream: [],
                                fileName: '',
                                fileID: 0
                            };
                            var bytearray = new Uint8Array(result);
                            fileUpload.fileStream = $.makeArray(bytearray);
                            fileUpload.fileName = item.name;
                            if (!$scope.multipleAttachmentsList) {
                                $scope.multipleAttachmentsList = [];
                            }
                            $scope.multipleAttachmentsList.push(fileUpload);
                        });
                });

            };

            $scope.updateTime = function (time) {
                var isDone = false;
                $scope.disableDecreaseButtons = true;
                $scope.disableAddButton = true;
                $scope.$on('timer-tick', function (event, args) {
                    var temp = event.targetScope.countdown;

                    if (!isDone) {
                        if (time < 0 && temp + time < 0) {
                            growlService.growl("You cannot reduce the time when it is already below 60 seconds", "inverse");
                            isDone = true;
                            return false;
                        }

                        isDone = true;
                        var params = {};
                        params.reqID = id;
                        params.sessionID = $scope.currentSessionId;
                        params.userID = $scope.currentUserId;
                        params.newTicks = ($scope.countdownVal + time);
                        params.lotID = $scope.auctionItem.lotId;
                        var parties = id + "$" + $scope.currentUserId + "$" + params.newTicks + "$" + $scope.currentSessionId + "$" + params.lotID;
                        $scope.invokeSignalR('UpdateTime', parties, function () { console.log('time:' + time); });
                    }
                });
            };

            $scope.Loding = false;

            $scope.priceCapError = false;

            $scope.validateCurrencyRate = function () {
                let pendingQuotationsVendors = $scope.vendorsQuotationStatus();
                if (pendingQuotationsVendors && pendingQuotationsVendors.length > 0) {
                    swal("Warning!", "Please approve or reject vendors Quotation. Pending vendors: " + pendingQuotationsVendors, "error");
                } else {
                    var startValue = $("#startTimeDate").val(); //$scope.startTime; //Need fix on this.
                    if (startValue && startValue != null && startValue != "") {
                        auctionsService.validateCurrencyRate({ reqid: $scope.reqId, sessionid: $scope.currentSessionId })
                            .then(function (currencyValidation) {
                                //$scope.updateAuctionStart(currencyValidation.currentTime);
                                if (currencyValidation.objectID) {
                                    swal({
                                        title: "Warning Currency Factor...",
                                        text: "Currency factor has been changed since Requirement creation, please click Ok to continue or click Cancel to re-calculate.",
                                        type: "warning",
                                        showCancelButton: true,
                                        confirmButtonColor: "#F44336",
                                        confirmButtonText: "OK",
                                        closeOnConfirm: false
                                    }, function (isConfirm) {
                                        if (!isConfirm) {
                                            $scope.showCurrencyRefresh = true;
                                        } else {
                                            $scope.updateAuctionStart(currencyValidation.currentTime);
                                        }
                                    });
                                } else {
                                    $scope.updateAuctionStart(currencyValidation.currentTime);
                                }
                            });
                    } else {
                        alert("Please enter the date and time to update Start Time to.");
                    }
                }
            };


            $scope.saveReqNegSettings = function () {
                $scope.NegotiationSettingsValidationMessage = '';
                $scope.NegotiationTimeValidation($scope.days, $scope.hours, $scope.mins, $scope.NegotiationSettings.minReductionAmount, $scope.NegotiationSettings.rankComparision);
                if ($scope.NegotiationSettingsValidationMessage) {
                    $scope.Loding = false;
                    return;
                }

                if ($scope.formRequest.reductionLevel && $scope.formRequest.reductionLevel === 'OVERALL' &&
                    $scope.NegotiationSettings.minReductionAmount && +$scope.NegotiationSettings.minReductionAmount > 25) {
                    $scope.Loding = false;
                    swal("Warning!", "Overall reduction % cannot be more than 25%.", "error");
                    return;
                }

                var params = {};
                params.reqID = $scope.auctionItem.requirementID;
                params.sessionID = $scope.currentSessionId;
                $scope.NegotiationSettings.negotiationDuration = $scope.days + ':' + $scope.hours + ':' + $scope.mins + ':00.0';
                params.NegotiationSettings = $scope.NegotiationSettings;

                auctionsService.SaveReqNegSettings(params)
                    .then(function (response) {
                        if (response.SaveReqNegSettingsResult) {
                            if (response.SaveReqNegSettingsResult.errorMessage == '') {
                                swal("Done!", "Saved Successfully.", "success");
                            } else {
                                swal("Error", response.SaveReqNegSettingsResult.errorMessage, "inverse");
                            }
                        } else {
                            swal("Error", response.SaveReqNegSettingsResult.errorMessage, "inverse");
                        }

                    });

            };

            $scope.updateAuctionStart = function (timeToValidate) {
                $scope.priceCapError = false;
                $scope.showCurrencyRefresh = false;
                $scope.Loding = true;
                $scope.NegotiationSettingsValidationMessage = '';
                $scope.NegotiationTimeValidation($scope.days, $scope.hours, $scope.mins, $scope.NegotiationSettings.minReductionAmount, $scope.NegotiationSettings.rankComparision);
                if ($scope.NegotiationSettingsValidationMessage) {
                    $scope.Loding = false;
                    return;
                }

                if ($scope.formRequest.reductionLevel && $scope.formRequest.reductionLevel === 'OVERALL' &&
                    $scope.NegotiationSettings.minReductionAmount && +$scope.NegotiationSettings.minReductionAmount > 25) {
                    $scope.Loding = false;
                    swal("Warning!", "Overall reduction % cannot be more than 25%.", "error");
                    return;
                }

                var params = {};
                params.auctionID = id;
                var startValue = $("#startTimeDate").val(); //$scope.startTime; //Need fix on this.
                if (startValue && startValue != null && startValue != "") {
                    //var ts = moment($scope.startTime, "DD-MM-YYYY HH:mm").valueOf();
                    var ts = moment(startValue, "DD-MM-YYYY HH:mm").valueOf();
                    var m = moment(ts);
                    var auctionStartDate = new Date(m);

                    var CurrentDateToLocal = userService.toLocalDate(timeToValidate);
                    var ts = moment(CurrentDateToLocal, "DD-MM-YYYY HH:mm").valueOf();
                    var m = moment(ts);
                    var deliveryDate = new Date(m);
                    var milliseconds = parseInt(deliveryDate.getTime() / 1000.0);
                    var CurrentDateToTicks = "/Date(" + milliseconds + "000+0530)/";
                    var CurrentDate = moment(new Date(parseInt(CurrentDateToTicks.substr(6))));
                    $log.debug(CurrentDate < auctionStartDate);
                    $log.debug('div' + auctionStartDate);
                    if (CurrentDate >= auctionStartDate) {
                        $scope.Loding = false;
                        swal("Done!", "Your Negotiation Start Time should be greater than current time.", "error");
                        return;
                    }

                    milliseconds = parseInt(auctionStartDate.getTime() / 1000.0);
                    // // #INTERNATIONALIZATION-0-2019-02-12
                    params.postedOn = "/Date(" + userService.toUTCTicks(startValue) + "+0530)/";
                    params.auctionEnds = "/Date(" + userService.toUTCTicks(startValue) + "+0530)/";
                    params.customerID = $scope.currentUserId;
                    params.sessionID = $scope.currentSessionId;

                    $scope.NegotiationSettings.negotiationDuration = $scope.days + ' ' + $scope.hours + ':' + $scope.mins + ':00.0';
                    params.NegotiationSettings = $scope.NegotiationSettings;

                    if ($scope.starreturn) {
                        $scope.Loding = false;
                        swal("Not Allowed", "You are not allowed to schedule the Negotiation time until all the Vendor Quotations are validated. (They should be either approved/rejected). The following vendors quotations are yet to be validated. (" + $scope.notviewedcompanynames + ")", "error");
                        return;
                    }

                    if ($scope.auctionItem.auctionVendors.length == 0 || !$scope.auctionItem.auctionVendors[0] || !$scope.auctionItem.auctionVendors[1] || $scope.auctionItem.auctionVendors[0].quotationUrl == "" || $scope.auctionItem.auctionVendors[1].quotationUrl == "" || $scope.auctionItem.auctionVendors[0].isQuotationRejected != 0 || $scope.auctionItem.auctionVendors[1].isQuotationRejected != 0 || $scope.starttimecondition1 != 0 || $scope.starttimecondition2 != 0) {
                        $scope.Loding = false;
                        swal("Not Allowed", "You are not allowed to create a start time until at least 2 vendors Approved.", "error");
                        return;
                    }
                    if ($scope.auctionItem.auctionVendors[0].companyName == "") {
                        $scope.Loding = false;
                        swal("Not Allowed", "You are not allowed to create a start time until at least 2 vendors Approved.", "error");
                        return;
                    }
                    if ($scope.priceCapError == true) {
                        $scope.Loding = false;
                        swal("Not Allowed", "Price CAP Value should be less then all the vendors price.", "error");
                        return;
                    }
                    else {
                        $scope.Loding = true;
                        requirementHub.invoke('UpdateAuctionStartSignalR', params, function (req) {
                            $scope.Loding = false;
                            swal("Done!", "Your Negotiation Start Time Updated Successfully!", "success");
                            $scope.Loding = false;
                            var start = $scope.auctionItem.startTime.split('+')[0].split('(')[1];
                            auctionsService.getdate()
                                .then(function (response) {
                                    //var curDate = new Date(parseInt(response.substr(6)));

                                    var CurrentDateToLocal = userService.toLocalDate(response);

                                    var ts = moment(CurrentDateToLocal, "DD-MM-YYYY HH:mm").valueOf();
                                    var m = moment(ts);
                                    var deliveryDate = new Date(m);
                                    var milliseconds = parseInt(deliveryDate.getTime() / 1000.0);
                                    var CurrentDateToTicks = "/Date(" + milliseconds + "000+0530)/";

                                    //var curDate = moment(new Date(parseInt(CurrentDateToTicks.substr(6))));
                                    var curDate = new Date(parseInt(CurrentDateToTicks.substr(6)));
                                    var myEpoch = curDate.getTime();
                                    if (($scope.auctionItem.customerID == $scope.currentUserId || $scope.auctionItem.superUserID == $scope.currentUserId || $scope.auctionItem.customerReqAccess) && +start > myEpoch) {
                                        $scope.startBtns = true;
                                        $scope.customerBtns = false;
                                    } else {

                                        $scope.startBtns = false;
                                        $scope.disableButtons();
                                        $scope.customerBtns = true;
                                    }

                                    if ($scope.auctionItem.timeLeft == null || $scope.auctionItem.timeLeft < 0) {
                                        $scope.showTimer = false;
                                    } else {

                                        $scope.showTimer = true;
                                    }
                                });
                        });
                    }


                    //auctionsService.getdate()
                    //    .then(function (response1) {


                    //    });

                    $scope.Loding = false;
                } else {
                    $scope.Loding = false;
                    alert("Please enter the date and time to update Start Time to.");
                }

                $scope.Loding = false;
            };



            $scope.makeaBidLoding = false;
            $scope.makeaBid = function (call, isReviced) {
                $scope.makeaBidLoding = true;

                var bidPrice = 0;

                if (isReviced == 0) {
                    var bidPrice = $("#quotationamount").val();
                }
                if (isReviced == 1) {

                    var bidPrice = $("#revquotationamount").val();
                }

                var params = {};
                params.reqID = parseInt(id);
                params.sessionID = $scope.currentSessionId;
                params.userID = $scope.currentUserId;

                params.price = $scope.precisionRound(parseFloat(bidPrice), $rootScope.companyRoundingDecimalSetting);
                params.quotation = $scope.bidAttachement;
                params.quotationName = $scope.bidAttachementName;
                params.discountAmount = $scope.discountAmount;

                params.tax = $scope.vendorTaxes;

                params.warranty = $scope.warranty;
                params.duration = $scope.duration;
                params.payment = $scope.payment;
                params.gstNumber = $scope.gstNumber;
                params.validity = $scope.validity;

                params.otherProperties = $scope.otherProperties;

                params.quotationType = 'USER';
                params.type = 'quotation';

                params.listRequirementTaxes = $scope.listRequirementTaxes;

                params.quotationObject = $scope.auctionItemVendor.listRequirementItems;

                if (isReviced == 0) {
                    params.revised = isReviced;
                    params.priceWithoutTax = $scope.totalprice;
                    params.price = $scope.vendorBidPrice;
                    params.discountAmount = $scope.discountAmount;
                }
                if (isReviced == 1) {
                    params.revised = isReviced;
                    params.priceWithoutTax = $scope.revtotalprice;
                    params.price = $scope.revvendorBidPrice;
                    params.discountAmount = $scope.discountAmount;
                }

                if (($scope.auctionItem.status == 'UNCONFIRMED' || $scope.auctionItem.status == 'NOTSTARTED') && $scope.auctionItem.isQuotationPriceLimit && $scope.vendorBidPrice > $scope.auctionItem.quotationPriceLimit) {
                    swal("Error!", 'Your Amount Should be less than Quotation Price limit :' + $scope.auctionItem.quotationPriceLimit, "error");
                    return;
                }

                auctionsService.getdate()
                    .then(function (responseFromServer) {
                        var dateFromServer = new Date(parseInt(responseFromServer.substr(6)));
                        var auctionStart = new Date(parseInt($scope.auctionItem.startTime.substr(6)));
                        var timeofauctionstart = auctionStart.getTime();
                        var currentServerTime = dateFromServer.getTime();




                        if ($scope.auctionItem.status == "Negotiation Ended") {



                            // $scope.DeleteRequirementTerms();


                            if ($scope.auctionItem.status == 'Negotiation Ended') {
                            } else {
                                // $scope.SaveRequirementTerms($scope.reqId);
                            }

                            auctionsService.makeabid(params).then(function (req) {
                                if (req.errorMessage == '') {

                                    swal({
                                        title: "Thanks!",
                                        text: "Your Quotation has been Uploaded Successfully",
                                        type: "success",
                                        showCancelButton: false,
                                        confirmButtonColor: "#DD6B55",
                                        confirmButtonText: "Ok",
                                        closeOnConfirm: true
                                    },
                                        function () {
                                            location.reload();
                                        });

                                    $("#quotationamount").val("");
                                    $scope.quotationStatus = true;
                                    $scope.auctionStarted = false;
                                    $scope.getData();
                                    $scope.makeaBidLoding = false;
                                } else {
                                    $scope.makeaBidLoding = false;
                                    swal({
                                        title: "Error",
                                        text: req.errorMessage,
                                        type: "error",
                                        showCancelButton: false,
                                        confirmButtonColor: "#DD6B55",
                                        confirmButtonText: "Ok",
                                        closeOnConfirm: true
                                    });
                                }
                            });

                        } else {
                            if (timeofauctionstart - currentServerTime >= 3600000 && $scope.auctionItem.auctionVendors[0].isQuotationRejected != 0) {
                                // $scope.DeleteRequirementTerms();


                                if ($scope.auctionItem.status == 'Negotiation Ended') {
                                } else {
                                    // $scope.SaveRequirementTerms($scope.reqId);
                                }

                                auctionsService.makeabid(params).then(function (req) {
                                    if (req.errorMessage == '') {

                                        swal({
                                            title: "Thanks!",
                                            text: "Your Quotation has been Uploaded Successfully",
                                            type: "success",
                                            showCancelButton: false,
                                            confirmButtonColor: "#DD6B55",
                                            confirmButtonText: "Ok",
                                            closeOnConfirm: true
                                        },
                                            function () {
                                                location.reload();
                                            });

                                        $("#quotationamount").val("");
                                        $scope.quotationStatus = true;
                                        $scope.auctionStarted = false;
                                        $scope.getData();
                                        $scope.makeaBidLoding = false;
                                    } else {
                                        $scope.makeaBidLoding = false;
                                        swal({
                                            title: "Error",
                                            text: req.errorMessage,
                                            type: "error",
                                            showCancelButton: false,
                                            confirmButtonColor: "#DD6B55",
                                            confirmButtonText: "Ok",
                                            closeOnConfirm: true
                                        });
                                    }
                                });
                            }

                            else {

                                var message = "You have missed the deadline for Quotation Submission";
                                if ($scope.auctionItem.auctionVendors[0].isQuotationRejected == 0 && $scope.auctionItem.status != "Negotiation Ended") { message = "Your Quotation Already Approved You cant Update Quotation Please Contact PRM360" }

                                $scope.makeaBidLoding = false;
                                swal({
                                    title: "Error",
                                    text: message,
                                    type: "error",
                                    showCancelButton: false,
                                    confirmButtonColor: "#DD6B55",
                                    confirmButtonText: "Ok",
                                    closeOnConfirm: true
                                });
                                $scope.getData();
                            }



                        }
                    });
            };

            $scope.revquotationupload = function () {
                $scope.makeaBidLoding = true;
                if (($scope.bidAttachementName == "" || $scope.bidAttachement.length == 0) && $scope.quotationStatus == false) {
                    $scope.bidAttachementValidation = true;
                    $scope.makeaBidLoding = false;
                    return false;
                } else {
                    $scope.bidAttachementValidation = false;
                }
                var params = {};
                params.reqID = parseInt(id);
                params.sessionID = $scope.currentSessionId;
                params.userID = $scope.currentUserId;
                params.price = 0;
                params.quotation = $scope.bidAttachement;
                params.quotationName = $scope.bidAttachementName;
                params.tax = 0;
                auctionsService.revquotationupload(params).then(function (req) {
                    if (req.errorMessage == '') {
                        swal({
                            title: "Thanks!",
                            text: "Your Revised Quotation Uploaded Successfully",
                            type: "success",
                            showCancelButton: false,
                            confirmButtonColor: "#DD6B55",
                            confirmButtonText: "Ok",
                            closeOnConfirm: true
                        },
                            function () {
                                location.reload();
                            });
                        $("#quotationamount1").val("");
                        $scope.quotationStatus = true;
                        $scope.auctionStarted = false;
                        $scope.getData();
                        $scope.makeaBidLoding = false;
                    } else {
                        $scope.makeaBidLoding = false;
                        swal("Error!", req.errorMessage, "error");
                    }
                });
            };

            $scope.$on('timer-tick', function (event, args) {
                if (event.targetScope.seconds == 5 ||
                    event.targetScope.seconds == 15 ||
                    event.targetScope.seconds == 25 ||
                    event.targetScope.seconds == 35 ||
                    event.targetScope.seconds == 45 ||
                    event.targetScope.seconds == 55) {
                    //auctionsService.CheckSystemDateTime();
                }
                if ($scope.auctionItem.IS_CB_ENABLED == false || $scope.auctionItem.IS_CB_ENABLED == 0) {
                    $scope.countdownVal = event.targetScope.countdown;
                    if (event.targetScope.countdown < 61 && !$scope.disableStopBids && !$scope.disablereduceTime) {
                        $timeout($scope.disableButtons(), 1000);
                    }
                    if (event.targetScope.countdown > 60) {
                        $timeout($scope.enableButtons(), 1000);
                    }
                    if (event.targetScope.countdown == 60 || event.targetScope.countdown == 59 || event.targetScope.countdown == 30 || event.targetScope.countdown == 29) {
                        //var msie = $(document) [0].documentMode;
                        var ua = window.navigator.userAgent;
                        var msieIndex = ua.indexOf("MSIE ");
                        var msieIndex2 = ua.indexOf("Trident");
                        // if is IE (documentMode contains IE version)
                        var params = {};
                        params.reqID = id;
                        params.sessionID = $scope.currentSessionId;
                        params.userID = $scope.currentUserId;
                        //var parties = id + "$" + $scope.currentUserId + "$" + $scope.currentSessionId;
                        //requirementHub.invoke('CheckRequirement', parties, function () {
                        //    $scope.getData();
                        //    //$scope.$broadcast('timer-set-countdown-seconds', 0);
                        //    //swal("Negotiation Completed!", "Congratulations! you procurement process is now completed. " + $scope.toprankerName + " is the least bider with the value " + $scope.auctionItem.minPrice + " \n Your savings through PRM :" + ($scope.vendorInitialPrice - $scope.auctionItem.minPrice), "success");
                        //});
                        if (msieIndex > 0 || msieIndex2 > 0) {
                            $scope.getData();
                        }
                    }
                    if (event.targetScope.countdown <= 0) {
                        if ($scope.auctionStarted && ($scope.auctionItem.status == "CLOSED" || $scope.auctionItem.status == "STARTED")) {
                            $scope.auctionItem.minPrice = $scope.auctionItem.auctionVendors[0].runningPrice;
                            //if (!$scope.NegotiationEnded) {
                            //    auctionsService.isnegotiationended(id, $scope.currentSessionId)
                            //        .then(function (response) {
                            //            if (response.errorMessage == '') {
                            //                if (response.objectID == 1) {
                            //                    var params = {};
                            //                    params.reqID = id;
                            //                    params.sessionID = $scope.currentSessionId;
                            //                    params.userID = $scope.currentUserId;
                            //                    var parties = id + "$" + $scope.currentUserId + "$" + $scope.currentSessionId;
                            //                    $scope.NegotiationEnded = true;
                            //                    $scope.invokeSignalR('EndNegotiation', parties);

                            //                }
                            //            }
                            //        })
                            //}
                        } else if ($scope.auctionItem.status == "NOTSTARTED") {
                            var params = {};
                            params.reqID = id;
                            params.sessionID = $scope.currentSessionId;
                            params.userID = $scope.currentUserId;

                            auctionsService.StartNegotiation(params)
                                .then(function (response) {
                                    if (response.errorMessage == '') {
                                        $scope.getData();
                                    }
                                })

                            var parties = id + "$" + $scope.currentUserId + "$" + $scope.currentSessionId;
                            $scope.invokeSignalR('CheckRequirement', parties);
                            //requirementHub.invoke('CheckRequirement', parties, function () {
                            //    $scope.getData();
                            //    //$scope.$broadcast('timer-set-countdown-seconds', 0);
                            //    //swal("Negotiation Completed!", "Congratulations! you procurement process is now completed. " + $scope.toprankerName + " is the least bider with the value " + $scope.auctionItem.minPrice + " \n Your savings through PRM :" + ($scope.vendorInitialPrice - $scope.auctionItem.minPrice), "success");
                            //});
                        }

                        //$scope.getData();
                    }
                    if (event.targetScope.countdown <= 120) {
                        $scope.timerStyle = {
                            'color': '#f00',
                            '-webkit - animation': 'flash linear 1s infinite',
                            'animation': 'flash linear 1s infinite'
                        };
                    }
                    //$log.info($('#reqTitle').visible());
                    //console.log($('#reqTitle').visible());
                    $scope.timerFloat = $('#reqTitle').show();
                    if ($scope.auctionItem.status == 'STARTED' && !$scope.timerFloat) {
                        $scope.divfix = {
                            'bottom': '2%',
                            'left': '3%',
                            'position': 'fixed',
                            'z-index': '3000'
                        };
                        $scope.divfix3 = {
                            'bottom': '9%',
                            'left': '3%',
                            'position': 'fixed',
                            'z-index': '3000'
                        };
                        $scope.divfix1 = {
                            'bottom': '8%',
                            'left': '3%',
                            'position': 'fixed',
                            'z-index': '3000'
                        };
                        $scope.divfix2 = {
                            'bottom': '13%',
                            'left': '3%',
                            'position': 'fixed',
                            'z-index': '3000'
                        };
                        $scope.divfixMakeabid = {
                            'bottom': '14%',
                            'left': '10%',
                            'position': 'fixed',
                            'z-index': '3000'
                        };
                        $scope.divfixMakeabidError = {
                            'bottom': '13%',
                            'left': '18%',
                            'position': 'fixed',
                            'z-index': '3000',
                            'background-color': 'lightgrey'
                        };

                        if (!$scope.isCustomer) {
                            $scope.boxfix = {
                                'background-color': 'lightgrey',
                                'width': '280px',
                                'height': '120px',
                                //'border': '25px solid green',
                                'padding': '25px',
                                'margin': '25px',


                                'bottom': '-2%',
                                'left': '0%',
                                'position': 'fixed',
                                'z-index': '3000'
                            }
                        }
                        else {
                            $scope.boxfix = {
                                'background-color': 'lightgrey',
                                'width': '280px',
                                'height': '90px',
                                //'border': '25px solid green',
                                'padding': '25px',
                                'margin': '25px',


                                'bottom': '-2%',
                                'left': '0%',
                                'position': 'fixed',
                                'z-index': '3000'
                            }
                        }

                    }
                    else {
                        $scope.divfix = {};
                        $scope.divfix3 = {};
                        $scope.divfix1 = {};
                        $scope.divfix2 = {};
                        $scope.divfixMakeabid = {};
                        $scope.divfixMakeabidError = {};
                        $scope.boxfix = {};
                    };
                    if ($scope.auctionItem.status == 'STARTED' || $scope.auctionItem.status == 'NOTSTARTED') {

                        if ($scope.auctionItem.isUnitPriceBidding == 1 && ($scope.auctionItem.status == 'STARTED' || $scope.auctionItem.status == 'NOTSTARTED') && $scope.auctionItem.isTabular == true) {
                            $scope.divBorder = {
                                'background-color': '#f5b2b2'
                            };
                        }

                        if ($scope.auctionItem.isUnitPriceBidding == 0 && ($scope.auctionItem.status == 'STARTED' || $scope.auctionItem.status == 'NOTSTARTED') && $scope.auctionItem.isTabular == true) {
                            $scope.divBorder1 = {
                                'background-color': '#f5b2b2'
                            };
                        }
                    }
                    if (event.targetScope.countdown > 120 && $scope.auctionItem.status == 'NOTSTARTED') {
                        $scope.timerStyle = { 'color': '#000' };
                    }
                    if (event.targetScope.countdown > 120 && $scope.auctionItem.status != 'NOTSTARTED') {
                        $scope.timerStyle = { 'color': '#228B22' };
                    }
                    if (event.targetScope.countdown <= 60 && $scope.auctionItem.status == 'STARTED') {
                        $scope.disableDecreaseButtons = true;
                    }
                    if (event.targetScope.countdown > 60 && $scope.auctionItem.status == 'STARTED') {
                        $scope.disableDecreaseButtons = false;
                    }
                    if (event.targetScope.countdown <= 0 && $scope.auctionItem.status == 'NOTSTARTED') {
                        $scope.showTimer = false;

                        //$scope.getData();
                        window.location.reload();
                    }
                    if (event.targetScope.countdown < 1 && ($scope.auctionItem.status == 'STARTED' || $scope.auctionItem.status == 'Negotiation Ended')) {
                        //var parties = id + "$" + $scope.currentUserId + "$" + $scope.currentSessionId;
                        //$scope.invokeSignalR('CheckRequirement', parties);  
                        $scope.showTimer = false;
                        $scope.auctionItem.status = 'Negotiation Ended';

                        swal({
                            title: "Thanks!",
                            text: "Negotiation complete! Thank you for being a part of this.",
                            type: "success",
                            showCancelButton: false,
                            confirmButtonColor: "#DD6B55",
                            confirmButtonText: "Ok",
                            closeOnConfirm: true
                        },
                            function () {
                                location.reload();
                                $log.info("Negotiation Ended.");
                            });
                    }

                    if (event.targetScope.countdown <= 3) {
                        $scope.disableAddButton = true;
                    }
                    if (event.targetScope.countdown < 6) {
                        $scope.disableBidButton = true;
                    }
                    if (event.targetScope.countdown > 3) {
                        //$scope.getData();
                        $scope.disableAddButton = false;
                    }
                    if (event.targetScope.countdown >= 6) {
                        $scope.disableBidButton = false;
                    }
                }
                else if ($scope.auctionItem.IS_CB_ENABLED == true || $scope.auctionItem.IS_CB_ENABLED == 1) {
                    //var temp = event.targetScope.countdown;
                    //console.log(temp);
                    //$scope.auctionItem.CB_TIME_LEFT = $scope.auctionItem.CB_TIME_LEFT - 1;

                    if (event.targetScope.minutes < 2) {
                        $scope.timerStyle = {
                            'color': '#f00',
                            '-webkit - animation': 'flash linear 1s infinite',
                            'animation': 'flash linear 1s infinite'
                        };
                    }

                    if (event.targetScope.minutes >= 2 || event.targetScope.days > 0 || event.targetScope.hours > 0) {
                        $scope.timerStyle = { 'color': '#228B22' };
                    }


                    if (event.targetScope.days == 0 &&
                        event.targetScope.hours == 0 &&
                        event.targetScope.minutes == 0 &&
                        (event.targetScope.seconds == 1)) {
                        location.reload();
                    }
                }

            });


            requirementHub.on('timerUpdateSignalR', function (payloadObj) {
                if (payloadObj && payloadObj.payLoad && payloadObj.payLoad.length > 0) {
                    $scope.auctionItem.timeLeft = +payloadObj.payLoad[0];
                    $scope.$broadcast('timer-set-countdown-seconds', +payloadObj.payLoad[0]);
                }
            });

            requirementHub.on('timerUpdateLotSignalR', function (payloadObj) {
                if (payloadObj && payloadObj.payLoad && payloadObj.payLoad.length > 0 && $scope.auctionItem && $scope.auctionItem.status !== 'STARTED') {
                    window.location.reload();
                }
            });

            requirementHub.on('checkRequirement', function (items) {
                var req = items.inv;
                var iteminvcallerID = 0;
                if ($scope.auctionItem.IS_CB_ENABLED == false || $scope.auctionItem.IS_CB_ENABLED == 0) {
                    if (items.inv) {
                        iteminvcallerID = items.inv.callerID;
                    }
                    if ($scope.isCustomer) {
                        var itemTemp = _.filter(items.payLoad, function (userItem) {
                            if (userItem.superUserID === $scope.auctionItem.superUserID) {
                                $scope.signalRCustomerAccess = true;
                            }

                            return userItem.superUserID === $scope.auctionItem.superUserID;
                        });
                    }
                    else {
                        var itemTemp = _.filter(items.payLoad, function (userItem) {
                            return userItem.entityID === +$scope.currentUserId;
                        });
                    }
                    let item = itemTemp[0];
                    $scope.setAuctionInitializer(item, '', iteminvcallerID);
                    $scope.$broadcast('timer-set-countdown-seconds', $scope.auctionItem.timeLeft);
                    $('.datetimepicker').datetimepicker({
                        useCurrent: false,
                        icons: {
                            time: 'glyphicon glyphicon-time',
                            date: 'glyphicon glyphicon-calendar',
                            up: 'glyphicon glyphicon-chevron-up',
                            down: 'glyphicon glyphicon-chevron-down',
                            previous: 'glyphicon glyphicon-chevron-left',
                            next: 'glyphicon glyphicon-chevron-right',
                            today: 'glyphicon glyphicon-screenshot',
                            clear: 'glyphicon glyphicon-trash',
                            close: 'glyphicon glyphicon-remove'

                        },

                        minDate: item.currentTime
                    });
                    if (!$scope.NegotiationEnded) {
                        $scope.$broadcast('timer-start');
                    }
                    if (id == req.requirementID && ($scope.signalRCustomerAccess || $scope.currentUserId == req.customerID || $scope.currentUserId == req.superUserID || req.userIDList.indexOf($scope.currentUserId) > -1 || req.custCompID == $scope.currentUserCompID)) {
                        if (req.methodName == "UpdateTime" && req.userIDList.indexOf($scope.currentUserId) > -1) {
                            growlService.growl("Negotiation time has been updated.", 'inverse');
                        } else if (req.methodName == "MakeBid" && ($scope.signalRCustomerAccess || $scope.currentUserId == req.customerID || req.superUserID == $scope.currentUserId || $scope.auctionItem.customerReqAccess || req.custCompID == $scope.currentUserCompID)) {
                            growlService.growl("A vendor has made a bid.", "success");
                        } else if (req.methodName == "RestartNegotiation") {
                            window.location.reload();
                        } else if (req.methodName == "RevUploadQuotation") {

                        } else if (req.methodName == "StopBids") {
                            if (req.userIDList.indexOf($scope.currentUserId) > -1) {
                                growlService.growl("Negotiation Time reduced to 1 minute by the customer. New bids will not extend time.");
                            }
                        } else if (req.methodName == "SAVE_DIFFERENTIAL_FACTOR") {
                            if ($scope.auctionItem.status == 'STARTED') {
                                growlService.growl("A vendor has made a bid.", "success");
                            } else {
                                //growlService.growl(req.status, "success");
                            }
                        } else if (!$scope.NegotiationEnded) {
                            auctionsService.isnegotiationended(id, $scope.currentSessionId)
                                .then(function (response) {
                                    if (response.errorMessage == '') {
                                        if (response.objectID == 1) {
                                            $scope.NegotiationEnded = true;
                                            if (($scope.signalRCustomerAccess || $scope.currentUserId == req.customerID || req.superUserID == $scope.currentUserId || $scope.auctionItem.customerReqAccess)) {
                                                // swal("Negotiation Completed!", "Congratulations! your procurement process is now completed. " + $scope.toprankerName + " is the least bidder with the value " + $scope.auctionItem.minPrice + " " + $scope.auctionItem.currency + " \n Your savings through PRM 360 :" + ($scope.auctionItem.savings) + " " + $scope.auctionItem.currency, "success");
                                                swal("Negotiation Completed!", "Congratulations! your procurement process is now completed.", "success");
                                            } else if (req.userIDList.indexOf($scope.currentUserId) > -1 && $scope.timeLeftMessage == "Negotiation Ends in: ") {
                                                if ($scope.vendorRank == 1) {
                                                    swal("Negotiation Completed!", "Congratulations! you are the least bidder for this requirement. Your price is : " + $scope.auctionItem.minPrice + " " + $scope.auctionItem.currency + "\n Customer would be reaching out you shortly. All the best!", "success");
                                                } else {
                                                    swal("Negotiation Completed!", "Bidding Completed.\n Thank You for your interest on this requirement. You ranked " + $scope.vendorRank + " in this requirement. Thank you for your participation.", "success");
                                                }
                                            }
                                        }
                                    }
                                });
                        }
                    } else {
                        if (req.methodName == "SAVE_DIFFERENTIAL_FACTOR" && iteminvcallerID != $scope.currentUserId) {
                            growlService.growl("A vendor has made a bid.", "success");
                        }
                    }
                }
                else if ($scope.auctionItem.IS_CB_ENABLED) {

                    if ($scope.userIsOwner) {


                        if (items.inv.methodName == 'CheckRequirement') {
                            var vendorCompanyName = '';
                            $scope.auctionItem.requirementVendorsList.forEach(function (vendor, vendorIndex) {
                                if (items.inv.callerID == vendor.vendorID) {
                                    vendorCompanyName = vendor.companyName;
                                    growlService.growl(vendorCompanyName + ' has made a bid.', "success");
                                }
                            });
                        }

                        $scope.getData();
                    } else if ($scope.auctionItem.auctionVendors[0].isQuotationRejected == 0) {
                        $scope.getData();
                    }
                }
            });

            $scope.stopBids = function () {
                swal({
                    title: "Are you sure?",
                    text: "The Negotiation will be stopped after one minute.",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#F44336",
                    confirmButtonText: "Yes, Stop Bids!",
                    closeOnConfirm: true
                }, function () {
                    var params = {};
                    params.reqID = id;
                    params.sessionID = $scope.currentSessionId;
                    params.userID = $scope.currentUserId;
                    var parties = params.reqID + "$" + params.userID + "$" + params.sessionID;
                    $scope.invokeSignalR('StopBids', parties, function () {
                        $scope.$broadcast('timer-set-countdown-seconds', 60);
                        $scope.disableButtons();
                        swal("Done!", "Negotiation time reduced to one minute.", "success");
                    });

                    //requirementHub.invoke('StopBids', parties, function (req) {
                    //    $scope.$broadcast('timer-set-countdown-seconds', 60);
                    //    $scope.disableButtons();
                    //    swal("Done!", "Negotiation time reduced to one minute.", "success");
                    //});
                });
            };

            $scope.disableButtons = function () {
                $scope.buttonsDisabled = true;
            }

            $scope.enableButtons = function () {
                $scope.buttonsDisabled = false;
            }

            $scope.editRequirement = function () {
                $log.info('in edit' + $stateParams.Id);
                //$state.go('form.addnewrequirement', { 'Id': $stateParams.Id });
                $state.go('save-requirementAdv', { 'Id': $stateParams.Id });
            }

            $scope.goToReqReport = function (reqID) {
                //$state.go("reports", { "reqID": reqID });

                var url = $state.href('reports', { "reqID": reqID });
                $window.open(url, '_blank');

            };

            $scope.generatePOforUser = function () {
                $state.go('po', { 'reqID': $stateParams.Id });
            }

            $scope.metrialDispatchmentForm = function () {
                $state.go('material-dispatchment', { 'Id': $stateParams.Id });
            };

            $scope.paymentdetailsForm = function () {
                $state.go('payment-details', { 'Id': $stateParams.Id });
            };

            $scope.deleteRequirement = function () {
                swal({
                    title: "Are you sure?",
                    text: "The Negotiation will be deleted and an email will be sent out to all vendors involved.",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#F44336",
                    confirmButtonText: "Yes, I am sure",
                    closeOnConfirm: true
                }, function () {
                    var params = {};
                    params.reqID = id;
                    params.sessionID = $scope.currentSessionId;
                    params.userID = $scope.currentUserId;
                    // auctionsService.updatebidtime(params);
                    // swal("Done!", "Auction time reduced to oneminute.", "success");
                    var parties = params.reqID + "$" + params.userID + "$" + params.sessionID + "$" + $scope.reason;
                    $scope.invokeSignalR('DeleteRequirement', parties);
                    //requirementHub.invoke('DeleteRequirement', parties, function (req) {
                    //    $scope.$broadcast('timer-set-countdown-seconds', 60);
                    //    $scope.disableButtons();
                    //    swal("Done!", "Requirement has been cancelled", "success");
                    //});
                });
            };

            $scope.DeleteVendorFromAuction = function (VendoID, quotationUrl) {
                if (($scope.auctionItem.auctionVendors.length > 2 || quotationUrl == "") && (quotationUrl == "" || (quotationUrl != "" && $scope.auctionItem.auctionVendors[2].quotationUrl != ""))) {
                    swal({
                        title: "Are you sure?",
                        text: "The Vendor will be deleted and an email will be sent out to The vendor.",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#F44336",
                        confirmButtonText: "Yes, I am sure",
                        closeOnConfirm: true
                    }, function () {
                        var params = {};
                        params.reqID = id;
                        params.sessionID = $scope.currentSessionId;
                        params.userID = VendoID;

                        auctionsService.DeleteVendorFromAuction(params)
                            .then(function (response) {
                                if (response.errorMessage == '') {

                                    $scope.getData();
                                    swal("Done!", "Done! Vendor Deleted Successfully!", "success");
                                } else {
                                    swal("Error", "You cannot Delete Vendor", "inverse");
                                }
                            });
                    });
                }
                else {
                    swal("Not Allowed", "You are not allowed to Delete the Vendors.", "error");
                }
            };



            $scope.makeBidUnitPriceValidation = function (revUnitPrice, TemperoryRevUnitPrice, productIDorName) {
                if (TemperoryRevUnitPrice < revUnitPrice && ($scope.auctionItem.status == 'STARTED' || $scope.auctionItem.status == 'Negotiation Ended') && $scope.auctionItem.isDiscountQuotation == 0) {
                    $scope.getData();
                    swal("Error!", 'Please enter price less than ' + TemperoryRevUnitPrice + ' of ' + productIDorName);
                }

            };

            $scope.makeBidOtherChargesValidation = function () {

            };

            $scope.changeTotalItemPrice = function (product) {
                if (product && product.itemPrice && product.productQuantity) {
                    product.itemPrice = +(product.itemPrice);
                    if (+product.sGst || +product.cGst || +product.iGst) {
                        let tax = 0;
                        if (+product.iGst) {
                            tax = +product.iGst;
                        } else if (+product.sGst || +product.cGst) {
                            tax = (+product.sGst) + (+product.cGst);
                        }

                        if (tax) {
                            product.unitPrice = (product.itemPrice / (1 + (tax / 100))) / product.productQuantity;
                        }

                    } else {
                        product.unitPrice = product.itemPrice / product.productQuantity;
                    }
                    $scope.totalprice = product.itemPrice;
                    $scope.unitPriceCalculation('PRC');
                    $scope.getTotalPrice($scope.discountAmount, $scope.freightCharges, $scope.totalprice,
                        $scope.packingCharges, $scope.packingChargesTaxPercentage, $scope.packingChargesWithTax,
                        $scope.installationCharges, $scope.installationChargesTaxPercentage, $scope.installationChargesWithTax,
                        $scope.freightChargesTaxPercentage, $scope.freightChargesWithTax);
                    $scope.getRevTotalPrice($scope.revfreightCharges, $scope.revtotalprice,
                        $scope.revpackingCharges, $scope.packingChargesTaxPercentage, $scope.revpackingChargesWithTax,
                        $scope.revinstallationCharges, $scope.installationChargesTaxPercentage, $scope.revinstallationChargesWithTax,
                        $scope.freightChargesTaxPercentage, $scope.revfreightChargesWithTax);
                }
            };

            $scope.unitPriceCalculation = function (val, product) {
                $scope.gstValidation = false;

                var itemfreight = 0;
                var itemRevfreight = 0;
                var myInit = 1;

                $scope.auctionItemVendor.listRequirementItems.forEach(function (item, index) {

                    if ((val == 'CGST' || val == 'SGST') && (item.sGst != 0 || item.cGst != 0) && myInit == 1) {
                        $scope.IGSTFields = true;
                        $scope.CSGSTFields = false;
                        myInit++;
                    } else if (val == 'IGST' && item.iGst != 0 && myInit == 1) {
                        $scope.CSGSTFields = true;
                        $scope.IGSTFields = false;
                        myInit++;
                    } else if (myInit == 1 && val != "PRC") {
                        $scope.CSGSTFields = false;
                        $scope.IGSTFields = false;
                    }

                    if (val == 'GST') {

                        if ($scope.auctionItem.isDiscountQuotation == 2) {

                            item = $scope.handlePrecision(item, 2);
                            if ($scope.auctionItem.isTabular && ($scope.auctionItem.status == 'UNCONFIRMED' || $scope.auctionItem.status == 'NOTSTARTED')) {
                                item.revCostPrice = item.costPrice;
                                item.revmarginAmount = item.marginAmount;
                            }

                            item.cGst = item.Gst / 2;
                            item.sGst = item.Gst / 2;
                            item.netPrice = 1 + item.Gst / 100;
                            item = $scope.handlePrecision(item, 2);
                            item.netPrice = item.costPrice * item.netPrice;
                            item = $scope.handlePrecision(item, 2);
                            item.marginAmount = item.unitMRP - item.netPrice;

                            item.revnetPrice = 1 + item.Gst / 100;
                            item = $scope.handlePrecision(item, 2);
                            item.revnetPrice = item.revCostPrice * item.revnetPrice;
                            item = $scope.handlePrecision(item, 2);
                            item.revmarginAmount = item.unitMRP - item.revnetPrice;

                            item = $scope.handlePrecision(item, 2);
                            //item.unitDiscount = (item.marginAmount / item.costPrice);
                            item.unitDiscount = (item.marginAmount / item.netPrice);
                            item.unitDiscount = item.unitDiscount * 100;
                            item = $scope.handlePrecision(item, 2);
                        }
                    }
                    if ($scope.auctionItem.biddingType === 'SPOT' ? $scope.auctionItem.isTabular && $scope.evaluateBiddingTypeBasedOnStatus() : $scope.auctionItem.isTabular && ($scope.auctionItem.status == 'UNCONFIRMED' || $scope.auctionItem.status == 'NOTSTARTED')) {
                        if ($scope.auctionItem.auctionVendors[0].isQuotationRejected != 0) {
                            item.revUnitPrice = item.unitPrice;
                        }

                        //item.sGst = item.cGst;
                        var tempUnitPrice = item.unitPrice;
                        var tempCGst = item.cGst;
                        var tempSGst = item.sGst;
                        var tempIGst = item.iGst;
                        if (item.unitPrice == undefined || item.unitPrice <= 0) {
                            item.unitPrice = 0;
                        };

                        item.itemPrice = item.unitPrice * item.productQuantity;
                        if (item.cGst == undefined || item.cGst <= 0) {
                            item.cGst = 0;
                        } else if (val == 'cGST') {
                            // item.sGst = item.cGst;
                            item.iGst = 0;
                            tempCGst = item.cGst;
                            tempSGst = item.sGst;
                            tempIGst = item.iGst;
                        }

                        if (item.sGst == undefined || item.sGst <= 0) {
                            item.sGst = 0;
                        } else if (val == 'sGST') {
                            // item.cGst = item.sGst;
                            item.iGst = 0;

                            tempCGst = item.cGst;
                            tempSGst = item.sGst;
                            tempIGst = item.iGst;
                        }
                        if (item.iGst == undefined || item.iGst <= 0) {
                            item.iGst = 0;
                        } else if (val == 'iGST') {
                            item.cGst = item.sGst = 0;

                            tempCGst = item.cGst;
                            tempSGst = item.sGst;
                            tempIGst = item.iGst;
                        }

                        //if (item.cGst < 10) {
                        //    if (item.cGst.toString().length > 4) {
                        //        tempCGst = 0;
                        //        item.cGst = 0;
                        //        swal("Error!", 'Please enter valid GST');
                        //    }
                        //};

                        //if (item.cGst >= 10 || item.cGst <= 100) {
                        //    if (item.cGst.toString().length > 5) {
                        //        tempCGst = 0;
                        //        item.cGst = 0;
                        //        swal("Error!", 'Please enter valid GST');
                        //    }
                        //};

                        //if (item.sGst < 10) {
                        //    if (item.sGst.toString().length > 4) {
                        //        tempSGst = 0;
                        //        item.sGst = 0;
                        //        swal("Error!", 'Please enter valid GST');
                        //    }
                        //};

                        //if (item.sGst >= 10 || item.sGst <= 100) {
                        //    if (item.sGst.toString().length > 5) {
                        //        tempSGst = 0;
                        //        item.sGst = 0;
                        //        swal("Error!", 'Please enter valid GST');
                        //    }
                        //};

                        //if (item.iGst < 10) {
                        //    if (item.iGst.toString().length > 4) {
                        //        tempIGst = 0;
                        //        item.iGst = 0;
                        //        swal("Error!", 'Please enter valid GST');
                        //    }
                        //};

                        //if (item.iGst >= 10 || item.iGst <= 100) {
                        //    if (item.iGst.toString().length > 5) {
                        //        tempIGst = 0;
                        //        item.iGst = 0;
                        //        swal("Error!", 'Please enter valid GST');
                        //    }
                        //};

                        if ((parseFloat(item.cGst) + parseFloat(item.sGst)) > 100) {
                            item.cGst = item.sGst = tempCGst = tempSGst = 0;
                            swal("Error!", 'Please enter valid TAX %');
                        } else if (item.iGst > 100) {
                            item.iGst = tempIGst = 0;
                            swal("Error!", 'Please enter valid TAX %');
                        }

                        item.itemPrice = (item.itemPrice + ((item.itemPrice / 100) * (parseFloat(item.cGst) + parseFloat(item.sGst) + parseFloat(item.iGst))) + 0);

                        item.unitPrice = tempUnitPrice;
                        item.cGst = tempCGst;
                        item.sGst = tempSGst;
                        item.iGst = tempIGst;

                        if (item.cGst < 0 || item.sGst < 0 || item.iGst < 0 || item.cGst == undefined || item.sGst == undefined || item.iGst == undefined || item.cGst > 100 || item.sGst > 100 || item.iGst > 100) {
                            // $scope.gstValidation = true;
                            // not mandatory
                        }
                    }


                    if ($scope.auctionItem.isTabular && ($scope.auctionItem.status == 'Negotiation Ended' || $scope.auctionItem.status == 'STARTED' || $scope.auctionItem.status == 'UNCONFIRMED' || $scope.auctionItem.status == 'NOTSTARTED')) {

                        var tempRevUnitPrice = item.revUnitPrice;
                        tempCGst = item.cGst;
                        tempSGst = item.sGst;
                        tempIGst = item.iGst;

                        if (item.revUnitPrice == undefined || item.revUnitPrice <= 0) {
                            item.revUnitPrice = 0;
                        }

                        item.revitemPrice = item.revUnitPrice * item.productQuantity;
                        if (item.cGst == undefined || item.cGst <= 0) {
                            item.cGst = 0;
                        }

                        if (item.sGst == undefined || item.sGst <= 0) {
                            item.sGst = 0;
                        }

                        if (item.iGst == undefined || item.iGst <= 0) {
                            item.iGst = 0;
                        }

                        item.revitemPrice = (item.revitemPrice + ((item.revitemPrice / 100) * (parseFloat(item.cGst) + parseFloat(item.sGst) + parseFloat(item.iGst))) + 0);
                        item.revUnitPrice = tempRevUnitPrice;
                        item.cGst = tempCGst;
                        item.sGst = tempSGst;
                        item.iGst = tempIGst;

                        if (item.cGst < 0 || item.sGst < 0 || item.iGst < 0 || item.cGst == undefined || item.sGst == undefined || item.iGst == undefined || item.cGst > 100 || item.sGst > 100 || item.iGst > 100) {
                            // $scope.gstValidation = true;
                            // not mandatory
                        }
                    }
                });

                // For disablig non core item prices when total price is not gretaer than 0
                if ($scope.totalprice > 0) {
                    $scope.fieldValidation();
                }
                // For disablig non core item prices when total price is not gretaer than 0

            };

            $scope.regretError = false;



            $scope.getTotalPrice = function (discountAmount, freightCharges, totalprice,
                packingCharges, packingChargesTaxPercentage, packingChargesWithTax,
                installationCharges, installationChargesTaxPercentage, installationChargesWithTax,
                freightChargesTaxPercentage, freightChargesWithTax) {
                if (!$scope.auctionItem) {
                    return;
                }


                $scope.regretError = false;
                $scope.priceValidationsVendor = '';
                $scope.taxValidation = false;
                $scope.ItemPriceValidation = false;
                $scope.discountfreightValidation = false;

                $scope.discountAmount = $scope.precisionRound(parseFloat(discountAmount), $rootScope.companyRoundingDecimalSetting);

                if (!$scope.auctionItem.isTabular) {
                    $scope.totalprice = totalprice;
                    if (isNaN($scope.totalprice) || $scope.totalprice == undefined || $scope.totalprice < 0 || $scope.totalprice == '') {
                        //$("#totalprice").val(0);
                        $scope.vendorBidPrice = 0;
                        $scope.vendorBidPriceWithoutDiscount = 0;
                    }
                }
                else {
                    $scope.auctionItem.isUOMDifferent = false;
                    if ($scope.auctionItemVendor && $scope.auctionItemVendor.listRequirementItems.length > 0) {

                        $scope.auctionItemVendor.listRequirementItems.forEach(function (item, index) {

                            if (item.unitPrice > 0) {
                                item.isRegret = false;
                                item.regretComments = '';
                            }

                            if (item.isRegret && (item.regretComments == '' || item.regretComments == undefined || item.regretComments == null)) {
                                $scope.regretError = true;
                            }

                            if (item.productQuantityIn.toUpperCase() != item.vendorUnits.toUpperCase()) {
                                //$scope.auctionItem.isUOMDifferent = true;
                            }

                            if (isNaN(item.itemPrice) || item.itemPrice == undefined || item.itemPrice < 0) {
                                $scope.ItemPriceValidation = true;
                            }

                            $scope.regretHandle(item);

                        });

                    }
                }

                $scope.discountAmountlocal = $scope.discountAmount;
                if (isNaN($scope.discountAmount) || $scope.discountAmount == undefined || $scope.discountAmount < 0) {
                    $scope.discountAmountlocal = 0;
                    $scope.discountfreightValidation = true;
                }

                $scope.discountAmountlocal = $scope.precisionRound(parseFloat($scope.discountAmountlocal), $rootScope.companyRoundingDecimalSetting);

                if ($scope.auctionItem.isTabular) {
                    $scope.totalprice = _.sumBy($scope.auctionItemVendor.listRequirementItems, 'itemPrice');
                    $scope.totalprice = $scope.precisionRound(parseFloat($scope.totalprice), $rootScope.companyRoundingDecimalSetting);
                    if ($scope.auctionItem.isDiscountQuotation == 2) {
                        $scope.totalprice = 0;
                        $scope.auctionItemVendor.listRequirementItems.forEach(function (item, index) {
                            item = $scope.handlePrecision(item, 2);
                            $scope.totalprice += item.netPrice * item.productQuantity;
                            item = $scope.handlePrecision(item, 2);
                        });
                    }

                    $scope.vendorBidPrice = $scope.precisionRound(parseFloat($scope.totalprice), $rootScope.companyRoundingDecimalSetting);

                    $scope.vendorBidPriceWithoutDiscount = $scope.vendorBidPrice;
                    $scope.vendorBidPrice = $scope.vendorBidPrice - $scope.discountAmountlocal;
                    $scope.vendorBidPrice = $scope.precisionRound(parseFloat($scope.vendorBidPrice), $rootScope.companyRoundingDecimalSetting);
                    if (!$scope.vendorBidPrice || isNaN($scope.vendorBidPrice)) {
                        $scope.vendorBidPrice = 0;
                    }

                    $scope.taxValidation = false;
                    if ($scope.auctionItem.auctionVendors && $scope.auctionItem.auctionVendors.length > 0) {
                        if ($scope.isTaxesMandatory && $scope.auctionItem.auctionVendors[0].selectedVendorCurrency === "INR" && $scope.auctionItemVendor.listRequirementItems && $scope.auctionItemVendor.listRequirementItems.length > 0) {
                            $scope.auctionItemVendor.listRequirementItems.forEach(function (item, index) {
                                if (!$scope.taxValidation && item.isCoreProductCategory && !item.isRegret) {
                                    if (item.isCoreProductCategory && (+item.iGst || +item.cGst || +item.sGst)) {
                                        $scope.taxValidation = false;
                                    } else {
                                        //$scope.taxValidation = true;
                                    }
                                }
                            });
                        }
                    }

                }
                if (!$scope.auctionItem.isTabular) {
                    $scope.totalprice = $scope.precisionRound(parseFloat($scope.totalprice), $rootScope.companyRoundingDecimalSetting);

                    $scope.calculatedSumOfAllTaxes = 0;
                    if ($scope.listRequirementTaxes) {
                        $scope.listRequirementTaxes.forEach(function (tax, index) {
                            if (tax.taxPercentage !== '' && tax.taxPercentage != undefined) {
                                $scope.calculatedSumOfAllTaxes += $scope.precisionRound(parseFloat($scope.totalprice), $rootScope.companyRoundingDecimalSetting)
                                    * $scope.precisionRound(parseFloat(tax.taxPercentage), $rootScope.companyRoundingDecimalSetting) / 100;
                            }
                        });

                        $scope.vendorTaxes = $scope.calculatedSumOfAllTaxes;

                        $scope.listRequirementTaxes.forEach(function (item, index) {
                            if (!item.taxName) {
                                $scope.taxValidation = true;
                            }
                            if (!item.taxPercentage) {
                                $scope.taxValidation = true;
                            }
                            if (item.taxPercentage <= 0) {
                                item.taxPercentage = 0;
                            }
                        });
                    }

                    $scope.vendorBidPrice = $scope.precisionRound(parseFloat($scope.totalprice), $rootScope.companyRoundingDecimalSetting) +
                        $scope.precisionRound(parseFloat($scope.vendorTaxes), $rootScope.companyRoundingDecimalSetting);

                    $scope.vendorBidPrice = $scope.precisionRound(parseFloat($scope.vendorBidPrice), $rootScope.companyRoundingDecimalSetting);

                    $scope.vendorBidPriceWithoutDiscount = $scope.vendorBidPrice;
                    $scope.vendorBidPrice = $scope.precisionRound(parseFloat($scope.vendorBidPrice), $rootScope.companyRoundingDecimalSetting) -
                        $scope.precisionRound(parseFloat($scope.discountAmountlocal), $rootScope.companyRoundingDecimalSetting);
                    $scope.vendorBidPrice = $scope.precisionRound(parseFloat($scope.vendorBidPrice), $rootScope.companyRoundingDecimalSetting);
                    if (!$scope.vendorBidPrice || isNaN($scope.vendorBidPrice)) {
                        $scope.vendorBidPrice = 0;
                    }
                }
            };

            $scope.getRevTotalPrice = function (revfreightCharges, revtotalprice,
                revpackingCharges, packingChargesTaxPercentage, revpackingChargesWithTax,
                revinstallationCharges, installationChargesTaxPercentage, revinstallationChargesWithTax,
                freightChargesTaxPercentage, revfreightChargesWithTax) {

                $scope.ItemPriceValidation = false;
                $scope.discountfreightValidation = false;

                if (!$scope.auctionItem.isTabular) {
                    $scope.revtotalprice = revtotalprice;
                    if (isNaN($scope.revtotalprice) || $scope.revtotalprice == undefined || $scope.revtotalprice < 0 || $scope.revtotalprice == '') {
                        $("#revtotalprice").val(0);
                        $scope.revvendorBidPrice = 0;
                    }
                }
                else {
                    if ($scope.auctionItemVendor.length > 0) {
                        $scope.auctionItemVendor.listRequirementItems.forEach(function (item, index) {

                            if (item.oldRevUnitPrice > 0 && item.revUnitPrice > 0 && item.oldRevUnitPrice > item.revUnitPrice && $scope.auctionItem.status == 'STARTED') {
                                if ($scope.auctionItem.isDiscountQuotation == 0) {
                                    var reductionval = parseFloat(item.oldRevUnitPrice).toFixed(2) - parseFloat(item.revUnitPrice).toFixed(2);
                                    //reductionval = parseFloat(reductionval).toFixed(2);                            
                                    if (reductionval >= item.itemMinReduction && reductionval > 0 && item.itemMinReduction > 0) {
                                        $scope.ItemPriceValidation = false;

                                    } else if (reductionval < item.itemMinReduction && reductionval > 0 && item.itemMinReduction > 0) {
                                        $scope.ItemPriceValidation = true;
                                        item.revUnitPrice = item.oldRevUnitPrice;
                                        $scope.getData();
                                        swal("Error!", 'Your Unit price reduction should be greater than Min.unit Price limit :' + item.itemMinReduction, "error");
                                        return;
                                    }
                                }

                            }

                            if (isNaN(item.revitemPrice) || item.revitemPrice == undefined || item.revitemPrice < 0) {
                                $scope.ItemPriceValidation = true;
                            }
                        });
                    }
                }


                if ($scope.auctionItem.isTabular) {
                    $scope.revtotalprice = _.sumBy($scope.auctionItemVendor.listRequirementItems, 'revitemPrice');
                    if ($scope.auctionItem.isDiscountQuotation == 2) {

                        $scope.revtotalprice = 0;

                        $scope.auctionItemVendor.listRequirementItems.forEach(function (item, index) {
                            item = $scope.handlePrecision(item, 2);
                            $scope.revtotalprice += item.revnetPrice * item.productQuantity;
                            item = $scope.handlePrecision(item, 2);
                        });
                    }



                    $scope.calculatedSumOfAllTaxes = 0;
                    if ($scope.listRequirementTaxes) {
                        $scope.listRequirementTaxes.forEach(function (tax, index) {
                            if (tax.taxPercentage != '' && tax.taxPercentage != undefined) {
                                $scope.calculatedSumOfAllTaxes += $scope.revtotalprice * tax.taxPercentage / 100;
                            }
                        });
                    }


                    $scope.vendorTaxes = $scope.calculatedSumOfAllTaxes;

                    $scope.revvendorBidPrice = $scope.precisionRound(parseFloat($scope.revtotalprice), $rootScope.companyRoundingDecimalSetting) +
                        $scope.precisionRound(parseFloat($scope.vendorTaxes), $rootScope.companyRoundingDecimalSetting);

                    $scope.revvendorBidPrice = $scope.precisionRound(parseFloat($scope.revvendorBidPrice), $rootScope.companyRoundingDecimalSetting);
                    if (!$scope.revvendorBidPrice || isNaN($scope.revvendorBidPrice)) {
                        $scope.revvendorBidPrice = 0;
                    }

                    $scope.reduceBidValue = $scope.precisionRound(parseFloat($scope.auctionItem.minPrice - $scope.revvendorBidPrice), $rootScope.companyRoundingDecimalSetting);
                    $scope.reduceBidAmount();
                }

                if (!$scope.auctionItem.isTabular) {
                    $scope.revtotalprice = $scope.precisionRound(parseFloat(revtotalprice), $rootScope.companyRoundingDecimalSetting);


                    $scope.calculatedSumOfAllTaxes = 0;
                    $scope.listRequirementTaxes.forEach(function (tax, index) {
                        if (tax.taxPercentage != '' && tax.taxPercentage != undefined) {
                            $scope.calculatedSumOfAllTaxes += $scope.revtotalprice * tax.taxPercentage / 100;
                        }
                    })
                    $scope.vendorTaxes = $scope.calculatedSumOfAllTaxes;

                    $scope.revvendorBidPrice = $scope.precisionRound(parseFloat($scope.revtotalprice), $rootScope.companyRoundingDecimalSetting) +
                        $scope.precisionRound(parseFloat($scope.vendorTaxes), 2);

                    $scope.revvendorBidPrice = $scope.precisionRound(parseFloat($scope.revvendorBidPrice), $rootScope.companyRoundingDecimalSetting);
                    if (!$scope.revvendorBidPrice || isNaN($scope.revvendorBidPrice)) {
                        $scope.revvendorBidPrice = 0;
                    }
                }

            };





            $scope.currencyValidationStyle = {};

            $scope.uploadQuotation = function (quotationType) {
                let hasItemQuotationRejected = false;
                $scope.currencyValidationStyle = {};

                if (!$scope.auctionItem.auctionVendors[0].selectedVendorCurrency) {
                    document.getElementById('priceDiv').scrollIntoView();
                    $scope.currencyValidationStyle = {
                        'border-color': 'red'
                    };
                    growlService.growl('Please Select Currency', "inverse");
                    return;
                };

                var uploadQuotationError = false;
                $scope.auctionItemVendor.listRequirementItems.forEach(function (item, index) {

                    if (!hasItemQuotationRejected && item.isItemQuotationRejected === 1) {
                        hasItemQuotationRejected = true;
                    }

                    if (uploadQuotationError == true) {
                        return false;
                    }
                    var slno = index + 1;
                    if (item.isRegret) {
                        if (item.regretComments == '' || item.regretComments == null || item.regretComments == undefined) {
                            growlService.growl("Please enter regret comments.", "inverse");
                            uploadQuotationError = true;
                            return false;
                        }
                    } else {
                        var isTaxDisabled = $scope.isTaxFieldEditableForSubItems(item.productQuotationTemplateArray);
                        if (item.unitPrice <= 0 && item.isCoreProductCategory > 0) {
                            growlService.growl("Please enter Unit price for Product " + slno + ".", "inverse");
                            uploadQuotationError = true;
                            return false;
                        }
                        //else if (!isTaxDisabled && item.unitPrice > 0 && (+item.iGst == 0 && (+item.cGst == 0 && +item.sGst == 0)) && ($scope.auctionItem.auctionVendors[0].selectedVendorCurrency == 'INR')) {
                        //    growlService.growl("Please enter SGST/CGST/IGST for Product " + slno + ".", "inverse");
                        //    uploadQuotationError = true;
                        //    return false;
                        //}
                        //else if (!isTaxDisabled && item.unitPrice > 0 && (+item.cGst == 0 && +item.sGst > 0) && ($scope.auctionItem.auctionVendors[0].selectedVendorCurrency == 'INR')) {
                        //    growlService.growl("Please enter CGST for Product " + slno + ".", "inverse");
                        //    uploadQuotationError = true;
                        //    return false;
                        //}
                        //else if (!isTaxDisabled && item.unitPrice > 0 && (+item.cGst > 0 && +item.sGst == 0) && ($scope.auctionItem.auctionVendors[0].selectedVendorCurrency == 'INR')) {
                        //    growlService.growl("Please enter SGST for Product " + slno + ".", "inverse");
                        //    uploadQuotationError = true;
                        //    return false;
                        //}
                    }


                });

                var TaxFiledValidation = 0;

                if ($scope.CSGSTFields == false) {
                    TaxFiledValidation = 1;
                }

                if (uploadQuotationError == true) {
                    return;
                }

                $scope.auctionItemVendor.listRequirementItems.forEach(function (item, itemIndexs) {
                    item.productIDorName = validateStringWithoutSpecialCharacters(item.productIDorName);
                    item.productNo = validateStringWithoutSpecialCharacters(item.productNo);
                    item.productDescription = validateStringWithoutSpecialCharacters(item.productDescription);
                    item.productBrand = validateStringWithoutSpecialCharacters(item.productBrand);
                    item.productDeliveryDetails = validateStringWithoutSpecialCharacters(item.productDeliveryDetails);
                    item.itemLevelInitialComments = validateStringWithoutSpecialCharacters(item.itemLevelInitialComments);
                    item.itemLevelRevComments = validateStringWithoutSpecialCharacters(item.itemLevelRevComments);
                    item.regretComments = validateStringWithoutSpecialCharacters(item.regretComments);
                    item.itemQuotationRejectedComment = validateStringWithoutSpecialCharacters(item.itemQuotationRejectedComment);

                    if (item.productQuotationTemplateArray && item.productQuotationTemplateArray.length > 0) {
                        item.productQuotationTemplateArray.forEach(function (subItem, subItemIndex) {
                            subItem.DESCRIPTION = validateStringWithoutSpecialCharacters(subItem.DESCRIPTION);
                            subItem.dateCreated = "/Date(1561000200000+0530)/";
                            subItem.dateModified = "/Date(1561000200000+0530)/";
                        });
                    }
                    item.productQuotationTemplateJson = JSON.stringify(item.productQuotationTemplateArray);
                    if ($scope.auctionItem.biddingType === 'SPOT') {
                        item.revUnitPrice = item.unitPrice;
                    }

                    //Remove Special Chars
                    //item.productQuotationTemplateJson = item.productQuotationTemplateJson.replace(/\'/gi, "\\'");
                    //item.productQuotationTemplateJson = item.productQuotationTemplateJson.replace(/(\r\n|\n|\r)/gm, "");
                    //let tempTemplateArray = JSON.parse(item.productQuotationTemplateJson);
                    //subItem.SPECIFICATION = item.productQuotationTemplateJson.SPECIFICATION.replace(/\'/gi, "\\'");
                    //if (tempTemplateArray && tempTemplateArray.length > 0) {
                    //    tempTemplateArray.forEach(function (subItem, subItemIndex) {
                    //        if (subItem.SPECIFICATION) {
                    //            subItem.SPECIFICATION = subItem.SPECIFICATION.replace(/"/g, '\\"');
                    //        }
                    //    });

                    //    item.productQuotationTemplateJson = JSON.stringify(tempTemplateArray);
                    //}

                });
                $scope.warranty = validateStringWithoutSpecialCharacters($scope.warranty);
                $scope.validity = validateStringWithoutSpecialCharacters($scope.validity);
                $scope.duration = validateStringWithoutSpecialCharacters($scope.duration);
                $scope.payment = validateStringWithoutSpecialCharacters($scope.payment);
                $scope.otherProperties = validateStringWithoutSpecialCharacters($scope.otherProperties);

                var params = {
                    'quotationObject': $scope.auctionItemVendor.listRequirementItems, // 1
                    'userID': $scope.currentUserId,
                    'reqID': $scope.auctionItem.requirementID,
                    'sessionID': $scope.currentSessionId,
                    'price': $scope.totalprice, // 5
                    'tax': $scope.vendorTaxes,
                    'vendorBidPrice': $scope.vendorBidPrice ? $scope.vendorBidPrice : 0,
                    'warranty': $scope.warranty,
                    'duration': $scope.duration,
                    'payment': $scope.payment, // 10
                    'gstNumber': $scope.gstNumber,
                    'validity': $scope.validity,
                    'otherProperties': $scope.otherProperties,
                    'quotationType': quotationType,
                    'revised': 0, // 15
                    'discountAmount': $scope.discountAmount ? $scope.discountAmount : 0,
                    'listRequirementTaxes': $scope.listRequirementTaxes,
                    'quotationAttachment': $scope.quotationAttachment,
                    'multipleAttachments': $scope.multipleAttachmentsList,
                    'TaxFiledValidation': TaxFiledValidation,
                    'selectedVendorCurrency': $scope.auctionItem.auctionVendors[0].selectedVendorCurrency, //35
                    'isVendAckChecked': $scope.auctionItem.auctionVendors[0].isVendAckChecked,
                    'quotFreezeTime': $scope.auctionItem.quotationFreezTime
                };

                params.unitPrice = $scope.auctionItem.unitPrice;
                params.productQuantity = $scope.auctionItem.productQuantity;
                params.cGst = $scope.auctionItem.cGst;
                params.sGst = $scope.auctionItem.sGst;
                params.iGst = $scope.auctionItem.iGst;
                params.INCO_TERMS = $scope.auctionItem.auctionVendors[0].INCO_TERMS;

                //auctionItem.auctionVendors[0].INCO_TERMS

                //if (!params.warranty) {
                //    params.warranty = '';
                //    growlService.growl('Please enter Warranty.', "inverse");
                //    return false;
                //}

                //if (!params.validity) {
                //    params.validity = '';
                //    growlService.growl('Please enter Price Validity.', "inverse");
                //    return false;
                //}

                if (!params.otherProperties) {
                    params.otherProperties = '';
                }

                //if (params.duration == null || params.duration == '' || params.duration == undefined) {
                //    params.duration = '';
                //    growlService.growl('Please enter Delivery Terms.', "inverse");
                //    return false;
                //}

                if (!params.payment) {
                    params.payment = '';
                    swal({
                        title: "Error",
                        text: "Please enter the Payment Terms",
                        type: "error",
                        showCancelButton: false,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Ok",
                        closeOnConfirm: true
                    });
                    return false;
                }

                //if ((params.payment == validateStringWithoutSpecialCharacters($scope.auctionItem.paymentTerms)) && !$scope.paymentPopUp) {
                //    swal({
                //        title: "",
                //        text: "Please check the payment terms",
                //        type: "warning",
                //        showCancelButton: false,
                //        confirmButtonClass: "btn-danger",
                //        confirmButtonText: "Ok",
                //        closeOnConfirm: true
                //    },
                //        function (isConfirm) {
                //            if (isConfirm) {
                //                $scope.paymentPopUp = true;
                //            } else {
                //                $scope.paymentPopUp = false;
                //            }
                //        });
                //    if (!$scope.paymentPopUp) {
                //        return;
                //    }
                //}

                //if (params.INCO_TERMS == null || params.INCO_TERMS == '' || params.INCO_TERMS == undefined) {
                //    params.payment = '';
                //    if ($scope.totalprice > 0) {
                //        growlService.growl('Please select Inco Terms.', "inverse");
                //        return false;
                //    }
                //}

                if (!params.gstNumber) {
                    params.gstNumber = '';
                }

                $scope.specificationData = [];
                params.quotationObject.forEach(function (item) {
                    if (item.productQuotationTemplateArray) {
                        item.productQuotationTemplateArray.forEach(function (spec) {
                            if ((!spec.SPECIFICATION || spec.SPECIFICATION === null) && spec.T_ID > 0 && spec.IS_MANDATE_TO_VENDOR === 1 && spec.IS_VALID === 1 && !item.isRegret) {
                                $scope.specificationData.push(spec.NAME)
                            }
                        })
                    }
                })
                if ($scope.specificationData.length > 0) {
                    growlService.growl('Please enter ' + $scope.specificationData.toString() + ' Specification.', "inverse");
                    return false;
                }

                if ($scope.auctionItem.status == 'Negotiation Ended') {
                    params.price = $scope.revtotalprice,
                        params.tax = $scope.vendorTaxes,
                        params.vendorBidPrice = $scope.revvendorBidPrice,
                        params.revised = 1
                }


                if (($scope.auctionItem.status == 'UNCONFIRMED' || $scope.auctionItem.status == 'NOTSTARTED') && $scope.auctionItem.isQuotationPriceLimit && $scope.vendorBidPrice > $scope.auctionItem.quotationPriceLimit) {
                    swal("Error!", 'Your Amount Should be less than Quotation Price limit :' + $scope.auctionItem.quotationPriceLimit, "error");
                    return;
                }

                $scope.taxEmpty = false;
                $scope.auctionItemVendor.listRequirementItems.forEach(function (item, index) {
                    if ((item.unitMRP > 0 && $scope.auctionItem.isDiscountQuotation == 1) || (item.unitPrice > 0 && $scope.auctionItem.isDiscountQuotation == 0)) {
                        if ((item.cGst == 0 || item.sGst == 0 || item.cGst == undefined || item.sGst == undefined || item.cGst > 100 || item.sGst > 100) && (item.iGst == 0 || item.iGst == undefined || item.iGst > 100)) {
                            // $scope.taxEmpty = true;
                            // NOT MANDATORY
                        }
                    } else {
                        if ((item.cGst == undefined || item.sGst == undefined) && (item.iGst == undefined)) {
                            // $scope.taxEmpty = true;
                            // NOT MANDATORY
                        }
                    }
                });


                if ($scope.taxEmpty) {

                    swal({
                        title: "warning!",
                        text: "Please fill TAXES slab",
                        type: "warning",
                        showCancelButton: false,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "OK",
                        closeOnConfirm: true
                    });

                }
                else {
                    auctionsService.getdate()
                        .then(function (responseFromServer) {
                            var dateFromServer = new Date(parseInt(responseFromServer.substr(6)));
                            var auctionStart = new Date(parseInt($scope.auctionItem.startTime.substr(6)));
                            var timeofauctionstart = auctionStart.getTime();
                            var currentServerTime = dateFromServer.getTime();

                            if ($scope.auctionItem.status == "Negotiation Ended") {


                                // $scope.DeleteRequirementTerms();

                                if ($scope.auctionItem.status == 'Negotiation Ended') {
                                } else {
                                    // $scope.SaveRequirementTerms($scope.reqId);
                                }

                                auctionsService.uploadQuotation(params)
                                    .then(function (req) {
                                        if (req.errorMessage == '' || req.errorMessage == 'reviced') {
                                            if (req.errorMessage == '') {
                                                var parties = id + "$" + $scope.currentUserId + "$" + $scope.currentSessionId;
                                                //$scope.invokeSignalR('CheckRequirement', parties);
                                                //    requirementHub.invoke('CheckRequirement', parties, function () {                    
                                                //});
                                            }

                                            let successText = 'Your Quotation uploaded successfully';
                                            let type = "success";
                                            var quotationFreeTime = moment($scope.auctionItem.quotationFreezTime, "DD-MM-YYYY HH:mm").valueOf();
                                            var quotationFreeTimeMoment = moment(quotationFreeTime);
                                            var today = moment();
                                            //if (quotationFreeTimeMoment < today) {
                                            //    successText = 'Quotation Freeze time has passed, you have lost an opportunity to participate in negotiation.';
                                            //    type = "warning";
                                            //}

                                            swal({
                                                title: "Thanks!",
                                                text: successText,
                                                type: type,
                                                showCancelButton: false,
                                                confirmButtonColor: "#DD6B55",
                                                confirmButtonText: "Ok",
                                                closeOnConfirm: true
                                            },
                                                function () {
                                                    location.reload();
                                                });

                                        } else {
                                            swal({
                                                title: "Error",
                                                text: req.errorMessage,
                                                type: "error",
                                                showCancelButton: false,
                                                confirmButtonColor: "#DD6B55",
                                                confirmButtonText: "Ok",
                                                closeOnConfirm: true
                                            });
                                        }
                                    });

                            }
                            else {
                                if ($scope.auctionItem.biddingType === 'SPOT' && ($scope.auctionItem.minPrice <= 0 ? true : $scope.auctionItem.minPrice > 0 && $scope.auctionItem.auctionVendors[0].isQuotationRejected == 1) ? true : timeofauctionstart - currentServerTime >= 3600000 &&
                                    ($scope.auctionItem.auctionVendors[0].isQuotationRejected != 0 || hasItemQuotationRejected)) {

                                    // $scope.DeleteRequirementTerms();

                                    if ($scope.auctionItem.status == 'Negotiation Ended') {
                                    } else {
                                        //$scope.SaveRequirementTerms($scope.reqId);
                                    }

                                    auctionsService.uploadQuotation(params)
                                        .then(function (req) {
                                            if (req.errorMessage == '' || req.errorMessage == 'reviced') {
                                                if (req.errorMessage == '') {
                                                    var parties = id + "$" + $scope.currentUserId + "$" + $scope.currentSessionId;
                                                    //$scope.invokeSignalR('CheckRequirement', parties);
                                                    //    requirementHub.invoke('CheckRequirement', parties, function () {                    
                                                    //});
                                                }

                                                let successText = 'Your Quotation uploaded successfully';
                                                let type = "success";
                                                var quotationFreeTime = moment($scope.auctionItem.quotationFreezTime, "DD-MM-YYYY HH:mm").valueOf();
                                                var quotationFreeTimeMoment = moment(quotationFreeTime);
                                                var today = moment();
                                                //if (quotationFreeTimeMoment < today) {
                                                //    successText = 'Quotation Freeze time has passed, you have lost an opportunity to participate in negotiation.';
                                                //    type = "warning";
                                                //}

                                                swal({
                                                    title: "Thanks!",
                                                    text: successText,
                                                    type: type,
                                                    showCancelButton: false,
                                                    confirmButtonColor: "#DD6B55",
                                                    confirmButtonText: "Ok",
                                                    closeOnConfirm: true
                                                },
                                                    function () {
                                                        location.reload();
                                                    });

                                            } else {
                                                swal({
                                                    title: "Error",
                                                    text: req.errorMessage,
                                                    type: "error",
                                                    showCancelButton: false,
                                                    confirmButtonColor: "#DD6B55",
                                                    confirmButtonText: "Ok",
                                                    closeOnConfirm: true
                                                });
                                            }
                                        });
                                } else {

                                    var message = "You have missed the deadline for Quotation Submission";
                                    if ($scope.auctionItem.auctionVendors[0].isQuotationRejected == 0 && $scope.auctionItem.status != "Negotiation Ended") {
                                        message = $scope.showMessageBasedOnBiddingType('Your Quotation Already Approved You cant Update Quotation Please Contact PRM360', 'QUOTATION_UPLOAD');
                                    }

                                    swal({
                                        title: "Error",
                                        text: message,
                                        type: "error",
                                        showCancelButton: false,
                                        confirmButtonColor: "#DD6B55",
                                        confirmButtonText: "Ok",
                                        closeOnConfirm: true
                                    });
                                    $scope.getData();
                                }
                            }
                        });
                }
            };

            $scope.UploadRequirementItemsCeilingSave = function (fileContent) {
                var params = {
                    reqID: $scope.reqId,
                    sessionID: userService.getUserToken(),
                    requirementItemsAttachment: fileContent
                };
                auctionsService.UploadRequirementItemsCeilingSave(params)
                    .then(function (response) {
                        $("#requirementItemsCeilingSaveAttachment").val(null);
                        if (response) {
                            location.reload();
                        }
                    });
            };

            $scope.getFile1 = function (id, itemid, ext) {
                $scope.file = $("#" + id)[0].files[0];
                fileReader.readAsDataUrl($scope.file, $scope)
                    .then(function (result) {

                        if (id == "excelquotation") {
                            var bytearray = new Uint8Array(result);
                            $scope.quotationAttachment = $.makeArray(bytearray);
                            $scope.uploadquotationsfromexcel();
                        } else if (id == "clientsideupload") {
                            var bytearray = new Uint8Array(result);
                            $scope.quotationAttachment = $.makeArray(bytearray);
                            $scope.uploadclientsidequotation('clientsideupload');
                            $scope.quotationAttachment = null;
                            $scope.file = [];
                            $scope.file.name = '';
                        }
                        else if (id == "quotationBulkUpload") {
                            var bytearray = new Uint8Array(result);
                            $scope.quotationAttachment = $.makeArray(bytearray);
                            $scope.uploadclientsidequotation('quotationBulkUpload');
                            $scope.quotationAttachment = null;
                            $scope.file = [];
                            $scope.file.name = '';
                        }
                        else if (id === "requirementItemsCeilingSaveAttachment") {
                            if (ext !== "xlsx") {
                                swal("Error!", "File type should be XSLX. Please download the template and  fill values accordingly.", "error");
                                return;
                            }

                            var bytearray = new Uint8Array(result);
                            $scope.UploadRequirementItemsCeilingSave($.makeArray(bytearray));
                            $scope.file = [];
                            $scope.file.name = '';
                            //return;
                        }
                        else {
                            var bytearray = new Uint8Array(result);
                            var arrayByte = $.makeArray(bytearray);
                            var ItemFileName = $scope.file.name;
                            var index = _.indexOf($scope.auctionItemVendor.listRequirementItems, _.find($scope.auctionItemVendor.listRequirementItems, function (o) {
                                return o.productSNo == id;
                            }));
                            var obj = $scope.auctionItemVendor.listRequirementItems[index];
                            obj.itemAttachment = arrayByte;
                            obj.attachmentName = ItemFileName;
                            $scope.auctionItemVendor.listRequirementItems.splice(index, 1, obj);
                        }
                    });
            };

            $scope.QuotationRatioButton = {
                upload: 'generate'
            };

            $scope.rejectreson = '';
            $scope.rejectresonValidation = false;
            $scope.QuatationAprovelvalue = true;
            $scope.QuatationAprovel = function (userID, value, comment, action, auctionVendor) {
                let operationResult = 'Quotation Approved Successfully';
                if (value) {
                    operationResult = "Quotation Rejected Successfully";
                }

                if (auctionVendor && $scope.auctionItem.isTechScoreReq && auctionVendor.technicalScore <= 0 && !value) {
                    swal("Error!", 'Vendor can be approved only after Technical Score.', "error");
                    return;
                }

                if (comment) {
                    comment = comment.trim();
                }

                if ((userID == auctionVendor.vendorID) && value && !comment) {
                    auctionVendor.rejectresonValidation = true;
                    growlService.growl('Please enter reject Reason', "inverse");
                    return false;
                } else {
                    auctionVendor.rejectresonValidation = false;
                }

                var params = {
                    'vendorID': userID,
                    'reqID': $scope.auctionItem.requirementID,
                    'sessionID': $scope.currentSessionId,
                    'customerID': $scope.currentUserId,
                    'value': value,
                    'reason': comment,
                    'technicalScore': auctionVendor.technicalScore,
                    'action': action
                };

                auctionsService.QuatationAprovel(params).then(function (req) {
                    if (!req.errorMessage) {
                        swal({
                            title: "Done!",
                            text: operationResult,
                            type: "success",
                            showCancelButton: false,
                            confirmButtonColor: "#DD6B55",
                            confirmButtonText: "Ok",
                            closeOnConfirm: true
                        },
                            function () {
                                location.reload();
                            });

                    } else {
                        swal("Error!", req.errorMessage, "error");
                    }
                });
            };

            $scope.submitButtonShow = 0;

            $scope.submitButtonShowfunction = function (value, auctionVendor) {

                auctionVendor.quotationRejectedComment = '';
                $scope.submitButtonShow = value;
                let tempArr = _.filter($scope.vendorApprovals, function (vendor) {
                    return Number(vendor.vendorID) !== Number(auctionVendor.vendorID)
                });

                tempArr.push(auctionVendor);
                $scope.vendorApprovals = tempArr;

                $scope.auctionItem.auctionVendors.forEach(function (v, vI) {
                    if (v.vendorID == auctionVendor.vendorID) {
                        v.isApproveOrRejectClicked = true;
                    }
                })

            }

            $scope.saveApprovalStatus = function () {
                let promises = [];
                $scope.vendorApprovals.forEach(function (auctionVendor, index) {
                    auctionVendor.rejectresonValidation = false;
                    let action = '';
                    let comments = '';
                    let isRejected = null;
                    if ($scope.auctionItem.status === 'Negotiation Ended') {
                        action = 'revquotation';
                        comments = auctionVendor.revQuotationRejectedComment;
                        isRejected = auctionVendor.isRevQuotationRejected;
                        //$scope.QuatationAprovel(auctionVendor.vendorID, auctionVendor.isRevQuotationRejected, auctionVendor.revQuotationRejectedComment, 'revquotation', auctionVendor);
                    } else {
                        action = 'quotation';
                        comments = auctionVendor.quotationRejectedComment;
                        isRejected = auctionVendor.isQuotationRejected;
                        //$scope.QuatationAprovel(auctionVendor.vendorID, auctionVendor.isQuotationRejected, auctionVendor.quotationRejectedComment, 'quotation', auctionVendor);
                    }

                    if (auctionVendor && $scope.auctionItem.isTechScoreReq && auctionVendor.technicalScore <= 0 && !isRejected) {
                        swal("Error!", 'Vendor can be approved only after Technical Score.', "error");
                        return;
                    }

                    if (comments) {
                        comments = comments.trim();
                    }


                    if (isRejected && !comments) {
                        auctionVendor.rejectresonValidation = true;
                        growlService.growl('Please enter reject Reason', "inverse");
                        return false;
                    } else {
                        auctionVendor.rejectresonValidation = false;
                    }

                    let params = {
                        'vendorID': auctionVendor.vendorID,
                        'reqID': $scope.auctionItem.requirementID,
                        'sessionID': $scope.currentSessionId,
                        'customerID': $scope.currentUserId,
                        'value': isRejected,
                        'reason': comments,
                        'technicalScore': auctionVendor.technicalScore,
                        'action': action
                    };

                    promises.push(auctionsService.QuatationAprovel(params));

                });

                if (promises && promises.length > 0) {
                    $q.all(promises).then(function (responses) {
                        let allGood = true;
                        if (responses && responses.length > 0) {
                            responses.forEach(function (response, idx) {
                                if (response.errorMessage) {
                                    allGood = false;
                                }
                            });
                        }

                        if (allGood) {
                            swal({
                                title: "Done!",
                                text: 'Saved successfully.',
                                type: "success",
                                showCancelButton: false,
                                confirmButtonColor: "#DD6B55",
                                confirmButtonText: "Ok",
                                closeOnConfirm: true
                            },
                                function () {
                                    location.reload();
                                });

                        } else {
                            swal("Error!", 'Error occured Accepting all, please contact support', "error");
                        }
                    });
                }
            };

            $scope.NegotiationSettingsValidationMessage = '';

            $scope.NegotiationTimeValidation = function (days, hours, mins, minReduction, rankComparision) {

                $scope.days = days;
                $scope.hours = hours;
                $scope.mins = mins;


                $log.info($scope.days);
                $log.info($scope.hours);
                $log.info($scope.mins);

                $scope.NegotiationSettingsValidationMessage = '';

                if ($scope.auctionItem.isDiscountQuotation != 2) {
                    if (parseFloat(minReduction) <= 0 || minReduction == undefined) {
                        $scope.NegotiationSettingsValidationMessage = 'Set Min 0.01 for Min.Amount reduction';
                        return;
                    }

                    if (parseFloat(rankComparision) <= 0 || rankComparision == undefined) {
                        $scope.NegotiationSettingsValidationMessage = 'Set Min 0.01 for Rank Comparision price';
                        return;
                    }

                    //if (parseFloat(minReduction) >= 0 && parseFloat(rankComparision) > parseFloat(minReduction)) {
                    //    $scope.NegotiationSettingsValidationMessage = 'Please enter Valid Rank Comparision price less than Min. Amount reduction';
                    //    return;
                    //}
                } else if ($scope.auctionItem.isDiscountQuotation == 2) {
                    $scope.NegotiationSettings.minReductionAmount = 0;
                    $scope.NegotiationSettings.rankComparision = 0;
                }

                if (days == undefined || days < 0) {
                    $scope.NegotiationSettingsValidationMessage = 'Please Enter Valid Days';
                    return;
                }
                if (hours < 0) {
                    $scope.NegotiationSettingsValidationMessage = 'Please Enter Valid Hours';
                    return;
                }
                if (mins < 0) {
                    $scope.NegotiationSettingsValidationMessage = 'Please Enter Valid Minunts';
                    return;
                }
                if (mins >= 60 || mins == undefined) {
                    $scope.NegotiationSettingsValidationMessage = 'Please Enter Valid Minunts';
                    return;
                }
                if (hours > 24 || hours == undefined) {
                    $scope.NegotiationSettingsValidationMessage = 'Please Enter Valid Hours';
                    return;
                }
                if (hours == 24 && mins > 0) {
                    $scope.NegotiationSettingsValidationMessage = 'Please Enter Valid Hours & Minuts';
                    return;
                }
                if (mins < 5 && hours == 0 && days == 0) {
                    $scope.NegotiationSettingsValidationMessage = 'Please Enter Min 5 Minutes';
                    return;
                }
            }

            $scope.listRequirementTaxes = [];
            $scope.reqTaxSNo = 1;

            $scope.listRequirementTaxeslength = $scope.listRequirementTaxes.length;

            $scope.requirementTaxes =
            {
                taxSNo: $scope.reqTaxSNo++,
                taxName: '',
                taxPercentage: 0
            }

            $scope.AddTax = function () {

                if ($scope.listRequirementTaxes.length > 4) {
                    return;
                }
                $scope.requirementTaxes =
                {
                    taxSNo: $scope.reqTaxSNo++,
                    taxName: '',
                    taxPercentage: 0
                };
                $scope.listRequirementTaxes.push($scope.requirementTaxes);
                $scope.listRequirementTaxeslength = $scope.listRequirementTaxes.length;
                $scope.getTotalPrice($scope.discountAmount, 0, $scope.totalprice);
            };

            $scope.deleteTax = function (SNo) {
                $scope.listRequirementTaxes = _.filter($scope.listRequirementTaxes, function (x) { return x.taxSNo !== SNo; });
                $scope.listRequirementTaxeslength = $scope.listRequirementTaxes.length;
                $scope.getTotalPrice($scope.discountAmount, 0, $scope.totalprice);
            };

            $scope.reports = {};

            $scope.isReportGenerated = 0;

            $scope.GetReportsRequirement = function () {
                auctionsService.GetReportsRequirement({ "reqid": $stateParams.Id, "sessionid": $scope.currentSessionId, 'userid': $scope.currentUserId })
                    .then(function (response) {
                        $scope.reports = response;
                        $scope.getData();
                        $scope.isReportGenerated = 1;
                    });
            }

            $scope.generateReports = function () {
                $scope.isReportGenerated = 0;
            }

            $scope.updatePriceCap = function (auctionVendor) {
                let levelPrice = 1;
                $scope.formRequest.priceCapValueMsg = '';
                let l1Vendor = $scope.precisionRound(parseFloat($scope.auctionItem.auctionVendors[0].totalPriceIncl), $rootScope.companyRoundingDecimalSetting);

                if ($scope.auctionItem.auctionVendors[0].companyName == 'PRICE_CAP') {
                    levelPrice = 2;
                    l1Vendor = $scope.precisionRound(parseFloat($scope.auctionItem.auctionVendors[1].totalPriceIncl), $rootScope.companyRoundingDecimalSetting);
                }

                if ($scope.precisionRound(parseFloat($scope.formRequest.priceCapValue), $rootScope.companyRoundingDecimalSetting) > 0 && $scope.precisionRound(parseFloat($scope.formRequest.priceCapValue), $rootScope.companyRoundingDecimalSetting) < l1Vendor) {
                    let priceDetails = {
                        uID: auctionVendor.vendorID,
                        reqID: $scope.reqId,
                        price: $scope.formRequest.priceCapValue,
                        sessionID: $scope.currentSessionId
                    };

                    auctionsService.UpdatePriceCap(priceDetails)
                        .then(function (response) {
                            if (response.errorMessage == '') {
                                growlService.growl('Successfully updated price cap.', "success");
                                $scope.formRequest.priceCapValueMsg = '';
                                auctionVendor.totalPriceIncl = $scope.formRequest.priceCapValue;
                            }
                            else {
                                growlService.growl(response.errorMessage, "inverse");
                            }
                        });
                }
                else {
                    $scope.formRequest.priceCapValueMsg = 'Price cap amount should be less L' + levelPrice + ' price ' + l1Vendor;
                    // growlService.growl('Price cap amount should be more than 0  and less than L1 price ' + l1Vendor, "inverse");
                }
            };

            $scope.savepricecap = function () {

                //if ($scope.IS_CB_ENABLED == true) {
                //    $scope.auctionItem.reqType = 'REGULAR';
                //    $scope.auctionItem.priceCapValue = 0;
                //}

                $scope.l1Price = $scope.auctionItem.auctionVendors[0].totalPriceIncl;

                $scope.l1CompnyName = $scope.auctionItem.auctionVendors[0].companyName;

                if ($scope.l1CompnyName == 'PRICE_CAP') {
                    $scope.l1Price = $scope.auctionItem.auctionVendors[1].totalPriceIncl;
                }


                if (!$scope.IS_CB_ENABLED) {
                    $scope.NegotiationTimeValidation($scope.days, $scope.hours, $scope.mins, $scope.NegotiationSettings.minReductionAmount, $scope.NegotiationSettings.rankComparision);
                    if ($scope.NegotiationSettingsValidationMessage !== '') {
                        $scope.Loding = false;
                        return;
                    }

                    $scope.NegotiationSettings.negotiationDuration = $scope.days + ' ' + $scope.hours + ':' + $scope.mins + ':00.0';
                }

                if ($scope.auctionItem.reqType == 'PRICE_CAP' && ($scope.auctionItem.priceCapValue <= 0 || $scope.auctionItem.priceCapValue == undefined || $scope.auctionItem.priceCapValue == '')) {
                    swal("Error!", 'Please Enter Valid Price cap', "error");
                    return;
                }

                var params = {
                    'reqID': $scope.auctionItem.requirementID,
                    'sessionID': $scope.currentSessionId,
                    'userID': $scope.currentUserId,
                    'reqType': $scope.auctionItem.reqType,
                    'priceCapValue': $scope.auctionItem.priceCapValue,
                    'isUnitPriceBidding': $scope.auctionItem.isUnitPriceBidding,
                    //#CB-0-2018-12-05
                    'IS_CB_ENABLED': $scope.IS_CB_ENABLED,
                    'IS_CB_NO_REGRET': $scope.IS_CB_NO_REGRET
                };

                if ($scope.IS_CB_ENABLED == true) {

                    swal({
                        title: "Type of bidding cannot be changed once selected. The only way to change it is by creating a new requirement.",
                        text: "Kindly confirm the action.",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#F44336",
                        confirmButtonText: "OK",
                        closeOnConfirm: true
                    }, function () {
                        auctionsService.savepricecap(params).then(function (req) {
                            if (req.errorMessage == '') {
                                //code before the pause
                                setTimeout(function () {
                                    //do what you need here
                                    swal({
                                        title: "Done!",
                                        text: 'Requirement Type and Bidding Type Saved Successfully',
                                        type: "success",
                                        showCancelButton: false,
                                        confirmButtonColor: "#DD6B55",
                                        confirmButtonText: "Ok",
                                        closeOnConfirm: true
                                    },
                                        function () {
                                            if ($scope.auctionItem.status == 'STARTED') {
                                                $scope.recalculate('', 0, true);
                                            }
                                            location.reload();
                                        });
                                }, 1000);
                            } else {
                                swal("Error!", req.errorMessage, "error");
                            }
                        });
                    })

                } else {
                    auctionsService.savepricecap(params).then(function (req) {
                        if (req.errorMessage == '') {
                            swal({
                                title: "Done!",
                                text: 'Requirement Type and Bidding Type Saved Successfully',
                                type: "success",
                                showCancelButton: false,
                                confirmButtonColor: "#DD6B55",
                                confirmButtonText: "Ok",
                                closeOnConfirm: true
                            },
                                function () {
                                    if ($scope.auctionItem.status == 'STARTED') {
                                        $scope.recalculate('', 0, true);
                                    }
                                    location.reload();
                                });
                        } else {
                            swal("Error!", req.errorMessage, "error");
                        }
                    });
                }

            };

            $scope.goToReqTechSupport = function () {
                //$state.go("reqTechSupport", { "reqId": $scope.reqId }, { reload: true, newtab: true });

                var url = $state.href("reqTechSupport", { "reqId": $scope.reqId });
                window.open(url, '_blank');
            };

            $scope.goToMaterialReceived = function () {
                //$state.go("reqTechSupport", { "reqId": $scope.reqId }, { reload: true, newtab: true });

                var url = $state.href("Material-Received", { "Id": $scope.reqId });
                window.open(url, '_blank');
            };

            $scope.goToVendorPo = function (vendorID) {
                var url = $state.href("po-list", { "reqID": $scope.reqId, "vendorID": vendorID, "poID": 0 });
                window.open(url, '_blank');
            };

            $scope.goToDescPo = function () {
                var url = $state.href("desc-po", { "reqID": $scope.reqId });
                window.open(url, '_blank');
            };

            $scope.goToPreNegotiation = function (Id) {
                var url = $state.href("req-savingsPreNegotiation", { "Id": $scope.reqId });
                window.open(url, '_blank');
            };

            $scope.goToComparatives = function (reqID) {
                if ($scope.auctionItem.isDiscountQuotation != 2) {
                    var url = $state.href("comparatives", { "reqID": $scope.reqId });
                    window.open(url, '_blank');
                }
                else if ($scope.auctionItem.isDiscountQuotation == 2) {
                    var url = $state.href("marginComparatives", { "reqID": $scope.reqId });
                    window.open(url, '_blank');
                }
            };

            $scope.goToQCS = function (reqID, qcsID) {
                var url = $state.href("qcs", { "reqID": $scope.reqId, "qcsID": qcsID });
                window.open(url, '_self');
            };

            $scope.goToQCSList = function (reqID) {
                var url = $state.href("list-qcs", { "reqID": $scope.reqId });
                window.open(url, '_blank');
            };

            $scope.goToCostQCS = function (reqID) {
                var url = $state.href("cost-comparisions-qcs", { "reqID": $scope.reqId });
                window.open(url, '_blank');
            };

            $scope.goToRMQCS = function (reqID, itemID) {
                var url = $state.href("qcsRM", { "reqID": $scope.reqId, "itemID": itemID });
                window.open(url, '_blank');
            };

            $scope.isItemChanged = function (product) {
                var isSame = 0;
                var filterCustItem = _.filter($scope.auctionItem.listRequirementItems, function (custItem) {
                    return custItem.itemID === product.itemID;
                });

                if (filterCustItem.length > 0) {
                    if (filterCustItem[0].productIDorNameCustomer.toUpperCase() !== product.productIDorName.toUpperCase()
                        || filterCustItem[0].productNoCustomer.toUpperCase() !== product.productNo.toUpperCase()) {
                        isSame = 1;
                    }
                }

                if (filterCustItem.length <= 0) {
                    isSame = 2;
                }

                return isSame;
            };

            $scope.paymentRadio = false;

            $scope.paymentlist = [];

            $scope.addpaymentvalue = function () {
                var listpaymet =
                {
                    reqTermsID: 0,
                    reqID: $scope.stateParamsReqID,
                    userID: $scope.currentUserId,
                    reqTermsDays: 0,
                    reqTermsPercent: 0,
                    reqTermsType: 'PAYMENT',
                    paymentType: '+'
                };

                if ($scope.auctionItem.status == 'Negotiation Ended') {
                    listpaymet.isRevised = 1
                } else {
                    listpaymet.isRevised = 0
                }

                $scope.paymentlist.push(listpaymet);
            };

            $scope.delpaymentvalue = function (val, id) {

                if (id > 0) {
                    $scope.listTerms.push(id);
                }

                $scope.paymentlist.splice(val, 1);
            };

            $scope.resetpayment = function () {
                $scope.paymentlist = [];
            };

            $scope.deliveryRadio = false;

            $scope.deliveryList = [];

            $scope.adddeliveryvalue = function (val) {
                var deliveryObj =
                {
                    reqTermsID: 0,
                    reqID: $scope.stateParamsReqID,
                    userID: $scope.currentUserId,
                    reqTermsDays: 0,
                    reqTermsPercent: 0,
                    reqTermsType: 'DELIVERY',
                    refReqTermID: val
                };


                if ($scope.auctionItem.status == 'Negotiation Ended') {
                    deliveryObj.isRevised = 1
                } else {
                    deliveryObj.isRevised = 0
                }

                $scope.deliveryList.push(deliveryObj);
            };

            $scope.deldeliveryvalue = function (val, id) {

                if (id > 0) {
                    $scope.listTerms.push(id);
                }


                $scope.deliveryList.splice(val, 1);
            };

            $scope.resetdelivery = function () {
                $scope.deliveryList = [];
            };

            $scope.listRequirementTerms = [];

            $scope.SaveRequirementTerms = function (reqID) {

                $scope.listRequirementTerms = [];

                $scope.deliveryList.forEach(function (item, index) {
                    item.reqID = reqID;
                    $scope.listRequirementTerms.push(item);
                });

                $scope.paymentlist.forEach(function (item, index) {

                    item.reqID = reqID;

                    if (item.paymentType == '-') {
                        item.reqTermsDays = -(item.reqTermsDays);
                    }

                    $scope.listRequirementTerms.push(item);
                });

                var params = {
                    "listRequirementTerms": $scope.listRequirementTerms,
                    sessionID: $scope.currentSessionId
                };
                if ($scope.formRequest.isForwardBidding) {

                } else {
                    auctionsService.SaveRequirementTerms(params)
                        .then(function (response) {
                            if (response.errorMessage != '') {
                                growlService.growl(response.errorMessage, "inverse");
                            }
                            else {
                                //growlService.growl(".", "success");
                                //$scope.GetCompanyDepartments();
                                //$scope.addnewdeptView = false;
                                //$scope.department = {
                                //    userID: $scope.userID,
                                //    deptID: 0,
                                //    sessionID: $scope.sessionID
                                //};
                                ////$window.history.back();
                            }
                        });
                }


            };

            $scope.listTerms = [];

            $scope.DeleteRequirementTerms = function () {

                var params = {
                    "listTerms": $scope.listTerms,
                    sessionID: $scope.currentSessionId
                };

                if (!$scope.formRequest.isForwardBidding) {
                    auctionsService.DeleteRequirementTerms(params)
                        .then(function (response) {
                            if (response.errorMessage) {
                                growlService.growl(response.errorMessage, "inverse");
                            }
                        });
                }
            };

            $scope.mrpDiscountCalculation = function () {

                if ($scope.auctionItem.isDiscountQuotation == 1 || $scope.auctionItem.isDiscountQuotation == 2) {

                    $scope.auctionItemVendor.listRequirementItems.forEach(function (item, index) {
                        if ($scope.auctionItem.isDiscountQuotation == 2) {


                            if ($scope.auctionItem.biddingType === 'SPOT' ? $scope.auctionItem.isTabular && $scope.evaluateBiddingTypeBasedOnStatus() : $scope.auctionItem.isTabular && ($scope.auctionItem.status == 'UNCONFIRMED' || $scope.auctionItem.status == 'NOTSTARTED')) {
                                item.revCostPrice = item.costPrice;
                                item.revmarginAmount = item.marginAmount;
                            }

                            item = $scope.handlePrecision(item, 2);
                            item.cGst = item.Gst / 2;
                            item.sGst = item.Gst / 2;
                            item = $scope.handlePrecision(item, 2);
                            item.netPrice = 1 + (item.Gst / 100);
                            item = $scope.handlePrecision(item, 2);
                            item.netPrice = item.costPrice * item.netPrice;
                            item = $scope.handlePrecision(item, 2);
                            item.marginAmount = item.unitMRP - item.netPrice;

                            item.revnetPrice = 1 + (item.Gst / 100);
                            item = $scope.handlePrecision(item, 2);
                            item.revnetPrice = item.revCostPrice * item.revnetPrice;
                            item = $scope.handlePrecision(item, 2);
                            item.revmarginAmount = item.unitMRP - item.revnetPrice;


                            item = $scope.handlePrecision(item, 2);
                            //item.unitDiscount = item.marginAmount / item.costPrice;
                            item.unitDiscount = item.marginAmount / item.netPrice;
                            item.unitDiscount = item.unitDiscount * 100;
                            item = $scope.handlePrecision(item, 2);
                            item.revitemPrice = item.revCostPrice * (1 + item.Gst / 100);
                            item = $scope.handlePrecision(item, 2);
                            item.revmarginAmount = item.unitMRP - item.revitemPrice;
                            item = $scope.handlePrecision(item, 2);
                            //item.revUnitDiscount = item.revmarginAmount / item.revCostPrice;
                            item.revUnitDiscount = item.revmarginAmount / item.revnetPrice;

                            item.revUnitDiscount = item.revUnitDiscount * 100;
                            item = $scope.handlePrecision(item, 2);
                        }

                        if ($scope.auctionItem.auctionVendors[0].isQuotationRejected != 0) {
                            item.revUnitDiscount = item.unitDiscount;

                        }

                        if (item.unitDiscount > 100) {
                            swal("Error!", 'Please enter valid discount percentage ');
                            item.unitDiscount = item.TemperoryUnitDiscount;
                        }
                        if (item.revUnitDiscount > 100) {
                            swal("Error!", 'Please enter valid discount percentage ');
                            item.revUnitDiscount = item.TemperoryRevUnitDiscount;
                        }

                        if ($scope.auctionItem.biddingType === 'SPOT' ? $scope.auctionItem.isTabular && $scope.evaluateBiddingTypeBasedOnStatus() : $scope.auctionItem.isTabular && ($scope.auctionItem.status == 'UNCONFIRMED' || $scope.auctionItem.status == 'NOTSTARTED')) {
                            item.tempUitMRP = 0;
                            item.tempUnitDiscount = 0;

                            item.tempUitMRP = item.unitMRP;
                            item.tempUnitDiscount = item.unitDiscount;

                            if (item.unitMRP == undefined || item.unitMRP <= 0) {
                                item.unitMRP = 0;
                            }
                            if (item.unitDiscount == undefined || item.unitDiscount <= 0) {
                                item.unitDiscount = 0;
                            }

                            item.unitPrice = item.unitMRP - ((item.unitMRP / 100) * (item.unitDiscount));




                            item.unitMRP = item.tempUitMRP;
                            item.unitDiscount = item.tempUnitDiscount;

                        }

                        item.reductionPrice = (item.unitMRP - item.unitPrice) * item.productQuantity;
                        item.reductionPrice = $scope.precisionRound(parseFloat(item.reductionPrice), $rootScope.companyRoundingDecimalSetting);

                        if ($scope.auctionItem.isTabular && ($scope.auctionItem.status == 'Negotiation Ended' || $scope.auctionItem.status == 'STARTED' || $scope.auctionItem.status == 'UNCONFIRMED' || $scope.auctionItem.status == 'NOTSTARTED')) {

                            item.tempUitMRP = 0;
                            item.tempRevUnitDiscount = 0;

                            item.tempUitMRP = item.unitMRP;
                            item.tempRevUnitDiscount = item.revUnitDiscount;

                            if (item.unitMRP == undefined || item.unitMRP <= 0) {
                                item.unitMRP = 0;
                            }
                            if (item.revUnitDiscount == undefined || item.revUnitDiscount <= 0) {
                                item.revUnitDiscount = 0;
                            }

                            item.revUnitPrice = item.unitMRP - ((item.unitMRP / 100) * (item.revUnitDiscount));

                            item.revReductionPrice = (item.unitMRP - item.revUnitPrice) * item.productQuantity;

                            item.revReductionPrice = $scope.precisionRound(parseFloat(item.revReductionPrice), $rootScope.companyRoundingDecimalSetting);


                            item.unitMRP = item.tempUitMRP;
                            item.revUnitDiscount = item.tempRevUnitDiscount;

                        }

                    });
                }
            };

            $scope.precisionRound = function (number, precision) {
                var factor = Math.pow(10, precision);
                return Math.round(number * factor) / factor;
            };

            $scope.downloadTemplate = function () {

                var name = '';
                if ($scope.auctionItem.isDiscountQuotation == 2) {
                    if ($scope.auctionItem.isTabular && ($scope.auctionItem.status == 'UNCONFIRMED' || $scope.auctionItem.status == 'NOTSTARTED')) {
                        name = 'MARGIN_QUOTATION_' + $scope.reqId;
                    }
                    else {
                        name = 'MARGIN_REV_QUOTATION_' + $scope.reqId;
                    }
                } else {
                    name = 'UNIT_PRICE_QUOTATION_' + $scope.reqId;
                }


                reportingService.downloadTemplate(name, $scope.currentUserId, $scope.reqId);
            };

            $scope.getReport = function (name) {
                reportingService.downloadTemplate(name + "_" + $scope.reqId, $scope.currentUserId, $scope.reqId);
            };

            $scope.EnableMarginFields = function (val) {
                var params = {
                    isEnabled: val,
                    reqID: $scope.reqId,
                    sessionID: $scope.currentSessionId
                };
                auctionsService.EnableMarginFields(params)
                    .then(function (response) {
                        if (response.errorMessage == "") {
                            growlService.growl("Saved Successfully.", "success");
                        }
                        else {
                            growlService.growl(response.errorMessage, "inverse");
                        }
                    });
            };

            $scope.numToDecimal = function (val, decimalRound) {

                val = parseFloat(val);
                return val.toFixed(decimalRound);
            };

            $scope.GetReqDataPriceCap = function () {

                var params = {
                    'reqid': $scope.reqId,
                    'sessionid': $scope.currentSessionId
                };

                if ($scope.isCustomer && $scope.auctionItem.reqType && $scope.auctionItem.reqType === 'PRICE_CAP') {
                    priceCapServices.GetReqData(params)
                        .then(function (response) {
                            $scope.priceCapData = response;

                            if ($scope.priceCapData && $scope.priceCapData.listRequirementItems && $scope.priceCapData.listRequirementItems.length > 0) {
                                $scope.priceCapData.listRequirementItems.forEach(function (item, index) {
                                    item.Gst = item.cGst + item.sGst + item.iGst;
                                });
                            }

                            $scope.priceCapCalculations();
                        });
                }
            };

            $scope.SavePriceCapItemLevel = function () {
                var params = {
                    reqID: $scope.reqId,
                    listReqItems: $scope.priceCapData.listRequirementItems,
                    price: $scope.formRequest.priceCapValue,
                    isDiscountQuotation: $scope.auctionItem.isDiscountQuotation,
                    sessionID: $scope.currentSessionId
                };
                priceCapServices.SavePriceCap(params)
                    .then(function (response) {
                        if (response.errorMessage == "") {
                            growlService.growl("Saved Successfully.", "success");
                            if ($scope.auctionItem.status == 'STARTED') {
                                $scope.recalculate('', 0, true);
                            }
                            location.reload();
                        }
                        else {
                            growlService.growl(response.errorMessage, "inverse");
                        }
                    });
            }

            $scope.GetDifferentialFactorPrice = function (vendorID) {



                $scope.auctionItem.auctionVendors.forEach(function (vendor, index) {

                    if (vendorID == vendor.vendorID) {
                        vendor.changedDF = {
                            'background-color': 'yellow'
                        }
                    }
                    var QP = vendor.QP;
                    var BP = vendor.BP;
                    var RP = vendor.RP;
                    var DIFFERENTIAL_FACTOR = vendor.DIFFERENTIAL_FACTOR;
                    vendor.DF_REV_VENDOR_TOTAL_PRICE = parseFloat(vendor.revVendorTotalPrice) + parseFloat(DIFFERENTIAL_FACTOR);

                    if ($scope.auctionItem.status == 'Negotiation Ended' || $scope.auctionItem.status == 'STARTED') {
                        vendor.DF_REVISED_VENDOR_TOTAL_PRICE = parseFloat(vendor.revVendorTotalPrice) + parseFloat(DIFFERENTIAL_FACTOR);


                        vendor.QP = parseFloat(vendor.initialPrice) + parseFloat(vendor.DIFFERENTIAL_FACTOR);
                        vendor.BP = parseFloat(vendor.totalPriceIncl) + parseFloat(vendor.DIFFERENTIAL_FACTOR);
                        vendor.RP = parseFloat(vendor.revVendorTotalPrice) + parseFloat(vendor.DIFFERENTIAL_FACTOR);
                    }
                })

            };


            $scope.SaveDifferentialFactor = function (auctionVendor) {


                var params = {
                    'vendorID': auctionVendor.vendorID,
                    'reqID': $scope.auctionItem.requirementID,
                    'sessionID': $scope.currentSessionId,
                    'customerID': $scope.currentUserId,
                    'value': auctionVendor.DIFFERENTIAL_FACTOR,
                    'reason': ''
                };

                auctionsService.SaveDifferentialFactor(params).then(function (req) {
                    if (req.errorMessage == '') {
                        growlService.growl('Saved Successfully.', "success");

                        var parties = params.reqID + "$" + $scope.currentUserId + "$" + $scope.currentSessionId + "$" + "SAVE_DIFFERENTIAL_FACTOR";

                        $scope.invokeSignalR('CheckRequirement', parties);
                        //swal({
                        //    title: "Done!",
                        //    text: 'Saved Successfully',
                        //    type: "success",
                        //    showCancelButton: false,
                        //    confirmButtonColor: "#DD6B55",
                        //    confirmButtonText: "Ok",
                        //    closeOnConfirm: true
                        //},
                        //    function () {
                        //        location.reload();
                        //    });

                    } else {
                        swal("Error!", req.errorMessage, "error");
                    }
                });
            };

            $scope.priceCapCalculations = function () {
                if ($scope.auctionItem.isDiscountQuotation == 1 || $scope.auctionItem.isDiscountQuotation == 0) {
                    $scope.formRequest.priceCapValue = 0;
                    if ($scope.priceCapData && $scope.priceCapData.listRequirementItems) {
                        $scope.priceCapData.listRequirementItems.forEach(function (item, index) {

                            //if ($scope.priceCapData.auctionVendors[0].isQuotationRejected != 0) {
                            //    item.revUnitDiscount = item.unitDiscount;
                            //}

                            item.cGst = item.Gst / 2;
                            item.sGst = item.Gst / 2;

                            if ($scope.auctionItem.isTabular && ($scope.auctionItem.status == 'UNCONFIRMED' || $scope.auctionItem.status == 'NOTSTARTED')) {
                                if ($scope.auctionItem.isDiscountQuotation == 1) {
                                    item.revUnitDiscount = item.unitDiscount;
                                    item.tempUitMRP = 0;
                                    item.tempUnitDiscount = 0;

                                    item.tempUitMRP = item.unitMRP;
                                    item.tempUnitDiscount = item.unitDiscount;

                                    if (item.unitMRP == undefined || item.unitMRP <= 0) {
                                        item.unitMRP = 0;
                                    }
                                    if (item.unitDiscount == undefined || item.unitDiscount <= 0) {
                                        item.unitDiscount = 0;
                                    }

                                    item.unitPrice = item.unitMRP - ((item.unitMRP / 100) * (item.unitDiscount));

                                    item.unitMRP = item.tempUitMRP;
                                    item.unitDiscount = item.tempUnitDiscount;


                                    if ($scope.auctionItem.isDiscountQuotation == 1) {
                                        item.reductionPrice = (item.unitMRP - item.unitPrice) * item.productQuantity;
                                        item.reductionPrice = $scope.precisionRound(parseFloat(item.reductionPrice), $rootScope.companyRoundingDecimalSetting);
                                    }

                                }


                                ////////////////////
                                item.sGst = item.cGst;
                                var tempUnitPrice = item.unitPrice;
                                var tempCGst = item.cGst;
                                var tempSGst = item.sGst;
                                var tempIGst = item.iGst;
                                if (item.unitPrice == undefined || item.unitPrice <= 0) {
                                    item.unitPrice = 0;
                                };

                                if ($scope.auctionItem.isDiscountQuotation == 0) {
                                    item.revUnitPrice = item.unitPrice;
                                }

                                item.itemPrice = item.unitPrice * item.productQuantity;


                                item.itemPrice = item.itemPrice + ((item.itemPrice / 100) * (item.cGst + item.sGst + item.iGst));

                                item.unitPrice = tempUnitPrice;
                                item.cGst = tempCGst
                                item.sGst = tempSGst;
                                item.iGst = tempIGst;

                                if (item.cGst < 0 || item.sGst < 0 || item.iGst < 0 || item.cGst == undefined || item.sGst == undefined || item.iGst == undefined || item.cGst > 100 || item.sGst > 100 || item.iGst > 100) {
                                    // $scope.gstValidation = true;
                                    // not mandatory
                                };
                                /////////////////////

                            }

                            if ($scope.auctionItem.isTabular && ($scope.auctionItem.status == 'Negotiation Ended' || $scope.auctionItem.status == 'STARTED' || $scope.auctionItem.status == 'UNCONFIRMED' || $scope.auctionItem.status == 'NOTSTARTED')) {

                                if ($scope.auctionItem.isDiscountQuotation == 1) {
                                    item.tempUitMRP = 0;
                                    item.tempRevUnitDiscount = 0;

                                    item.tempUitMRP = item.unitMRP;
                                    item.tempRevUnitDiscount = item.revUnitDiscount;

                                    if (item.unitMRP == undefined || item.unitMRP <= 0) {
                                        item.unitMRP = 0;
                                    }
                                    if (item.revUnitDiscount == undefined || item.revUnitDiscount <= 0) {
                                        item.revUnitDiscount = 0;
                                    }

                                    item.revUnitPrice = item.unitMRP - ((item.unitMRP / 100) * (item.revUnitDiscount));

                                    item.revReductionPrice = (item.unitMRP - item.revUnitPrice) * item.productQuantity;

                                    item.revReductionPrice = $scope.precisionRound(parseFloat(item.revReductionPrice), $rootScope.companyRoundingDecimalSetting);


                                    item.unitMRP = item.tempUitMRP;
                                    item.revUnitDiscount = item.tempRevUnitDiscount;
                                }

                                ////////////////////////
                                var tempRevUnitPrice = item.revUnitPrice;
                                tempCGst = item.cGst;
                                tempSGst = item.sGst;
                                tempIGst = item.iGst;

                                if (item.revUnitPrice == undefined || item.revUnitPrice <= 0) {
                                    item.revUnitPrice = 0;
                                };

                                item.revitemPrice = item.revUnitPrice * item.productQuantity;

                                if (item.cGst == undefined || item.cGst <= 0) {
                                    item.cGst = 0;
                                };
                                if (item.sGst == undefined || item.sGst <= 0) {
                                    item.sGst = 0;
                                };
                                if (item.iGst == undefined || item.iGst <= 0) {
                                    item.iGst = 0;
                                };

                                item.revitemPrice = item.revitemPrice + ((item.revitemPrice / 100) * (item.cGst + item.sGst + item.iGst));

                                item.revUnitPrice = tempRevUnitPrice;
                                item.cGst = tempCGst
                                item.sGst = tempSGst;
                                item.iGst = tempIGst;

                                if (item.cGst < 0 || item.sGst < 0 || item.iGst < 0 || item.cGst == undefined || item.sGst == undefined || item.iGst == undefined || item.cGst > 100 || item.sGst > 100 || item.iGst > 100) {
                                    // $scope.gstValidation = true;
                                    // not mandatory
                                }
                                //////////////////////

                            }

                            if (item.revitemPrice > 0) {
                                $scope.formRequest.priceCapValue = parseFloat($scope.formRequest.priceCapValue);
                                $scope.formRequest.priceCapValue += item.revitemPrice;
                            }


                            //item.unitPrice = $scope.numToDecimal(item.unitPrice, $rootScope.companyRoundingDecimalSetting);
                            item.itemPrice = $scope.numToDecimal(item.itemPrice, $rootScope.companyRoundingDecimalSetting);

                            //item.revUnitPrice = $scope.numToDecimal(item.revUnitPrice, $rootScope.companyRoundingDecimalSetting);
                            item.revitemPrice = $scope.numToDecimal(item.revitemPrice, $rootScope.companyRoundingDecimalSetting);


                            $scope.formRequest.priceCapValue = $scope.numToDecimal($scope.formRequest.priceCapValue, $rootScope.companyRoundingDecimalSetting);
                        });
                    }
                }
            };

            $scope.removeAttach = function (index) {
                $scope.multipleAttachmentsList.splice(index, 1);
                $scope.auctionItem.auctionVendors[0].multipleAttachmentsList.splice(index, 1);
            };

            $scope.regretHandle = function (item, type) {
                if (item.isRegret) {
                    item.unitPrice = 0;
                    item.unitMRP = 0;
                    item.unitDiscount = 0;
                    item.Gst = 0;
                    item.sGst = 0;
                    item.cGst = 0;
                    item.iGst = 0;

                    item.isEdit = true;

                    if (item.productQuotationTemplateArray && item.productQuotationTemplateArray.length > 0) {
                        item.productQuotationTemplateArray.forEach(function (subItem, subItemIndex) {
                            if (subItem.HAS_PRICE) {
                                subItem.BULK_PRICE = 0;
                                subItem.TAX = 0;
                                subItem.UNIT_PRICE = 0;
                                subItem.REV_BULK_PRICE = 0;
                                subItem.REV_UNIT_PRICE = 0;
                            }
                        });
                    }
                } else {
                    item.regretComments = '';
                    if (type == 'REGRET') {
                        item.isEdit = false;
                    }
                }


                if ($scope.totalprice == 0) {

                    $("#totalprice").val(0);
                    $("#revtotalprice").val(0);
                    $("#vendorBidPrice").val(0);
                    $("#revvendorBidPrice").val(0);

                }
            };

            $scope.showVendorRegretDetails = function (vendor) {
                $scope.regretDetails = vendor.listRequirementItems;
            };

            //#CB-0-2018-12-05
            $scope.goToCbCustomer = function (vendorID) {
                var url = $state.href("cb-customer", { "reqID": $scope.reqId, "vendorID": vendorID });
                window.open(url, '_blank');
            };

            //$scope.UpdateCBTime = function () {

            //    var ts = moment($scope.formRequest.CB_END_TIME, "DD-MM-YYYY HH:mm").valueOf();
            //    var m = moment(ts);
            //    var date = new Date(m);
            //    var milliseconds = parseInt(date.getTime() / 1000.0);
            //    var cbEndTime = "/Date(" + milliseconds + "000+0530)/";

            //    var params = {
            //        cbEndTime: cbEndTime,
            //        userID: $scope.currentUserId,
            //        reqID: $scope.reqId,
            //        sessionID: $scope.currentSessionId
            //    }

            //    auctionsService.UpdateCBTime(params)
            //        .then(function (response) {
            //            if (response.errorMessage != "") {
            //                growlService.growl(response.errorMessage, "inverse");
            //            } else {
            //                $scope.recalculate();
            //                growlService.growl('Saved Successfully', "inverse");
            //            }
            //        });
            //};

            $scope.UpdateCBTime = function () {

                if ($scope.auctionItem.auctionVendors.length == 0 || !$scope.auctionItem.auctionVendors[0] || $scope.auctionItem.auctionVendors[0].quotationUrl == "" || $scope.auctionItem.auctionVendors[0].isQuotationRejected != 0 || $scope.starttimecondition1 != 0) {
                    $scope.Loding = false;
                    swal("Not Allowed", "You are not allowed to create a end time until at least one vendor Approved.", "error");
                    return;
                }


                var CB_END_TIME = $("#CB_END_TIME").val(); //$scope.startTime; //Need fix on this.

                if (CB_END_TIME && CB_END_TIME != null && CB_END_TIME != "") {

                    //var ts = moment($scope.startTime, "DD-MM-YYYY HH:mm").valueOf();
                    var ts = moment(CB_END_TIME, "DD-MM-YYYY HH:mm").valueOf();
                    var m = moment(ts);
                    var auctionStartDate = new Date(m);
                    auctionsService.getdate()
                        .then(function (getdateResponse) {
                            //var CurrentDate = moment(new Date(parseInt(getdateResponse.substr(6))));

                            var CurrentDateToLocal = userService.toLocalDate(getdateResponse);

                            var ts = moment(CurrentDateToLocal, "DD-MM-YYYY HH:mm").valueOf();
                            var m = moment(ts);
                            var deliveryDate = new Date(m);
                            var milliseconds = parseInt(deliveryDate.getTime() / 1000.0);
                            var CurrentDateToTicks = "/Date(" + milliseconds + "000+0530)/";

                            var CurrentDate = moment(new Date(parseInt(CurrentDateToTicks.substr(6))));

                            if (CurrentDate >= auctionStartDate) {
                                $scope.Loding = false;
                                swal("Done!", "End time should be greater than current time.", "error");
                                return;
                            }

                            swal({
                                title: "Are you sure?",
                                text: "Your negotiation process will be automatically closed at " + $scope.formRequest.CB_END_TIME + ".",
                                type: "warning",
                                showCancelButton: true,
                                confirmButtonColor: "#F44336",
                                confirmButtonText: "OK",
                                closeOnConfirm: true
                            }, function () {

                                var ts = moment($scope.formRequest.CB_END_TIME, "DD-MM-YYYY HH:mm").valueOf();
                                var m = moment(ts);
                                var date = new Date(m);
                                var milliseconds = parseInt(date.getTime() / 1000.0);
                                // // #INTERNATIONALIZATION-0-2019-02-12
                                var cbEndTime = "/Date(" + userService.toUTCTicks(CB_END_TIME) + "+0530)/";
                                //000

                                var params = {
                                    cbEndTime: cbEndTime,
                                    userID: $scope.currentUserId,
                                    reqID: $scope.reqId,
                                    sessionID: $scope.currentSessionId
                                };

                                auctionsService.UpdateCBTime(params)
                                    .then(function (response) {
                                        if (response.errorMessage != "") {
                                            growlService.growl(response.errorMessage, "inverse");
                                        } else {
                                            $scope.recalculate('UPDATE_CB_TIME_CUSTOMER', 0, false);
                                            growlService.growl('Saved Successfully', "inverse");
                                        }
                                    });
                            });
                        });
                }
                else {
                    swal("Done!", "Please enter the date and time to update End Time.", "error");
                    //alert("Please enter the date and time to update Start Time to.");
                }


            };

            $scope.CBStopQuotations = function (value) {

                var params = {
                    stopCBQuotations: value,
                    userID: $scope.currentUserId,
                    reqID: $scope.reqId,
                    sessionID: $scope.currentSessionId
                };

                swal({
                    title: "Are you sure?",
                    text: "You will not receive any further quotations.",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#F44336",
                    confirmButtonText: "OK",
                    closeOnConfirm: true
                }, function () {
                    auctionsService.CBStopQuotations(params)
                        .then(function (response) {
                            if (response.errorMessage != "") {
                                growlService.growl(response.errorMessage, "inverse");
                            } else {
                                $scope.recalculate('CB_STOP_QUOTATIONS_CUSTOMER', 0, false);
                                growlService.growl('Saved Successfully', "inverse");
                            }
                        });
                })
            };

            $scope.CBMarkAsComplete = function (value) {

                var params = {
                    isCBCompleted: value,
                    userID: $scope.currentUserId,
                    reqID: $scope.reqId,
                    sessionID: $scope.currentSessionId
                };

                /*    swal({
                        title: "Once requirement is marked as completed user will not have privileges to edit and negotiate with vendors.",
                        text: "Kindly confirm.",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#F44336",
                        confirmButtonText: "OK",
                        closeOnConfirm: true
                    }, function () {*/
                auctionsService.CBMarkAsComplete(params)
                    .then(function (response) {
                        if (response.errorMessage != "") {
                            growlService.growl(response.errorMessage, "inverse");
                        } else {
                            $scope.recalculate('CB_MARK_AS_COMPLETED_CUSTOMER', 0, false);
                            $scope.getrevisedquotations();
                            growlService.growl('Saved Successfully', "inverse");
                        }
                    });
                //  })
            };

            $scope.changeBiddingType = function (value) {
                if (value == '1' || value == '2') {
                    $scope.IS_CB_ENABLED = true;
                    if (value == '2') {
                        $scope.IS_CB_NO_REGRET = true;
                    } else {
                        $scope.IS_CB_NO_REGRET = false;
                    }
                }
                if (value == '0') {
                    $scope.IS_CB_ENABLED = false;
                }
            };

            $scope.shouldShow = function (permissionLevel) {
                // put your authorization logic here
                if (permissionLevel) {
                    return true;
                } else {
                    return false;
                }
            };


            $scope.markAsCompleteREQ = function (reqId, value, isMACForCBOrNot) {
                var params = {
                    isREQCompleted: value,
                    userID: $scope.currentUserId,
                    reqID: reqId,
                    sessionID: $scope.currentSessionId
                };

                swal({
                    title: "Once requirement is marked as completed user will not have privileges to edit this requirement.",
                    text: "Kindly confirm.",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#F44336",
                    confirmButtonText: "OK",
                    closeOnConfirm: true
                }, function () {
                    auctionsService.markAsCompleteREQ(params)
                        .then(function (response) {

                            //$scope.CBMarkAsComplete(1);

                            if (response.errorMessage != "") {
                                growlService.growl(response.errorMessage, "inverse");
                            } else {
                                //   $scope.recalculate('CB_MARK_AS_COMPLETED_CUSTOMER', 0, false);
                                //   $scope.getrevisedquotations();
                                growlService.growl('Saved Successfully', "success");
                                location.reload();
                                //auctionItem.IS_CB_ENABLED
                                if (isMACForCBOrNot == true) {
                                    $scope.CBMarkAsComplete(1);
                                }
                            }
                        });
                });
            };








            $scope.GetItemUnitPrice = function (isPerUnit, templateIndex) {
                //if (itemTemplate && itemTemplate.HAS_PRICE <= 0) {
                //    $scope.auctionItemVendor.listRequirementItems.forEach(function (item, itemIndexs) {
                //        if (item.productQuotationTemplateArray != null && item.productQuotationTemplateArray != undefined && item.productQuotationTemplateArray.length > 0) {
                //            item.productQuotationTemplateJson = JSON.stringify(item.productQuotationTemplateArray);
                //        } else {
                //            item.productQuotationTemplateJson = '';
                //        }
                //    });
                //    return;
                //}
                $scope.auctionItemVendor.listRequirementItems.forEach(function (item, itemIndexs) {
                    if (isPerUnit) {
                        if (item.productQuotationTemplateArray && item.productQuotationTemplateArray.length > 0) {
                            item.unitPrice = 0;
                            item.revUnitPrice = 0;
                            item.UNIT_PRICE = 0;
                            item.REV_UNIT_PRICE = 0;
                            item.margin15 = 0;
                            item.revmargin15 = 0;
                            item.margin3 = 0;
                            item.revmargin3 = 0;

                            item.productQuotationTemplateArray.forEach(function (subItem, subItemIndex) {
                                if (templateIndex == subItemIndex || templateIndex == -1) {
                                    if ($scope.auctionItem.auctionVendors[0].isQuotationRejected != 0) {
                                        subItem.REV_BULK_PRICE = subItem.BULK_PRICE;
                                        subItem.REV_CONSUMPTION = subItem.CONSUMPTION;
                                        subItem.REV_TAX = subItem.TAX;
                                        subItem.REV_UNIT_PRICE = subItem.UNIT_PRICE;
                                    }

                                    var BULK_PRICE = 0;
                                    if (parseFloat(subItem.BULK_PRICE) > 0) {
                                        BULK_PRICE = parseFloat(subItem.BULK_PRICE);
                                    }
                                    var REV_BULK_PRICE = 0;
                                    if (parseFloat(subItem.REV_BULK_PRICE) > 0) {
                                        REV_BULK_PRICE = parseFloat(subItem.REV_BULK_PRICE);
                                    }

                                    var CONSUMPTION = 0;
                                    if (parseFloat(subItem.CONSUMPTION) > 0) {
                                        CONSUMPTION = parseFloat(subItem.CONSUMPTION);
                                    }
                                    var REV_CONSUMPTION = 0;
                                    if (parseFloat(subItem.REV_CONSUMPTION) > 0) {
                                        REV_CONSUMPTION = parseFloat(subItem.REV_CONSUMPTION);
                                    }

                                    var TAX = 0;
                                    if (parseFloat(subItem.TAX) > 0) {
                                        TAX = parseFloat(subItem.TAX);
                                    }
                                    var REV_TAX = 0;
                                    if (parseFloat(subItem.REV_TAX) > 0) {
                                        REV_TAX = parseFloat(subItem.REV_TAX);
                                    }

                                    var UNIT_PRICE = 0;
                                    if (parseFloat(subItem.UNIT_PRICE) > 0) {
                                        UNIT_PRICE = parseFloat(subItem.UNIT_PRICE);
                                    }
                                    var REV_UNIT_PRICE = 0;
                                    if (parseFloat(subItem.REV_UNIT_PRICE) > 0) {
                                        REV_UNIT_PRICE = parseFloat(subItem.REV_UNIT_PRICE);
                                    }

                                    //UNIT_PRICE = BULK_PRICE * CONSUMPTION;
                                    //REV_UNIT_PRICE = REV_BULK_PRICE * REV_CONSUMPTION;
                                    if (CONSUMPTION > 0) {
                                        subItem.BULK_PRICE = UNIT_PRICE / CONSUMPTION;
                                    }
                                    else {
                                        subItem.BULK_PRICE = 0;
                                    }
                                    if (REV_CONSUMPTION > 0) {
                                        subItem.REV_BULK_PRICE = REV_UNIT_PRICE / REV_CONSUMPTION;
                                    }
                                    else {
                                        subItem.REV_BULK_PRICE = 0;
                                    }
                                    //subItem.UNIT_PRICE = UNIT_PRICE;
                                    //subItem.REV_UNIT_PRICE = REV_UNIT_PRICE;

                                    //if (subItem.IS_CALCULATED == 0) {
                                    //    //item.unitPrice += UNIT_PRICE;
                                    //    //item.revUnitPrice += REV_UNIT_PRICE;
                                    //    item.UNIT_PRICE += UNIT_PRICE;
                                    //    item.REV_UNIT_PRICE += REV_UNIT_PRICE;

                                    //}
                                }
                            });
                        }
                    }
                    else {
                        if (item.productQuotationTemplateArray && item.productQuotationTemplateArray.length > 0 && $scope.isUnitPriceEditableForSubItems(item.productQuotationTemplateArray)) {
                            item.unitPrice = 0;
                            item.revUnitPrice = 0;
                            item.UNIT_PRICE = 0;
                            item.REV_UNIT_PRICE = 0;
                            item.margin15 = 0;
                            item.revmargin15 = 0;
                            item.margin3 = 0;
                            item.revmargin3 = 0;

                            item.productQuotationTemplateArray.forEach(function (subItem, subItemIndex) {
                                if (templateIndex == subItemIndex || templateIndex == -1) {
                                    if ($scope.auctionItem.biddingType === 'SPOT' ? $scope.evaluateBiddingTypeBasedOnStatus() : $scope.auctionItem.auctionVendors[0].isQuotationRejected != 0) {
                                        subItem.REV_BULK_PRICE = subItem.BULK_PRICE;
                                        subItem.REV_CONSUMPTION = subItem.CONSUMPTION;
                                        subItem.REV_TAX = subItem.TAX;
                                        subItem.REV_UNIT_PRICE = subItem.UNIT_PRICE;
                                    }

                                    var BULK_PRICE = 0;
                                    if (parseFloat(subItem.BULK_PRICE) > 0) {
                                        BULK_PRICE = parseFloat(subItem.BULK_PRICE);
                                    }
                                    var REV_BULK_PRICE = 0;
                                    if (parseFloat(subItem.REV_BULK_PRICE) > 0) {
                                        REV_BULK_PRICE = parseFloat(subItem.REV_BULK_PRICE);
                                    }

                                    var CONSUMPTION = 0;
                                    if (parseFloat(subItem.CONSUMPTION) > 0) {
                                        CONSUMPTION = parseFloat(subItem.CONSUMPTION);
                                    }
                                    var REV_CONSUMPTION = 0;
                                    if (parseFloat(subItem.REV_CONSUMPTION) > 0) {
                                        REV_CONSUMPTION = parseFloat(subItem.REV_CONSUMPTION);
                                    }

                                    var TAX = 0;
                                    if (parseFloat(subItem.TAX) > 0) {
                                        TAX = parseFloat(subItem.TAX);
                                    }
                                    var REV_TAX = 0;
                                    if (parseFloat(subItem.REV_TAX) > 0) {
                                        REV_TAX = parseFloat(subItem.REV_TAX);
                                    }

                                    var UNIT_PRICE = 0;
                                    if (parseFloat(subItem.UNIT_PRICE) > 0) {
                                        UNIT_PRICE = parseFloat(subItem.UNIT_PRICE);
                                    }
                                    var REV_UNIT_PRICE = 0;
                                    if (parseFloat(subItem.REV_UNIT_PRICE) > 0) {
                                        REV_UNIT_PRICE = parseFloat(subItem.REV_UNIT_PRICE);
                                    }

                                    UNIT_PRICE = BULK_PRICE * CONSUMPTION + ((BULK_PRICE * CONSUMPTION) * (TAX / 100));
                                    REV_UNIT_PRICE = REV_BULK_PRICE * REV_CONSUMPTION + ((REV_BULK_PRICE * REV_CONSUMPTION) * (TAX / 100));

                                    subItem.UNIT_PRICE = UNIT_PRICE;
                                    subItem.REV_UNIT_PRICE = REV_UNIT_PRICE;

                                    //subItem.CEILING_PRICE = +subItem.CEILING_PRICE;
                                    //if (subItem.HAS_PRICE && subItem.CEILING_PRICE > 0) {
                                    //    item.ceilingPrice += parseFloat(subItem.CEILING_PRICE);
                                    //}
                                    //if (subItem.IS_CALCULATED == 0) {
                                    //    //item.unitPrice += UNIT_PRICE;
                                    //    //item.revUnitPrice += REV_UNIT_PRICE;
                                    //    item.UNIT_PRICE += UNIT_PRICE;
                                    //    item.REV_UNIT_PRICE += REV_UNIT_PRICE;
                                    //}
                                }
                            });
                        }
                    }
                });

                $scope.auctionItemVendor.listRequirementItems.forEach(function (item, itemIndexs) {
                    //item.ceilingPrice = 0;
                    if (item.productQuotationTemplateArray && item.productQuotationTemplateArray.length > 0) {
                        item.productQuotationTemplateArray.forEach(function (subItem, subItemIndex) {
                            if (subItem.IS_CALCULATED == 0) {
                                if (subItem.UNIT_PRICE > 0) {
                                    item.UNIT_PRICE += parseFloat(subItem.UNIT_PRICE);
                                }
                                if (subItem.REV_UNIT_PRICE > 0) {
                                    item.REV_UNIT_PRICE += parseFloat(subItem.REV_UNIT_PRICE);
                                }
                                if (subItem.HAS_PRICE && +subItem.CEILING_PRICE > 0) {
                                    subItem.CEILING_PRICE1 = +subItem.CEILING_PRICE;
                                    item.ceilingPrice = _.sumBy(item.productQuotationTemplateArray, 'CEILING_PRICE1');
                                }
                            }
                        });
                    }
                });

                $scope.auctionItemVendor.listRequirementItems.forEach(function (item, itemIndexs) {
                    if (item.productQuotationTemplateArray && item.productQuotationTemplateArray.length > 0) {
                        //item.margin15 = (item.UNIT_PRICE / 100) * 15;
                        //item.revmargin15 = (item.REV_UNIT_PRICE / 100) * 15;

                        //item.margin3 = (item.UNIT_PRICE / 100) * 3;
                        //item.revmargin3 = (item.REV_UNIT_PRICE / 100) * 3;
                    }
                });

                $scope.auctionItemVendor.listRequirementItems.forEach(function (item, itemIndexs) {
                    if (item.productQuotationTemplateArray && item.productQuotationTemplateArray.length > 0) {
                        item.productQuotationTemplateArray.forEach(function (subItem, subItemIndex) {
                            if (subItem.IS_CALCULATED == 1) {
                                subItem.UNIT_PRICE = item.UNIT_PRICE;
                                subItem.REV_UNIT_PRICE = item.REV_UNIT_PRICE;
                            }
                            if (subItem.IS_CALCULATED == 2) {
                                subItem.UNIT_PRICE = item.margin15;
                                subItem.REV_UNIT_PRICE = item.revmargin15;
                            }
                            if (subItem.IS_CALCULATED == 3) {
                                subItem.UNIT_PRICE = item.margin3;
                                subItem.REV_UNIT_PRICE = item.revmargin3;
                            }
                            if (subItem.IS_CALCULATED == 4) {
                                item.UNIT_PRICE = parseFloat(item.UNIT_PRICE) + parseFloat(subItem.UNIT_PRICE);
                                item.REV_UNIT_PRICE = parseFloat(item.REV_UNIT_PRICE) + parseFloat(subItem.REV_UNIT_PRICE);
                            }
                        });
                    }
                });

                $scope.auctionItemVendor.listRequirementItems.forEach(function (item, itemIndexs) {
                    if (item.productQuotationTemplateArray && item.productQuotationTemplateArray.length > 0 && $scope.isUnitPriceEditableForSubItems(item.productQuotationTemplateArray)) {
                        item.unitPrice = parseFloat(item.UNIT_PRICE) + parseFloat(item.margin15) + parseFloat(item.margin3);
                        item.revUnitPrice = parseFloat(item.REV_UNIT_PRICE) + parseFloat(item.revmargin15) + parseFloat(item.revmargin3);
                        item.productQuotationTemplateArray.forEach(function (subItem, subItemIndex) {
                            if (subItem.IS_CALCULATED == 5) {
                                subItem.UNIT_PRICE = item.unitPrice;
                                subItem.REV_UNIT_PRICE = item.revUnitPrice;
                            }
                        });
                    }
                });

                $scope.auctionItemVendor.listRequirementItems.forEach(function (item, itemIndexs) {
                    if (item.productQuotationTemplateArray && item.productQuotationTemplateArray.length > 0) {
                        item.productQuotationTemplateArray.forEach(function (subItem, subItemIndex) {
                            if (subItem.SPECIFICATION) {
                                subItem.SPECIFICATION = subItem.SPECIFICATION.replace(/\'/gi, "");
                                subItem.SPECIFICATION = subItem.SPECIFICATION.replace(/\"/gi, "");
                                subItem.SPECIFICATION = subItem.SPECIFICATION.replace(/[`^_|\?;:'",<>\{\}\[\]\\\/]/gi, "");
                                subItem.SPECIFICATION = subItem.SPECIFICATION.replace(/(\r\n|\n|\r)/gm, "");
                                subItem.SPECIFICATION = subItem.SPECIFICATION.replace(/\t/g, '');
                            }
                        });

                        item.productQuotationTemplateJson = JSON.stringify(item.productQuotationTemplateArray);

                    } else {
                        item.productQuotationTemplateJson = '';
                    }
                });

            };


            $scope.VendorItemlevelPrices = [];
            $scope.showVendorItemlevelPrices = function (vendor) {
                $scope.disableSubmitAllItemApproval = true;
                $scope.selectedVendorForItemLevelPrices = vendor;
                $scope.VendorItemlevelPrices = [];
                $scope.VendorItemlevelPrices = vendor.listRequirementItems;

                $scope.VendorItemlevelPrices.sort(function (a, b) {
                    return b.isCoreProductCategory - a.isCoreProductCategory;
                });

                $scope.VendorItemlevelPrices.map((element) => {
                    return element.otherProperties = vendor.otherProperties;
                });

                $scope.VendorItemlevelPrices.map((element1) => {
                    return element1.surrogateComments = vendor.surrogateComments;
                });

                console.log($scope.VendorItemlevelPrices);
            };



            $scope.createLot = function (requirementId) {

                swal({
                    title: "Once Lot Requirement is Created Vendors cannot submit Quotations for this Requirement.",
                    text: "Kindly confirm.",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#F44336",
                    confirmButtonText: "OK",
                    closeOnConfirm: true
                }, function () {
                    PRMLotReqService.lotdetails(0, requirementId)
                        .then(function (response) {
                            if (response) {
                                if (response) {
                                    $state.go("save-lot-details", { "Id": response.LotId, "reqId": requirementId });
                                }
                            }
                        });
                });


            };

            $scope.isValidLotBidding = function () {
                if ($scope.auctionItem.auctionVendors.length == 0 || $scope.auctionItem.auctionVendors[0].quotationUrl == "" || $scope.auctionItem.auctionVendors[1].quotationUrl == "" || $scope.auctionItem.auctionVendors[0].isQuotationRejected != 0 || $scope.auctionItem.auctionVendors[1].isQuotationRejected != 0 || $scope.starttimecondition1 != 0 || $scope.starttimecondition2 != 0) {
                    return false;
                }
                else {
                    return true;
                }
            };

            $scope.handleSubItemsVisibility = function (product) {
                product.subIemsColumnsVisibility = {
                    hideSPECIFICATION: true,
                    hideQUANTITY: true,
                    hideTAX: true,
                    hidePRICE: true
                };
                var productQuotationTemplateArray = product.productQuotationTemplateArray;
                if (productQuotationTemplateArray && productQuotationTemplateArray.length > 0) {
                    var items = productQuotationTemplateArray.filter(function (template) {
                        return (template.HAS_SPECIFICATION === 1);
                    });

                    if (items && items.length > 0) {
                        product.subIemsColumnsVisibility.hideSPECIFICATION = false;
                    }

                    items = productQuotationTemplateArray.filter(function (template) {
                        return (template.HAS_QUANTITY === 1);
                    });

                    if (items && items.length > 0) {
                        product.subIemsColumnsVisibility.hideQUANTITY = false;
                    }

                    items = productQuotationTemplateArray.filter(function (template) {
                        return (template.HAS_TAX === 1);
                    });

                    if (items && items.length > 0) {
                        product.subIemsColumnsVisibility.hideTAX = false;
                    }

                    items = productQuotationTemplateArray.filter(function (template) {
                        return (template.HAS_PRICE === 1);
                    });

                    if (items && items.length > 0) {
                        product.subIemsColumnsVisibility.hidePRICE = false;
                    }

                    //product.subIemsColumnsVisibility = _.clone($scope.subIemsColumnsVisibility);
                }
            };

            $scope.isUnitPriceEditableForSubItems = function (productQuotationTemplateArray) {
                var isDisabled = false;
                if (productQuotationTemplateArray && productQuotationTemplateArray.length > 0) {
                    var hasOnlySpecificationItems = productQuotationTemplateArray.filter(function (template) {
                        return (template.HAS_PRICE === 1);
                    });

                    if (hasOnlySpecificationItems && hasOnlySpecificationItems.length > 0) {
                        isDisabled = true;
                    }
                }

                return isDisabled;
            };

            $scope.isTaxFieldEditableForSubItems = function (productQuotationTemplateArray) {
                var isDisabled = false;
                if (productQuotationTemplateArray && productQuotationTemplateArray.length > 0) {
                    var hasOnlySpecificationItems = productQuotationTemplateArray.filter(function (template) {
                        return (template.HAS_TAX === 1);
                    });

                    if (hasOnlySpecificationItems && hasOnlySpecificationItems.length > 0) {
                        isDisabled = true;
                    }
                }

                return isDisabled;
            };

            $scope.getCustomFieldsByModuleId = function () {
                params = {
                    "compid": $scope.currentUserCompID,
                    "fieldmodule": 'REQUIREMENT',
                    "moduleid": $stateParams.Id,
                    "sessionid": $scope.currentSessionId
                };

                PRMCustomFieldService.GetCustomFieldsByModuleId(params)
                    .then(function (response) {
                        $scope.selectedcustomFieldList = response;
                    });
            };

            //$scope.getCustomFieldsByModuleId(); //Temporarly disabled

            $scope.IsAllChecked = false;
            $scope.isSaveEmailDisabled = true;
            // $scope.disableToOtherUsers = [];



            $scope.SendEmailLinksToVendors = function (emailToVendors) {

                $scope.vendors = [];

                emailToVendors.forEach(function (item, index) {
                    if (item.isEmailSent == true) {
                        if (item.WF_ID > 0) {
                            item.VendorObj =
                            {
                                vendorID: item.vendorID,
                                WF_ID: item.WF_ID
                            }
                            item = item.VendorObj;
                            $scope.vendors.push(item);
                        } else {
                            $scope.auctionItemTemporary.emailLinkVendors.forEach(function (mand, mandIndex) {
                                if (mand.vendorID == item.vendorID) {
                                    mand.showingWorkflowErrorMessage = 'Please Select Workflow';
                                }
                            })
                            return;
                        }
                    }

                });

                if ($scope.vendors.length <= 0) {
                    growlService.growl("Mandatory fields are required.", "inverse");
                    return;
                }


                var params =
                {
                    userID: $scope.currentUserId,
                    vendorIDs: $scope.vendors,
                    sessionID: $scope.currentSessionId
                };

                auctionsService.shareemaillinkstoVendors(params)
                    .then(function (response) {
                        if (response.errorMessage == "") {
                            angular.element('#showVendors').modal('hide');
                            growlService.growl("Email Has Been Send To The Selected Vendors ", "success");
                            $scope.auctionItemTemporary.emailLinkVendors.forEach(function (item, index) {
                                item.isEmailSent = false;
                                item.WF_ID = 0;
                                item.showingWorkflowErrorMessage = '';
                            });
                            $scope.IsAllChecked = false;

                            //   $scope.disableToOtherUsers = angular.copy($scope.vendors);
                        }
                    });

            };



            $scope.showErrorMessagesForVendors = function () {
                $scope.disableVendors = [];
                var vendorsString = '';
                vendorsString = $scope.existingVendors.join(',');

                var params =
                {
                    userID: $scope.currentUserId,
                    vendorIDs: vendorsString,
                    sessionID: $scope.currentSessionId
                };

                auctionsService.GetSharedLinkVendors(params)
                    .then(function (response) {
                        //$scope.auctionItemTemporary.emailLinkVendors
                        $scope.disableVendors = response;

                        $scope.auctionItemTemporary.emailLinkVendors.forEach(function (item, index) {
                            item.isDisable = false;
                            item.message = '';
                            item.errorMessage = '';
                            $scope.disableVendors.forEach(function (item1, index1) {
                                if (item.vendorID == item1.vendorID) {
                                    item.isDisable = true;
                                    item.message = 'Vendor has already received the registration link by';
                                    item.errorMessage = item1.errorMessage;
                                }
                            })
                        })
                    });

            }



            $scope.selectAll = function (value) {
                if (value) {
                    $scope.auctionItemTemporary.emailLinkVendors.forEach(function (item, index) {
                        item.isEmailSent = true;
                        $scope.disableVendors.forEach(function (item1, index1) {
                            if (item.vendorID == item1.vendorID) {
                                item.isEmailSent = false;
                            }
                            return;
                        })
                    });
                } else {
                    $scope.auctionItemTemporary.emailLinkVendors.forEach(function (item, index) {
                        item.isEmailSent = false;
                    });
                    //$scope.isSaveEmailDisabled = true;
                }

            };


            $scope.selectedVendor = function () {
                //console.log($scope.vendors.length);
            }


            /*region start WORKFLOW*/

            $scope.getWorkflows = function () {
                if ($scope.isCustomer && $scope.auctionItem.status === 'Negotiation Ended') {
                    workflowService.getWorkflowList()
                        .then(function (response) {
                            $scope.workflowList = [];
                            $scope.workflowListTemp = response;
                            $scope.workflowListTemp.forEach(function (item, index) {
                                if (item.WorkflowModule == $scope.WorkflowModule) {
                                    $scope.workflowList.push(item);

                                }
                            });

                            if ($scope.auctionItemTemporary) {
                                $scope.auctionItemTemporary.emailLinkVendors.forEach(function (item, index) {
                                    item.listWorkflows = $scope.workflowList;
                                });
                            }

                            if ($scope.isSuperUser) {
                                $scope.workflowList = $scope.workflowList;
                            }
                            else {
                                $scope.workflowList = $scope.workflowList.filter(function (item) {
                                    return item.deptID == userService.getSelectedUserDepartmentDesignation().deptID;

                                });
                            }
                        });
                }
            };


            /*region end WORKFLOW*/

            $scope.clearNonCoreItemsValues = function () {
                $scope.auctionItemVendor.listRequirementItems.forEach(function (item, index) {
                    if (!item.isCoreProductCategory) {
                        item.sGst = 0;
                        item.cGst = 0;
                        item.iGst = 0;
                        item.unitPrice = 0;
                    }
                });
            };

            $scope.fieldValidation = function () {
                if ($scope.auctionItem.auctionVendors.length > 0) {
                    $scope.auctionItem.auctionVendors.forEach(function (vendor, index) {
                        if (vendor.INCO_TERMS && !$scope.vendorIncoTerms[vendor.INCO_TERMS]) {
                            $scope.vendorIncoTerms[vendor.INCO_TERMS] = [];
                            auctionsService.getIncotermProductConfig(vendor.INCO_TERMS, $scope.currentSessionId)
                                .then(function (response) {
                                    $scope.vendorIncoTerms[vendor.INCO_TERMS] = response;
                                    $scope.auctionItem.auctionVendors.forEach(function (vendor1, index) {
                                        if (vendor1.INCO_TERMS === vendor.INCO_TERMS) {
                                            validateIncoTerms(response);
                                        }
                                    });
                                });
                        } else if (vendor.INCO_TERMS && $scope.vendorIncoTerms[vendor.INCO_TERMS]) {
                            validateIncoTerms($scope.vendorIncoTerms[vendor.INCO_TERMS]);
                        }
                    });
                }
            };

            function validateIncoTerms(incoTerms) {
                incoTerms.forEach(function (incoItem, itemIndexs) {
                    $scope.auctionItemVendor.listRequirementItems.forEach(function (item, index) {
                        item.isEdit = false;
                        if (item.isCoreProductCategory == 0 && item.catalogueItemID == incoItem.ProductId) {
                            item.isEdit = false;
                            //if ($scope.userIsOwner && incoItem.IS_CUSTOMER_EDIT) {
                            //    item.isEdit = false;
                            //} else if (!$scope.userIsOwner && incoItem.IS_VENDOR_EDIT && $scope.totalprice > 0) {
                            //    item.isEdit = false;
                            //} else {
                            //    item.isEdit = true;
                            //}
                        } else if (item.isCoreProductCategory) {
                            item.isEdit = false;
                        }

                    });
                });
            }


            $scope.getDFTotalPrice = function (qp, oc) {
                qp = parseFloat(qp);
                oc = parseFloat(oc);
                var result = parseFloat(qp + oc).toFixed(2);
                return result;
            };

            $scope.detectLinks = function urlify(text) {
                if (text) {
                    var urlRegex = /(https?:\/\/[^\s]+)/g;
                    return text.replace(urlRegex, function (url) {
                        return '<a target="_blank" href="' + url + '">' + url + '</a>';
                    });
                }

            };

            $scope.saveItemLevelCeilingPrice = function () {
                var updatedItems = [];
                $scope.auctionItem.listRequirementItems.forEach(function (item, itemIndexs) {
                    let itemCeilingPrice = item.ceilingPrice;
                    let productQuotationJSON = JSON.stringify(item.productQuotationTemplateArray);
                    if ($scope.itemCeilingVendor) {
                        var vendorItem = $scope.itemCeilingVendor.listRequirementItems.filter(function (vendItem) {
                            return vendItem.itemID === item.itemID;
                        });

                        if (vendorItem && vendorItem.length > 0 && vendorItem[0].ceilingPrice) {
                            itemCeilingPrice = vendorItem[0].ceilingPrice;
                            productQuotationJSON = JSON.stringify(vendorItem[0].productQuotationTemplateArray)
                        }
                    }

                    updatedItems.push({
                        ceilingPrice: itemCeilingPrice,//item.ceilingPrice,
                        itemID: item.itemID,
                        requirementID: item.requirementID,
                        productQuotationTemplateJson: productQuotationJSON
                    });
                });

                var params = {
                    user: $scope.currentUserId,
                    vendorid: ($scope.itemCeilingVendor ? $scope.itemCeilingVendor.vendorID : 0),
                    reqid: $scope.reqId,
                    requirementitemlist: updatedItems,
                    sessionid: $scope.currentSessionId
                };

                auctionsService.saveRequirementItemList(params)
                    .then(function (response) {
                        if (response.errorMessage == "") {
                            growlService.growl("Details have been saved successfully", "inverse");
                            location.reload();
                        }
                    });
            };

            $scope.getItemCeilingPrie = function (itemId) {
                let ceilingPrice = 0;
                var requirementItem = $scope.auctionItem.listRequirementItems.filter(function (reqItem) {
                    return reqItem.itemID === itemId;
                });

                if (requirementItem && requirementItem.length > 0) {
                    ceilingPrice = requirementItem[0].ceilingPrice;
                }

                return ceilingPrice;
            };

            $scope.validateCeilingPrice = function (product, type) {
                //let currentRevPrice = product.revUnitPriceOld;
                //let ceilingPrice = $scope.getItemCeilingPrie(product.itemID);
                //if (ceilingPrice && product.revUnitPrice) {
                //    if (+product.revUnitPrice > ceilingPrice) {
                //        swal("Error!", "Your item revised amount should be less than Item Ceiling amount " + ceilingPrice, 'error');
                //        product.revUnitPrice = currentRevPrice;
                //    }
                //}
                if (type === 'ITEM') {
                    if (product && product.ceilingPrice > 0 && +product.unitPrice > product.ceilingPrice) {
                        $timeout(function () {
                            swal("Error!", "Please enter Unit Price less than ceiling price given.", "error");
                        }, 0);
                        product.unitPrice = product.ceilingPrice;
                        $scope.unitPriceCalculation('PRC');
                        $scope.getTotalPrice(0, 0, 0);
                        return;
                    }
                } else if (type === 'SUB_ITEM') {
                    if (product && product.ceilingPrice > 0) {
                        if (product.productQuotationTemplateArray && product.productQuotationTemplateArray.length > 0) {
                            product.productQuotationTemplateArray.forEach(function (subItem, subItemIndex) {
                                if (subItem.HAS_PRICE && subItem.IS_VALID && subItem.CEILING_PRICE > 0) {
                                    if (+subItem.BULK_PRICE > subItem.CEILING_PRICE) {
                                        $timeout(function () {
                                            swal("Error!", "Please enter Unit Price less than ceiling price given.", "error");
                                        }, 0);
                                        subItem.BULK_PRICE = subItem.CEILING_PRICE;
                                        $scope.GetItemUnitPrice(false, subItemIndex);
                                        $scope.unitPriceCalculation('PRC');
                                        $scope.getTotalPrice(0, 0, 0);
                                        return;
                                    }
                                }
                            });
                        }
                    }
                }

            };

            $scope.hasOtherCharges = function () {
                let hasOtherCharges = false;
                let vendors = $scope.auctionItem.requirementVendorsList ? $scope.auctionItem.requirementVendorsList : $scope.auctionItem.auctionVendors;
                if ($scope.auctionItem && vendors && vendors.length > 0) {
                    let otherChargesVendor = vendors.filter(function (vendor) {
                        return vendor.DIFFERENTIAL_FACTOR > 0;
                    });

                    if (otherChargesVendor && otherChargesVendor.length > 0) {
                        hasOtherCharges = true;
                    }
                }

                return hasOtherCharges;
            };


            $scope.isValidCeilingAccept = function () {
                //auctionItem.auctionVendors[0].vendorCurrencyFactor
                let isValid = false;
                if ($scope.auctionItem) {
                    let vendors = $scope.auctionItem.requirementVendorsList ? $scope.auctionItem.requirementVendorsList : $scope.auctionItem.auctionVendors;
                    if (vendors && vendors.length > 0) {
                        let currentVendor = vendors.filter(function (vendor) {
                            return +vendor.vendorID === $scope.currentUserId;
                        });

                        if (currentVendor && currentVendor.length > 0 && currentVendor[0].listRequirementItems && currentVendor[0].listRequirementItems.length > 0) {
                            currentVendor[0].listRequirementItems.forEach(function (vendorItem, itemIndexs) {
                                var requirementItem = $scope.auctionItem.listRequirementItems.filter(function (reqItem) {
                                    return reqItem.itemID === vendorItem.itemID;
                                });

                                if (requirementItem && requirementItem.length > 0 && requirementItem[0].ceilingPrice && (+vendorItem.revUnitPrice) > (requirementItem[0].ceilingPrice / currentVendor[0].vendorCurrencyFactor).toFixed(2)) {
                                    isValid = true;
                                }
                            });
                        }
                    }
                }

                return isValid;
            };

            $scope.acceptCeilingPrices = function () {
                var currencyFactor = $scope.auctionItemVendor.auctionVendors[0].vendorCurrencyFactor;
                swal({
                    title: "Accept Ceiling Price ?",
                    text: "Please click to accept Ceiling Prices for all items in requirement.",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#F44336",
                    confirmButtonText: "OK",
                    closeOnConfirm: true
                }, function () {
                    $scope.auctionItemVendor.listRequirementItems.forEach(function (vendorItem, itemIndexs) {
                        var requirementItem = $scope.auctionItem.listRequirementItems.filter(function (reqItem) {
                            return reqItem.itemID === vendorItem.itemID;
                        });

                        if (requirementItem && requirementItem.length > 0 && requirementItem[0].ceilingPrice && +vendorItem.revUnitPrice > (requirementItem[0].ceilingPrice / currencyFactor)) {
                            vendorItem.revUnitPriceOld = vendorItem.revUnitPrice;
                            vendorItem.revUnitPrice = (requirementItem[0].ceilingPrice / currencyFactor);
                            if (vendorItem.productQuotationTemplateArray && vendorItem.productQuotationTemplateArray.length > 0) {
                                vendorItem.productQuotationTemplateArray.forEach(function (subItem, subItemIndex) {
                                    if (subItem.HAS_PRICE === 1 && subItem.IS_VALID === 1) {
                                        subItem.REV_BULK_PRICE = subItem.CEILING_PRICE;
                                        $scope.GetItemUnitPrice(false, subItemIndex);
                                    }
                                });
                            }
                            $scope.unitPriceCalculation('PRC');
                            //$scope.validateCeilingPrice(vendorItem);
                            $scope.makeBidUnitPriceValidation(vendorItem.revUnitPrice, vendorItem.TemperoryRevUnitPrice, vendorItem.productIDorName);
                            $scope.getRevTotalPrice(0, $scope.revtotalprice, $scope.revpackingChargesWithTax, $scope.revpackingCharges, $scope.revinstallationCharges);
                        }
                    });

                    $scope.makeaBid1();
                });
            };

            $scope.GetCompanyGSTInfo = function () {
                let params = {
                    companyId: $scope.currentUserCompID,
                    sessionid: $scope.sessionid
                };
                auctionsService.getCompanyGSTInfo(params)
                    .then(function (response) {
                        $scope.companyGSTInfo = response;

                        let currentVendor = $scope.auctionItem.auctionVendors[0];
                        if (currentVendor.quotationUrl == '' || currentVendor.quotationUrl == null || currentVendor.quotationUrl == undefined) {
                            $scope.getUserCredentials();
                        }
                    });
            };

            //if (!$scope.isCustomer) {
            //    $scope.GetCompanyGSTInfo();
            //}

            $scope.itemQuotationApproval = function (item, vendor) {
                var params = {
                    'vendorId': vendor.vendorID,
                    'reqId': $scope.auctionItem.requirementID,
                    'sessionId': $scope.currentSessionId,
                    'customerId': $scope.currentUserId,
                    'itemId': item.itemID,
                    'value': item.isItemQuotationRejected,
                    'comment': item.itemQuotationRejectedComment
                };

                auctionsService.ItemQuotationApproval(params).then(function (req) {
                    if (!req.errorMessage) {
                        toastr.info('Successfully saved.');

                    } else {
                        swal("Error!", req.errorMessage, "error");
                    }
                });
            };

            $scope.isItemFullyApproved = function (vendor, item, checkAuctionStatus) {
                let isApproved = false;
                item.quotationApprovedColor = {
                    'background-color': '#f5b2b2'
                };

                //The Below Code is because when the RFQ is selected as SPOT then vendor QUOTATION is Approved By default 
                //as this is done Unit price fields are getting disabled so to upload the QUOTE in NOTSTARTED state
                //we have to enable when the reduction setting type is OVERALL.
                if ($scope.auctionItem.biddingType === "SPOT") {
                    return $scope.enableWhenQuotationApproved();
                }

                if (vendor && vendor.isQuotationRejected === 0 && item.isItemQuotationRejected === 0) {
                    isApproved = true;
                    item.quotationApprovedColor = {};
                }

                if (checkAuctionStatus && !isApproved && $scope.auctionItem.status === 'STARTED') {
                    isApproved = true;
                    item.quotationApprovedColor = {};
                }

                //if (!item.isCoreProductCategory) {
                //    isApproved = true;
                //}

                return isApproved;
            };

            $scope.handleAllItemRejection = function (value) {
                $scope.disableSubmitAllItemApproval = false;
                if ($scope.VendorItemlevelPrices && $scope.VendorItemlevelPrices.length > 0) {
                    $scope.VendorItemlevelPrices.forEach(function (item, index) {
                        item.isItemQuotationRejected = value;
                        item.itemQuotationRejectedComment = $scope.formRequest.overallItemLevelComments;
                    });
                }
            };

            $scope.submitAllItemRejectionStatus = function (vendor) {
                if ($scope.VendorItemlevelPrices && $scope.VendorItemlevelPrices.length > 0) {
                    var params = {
                        'items': [],
                        'vendorId': vendor.vendorID,
                        'reqId': $scope.auctionItem.requirementID,
                        'customerId': $scope.currentUserId,
                        'sessionId': $scope.currentSessionId
                    };

                    $scope.VendorItemlevelPrices.forEach(function (currentItem, index) {
                        var item = {
                            itemID: currentItem.itemID,
                            isItemQuotationRejected: currentItem.isItemQuotationRejected,
                            itemQuotationRejectedComment: currentItem.itemQuotationRejectedComment
                        };

                        params.items.push(item);
                    });

                    auctionsService.ItemLevelQuotationApproval(params).then(function (req) {
                        if (!req.errorMessage) {
                            toastr.info('Successfully saved.');
                            location.reload();
                        } else {
                            swal("Error!", req.errorMessage, "error");
                        }
                    });
                }
            };

            $scope.anyItemQuotationsRejected = function () {
                let contains = false;
                if (!$scope.isCustomer && $scope.auctionItem && $scope.auctionItem.auctionVendors && $scope.auctionItem.auctionVendors.length > 0 &&
                    $scope.auctionItem.auctionVendors[0].listRequirementItems) {
                    let vendorQuotationItems = $scope.auctionItem.auctionVendors[0].listRequirementItems;
                    if (vendorQuotationItems && vendorQuotationItems.length > 0) {
                        var quotationRejetedItems = vendorQuotationItems.filter(function (reqItem) {
                            return reqItem.isItemQuotationRejected === 1;
                        });

                        if (quotationRejetedItems && quotationRejetedItems.length > 0) {
                            contains = true;
                        }
                    }
                }

                return contains;
            };

            $scope.itemQuotationsMessage = function () {
                let message = '';
                if (!$scope.isCustomer && $scope.auctionItem && $scope.auctionItem.auctionVendors && $scope.auctionItem.auctionVendors.length > 0 &&
                    $scope.auctionItem.auctionVendors[0].listRequirementItems) {
                    let vendorQuotationItems = $scope.auctionItem.auctionVendors[0].listRequirementItems;
                    if (vendorQuotationItems && vendorQuotationItems.length > 0) {
                        var quotationOtherItems = vendorQuotationItems.filter(function (reqItem) {
                            return reqItem.isItemQuotationRejected === -1 && reqItem.isCoreProductCategory;
                        });

                        if (quotationOtherItems && quotationOtherItems.length > 0) {
                            message = 'Your quotation partially reviewed. Please view below table for more details with status and comments.';
                        } else {
                            var quotationRejectedItems = vendorQuotationItems.filter(function (reqItem) {
                                return reqItem.isItemQuotationRejected === 1 && reqItem.isCoreProductCategory;
                            });

                            var quotationApprovedItems = vendorQuotationItems.filter(function (reqItem) {
                                return reqItem.isItemQuotationRejected === 0 && reqItem.isCoreProductCategory;
                            });

                            if (quotationRejectedItems && quotationApprovedItems && quotationRejectedItems.length > 0 && quotationApprovedItems.length > 0) {
                                message = 'Your quotation partially approved. Please view below table for more details with status and comments.';
                            }
                        }
                    }
                }

                return message;
            };

            $scope.getSubUserData = function () {
                if ($scope.isCustomer) {
                    userService.getCompanyUsers({ "compId": $scope.currentUserCompID, "sessionId": $scope.currentSessionId })
                        .then(function (response) {
                            response.forEach(function (cust, index) {

                            });

                            $scope.subUsers = $filter('filter')(response, { isValid: true });
                        });
                }
            };

            $scope.getSubUserData();


            function resetSurrogateObj() {
                $scope.surrogateObj = {
                    surrogates: [],
                    reqid: $scope.reqId,
                    comments: '',
                    user: $scope.currentUserId,
                    sessionid: $scope.currentSessionId
                };
            }

            resetSurrogateObj();
            $scope.searchSurrogate = function (value) {
                $scope.filteredSubUsers = [];
                if (value) {
                    $scope.filteredSubUsers = _.filter($scope.subUsers, function (item) {
                        return (item.email.toUpperCase().indexOf(value.toUpperCase()) > -1
                            || item.firstName.toUpperCase().indexOf(value.toUpperCase()) > -1
                            || item.lastName.toUpperCase().indexOf(value.toUpperCase()) > -1
                            || item.firstName.concat(" ").concat(item.lastName).toUpperCase().indexOf(value.toUpperCase()) > -1
                        );
                    });

                } else {
                    $scope.filteredSubUsers = $scope.subUsers;
                }
            };

            $scope.addSurrogateUser = function (customerObj) {
                customerObj.isSurrogateUser = !customerObj.isSurrogateUser;
                let filteredUers = _.filter($scope.surrogateUsers, function (user) {
                    return (+user.CUSTOMER_ID === +customerObj.userID);
                });

                if (filteredUers && filteredUers.length > 0) {
                    filteredUers[0].IS_VALID = (customerObj.isSurrogateUser ? 1 : 0);
                    filteredUers[0].comments = '';
                } else {
                    $scope.surrogateUsers.push({
                        VENDOR_ID: $scope.selectedSurrogateVendor.vendorID,
                        CUSTOMER_ID: customerObj.userID,
                        COMMENTS: $scope.surrogateObj.comments,
                        CUSTOMER_NAME: customerObj.firstName + ' ' + customerObj.lastName,
                        IS_VALID: 1
                    });
                }

                $scope.searchSurrogateString = '';
            };

            $scope.removeSurrogateUser = function (customerObj) {
                let filteredUers = _.filter($scope.surrogateUsers, function (user) {
                    return (user.CUSTOMER_ID === customerObj.CUSTOMER_ID);
                });

                if (filteredUers && filteredUers.length > 0) {
                    filteredUers[0].IS_VALID = 0;
                    filteredUers[0].comment = '';
                }

                var filteredSubUser = _.filter($scope.subUsers, function (subuser) {
                    return (+subuser.userID === +customerObj.CUSTOMER_ID);
                });

                if (filteredSubUser && filteredSubUser.length > 0) {
                    filteredSubUser[0].isSurrogateUser = false;
                }
            };

            $scope.getSurrogateUsers = function (vendor) {
                $scope.surrogateObj.comments = '';
                $scope.searchSurrogateString = '';

                $scope.surrogateUsers = [];
                if ($scope.subUsers && $scope.subUsers.length > 0) {
                    $scope.subUsers.forEach(function (user) {
                        user.isSurrogateUser = false;
                    });
                }

                resetSurrogateObj();
                $scope.surrogateObj.comments1 = '';
                $scope.selectedSurrogateVendor = vendor;
                let params = {
                    reqid: $scope.reqId,
                    vendorid: vendor.vendorID,
                    deptid: 0
                };

                PRMSurrogateBidService.GetSurrogates(params)
                    .then(function (response) {
                        if (response && response.length > 0 && $scope.subUsers && $scope.subUsers.length > 0) {
                            $scope.surrogateUsers = response;
                            $scope.surrogateUsers.forEach(function (surrogateuser) {
                                if (surrogateuser.COMMENTS) {
                                    $scope.surrogateObj.comments1 += surrogateuser.CUSTOMER_NAME + ': ' + surrogateuser.COMMENTS + '<br/>';
                                }

                                var filterResult = _.filter($scope.subUsers, function (subuser) {
                                    return (+subuser.userID === +surrogateuser.CUSTOMER_ID);
                                });

                                if (filterResult && filterResult.length > 0) {
                                    filterResult[0].isSurrogateUser = true;
                                }
                            });


                            $scope.searchSurrogate($scope.searchSurrogateString);
                        }
                    });
            };

            $scope.saveSurrogateUsers = function () {
                $scope.surrogateUsers.forEach(function (user, index) {
                    user.COMMENTS = user.COMMENTS ? user.COMMENTS : $scope.surrogateObj.comments;
                });
                $scope.surrogateObj.surrogates = $scope.surrogateUsers;
                PRMSurrogateBidService.SaveSurrogateUsers($scope.surrogateObj)
                    .then(function (response) {
                        if (response && !response.errorMessage) {
                            resetSurrogateObj();
                            //angular.element('#SurrogateUsersPopUp').modal('hide');
                            swal("Thanks !", "Successfully updated", "success");
                            $scope.getSurrogateUsers($scope.selectedSurrogateVendor);
                        } else {
                            swal("Error!", "Error saving data.", "error");
                        }
                    });
            };

            $scope.surrogateVendor = function (vendor) {

                //let surrogateURL = $state.href("surrogate-bid", { "Id": $scope.reqId, 'VendorId': vendor.vendorID, compId: $scope.currentUserCompID });
                //if ($scope.IS_CB_ENABLED) {
                //    surrogateURL = $state.href("surrogate-bid-cb", { "Id": $scope.reqId, 'VendorId': vendor.vendorID, compId: $scope.currentUserCompID });
                //}

                //window.open(surrogateURL, '_blank');

                let params = {
                    reqid: $scope.reqId,
                    vendorid: vendor.vendorID,
                    deptid: 0
                };

                PRMSurrogateBidService.GetSurrogates(params)
                    .then(function (response) {
                        if (response && response.length > 0) {
                            var filterResult = _.filter(response, function (surrogateUser) {
                                return (surrogateUser.CUSTOMER_ID === $scope.currentUserId);
                            });

                            if (filterResult && filterResult.length > 0) {
                                let surrogateURL = $state.href("surrogate-bid", { "Id": $scope.reqId, 'VendorId': vendor.vendorID, compId: $scope.currentUserCompID });
                                if ($scope.IS_CB_ENABLED) {
                                    surrogateURL = $state.href("surrogate-bid-cb", { "Id": $scope.reqId, 'VendorId': vendor.vendorID, compId: $scope.currentUserCompID });
                                }

                                window.open(surrogateURL, '_blank');
                            } else {
                                swal("Error!", "Unauthorized to Surrogate current Vendor.", "error");
                            }
                        } else {
                            swal("Error!", "No Surrogate users assigned.", "error");
                        }
                    });
            };

            $scope.searchTable = function (str) {
                if ($scope.isCustomer) {
                    $scope.listRequirementItemsTemp = $scope.auctionItem.listRequirementItems.filter(function (item) {
                        return item.productIDorName.toUpperCase().indexOf(str.toUpperCase()) >= 0;
                    });
                } else {
                    $scope.listRequirementItemsTemp = $scope.auctionItemVendor.listRequirementItems.filter(function (item) {
                        return item.productIDorName.toUpperCase().indexOf(str.toUpperCase()) >= 0;
                    });
                }

                $scope.totalItems = $scope.listRequirementItemsTemp.length;
            };

            $scope.arrayLength = function (arr, count) {
                return arr.slice(0, count);
            };

            $scope.getRequirementSettings = function () {
                $scope.requirementSettings = [];
                $scope.formRequest.rankLevel = 'BOTH';
                $scope.selectedTemplate.TEMPLATE_NAME = 'PRM_DEFAULT';
                auctionsService.getRequirementSettings({ "reqid": $scope.reqId, "sessionid": $scope.currentSessionId })
                    .then(function (response) {
                        $scope.requirementSettings = response;
                        if ($scope.requirementSettings && $scope.requirementSettings.length > 0) {
                            var rankLevel = $scope.requirementSettings.filter(function (setting) {
                                return setting.REQ_SETTING === 'RANK_LEVEL';
                            });

                            if (rankLevel && rankLevel.length > 0) {
                                $scope.formRequest.rankLevel = rankLevel[0].REQ_SETTING_VALUE;
                            }


                            var template = $scope.requirementSettings.filter(function (setting) {
                                return setting.REQ_SETTING === 'TEMPLATE_ID';
                            });

                            if (template && template.length > 0) {
                                $scope.selectedTemplate.TEMPLATE_ID = template[0].REQ_SETTING_VALUE;
                            }
                        }

                        if ($scope.selectedTemplate.TEMPLATE_ID || $scope.selectedTemplate.TEMPLATE_NAME) {
                            $scope.GetPRMTemplateFields();
                        }
                    });
            };

            $scope.getRequirementSettings();

            $scope.saveRankLevelSetting = function () {
                $scope.tempScopeVariables.RankTypeSaving = true;
                $scope.rankLevelChanged = false;
                let param = {
                    requirementSetting: {
                        REQ_ID: $scope.reqId,
                        REQ_SETTING: 'RANK_LEVEL',
                        REQ_SETTING_VALUE: $scope.formRequest.rankLevel ? $scope.formRequest.rankLevel : 'BOTH',
                        USER: $scope.currentUserId,
                        sessionID: $scope.currentSessionId
                    }
                };

                auctionsService.saveRequirementSetting(param)
                    .then(function (response) {
                        $scope.tempScopeVariables.RankTypeSaving = false;
                        if (response && !response.errorMessage) {
                            swal("Thanks !", "Successfully updated", "success");
                        } else {
                            swal("Error!", "Error saving data.", "error");
                        }
                    });
            };

            $scope.displayItemLevelRank = function () {
                let itemLevel = $scope.formRequest.rankLevel && ($scope.formRequest.rankLevel === 'BOTH' || $scope.formRequest.rankLevel === 'ITEM');
                if (!itemLevel && $scope.anyItemQuotationsRejected()) {
                    itemLevel = true;
                }

                return itemLevel;
            };

            $scope.displayRequirementLevelRank = function () {
                let display = true;
                if ($scope.formRequest.rankLevel && $scope.formRequest.rankLevel === 'ITEM') {
                    display = false;
                }

                return display;
            };

            $scope.conertToCounterBid = function () {
                let param = {
                    reqId: $scope.reqId,
                    isCounterBid: $scope.IS_CB_ENABLED ? 0 : 1,
                    user: $scope.currentUserId,
                    sessionId: $scope.currentSessionId
                };

                auctionsService.toggleCounterBid(param)
                    .then(function (response) {
                        if (response && !response.errorMessage) {
                            swal("Thanks !", "Successfully updated", "success");
                            location.reload();
                        } else {
                            swal("Error!", response.errorMessage, "error");
                        }
                    });
            };

            $scope.GetPRMTemplateFields = function () {
                $scope.prmFieldMappingDetails = {};
                var params = {
                    "templateid": $scope.selectedTemplate.TEMPLATE_ID ? $scope.selectedTemplate.TEMPLATE_ID : 0,
                    "templatename": $scope.selectedTemplate.TEMPLATE_NAME ? $scope.selectedTemplate.TEMPLATE_NAME : '',
                    "sessionid": userService.getUserToken()
                };

                PRMCustomFieldService.GetTemplateFields(params).then(function (mappingDetails) {
                    mappingDetails.forEach(function (item, index) {
                        $scope.prmFieldMappingDetails[item.FIELD_NAME] = item;
                    });
                });
            };

            $scope.isTaxAvailable = function (taxType) {
                var isVisible = true;

                if ($scope.auctionItem && $scope.auctionItem.auctionVendors && $scope.auctionItem.auctionVendors.length > 0 &&
                    $scope.auctionItem.auctionVendors[0].selectedVendorCurrency !== 'INR') {
                    return false;
                }

                if ($scope.gstNumber) {
                    if ($scope.auctionItem && $scope.auctionItem.auctionVendors && $scope.auctionItem.auctionVendors.length > 0 &&
                        $scope.auctionItem.auctionVendors[0].selectedVendorCurrency !== 'INR') {
                        isVisible = false;
                    } else if ($scope.auctionItem && $scope.auctionItem.auctionVendors && $scope.auctionItem.auctionVendors.length > 0 &&
                        $scope.auctionItem.auctionVendors[0].selectedVendorCurrency === 'INR') {
                        if (!$scope.disableLocalGSTFeature) {
                            if ($scope.gstNumber && $scope.gstNumber.startsWith($scope.localGSTNumberCode) && taxType && taxType === 'IGST') {
                                isVisible = false;
                            } else if ($scope.gstNumber && $scope.gstNumber.startsWith($scope.localGSTNumberCode) && taxType && (taxType === 'CGST' || taxType === 'SGST')) {
                                isVisible = true;
                            } else if (taxType && (taxType === 'CGST' || taxType === 'SGST')) {
                                isVisible = false;
                            }
                        } else {
                            return true;
                        }
                    }
                }

                return isVisible;
            };

            $scope.refreshRequirementCurrency = function () {
                auctionsService.refreshRequirementCurrency({ reqid: $scope.reqId, sessionid: $scope.currentSessionId })
                    .then(function () {
                        swal("Thanks !", "Successfully Refreshed", "success");
                        location.reload();
                    });
            };

            $scope.getCurrencyRateStatus = function () {
                $scope.showCurrencyRefresh = false;
                auctionsService.validateCurrencyRate({ reqid: $scope.reqId, sessionid: $scope.currentSessionId })
                    .then(function (currencyValidation) {
                        if (currencyValidation.objectID) {
                            $scope.showCurrencyRefresh = true;
                        }
                    });
            };

            $scope.vendorsQuotationStatus = function () {
                let pendingQuotationVendors = [];
                if ($scope.auctionItem && $scope.auctionItem.auctionVendors && $scope.auctionItem.auctionVendors.length > 0) {

                    $scope.auctionItem.auctionVendors.forEach(function (vendor, index) {
                        let pendingQuotationItems = vendor.listRequirementItems.filter(function (reqItem) {
                            return reqItem.isItemQuotationRejected === -1 && reqItem.isCoreProductCategory;
                        });

                        if (pendingQuotationItems && pendingQuotationItems.length > 0) {
                            pendingQuotationVendors.push(vendor.vendorName);
                        }
                    });
                }

                return pendingQuotationVendors.join();
            };

            $scope.hasAnyPartiallyApprovedVendors = function () {
                let anyPartiallyApprovedVendors = false;
                if ($scope.auctionItem && $scope.auctionItem.auctionVendors && $scope.auctionItem.auctionVendors.length > 0) {
                    $scope.auctionItem.auctionVendors.forEach(function (vendor, index) {
                        if (vendor.isQuotationRejected === 0 && !anyPartiallyApprovedVendors) {
                            let rejectedQuotationItems = vendor.listRequirementItems.filter(function (reqItem) {
                                return reqItem.isItemQuotationRejected === 1 && reqItem.isCoreProductCategory;
                            });
                            if (rejectedQuotationItems && rejectedQuotationItems.length > 0) {
                                anyPartiallyApprovedVendors = true;
                            }
                        }
                    });
                }

                if (anyPartiallyApprovedVendors) {
                    $scope.formRequest.rankLevel = 'ITEM';
                }

                return anyPartiallyApprovedVendors;
            };

            $scope.hasAnyItemsApprovedVendors = function (vendor) {
                let anyPartiallyApprovedVendors = false;
                if (vendor && vendor.listRequirementItems.length > 0) {
                    if (vendor.isQuotationRejected === 0 && !anyPartiallyApprovedVendors) {
                        let notApprovedQuotationItems = vendor.listRequirementItems.filter(function (reqItem) {
                            return reqItem.isItemQuotationRejected === -1 && reqItem.isCoreProductCategory;
                        });

                        if (notApprovedQuotationItems && notApprovedQuotationItems.length > 0) {
                            anyPartiallyApprovedVendors = true;
                        }
                    }
                }

                return anyPartiallyApprovedVendors;
            };

            $scope.currencyChangeEvent = function () {
                //listRequirementItemsTemp
                if ($scope.auctionItem.auctionVendors[0].selectedVendorCurrency && $scope.auctionItem.auctionVendors[0].selectedVendorCurrency !== 'INR') {
                    console.log($scope.auctionItem.auctionVendors[0].selectedVendorCurrency);
                    $scope.auctionItemVendor.listRequirementItems.forEach(function (item, itemIndexs) {
                        item.sGst = 0;
                        item.iGst = 0;
                        item.cGst = 0;
                        $scope.unitPriceCalculation('CGST');
                        $scope.unitPriceCalculation('SGST');
                        $scope.getTotalPrice($scope.discountAmount, $scope.freightCharges, $scope.totalprice,
                            $scope.packingCharges, $scope.packingChargesTaxPercentage, $scope.packingChargesWithTax,
                            $scope.installationCharges, $scope.installationChargesTaxPercentage, $scope.installationChargesWithTax,
                            $scope.freightChargesTaxPercentage, $scope.freightChargesWithTax);
                        $scope.getRevTotalPrice($scope.revfreightCharges, $scope.revtotalprice,
                            $scope.revpackingCharges, $scope.packingChargesTaxPercentage, $scope.revpackingChargesWithTax,
                            $scope.revinstallationCharges, $scope.installationChargesTaxPercentage, $scope.revinstallationChargesWithTax,
                            $scope.freightChargesTaxPercentage, $scope.revfreightChargesWithTax);
                    });
                }
            };

            $scope.gstSelect = function () {
                if ($scope.gstNumber) {
                    let isIGSTVisible = $scope.isTaxAvailable('IGST');
                    console.log(isIGSTVisible);
                    $scope.auctionItemVendor.listRequirementItems.forEach(function (vendorItem, itemIndexs) {
                        if (vendorItem.iGst) {
                            if (!isIGSTVisible) {
                                vendorItem.sGst = vendorItem.iGst / 2;
                                vendorItem.cGst = vendorItem.iGst / 2;
                                vendorItem.iGst = 0;
                            }
                        } else if (vendorItem.sGst && vendorItem.cGst) {
                            if (isIGSTVisible) {
                                vendorItem.iGst = (+vendorItem.sGst + +vendorItem.cGst);
                                vendorItem.sGst = 0;
                                vendorItem.cGst = 0;
                            }
                        }



                        $scope.unitPriceCalculation('CGST');
                        $scope.unitPriceCalculation('SGST');
                        $scope.unitPriceCalculation('IGST');
                        $scope.getTotalPrice($scope.discountAmount, $scope.freightCharges, $scope.totalprice,
                            $scope.packingCharges, $scope.packingChargesTaxPercentage, $scope.packingChargesWithTax,
                            $scope.installationCharges, $scope.installationChargesTaxPercentage, $scope.installationChargesWithTax,
                            $scope.freightChargesTaxPercentage, $scope.freightChargesWithTax);
                        $scope.getRevTotalPrice($scope.revfreightCharges, $scope.revtotalprice,
                            $scope.revpackingCharges, $scope.packingChargesTaxPercentage, $scope.revpackingChargesWithTax,
                            $scope.revinstallationCharges, $scope.installationChargesTaxPercentage, $scope.revinstallationChargesWithTax,
                            $scope.freightChargesTaxPercentage, $scope.revfreightChargesWithTax);
                    });
                }
            };

            $scope.changeItemPercentage = function (perc) {
                $scope.overAllValue.overallPercentage = perc;
                $scope.EnableMakeaBid();
                if (perc > 0 && perc < $scope.NegotiationSettings.minReductionAmount) {
                    swal("Error!", "Overall reduction should greater or equal to: " + $scope.NegotiationSettings.minReductionAmount + '%', "error");
                    return;
                }

                $scope.auctionItemVendor.listRequirementItems.forEach(function (item, itemIndexs) {
                    item.revUnitPrice = item.revUnitPriceOld ? item.revUnitPriceOld : item.revUnitPrice;
                    if (item.productQuotationTemplateArray && item.productQuotationTemplateArray.length > 0) {
                        item.productQuotationTemplateArray.forEach(function (subItem, subItemIndex) {
                            if (subItem.HAS_PRICE === 1 && subItem.IS_VALID === 1) {
                                subItem.REV_BULK_PRICE = subItem.REV_BULK_PRICE_OLD ? subItem.REV_BULK_PRICE_OLD : subItem.REV_BULK_PRICE;
                            }
                        });
                    }
                });

                $scope.auctionItemVendor.listRequirementItems.forEach(function (item, itemIndexs) {
                    if (item.revUnitPrice && perc) {
                        item.revUnitPriceOld = item.revUnitPrice;
                        item.revUnitPrice = item.revUnitPrice - (item.revUnitPrice * perc / 100);
                        if (item.productQuotationTemplateArray && item.productQuotationTemplateArray.length > 0) {
                            item.productQuotationTemplateArray.forEach(function (subItem, subItemIndex) {
                                if (subItem.HAS_PRICE === 1 && subItem.IS_VALID === 1) {
                                    subItem.REV_BULK_PRICE_OLD = subItem.REV_BULK_PRICE;
                                    subItem.REV_BULK_PRICE = +subItem.REV_BULK_PRICE - (+subItem.REV_BULK_PRICE * +perc / 100);
                                    //subItem.REV_UNIT_PRICE = subItem.REV_BULK_PRICE * subItem.REV_CONSUMPTION + ((subItem.REV_BULK_PRICE * subItem.REV_CONSUMPTION) * (subItem.TAX / 100));
                                    $scope.GetItemUnitPrice(false, subItemIndex);
                                }
                            });
                        }
                    }

                    $scope.unitPriceCalculation('PRC');
                    //$scope.validateCeilingPrice(item);
                    $scope.makeBidUnitPriceValidation(item.revUnitPrice, item.TemperoryRevUnitPrice, item.productIDorName);
                });
            };

            $scope.sendQuotationApprovalStatusEmail = function (vendor) {
                if ($scope.VendorItemlevelPrices && $scope.VendorItemlevelPrices.length > 0) {
                    var params = {
                        'items': [],
                        'vendorId': vendor.vendorID,
                        'reqId': $scope.auctionItem.requirementID,
                        'customerId': $scope.currentUserId,
                        'sessionId': $scope.currentSessionId
                    };

                    $scope.VendorItemlevelPrices.forEach(function (currentItem, index) {
                        if (currentItem.isItemQuotationRejected == false) {
                            currentItem.isItemQuotationRejected = 0;
                        } else if (currentItem.isItemQuotationRejected == true) {
                            currentItem.isItemQuotationRejected = 1;
                        }
                        var item = {
                            itemID: currentItem.itemID,
                            isItemQuotationRejected: currentItem.isItemQuotationRejected,
                            itemQuotationRejectedComment: currentItem.itemQuotationRejectedComment,
                            isCoreProductCategory: currentItem.isCoreProductCategory
                        };

                        params.items.push(item);
                    });

                    auctionsService.SendQuotationApprovalStatusEmail(params).then(function (req) {
                        if (!req.errorMessage) {
                            toastr.info('Successfully Sent.');
                        } else {
                            swal("Error!", req.errorMessage, "error");
                        }
                    });
                }
            };


            $scope.getReductionSettings = function () {
                $scope.reductionSettings = [];
                $scope.formRequest.reductionLevel = 'OVERALL';
                auctionsService.getReductionSettings({ "reqid": $scope.reqId, "sessionid": $scope.currentSessionId })
                    .then(function (response) {
                        $scope.reductionSettings = response;
                        if ($scope.reductionSettings && $scope.reductionSettings.length > 0) {
                            var reductionLevel = $scope.reductionSettings.filter(function (setting) {
                                return setting.REDUCTION_SETTING === 'REDUCTION_LEVEL';
                            });

                            if (reductionLevel && reductionLevel.length > 0) {
                                $scope.formRequest.reductionLevel = reductionLevel[0].REDUCTION_SETTING_VALUE;
                            }
                        }
                        $scope.displayOverAllReduction();
                    });
            };

            $scope.getReductionSettings();
            // $scope.displayOverAllReduction();

            $scope.saveReductionLevelSetting = function () {
                $scope.NegotiationSettingsValidationMessage = '';
                $scope.NegotiationTimeValidation($scope.days, $scope.hours, $scope.mins, $scope.NegotiationSettings.minReductionAmount, $scope.NegotiationSettings.rankComparision);
                if ($scope.NegotiationSettingsValidationMessage) {
                    $scope.Loding = false;
                    return;
                }

                if ($scope.formRequest.reductionLevel && $scope.formRequest.reductionLevel === 'OVERALL' &&
                    $scope.NegotiationSettings.minReductionAmount && +$scope.NegotiationSettings.minReductionAmount > 25) {
                    $scope.Loding = false;
                    swal("Warning!", "Overall reduction % cannot be more than 25%.", "error");
                    return;
                }

                var params = {};
                params.reqID = $scope.auctionItem.requirementID;
                params.sessionID = $scope.currentSessionId;
                $scope.NegotiationSettings.negotiationDuration = $scope.days + ':' + $scope.hours + ':' + $scope.mins + ':00.0';
                params.NegotiationSettings = $scope.NegotiationSettings;

                auctionsService.SaveReqNegSettings(params)
                    .then(function (response) {
                        if (response.SaveReqNegSettingsResult) {
                            if (response.SaveReqNegSettingsResult.errorMessage == '') {
                                swal("Done!", "Saved Successfully.", "success");
                            } else {
                                swal("Error", response.SaveReqNegSettingsResult.errorMessage, "inverse");
                            }
                        } else {
                            swal("Error", response.SaveReqNegSettingsResult.errorMessage, "inverse");
                        }

                    });

                $scope.tempScopeVariables.ReductionTypeSaving = true;
                $scope.reductionLevelChanged = false;
                let param = {
                    reductionSetting: {
                        REQ_ID: $scope.reqId,
                        REDUCTION_SETTING: 'REDUCTION_LEVEL',
                        REDUCTION_SETTING_VALUE: $scope.formRequest.reductionLevel ? $scope.formRequest.reductionLevel : 'OVERALL',
                        USER: $scope.currentUserId,
                        sessionID: $scope.currentSessionId
                    }
                };

                auctionsService.saveReductionLevelSetting(param)
                    .then(function (response) {
                        $scope.tempScopeVariables.ReductionTypeSaving = false;
                        if (response && !response.errorMessage) {
                            swal("Thanks !", "Successfully updated", "success");
                        } else {
                            swal("Error!", "Error saving data.", "error");
                        }
                    });
            };


            $scope.displayItemLevelReduction = function () {
                let reductionLevel = $scope.formRequest.reductionLevel && ($scope.formRequest.reductionLevel === 'ITEM');
                if (!reductionLevel) {
                    reductionLevel = true;
                }

                return reductionLevel;
            };

            $scope.displayOverAllReduction = function () {
                $scope.formRequest.display = true;
                if ($scope.formRequest.reductionLevel && $scope.formRequest.reductionLevel === 'ITEM') {
                    $scope.formRequest.display = false;
                }
                $scope.EnableMakeaBid();
                return $scope.formRequest.display;
            };

            $scope.revDiscountPrice = 0;
            $scope.updateRevisedUnitDiscount = function (perc) {
                $scope.auctionItemVendor.listRequirementItems.forEach(function (item, itemIndexs) {
                    if (item.revUnitDiscount > perc) {
                        swal("Error!", "Overall reduction should greater or equal to: " + item.revUnitDiscount, "error");
                        //item.revUnitDiscount = 0;
                        $scope.overAllValue.overallDiscountPercentage = item.revUnitDiscount;
                        return;
                    } else {
                        item.revUnitDiscount = perc;
                        $scope.revDiscountPrice = item.revUnitDiscount;
                        $scope.overAllValue.overallDiscountPercentage = perc;
                        $scope.mrpDiscountCalculation();
                        $scope.unitPriceCalculation();
                        $scope.getRevTotalPrice($scope.revfreightcharges, $scope.revtotalprice);
                        $scope.makeBidUnitPriceValidation(item.revUnitPrice, item.TemperoryRevUnitPrice, item.productIDorName);
                    }

                    $scope.EnableMakeaBid();
                });
            };

            $scope.EnableMakeaBid = function () {
                $scope.formRequest.MakeBiddisplay = false;
                if ($scope.formRequest.display == true && $scope.auctionItem.isDiscountQuotation == 0) {
                    if ($scope.overAllValue.overallPercentage < $scope.NegotiationSettings.minReductionAmount) {
                        $scope.formRequest.MakeBiddisplay = true;
                    }

                }
                if ($scope.formRequest.display == true && $scope.auctionItem.isDiscountQuotation == 1) {
                    //if ($scope.overAllValue.overallDiscountPercentage < $scope.NegotiationSettings.minReductionAmount) {
                    //    $scope.formRequest.MakeBiddisplay = true;
                    //}
                    $scope.formRequest.MakeBiddisplay = false;
                }

                return $scope.formRequest.MakeBiddisplay;
            };

            $scope.EnableMakeaBid();


            $scope.GetReqPRItems = function () {
                $scope.requirementPRStatus.MESSAGE = '';
                $scope.requirementPRStatus.showStatus = false;
                $scope.requirementPRStatus.validPRs = '';

                var params = {
                    "reqid": $scope.reqId,
                    "sessionid": userService.getUserToken()
                };

                PRMPRServices.GetPRItemsByReqId(params)
                    .then(function (response) {
                        console.log(response);
                        let inValidItems = _.filter(response, function (prItem) {
                            return prItem.IS_VALID <= 0;
                        });

                        let validPRs = _.filter(response, function (prItem) {
                            return prItem.IS_VALID > 0;
                        });

                        let validPRArray = [];
                        if (validPRs && validPRs.length > 0) {
                            validPRs.forEach(function (item, itemIndexs) {
                                if (!validPRArray.includes(item.PR_NUMBER)) {
                                    validPRArray.push(item.PR_NUMBER);
                                }
                            });

                            if (validPRArray && validPRArray.length > 0) {
                                $scope.requirementPRStatus.validPRs = validPRArray.join();
                            }
                        }

                        if (inValidItems && inValidItems.length > 0) {

                            let message = 'PR NUMBER - ITEM NUMBER - ITEM NAME<br/><br/>';
                            inValidItems.forEach(function (item, itemIndexs) {
                                message = message + item.PR_NUMBER + ' - ' + item.ITEM_NUM + ' - ' + item.ITEM_NAME + '.';
                            });

                            swal({
                                title: "It is observed that following PR-Items linked to this RFQ has been deleted.",
                                text: message + '<br/> <strong>Note</strong>: On clicking Continue. These PR-Items would be delinked automatically from the RFQ. You can continue with RFQ with these items as well however the PO would not be created for these deleted items',
                                type: "warning",
                                html: true,
                                showCancelButton: true,
                                confirmButtonColor: "#DD6B55",
                                confirmButtonText: "Continue",
                                cancelButtonText: "Cancel",
                                closeOnConfirm: true,
                                closeOnCancel: true
                            },
                                function (isConfirm) {
                                    if (isConfirm) {
                                        PRMPRServices.DeLinkInValidPRItems(params)
                                            .then(function (response) {
                                                growlService.growl("In-Valid PR Items are de-linked with Requirement.", "success");
                                            });
                                    } else {
                                        $state.go('home');
                                    }
                                });
                        }

                        let modiFiedItems = _.filter(response, function (prItem) {
                            return prItem.IS_MODIFIED > 0;
                        });

                        if (modiFiedItems && modiFiedItems.length > 0) {
                            $scope.requirementPRStatus.MESSAGE = 'Please validate the PRs added to the RFQ, ';
                            $scope.requirementPRStatus.showStatus = true;

                            modiFiedItems.forEach(function (item, itemIndexs) {
                                $scope.requirementPRStatus.MESSAGE = $scope.requirementPRStatus.MESSAGE + '(' + item.PR_NUMBER + ' - ' + item.ITEM_NUM + ' - ' + item.ITEM_NAME + ' modified on ' + userService.toLocalDate(item.MODIFIED_DATE) + '), ';
                            });

                            $scope.requirementPRStatus.MESSAGE = $scope.requirementPRStatus.MESSAGE + ' the requirement last updated time (' + userService.toLocalDate($scope.auctionItem.dateModified) + '). Please review the changes and do the needful.';
                        }
                    });
            };


            $scope.enableWhenQuotationApproved = function () {

                var showField = true;

                if ($scope.auctionItem.biddingType === 'SPOT') {
                    if ($scope.auctionItem.minPrice > 0) {
                        if ($scope.auctionItem.status === 'STARTED' || $scope.auctionItem.status === 'Negotiation Ended') {
                            showField = true;
                        } else {
                            showField = false;
                        }
                    } else {
                        showField = false;
                    }

                } else {
                    if (!$scope.auctionItem.auctionVendors[0].isQuotationRejected == 0) {
                        showField = false;
                    }
                }
                return showField;
            };

            // The Below Function is for Tax fields when the RFQ is SPOT (This will work When the Negotiation is STARTED and when vendor doesn't 
            //submit the Quotation then Unit price calculation function isn't working because of STATUS so if the RFQ is SPOT then to calculate the Prices with QTY this will work)
            $scope.evaluateBiddingTypeBasedOnStatus = function () {
                var enableTax = false;
                if ($scope.auctionItem.biddingType === 'SPOT') {
                    if ($scope.auctionItem.status !== 'STARTED' || ($scope.auctionItem.status === 'STARTED' && $scope.auctionItem.auctionVendors[0].initialPrice <= 0)) {
                        enableTax = true;
                    }
                } else {
                    return $scope.auctionItem.status !== 'STARTED';
                }
                return enableTax;
            };


            $scope.showMessageBasedOnBiddingType = function (defaultMessage, type) {

                if (type === 'QUOTATION_UPLOAD') {
                    defaultMessage = $scope.auctionItem.biddingType === 'SPOT' ? "Your first bid is already submitted, you can revise the bid only during the auction time" : defaultMessage;
                } else if (type === 'INITIAL_QUOTE') {
                    defaultMessage = $scope.auctionItem.biddingType === 'SPOT' ? "" : defaultMessage;
                } else if (type === 'NEG_STARTED') {
                    defaultMessage = $scope.auctionItem.biddingType === 'SPOT' ? "" : defaultMessage;
                }

                return defaultMessage;
            };


            $scope.enableBasedOnBiddingType = function () {
                var display = true;
                if ($scope.auctionItem.biddingType === 'SPOT') {
                    display = false;
                } else {
                    display = true;
                }
                return display;//$scope.auctionItem.biddingType === 'SPOT' ? display = false : display
            };

            $scope.enableBasedOnBiddingTypeForSubItems = function (type, isRevFields) {

                var isDisabled = true;

                if (type && type.IS_CALCULATED == 0) {
                    if (isRevFields === 'INITIAL' && ($scope.auctionItem.auctionVendors[0].isQuotationRejected == 0 && $scope.auctionItem.minPrice <= 0 || $scope.auctionItem.auctionVendors[0].isQuotationRejected == 1)) {
                        isDisabled = false;
                    } else if (isRevFields === 'REV' && ($scope.auctionItem.auctionVendors[0].isQuotationRejected == 0 && $scope.auctionItem.minPrice > 0 || $scope.auctionItem.auctionVendors[0].isQuotationRejected == 1)) {
                        isDisabled = false;
                    }
                }

                return isDisabled;
            };

            $scope.getSubItemCeilingPrice = function (itemId) {
                let subItemCeilingPrice = 0;
                var requirementItem = $scope.auctionItem.listRequirementItems.filter(function (reqItem) {
                    return reqItem.itemID === itemId;
                });

                if (requirementItem && requirementItem.length > 0) {
                    if (requirementItem[0].productQuotationTemplateArray && requirementItem[0].productQuotationTemplateArray.length > 0) {
                        requirementItem[0].productQuotationTemplateArray.forEach(function (subItem, subItemIndex) {
                            if (subItem.CEILING_PRICE && +subItem.CEILING_PRICE > 0) {
                                subItemCeilingPrice = subItem.CEILING_PRICE;
                            }
                        });
                    }
                }

                return subItemCeilingPrice;


            };


            $scope.UploadQuotationForSelectVendorCeilingPrices = function (vendorID) {

                $scope.itemCeilingVendor.listRequirementItems.forEach(function (item, itemIndexs) {
                    if (item.productQuotationTemplateArray && item.productQuotationTemplateArray.length > 0) {
                        item.productQuotationTemplateArray.forEach(function (subItem, subItemIndex) {
                            subItem.dateCreated = "/Date(1561000200000+0530)/";
                            subItem.dateModified = "/Date(1561000200000+0530)/";
                        });
                        item.productQuotationTemplateJson = JSON.stringify(item.productQuotationTemplateArray);
                    }
                    item.unitPrice = 0;
                });

                $scope.itemCeilingVendor.listRequirementItems = $scope.itemCeilingVendor.listRequirementItems = $scope.itemCeilingVendor.listRequirementItems.filter(function (item, index) {
                    return +item.ceilingPrice > 0 || (item.isCoreProductCategory === 0 && +item.ceilingPrice === 0);//PLEASE DONOT REMOVE THIS FILTER.  This Filter is because if the ceiling price is not given for any other items then also we are inserting that item into quotations so next time if we are saving the ceiling price for that item then it's not saving
                });

                var params = {
                    'quotationObject': $scope.itemCeilingVendor.listRequirementItems, // 1
                    'userID': vendorID,
                    'reqID': $scope.auctionItem.requirementID,
                    'sessionID': $scope.currentSessionId,
                    'price': 0, // 5
                    'tax': 0,
                    'vendorBidPrice': 0,
                    'warranty': '',
                    'duration': '',
                    'payment': '', // 10
                    'gstNumber': '',
                    'validity': '',
                    'otherProperties': '',
                    'quotationType': '',
                    'revised': 0, // 15
                    'discountAmount': 0,
                    'listRequirementTaxes': [],
                    'quotationAttachment': null,
                    'multipleAttachments': [],
                    'TaxFiledValidation': 0,
                    'selectedVendorCurrency': '', //35
                    'isVendAckChecked': false,
                    'quotFreezeTime': ''
                };

                auctionsService.UploadQuotationForSelectVendorCeilingPrices(params)
                    .then(function (response) {
                        if (response.errorMessage == "") {
                            growlService.growl("Details have been saved successfully", "inverse");
                            location.reload();
                        }
                    });
            };


            $scope.sumCeilingPriceForSubItems = function () {

                $scope.itemCeilingVendor.listRequirementItems.forEach(function (reqItem, reqIndex) {
                    if (reqItem.productQuotationTemplateArray && reqItem.productQuotationTemplateArray.length > 0) {
                        reqItem.productQuotationTemplateArray.forEach(function (subItem, subItemIndex) {
                            if (subItem.IS_CALCULATED == 0 && subItem.HAS_PRICE == 1) {
                                subItem.CEILING_PRICE1 = +subItem.CEILING_PRICE;
                            }
                        });
                        reqItem.ceilingPrice = _.sumBy(reqItem.productQuotationTemplateArray, 'CEILING_PRICE1');
                    }
                });

            };

            $scope.rankLevelChange = function () {
                $scope.rankLevelChanged = true;
            };

            $scope.reductionLevelChange = function () {
                $scope.reductionLevelChanged = true;
                $scope.getMinReductionAmount();
            };

            $scope.closeDeleteRequirementPopup = function () {
                angular.element('#deleteReqPopup').modal('hide');
            };


            function validateStringWithoutSpecialCharacters(string) {
                if (string) {
                    string = string.replace(/\'/gi, "");
                    string = string.replace(/\"/gi, "");
                    string = string.replace(/[`^_|\?;:'",<>\{\}\[\]\\\/]/gi, "");
                    string = string.replace(/(\r\n|\n|\r)/gm, "");
                    string = string.replace(/\t/g, '');
                    return string;
                }
            }


            $scope.SaveVendorConfirmation = function (auctionItem) {

                if ($scope.auctionItem.auctionVendors[0].isVendAckChecked) {
                    $('#termsAndConditionsModal').modal('hide');
                    //swal({
                    //    title: "Warning",
                    //    text: "You've acknowledged Successfully. Kindly click on Generate and Submit Button to Submit Quotation.",
                    //    type: "warning",
                    //    showCancelButton: false,
                    //    confirmButtonColor: "#F44336",
                    //    confirmButtonText: "OK",
                    //    closeOnConfirm: true
                    //});
                } else {

                }

            }

            $scope.RemoveVendorConformation = function (auctionItem) {
                if (!$scope.auctionItem.auctionVendors[0].isVendAckApprove) {
                    $scope.auctionItem.auctionVendors[0].isVendAckChecked = false;
                }
            };
            $scope.taxObj =
            {
                APPLY_TAX_TO_ALL: 0,
                APPLY_TAX_TO_ALL1: 0
            };

            $scope.updateGSTForAllItems = function (val, tax) {
                if (+tax >= 0) {
                    $scope.auctionItemVendor.listRequirementItems.forEach(function (product, index) {
                        if (val === 'SGST') {
                            product.cGst = +tax;
                            product.sGst = +tax;
                            product.iGst = 0;
                        }

                        if (val === 'IGST') {
                            product.iGst = +tax;
                            product.cGst = 0;
                            product.sGst = 0;
                        }
                        
                        $scope.unitPriceCalculation(val, product);
                        $scope.getTotalPrice($scope.discountAmount, $scope.freightCharges, $scope.totalprice,
                            $scope.packingCharges, $scope.packingChargesTaxPercentage, $scope.packingChargesWithTax,
                            $scope.installationCharges, $scope.installationChargesTaxPercentage, $scope.installationChargesWithTax,
                            $scope.freightChargesTaxPercentage, $scope.freightChargesWithTax);
                        $scope.getRevTotalPrice($scope.revfreightCharges, $scope.revtotalprice,
                            $scope.revpackingCharges, $scope.packingChargesTaxPercentage, $scope.revpackingChargesWithTax,
                            $scope.revinstallationCharges, $scope.installationChargesTaxPercentage, $scope.revinstallationChargesWithTax,
                            $scope.freightChargesTaxPercentage, $scope.revfreightChargesWithTax);
                    });
                }



               

                //if (val === 'SGST') {
                //    $scope.IGSTFields = true;
                //    $scope.CSGSTFields = false;
                //    $scope.taxObj.APPLY_TAX_TO_ALL1 = 0;
                   
                //}

                //if (val === 'IGST') {
                //    $scope.CSGSTFields = true;
                //    $scope.IGSTFields = false;
                //    $scope.taxObj.APPLY_TAX_TO_ALL = 0;
                //    $scope.auctionItemVendor.listRequirementItems.forEach(function (item, index) {
                //        if (item.isCoreProductCategory) {
                //            if (+tax > 0) {
                //                item.sGst = 0;
                //                item.cGst = 0;
                //                item.iGst = tax;
                //            }
                //        }
                //    });
                //}
            };

            $scope.isValidJson = function (str) {
                try {
                    JSON.parse(str);
                    return true;
                } catch (e) {
                    return false;
                }
            }

        }]);