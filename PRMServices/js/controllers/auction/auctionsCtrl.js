prmApp
    // =========================================================================
    // Auction Tiles
    // =========================================================================
    .controller('auctionsCtrl', ["$timeout", "$state", "$scope", "$log", "growlService", "userService", "auctionsService", "$http", "$rootScope",
        "SignalRFactory", "signalRHubName", "logisticServices", "$filter", "store", "poService", "workflowService", "PRMProjectServices", "PRMUploadServices", "fileReader",
        function ($timeout, $state, $scope, $log, growlService, userService, auctionsService, $http, $rootScope, $filter, SignalRFactory, signalRHubName, logisticServices, store, poService,
            workflowService, PRMProjectServices, PRMUploadServices, fileReader) {
            $scope.sessionid = userService.getUserToken();
            $scope.isVendor = (userService.getUserType() == "VENDOR") ? true : false;
            $scope.USER_ID = userService.getUserId();
            $scope.COMP_ID = userService.getUserCompanyId();
           
            $scope.name = $state.current.name;
            $scope.TYPE = 1;
            if ($scope.name === 'managementLevelDashboard')
            {
                $scope.TYPE = 0;
            }

            /*      Projects Region Start*/
            $scope.filtersList = {};
            $scope.filtersList.projects = [
                { name: 'Proj1' },
                { name: 'Proj2' },
                { name: 'Proj3' },
                { name: 'Proj4' },
                { name: 'Proj5' },
            ];

            $scope.type =
            {
                "LEVEL": 1
            };
            $scope.hideFilters = true;

            $scope.hideFilterSection = function (value) {
                if (value == 0) {
                    $scope.hideFilters = false;
                } else {
                    $scope.hideFilters = true;
                }
            };
            /*      Projects Region End*/

            $scope.filterDate = {
                toDate: '',
                fromDate: ''
            };

            $scope.D = 0;
            $scope.H = 0;
            $scope.M = 0;
            $scope.deptIDs = [];
            $scope.desigIDs = [];
            $scope.deptIDStr = '';
            $scope.desigIDStr = '';
            $scope.SelectedDeptId = 0;
            //#region ListUserDepartmentDesignations
            if (!$scope.isVendor) {
                $scope.ListUserDepartmentDesignations = userService.getListUserDepartmentDesignations();
                if (userService.getSelectedUserDepartmentDesignation()) {
                    $scope.SelectedUserDepartmentDesignation = userService.getSelectedUserDepartmentDesignation();
                    $scope.SelectedDeptId = $scope.SelectedUserDepartmentDesignation.deptID;
                }

                if ($scope.ListUserDepartmentDesignations && $scope.ListUserDepartmentDesignations.length > 0) {
                    $scope.ListUserDepartmentDesignations.forEach(function (item, index) {
                        $scope.deptIDs.push(item.deptID);
                        item.listDesignation.forEach(function (item1, index1) {
                            if (item1.isAssignedToUser && item1.isValid) {
                                $scope.desigIDs.push(item1.desigID);
                                $scope.desigIDs = _.union($scope.desigIDs, [item1.desigID]);
                            }
                        });
                    });
                    $scope.deptIDStr = $scope.deptIDs.join(',');
                    $scope.desigIDStr = $scope.desigIDs.join(',');
                }
            }

            $scope.changeUserDepartmentDesignation = function () {
                $scope.SelectedUserDepartmentDesignation = $scope.ListUserDepartmentDesignations.filter(function (udd) {
                    return (udd.deptID == $scope.SelectedDeptId);
                });

                $scope.SelectedUserDepartmentDesignation = $scope.SelectedUserDepartmentDesignation[0];
                store.set('SelectedUserDepartmentDesignation', $scope.SelectedUserDepartmentDesignation);

                location.reload();
            };
            //#endregion ListUserDepartmentDesignations


            $scope.filterDate.toDate = moment().format('YYYY-MM-DD');
            $scope.filterDate.fromDate = moment().subtract(30, "days").format("YYYY-MM-DD");

            $scope.filters =
            {
                projectCode: '',
                client: '',
                location: '',
                costCenter: ''
            };

            $scope.filtersList = {
                projectCode: [],
                client: [],
                location: []
            };

            $scope.GetDashboardFilters = function () {
                let projectCodesTemp = [];
                let clientTemp = [];
                let locationsTemp = [];
                var params = {
                    "COMP_ID": $scope.COMP_ID,
                    "USER_ID": $scope.USER_ID,
                    "FROM_DATE": $scope.filterDate.fromDate,
                    "TO_DATE": $scope.filterDate.toDate,
                    "DEPT_ID": $scope.SelectedDeptId > 0 ? $scope.SelectedDeptId : $scope.deptIDStr ,
                    "DESIG_ID": $scope.desigIDStr,
                    "sessionid": userService.getUserToken()
                };

                PRMProjectServices.getDashboardFilters(params)
                    .then(function (response) {
                        $scope.projectlistFilteredTemp = response;
                        $scope.projectlistFilteredTemp.forEach(function (item, index) {
                            if (item.TYPE === 'PROJ_CODE') {
                                projectCodesTemp.push({ type: item.TYPE, value: item.VALUE, id: item.ID });
                            } else if (item.TYPE === 'CLIENT') {
                                clientTemp.push({ type: item.TYPE, value: item.VALUE });
                            } else if (item.TYPE === 'LOCATION') {
                                locationsTemp.push({ type: item.TYPE, value: item.VALUE });
                            }
                        });

                        $scope.filtersList.projectCode = projectCodesTemp;
                        /*$scope.filtersList.client = clientTemp;*/
                        /*$scope.filtersList.location = locationsTemp;*/
                        $scope.filtersList.client = _.uniqBy(clientTemp, 'value');
                        $scope.filtersList.location = _.uniqBy(locationsTemp, 'value');
                        $scope.getType();
                    });
            };

            $scope.GetDashboardFilters();




            $scope.showRevenueRecognised = function (revenueRecognised,pendingPOAmount,revPercentage)
            {
                $scope.revenueRecongnised = {
                    chart: {
                        type: 'gauge',
                        plotBackgroundColor: null,
                        plotBackgroundImage: null,
                        plotBorderWidth: 0,
                        plotShadow: false,
                        width: 400,
                        height: 300
                    },

                    credits: {
                    enabled: false
                    },
                    title: {
                        text: 'Revenue Recognised by Finance'
                    },

                    pane: {
                        startAngle: -90,
                        endAngle: 89.9,
                        background: null,
                        center: ['50%', '75%'],
                        size: '110%'
                    },

                    // the value axis
                    yAxis: {
                        min: 0,
                        max: 200,
                        tickPixelInterval: 72,
                        tickPosition: 'inside',
                        tickColor: Highcharts.defaultOptions.chart.backgroundColor || '#FFFFFF',
                        tickLength: 20,
                        tickWidth: 2,
                        minorTickInterval: null,
                        labels: {
                            distance: 20,
                            style: {
                                fontSize: '13px'
                            }
                        },
                        plotBands: [{
                            from: 0,
                            to: 120,
                            color: '#55BF3B', // green
                            thickness: 20
                        }, {
                            from: 120,
                            to: 160,
                            color: '#DDDF0D', // yellow
                            thickness: 20
                        }, {
                            from: 160,
                            to: 200,
                            color: '#DF5353', // red
                            thickness: 20
                        }]
                    },

                    series: [{
                        name: 'Percentage',
                        data: [revPercentage],
                        tooltip: {
                            valueSuffix: ' %'
                        },
                        dataLabels: {
                            format: ' Revenue Recognised : ' + revenueRecognised + ' <br/><br/> PO Amount :' + pendingPOAmount,
                            borderWidth: 0,
                            color: (
                                Highcharts.defaultOptions.title &&
                                Highcharts.defaultOptions.title.style &&
                                Highcharts.defaultOptions.title.style.color
                            ) || '#333333',
                            style: {
                                fontSize: '16px'
                            }
                        },
                        dial: {
                            radius: '70%',
                            backgroundColor: 'gray',
                            baseWidth: 12,
                            baseLength: '0%',
                            rearLength: '0%'
                        },
                        pivot: {
                            backgroundColor: 'gray',
                            radius: 6
                        }

                    }],

                };

            };

            $scope.showBillGraph = function (billCertAmt, remainingAmount, billPoValue) {
                $scope.totalPOAmount = {
                    chart: {
                        type: 'bar',
                        width: 400,
                        height: 300
                    },
                    title: {
                        text: 'PO v/s Vendor Inv Certifed Amount'
                    },
                    subtitle: {
                        text: 'Total PO Amount (' + billPoValue + ')'
                    },
                    xAxis: {
                        categories: ['']
                    },
                    yAxis: {
                        min: 0,
                        title: {
                            text: ''
                        }
                    },
                    legend: {
                        reversed: true
                    },
                    plotOptions: {
                        series: {
                            stacking: 'normal'
                        }
                    },
                    series:
                        [
                            {
                                name: 'Remaining Amount',
                                data: [remainingAmount]
                            },
                            {
                                name: 'Vendor Certified Amount',
                                data: [billCertAmt]
                            }
                        ],
                    credits: {
                        enabled: false
                    }
                };


            };


            $scope.projectId = '';

            $scope.GetProjectDashboardStats = function ()
            {

                var params =
                {
                    "COMP_ID": $scope.COMP_ID,
                    "USER_ID": $scope.USER_ID,
                    "costCenter": $scope.filters.costCenter ? $scope.filters.costCenter :'',
                    "location": $scope.filters.location ? $scope.filters.location : '',
                    "clientName": $scope.filters.client ? $scope.filters.client : '',
                    "projectCode": $scope.filters.projectCode ? $scope.filters.projectCode: '',
                    "projectId": $scope.projectId ? $scope.projectId: '',
                    "fromDate": $scope.filterDate.fromDate,
                    "toDate": $scope.filterDate.toDate,
                    "sessionid": userService.getUserToken(),
                    "type": $scope.TYPE,
                    "DEPT_ID": $scope.SelectedDeptId > 0 ? $scope.SelectedDeptId :  $scope.deptIDStr,
                    "DESIG_ID": $scope.desigIDStr
                };


                PRMProjectServices.getProjectDashboardStats(params)
                    .then(function (response) {
                        $scope.projectStats = response;

                        $scope.D = parseInt($scope.projectStats.TURN_AROUND_TIME / (24 * 60));
                        $scope.H = parseInt(($scope.projectStats.TURN_AROUND_TIME % (24 * 60)) / 60);
                        $scope.M = parseInt(($scope.projectStats.TURN_AROUND_TIME % (24 * 60)) % 60);

                        $scope.purchaseVolumePerBuyer.series[0].data = [];
                        if ($scope.projectStats.PURCHASE_VOL_BY_PROCUREMENT && $scope.projectStats.PURCHASE_VOL_BY_PROCUREMENT.length > 0 && +$scope.type.LEVEL === 1) {
                            $scope.projectStats.PURCHASE_VOL_BY_PROCUREMENT.forEach(function (item, itemIndex) {
                                var obj =
                                {
                                    "name": item.BUYER_NAME,
                                    "y": $scope.precisionRound(parseFloat(item.PURCHASE_VOLUME), $rootScope.companyRoundingDecimalSetting),
                                    "drilldown": null
                                };
                                $scope.purchaseVolumePerBuyer.series[0].data.push(obj);
                            });
                            if (angular.element('#purchaseVolume').length >0) {
                                Highcharts.chart('purchaseVolume', $scope.purchaseVolumePerBuyer);
                            }
                            $scope.purchaseVolumePerBuyerChartShow = true;
                        }

                        if ($scope.projectStats.PACKAGES_VOLUMES && $scope.projectStats.PACKAGES_VOLUMES.length > 0 && $scope.projectId) {
                            $scope.projectStats.PACKAGES_VOLUMES.forEach(function (keyVal, index) {
                                $scope.ProjectTrgtAmtvsPOVal.xAxis.categories.push(keyVal.PACKAGE_NAME);
                                $scope.ProjectTrgtAmtvsPOVal.series[0].data.push(keyVal.PACKAGE_EST_BUDGET_AMOUNT);
                                $scope.ProjectTrgtAmtvsPOVal.series[1].data.push(keyVal.ANTICIPATED_PO_VALUE);
                            });
                            $scope.ProjectTrgtAmtvsPOValChartShow = true;
                        }

                        $scope.revenueStats.xAxis.categories = [];
                        $scope.revenueStats.series[0].data = [];

                        if ($scope.projectStats.REVENUE_STATS && $scope.projectStats.REVENUE_STATS.length > 0) {
                            $scope.projectStats.REVENUE_STATS.forEach(function (keyVal, index) {
                                $scope.revenueStats.xAxis.categories.push(keyVal.MONTH_YEAR);
                                $scope.revenueStats.series[0].data.push(keyVal.REVENUE_BOOKED);
                            });
                            if (angular.element('#revenue').length > 0) {
                                Highcharts.chart('revenue', $scope.revenueStats);
                            }
                            $scope.revenueChartShow = true;
                        } else {
                            $scope.revenueStats.xAxis.categories = [];
                            $scope.revenueStats.series[0].data = [];
                            if (angular.element('#revenue').length) {
                                Highcharts.chart('revenue', $scope.revenueStats);
                            }
                            $scope.revenueChartShow = false;
                        }
                        $scope.projectStats.BILL_REMAINING_AMOUNT = 0;
                        if ($scope.projectStats.BILL_PO_VALUE > 0 && $scope.projectId) {
                            $scope.projectStats.BILL_REMAINING_AMOUNT = ($scope.projectStats.BILL_PO_VALUE - $scope.projectStats.BILL_CERTIFIED_AMOUNT);
                            $scope.showBillGraph($scope.projectStats.BILL_CERTIFIED_AMOUNT, $scope.projectStats.BILL_REMAINING_AMOUNT, $scope.projectStats.BILL_PO_VALUE);

                            if (angular.element('#POAmount').length > 0) {
                                Highcharts.chart('POAmount', $scope.totalPOAmount);
                            }
                            $scope.totalPOAmountChartShow = true;
                        }
                        $scope.revenuePercentage = 0;
                        if ($scope.projectStats.REVENUE_RECOGNISED > 0 && $scope.projectStats.PENDING_PO_OVERALL_AMOUNT > 0 && $scope.projectId)
                        {
                            $scope.revenuePercentage = ($scope.projectStats.REVENUE_RECOGNISED / $scope.projectStats.PENDING_PO_OVERALL_AMOUNT) * 100
                            $scope.showRevenueRecognised($scope.projectStats.REVENUE_RECOGNISED, $scope.projectStats.PENDING_PO_OVERALL_AMOUNT, $scope.revenuePercentage);
                            if (angular.element('#revenueRec').length) {
                                Highcharts.chart('revenueRec', $scope.revenueRecongnised);
                                $scope.projectStats.REVENUE_RECOGNISED = 0;
                                $scope.projectStats.PENDING_PO_OVERALL_AMOUNT = 0;
                                $scope.projectStats.revenuePercentage = 0;
                            }
                            $scope.revenueRecognisedChartShow = true;
                        }
                        if ($scope.projectStats.REVENUE_DETAILS && $scope.projectStats.REVENUE_DETAILS.length > 0 && $scope.projectId) {
                            $scope.projectStats.REVENUE_DETAILS.forEach(function (keyVal, index) {
                                $scope.INVOICE_RAISED = keyVal.INVOICE_RAISED;
                                $scope.INVOICE_PAID = keyVal.INVOICE_PAID;
                                $scope.REC_OUTSTANDING = ($scope.INVOICE_RAISED - $scope.INVOICE_PAID);
                                $scope.showRecievableAmtGraph($scope.REC_OUTSTANDING, $scope.INVOICE_RAISED,$scope.INVOICE_PAID);
                            });
                            $scope.receivableOutstandingChartShow = true;
                        }
                        if ($scope.projectStats.PACKAGES_SPENT && $scope.projectStats.PACKAGES_SPENT.length > 0 && $scope.projectId) {

                            $scope.projectStats.PACKAGES_SPENT.forEach(function (keyVal, index) {

                                $scope.spentDetails.xAxis.categories.push(keyVal.PACKAGE_NAME.toString());
                                $scope.spentDetails.series[0].data.push(keyVal.PLANNED);
                                $scope.spentDetails.series[1].data.push(keyVal.SPENT);
                                //$scope.spentDetails.series[2].data.push((keyVal.PLANNED - keyVal.SPENT));
                            });

                            $scope.packageSpentChartShow = true;
                        }

                        /* && $scope.projectId*/

                        if ($scope.projectStats.MONTHLY_AVG_SAVINGS && $scope.projectStats.MONTHLY_AVG_SAVINGS.length > 0) {
                            $scope.monthlyDetails.xAxis.categories = [];
                            $scope.monthlyDetails.series[0].data = [];
                            $scope.monthlyDetails.series[1].data = [];
                            $scope.projectStats.MONTHLY_AVG_SAVINGS.forEach(function (keyVal, index) {
                                $scope.monthlyDetails.xAxis.categories.push(keyVal.MONTH.concat('-' +keyVal.YEAR)).toString();
                                $scope.monthlyDetails.series[0].data.push(keyVal.QUOTATION_VALUE);
                                $scope.monthlyDetails.series[1].data.push(keyVal.SAVINGS);
                            });
       
                            if (angular.element('#savings').length > 0) {
                                Highcharts.chart('savings', $scope.monthlyDetails);
                            }
                            $scope.monthlyDetailsChartShow = true;
                        } else {
                            $scope.monthlyDetails.xAxis.categories = [];
                            $scope.monthlyDetails.series[0].data = [];
                            $scope.monthlyDetails.series[1].data = [];
                            if (angular.element('#savings').length) {
                                Highcharts.chart('savings', $scope.monthlyDetails);
                            }
                            $scope.monthlyDetailsChartShow = false;

                        }

                        /*  ###########NEED TO CHECK*/



                        if ($scope.projectStats.chartSeries && $scope.projectStats.chartSeries.length > 0) {
                            $scope.requirementDetails.xAxis.categories = [];
                            $scope.requirementDetails.series[0].data = [];
                            $scope.requirementDetails.series[1].data = [];
                            if ($scope.projectStats.chartSeries.length == 1) {
                                $scope.projectStats.chartSeries.push({ name:'Requirement Posted', 'data':[]});
                            }
                            $scope.projectStats.chartSeries[1].data = _.zipWith($scope.projectStats.chartSeries[0].data, $scope.projectStats.chartSeries[1].data , function (a, b) {
                                return a + b;
                            });

                            $scope.projectStats.chartSeries.forEach(function (obj, index) {
                                $scope.requirementDetails.series[index].name = obj.name;
                                $scope.requirementDetails.series[index].data = obj.data;
                            });

                            if ($scope.projectStats && $scope.projectStats.months.length > 0) {
                                $scope.projectStats.months.forEach(function (data) {
                                    $scope.requirementDetails.xAxis.categories.push(data);
                                });
                            }

                            if (angular.element('#requirement').length > 0) {
                                Highcharts.chart('requirement', $scope.requirementDetails);
                            }
                            $scope.requirementDetailsChartShow = true;
                        } else {
                            $scope.requirementDetails.xAxis.categories = [];
                            $scope.requirementDetails.series[0].data = [];
                            $scope.requirementDetails.series[1].data = [];
                            if (angular.element('#requirement').length) {
                                Highcharts.chart('requirement', $scope.requirementDetails);
                            }
                            $scope.requirementDetailsChartShow = false;
                        }



                        if ($scope.projectStats.PROJECT_DETAILS && $scope.projectStats.PROJECT_DETAILS.length > 0)
                        {
                            $scope.projectStats.PROJECT_DETAILS.forEach(function (prjItem,prjIndex) {
                                prjItem.POS_RELEASED_COUNT = 0;
                                prjItem.PENDING_BILLS = 0;
                                prjItem.PENDING_INVOICE_FOR_APPROVAL = 0;
                                var posCount = _.filter($scope.projectStats.RELEASEDPO_COUNT, function (detail) { return detail.PROJECT_ID === prjItem.PROJECT_ID; })
                                if (posCount && posCount.length > 0)
                                {
                                    prjItem.POS_RELEASED_COUNT = posCount[0].POS_RELEASED_COUNT;
                                }
                                var billsCount = _.filter($scope.projectStats.PENDING_BILLS_COUNT, function (detail) { return detail.PROJECT_ID === prjItem.PROJECT_ID; })
                                if (billsCount && billsCount.length > 0) {
                                    prjItem.PENDING_BILLS = billsCount[0].PENDING_BILLS;
                                }

                                var pendingCount = _.filter($scope.projectStats.PENDING_INVOICE_APPROVAL, function (detail) { return detail.PROJECT_ID === prjItem.PROJECT_ID; })
                                if (pendingCount && pendingCount.length > 0) {
                                    prjItem.PENDING_INVOICE_FOR_APPROVAL = pendingCount[0].PENDING_INVOICE_FOR_APPROVAL;
                                }
                            });
                        }

                        $scope.transactionDetails.xAxis.categories = [];
                        $scope.transactionDetails.series[0].data = [];
                        $scope.transactionDetails.series[1].data = [];
                        if ($scope.projectStats.TRANSACTIONAL_TREND && $scope.projectStats.TRANSACTIONAL_TREND.length > 0) {
                            $scope.projectStats.TRANSACTIONAL_TREND.forEach(function (keyVal, index) {
                                $scope.transactionDetails.xAxis.categories.push(keyVal.MONTH_NAME.toString());
                                $scope.transactionDetails.series[0].data.push(keyVal.MY_REQ_COUNT);
                                $scope.transactionDetails.series[1].data.push(keyVal.TOTAL_REQ_COUNT);
                            });
                            if (angular.element('#transaction').length >0) {
                                Highcharts.chart('transaction', $scope.transactionDetails);
                            }
                            $scope.transactionDetailsChartShow = true;
                        } else {
                            if (angular.element('#transaction').length) {
                                Highcharts.chart('transaction', $scope.transactionDetails);
                            }
                            $scope.transactionDetails.xAxis.categories = [];
                            $scope.transactionDetails.series[0].data = [];
                            $scope.transactionDetails.series[1].data = [];
                            $scope.transactionDetailsChartShow = false;
                        }
                    });
            };

            $scope.precisionRound = function (number, precision) {
                var factor = Math.pow(10, precision);
                return Math.round(number * factor) / factor;
            };

            $scope.getType = function (type)
            {
                if (type == 'OVERALL' ) {
                    $scope.filters = {};
                    $scope.searchValue.projects = '';
                    $scope.projectId = '';
                    $scope.filterDate.toDate = moment().format('YYYY-MM-DD');
                    $scope.filterDate.fromDate = moment().subtract(30, "days").format("YYYY-MM-DD");
                }
                if (type == 'PROJECTS') {
                    $scope.filters = {};
                    $scope.searchValue.projects = '';
                    $scope.projectId = '';
                    $scope.filterDate.toDate = '';
                    $scope.filterDate.fromDate = '';
                }
                $scope.GetProjectDashboardStats();
            };


            $scope.searchValue =
            {
                projects: ''
            };
            $scope.projectCodeTemp = [];
            $scope.filterProjects = function (value)
            {
                $scope.projectCodeTemp = [];
                if (value) {
                    $scope.getProjectLevelFilter(value);
                    $scope.height = document.getElementById("tableHeight").offsetTop;
                    $scope.isProjectSelected = false;
                } else {
                    $scope.projectCodeTemp = [];
                    $scope.isProjectSelected = false;
                }
            };

            $scope.getProjectLevelFilter = function (value)
            {
                let projectCodesTemp = [];
                let clientTemp = [];
                let locationsTemp = [];
                var params = {
                    "COMP_ID": $scope.COMP_ID,
                    "USER_ID": $scope.USER_ID,
                    "FROM_DATE": '2020-01-01',
                    "TO_DATE": moment().format('YYYY-MM-DD'),
                    "DEPT_ID": $scope.SelectedDeptId > 0 ? $scope.SelectedDeptId : $scope.deptIDStr,
                    "DESIG_ID": $scope.desigIDStr,
                    "sessionid": userService.getUserToken()
                };

                PRMProjectServices.getDashboardFilters(params)
                    .then(function (response) {
                        $scope.projectlistFilteredTemp = response;
                        $scope.projectlistFilteredTemp.forEach(function (item, index) {
                            if (item.TYPE === 'PROJ_CODE') {
                                projectCodesTemp.push({ type: item.TYPE, value: item.VALUE, id: item.ID });
                            } else if (item.TYPE === 'CLIENT') {
                                clientTemp.push({ type: item.TYPE, value: item.VALUE });
                            } else if (item.TYPE === 'LOCATION') {
                                locationsTemp.push({ type: item.TYPE, value: item.VALUE });
                            }
                        });

                        $scope.filtersList.projectCode = projectCodesTemp;

                        $scope.projectCodeTemp = _.filter($scope.filtersList.projectCode, function (item) {
                            return (item.value.toUpperCase().indexOf(value.toUpperCase()) > -1
                            );
                        });

                        $scope.filterDate.toDate = moment().format('YYYY-MM-DD');
                        $scope.filterDate.fromDate = moment().subtract(30, "days").format("YYYY-MM-DD");
                        $scope.GetDashboardFilters();

                    });
            };


            $scope.isProjectSelected = false;
            $scope.populateProject = function (project)
            {
                $scope.ProjectTrgtAmtvsPOValChartShow = false;
                $scope.ProjectTrgtAmtvsPOVal.xAxis.categories = [];
                $scope.ProjectTrgtAmtvsPOVal.series[0].data = [];
                $scope.ProjectTrgtAmtvsPOVal.series[1].data = [];
                $scope.purchaseVolumePerBuyer.series[0].data = [];
                $scope.receivableOutstandingChartShow = false;
                $scope.projectStats.PURCHASE_VOL_BY_PROCUREMENT = [];
                $scope.purchaseVolumePerBuyerChartShow = false;
                $scope.monthlyDetailsChartShow = false;
                $scope.monthlyDetails.xAxis.categories = [];
                $scope.monthlyDetails.series[0].data = [];
                $scope.monthlyDetails.series[1].data = [];

                $scope.transactionDetailsChartShow = false;
                //$scope.transactionDetails.xAxis.categories = [];
                //$scope.transactionDetails.series[0].data = [];
                //$scope.transactionDetails.series[1].data = [];

                $scope.requirementDetailsChartShow = false;

                $scope.packageSpentChartShow = false;
                $scope.spentDetails.xAxis.categories = [];
                $scope.spentDetails.series[0].data = [];
                $scope.spentDetails.series[1].data = [];
                //$scope.spentDetails.series[2].data = [];

                $scope.revenuePercentage = 0;
                $scope.projectStats.REVENUE_RECOGNISED = 0;
                $scope.projectStats.PENDING_PO_OVERALL_AMOUNT = 0;
                $scope.projectStats.BILL_REMAINING_AMOUNT = 0;
                $scope.projectStats.BILL_PO_VALUE = 0;
                $scope.projectStats.BILL_CERTIFIED_AMOUNT = 0;
                $scope.totalPOAmountChartShow = false;
                $scope.projectId = project.id;
                $scope.searchValue.projects = project.value;
                $scope.isProjectSelected = true;                
                $scope.GetProjectDashboardStats();
            };


            $scope.ProjectTrgtAmtvsPOVal = {

                chart: {
                    type: 'column',
                    width: 400,
                    height:300
                },
                title: {
                    text: 'Targeted Budget Amount (vs) Anticipated PO Value'
                },
                xAxis: {
                    categories: [],
                    crosshair: true
                },
                yAxis: {
                    min: 0,
                    title: {
                        text: 'Count in Number'
                    }
                },
                tooltip: {
                    headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                    pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                        '<td style="padding:0"><b>{point.y}</b></td></tr>',
                    footerFormat: '</table>',
                    shared: true,
                    useHTML: true
                },
                plotOptions: {
                    column: {
                        pointPadding: 0.2,
                        borderWidth: 0
                    }
                },
                series: [{
                    name: 'Targeted Budget Amount',
                    data: []

                }, {
                    name: 'Anticipated PO Value',
                    data: []

                }],
                credits: {
                    enabled: false
                }

            };

            $scope.showRecievableAmtGraph = function (recievableAmt, invRaised, invPaid) {
                $scope.receivableOutstanding = {

                    chart: {
                        type: 'column',
                        width: 400,
                        height: 300
                    },
                    title: {
                        text: 'Receivable Outstanding'
                    },
                    xAxis: {
                        categories: ['Receivable Outstanding'],
                        crosshair: true
                    },
                    yAxis: {
                        min: 0,
                        title: {
                            text: ''
                        }
                    },
                    tooltip: {
                        headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                        pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                            '<td style="padding:0"><b>{point.y}</b></td></tr>',
                        footerFormat: '</table>',
                        shared: true,
                        useHTML: true
                    },
                    plotOptions: {
                        column: {
                            stacking: 'normal',
                            pointPadding: 0.2,
                            borderWidth: 0
                        }
                    },
                    series:
                        [
                            {
                                name: 'Recievable OutStanding',
                                data: [recievableAmt]
                            },
                            {
                                name: 'Invoice Raised',
                                data: [invRaised]
                            },
                            {
                                name: 'Invoice Paid',
                                data: [invPaid]
                            }
                        ],
                    credits: {
                        enabled: false
                    }
                };
            };


            $scope.spentDetails = {

                chart: {
                    type: 'column',
                    width: 400,
                    height: 300
                },
                title: {
                    text: 'Budget'
                },
                xAxis: {
                    categories: [],
                    crosshair: true
                },
                yAxis: {
                    min: 0,
                    title: {
                        text: ''
                    }
                },
                tooltip: {
                    headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                    pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                        '<td style="padding:0"><b>{point.y}</b></td></tr>',
                    footerFormat: '</table>',
                    shared: true,
                    useHTML: true
                },
                plotOptions: {
                    column: {
                        stacking: 'normal'
                    }
                },
                series:
                    [
                         {
                            name: 'Planned',
                            data: []
                        },
                        {
                            name: 'Spent',
                            data: []
                        }
                        //,
                        //{
                        //    name: 'Remaining',
                        //    data: []
                        //}
                    ],
                credits: {
                    enabled: false
                }
            };



            $scope.monthlyDetails = {
                credits: {
                    enabled: false
                },
                chart: {
                    type: 'column',
                    width: 400,
                    height: 280
                },
                title: {
                    text: 'Monthly Average Savings'
                },
                subtitle: {
                    text: 'Source: Requirements'
                },
                xAxis: {
                    categories: []
                },
                yAxis: {
                    title: {
                        text: 'Price (INR)'
                    }
                },
                plotOptions: {
                    bar: {
                        dataLabels: {
                            enabled: true
                        },
                        enableMouseTracking: false
                    }
                },
                series: [{
                    name: 'Requirement Value',
                    data: []
                }, {
                    name: 'Savings Achieved',
                    data: []
                }
                ]
            };

            $scope.transactionDetails = {
                credits: {
                    enabled: false
                },
                chart: {
                    type: 'column',
                    width: 400,
                    height: 280
                },
                title: {
                    text: ' My Transactional Trend'
                },
                subtitle: {
                    text: 'Source: My Transactional Trend Vs Organizational Trend'
                },
                xAxis: {
                    categories: []
                },
                yAxis: {
                    title: {
                        text: 'RFQ counts'
                    }

                },
                plotOptions: {
                    bar: {
                        dataLabels: {
                            enabled: true
                        },
                        enableMouseTracking: false
                    }
                },
                series: [{
                    name: 'Organizational Trend',
                    data: []
                }, {
                    name: 'My Transactional Trend',
                    data: []
                }
                ]
            };


            $scope.requirementDetails = {
                credits: {
                    enabled: false
                },
                chart: {
                    type: 'column',
                    width: 400,
                    height:280
                },
                title: {
                    text: 'Monthly Average Utilization'
                },
                subtitle: {
                    text: 'Source: Requirements'
                },
                xAxis: {
                    categories: []
                },
                yAxis: {

                    title: {
                        text: 'Requriments'
                    }

                },
                plotOptions: {
                    bar: {
                        dataLabels: {
                            enabled: true
                        },
                        enableMouseTracking: false
                    }
                },
                series: [{
                    name: 'Requirement Pending',
                    data: []
                }, {
                    name: 'Requirement Posted',
                    data: []
                }
                ]
            };


            $scope.purchaseVolumePerBuyer = {
                chart: {
                    type: 'column'
                },

                title: {
                    text: 'Purchase Volume Per Buyer'
                },
                accessibility: {
                    announceNewData: {
                        enabled: true
                    }
                },
                xAxis: {
                    type: 'category'
                },
                yAxis: {
                    title: {
                        text: 'Purchase Volume'
                    }

                },
                legend: {
                    enabled: false
                },
                plotOptions: {
                    series: {
                        borderWidth: 0,
                        dataLabels: {
                            enabled: true,
                            format: '{point.y:.' + $scope.companyRoundingDecimalSetting + '"f}'
                        }
                    }
                },

                tooltip: {
                    //headerFormat: '<span style="font-size:11px;color:{point.name}"></span>',
                    //pointFormat: ': <b> { point.y:.' + $scope.companyRoundingDecimalSetting + '"f}</b><br/>'
                },

                series: [
                    {
                        name: "",
                        colorByPoint: true,
                        data: []
                    }
                ],
                credits: {
                    enabled: false
                }
            };


            $scope.revenueStats = {
                chart: {
                    type: 'column'
                },

                title: {
                    text: 'Revenue'
                },
                accessibility: {
                    announceNewData: {
                        enabled: true
                    }
                },
                xAxis: {
                    type: []
                },
                yAxis: {
                    title: {
                        text: ''
                    }

                },
                legend: {
                    enabled: false
                },
                plotOptions: {
                    series: {
                        borderWidth: 0,
                        dataLabels: {
                            enabled: true,
                            format: '{point.y:.' + $scope.companyRoundingDecimalSetting + '"f}'
                        }
                    }
                },

                tooltip: {
                    //headerFormat: '<span style="font-size:11px;color:{point.name}"></span>',
                    //pointFormat: ': <b> { point.y:.' + $scope.companyRoundingDecimalSetting + '"f}</b><br/>'
                },

                series: [
                    {
                        name: "",
                        colorByPoint: true,
                        data: []
                    }
                ],
                credits: {
                    enabled: false
                }
            };


            //Pending Approvals
            $scope.GetMyPendingApprovals = function () {
                $scope.totalPendingApprovals = 0;
                let listUserDepartmentDesignations = userService.getListUserDepartmentDesignations();
                let deptIDStr = '';
                let desigIDStr = '';
                let desigIDs = [];
                let deptIDs = [];
                if (listUserDepartmentDesignations && listUserDepartmentDesignations.length > 0) {
                    listUserDepartmentDesignations.forEach(function (item, index) {
                        deptIDs.push(item.deptID);
                        item.listDesignation.forEach(function (item1, index1) {
                            if (item1.isAssignedToUser && item1.isValid) {
                                desigIDs.push(item1.desigID);
                                desigIDs = _.union(desigIDs, [item1.desigID]);
                            }
                        });
                    });

                    deptIDStr = deptIDs.join(',');
                    desigIDStr = desigIDs.join(',');
                }

                workflowService.GetMyPendingApprovals(deptIDStr, desigIDStr)
                    .then(function (response) {
                        if (response) {
                            $scope.totalPendingApprovals = response.length;
                        }
                    });
            };

            $scope.GetMyPendingApprovals();
            //^Pending Approvals

            $scope.goToPendingApprovals = function () {
                $state.go('workflow-pending-approvals', { 'userId': userService.getUserId() });
            };


            function findSingle(arr) {
                return arr.filter(i => arr.filter(j => i === j).length === 1)
            }

        }]);