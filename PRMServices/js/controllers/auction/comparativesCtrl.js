﻿prmApp
    .controller('comparativesCtrl', ["$scope", "$state", "$log", "$stateParams", "userService", "auctionsService", "$window",
        "$timeout", "reportingService", "PRMCustomFieldService",
        function ($scope, $state, $log, $stateParams, userService, auctionsService, $window, $timeout, reportingService, PRMCustomFieldService) {
        $scope.reqId = $stateParams.reqID;
        $scope.isUOMDifferent = false;
        $scope.isCustomer = userService.getUserType() == "CUSTOMER" ? true : false;
        if (!$scope.isCustomer) {
            $state.go('home');
            };
        //$scope.CSS_COLOR_NAMES_TEMP = ["AliceBlue", "AntiqueWhite", "Aqua", "Aquamarine", "Azure", "Beige", "Bisque", "Black", "BlanchedAlmond", "Blue", "BlueViolet", "Brown", "BurlyWood", "CadetBlue", "Chartreuse", "Chocolate", "Coral", "CornflowerBlue", "Cornsilk", "Crimson", "Cyan", "DarkBlue", "DarkCyan", "DarkGoldenRod", "DarkGray", "DarkGrey", "DarkGreen", "DarkKhaki", "DarkMagenta", "DarkOliveGreen", "DarkOrange", "DarkOrchid", "DarkRed", "DarkSalmon", "DarkSeaGreen", "DarkSlateBlue", "DarkSlateGray", "DarkSlateGrey", "DarkTurquoise", "DarkViolet", "DeepPink", "DeepSkyBlue", "DimGray", "DimGrey", "DodgerBlue", "FireBrick", "FloralWhite", "ForestGreen", "Fuchsia", "Gainsboro", "GhostWhite", "Gold", "GoldenRod", "Gray", "Grey", "Green", "GreenYellow", "HoneyDew", "HotPink", "IndianRed", "Indigo", "Ivory", "Khaki", "Lavender", "LavenderBlush", "LawnGreen", "LemonChiffon", "LightBlue", "LightCoral", "LightCyan", "LightGoldenRodYellow", "LightGray", "LightGrey", "LightGreen", "LightPink", "LightSalmon", "LightSeaGreen", "LightSkyBlue", "LightSlateGray", "LightSlateGrey", "LightSteelBlue", "LightYellow", "Lime", "LimeGreen", "Linen", "Magenta", "Maroon", "MediumAquaMarine", "MediumBlue", "MediumOrchid", "MediumPurple", "MediumSeaGreen", "MediumSlateBlue", "MediumSpringGreen", "MediumTurquoise", "MediumVioletRed", "MidnightBlue", "MintCream", "MistyRose", "Moccasin", "NavajoWhite", "Navy", "OldLace", "Olive", "OliveDrab", "Orange", "OrangeRed", "Orchid", "PaleGoldenRod", "PaleGreen", "PaleTurquoise", "PaleVioletRed", "PapayaWhip", "PeachPuff", "Peru", "Pink", "Plum", "PowderBlue", "Purple", "RebeccaPurple", "Red", "RosyBrown", "RoyalBlue", "SaddleBrown", "Salmon", "SandyBrown", "SeaGreen", "SeaShell", "Sienna", "Silver", "SkyBlue", "SlateBlue", "SlateGray", "SlateGrey", "Snow", "SpringGreen", "SteelBlue", "Tan", "Teal", "Thistle", "Tomato", "Turquoise", "Violet", "Wheat", "White", "WhiteSmoke", "Yellow", "YellowGreen"];
        $scope.CSS_COLOR_NAMES = ["AliceBlue", "AntiqueWhite", "Aqua", "Aquamarine", "Azure", "Beige", "Bisque", "BlanchedAlmond", "Blue", "BlueViolet", "Brown", "BurlyWood", "CadetBlue", "Chartreuse", "Chocolate", "Coral", "CornflowerBlue", "Cornsilk", "Crimson", "Cyan", "DarkBlue", "DarkCyan", "DarkGoldenRod", "DarkGray", "DarkGrey", "DarkGreen", "DarkKhaki", "DarkMagenta", "DarkOliveGreen", "DarkOrange", "DarkOrchid", "DarkRed", "DarkSalmon", "DarkSeaGreen", "DarkSlateBlue", "DarkSlateGray", "DarkSlateGrey", "DarkTurquoise", "DarkViolet", "DeepPink", "DeepSkyBlue", "DimGray", "DimGrey", "DodgerBlue", "FireBrick", "FloralWhite", "ForestGreen", "Fuchsia", "Gainsboro", "GhostWhite", "Gold", "GoldenRod", "Gray", "Grey", "Green", "GreenYellow", "HoneyDew", "HotPink", "IndianRed", "Indigo", "Ivory", "Khaki", "Lavender", "LavenderBlush", "LawnGreen", "LemonChiffon", "LightBlue", "LightCoral", "LightCyan", "LightGoldenRodYellow", "LightGray", "LightGrey", "LightGreen", "LightPink", "LightSalmon", "LightSeaGreen", "LightSkyBlue", "LightSlateGray", "LightSlateGrey", "LightSteelBlue", "LightYellow", "Lime", "LimeGreen", "Linen", "Magenta", "Maroon", "MediumAquaMarine", "MediumBlue", "MediumOrchid", "MediumPurple", "MediumSeaGreen", "MediumSlateBlue", "MediumSpringGreen", "MediumTurquoise", "MediumVioletRed", "MidnightBlue", "MintCream", "MistyRose", "Moccasin", "NavajoWhite", "Navy", "OldLace", "Olive", "OliveDrab", "Orange", "OrangeRed", "Orchid", "PaleGoldenRod", "PaleGreen", "PaleTurquoise", "PaleVioletRed", "PapayaWhip", "PeachPuff", "Peru", "Pink", "Plum", "PowderBlue", "Purple", "RebeccaPurple", "Red", "RosyBrown", "RoyalBlue", "SaddleBrown", "Salmon", "SandyBrown", "SeaGreen", "SeaShell", "Sienna", "Silver", "SkyBlue", "SlateBlue", "SlateGray", "SlateGrey", "Snow", "SpringGreen", "SteelBlue", "Tan", "Teal", "Thistle", "Tomato", "Turquoise", "Violet", "Wheat", "White", "WhiteSmoke", "Yellow", "YellowGreen"];
        $scope.userId = userService.getUserId();
        $scope.sessionid = userService.getUserToken();
        $scope.listRequirementTaxes = [];
        $scope.listRequirementTaxeslength = $scope.listRequirementTaxes.length;
        $scope.uomDetails = [];
            $scope.requirementSettings = [];
            $scope.selectedTemplate = {};
            $scope.prmFieldMappingDetails = {};
        $scope.subIemsColumnsVisibility = {
            hideSPECIFICATION: true,
            hideQUANTITY: true,
            hideTAX: true,
            hidePRICE: true
        };

       

            $scope.handleUOMItems = function (item) {
                if (item) {
                    item.reqVendors.forEach(function (vendor, index) {
                        var uomDetailsObj = {
                            itemId: item.itemID,
                            vendorId: vendor.vendorID,
                            containsUOMItem: false
                        };

                        if (vendor.quotationPrices) {
                            uomDetailsObj.containsUOMItem = item.productQuantityIn.toUpperCase() != vendor.quotationPrices.vendorUnits.toUpperCase();
                        }

                        $scope.uomDetails.push(uomDetailsObj);
                    });
                }
            };

            $scope.containsUOMITems = function (vendorId) {
                var filteredUOMitems = _.filter($scope.uomDetails, function (uomItem) { return uomItem.vendorId == vendorId && uomItem.containsUOMItem == true; });
                if (filteredUOMitems && filteredUOMitems.length > 0) {
                    return true;
                }

                return false;
            };

            $scope.subPackagesGroupWithSubCategoryArr = [];
            $scope.tempArr = [];
            $scope.tempArr1 = [];
            $scope.GetReqReportForExcel = function ()
            {
                reportingService.GetBidHistoryComparatives({ "REQ_ID": $scope.reqId })
                    .then(function (response) {
                        if (response) {
                            $scope.totalArr = [];
                            $scope.tempArr = JSON.parse(response).Table;
                            $scope.tempArr1 = JSON.parse(response).Table1;
                            $scope.totalArr = $scope.tempArr.concat($scope.tempArr1);
                            $scope.uniqueFirstFiveBidVendors = [];
                            $scope.tempArr.forEach(function (firstBidVendorItem, firstBidVendorIndex) {
                                firstBidVendorItem.BID_NUMBER = 'BID-' + (firstBidVendorItem.INITIAL_BID);
                                $scope.uniqueFirstFiveBidVendors.push(firstBidVendorItem);
                            });
                            let intervals = [2, 3, 4, 5, 6];
                            var uniqueVendors = _.uniqBy($scope.tempArr, item => item.U_ID);
                            $scope.uniqueFirstFiveBidVendors = uniqueVendors;
                            var uniqueItems = _.uniqBy($scope.tempArr, item => item.ITEM_ID);
                            if (uniqueItems && uniqueItems.length > 0) {
                                uniqueItems.forEach(function (item, index) {
                                    let filtererdItems = _.filter($scope.tempArr, function (allItems) { return allItems.ITEM_ID === item.ITEM_ID; });
                                    let filtererdItems1 = _.filter($scope.tempArr1, function (allItems) { return allItems.ITEM_ID === item.ITEM_ID; });
                                    item.VENDORS = filtererdItems;
                                    if (filtererdItems1 && filtererdItems1.length > 0) {
                                        filtererdItems1.forEach(function (obj, objIndex) {
                                            item.VENDORS.push(obj);
                                        });
                                        intervals.forEach(function (interval) {
                                            filtererdItems1.forEach(function (obj, objIndex) {
                                                obj.BID_ID = (obj.REVISED_BID + 1);
                                                if (interval === obj.BID_ID) {
                                                    obj.BID_NUMBER = 'BID-' + (obj.REVISED_BID + 1);
                                                    var ifExists = _.findIndex($scope.uniqueFirstFiveBidVendors, function (existVendWithBidId) { return existVendWithBidId.BID_NUMBER === obj.BID_NUMBER && existVendWithBidId.U_ID === obj.U_ID });
                                                    if (ifExists < 0) {
                                                        $scope.uniqueFirstFiveBidVendors.push(obj);
                                                    }
                                                }
                                            });
                                        });
                                    }
                                });
                            }
                            $scope.uniqueFirstFiveBidVendors.forEach(function (item, index) {
                                let filteredBidNumbers = _.filter($scope.uniqueFirstFiveBidVendors, function (allItems) { return allItems.BID_NUMBER === item.BID_NUMBER; }).length;
                                item.COUNT = filteredBidNumbers;
                                item.COLOR = $scope.CSS_COLOR_NAMES[index];
                            });
                            $scope.subPackagesGroupWithSubCategoryArr = uniqueItems;
                            

                            $scope.download = { isPageLoaded: false };
                            $timeout(function () {
                                executeExcelDownload();
                                $scope.download = { isPageLoaded: true };
                            }, 5000);
                        }
                    });
            };

            $scope.getSubTotal = function (subPackage,subCategory,vendor)
            {
                var items = _.filter($scope.totalArr, function (allItems) { return allItems.PROD_ID === subPackage && allItems.BRAND === subCategory && allItems.U_ID === vendor.U_ID && allItems.BID_NUMBER === vendor.BID_NUMBER });
                return _.sumBy(items, 'AMOUNT');
            };

            $scope.getTotal = function (vendor) {
                var items = _.filter($scope.totalArr, function (allItems) { return allItems.U_ID === vendor.U_ID && allItems.BID_NUMBER === vendor.BID_NUMBER });
                return _.sumBy(items, 'AMOUNT');
            };

            $scope.getGstAmount = function (vendor) {
                var items = _.filter($scope.totalArr, function (allItems) { return allItems.U_ID === vendor.U_ID && allItems.BID_NUMBER === vendor.BID_NUMBER });
                return _.sumBy(items, 'GST_AMOUNT');
            };

            $scope.getTotalPrice = function (vendor) {
                var items = _.filter($scope.totalArr, function (allItems) { return allItems.U_ID === vendor.U_ID && allItems.BID_NUMBER === vendor.BID_NUMBER });
                return _.sumBy(items, 'TOTAL_PRICE');
            };

            $scope.GetReqReportForExcel();

            function executeExcelDownload()
            {
                tableToExcel('testTable', 'Comparitives');
            }


        $scope.doPrint = false;

            $scope.printReport = function () {
                $scope.doPrint = true;
                $timeout(function () {
                    $window.print();
                    $scope.doPrint = false
                }, 1000);
            };


        $scope.htmlToCanvasSaveLoading = false;

        $scope.htmlToCanvasSave = function (format) {
            $scope.htmlToCanvasSaveLoading = true;
            setTimeout(function () {
                try {
                    var name = "Comparisions-ReqID-" + $scope.reqId + "." + format;
                    var canvas = document.createElement("canvas");
                    if (format == 'pdf') {
                        document.getElementById("widget").style["display"] = "";
                    }
                    else {
                        document.getElementById("widget").style["display"] = "inline-block";
                    }

                    html2canvas($("#widget"), {
                        onrendered: function (canvas) {
                            theCanvas = canvas;

                            const a = document.createElement("a");
                            a.style = "display: none";
                            a.href = canvas.toDataURL();

                            // Add Image to HTML
                            //document.body.appendChild(canvas);

                            /* Save As PDF */
                            if (format == 'pdf') {
                                var imgData = canvas.toDataURL();
                                var pdf = new jsPDF();
                                pdf.addImage(imgData, 'JPEG', 0, 0);
                                pdf.save(name);
                            }
                            else {
                                a.download = name;
                                a.click();
                            }

                            // Clean up 
                            //document.body.removeChild(canvas);
                            document.getElementById("widget").style["display"] = "";
                            $scope.htmlToCanvasSaveLoading = false;
                        }
                    });
                }
                catch (err) {
                    document.getElementById("widget").style["display"] = "";
                    $scope.htmlToCanvasSaveLoading = false;
                }
                finally {

                }

            }, 500);

        };

            $scope.getTotalTax = function (quotation) {
                if (quotation) {
                    if (!quotation.cGst || quotation.cGst == undefined) {
                        quotation.cGst = 0;
                    }

                    if (!quotation.sGst || quotation.sGst == undefined) {
                        quotation.sGst = 0;
                    }

                    if (!quotation.iGst || quotation.iGst == undefined) {
                        quotation.iGst = 0;
                    }

                    return quotation.cGst + quotation.sGst + quotation.iGst;

                } else {
                    return 0;
                }
            };

            $scope.isOnlySpecificationTemplate = function (productQuotationTemplateArray) {
                var isOnlySpecificatoin = true;
                if (productQuotationTemplateArray.length > 0) {
                    var items = _.filter(productQuotationTemplateArray, function (item) { return item.HAS_PRICE; });
                    if (items && items.length > 0) {
                        isOnlySpecificatoin = false;
                    }
                }

                return isOnlySpecificatoin;
            };

            $scope.handleSubItemsVisibility = function (productQuotationTemplateArray) {
                if (productQuotationTemplateArray && productQuotationTemplateArray.length > 0) {
                    var items = productQuotationTemplateArray.filter(function (template) {
                        return (template.HAS_SPECIFICATION === 1);
                    });

                    if (items && items.length > 0) {
                        $scope.subIemsColumnsVisibility.hideSPECIFICATION = false;
                    }

                    items = productQuotationTemplateArray.filter(function (template) {
                        return (template.HAS_QUANTITY === 1);
                    });

                    if (items && items.length > 0) {
                        $scope.subIemsColumnsVisibility.hideQUANTITY = false;
                    }

                    items = productQuotationTemplateArray.filter(function (template) {
                        return (template.HAS_TAX === 1);
                    });

                    if (items && items.length > 0) {
                        $scope.subIemsColumnsVisibility.hideTAX = false;
                    }

                    items = productQuotationTemplateArray.filter(function (template) {
                        return (template.HAS_PRICE === 1);
                    });

                    if (items && items.length > 0) {
                        $scope.subIemsColumnsVisibility.hidePRICE = false;
                    }
                }
            };



            $scope.detectLinks = function urlify(text) {
                var urlRegex = /(https?:\/\/[^\s]+)/g;
                return text.replace(urlRegex, function (url) {
                    return '<a target="_blank" href="' + url + '">' + url + '</a>';
                });
            };


            $scope.getRequirementSettings = function () {
                $scope.requirementSettings = [];
                $scope.selectedTemplate.TEMPLATE_NAME = 'PRM_DEFAULT';
                auctionsService.getRequirementSettings({ "reqid": $scope.reqId, "sessionid": $scope.sessionid })
                    .then(function (response) {
                        $scope.requirementSettings = response;
                        if ($scope.requirementSettings && $scope.requirementSettings.length > 0) {
                            var template = $scope.requirementSettings.filter(function (setting) {
                                return setting.REQ_SETTING === 'TEMPLATE_ID';
                            });

                            if (template && template.length > 0) {
                                $scope.selectedTemplate.TEMPLATE_ID = template[0].REQ_SETTING_VALUE;
                            }
                        }

                        if ($scope.selectedTemplate.TEMPLATE_ID || $scope.selectedTemplate.TEMPLATE_NAME) {
                            $scope.GetPRMTemplateFields();
                        }
                    });
            };

            $scope.getRequirementSettings();

            $scope.GetPRMTemplateFields = function () {
                $scope.prmFieldMappingDetails = {};
                var params = {
                    "templateid": $scope.selectedTemplate.TEMPLATE_ID ? $scope.selectedTemplate.TEMPLATE_ID : 0,
                    "templatename": $scope.selectedTemplate.TEMPLATE_NAME ? $scope.selectedTemplate.TEMPLATE_NAME : '',
                    "sessionid": userService.getUserToken()
                };

                PRMCustomFieldService.GetTemplateFields(params).then(function (mappingDetails) {
                    mappingDetails.forEach(function (item, index) {
                        $scope.prmFieldMappingDetails[item.FIELD_NAME] = item;
                    });
                });
            };





    }]);