﻿prmApp
    .controller('cashFlowCtrl', ["$rootScope", "$scope", "$stateParams", "$log", "$state", "$window", "userService", "auctionsService", "storeService", "growlService", "PRMPRServices",
        "PRMCustomFieldService", "catalogService", "$location", "PRMUploadServices", "$element", "fileReader",
        function ($rootScope, $scope, $stateParams, $log, $state, $window, userService, auctionsService, storeService, growlService, PRMPRServices, PRMCustomFieldService, catalogService, $location, PRMUploadServices, $element, fileReader) {

            $scope.userID = userService.getUserId();
            $scope.sessionID = userService.getUserToken();
            $scope.deptID = userService.getSelectedUserDepartmentDesignation().deptID;
            $scope.compId = userService.getUserCompanyId();

            $scope.totalItems = 0;
            $scope.currentPage = 1;
            $scope.itemsPerPage = 10;
            $scope.maxSize = 5;
            $scope.filters = {
                searchKeyword: '',
            }

            $scope.setPage = function (pageNo) {
                $scope.currentPage = pageNo;
                $scope.getCashFlowDetails(($scope.currentPage - 1), 10,$scope.filters.searchKeyword);
            };

            $scope.pageChanged = function () {
                ////console.log('Page changed to: ' + $scope.currentPage);
            };

            if ($state.current.name == 'invoiceUpload')
            {
                $scope.type = 'INVOICES';
            }
            if ($state.current.name == 'revenueUpload') {
                $scope.type = 'REVENUE';
            }




            $scope.exportToExcel = function (type) {
                PRMUploadServices.GetExcelTemplate(type);
            };

            $scope.showErrorPopup = false;
            $scope.rowErrors = [];


            $scope.uploadExcel = function (type, proceedFurther, fileName) {
                $scope.showErrorPopup = false;
                $scope.FILE_NAME = fileName ? fileName : $scope.FILE_NAME;
                var params = {
                    name: type,
                    compID: $scope.compId,
                    sessionID: userService.getUserToken(),
                    tableName: 'SAP_CASH_FLOW',
                    attachment: $scope.attachment,
                    proceedFurther: proceedFurther,
                    fileName: $scope.FILE_NAME
                };
                PRMUploadServices.uploadTemplate(params)
                    .then(function (response) {

                        if (response && response.length > 0) {
                            if (response[0].IS_SUCCESS) {
                                angular.element('#errorPopUp').modal('hide');
                                swal("Status!", "Total Count : " + response[0].TOTAL_COUNT + " \n " + " Successfull Count : " + response[0].SUCCESS_COUNT + " \n " + " Failed Count : " + response[0].FAILED_COUNT, "warning");
                                $("#clientupload").val(null);
                                $scope.getCashFlowDetails(0, 10, $scope.filters.searchKeyword);
                                //$scope.setPage(1);
                            } else {
                                if (!response[0].IS_SUCCESS && proceedFurther === 1) {
                                    $scope.showErrorPopup = true;
                                    $scope.rowErrors = response;
                                    angular.element('#errorPopUp').modal('show');
                                    if (response[0].IS_NO_RECORDS) {
                                        swal("Status!", "Sheet is empty.. So cannot continue further ! Please validate the sheet again and upload.", "error");
                                    }
                                    //else {
                                    //    swal("Status!", "All the records in this sheet contains errors.. So cannot continue further ! Please validate the sheet again and upload.", "error");
                                    //}
                                    $("#clientupload").val(null);
                                    //angular.element('#errorPopUp').modal('hide');
                                } else {
                                    $scope.showErrorPopup = true;
                                    $scope.rowErrors = response;
                                    angular.element('#errorPopUp').modal('show');
                                    $("#clientupload").val(null);
                                }
                            }
                        } else {
                                $scope.uploadExcel($scope.type, 1);
                                $("#clientupload").val(null);
                        }
                    });
            };
            $scope.getFile1 = function (id, itemid, ext) {

                console.log(1)
                $scope.filesTemp = $("#" + id)[0].files;
                $scope.filesTemp = Object.values($scope.filesTemp);
                $scope.filesTemp.forEach(function (attach, attachIndex) {
                    $scope.file = $("#" + id)[0].files[attachIndex];

                    fileReader.readAsDataUrl($scope.file, $scope)
                        .then(function (result) {
                            var fileUpload = {
                                fileStream: [],
                                fileName: '',
                                fileID: 0
                            };
                            var bytearray = new Uint8Array(result);
                            $scope.attachment = $.makeArray(bytearray);
                            fileUpload.fileStream = $.makeArray(bytearray);
                            fileUpload.fileName = attach.name;

                            if (id == "clientupload") {
                                $scope.uploadExcel($scope.type, 0, fileUpload.fileName);
                                $scope.file = [];
                                $scope.file.name = '';
                            }
                        });
                });
            };

            $scope.exportErrorDetails = function (type) {
                $state.go('moduleErrorList', { 'moduleName': type });
            };



            $scope.cancel = function () {
                angular.element('#errorPopUp').modal('hide');
            };

            $scope.getDate = function (value) {
                return userService.toLocalDate(value);
            }
            $scope.tableColumns = [];
            $scope.rows = [];
           
            $scope.getCashFlowDetails = function (recordsFetchFrom, pageSize, searchKeyword) {
                $scope.tableValues = [];

                var params =
                {
                    "TEMPLATE_NAME": $scope.type,
                    "compID": $scope.compId,
                    "sessionid": $scope.sessionID,
                    "searchKeyword": searchKeyword,
                    "PageSize": recordsFetchFrom * pageSize,
                    "NumberOfRecords": pageSize
                    
                };

                auctionsService.GetCashFlowDetails(params)
                    .then(function (response) {
                        if (response) {
                            $scope.arr = JSON.parse(response).Table;
                            if ($scope.arr && $scope.arr.length > 0) {
                                $scope.totalItems = $scope.arr[0].TOTAL_COUNT;
                                $scope.arr.forEach(a => delete a.TOTAL_COUNT);
                                if ($scope.tableColumns.length <= 0) {
                                    $scope.tableColumnsTemp = angular.copy(_.keys($scope.arr[0]));
                                    $scope.tableColumnsTemp.forEach(function (item, index) {
                                        item = item.replaceAll("_", " ");
                                        $scope.tableColumns.push(item);
                                    });
                                }

                                $scope.rows = $scope.arr;
                                $scope.arr.forEach(function (item, index) {
                                    if (item.Date) {
                                        item.Date = moment(item.Date).format("DD-MM-YYYY");
                                    }
                                    var obj = angular.copy(_.values(item));
                                    if (obj) {
                                        item.tableValues = [];
                                        obj.forEach(function (value, valueIndex) {
                                            item.tableValues.push(value);
                                        });
                                    }
                                });
                            }
                        } else {
                            $scope.rows = [];
                            $scope.arr = [];
                            $scope.totalItems = 0;
                        }


                        
                    })
                
            }
            $scope.getCashFlowDetails(0, 10, $scope.filters.searchKeyword);
            

          
        }]);