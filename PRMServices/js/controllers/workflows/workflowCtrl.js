﻿prmApp
    .controller('workflowCtrl', function ($scope, $state, $stateParams, userService, growlService, workflowService, auctionsService, fileReader, $log, catalogService) {
        $scope.compID = userService.getUserCompanyId();
        $scope.sessionID = userService.getUserToken();
        $scope.currentID = userService.getUserId();
        $scope.editWorkflowFlag = false;
        $scope.isCustomer = userService.getUserType() == "CUSTOMER" ? true : false;
        $scope.newStep = {};
        $scope.Departments = [];
        $scope.enableAddStep = false;
        $scope.editEnable = false;

        $scope.sub = {
            approvers:[]
        }

        //$scope.modules = ['CIJ', 'INDENT', 'QUOTATION', 'MARK AS COMPLETED'];

        $scope.modules = [
            //{
            //    display: 'CIJ',
            //    value: 'CIJ'
            //},
            //{
            //    display: 'INDENT',
            //    value: 'INDENT'
            //},
            //{
            //    display: 'QUOTATION',
            //    value: 'QUOTATION'
            //},
            //{
            //    display: 'MARK AS COMPLETED',
            //    value: 'MAC'
            //},
            //{
            //    display: 'QCS',
            //    value: 'QCS'
            //},
            {
                display: 'BUDGET',
                value: 'BUDGET'
            },
            {
                display: 'VENDOR_SELECTION',
                value: 'VENDOR_SELECTION'
            },
            //{
            //    display: 'BILL_CERTIFICATION',
            //    value: 'BILL_CERTIFICATION'
            //},
            {
                display: 'PACKAGE_APPROVAL',
                value: 'PACKAGE_APPROVAL'
            }
            //,
            //{
            //    display: 'VENDOR_INVOICE',
            //    value: 'VENDOR_INVOICE'
            //}
            //{
            //    display: 'VENDOR',
            //    value: 'VENDOR'
            //},
            //{
            //    display: 'UNBLOCK VENDOR',
            //    value: 'UNBLOCK_VENDOR'
            //}
        ];


        $scope.CostCentres = userService.getCostCentres();


        $scope.companyLocation1 =
            [
                {
                FIELD_NAME: 'Chirala',
                FIELD_VALUE: 'Chirala'
                },
                {
                    FIELD_NAME: 'Anaparthy',
                    FIELD_VALUE: 'Anaparthy'
                },
                {
                    FIELD_NAME: 'Mysore',
                    FIELD_VALUE: 'Mysore'
                },
                {
                    FIELD_NAME: 'Guntur',
                    FIELD_VALUE: 'Guntur'
                }
            ];

        $scope.getWorkflows = function () {
            workflowService.getWorkflowList()
                .then(function (response) {
                    $scope.workflowList = response;
                    //$scope.showEdit = true;

                    //$scope.workflowList.forEach(function (item, index) {
                    //    if (item.location) {
                    //        item.location = +item.location;
                    //    }
                    //});

                });
        };

        $scope.getWorkflows();
        
        $scope.editWorkflow = function (workflow, flag) {
            $scope.viewDisable = false;
            if (flag == 'EDIT') {
                $scope.viewDisable = false;
            } else if (flag == 'VIEW') {
                $scope.viewDisable = true;
            }
            window.scrollTo(0, 0);
            $scope.currentWorkflow = workflow;
            $scope.workflow = workflow;
            $scope.workflowTitle = workflow.workflowTitle;

            var index = 0;
            index = _.indexOf($scope.DepartmentsArray, _.find($scope.DepartmentsArray, function (o) {
                return o.companyDepartments.deptID == workflow.deptID;
            }));

            $scope.currentWorkflow.deptIDObj = $scope.DepartmentsArray[index];
            $scope.editWorkflowFlag = true;
            $scope.editEnable = true;
        }

        $scope.addWorkflow = function () {
            $scope.currentWorkflow = {};
            $scope.currentWorkflow.WorkflowStages = [];
            $scope.workflow = {};
            $scope.workflowTitle = "";
            $scope.editWorkflowFlag = true;
            $scope.editEnable = false;
            $scope.workflowTitleValidation = false;
        }

        $scope.cancelWorkflow = function () {
            $scope.workflowTitle = "";
            $scope.editWorkflowFlag = false;
            $scope.currentWorkflow.WorkflowStages = [];
            location.reload();
        }

        $scope.deleteStep = function (step, index) {

            //if ($scope.editEnable && index == 0 && $scope.currentWorkflow.WorkflowStages.length == 1) {
            //    if ($scope.editEnable && index == 0 && $scope.currentWorkflow.WorkflowStages.length == 1) {
            //        //$scope.currentWorkflow.WorkflowStages.length = [];
            //        growlService.growl("Cannot Delete This Step", "inverse");
            //    } else {
            //        workflowService.deleteWorkflowStage({ stageID: step.stageID, sessionID: $scope.sessionID })
            //            .then(function (response) {
            //                $scope.currentWorkflow.WorkflowStages.splice(index, 1);
            //                $scope.currentWorkflow.WorkflowStages.forEach(function (item, index) {
            //                    item.order = index + 1;
            //                })
            //                $scope.getWorkflows();
            //            })
            //    }
            //} else {

            if ($scope.editEnable) {
                swal({
                    title: "Are you sure?",
                    text: "Your Steps Will Be Deleted Once You Click On OK Button.",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#F44336",
                    confirmButtonText: "OK",
                    closeOnConfirm: true
                }, function () {
                    if ($scope.editEnable && index == 0 && $scope.currentWorkflow.WorkflowStages.length == 1) {
                        //$scope.currentWorkflow.WorkflowStages.length = [];
                        growlService.growl("Cannot Delete This Step", "inverse");
                    } else {
                        workflowService.deleteWorkflowStage({ stageID: step.stageID, sessionID: $scope.sessionID })
                            .then(function (response) {
                                $scope.currentWorkflow.WorkflowStages.splice(index, 1);
                                $scope.currentWorkflow.WorkflowStages.forEach(function (item, index) {
                                    item.order = index + 1;
                                })
                                //$scope.updateWorkflow();
                                $scope.getWorkflows();
                            })
                    }
                })
            } else {    
                workflowService.deleteWorkflowStage({ stageID: step.stageID, sessionID: $scope.sessionID })
                    .then(function (response) {
                        $scope.currentWorkflow.WorkflowStages.splice(index, 1);
                        $scope.currentWorkflow.WorkflowStages.forEach(function (item, index) {
                            item.order = index + 1;
                        })
                        //$scope.updateWorkflow();
                        $scope.getWorkflows();
                    })

            }

         //   }

        };
        
        $scope.deleteWorkflow = function (workflow) {
            swal({
                title:"",
                text: "Are you sure about deleting workflow?",
                type: "error",
                showCancelButton: true,
                confirmButtonClass: "btn-danger",
                confirmButtonText: "Yes",
                closeOnConfirm: false
               
            }, function () {
                    $scope.workflowList.slice($scope.workflowList.indexOf(workflow), 1);
                    $scope.editWorkflowFlag = false;
                    workflowService.deleteWorkflow({ wID: workflow.workflowID, sessionID: $scope.sessionID })
                        .then(function (response) {
                            //console.log(response); in delete workflow kims
                            //$scope.currentWorkflow = {};
                            //$scope.currentWorkflow.WorkflowStages = [];
                            //$scope.newStep = {};
                            //$scope.getWorkflows();
                            

                            if (response.errorMessage != '') {
                                growlService.growl(response.errorMessage, "inverse");
                                $scope.currentWorkflow = {};
                                $scope.currentWorkflow.WorkflowStages = [];
                                $scope.newStep = {};
                                $scope.getWorkflows();
                            } else {
                                swal("Deleted!", "Your Workflow has been deleted.", "success");
                                $scope.currentWorkflow = {};
                                $scope.currentWorkflow.WorkflowStages = [];
                                $scope.newStep = {};
                                $scope.getWorkflows();
                            }

                        })
            });
                
            
           
        }

        $scope.isDuplicate = true;
        $scope.addStep = function () {
            $scope.newStep.time = 1;
            $scope.newStep.order = 1;
            if ($scope.currentWorkflow && $scope.currentWorkflow.WorkflowStages) {
                $scope.newStep.order = $scope.currentWorkflow.WorkflowStages.length + 1;
            }
            else {
                $scope.currentWorkflow.WorkflowStages = [];
            }
            $scope.isDuplicate = false;
            $scope.currentWorkflow.WorkflowStages.forEach(function (item,index) {
                if (item.approver.desigID == $scope.newStep.approver.desigID && item.department.deptID == $scope.newStep.department.deptID) {
                    $scope.isDuplicate = true;
                }
            })

            if ($scope.isDuplicate)
            {
                growlService.growl("Cannot Add The Same Department And Designation", "inverse");
                return;
            }
            $scope.currentWorkflow.WorkflowStages.push($scope.newStep);
            $scope.newStep = {};
            $scope.enableAddStep = false;
            growlService.growl("Step Added Successfully.", "success");


            $scope.Departments.forEach(function (item, index) {
                item.isChecked = false;
            });


        }

        $scope.addApprover = function (dept, approver, deptIndex, approverIndex) {
            //dept.isChecked = false;
            $scope.newStep.approver = {};
            $scope.newStep.approver.objectID = approver.userDesigID;
            $scope.newStep.department = {};
            $scope.newStep.approver.desigCode = approver.companyDesignations.desigCode;
            $scope.newStep.approver.desigID = approver.companyDesignations.desigID;
            $scope.newStep.department.deptCode = dept.companyDepartments.deptCode;
            $scope.newStep.department.deptID = dept.companyDepartments.deptID;
            $scope.enableAddStep = true;
            if (approver.companyDesignations.approverRange > 0) {
                $scope.newStep.approverRange = approver.companyDesignations.approverRange;
            } else {
                $scope.newStep.approverRange = 0;
            }
            //$scope.validate(dept, approver);

            $scope.deptIndex = deptIndex;
            $scope.approverIndex = approverIndex;

        }

        $scope.validate = function (dept, approver) {
            $scope.Departments.forEach(function (depart, index) {
                depart.listUserDesignations.forEach(function (desig, index) {

                    if (depart.companyDepartments.deptID == dept.companyDepartments.deptID && desig.companyDesignations.desigID == approver.companyDesignations.desigID) {
                        approver.companyDesignations.approverRangeDisabled = false;
                    } else {
                        approver.companyDesignations.approverRange = 0;
                        approver.companyDesignations.approverRangeDisabled = true;
                    }

                })
            })
        }



        $scope.saveWorkflow = function () {
            $scope.currentWorkflow.push($scope.newStep);
            $scope.newStep = {};
        }

        $scope.DepartmentsArray = [];

        $scope.GetCompDeptDesig = function () {
            auctionsService.GetCompDeptDesig($scope.compID, $scope.sessionID)
                .then(function (response) {
                    $scope.Departments = response;
                    $scope.Departments = $scope.Departments.filter(function (item) {
                        if (item.companyDepartments.isValid == 1) {
                            return item;
                        } else {

                        }
                    })
                    $scope.DepartmentsArray = $scope.Departments;

                    //$scope.DepartmentsArray = $scope.Departments.filter(function (item) {
                    //    return item.companyDepartments.selectedDeptType.typeID == 5;
                    //});

                    
                });
        }

        $scope.GetCompDeptDesig();

        $scope.workflowTitleValidation = false;
        $scope.updateWorkflow = function () {
            $scope.currentWorkflow.modifiedBy = $scope.currentID;
            $scope.currentWorkflow.companyID = $scope.compID;
            if ($scope.currentWorkflow.WorkflowStages && $scope.currentWorkflow.WorkflowStages.length > 0) {
                $scope.currentWorkflow.WorkflowStages.forEach(function (item) {
                    item.modifiedBy = $scope.currentID;
                })
                

                if ($scope.currentWorkflow.deptIDObj && $scope.currentWorkflow.deptIDObj.companyDepartments && $scope.currentWorkflow.deptIDObj.companyDepartments.deptID > 0) {
                    $scope.currentWorkflow.deptID = $scope.currentWorkflow.deptIDObj.companyDepartments.deptID;
                } else {
                    $scope.currentWorkflow.deptID = 0;
                }


                workflowService.updateWorkflow($scope.currentWorkflow)
                .then(function (response) {
                    if (response.errorMessage != '') {
                        growlService.growl(response.errorMessage, "inverse");
                        $scope.workflowTitleValidation = true; 
                    } else {
                        $scope.getWorkflows();
                        growlService.growl("Workflow Saved Successfully.", "success");
                        $scope.currentWorkflow.WorkflowStages = [];
                        $scope.workflowTitle = "";
                        $scope.workflowTitleValidation = false;
                        $scope.editWorkflowFlag = false;
                    }
                });
            }

            
        }

        $scope.cancelEditWF = function () {
            $scope.editWorkflowFlag = false;
            $scope.currentWorkflow = {
                WorkflowStages: []
            }           
        }

        $scope.minimizeOthers = function (dept) {
            $scope.deptIndex = -1;
            $scope.approverIndex = -1;
            $scope.Departments.forEach(function (item) {
                if (item.companyDepartments.deptID != dept.companyDepartments.deptID) {
                    item.isChecked = false;
                }

                item.listUserDesignations.forEach(function (desg) {
                    desg.companyDesignations.approverRange = 0;
                })

            })
        };


        $scope.SubPackages = [];

        $scope.loadSubCategories = function () {
            if ($scope.currentWorkflow.deptIDObj) {
                $scope.currentWorkflow.deptID = 0;
            }

            catalogService.getcategories($scope.compID, 0, 0)
                .then(function (response) {
                    response.forEach(function (catItem, catIndex) {
                        catItem.catNameTemp = catItem.catName.toLowerCase();
                    });
                    $scope.companyCatalogTemp = [];
                    $scope.companyCatalog = response;
                    $scope.companyCatalogTemp = response;


                    $scope.companyCatalog.forEach(function (item, index) {
                        if (item.nodes && item.nodes.length > 0) {
                            item.nodes.forEach(function (nodeItem, nodeItemIndex) {
                                $scope.SubPackages.push(nodeItem);
                            });
                        }
                    });

                });

        };

    });