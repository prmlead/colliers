﻿prmApp
    .controller('workflowPendingApprovalsCtrl', function ($scope, $state, $stateParams, userService, growlService,
        workflowService, $http, domain,
        auctionsService, fileReader, $log, $window) {
        
        $scope.MyPendingApprovals = [];

        $scope.MyPendingApprovals1 = [];

        $scope.MyPendingApprovals2 = [];

        $scope.MyPendingApprovalsSearch = [];
        $scope.pendinguserlist = [];
        $scope.pendinguserlist.push('ALL');
        /*PAGINATION CODE*/
        $scope.totalItems = 0;
        $scope.currentPage = 1;
        $scope.itemsPerPage = 10;
        $scope.maxSize = 5; //Number of pager buttons to show
        $scope.deptIDs = [];
        $scope.desigIDs = [];
        $scope.deptIDStr = '';
        $scope.desigIDStr = '';


        $scope.modules = [
            {
                display: 'BUDGET',
                value: 'BUDGET'
            },
            {
                display: 'VENDOR SELECTION',
                value: 'VENDOR_SELECTION'
            },
            {
                display: 'PACKAGE APPROVAL',
                value: 'PACKAGE_APPROVAL'
            },
            {
                display: 'BILL CERTIFICATION',
                value: 'BILL_CERTIFICATION'
            },
            {
                display: 'PO_AMENDMENT',
                value: 'PO_AMENDMENT'
            },
            {
                display: 'PROJECT_AMENDMENT',
                value: 'PROJECT_AMENDMENT'
            },
            {
                display: 'VENDOR_FORM_WORKFLOW',
                value: 'VENDOR_FORM_WORKFLOW'
            }
        ];
        
        $scope.ListUserDepartmentDesignations = userService.getListUserDepartmentDesignations();
        if ($scope.ListUserDepartmentDesignations && $scope.ListUserDepartmentDesignations.length > 0) {
            $scope.ListUserDepartmentDesignations.forEach(function (item, index) {
                $scope.deptIDs.push(item.deptID);
                item.listDesignation.forEach(function (item1, index1) {
                    if (item1.isAssignedToUser && item1.isValid) {
                        $scope.desigIDs.push(item1.desigID);
                        $scope.desigIDs = _.union($scope.desigIDs, [item1.desigID]);
                    }
                });
            });
            $scope.deptIDStr = $scope.deptIDs.join(',');
            $scope.desigIDStr = $scope.desigIDs.join(',');
        }
        
        $scope.setPage = function (pageNo) {
            $scope.currentPage = pageNo;
        };

        $scope.pageChanged = function () {
            ////console.log('Page changed to: ' + $scope.currentPage);
        };
        /*PAGINATION CODE*/

        $scope.GetMyPendingApprovals = function () {
            workflowService.GetMyPendingApprovals($scope.deptIDStr, $scope.desigIDStr)
                .then(function (response) {
                    $scope.MyPendingApprovals = response;

                    var arr = [];
                    $scope.MyPendingApprovals.forEach(function (item, index) {
                        if (item.DATE_CREATED)
                        {
                            item.DATE_CREATED = $scope.GetDateconverted(item.DATE_CREATED);
                        }

                        if (item.pendingApprovalName) {
                            var arr = [];
                            arr = item.pendingApprovalName.split(',');
                            if (arr && arr.length > 0) {
                                arr.forEach(function (item1, index1) {
                                    if ($scope.pendinguserlist.length <= 0) {
                                        $scope.pendinguserlist.push(item1);
                                    } else {
                                        if ($scope.pendinguserlist.indexOf(item1) <= -1) {
                                            $scope.pendinguserlist.push(item1);
                                        }
                                    }

                                })
                            }
                        }

                    });


                    $scope.MyPendingApprovals1 = response;
                    $scope.MyPendingApprovals2 = response;
                    $scope.MyPendingApprovalsSearch = response;
                    /*PAGINATION CODE START*/
                    $scope.totalItems = $scope.MyPendingApprovals.length;
                    /*PAGINATION CODE END*/
                });
        }

        $scope.GetMyPendingApprovals();

        $scope.gotoWFApproval = function (module, moduleID, workflowID,wf) {
            if (module == 'PR')
            {
                var url = $state.href('save-pr-details', { "Id": moduleID });
                $window.open(url, '_blank');
                //var url = $state.href('workflow-status', { "moduleID": moduleID, "module": module, "workflowID": workflowID });
                //$window.open(url, '_blank');
            }
            else if (module == 'QUOTATION')
            {
                var url = $state.href('workflow-status', { "moduleID": moduleID, "module": module, "workflowID": workflowID });
                $window.open(url, '_blank');
            }
            else if (module == 'PO') {
                var url = $state.href('workflow-status', { "moduleID": moduleID, "module": module, "workflowID": workflowID });
                $window.open(url, '_blank');
            }
            else if (module == 'JMS') {
                var url = $state.href('save-jms-details', { "Id": moduleID, "module": module, "workflowID": workflowID });
                $window.open(url, '_blank');
            }
            else if (module == 'CL') {
                var url = $state.href('checklistForMMS', { "Id": moduleID, "module": module, "workflowID": workflowID });
                $window.open(url, '_blank');
            }
            else if (module == 'QCS') {
                if (wf.qcsWorkFlow == 'DOMESTIC') {
                    var url1 = $state.href('cost-comparisions-qcs', { "reqID": wf.workflowModuleViewID, "qcsID": moduleID });
                } else {
                    var url1 = $state.href('import-qcs', { "reqID": wf.workflowModuleViewID, "qcsID": moduleID });
                }
                $window.open(url1, '_self');
            }
            else if (module == 'BUDGET' || module == 'VENDOR_SELECTION') {
                var url1 = $state.href('setupBudget', { "projectId": wf.workflowModuleViewID, "budgetId": moduleID });
                $window.open(url1, '_self');
            }
            else if (module == 'PACKAGE_APPROVAL') {
                var url1 = $state.href('packageApproval', { "projectId": wf.workflowModuleViewID, "budgetId": wf.reqId, "packageApprovalId": moduleID, "qcsId": wf.QCS_ID});
                $window.open(url1, '_self');
            }
            else if (module == 'BILL_CERTIFICATION') {
                var url1 = $state.href('billRequestsList');
                $window.open(url1, '_self');
            }
            else if (module == 'VENDOR') {
                var url2 = $state.href('pages.profile.new-vendor');
                $window.open(url2, '_self');
            }
            else if (module == 'UNBLOCK_VENDOR') {
                var url3 = $state.href('pages.profile.new-vendor');
                $window.open(url3, '_blank');
            }
            else if (module == 'VENDOR_INVOICE') {
                var url4 = $state.href('uploadvendorinvoice', { "ID": wf.workflowModuleViewID });
                $window.open(url4, '_blank');
            }
            else if (module == 'PROJECT_AMENDMENT') {
                var url4 = $state.href('projectAmendment', { "projectId": wf.workflowModuleViewID, "budgetId": wf.reqId, "amendmentId": +wf.workflowModuleID });
                $window.open(url4, '_blank');
            }
            else if (module == 'PO_AMENDMENT') {
                var url4 = $state.href('poAmendment', { "poID": wf.reqNumber, "PO_AMENDMENT_ID": +wf.workflowModuleID });
                $window.open(url4, '_blank');
            }
            else if (module == 'VENDOR_FORM_WORKFLOW') {
                var url4 = $state.href('vendorRegistration1', { "vendorId": wf.workflowModuleViewID});
                $window.open(url4, '_blank');
            }
            
        };

        //$scope.showPopUpClick = function (display, moduleID, module, workflowID) {
        //    $state.go('workflow-status', { moduleID: moduleID, module: module, workflowID: workflowID });
        //};



        $scope.goToModuleView = function (module, moduleViewID) {

            //$stateParams.moduleID, 
            if (module == 'PR') {
                var url = $state.href("save-pr-details", { "Id": moduleViewID });
                window.open(url, '_blank');
            }
            if (module == 'QUOTATION' || module == 'PO') {
                var url = $state.href("view-requirement", { "Id": moduleViewID });
                window.open(url, '_blank');
            }
        };


        $scope.setFilters = function (status, searchKeyword) {
            if (searchKeyword == "") {
                searchKeyword = null;
            }
            if (status && searchKeyword) {

                $scope.MyPendingApprovals = $scope.MyPendingApprovals1.filter(function (item) {
                    if (item.workflowModule === status && ((String(item.workflowModuleCode).toUpperCase().includes(searchKeyword.toUpperCase()))
                        || (String(item.pendingApprovalName).toUpperCase().includes(searchKeyword.toUpperCase()))
                        || (String(item.creatorName).toUpperCase().includes(searchKeyword.toUpperCase())))) {
                        return item.workflowModule === status &&
                            (String(item.workflowModuleCode).toUpperCase().includes(searchKeyword.toUpperCase()) || (String(item.pendingApprovalName).toUpperCase().includes(searchKeyword.toUpperCase()))
                             || (String(item.creatorName).toUpperCase().includes(searchKeyword.toUpperCase())));
                    }

                    });

                } else {
                if (!status && searchKeyword) {
                    $scope.MyPendingApprovals = _.filter($scope.MyPendingApprovals1, function (item) {
                        return (String(item.workflowModuleCode).toUpperCase().includes(searchKeyword.toUpperCase()) || (String(item.pendingApprovalName).toUpperCase().includes(searchKeyword.toUpperCase()))
                            || (String(item.creatorName).toUpperCase().includes(searchKeyword.toUpperCase())))== true;
                    });
                } else {
                    $scope.MyPendingApprovals = $scope.MyPendingApprovals1.filter(function (item) {
                        return item.workflowModule == status;
                    });
                }
            };

            if (!status && !searchKeyword ) {
                $scope.MyPendingApprovals = $scope.MyPendingApprovals1;
            }

            $scope.totalItems = $scope.MyPendingApprovals.length;

        };
        $scope.loadPendingUsers = function () {
            $scope.pendinguserlist = [];
            $scope.pendinguserlist.push('ALL');
            $http({
                method: 'GET',
                url: domain + 'getWorkflowPendingUsers?&sessionid=' + userService.getUserToken(),
                encodeURI: true,
                headers: { 'Content-Type': 'application/json' }
            }).then(function (response) {
                if (response && response.data) {
                    if (response.data && response.data.length > 0) {
                        response.data.forEach(function (item) {
                            $scope.pendinguserlist.push(item);
                        });
                    }
                }
            }, function (result) {

            });

        };

        //$scope.loadPendingUsers();

        $scope.searchTableByPendingNames = function () {
            if ($scope.PendingUserFilter!='ALL') {
                $scope.MyPendingApprovals = _.filter($scope.MyPendingApprovalsSearch, function (item) {
                    return (String(item.pendingApprovalName).includes($scope.PendingUserFilter)) ||
                        String(item.creatorName).toUpperCase().includes($scope.PendingUserFilter.toUpperCase()) == true;
                });
            } else {
                $scope.MyPendingApprovals = $scope.MyPendingApprovalsSearch;
            }
            $scope.setPage(1);
            $scope.totalItems = $scope.MyPendingApprovals.length;


        };

        $scope.GetDateconverted = function (dateBefore) {
            if (dateBefore) {
                return userService.toLocalDate(dateBefore);
            }
        };

    });