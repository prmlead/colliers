﻿prmApp
    .controller('managetechevalCtrl', ["$scope", "$state", "$stateParams", "userService", "growlService", "techevalService", "fileReader", "$log",
        function ($scope, $state, $stateParams, userService, growlService, techevalService, fileReader, $log) {
            $scope.compID = userService.getUserCompanyId();
            $scope.sessionID = userService.getUserToken();
            $scope.currentID = userService.getUserId();

            $scope.isCustomer = userService.getUserType() == "CUSTOMER" ? true : false;


            $scope.questionnaire = {
                evalID: 0,
                reqID: 0,
                userID: userService.getUserId(),
                title: '',
                description: '',
                startDate: '',
                endDate: '',
                questions: [],
                sessionID: $scope.sessionID
            };

            $scope.questionnaireList = [
                questionnaire = $scope.questionnaire
            ];

            $scope.question = {
                evalID: 0,
                questionID: 0,
                attachmentID: 0,
                userID: userService.getUserId(),
                type: '',
                text: '',
                options: '',
                marks: 0,
                isValid: 0,
                sessionID: $scope.sessionID
            };


            $scope.entity = {
                entityName: 'Questions',
                userid: userService.getUserId(),
                attachment: [],
                sessionID: $scope.sessionID
            };

            techevalService.getquestionnairelist(0)
                .then(function (response) {
                    $scope.questionnaireList = response;

                    $scope.questionnaireList.forEach(function (item, index) {
                        item.startDate = new moment(item.startDate).format("DD-MM-YYYY");
                        item.endDate = new moment(item.endDate).format("DD-MM-YYYY");
                    })
                })

            $scope.goToQuestionnaireEdit = function (compID, questionnaireID) {
                $state.go("savequestionnaire", { "compID": compID, "questionnaireID": questionnaireID });
            }

            $scope.gotechevalquestions = function (evalID) {
                $log.info(userService.getUserType);
                if (userService.getUserType() == "VENDOR") {
                    $state.go("techevalresponses", { "evalID": evalID, "vendorID": $scope.currentID });
                } else {
                    $state.go("techevalresponses", { "evalID": evalID, "vendorID": 0 });
                }

            }

            $scope.gotovendorspage = function (evalID) {
                $state.go("techeval", { "reqID": 0, "evalID": evalID });
            }

            $scope.goToAddResponse = function (questionnaire) {

            }

            $scope.downloadTemplate = function (evalIDValue) {
                var mystyle = {
                    sheetid: 'Questions',
                    headers: true,
                    column: {
                        style: 'font-size:15px;background:#233646;color:#FFF;'
                    }
                };
                alasql.fn.getevalID = function () {
                    return evalIDValue;
                }
                alasql.fn.getqueID = function () {
                    return 0;
                }
                alasql('SELECT getevalID() as [EVAL_ID], getqueID() as [Q_ID], attachmentID as [Q_ATTACH_ID], type as [Q_TYPE], text as [Q_TEXT], options as [Q_OPTIONS], marks as [Q_MARKS] INTO XLSX(?,{headers:true,sheetid: "Questions", style: "font-size:15px;background:#233646;color:#FFF;"}) FROM ?', ["Questions.xlsx", $scope.question]);
            }


            $scope.getFile1 = function (id, itemid, ext) {
                $scope.file = $("#" + id)[0].files[0];
                fileReader.readAsDataUrl($scope.file, $scope)
                    .then(function (result) {

                        if (id == "excelquotation") {
                            var bytearray = new Uint8Array(result);
                            $scope.entity.attachment = $.makeArray(bytearray);
                            $scope.importEntity();
                        }
                    });
            };

            $scope.importEntity = function () {
                var params = {
                    "entity": $scope.entity
                };
                techevalService.importentity(params)
                    .then(function (response) {
                        if (response.errorMessage != '') {
                            growlService.growl(response.errorMessage, "inverse");
                        }
                        else {
                            growlService.growl("Quotions Saved Successfully.", "success");
                            location.reload();
                        }
                    })
            };

            $scope.goToAddQuestions = function (questionnaire) {
                $state.go("questions", { "reqID": questionnaire.reqID, "evalID": questionnaire.evalID });
            }

            $scope.goToEditQuestionaire = function (questionnaire) {
                $state.go("savequestionnaire", { "compID": $scope.compID, "questionnaireID": questionnaire.evalID });
            }

            $scope.goToCloneQuestionaire = function (questionnaire) {
                $state.go("clonequestionnaire", { "compID": $scope.compID, "questionnaireID": questionnaire.evalID, "cloneData": true });
            }


            $scope.deletequestionnaire = function (evalID) {

                var params = {
                    "evalID": evalID,
                    sessionID: $scope.sessionID
                };


                swal({
                    title: "Are You Sure!",
                    text: 'Do You want to Delete Questionnaire Permanently',
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Yes",
                    closeOnConfirm: true
                },
                    function () {
                        techevalService.deletequestionnaire(params)
                            .then(function (response) {
                                if (response.errorMessage != '') {
                                    growlService.growl(response.errorMessage, "inverse");
                                }
                                else {
                                    growlService.growl("Questionnaire Deleted Successfully.", "success");
                                    location.reload();
                                }
                            });
                    });



            }

        }]);