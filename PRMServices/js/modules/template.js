prmApp

    // =========================================================================
    // LAYOUT
    // =========================================================================

    .directive('changeLayout', function () {

        return {
            restrict: 'A',
            scope: {
                changeLayout: '='
            },

            link: function (scope, element, attr) {

                //Default State
                if (scope.changeLayout === '1') {
                    element.prop('checked', true);
                }

                //Change State
                element.on('change', function () {
                    if (element.is(':checked')) {
                        localStorage.setItem('ma-layout-status', 1);
                        scope.$apply(function () {
                            scope.changeLayout = '1';
                        })
                    }
                    else {
                        localStorage.setItem('ma-layout-status', 0);
                        scope.$apply(function () {
                            scope.changeLayout = '0';
                        })
                    }
                })
            }
        }
    })



    // =========================================================================
    // MAINMENU COLLAPSE
    // =========================================================================

    .directive('toggleSidebar', function () {

        return {
            restrict: 'A',
            scope: {
                modelLeft: '=',
                modelRight: '='
            },

            link: function (scope, element, attr) {
                element.on('click', function () {

                    if (element.data('target') === 'mainmenu') {
                        if (scope.modelLeft === false) {
                            scope.$apply(function () {
                                scope.modelLeft = true;
                            })
                        }
                        else {
                            scope.$apply(function () {
                                scope.modelLeft = false;
                            })
                        }
                    }

                    if (element.data('target') === 'chat') {
                        if (scope.modelRight === false) {
                            scope.$apply(function () {
                                scope.modelRight = true;
                            })
                        }
                        else {
                            scope.$apply(function () {
                                scope.modelRight = false;
                            })
                        }

                    }
                })
            }
        }

    })



    // =========================================================================
    // SUBMENU TOGGLE
    // =========================================================================

    .directive('toggleSubmenu', function () {

        return {
            restrict: 'A',
            link: function (scope, element, attrs) {
                element.click(function () {
                    element.next().slideToggle(200);
                    element.parent().toggleClass('toggled');
                });
            }
        }
    })


    // =========================================================================
    // STOP PROPAGATION
    // =========================================================================

    .directive('stopPropagate', function () {
        return {
            restrict: 'C',
            link: function (scope, element) {
                element.on('click', function (event) {
                    event.stopPropagation();
                });
            }
        }
    })

    .directive('aPrevent', function () {
        return {
            restrict: 'C',
            link: function (scope, element) {
                element.on('click', function (event) {
                    event.preventDefault();
                });
            }
        }
    })


    // =========================================================================
    // PRINT
    // =========================================================================

    .directive('print', function () {
        return {
            restrict: 'A',
            link: function (scope, element) {
                element.click(function () {
                    window.print();
                })
            }
        }
    })


    // =========================================================================
    // REMOVE INCREMENT DECREMENT IN INPUT FEILD NUMBER
    // =========================================================================

    .directive('input', function () {
        return {
            restrict: 'E',
            scope: {
                type: '@'
            },
            link: function (scope, element) {
                if (scope.type == 'number') {
                    element.on('focus', function () {
                        angular.element(this).on('mousewheel', function (e) {
                            e.preventDefault();
                        });
                    });
                    element.on('blur', function () {
                        angular.element(this).off('mousewheel');
                    });
                }
            }
        }
    })


    .directive('miniItems', function () {
        return {
            restrict: 'EA',
            scope:
            {
                item: '=',
                timeleft: '='
            },
            templateUrl: 'views/mini-item.html',
            controller: ['$scope', '$element', '$attrs', '$timeout', 'userService', '$http', 'auctionsService', 'SignalRFactory', 'domain', function ($scope, $element, $attrs, $timeout, userService, $http, auctionsService, SignalRFactory, domain) {
                // var requirementHub = SignalRFactory('', 'requirementHub');
                $scope.countdown1 = 2;
                $scope.auctionStarted = $scope.item.auctionStarted;
                $scope.stopBids = function () {
                    swal({
                        title: "Are you sure?",
                        text: "The Auction will be stopped after one minute.",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#F44336",
                        confirmButtonText: "Yes, Stop Bids!",
                        closeOnConfirm: false
                    }, function () {
                        $scope.$broadcast('timer-set-countdown-seconds', 60);
                        $scope.disableButtons();
                        var params = {};
                        params.reqID = $scope.item.auctionID;
                        params.sessionID = userService.getUserToken();
                        params.userID = userService.getUserId();
                        params.newTicks = 60;
                        var parties = params.reqID + "$" + params.userID + "$" + params.newTicks + "$" + params.sessionID;
                        auctionsService.updatebidtime(params).then(function (req) {
                            //console.log('requirement updated for item' + req.requirementID);
                        });
                        // auctionsService.updatebidtime(params);
                        // swal("Done!", "Auction time reduced to oneminute.", "success");
                    });
                };
                $scope.updateTime = function (timerId, time) {
                    var isDone = false;
                    $scope.$on('timer-tick', function (event, args) {
                        if (!isDone) {
                            addCDSeconds($scope.item.auctionID, time);
                            isDone = true;
                            var params = {};
                            params.reqID = $scope.item.auctionID;
                            params.sessionID = userService.getUserToken();
                            params.userID = userService.getUserId();
                            params.newTicks = ($scope.countdownVal + time);
                            var parties = params.reqID + "$" + params.userID + "$" + params.newTicks + "$" + params.sessionID;
                            auctionsService.updatebidtime(params).then(function (req) {
                                //console.log('requirement updated for item' + req.requirementID);
                            });
                            //auctionsService.updatebidtime(params);
                        }
                    });
                }

                $scope.$on('timer-tick', function (event, args) {
                    $scope.countdownVal = event.targetScope.countdown;
                    if (event.targetScope.countdown < 61 && !$scope.disableStopBids && !$scope.disablereduceTime) {
                        $timeout($scope.disableButtons(), 1000);
                    }
                    if (event.targetScope.countdown <= 0) {
                        if ($scope.auctionStarted && ($scope.item.status == "CLOSED" || $scope.item.status == "STARTED")) {
                            //$scope.item.minPrice = $scope.item.auctionVendors[0].runningPrice;
                            /*if (($scope.auctionItem.customerID != userService.getUserId() && $scope.auctionItem.superUserID != userService.getUserId() ) && $scope.vendorRank == 1) {
                                swal("Negotiation Completed!", "Congratulations! you are the least bidder for this requirement. Your price is : " + $scope.auctionItem.minPrice + "\n Customer would be reaching out you shortly. All the best!", "success");
                            } else if (($scope.auctionItem.customerID != userService.getUserId() && $scope.auctionItem.superUserID != userService.getUserId() ) && $scope.vendorRank != 1) {
                                swal("Negotiation Completed!", "Bidding Completed.\n Thank You for your interest on this requirement. You ranked " + $scope.vendorRank + " in this requirement. Thank you for your participation.", "success");
                            } 
                            else*/
                            if (($scope.item.customerID == userService.getUserId())) {
                                var params = {};
                                params.reqID = $scope.item.auctionID;
                                params.sessionID = userService.getUserToken();
                                params.userID = userService.getUserId();
                                //var parties = id + "$" + userService.getUserId() + "$" + userService.getUserToken();
                                $http({
                                    method: 'POST',
                                    url: domain + 'endnegotiation',
                                    headers: { 'Content-Type': 'application/json' },
                                    encodeURI: true,
                                    data: params
                                }).then(function (response) {
                                    window.location.reload();
                                });
                                //requirementHub.invoke('EndNegotiation', parties, function () {
                                //    $scope.$broadcast('timer-set-countdown-seconds', 0);
                                //    //swal("Negotiation Completed!", "Congratulations! you procurement process is now completed. " + $scope.toprankerName + " is the least bider with the value " + $scope.auctionItem.minPrice + " \n Your savings through PRM :" + ($scope.vendorInitialPrice - $scope.auctionItem.minPrice), "success");
                                //});

                            }
                        } else if ($scope.item.status == "NOTSTARTED") {
                            window.location.reload();
                        }

                        //$scope.getData();
                    }
                    if (event.targetScope.countdown <= 120) {
                        $scope.timerStyle = { 'color': '#f00' };
                    }
                    if (event.targetScope.countdown > 120 && $scope.item.status == 'NOTSTARTED') {
                        $scope.timerStyle = { 'color': '#000' };
                    }
                    if (event.targetScope.countdown > 120 && $scope.item.status != 'NOTSTARTED') {
                        $scope.timerStyle = { 'color': '#228B22' };
                    }
                    if (event.targetScope.countdown <= 60 && $scope.item.status == 'STARTED') {
                        $scope.disableDecreaseButtons = true;
                    }
                    if (event.targetScope.countdown > 60 && $scope.item.status == 'STARTED') {
                        $scope.disableDecreaseButtons = false;
                    }
                    if (event.targetScope.countdown <= 0 && $scope.item.status == 'NOTSTARTED') {
                        $scope.showTimer = false;

                        //$scope.getData();
                        window.location.reload();
                    }
                    if (event.targetScope.countdown > 0) {
                        //$scope.getData();
                        $scope.disableAddButton = false;
                    }
                });

                $scope.disableButtons = function () {
                    $scope.disablereduceTime = true;
                    $scope.disableStopBids = true;
                }
                $scope.userType = userService.getUserType();
                if ($scope.item.customerID != userService.getUserId()) {
                    $scope.customerBtns = false;
                    $scope.vendorBtns = true;
                } else {
                    $scope.customerBtns = true;
                    $scope.vendorBtns = false;
                }
                $scope.cancelBid = function (itemId) {
                    swal({
                        title: "Are you sure?",
                        text: "The Auction will be cancelled and vendors will be notified.",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#F44336",
                        confirmButtonText: "Yes, Cancel.",
                        closeOnConfirm: false
                    }, function () {
                        $scope.$broadcast('timer-set-countdown-seconds', 0);
                        $scope.disableButtons();
                        var params = {};
                        params.reqID = $scope.item.auctionID;
                        params.sessionID = userService.getUserToken();
                        params.userID = userService.getUserId();
                        params.newTicks = -1;
                        var parties = params.reqID + "$" + params.userID + "$" + params.newTicks + "$" + params.sessionID;
                        auctionsService.updatebidtime(params).then(function (req) {
                            //console.log('requirement updated for item' + req.requirementID);
                        });
                        // auctionsService.updatebidtime(params);
                        // swal("Done!", "Requirement cancelled.", "success");
                    });
                }
            }]
        }
    })

    .directive('miniItemsCompanyLeads', function () {
        return {
            restrict: 'EA',
            scope:
            {
                item: '=',
                timeleft: '='
            },
            templateUrl: 'views/mini-item-company-leads.html',
            controller: ['$scope', '$element', '$attrs', '$timeout', 'userService', 'auctionsService', function ($scope, $element, $attrs, $timeout, userService, auctionsService) {
                $scope.countdown1 = 2;
                $scope.auctionStarted = $scope.item.auctionStarted;
                $scope.stopBids = function () {
                    swal({
                        title: "Are you sure?",
                        text: "The Auction will be stopped after one minute.",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#F44336",
                        confirmButtonText: "Yes, Stop Bids!",
                        closeOnConfirm: false
                    }, function () {
                        $scope.$broadcast('timer-set-countdown-seconds', 60);
                        $scope.disableButtons();
                        var params = {};
                        params.reqID = $scope.item.auctionID;
                        params.sessionID = userService.getUserToken();
                        params.userID = userService.getUserId();
                        params.newTicks = 60
                            ; auctionsService.updatebidtime(params);
                        swal("Done!", "Auction time reduced to oneminute.", "success");
                    });
                };
                $scope.updateTime = function (timerId, time) {
                    var isDone = false;
                    $scope.$on('timer-tick', function (event, args) {
                        if (!isDone) {
                            addCDSeconds($scope.item.auctionID, time);
                            isDone = true;
                            var params = {};
                            params.reqID = $scope.item.auctionID;
                            params.sessionID = userService.getUserToken();
                            params.userID = userService.getUserId();
                            params.newTicks = ($scope.countdownVal + time);
                            auctionsService.updatebidtime(params);
                        }
                    });
                }

                $scope.$on('timer-tick', function (event, args) {
                    $scope.countdownVal = event.targetScope.countdown;
                    if (event.targetScope.countdown < 61 && !$scope.disableStopBids && !$scope.disablereduceTime) {
                        $timeout($scope.disableButtons(), 1000);
                    }
                });

                $scope.disableButtons = function () {
                    $scope.disablereduceTime = true;
                    $scope.disableStopBids = true;
                }
                $scope.userType = userService.getUserType();
                if ($scope.userType == "VENDOR") {
                    $scope.customerBtns = false;
                    $scope.vendorBtns = true;
                } else {
                    $scope.customerBtns = true;
                    $scope.vendorBtns = false;
                }
                $scope.cancelBid = function (itemId) {
                    swal({
                        title: "Are you sure?",
                        text: "The Auction will be cancelled and vendors will be notified.",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#F44336",
                        confirmButtonText: "Yes, Cancel.",
                        closeOnConfirm: false
                    }, function () {
                        $scope.$broadcast('timer-set-countdown-seconds', 0);
                        $scope.disableButtons();
                        var params = {};
                        params.reqID = $scope.item.auctionID;
                        params.sessionID = userService.getUserToken();
                        params.userID = userService.getUserId();
                        params.newTicks = -1;
                        ; auctionsService.updatebidtime(params);
                        swal("Done!", "Requirement cancelled.", "success");
                    });
                }
            }]
        }
    })




    .directive('miniItemsTechevaluation', function () {
        return {
            restrict: 'EA',
            scope:
            {
                item: '=',
                answers: '=',
                callbackFn: '=method',
                callbackFn2: '=method2',
                callbackFn3: '=method3'

            },
            templateUrl: 'views/techevalviews/mini-item-techevaluation.html',
            controller: ['$scope', '$element', '$stateParams', '$attrs', '$timeout', 'userService', 'techevalService', 'fileReader',
                function ($scope, $element, $stateParams, $attrs, $timeout, userService, techevalService, fileReader) {
                    $scope.evalID = $stateParams.evalID == "" ? 0 : $stateParams.evalID;

                    $scope.order = $scope.answers ? ($scope.answers.length > 0 ? $scope.answers.indexOf($scope.item) + 1 : 0) : 0;
                    $scope.compID = userService.getUserCompanyId();
                    $scope.sessionID = userService.getUserToken();
                    $scope.isCustomer = userService.getUserType();


                    $scope.answers = [
                        //answer = $scope.answer
                    ];

                    $scope.directiveToggle = function (option, list, qid) {
                        $scope.callbackFn(option, list, qid);
                    };

                    $scope.directiveRadio = function (option, qid) {
                        $scope.callbackFn2(option, qid);
                    };

                    $scope.directiveDesc = function (option, qid) {
                        $scope.callbackFn3(option, qid);
                    };

                    $scope.doCheck = function (option, list) {
                        if (list.indexOf(option) > -1) {
                            return true;
                        }
                        else {
                            return false;
                        }
                    };

                    $scope.getFile = function () {

                        $scope.file = $("#attachment_" + $scope.item.answerID)[0].files[0];

                        fileReader.readAsDataUrl($scope.file, $scope)
                            .then(function (result) {
                                var bytearray = new Uint8Array(result);
                                $scope.item.attachment = $.makeArray(bytearray);
                                $scope.item.attachmentName = $scope.file.name;
                            });
                    };

                    $scope.getFile1 = function (id, itemid, ext) {
                        $scope.file = $("#" + id)[0].files[0];
                        fileReader.readAsDataUrl($scope.file, $scope)
                            .then(function (result) {
                                var bytearray = new Uint8Array(result);
                                var arrayByte = $.makeArray(bytearray);
                                var ItemFileName = $scope.file.name;
                                var index = _.indexOf($scope.answers, _.find($scope.answers, function (o) { return o.answerID == id; }));
                                var obj = $scope.item;
                                obj.attachment = arrayByte;
                                obj.attachmentName = ItemFileName;
                                $scope.answers.splice(index, 1, obj);
                            });
                    }

                }]
        }
    })
    .directive('miniItemsTechevalQuestions', function () {
        return {
            restrict: 'EA',
            scope:
            {
                item: '=',
                questions: '=',
                callbackFn: '=method',
                callbackFn2: '=method2',
                callbackFn3: '=method3'

            },
            templateUrl: 'views/techevalviews/mini-item-techeval-questions.html',
            controller: ['$scope', '$element', '$stateParams', '$attrs', '$timeout', 'userService', 'techevalService',
                function ($scope, $element, $stateParams, $attrs, $timeout, userService, techevalService) {
                    $scope.evalID = $stateParams.evalID == "" ? 0 : $stateParams.evalID;

                    $scope.order = $scope.questions ? ($scope.questions.length > 0 ? $scope.questions.indexOf($scope.item) + 1 : 0) : 0;
                    $scope.compID = userService.getUserCompanyId();
                    $scope.sessionID = userService.getUserToken();
                    $scope.isCustomer = userService.getUserType();


                    $scope.answers = [
                        //answer = $scope.answer
                    ];

                    $scope.deleteQuestion = function (item) {
                        $scope.callbackFn(item);
                    };

                    $scope.editQuestion = function (item) {
                        $scope.callbackFn2(item);
                    };

                    $scope.directiveDesc = function (option, qid) {
                        $scope.callbackFn3(option, qid);
                    };

                    $scope.doCheck = function (option, list) {
                        if (list.indexOf(option) > -1) {
                            return true;
                        }
                        else {
                            return false;
                        }
                    };

                }]
        }
    })
    .directive('prmConfigSelect', function ($timeout, $log, userService, auctionsService) { //http://jsfiddle.net/ndxbxrme/hprn3d9b/2/
        return {
            restrict: 'A',
            require: 'ngModel',
            scope: {
                configKey1: '=configkey',
                ngModel: '='
            },
            template: '<div class="prmDropdown" ng-click="open=!open" ng-class="{open:open}"><div ng-repeat="thing in list" style="top: {{($index + 1) * height}}px; -webkit-transition-delay: {{(list.length - $index) * 0.03}}s; z-index: {{list.length - $index}}" ng-hide="!open" ng-click="update(thing)" ng-class="{selected:selected===thing.configValue}"><span>{{thing.configText}}</span></div><span class="title" style="top: 0px; z-index: {{list.length + 1}}"><span>{{selectedText}}</span></span><span class="clickscreen" ng-hide="!open">&nbsp;</span></div>',
            replace: true,
            link: function (scope, elem, attrs, ngModel) {
                attrs.$observe('configkey', function (key) {

                    //scope.list = [
                    //    { compConfigID: 0, configKey: key, configValue: 'Select Value', configText: 'Select Value', CompID: 0 }
                    //];
                    scope.list = [];

                    auctionsService.GetCompanyConfiguration(userService.getUserCompanyId(), key, userService.getUserToken())
                        .then(function (response) {
                            //ngModel.$setViewValue("Select Value");
                            scope.list = scope.list.concat(response);
                            modalFunc();
                        });
                });

                scope.height = elem[0].offsetHeight;
                var modalFunc = function () {
                    scope.selected = ngModel.$modelValue;
                    $.each(scope.list, function (index, value) {
                        if (value.configValue == ngModel.$modelValue) {
                            scope.selectedText = value.configText;
                        }
                    });

                };

                scope.$watch('ngModel', modalFunc);
                scope.update = function (thing) {
                    ngModel.$setViewValue(thing.configValue);
                    ngModel.$render();
                };




            }
        };
    })
    .directive('numberInputVal', function () {
        return {
            restrict: 'A',
            require: 'ngModel',
            link: function (scope, element, attrs, ngModel) {
                var v = 0;
                scope.$watch(attrs.ngModel, function (val) {
                    v = val;
                });
                var maxValue = (attrs.max == undefined || attrs.max == null) ? 0 : Number(attrs.max);
                var minValue = (attrs.min === undefined || attrs.min === null) ? 0 : Number(attrs.min);
                v = (v === undefined || v === null) ? 0 : v;
                element.bind('blur', function () {
                    if (v == null || v == "" || isNaN(v) || v < minValue || v > maxValue) {
                        ngModel.$setViewValue(0);
                        ngModel.$render()
                    }
                    else {
                        ngModel.$setViewValue(v);
                        ngModel.$render()
                    }
                });
            }
        };
    })

    .directive('escClear', function () {
        return {
            restrict: 'A',
            require: 'ngModel',
            link: function (scope, element, attrs, ngModel) {
                var v = 0;
                scope.$watch(attrs.ngModel, function (val) {
                    v = val;
                });
                var maxValue = (attrs.max == undefined || attrs.max == null) ? 0 : Number(attrs.max);
                var minValue = (attrs.min === undefined || attrs.min === null) ? 0 : Number(attrs.min);
                v = (v === undefined || v === null) ? 0 : v;
                element.bind('keydown keypress', function (event) {
                    if (event.which === 27) { // 27 = esc key
                        ngModel.$setViewValue("");
                        ngModel.$render()
                        event.preventDefault();
                    }
                });
            }
        };
    })

    .directive('dateTimePicker', function () {
        return {
            restrict: 'E',
            replace: true,
            scope: {
                recipient: '='
            }, link: function (scope, element, attrs, itemCtrl) {
                var input = element.find('input');

                input.datetimepicker({
                    format: "mm/dd/yyyy hh:ii"
                });

                element.bind('blur keyup change', function () {
                    itemCtrl.startTime = input.val();
                });
            }
        }
    })
    .directive("owlCarousel", function () {
        return {
            restrict: 'E',
            transclude: false,
            link: function (scope) {
                scope.initCarousel = function (element) {
                    // provide any default options you want
                    var defaultOptions = {
                    };
                    var customOptions = scope.$eval($(element).attr('data-options'));
                    // combine the two options objects
                    for (var key in customOptions) {
                        defaultOptions[key] = customOptions[key];
                    }
                    // init carousel
                    $(element).owlCarousel(defaultOptions);
                };
            }
        };
    })
    .directive('fileModel', ['$parse', function ($parse) {
        return {
            restrict: 'A',
            link: function (scope, element, attrs) {
                var model = $parse(attrs.fileModel);
                var modelSetter = model.assign;

                element.bind('change', function () {
                    scope.$apply(function () {
                        modelSetter(scope, element[0].files[0]);
                    });
                });
            }
        };
    }])

    .directive('owlCarouselItem', [function () {
        return {
            restrict: 'A',
            transclude: false,
            link: function (scope, element) {
                // wait for the last item in the ng-repeat then call init
                if (scope.$last) {
                    scope.initCarousel(element.parent());
                }
            }
        };
    }])
    .directive("ngFileSelect", function () {
        // js/modules/template.js
        return {
            link: function ($scope, el) {                
                el.bind("change", function (e) { 
                    $scope.file = (e.srcElement || e.target).files[0];
                    let fileName = ($scope.file && $scope.file.name) ? $scope.file.name.toLowerCase() : '';
                    var allowedExtns = ['xlsx', 'xlx', 'pdf', 'txt', 'jpeg', 'jpg', 'png', 'gif', 'docx', 'doc', 'xls', 'xlt', 'ics','xml','eml'];
                    var contains = allowedExtns.some(substring => fileName.includes(substring));
                    if (contains) {
                        $scope.getFile();
                    }
                });
            }
        };
    }).directive("ngFileSelectDocs", function () {

        return {
            link: function ($scope, el) {
                el.bind("change", function (e) {
                    var id = e.currentTarget.attributes['id'].nodeValue;
                    var doctype = e.currentTarget.attributes['doctype'].nodeValue;
                    $scope.file = (e.srcElement || e.target).files[0];
                    var ext = $scope.file.name.split('.').pop();
                    $scope.getFile1(id, doctype, ext);
                })
            }
        }
    }).directive("ngFileSelectItems", function () {

        return {
            link: function ($scope, el) {
                el.bind("change", function (e) {
                    var allowedExtns = ['xlsx', 'xlx', 'pdf', 'txt', 'jpeg', 'jpg', 'png', 'gif', 'docx', 'doc'];
                    var id = e.currentTarget.attributes['id'].nodeValue;
                    var itemid = e.currentTarget.attributes['itemid'].nodeValue;
                    $scope.file = (e.srcElement || e.target).files[0];
                    let fileName = ($scope.file && $scope.file.name) ? $scope.file.name.toLowerCase() : '';
                    var contains = allowedExtns.some(substring => fileName.includes(substring));
                    if (contains) {
                        var ext = $scope.file.name.split('.').pop();
                        $scope.getFile1(id, itemid, ext);
                    } else {
                        swal("Error!", "System will not accept the extension with the File Name : " + fileName + ". Accepted extensions are " + allowedExtns, "error");
                    }
                });
            }
        };
    })

    .directive('pwCheck', [function () {
        return {
            require: 'ngModel',
            link: function (scope, elem, attrs, ctrl) {
                var firstPassword = '#' + attrs.pwCheck;
                elem.add(firstPassword).on('keyup', function () {
                    scope.$apply(function () {
                        var v = elem.val() === $(firstPassword).val();
                        ctrl.$setValidity('pwmatch', v);
                    });
                });
            }
        }
    }]).directive("ngUniqueBlade", function (userService) {
        return {
            restrict: 'A',
            require: 'ngModel',
            link: function (scope, element, attrs, ngModel) {
                element.bind('blur', function (e) {
                    if (!ngModel || !element.val()) return;
                    var type = attrs.ngUnique;
                    var currentValue = element.val();
                    userService.checkUniqueValue(type, currentValue)
                        .then(function (unique) {
                            //since the Ajax call was made
                            if (currentValue == element.val()) {
                                ngModel.$setValidity('unique', unique);
                                scope.$broadcast('show-errors-check-validity');
                            }
                        });
                });
            }
        }
    }).directive("ngUnique", function (userService) {
        return {
            restrict: 'A',
            require: 'ngModel',
            link: function (scope, element, attrs, ngModel) {
                element.bind('blur', function (e) {
                    if (!ngModel || !element.val()) return;
                    var type = attrs.ngUnique;
                    var currentValue = element.val();
                    userService.checkUniqueValue(type, currentValue)
                        .then(function (unique) {
                            //since the Ajax call was made
                            if (unique) {
                                ngModel.$setValidity('unique', unique);
                                scope.$broadcast('show-errors-check-validity');
                            }
                        });
                });
            }
        }
    }).directive("stringToNumber", function (userService) {
        return {
            require: 'ngModel',
            link: function (scope, element, attrs, ngModel) {
                ngModel.$parsers.push(function (value) {
                    return '' + value;
                });
                ngModel.$formatters.push(function (value) {
                    return parseFloat(value);
                });
            }
        }
    }).directive('hcChart', function () {
        return {
            restrict: 'E',
            template: '<div></div>',
            scope: {
                options: '='
            },
            link: function (scope, element) {
                Highcharts.chart(element[0], scope.options);
            }
        };
    })
    .directive('scrollToBottom', function ($timeout, $window) {
        return {
            scope: {
                scrollToBottom: "="
            },
            restrict: 'A',
            link: function (scope, element, attr) {
                scope.$watchCollection('scrollToBottom', function (newVal) {
                    if (newVal) {
                        $timeout(function () {
                            element[0].scrollTop = element[0].scrollHeight;
                        }, 0);
                    }

                });
            }
        };
    })
    .directive('whenScrolled', function () {
        return function (scope, elm, attr) {
            var raw = elm[0];
            elm.bind('scroll', function () {
                if (raw.scrollTop + raw.offsetHeight >= raw.scrollHeight) {
                    scope.$apply(attr.whenScrolled);
                }
            });
        };
    })


    .directive('indianCurrency', function ($timeout) {
        return {
            require: '?ngModel',
            priority: 0,
            link: function (scope, element, attrs, ngModelCtrl) {
                let isEditing = false;
                function formatIndianCurrency(value) {
                    let formattedValue = '';
                    if (isNaN(value) || value === undefined || value === null || value === '') {
                        return '';
                    }
                    if (value) {
                        value = value.toString();
                        let [integerPart, decimalPart] = value.split('.');

                        let lastThree = integerPart.substring(integerPart.length - 3);
                        let otherNumbers = integerPart.substring(0, integerPart.length - 3);

                        if (otherNumbers !== '') {
                            lastThree = ',' + lastThree;
                        }

                        let formattedIntegerPart = otherNumbers.replace(/\B(?=(\d{2})+(?!\d))/g, ",") + lastThree;

                        formattedValue = decimalPart ? `${formattedIntegerPart}.${decimalPart}` : formattedIntegerPart;
                    }
                    return formattedValue;
                    }
                

                // Update model on input
                element.on('focus', function () {
                    isEditing = true; // Set flag when editing starts
                }).on('blur', function () {
                    isEditing = false; // Reset flag when editing ends
                    let value = element.val();
                    ngModelCtrl.$setViewValue(value);
                    ngModelCtrl.$render();
                });

                // Format the view value only if not editing
                ngModelCtrl.$formatters.push(function (value) {
                    return isEditing ? parseFloat(value) : formatIndianCurrency(value);
                });

              
                // Initialize the value when the directive is linked
                ngModelCtrl.$render = function () {
                    element.val(ngModelCtrl.$viewValue);
                };
            }
        };
    })


    .directive('validNumber', function () {
        return {
            require: '?ngModel',
            priority: 1,
            link: function (scope, element, attrs, ngModelCtrl) {

                element.on('keydown', function (event) {
                    var keyCode=[]
                    if(attrs.allowNegative == "true")
                    { keyCode = [8, 9, 36, 35, 37, 39, 46, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 96, 97, 98, 99, 100, 101, 102, 103, 104, 105, 109, 110, 173, 190,189];
                    }
                    else{
                        var keyCode = [8, 9, 36, 35, 37, 39, 46, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 96, 97, 98, 99, 100, 101, 102, 103, 104, 105, 110, 173, 190];
                    }
              
              
                    if(attrs.allowDecimal == "false") {
                
                        var index = keyCode.indexOf(190);


                        if (index > -1) {
                            keyCode.splice(index, 1);
                        }
                
                    }
                        
                    if ($.inArray(event.which, keyCode) == -1) event.preventDefault();
                    else {
                        var oVal = ngModelCtrl.$modelValue || '';
                        if ($.inArray(event.which, [109, 173]) > -1 && oVal.indexOf('-') > -1) event.preventDefault();
                        else if ($.inArray(event.which, [110, 190]) > -1 && oVal.indexOf('.') > -1) event.preventDefault();
                    }
                })
                .on('blur', function () {

                    if (element.val() == '' || parseFloat(element.val()) == 0.0 || element.val() == '-') {
                        ngModelCtrl.$setViewValue('0.00');
                    }
                    else if (attrs.allowDecimal == "false") {
                        ngModelCtrl.$setViewValue(element.val());
                    }
                    else {
                        if (attrs.decimalUpto) {
                            var fixedValue = parseFloat(element.val()).toFixed(attrs.decimalUpto);
                        }
                        else { var fixedValue = parseFloat(element.val()).toFixed(scope.companyRoundingDecimalSetting); }
                        ngModelCtrl.$setViewValue(fixedValue);
                    }
                    
                    if (attrs.minValue && !isNaN(attrs.minValue) && parseFloat(attrs.minValue) && parseFloat(element.val()) < +attrs.minValue) {
                        console.log(attrs.minValue);
                        ngModelCtrl.$setViewValue(parseFloat(attrs.minValue));
                    }

                    if (attrs.maxValue && !isNaN(attrs.maxValue) && parseFloat(attrs.maxValue) && parseFloat(element.val()) > +attrs.maxValue) {
                        console.log(attrs.maxValue);
                        ngModelCtrl.$setViewValue(parseFloat(attrs.maxValue));
                    }

                    ngModelCtrl.$render();
                    scope.$apply();
                });

                ngModelCtrl.$parsers.push(function (text) {
                    var oVal = ngModelCtrl.$modelValue;
                    var nVal = ngModelCtrl.$viewValue;
                    //console.log(nVal);
                    if (parseFloat(nVal) != nVal) {

                        if (nVal === null || nVal === undefined || nVal == '' || nVal == '-') oVal = nVal;

                        ngModelCtrl.$setViewValue(oVal);
                        ngModelCtrl.$render();
                        return oVal;
                    }
                    else {
                        var decimalCheck = nVal.split('.');
                        if (!angular.isUndefined(decimalCheck[1])) {
                            if(attrs.decimalUpto)
                                decimalCheck[1] = decimalCheck[1].slice(0, attrs.decimalUpto);
                            else
                                decimalCheck[1] = decimalCheck[1].slice(0, scope.companyRoundingDecimalSetting);
                            nVal = decimalCheck[0] + '.' + decimalCheck[1];
                            if(attrs.allowDecimal == "false")
                            {
                                nVal = decimalCheck[0];
                            }
                        }

                        ngModelCtrl.$setViewValue(nVal);
                        ngModelCtrl.$render();
                        return nVal;
                    }
                });

                ngModelCtrl.$formatters.push(function (text) {
                    if (text == '0' || text == null && attrs.allowDecimal == "false") return '0';
                    else if (text == '0' || text == null && attrs.allowDecimal != "false" && attrs.decimalUpto == undefined) return '0.00';
                    else if (text == '0' || text == null && attrs.allowDecimal != "false" && attrs.decimalUpto != undefined) return parseFloat(0).toFixed(attrs.decimalUpto);
                    else if (attrs.allowDecimal != "false" && attrs.decimalUpto != undefined) return parseFloat(text).toFixed(attrs.decimalUpto);
                    else if (attrs.allowDecimal == "false") return parseInt(text);
                    else return parseFloat(text).toFixed(scope.companyRoundingDecimalSetting);
                });
            }
        };
    })

    .directive("angularStarRating", function () {
        return {
            restrict: 'EA',
            scope: {
                'value': '=value',
                'max': '=max',
                'hover': '=hover'
            },
            link: linkFunc,
            template: '<span>' +
                '<i ng-class="renderObj" ' +
                'ng-repeat="renderObj in renderAry" ' +
                'ng-click="setValue($index)" ' +
                'ng-mouseenter="changeValue($index)" >' +
                '</i>' +
                '</span>',
            replace: true
        }

        function linkFunc(scope, element, attrs, ctrl) {
            if (scope.max === undefined) {
                scope.max = 5; // default
            }
            console.log(scope.test);
            function renderValue() {
                scope.renderAry = [];
                for (var i = 0; i < scope.max; i++) {
                    if (i < scope.value) {
                        scope.renderAry.push({
                            'fa fa-star text-primary': true
                        });
                    } else {
                        scope.renderAry.push({
                            'far fa-star text-primary': true
                        });
                    }
                }
            }

            scope.setValue = function (index) {
                //  if (!scope.isReadonly && scope.isReadonly !== undefined) {
                scope.value = index + 1;
                // }
            };

            scope.changeValue = function (index) {
                if (scope.hover) {
                    scope.setValue(index);
                } else {
                    // !scope.changeOnhover && scope.changeOnhover != undefined
                }
            };

            scope.$watch('value', function (newValue, oldValue) {
                // console.log('oldValue -> newValue', oldValue, newValue);
                if (newValue) {
                    renderValue();
                }
            });

            scope.$watch('max', function (newValue, oldValue) {
                // console.log('oldValue -> newValue', oldValue, newValue);
                if (newValue) {
                    renderValue();
                }
            });
        }


    })


    
    .filter('ctime', function () {

        return function (jsonDate) {
            var date = '';
            if (jsonDate != undefined)
                date = parseInt(jsonDate.substr(6));
            return date;
        };

    })

    .filter('numToCurrency', function ($sce) {

        return function (amount, currentCurrency) {
            if (isNaN(amount)) {
                return amount;
            }

            if (currentCurrency && currentCurrency != 'INR') {
                var res = Number(amount.toFixed(2)).toLocaleString();
                var htmlCode = '&#36; ';
                if (currentCurrency == "USD") {
                    htmlCode = '<span class="fa fa-dollar"></span> ';//'&#36; ';
                }
                else if (currentCurrency == "EUR") {
                    htmlCode = '<span class="fa fa-eur"></span> ';//'&#8364; ';
                }
                else if (currentCurrency == "JPY") {
                    htmlCode = '<span class="fa fa-cny"></span> ';//'&#165; ';
                }
                else if (currentCurrency == "GBP") {
                    htmlCode = '<span class="fa fa-gbp"></span> ';//'&#163; ';
                }
                else if (currentCurrency == "CNY") {
                    htmlCode = '<span class="fa fa-cny"></span> ';//'&#165; ';
                }

                htmlCode = '';
                return $sce.trustAsHtml(htmlCode + res);
            }
            else {
                x = amount;
                if (!x || isNaN(x)) {
                    x = 0;
                }
                x = x.toString();
                var afterPoint = '';
                if (x.indexOf('.') > 0)
                    afterPoint = x.substring(x.indexOf('.'), x.length);
                x = Math.floor(x);
                x = x.toString();
                var lastThree = x.substring(x.length - 3);
                var otherNumbers = x.substring(0, x.length - 3);
                if (otherNumbers != '')
                    lastThree = ',' + lastThree;
                var res = otherNumbers.replace(/\B(?=(\d{2})+(?!\d))/g, ",") + lastThree + afterPoint;

                var htmlCode = '<span class="fa fa-rupee-sign"></span> ';//'&#36; ';
                htmlCode = '';
                return $sce.trustAsHtml(htmlCode + res);
            }
        };

    })

    .filter('roundedDecimalDisplay', function ($sce) {
        return function (num, decimalPoints) {
            var power = Math.pow(10, decimalPoints);
            return (Math.round(num * power) / power).toString();
        };
    });

    app.filter('inrCurrency', function () {
        return function (input) {
            if (!isNaN(input)) {
                //var output = Number(input).toLocaleString('en-IN');   <-- This method is not working fine in all browsers!           
                var result = input.toString().split('.');

                var lastThree = result[0].substring(result[0].length - 3);
                var otherNumbers = result[0].substring(0, result[0].length - 3);
                if (otherNumbers != '')
                    lastThree = ',' + lastThree;
                var output = otherNumbers.replace(/\B(?=(\d{2})+(?!\d))/g, ",") + lastThree;

                if (result.length > 1) {
                    output += "." + result[1];
                }

                return output;
            }
        }
    });


