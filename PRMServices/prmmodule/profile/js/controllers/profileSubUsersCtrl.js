angular = require('angular');

angular.module('profileModule')
.controller('profileSubUsersCtrl', ["$scope", "$state", "$filter", "$log", "$http", "domain", "growlService", "loginService", "profileService", function ($scope, $state, $filter, $log, $http, domain, growlService, loginService, profileService) {

	$scope.addnewuserobj = {};
	
	$scope.subUsers = [];
	$scope.inactiveSubUsers = [];
	
	$scope.showMessage = false;
	$scope.msg = '';
	
	$scope.userObj = {};
	
	profileService.getUserDataNoCache()
	.then(function(response){
		$scope.userObj = response;
	})
	
	$scope.addUser = 0;
	
	$scope.showAccess = false;
	$scope.UserName = '';

    $scope.userDetails = {
            achievements: "",
            assocWithOEM: false,
            clients: "",
            establishedDate: "01-01-1970",
            aboutUs: "",
            logoFile: "",
            logoURL: "",
            products: "",
            strengths: "",
            responseTime: "",
            oemCompanyName: "",
            oemKnownSince: "",
            workingHours: "",
            files: [],
            directors: "",
            address: "",
            dateshow: 0

    };

    $scope.currencies = [];
    $scope.timezones = [];


    $scope.addUserFunction = function(value)
    {
        $scope.addUser = value;
    };

    var loginUserData = loginService.getUserObj();

    $scope.isOTPVerified = loginUserData.isOTPVerified;
    $scope.isEmailOTPVerified = loginUserData.isEmailOTPVerified;

	$scope.getSubUserData = function() {
		profileService.getSubUsersData({ "userid": loginService.getUserId(), "sessionid": loginService.getUserToken() })
		.then(function (response) {
			
            $scope.subUsers = _.filter(response, ['isValid', true]);
            //$scope.subUsers = $filter('filter')(response, {isValid: true});
            $scope.inactiveSubUsers = _.filter(response, ['isValid', false]);
			//$scope.inactiveSubUsers = $filter('filter')(response, {isValid: false});            
		});
	}
	
	$scope.getSubUserData();
				
	$scope.deleteUser = function(userid){
            profileService.deleteUser({"userID": userid, "referringUserID": loginService.getUserId() })
            .then(function (response) {
                if(response.errorMessage != ""){
                    growlService.growl(response.errorMessage,"inverse");
                } else {
                    growlService.growl("User deleted Successfully", "inverse");
                    $scope.getSubUserData();
                }
            });
        }
        
        $scope.activateUser = function(userid){
            profileService.activateUser({"userID": userid, "referringUserID": loginService.getUserId() })
            .then(function (response) {
                if(response.errorMessage != ""){
                    growlService.growl(response.errorMessage,"inverse");
                } else {
                    growlService.growl("User Addes Successfully", "success");
                    $scope.getSubUserData();
                }
            });
        }
        
		$scope.AddNewUser = function () {
            if (isNaN($scope.addnewuserobj.phoneNum)) {
                $scope.showMessage = true;
                $scope.msg = $scope.getErrMsg("Please enter a valid Phone Number");
            } else {
                $scope.showMessage = false;
                $scope.msg = '';
            }
			$scope.addnewuserobj.userType = profileService.getUserType();
			$scope.addnewuserobj.username = $scope.addnewuserobj.email;
			$scope.addnewuserobj.companyName = $scope.userObj.institution;
			$scope.addnewuserobj.currency = $scope.addnewuserobj.currency.key;
			
			profileService.addnewuser($scope.addnewuserobj)
			.then(function (response) {
				if (response.errorMessage != "") {
					growlService.growl(response.errorMessage, "inverse");
				} else {
					growlService.growl("User added successfully.", "inverse");
					$scope.addUser = 0;
					$state.reload();
				}
			});                             
        };							

        $scope.loadAccess = function(action, userID, userName)
        {
            $scope.subuserentitlements = [];            
            $scope.showAccess = action;
            $scope.UserName = userName;
            if (action)
            {
                loginService.getuseraccess(userID, loginService.getUserToken())
               .then(function (responseObj) {
                   if (responseObj && responseObj.length > 0) {
                       $scope.subuserentitlements = responseObj;
                }              
                })
            }
        }
		
		$scope.loadAccess();
		
		$scope.validateAccess = function (isEntitled, accessType)
        {
            if (accessType == 'View Requirement' && isEntitled == false) {
                $scope.subuserentitlements.forEach(function (item, index) {
                    if (item.accessType == 'Requirement Posting') {
                        item.isEntitled = false;
                    }
                    if (item.accessType == 'Quotations Verification') {
                        item.isEntitled = false;
                    }
                    if (item.accessType == 'Negotiation Schedule') {
                        item.isEntitled = false;
                    }
                    if (item.accessType == 'Live Negotiation (Bidding Process)') {
                        item.isEntitled = false;
                    }
                    if (item.accessType == 'Po Generation') {
                        item.isEntitled = false;
                    }
                })
            }


            if (accessType == 'Requirement Posting' && isEntitled == true) {
                $scope.subuserentitlements.forEach(function (item, index) {
                    if (item.accessType == 'View Requirement') {
                            item.isEntitled = true;
                    }
                })
            }


            if (accessType == 'Quotations Verification' && isEntitled == true) {
                $scope.subuserentitlements.forEach(function (item, index) {
                    if (item.accessType == 'View Requirement') {
                        item.isEntitled = true;
                    }
                    if (item.accessType == 'Requirement Posting') {
                        item.isEntitled = true;
                    }
                })
            }
            

            if (accessType == 'Negotiation Schedule' && isEntitled == true) {
                $scope.subuserentitlements.forEach(function (item, index) {
                    if (item.accessType == 'View Requirement') {
                        item.isEntitled = true;
                    }
                    if (item.accessType == 'Requirement Posting') {
                        item.isEntitled = true;
                    }

                    if (item.accessType == 'Quotations Verification') {
                        item.isEntitled = true;
                    }
                })
            }
            

            if (accessType == 'Live Negotiation (Bidding Process)' && isEntitled == true) {
                $scope.subuserentitlements.forEach(function (item, index) {
                    if (item.accessType == 'View Requirement') {
                        item.isEntitled = true; 
                    }
                    if (item.accessType == 'Requirement Posting') {
                        item.isEntitled = true;
                    }

                    if (item.accessType == 'Quotations Verification') {
                        item.isEntitled = true;
                    }
                    if (item.accessType == 'Negotiation Schedule') {
                        item.isEntitled = true;
                    }                    
                })
            }
            
            
            if (accessType == 'Po Generation' && isEntitled == true) {
                $scope.subuserentitlements.forEach(function (item, index) {
                    if (item.accessType == 'View Requirement') {
                        item.isEntitled = true;
                    }
                    if (item.accessType == 'Requirement Posting') {
                        item.isEntitled = true;
                    }

                    if (item.accessType == 'Quotations Verification') {
                        item.isEntitled = true;
                    }
                    if (item.accessType == 'Negotiation Schedule') {
                        item.isEntitled = true;
                    }
                    if (item.accessType == 'Live Negotiation (Bidding Process)') {
                        item.isEntitled = true;
                    }
                })
            }                        
        }
		
		$scope.saveUserAccess = function()
        {         
            var params = {               
                "listUserAccess": $scope.subuserentitlements,
                "sessionID": loginService.getUserToken()
            };

            profileService.saveUserAccess(params)
               .then(function (response) {
                   if (response.errorMessage != '') {                                             
                       growlService.growl(response.errorMessage, "inverse");
                   }
                   else {

                       growlService.growl("Access Levels Defined Successfully.", "success");
                       $scope.showAccess = false;
                   }
               })
        }
				
        $scope.getKeyValuePairs = function (parameter) {

            loginService.getKeyValuePairs(parameter)
            .then(function (response) {
                if (parameter == "CURRENCY") {
                    $scope.currencies = response;

                } else if (parameter == "TIMEZONES") {
                    $scope.timezones = response;

                }

                $scope.selectedCurrency = $filter('filter')($scope.currencies, { value: response.currency });
                $scope.selectedCurrency = $scope.selectedCurrency[0];
                //$scope.selectedTimezone = $filter('filter')($scope.timezones, { value: response.timeZone });
                //$scope.selectedTimezone = $scope.selectedTimezone[0];
            })

        }

        $scope.getKeyValuePairs('CURRENCY');
        $scope.getKeyValuePairs('TIMEZONES');

        $scope.callGetUserDetails = function () {
            $log.info("IN GET USER DETAILS");
            profileService.getProfileDetails({ "userid": loginService.getUserId(), "sessionid": loginService.getUserToken() })
                .then(function (response) {
                    $scope.userStatus = "registered";
                    if (response != undefined) {
                        $scope.userDetails = response;                        

                        $http({
                            method: 'GET',
                            url: domain + 'getcategories?userid=' + loginService.getUserId() + '&sessionid=' + loginService.getUserToken(),
                            encodeURI: true,
                            headers: { 'Content-Type': 'application/json' }
                        }).then(function (response) {
                            if (response && response.data) {
                                if (response.data.length > 0) {
                                    
                                    //$scope.totalSubcats = $filter('filter')(response.data, { category: $scope.userDetails.category });
                                    //$scope.selectedCurrency = $filter('filter')($scope.currencies, { value: response.currency });

                                    response.data.forEach(function (item, index) {
                                        if (item.category == $scope.userDetails.category) {
                                            $scope.totalSubcats.push(item);
                                        }                                        
                                    })

                                    $scope.currencies.forEach(function (item, index) {
                                        if (item.value == response.currency) {
                                            $scope.selectedCurrency.push(item);
                                        }                                        
                                    })

                                    $log.info("IN GET USER DETAILS " + $scope.selectedCurrency);
                                    $scope.selectedCurrency = $scope.selectedCurrency[0];
                                    if ($scope.userDetails.subcategories && $scope.userDetails.subcategories.length > 0) {
                                        for (i = 0; i < $scope.userDetails.subcategories.length; i++) {
                                            for (j = 0; j < $scope.totalSubcats.length; j++) {
                                                if ($scope.userDetails.subcategories[i].id == $scope.totalSubcats[j].id) {
                                                    $scope.totalSubcats[j].ticked = true;
                                                }
                                            }
                                        }
                                    }
                                }
                            } else {
                                //console.log(response.data[0].errorMessage);
                            }
                        }, function (result) {
                            //console.log("there is no current auctions");
                        });
                        if ($scope.userDetails.subcategories && $scope.userDetails.subcategories.length > 0) {
                            for (i = 0; i < $scope.userDetails.subcategories.length; i++) {
                                $scope.subcategories += $scope.userDetails.subcategories[i].subcategory + ";";
                            }
                        }
                        var data = response.establishedDate;
                        var date = new Date(parseInt(data.substr(6)));
                        $scope.userDetails.establishedDate = date.getDate() + '/' + (date.getMonth() + 1) + '/' + date.getFullYear();
                        
                        var today = new Date();
                        var todayDate = today.getDate() + '/' + (today.getMonth() + 1) + '/' + today.getFullYear();
                        $scope.userDetails.dateshow = 0;
                        if($scope.userDetails.establishedDate == todayDate){
                            $scope.userDetails.dateshow = 1;
                        } 
                    }

                    $log.info($scope.userDetails);
                });       
        }

        $scope.callGetUserDetails();


}]);