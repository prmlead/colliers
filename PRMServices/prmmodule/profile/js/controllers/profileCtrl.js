angular = require('angular');

angular.module('profileModule')
.controller('profileCtrl', ["$scope", "$state", "loginService", "profileService", function ($scope, $state, loginService, profileService) {

    $scope.showAddNewReq = function () {
        if (loginService.getUserType() == "CUSTOMER") {
            return true;
        } else {
            return false;
        }
    }

    $scope.showIsSuperUser = function () {
        if (loginService.getUserObj().isSuperUser) {
            return true;
        } else {
            return false;
        }
    }
    
	$scope.stateChangeFunction = function(page)
    {
        $state.go(page);
    }

}]);
		