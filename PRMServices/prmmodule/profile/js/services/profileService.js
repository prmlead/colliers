angular = require('angular');

angular.module('profileModule')	
	.service('profileService', ["$http", "store",  "domain", "version", "growlService", "loginService", function ($http, store, domain, version, growlService, loginService) {
		var profileService = this;
		
		profileService.getUserDataNoCache = function(){
			var params = {
				userid: loginService.getUserId(),
				sessionid: loginService.getUserToken()
			}
			return $http({
				method: 'GET',
				url: domain + 'getuserinfo?userid=' + params.userid + '&sessionid=' + params.sessionid,
				encodeURI: true,
				headers: { 'Content-Type': 'application/json' }
				//data: params
			}).then(function (response) {

				store.set('currentUser', response.data);
				loginService.setCurrentUser();
				var list = {};
				if (response && response.data) {
					list = response.data;
					return list;
				} else {
					//console.log(response.data.errorMessage);
				}
			}, function (result) {
				//console.log(result);
			});
    	}

		profileService.getProfileDetails = function (params) {
			return $http({
				method: 'GET',
				url: domain + 'getuserdetails?userid=' + params.userid + '&sessionid=' + params.sessionid,
				encodeURI: true,
				headers: { 'Content-Type': 'application/json' }
				//data: params
			}).then(function (response) {
				var list = {};
				if (response && response.data) {
					list = response.data;
					return list;
				} else {
					//console.log(response.data.errorMessage);
				}
			}, function (result) {
				//console.log(result);
			});
			//$state.go('pages.profile.profile-about');
    	}

		profileService.updateVerified = function (verified) {
        	store.set('credverified', verified);
    	}

		profileService.updatePassword = function(params){
			return $http({
				method: 'POST',
				url: domain + 'changepassword',
				headers: { 'Content-Type': 'application/json' },
				encodeURI: true,
				data: params
			}).then(function (response) {
				return response.data;
			});
    	}

		profileService.updateUser = function (params) {
			var params1 = {
				"user": {
					"userID": params.userID,
					"firstName": params.firstName,
					"lastName": params.lastName,
					"email": params.email,
					"phoneNum": params.phoneNum,
					"sessionID": params.sessionID,
					"isOTPVerified": params.isOTPVerified,
					"credentialsVerified": params.credentialsVerified,
					"errorMessage": params.errorMessage,
					"logoFile": params.logoFile ? params.logoFile : null,
					"logoURL": params.logoURL ? params.logoURL : "",
					"aboutUs": params.aboutUs ? params.aboutUs : "",
					"achievements": params.achievements ? params.achievements : "",
					"assocWithOEM": params.assocWithOEM ? params.assocWithOEM : false ,
					"assocWithOEMFile": params.assocWithOEMFile ? params.assocWithOEMFile : null ,                
					"assocWithOEMFileName": params.assocWithOEMFileName ? params.assocWithOEMFileName : "" ,                
					"clients": params.clients ? params.clients : "",
					"establishedDate": ("establishedDate" in params) ? params.establishedDate :'/Date(634334872000+0000)/',
					"products": params.products ? params.products : "",
					"strengths": params.strengths ? params.strengths : "",
					"responseTime": params.responseTime ? params.responseTime : "",
					"oemCompanyName": params.oemCompanyName ? params.oemCompanyName : "",
					"oemKnownSince": params.oemKnownSince? params.oemKnownSince : "",
					"files": [],
					"profileFile": params.profileFile ? params.profileFile : null ,                
					"profileFileName": params.profileFileName ? params.profileFileName : "",
					"workingHours": params.workingHours ? params.workingHours : "",
					"directors": params.directors ? params.directors : "",
					"address": params.address ? params.address : "",
					"subcategories": params.subcategories
				}
			}       
			return $http({
				method: 'POST',
				url: domain + 'updateuserinfo',
				headers: { 'Content-Type': 'application/json' },
				encodeURI: true,
				data: params1
			}).then(function (response) {
				if (response && response.data && response.data.errorMessage == "" && response.data.objectID != 0) {
					store.set('sessionid', response.data.sessionID);
					store.set('verified', 0);
					store.set('emailverified', 0);
					store.set('currentUser', response.data.userInfo);
					loginService.setCurrentUser();
					if (response.data.userInfo.isOTPVerified == 1 && response.data.userInfo.credentialsVerified == 1) {
						store.set('verified', 1);
					} if (response.data.userInfo.isEmailOTPVerified == 1) {
						store.set('emailverified', 1);
					}
					if(response.data.userInfo.credentialsVerified == 1){
						store.set('credverified', 1);
					} else {
						store.set('credverified', 0);
					}
					if (response.data.objectID > 0) {

						growlService.growl('User data has updated Successfully!', 'inverse');						
						if(params.ischangePhoneNumber == 1){							
							//$state.go('login');
							return responce.data;
						}else{
							//$state.reload();
						}
						return "true";
					} else {
						return "false";
					}
				} else if (response && response.data && response.data.errorMessage != "") {
					swal('Error', response.data.errorMessage, 'error');					
					return response.data.errorMessage;
					
				} else {
					return "Update failed";
				}
			}, function (result) {
				return "Update failed";
			});
    	}

		profileService.getSubUsersData = function (params) {
			return $http({
				method: 'GET',
				url: domain + 'getsubuserdata?userid=' + params.userid + '&sessionid=' + params.sessionid,
				encodeURI: true,
				headers: { 'Content-Type': 'application/json' }				
				}).then(function (response) {
					return response.data;
			});
		}

		profileService.getUserType = function () {
			return loginService.getUserObj().userType;
		};


		profileService.addnewuser = function (addnewuserobj) {
			var referringUserID = 0;
			referringUserID = loginService.getUserId();
			var params = {
				"register": {
					"firstName": addnewuserobj.firstName,
					"lastName": addnewuserobj.lastName,
					"email": addnewuserobj.email,
					"phoneNum": addnewuserobj.phoneNum,
					"username": addnewuserobj.email,
					"password": addnewuserobj.phoneNum,
					"companyName": addnewuserobj.companyName ? addnewuserobj.companyName : "",
					"isOTPVerified": 0,
					"category": addnewuserobj.category ? addnewuserobj.category : "",
					"userType": addnewuserobj.userType,
					"panNumber": "",
					"stnNumber": "",
					"vatNumber": "",
					"referringUserID": referringUserID,
					"knownSince": "",
					"errorMessage": "",
					"sessionID": "",
					"userID": 0,
					"department": "",
					"currency": addnewuserobj.currency
				}
			};
			return $http({
				method: 'POST',
				url: domain + 'register',
				encodeURI: true,
				headers: { 'Content-Type': 'application/json' },
				data: params
			}).then(function (response) {
				return response.data;
			});
		};

		profileService.saveUserAccess = function (params) {
			return $http({
				method: 'POST',
				url: domain + 'saveuseraccess',
				headers: { 'Content-Type': 'application/json' },
				encodeURI: true,
				data: params
			}).then(function (response) {
				return response.data;
			});
		}

		profileService.deleteUser = function(params){
			params.sessionID = loginService.getUserToken();
			return $http({
				method: 'POST',
				data: params,
				url: domain + 'deleteuser',
				headers: { 'Content-Type': 'application/json' }
			}).then(function (response) {
				return response.data;
			});
		}

		profileService.activateUser = function(params){
			params.sessionID = loginService.getUserToken();
			return $http({
				method: 'POST',
				data: params,
				url: domain + 'activateuser',
				headers: { 'Content-Type': 'application/json' }
			}).then(function (response) {
				return response.data;
			});
		}


		profileService.getactiveleads= function(params){
            return $http({
                method: 'GET',
                url: domain+'getactiveleads?userid='+params.userid+"&sessionid="+params.sessionid,
                encodeURI: true,
                headers: {'Content-Type': 'application/json'},
                data: params
            }).then(function(response) {
                var list = {};
                if(response && response.data ){
                     list=response.data;
                     return list;
                }else{
                    //console.log(response.data.errorMessage);
                }
            }, function(result) {
                //console.log(result);
                //console.log("there is no your Negotiations");
            });
        };

		profileService.getmyAuctions= function(params){
            return $http({
                method: 'GET',
                url: domain+'getmyauctions?userid='+params.userid+"&sessionid="+params.sessionid,
                encodeURI: true,
                headers: {'Content-Type': 'application/json'},
                data: params
            }).then(function(response) {
                var list = {};
                if(response && response.data ){
                     list=response.data;
                     return list;
                }else{
                    //console.log(response.data.errorMessage);
                }
            }, function(result) {
                //console.log(result);
                //console.log("there is no your Negotiations");
            });
        };

		profileService.GetCompanyVendors = function (params) {
			return $http({
				method: 'GET',
				url: domain + 'getcompanyvendors?userid=' + params.userID + '&sessionid=' + params.sessionID,
				encodeURI: true,
				headers: { 'Content-Type': 'application/json' }
				//data: params
			}).then(function (response) {
				var list = {};
				if (response && response.data) {
					list = response.data;
					return list;
				} else {
				}
			}, function (result) {
			});
		}

		profileService.activatecompanyvendor = function (params) {
			params.sessionID = loginService.getUserToken();
			return $http({
				method: 'POST',
				data: params,
				url: domain + 'activatecompanyvendor',
				headers: { 'Content-Type': 'application/json' }
			}).then(function (response) {
				return response.data;
			});
		}

		profileService.AddVendorsExcel = function (params) {
			return $http({
				method: 'POST',
				url: domain + 'importentity',
				headers: { 'Content-Type': 'application/json' },
				encodeURI: true,
				data: params
			}).then(function (response) {
				return response.data;
			});
		}

		profileService.isnegotiationrunning = function () {
			var data = {
				"userID": loginService.getUserId(),
				"sessionID": loginService.getUserToken()
			};
			return $http({
				method: 'POST',
				url: domain + 'isnegotiationrunning',
				headers: { 'Content-Type': 'application/json' },
				encodeURI: true,
				data: data
			}).then(function (response) {
				return response;
			});
		};

		profileService.updateUserBasicInfo = function (params) {			
			return $http({
				method: 'POST',
				url: domain + 'updateuserbasicinfo',
				headers: { 'Content-Type': 'application/json' },
				encodeURI: true,
				data: params
			}).then(function (response) {
				if (response && response.data && response.data.errorMessage == "" && response.data.objectID != 0) {
					store.set('sessionid', response.data.sessionID);
					store.set('verified', 0);
					store.set('emailverified', 0);
					store.set('currentUser', response.data.userInfo);
					loginService.setCurrentUser();
					if (response.data.userInfo.isOTPVerified == 1 && response.data.userInfo.credentialsVerified == 1) {
						store.set('verified', 1);
					} if (response.data.userInfo.isEmailOTPVerified == 1) {
						store.set('emailverified', 1);
					}
					if(response.data.userInfo.credentialsVerified == 1){
						store.set('credverified', 1);
					} else {
						store.set('credverified', 0);
					}
					if (response.data.objectID > 0) {

						growlService.growl('User data has updated Successfully!', 'inverse');
						//loginService.getProfileDetails();
						//$state.go('pages.profile.profile-about');
						if(params.ischangePhoneNumber == 1){
							//loginService.logout();
							//$state.go('login');
							return responce.data;
						}else{
							//$state.reload();
						}
						return "true";
					} else {
						return "false";
					}
				} else if (response && response.data && response.data.errorMessage != "") {
					swal('Error', response.data.errorMessage, 'error');
					//store.set('emailverified', 1);
					return response.data.errorMessage;
					
				} else {
					return "Update failed";
				}
			}, function (result) {
				return "Update failed";
			});
		};

		profileService.updateUserProfileInfo = function (params) {			
			return $http({
				method: 'POST',
				url: domain + 'updateuserprofileinfo',
				headers: { 'Content-Type': 'application/json' },
				encodeURI: true,
				data: params
			}).then(function (response) {
				if (response && response.data && response.data.errorMessage == "" && response.data.objectID != 0) {
					if (response.data.objectID > 0) {
						growlService.growl('User data has updated Successfully!', 'inverse');
						return "true";
					} else {
						return "false";
					}
				} else if (response && response.data && response.data.errorMessage != "") {
					swal('Error', response.data.errorMessage, 'error');
					return response.data.errorMessage;
				} else {
					return "Update failed";
				}
			}, function (result) {
				return "Update failed";
			});
		};

		profileService.verifyEmailOTP = function (otpobj) {
        var data = {
            "OTP": otpobj,
            "userID": loginService.getUserId(),
            "email": otpobj.email ? otpobj.email : ""
        };
        return $http({
            method: 'POST',
            url: domain + 'verifyemailotp',
            headers: { 'Content-Type': 'application/json' },
            encodeURI: true,
            data: data
        }).then(function (response) {
            if (response.data.sessionID != "" || response.data.userInfo.sessionID != "") {
                store.set('sessionid', response.data.sessionID != "" ? response.data.sessionID : response.data.userInfo.sessionID);
                if (response.data.userInfo.isEmailOTPVerified == 1) {
                    store.set('emailverified', 1);
                } else {
                    store.set('emailverified', 0);
                }
                if (response.data.userInfo.isOTPVerified == 1) {
                    store.set('verified', 1);
                } else {
                    store.set('verified', 0);
                }
                if(response.data.userInfo.credentialsVerified == 1){
                    store.set('credverified', 1);
                } else {
                    store.set('credverified', 0);
                }
                store.set('currentUser', response.data.userInfo);
                loginService.setCurrentUser();
                //$state.go('pages.profile.profile-about');
            } else {
                swal('Warning', "The OTP is not valid. Please enter the valid OTP sent to your Email", "warning");
            }
            /*if(response.data.userInfo.isOTPVerified==1 && response.data.userInfo.credentialsVerified==1){
              store.set('verified',response.data.userInfo.credentialsVerified);
              $state.go('home');
              growlService.growl('Welcome to PRM360', 'inverse');                                                
            }*/
            return response.data;
        });
    };

	return profileService;		
}]);