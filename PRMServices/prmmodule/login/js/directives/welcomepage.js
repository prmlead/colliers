angular = require('angular');

angular.module('loginModule')
    .directive('welcomePage', function () {
        return {
            templateUrl: '/partials/welcomepage.html'
        };
    });