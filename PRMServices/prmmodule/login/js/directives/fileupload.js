angular = require('angular');

angular.module('loginModule')
.directive("ngFileSelect",function(){

  return {
    link: function($scope,el){      
      el.bind("change", function(e){      
        $scope.file = (e.srcElement || e.target).files[0];
        $scope.getFile();
      })      
    }    
  }
}).directive("ngFileSelectDocs",function(){

  return {
    link: function($scope,el){
      el.bind("change", function(e){
        var id=e.currentTarget.attributes['id'].nodeValue;
        var doctype=e.currentTarget.attributes['doctype'].nodeValue;
        $scope.file = (e.srcElement || e.target).files[0];
        var ext = $scope.file.name.split('.').pop();  
        $scope.getFile1(id,doctype,ext);
      })      
    }    
  }
}).directive("ngFileSelectItems", function () {

    return {
        link: function ($scope, el) {
            el.bind("change", function (e) {
                var id = e.currentTarget.attributes['id'].nodeValue;
                var itemid = e.currentTarget.attributes['itemid'].nodeValue;
                $scope.file = (e.srcElement || e.target).files[0];
                var ext = $scope.file.name.split('.').pop();
                $scope.getFile1(id, itemid, ext);
            })
        }
    }
})