angular = require('angular');
store = require('angular-storage');
jQuery = require('jquery');
//growl = require('growl');
require('notifyjs');
require('notify-send');
var angularNotify = require("angular-notify");
require('angular-ui-router');
require('ng-dialog');
require('moment');
require('sweetalert');
window._ = require('lodash');
require('angular-ui-bootstrap');
require('../dist/templateCachePartials');

var app = angular.module('loginModule', ['ui.router', 'loginPartials', 'angular-storage', 'ngDialog', 'angular-notify']);

require('./config/index.js');
require('./services/index.js');
require('./directives/index.js');
require('./controllers/index.js');
