angular = require('angular');

angular.module('loginModule')	
	.service('loginService', ["$http","store","$state","$rootScope","domain", "version", function($http,store,$state,$rootScope,domain, version) {
		var loginService = this;
		var responseObj = {};
		responseObj.objectID = 0;
		loginService.userData = { currentUser: null };
		loginService.isValidSession = function (sessionid) {
			return $http({
				method: 'GET',
				url: domain + 'issessionvalid?sessionid=' + sessionid,
				encodeURI: true,
				headers: { 'Content-Type': 'application/json' }
			}).then(function (response) {
				if (response && response.data) {
					responseObj = response.data;
					return responseObj;
				} else {
					return responseObj;
				}
			}, function (result) {
				//console.log("date error");
			});
		}

		loginService.getErrMsg = function (message) {
            if (typeof message == 'string') {
                return message;
            } else if (typeof message == 'object') {
                var result = "";
                angular.forEach(message, function (value, key) {
                    result += value[0] + '<br />';
                });
                return result;
            }
        }

		loginService.resendotp = function (userid) {
			var data = {
				userID: userid
			};
			return $http({
				method: 'POST',
				url: domain + 'sendotp',
				headers: { 'Content-Type': 'application/json' },
				encodeURI: true,
				data: data
			}).then(function (response) {
				swal("Done!", "OTP send to your registered Mobile No.", "success");
			});
		};

		loginService.getUserId = function () {
			return loginService.getUserObj().userID;
		};

		loginService.getUserToken = function () {
			return store.get('sessionid');
		};

		loginService.getUserCompanyId = function () {
        	return self.getUserObj().companyId;
    	};

		loginService.getKeyValuePairs = function (params) {
            return $http({
                method: 'GET',
                url: domain + 'getkeyvaluepairs?parameter=' + params,
                encodeURI: true,
                headers: { 'Content-Type': 'application/json' }
                //data: params
            }).then(function (response) {
                var list = [];
                if (response && response.data) {
                    if (response.data.length > 0) {
                        list = response.data;
                    }
                    return list;
                } else {
                    //console.log(response.data[0].errorMessage);
                }
            }, function (result) {
                //console.log("there is no categories");
            });
        };

		loginService.getCategories=function (params) {
            return $http({
                method: 'GET',
                url: domain+'getcategories?userid=' + (params ? params : -1),
                encodeURI: true,
                headers: {'Content-Type': 'application/json'}
                //data: params
            }).then(function(response) {
                var list=[];
                if(response && response.data ){
                     if(response.data.length>0){
                        list=response.data;   
                     }
                     return list;
                }else{
                    //console.log(response.data[0].errorMessage);
                }
            }, function(result) {
                //console.log("there is no categories");
            });
        };

		loginService.isLoggedIn = function () {
        	return (loginService.getUserToken() && loginService.getOtpVerified() && loginService.getDocsVerified()) ? true : false;
  		};

		loginService.getOtpVerified = function () {
			return loginService.getUserObj().isOTPVerified;
		};

		loginService.getEmailOtpVerified = function () {
			return loginService.getUserObj().isEmailOTPVerified;
		};

		loginService.getDocsVerified = function () {
			return loginService.getUserObj().credentialsVerified;
		};

		loginService.removeUserToken = function () {
			store.remove('sessionid');
		};

		loginService.userregistration = function (register) {
			var params = {
				"userInfo": {
					"userID": "0",
					"firstName": register.firstName,
					"lastName": register.lastName,
					"email": register.email,
					"phoneNum": register.phoneNum,
					"username": register.username,
					"password": register.password,
					"birthday": '/Date(634334872000+0000)/',
					"userType": "CUSTOMER",
					"isOTPVerified": 0,
					"category": "",
					"institution": ("institution" in register) ? register.institution : "",
					"addressLine1": "",
					"addressLine2": "",
					"addressLine3": "",
					"city": "",
					"state": "",
					"country": "",
					"zipCode": "",
					"addressPhoneNum": "",
					"extension1": "",
					"extension2": "",
					"userData1": "",
					"registrationScore" : 0,
					"errorMessage": "",
					"sessionID": ""
				}
			};

			return $http({
				method: 'POST',
				url: domain + 'registeruser',
				encodeURI: true,
				headers: { 'Content-Type': 'application/json' },
				data: params
			}).then(function (response) {
				if (response && response.data && response.data.errorMessage == "" && response.data.objectID != 0) {
					store.set('sessionid', response.data.sessionID);
					store.set('verified', 0);
					store.set('emailverified', 0);
					if(response.data.userInfo.credentialsVerified == 1){
						store.set('credverified', 1);
					} else {
						store.set('credverified', 0);
					}
					store.set('currentUser', response.data.userInfo);
					loginService.setCurrentUser();
					return response.data;
					//$state.go('home');
					//growlService.growl('Welcome to PRM360, To add new requirement Please use "+" at the Bottom.', 'inverse');
				} else if (response && response.data && response.data.errorMessage != "") {
					return response.data.errorMessage;
				} else {
					return "Registeration failed";
				}
			}, function (result) {
				return "Registeration failed";
			});
		};

		loginService.setCurrentUser = function () {
			loginService.userData.sessionid = loginService.getUserToken();
			loginService.userData.currentUser = loginService.getUserObj();
		};

		loginService.vendorregistration = function (vendorregisterobj) {
			var vendorcat = [];
			vendorcat.push(vendorregisterobj.category);

			var params = {
				"vendorInfo": {
					"firstName": vendorregisterobj.firstName,
					"lastName": vendorregisterobj.lastName,
					"email": vendorregisterobj.email,
					"contactNum": vendorregisterobj.phoneNum,
					"username": vendorregisterobj.username,
					"password": vendorregisterobj.password,
					"institution": vendorregisterobj.institution ? vendorregisterobj.institution : "",
					"rating": 1,
					"isOTPVerified": 0,
					"category": vendorcat,
					"panNum": ("panno" in vendorregisterobj) ? vendorregisterobj.panno : "",
					"serviceTaxNum": ("taxno" in vendorregisterobj) ? vendorregisterobj.taxno : "",
					"vatNum": ("vatno" in vendorregisterobj) ? vendorregisterobj.vatno : "",
					"referringUserID": 0,
					"knownSince": "",
					"errorMessage": "",
					"sessionID": ""
				}
			};
			return $http({
				method: 'POST',
				url: domain + 'addnewvendor',
				encodeURI: true,
				headers: { 'Content-Type': 'application/json' },
				data: params
			}).then(function (response) {
				if (response && response.data && response.data.errorMessage == "") {
					store.set('sessionid', response.data.sessionID);
					store.set('currentUser', response.data);
					store.set('verified', 0);
					store.set('emailverified', 0);
					if(response.data.credentialsVerified == 1){
						store.set('credverified', 1);
					} else {
						store.set('credverified', 0);
					}
					loginService.setCurrentUser();
					return response.data;
					//growlService.growl('Welcome to PRM360, Please go thourgh with your Auctions', 'inverse');
					//return "Vendor Registration Successfully Completed.";
				} else if (response && response.data && response.data.errorMessage != "") {
					return response.data.errorMessage;
				} else {
					return "Vendor Registration Failed.";
				}
			});
		};

		loginService.vendorregistration1 = function (vendorregisterobj) {
			var vendorcat = [];
			vendorcat.push(vendorregisterobj.category);
			////console.log(vendorregisterobj.role);
			var params = {
				"register": {
					"firstName": vendorregisterobj.firstName,
					"lastName": vendorregisterobj.lastName,
					"email": vendorregisterobj.email,
					"phoneNum": vendorregisterobj.phoneNum,
					"username": vendorregisterobj.username,
					"password": vendorregisterobj.password,
					"companyName": vendorregisterobj.institution ? vendorregisterobj.institution : "",
					"isOTPVerified": 0,
					"category": vendorregisterobj.category ? vendorregisterobj.category : "",
					"userType": vendorregisterobj.role,
					"panNumber": ("panno" in vendorregisterobj) ? vendorregisterobj.panno : "",
					"stnNumber": ("taxno" in vendorregisterobj) ? vendorregisterobj.taxno : "",
					"vatNumber": ("vatno" in vendorregisterobj) ? vendorregisterobj.vatno : "",
					"referringUserID": 0,
					"knownSince": "",
					"errorMessage": "",
					"sessionID": "",
					"userID": 0,
					"department": "",
					"currency": vendorregisterobj.currency,
					"timeZone": vendorregisterobj.timeZone,
					"subcategories": vendorregisterobj.subcategories ? vendorregisterobj.subcategories : []
					
				}
			};
			return $http({
				method: 'POST',
				url: domain + 'register',
				encodeURI: true,
				headers: { 'Content-Type': 'application/json' },
				data: params
			}).then(function (response) {
				if (response && response.data && response.data.errorMessage == "") {
					store.set('sessionid', response.data.sessionID);
					store.set('currentUser', response.data.userInfo);
					store.set('verified', 0);
					store.set('emailverified', 0);
					if(response.data.credentialsVerified == 1){
						store.set('credverified', 1);
					} else {
						store.set('credverified', 0);
					}
					loginService.setCurrentUser();
					return response.data;
					//growlService.growl('Welcome to PRM360, Please go thourgh with your Auctions', 'inverse');
					//return "Vendor Registration Successfully Completed.";
				} else if (response && response.data && response.data.errorMessage != "") {
					return response.data.errorMessage;
				} else {
					return "Vendor Registration Failed.";
				}
			});
		};

		loginService.verifyOTP = function (otpobj) {
			var data = {
				"OTP": otpobj,
				"userID": loginService.getUserId(),
				"phone": otpobj.phone ? otpobj.phone : ""
			};
			return $http({
				method: 'POST',
				url: domain + 'verifyotp',
				headers: { 'Content-Type': 'application/json' },
				encodeURI: true,
				data: data
			}).then(function (response) {
				if (response.data.sessionID != "" || response.data.userInfo.sessionID != "") {
					store.set('sessionid', response.data.sessionID != "" ? response.data.sessionID : response.data.userInfo.sessionID);
					if (response.data.userInfo.isOTPVerified == 1 && response.data.userInfo.credentialsVerified == 1) {
						store.set('verified', 1);
					} else {
						store.set('verified', 0);
					}
					if (response.data.userInfo.isEmailOTPVerified == 1) {
						store.set('emailverified', 1);
					} else {
						store.set('emailverified', 0);
					}
					if(response.data.userInfo.credentialsVerified == 1){
						store.set('credverified', 1);
					} else {
						store.set('credverified', 0);
					}
					store.set('currentUser', response.data.userInfo);
					loginService.setCurrentUser();
					$state.go('pages.profile.profile-about');
				} else {
					swal('Warning', "The OTP is not valid. Please enter the valid OTP sent to your mobile number", "warning");
				}
				/*if(response.data.userInfo.isOTPVerified==1 && response.data.userInfo.credentialsVerified==1){
				store.set('verified',response.data.userInfo.credentialsVerified);
				$state.go('home');
				growlService.growl('Welcome to PRM360', 'inverse');                                                
				}*/
				return response.data;
			});
		};

		loginService.getUserObj = function () {
			if (!loginService.userData.currentUser || !loginService.userData.currentUser.id) {
				loginService.userData.currentUser = store.get('currentUser');
			}
			return loginService.userData.currentUser || {};
		};

		loginService.updateUser = function (params) {
			var params1 = {
				"user": {
					"userID": params.userID,
					"firstName": params.firstName,
					"lastName": params.lastName,
					"email": params.email,
					"phoneNum": params.phoneNum,
					"sessionID": params.sessionID,
					"isOTPVerified": params.isOTPVerified,
					"credentialsVerified": params.credentialsVerified,
					"errorMessage": params.errorMessage,
					"logoFile": params.logoFile ? params.logoFile : null,
					"logoURL": params.logoURL ? params.logoURL : "",
					"aboutUs": params.aboutUs ? params.aboutUs : "",
					"achievements": params.achievements ? params.achievements : "",
					"assocWithOEM": params.assocWithOEM ? params.assocWithOEM : false ,
					"assocWithOEMFile": params.assocWithOEMFile ? params.assocWithOEMFile : null ,                
					"assocWithOEMFileName": params.assocWithOEMFileName ? params.assocWithOEMFileName : "" ,                
					"clients": params.clients ? params.clients : "",
					"establishedDate": ("establishedDate" in params) ? params.establishedDate :'/Date(634334872000+0000)/',
					"products": params.products ? params.products : "",
					"strengths": params.strengths ? params.strengths : "",
					"responseTime": params.responseTime ? params.responseTime : "",
					"oemCompanyName": params.oemCompanyName ? params.oemCompanyName : "",
					"oemKnownSince": params.oemKnownSince? params.oemKnownSince : "",
					"files": [],
					"profileFile": params.profileFile ? params.profileFile : null ,                
					"profileFileName": params.profileFileName ? params.profileFileName : "",
					"workingHours": params.workingHours ? params.workingHours : "",
					"directors": params.directors ? params.directors : "",
					"address": params.address ? params.address : "",
					"subcategories": params.subcategories
				}
			}
			return $http({
				method: 'POST',
				url: domain + 'updateuserinfo',
				headers: { 'Content-Type': 'application/json' },
				encodeURI: true,
				data: params1
			}).then(function (response) {
				if (response && response.data && response.data.errorMessage == "" && response.data.objectID != 0) {
					store.set('sessionid', response.data.sessionID);
					store.set('verified', 0);
					store.set('emailverified', 0);
					store.set('currentUser', response.data.userInfo);
					loginService.setCurrentUser();
					if (response.data.userInfo.isOTPVerified == 1 && response.data.userInfo.credentialsVerified == 1) {
						store.set('verified', 1);
					} if (response.data.userInfo.isEmailOTPVerified == 1) {
						store.set('emailverified', 1);
					}
					if(response.data.userInfo.credentialsVerified == 1){
						store.set('credverified', 1);
					} else {
						store.set('credverified', 0);
					}
					if (response.data.objectID > 0) {

						growlService.growl('User data has updated Successfully!', 'inverse');
						//loginService.getProfileDetails();
						//$state.go('pages.profile.profile-about');
						if(params.ischangePhoneNumber == 1){
							//loginService.logout();
							$state.go('login');
							return responce.data;
						}else{
							$state.reload();
						}
						return "true";
					} else {
						return "false";
					}
				} else if (response && response.data && response.data.errorMessage != "") {
					swal('Error', response.data.errorMessage, 'error');
					//store.set('emailverified', 1);
					return response.data.errorMessage;
					
				} else {
					return "Update failed";
				}
			}, function (result) {
				return "Update failed";
			});
		};

		loginService.checkUserUniqueResult = function (uniquevalue, idtype) {
			var data = {
				"phone": uniquevalue,
				"idtype": idtype
			};
			return $http({
				method: 'POST',
				url: domain + 'checkuserifexists',
				headers: { 'Content-Type': 'application/json' },
				encodeURI: true,
				data: data
			}).then(function (response) {
				return response.data.CheckUserIfExistsResult;
			});
		}

		loginService.forgotpassword = function (forgot) {
			var data = {
				"email": forgot.email,
				"sessionid":''
			};
			//data=forgot.email;
			return $http({
				method: 'POST',
				url: domain + 'forgotpassword',
				headers: { 'Content-Type': 'application/json' },
				encodeURI: true,
				data: data
			}).then(function (response) {
				return response;
			});
			//  $http.post(domain+"forgotassword", data).then( function(res) {
			//   return res;
			// });
		};

		loginService.resendemailotp = function (userid) {
			var data = {
				"userID": userid
			};
			return $http({
				method: 'POST',
				url: domain + 'sendotpforemail',
				headers: { 'Content-Type': 'application/json' },
				encodeURI: true,
				data: data
			}).then(function (response) {
				swal("Done!", "OTP send to your registered Email.", "success");
			});
		};

		loginService.getuseraccess = function (userid, sessionid) {
			return $http({
				method: 'GET',
				url: domain + 'getuseraccess?userid=' + userid + '&sessionid=' + sessionid,
				encodeURI: true,
				headers: { 'Content-Type': 'application/json' }
			}).then(function (response) {
				return response.data;
			}, function (result) {
				//console.log("date error");
			});
		}

		loginService.login = function (user) {
			return $http({
				method: 'POST',
				url: domain + 'loginuser',
				headers: { 'Content-Type': 'application/json' },
				encodeURI: true,
				data: user
			}).then(function (response) {
				if (response && response.data && response.data.errorMessage == "" && response.data.objectID != 0) {
					store.set('sessionid', response.data.sessionID);
					store.set('verified', 0);
					store.set('emailverified', 0);
					store.set('currentUser', response.data.userInfo);
					loginService.setCurrentUser();
					if (response.data.userInfo.isOTPVerified == 1) {
						store.set('verified', 1);
					}
					else if (response.data.userInfo.isOTPVerified == 0) {
						loginService.resendotp(response.data.objectID);                    
					}
					if(response.data.userInfo.credentialsVerified == 1){
						store.set('credverified', 1);
					} else {
						store.set('credverified', 0);
					}
					if (response.data.userInfo.isEmailOTPVerified == 1) {
						store.set('emailverified', 1);
					}
					else if (response.data.userInfo.isEmailOTPVerified == 0) {
						loginService.resendemailotp(response.data.objectID);                    
					}

					$rootScope.entitlements = [];
					loginService.getuseraccess(response.data.objectID, response.data.sessionID)
				.then(function (responseObj) {
					if (responseObj && responseObj.length > 0) {
						$rootScope.entitlements = responseObj;
						store.set('localEntitlement', $rootScope.entitlements);
					}

					$state.go('home');
				})
					
					return response.data;
				} else if (response && response.data && response.data.errorMessage != "") {
					return response.data.errorMessage;
				} else {
					return "Login failed";
				}
			}, function (result) {
				return "Login failed";
			});
		};

		loginService.getUserType = function () {
        	return loginService.getUserObj().userType;
    	};
		return loginService;
	}]);