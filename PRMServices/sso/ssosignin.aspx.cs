﻿using System;
using System.Configuration;
using System.Net;
using System.Web;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.OpenIdConnect;

namespace PRMServices
{
    public partial class ssosignin : System.Web.UI.Page
    {
        private static NLog.Logger logger = NLog.LogManager.GetCurrentClassLogger();
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ServicePointManager.Expect100Continue = true;
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls
                                                       | SecurityProtocolType.Tls11
                                                       | SecurityProtocolType.Tls12
                                                       | SecurityProtocolType.Ssl3;

                HttpContext.Current.GetOwinContext().Authentication.Challenge(
                   //new AuthenticationProperties { RedirectUri = "https://localhost:9401/sso/prmlogin.aspx" },
                   new AuthenticationProperties { RedirectUri = ConfigurationManager.AppSettings["SSO_SIGNIN"] },
                   OpenIdConnectAuthenticationDefaults.AuthenticationType);
            }
            catch(Exception ex)
            {
                logger.Error(ex.Message);
            }            
        }
    }
}