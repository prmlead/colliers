﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNet.SignalR;
using Owin;
using Microsoft.Owin;
using Microsoft.Owin.Cors;
using Newtonsoft.Json;


using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.Web;
using Microsoft.Owin.Extensions;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.Cookies;
using Microsoft.Owin.Security.OpenIdConnect;
using Microsoft.Owin.Security.Notifications;
using Microsoft.IdentityModel.Protocols.OpenIdConnect;
using System.IdentityModel.Claims;
using System.IO;
using Microsoft.AspNet.Identity.Owin;
using System.Web.UI.HtmlControls;
using System.Net;

namespace PRMServices
{
    public partial class ssologin : System.Web.UI.Page
    {

        public string email = string.Empty;
        public string loginPage = string.Empty;
        private static NLog.Logger logger = NLog.LogManager.GetCurrentClassLogger();
        private bool isAuthenticated = false;
        protected void Page_Load(object sender, EventArgs e)
        {
            string errorMessage = "You don't have authorization for accessing PRM360. Please contact your administrator.";
            HtmlGenericControl messageControl = FindControl("SSOMessage") as HtmlGenericControl;
            if (!Request.IsAuthenticated)
            {
                
                messageControl.InnerText = errorMessage;
                logger.Info("NOT AUTH BLOCK");
                try
                {
                    //HttpContext.Current.GetOwinContext().Authentication.Challenge(
                    //   new AuthenticationProperties { RedirectUri = "/" },
                    //   OpenIdConnectAuthenticationDefaults.AuthenticationType);
                }
                catch (Exception ex)
                {
                    logger.Error(ex.Message);
                }
            }
            else
            {
                logger.Info("DONE AUTH BLOCK");
                try
                {
                    logger.Info(User.Identity.IsAuthenticated);
                    logger.Info(Request.RequestContext.HttpContext.User.Identity.Name);
                    string email = Request.RequestContext.HttpContext.User.Identity.Name;
                    if (Request.RequestContext.HttpContext.User.Identity.Name.Contains("#"))
                    {
                        email = Request.RequestContext.HttpContext.User.Identity.Name.Split('#')[1];
                    }

                    string cookieEmail = "";
                    if (Request.Cookies["currentuser"] != null)
                    {
                        logger.Info("IN COOKIE BLOCK");
                        cookieEmail = Request.Cookies["currentuser"].Value;
                        if (!string.IsNullOrEmpty(cookieEmail))
                        {
                            logger.Info("IN COOKIE BLOCK1");
                            cookieEmail = HttpUtility.UrlDecode(cookieEmail);
                            logger.Info("IN COOKIE BLOCK2:" + cookieEmail);
                        }

                        Response.Cookies["currentuser"].Expires = DateTime.Now.AddDays(-1);
                    }

                    logger.Info("SSO EMAIL:" + email);
                    logger.Info("COOKIE EMAIL:" + cookieEmail);
                    if (string.IsNullOrWhiteSpace(cookieEmail)) //Need to find good solution
                    {
                        cookieEmail = email;
                    }

                    if (!string.IsNullOrWhiteSpace(email) && !string.IsNullOrWhiteSpace(cookieEmail) && email.Equals(cookieEmail, StringComparison.InvariantCultureIgnoreCase))
                    {
                        PRMServices pRMServices = new PRMServices();
                        var userInfo = pRMServices.GetUserByEmail(email);
                        if (userInfo != null && !string.IsNullOrEmpty(userInfo.UserType) && userInfo.UserType.Equals("CUSTOMER") && !string.IsNullOrEmpty(userInfo.UserID))
                        {
                            var jwtToken = pRMServices.GetJWTToken(userInfo.UserID, userInfo.CompanyId, userInfo.Email, 30);
                            if (!string.IsNullOrEmpty(jwtToken))
                            {
                                string script1 = string.Format("sessionStorage.setItem('ssojwttoken', '{0}');", jwtToken);
                                ClientScript.RegisterClientScriptBlock(this.GetType(), "key", script1, true);
                                HttpCookie userIdCookie = new HttpCookie("ssojwttoken");
                                userIdCookie.Value = jwtToken;
                                userIdCookie.Path = "/";
                                Response.Cookies.Add(userIdCookie);
                                isAuthenticated = true;
                            }
                            else
                            {
                                logger.Info("SSO failed due to internal error in PRM360, JWT TOKEN EMPTY");
                                
                                messageControl.InnerText = errorMessage;
                            }
                        }
                        else
                        {
                            logger.Info($"SSO failed due to user profiles is missing for email in PRM 360: {email}.");
                            
                            messageControl.InnerText = errorMessage;
                        }
                    }
                    else
                    {
                        logger.Info($"EMPTY EMAIL.");                        
                        messageControl.InnerText = errorMessage;
                    }
                }
                catch (Exception ex)
                {
                    
                    messageControl.InnerText = errorMessage;
                    logger.Error(ex.Message);
                }
            }
        }

        public void SignOut()
        {
            //HttpContext.GetOwinContext().Authentication.SignOut(
            //        OpenIdConnectAuthenticationDefaults.AuthenticationType,
            //        CookieAuthenticationDefaults.AuthenticationType);
        }

        protected void Page_LoadComplete(object sender, EventArgs e)
        {
            logger.Error("Page load complete");
            if (isAuthenticated)
            {
                logger.Error($"Page load complete: isAuthenticated: {isAuthenticated}");
                Response.Redirect(ConfigurationManager.AppSettings["WEBSITE_LOGIN_URL"]);
            }
        }
    }
}