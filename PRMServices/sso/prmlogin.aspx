﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="prmlogin.aspx.cs" Inherits="PRMServices.ssologin" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate" />
    <meta http-equiv="Pragma" content="no-cache" />
    <meta http-equiv="Expires" content="-1" />
    <title>PRM360</title>
    <!--<link href="https://fonts.googleapis.com/css?family=Rubik:300,400,500,700,900" rel="stylesheet">-->
    <link href="../prm360.min.CACHE.css" rel="stylesheet" />
    <link href="../js/resources/css/prmCustom.css" rel="stylesheet" />
</head>
<body>

<div>
    <section id="wrapper" class="new-login-register">
        <div class="lg-info-panel" style="font-size:10px">
            <div class="inner-panel">
                <a href="javascript:void(0)" class="p-20 p-l-50 di"><img src="../js/resources/images/logo.png"></a>
                <div class="lg-content" style="font-size:10px">
                    <h2>Purchasing is now simplified with PRM360</h2>
                    <h3>Helping Buyers and Sellers Close the Deal In just 15 minutes</h3>

                    <ul class="list-icons">
                        <li>Effective grading system to build trust among the buyers and sellers</li>
                        <li>Customer Relationship solidified on top of a Reliable Support System</li>
                        <li>Vendor Relationship Management enhanced by continuous Interaction and Communication</li>
                        <li>Fast, Easy & Effective execution, through and through</li>
                    </ul>

                </div>
            </div>
        </div>

        <div class="new-login-box" style="margin-left:40% !important; margin-top: 12% !important; width:750px !important;">
            <div class="white-box">
                <div class="row">
                    <div class="col-md-12 text-center">
                        <br />
                        <h1 class="box-title m-b-0" style="font-weight:bolder; font-size:larger;" runat="server" id="SSOMessage"></h1>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

</body>
</html>
    