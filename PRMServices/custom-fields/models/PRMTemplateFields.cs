﻿using PRM.Core.Common;
using PRM.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace PRMServices.models
{
    [DataContract]
    public class PRMTemplateFields : Entity
    {
        [DataMember]
        [DataNames("TEMPLATE_FIELD_ID")]
        public int TEMPLATE_FIELD_ID { get; set; }

        [DataMember]
        [DataNames("TEMPLATE_ID")]
        public int TEMPLATE_ID { get; set; }

        [DataMember]
        [DataNames("FIELD_NAME")]
        public string FIELD_NAME { get; set; }

        [DataMember]
        [DataNames("FIELD_LABEL")]
        public string FIELD_LABEL { get; set; }

        [DataMember]
        [DataNames("FIELD_PLACEHOLDER")]
        public string FIELD_PLACEHOLDER { get; set; }

        [DataMember]
        [DataNames("FIELD_DATA_TYPE")]
        public string FIELD_DATA_TYPE { get; set; }

        [DataMember]
        [DataNames("FIELD_DEFAULT_VALUE")]
        public string FIELD_DEFAULT_VALUE { get; set; }

        [DataMember]
        [DataNames("FIELD_OPTIONS")]
        public string FIELD_OPTIONS { get; set; }

        [DataMember]
        [DataNames("FIELD_READ_ONLY")]
        public int FIELD_READ_ONLY { get; set; }

        [DataMember]
        [DataNames("FIELD_IS_VENDOR")]
        public int FIELD_IS_VENDOR { get; set; }

        [DataMember]
        [DataNames("FIELD_IS_CUSTOMER")]
        public int FIELD_IS_CUSTOMER { get; set; }
    }
}