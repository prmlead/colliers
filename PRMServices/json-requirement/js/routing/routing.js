﻿prmApp
    .config(["$stateProvider", "$urlRouterProvider", "$httpProvider", "$provide", "domain", "version",
        function ($stateProvider, $urlRouterProvider, $httpProvider, $provide, domain, version) {
            $stateProvider

            .state('json-save-requirement', {
                url: '/json-save-requirement/:REQ_ID',
                templateUrl: 'json-requirement/views/json-save-requirement.html'
            })

        }]);