﻿prmApp
    // =========================================================================
    // COMMON FORMS
    // =========================================================================

    .controller('poManagementCtrl', ["$state", "$stateParams", "$scope", "$rootScope", "auctionsService", "userService", "$http", "$window", "domain",
        "fileReader", "growlService", "$log", "$filter", "ngDialog", "catalogService",
        "PRMPOServices", "PRMCustomFieldService", "$modal",
        function ($state, $stateParams, $scope, $rootScope, auctionsService, userService, $http, $window, domain,
            fileReader, growlService, $log, $filter, ngDialog, catalogService, PRMPOServices, PRMCustomFieldService, $modal) {

            $scope.compId = userService.getUserCompanyId();
            $scope.totalItems = 0;
            $scope.currentPage = 1;
            $scope.itemsPerPage = 10;
            $scope.maxSize = 5;
            $scope.POStats = {
                totalPOs: 0
            };
            $scope.totalItems1 = 0;
            $scope.currentPage1 = 1;
            $scope.itemsPerPage1 = 10;
            $scope.maxSize1 = 5;
            $scope.searchString = '';
            $scope.initialPOPageArray = [];
            $scope.filterValues = [];
            $scope.setPage1 = function (pageNo) {
                $scope.currentPage1 = pageNo;
            };

            $scope.setPage = function (pageNo) {
                $scope.currentPage = pageNo;
                $scope.getpolist($scope.currentPage - 1, $scope.itemsPerPage, $scope.filters.search);
            };

            $scope.poDet = {
                poLevel: true
            };

            $scope.domestic = false;
            $scope.import = false;
            $scope.bonded = false;
            $scope.service = false;

            $scope.filtersList = {
                plantList: [],
                requisitionList: [],
                docTypeList: [],
                vendoridList: [],
                purchaseGroupList: [],
                statusList: []
            };

            $scope.filters = {
                status: '',
                plant: {},
                requisitionerName: {},
                template: {},
                vendorid: 0,
                purcahseGroup: {},
                searchKeyword: ''
            };

            $scope.filteredPOList = [];
            $scope.itemsList = [];
            $scope.downloadExcel = false;
            $scope.POList = [];

            $scope.setFilters = function (currentPage) {
                $scope.POStats.totalPOs = 0;
                if ($scope.poDet.poLevel) {
                    $scope.filteredPOList = $scope.POList;
                    $scope.totalItems = $scope.filteredPOList.length;
                    $scope.getpolist(currentPage, $scope.itemsPerPage, $scope.filters.searchKeyword);
                }
            };

            $scope.getpolist = function (page, pageSize, searchString) {

                var plant, vendorid, purchaseCode, creatorName, template;

                if (_.isEmpty($scope.filters.plant)) {
                    plant = '';
                } else if ($scope.filters.plant && $scope.filters.plant.length > 0) {
                    var plants = _($scope.filters.plant)
                        .filter(item => item.id)
                        .map('id')
                        .value();
                    plant = plants.join(',');
                }


                if (!$scope.filters.vendorid) {
                    vendorid = 0;
                } else {
                    vendorid = $scope.filters.vendorid ;
                }
                
                
                if (_.isEmpty($scope.filters.purcahseGroup)) {
                    purchaseCode = '';
                } else if ($scope.filters.purcahseGroup && $scope.filters.purcahseGroup.length > 0) {
                    var purchaseCodes = _($scope.filters.purcahseGroup)
                        .filter(item => item.id)
                        .map('id')
                        .value();
                    purchaseCode = purchaseCodes.join(',');
                }

                if (_.isEmpty($scope.filters.requisitionerName)) {
                    creatorName = '';
                } else if ($scope.filters.requisitionerName && $scope.filters.requisitionerName.length > 0) {
                    var creators = _($scope.filters.requisitionerName)
                        .filter(item => item.id)
                        .map('id')
                        .value();
                    creatorName = creators.join(',');
                }

                if (_.isEmpty($scope.filters.template)) {
                    template = '';
                } else if ($scope.filters.template && $scope.filters.template.length > 0) {
                    $scope.domestic = false;
                    $scope.import = false;
                    $scope.bonded = false;
                    $scope.service = false;

                    $scope.filters.template.forEach(function (template) {
                        if (template.id === 'ZSDM') {
                            $scope.domestic = true;
                        } else if (template.id === 'ZSIM') {
                            $scope.import = true;
                        } else if (template.id === 'ZSBW') {
                            $scope.bonded = true;
                        } else {
                            $scope.service = true;
                        }

                    });

                    var templates = _($scope.filters.template)
                        .filter(item => item.id)
                        .map('id')
                        .value();
                    template = templates.join(',');

                }
                
                var params = {
                    "compid": userService.getUserCompanyId(),
                    "template": template,
                    "vendorid": vendorid,
                    "status": '',
                    "creator": creatorName,
                    "plant": plant,
                    "purchasecode": purchaseCode,
                    "search": searchString ? searchString : "",
                    "sessionid": userService.getUserToken(),
                    "page": page,
                    "pageSize": pageSize
                };

                $scope.pageSizeTemp = params.page;

                PRMPOServices.getPOGenerateDetails(params)
                    .then(function (response) {
                        if (!$scope.downloadExcel) {
                            $scope.POList = [];
                            $scope.filteredPOList = [];
                            if (response && response.length > 0) {
                                $scope.POList = response;
                                $scope.totalItems = $scope.POList[0].TOTAL_PO_COUNT;
                                $scope.POStats.totalPOs = $scope.totalItems;
                                $scope.filteredPOList = $scope.POList;
                            }
                        } else {
                            if (response && response.length > 0) {
                                $scope.poExcelReport = response;
                                downloadPOExcel();
                            } else {
                                swal("Error!", "No records.", "error");
                                $scope.downloadExcel = false;
                            }
                        }
                    });
            };

            $scope.getpolist(0, $scope.itemsPerPage, $scope.searchString);
            
            $scope.getFilters = function () {
                var params =
                {
                    "compid": $scope.compId
                };

                let plantListTemp = [];
                let purchaseGroupTemp = [];
                let requisitionListTemp = [];
                let vendoridTemp = [{ id: 0, name: 'All' }];
                let docTypeTemp = [];

                PRMPOServices.getFilterValues(params)
                    .then(function (response) {
                        if (response && response.length > 0) {
                            $scope.filterValues = response;
                            if ($scope.filterValues && $scope.filterValues.length > 0) {
                                $scope.filterValues.forEach(function (item, index) {
                                    if (item.TYPE === 'PLANTS') {
                                        plantListTemp.push({ id: +item.NAME, name: item.ID + ' - ' + item.NAME });
                                    } else if (item.TYPE === 'PURCHASE_GROUP') {
                                        purchaseGroupTemp.push({ id: +item.NAME, name: item.ID });
                                    } else if (item.TYPE === 'PO_CREATOR_NAME') {
                                        requisitionListTemp.push({ id: item.ID, name: item.NAME });
                                    } else if (item.TYPE === 'VENDORS') {
                                        vendoridTemp.push({ id: item.ID, name: item.NAME });
                                    } else if (item.TYPE === 'DOC_TYPE') {
                                        docTypeTemp.push({ id: item.ID, name: item.NAME });
                                    }
                                });

                                $scope.filtersList.requisitionList = requisitionListTemp;
                                $scope.filtersList.plantList = plantListTemp;
                                $scope.filtersList.purchaseGroupList = purchaseGroupTemp;
                                $scope.filtersList.vendoridList = vendoridTemp;
                                $scope.filtersList.docTypeList = docTypeTemp;
                            }
                        }
                    });
            };

            $scope.getFilters();
            $scope.getPoItems = function (poDetails) {
                if (poDetails) {
                    var params = {
                        "ponumber": poDetails.PO_NO,
                        "quotno": poDetails.QUOT_NO,
                        "sessionid": userService.getUserToken()
                    };

                    PRMPOServices.getPOItems(params)
                        .then(function (response) {
                            console.log(response);
                            $scope.POItems = response;
                            poDetails.POItems = $scope.POItems;
                        });
                }
            };

            


            //$scope.downloadExcel = false;
            //$scope.GetReport = function () {
            //    $scope.POList = [];
            //    $scope.downloadExcel = true; 
            //    $scope.getpolist(0, 0, $scope.searchString);
            //};

            //function downloadPOExcel() {
            //    alasql('SELECT PR_NUMBER as [PR Number],PLANT as [Plant],PLANT_NAME as [Plant Name], ' +
            //        'PR_CREATOR_NAME as [PR Creator Name], NEW_PR_STATUS as [Status],RELEASE_DATE as [Release Date], ' +
            //        'GMP as [GMP],PURCHASE_GROUP_CODE as [Purchase Group Code], ' +
            //        'PURCHASE_GROUP_NAME as [Purchase Group Name]' +

            //        'INTO XLSX(?, { headers: true, sheetid: "PR_DETAILS", style: "font-size:15px;background:#233646;color:#FFF;" }) FROM ? ',
            //        ["PR Details.xlsx", $scope.prExcelReport]);
            //    $scope.downloadExcel = false;
            //}


        }]);