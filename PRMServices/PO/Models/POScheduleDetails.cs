﻿using PRM.Core.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace PRMServices.Models
{
    [DataContract]
    public class POItem : ResponseAudit

    {
        [DataMember] [DataNames("ROW_ID")] public int ROW_ID { get; set; }
        [DataMember] [DataNames("PO_NUMBER")] public string PO_NUMBER { get; set; }
        [DataMember] [DataNames("PO_AMENDMENT_ID")] public string PO_AMENDMENT_ID { get; set; }
        [DataMember] [DataNames("CUSTOMER_COMP_ID")] public int CUSTOMER_COMP_ID { get; set; }
        [DataMember] [DataNames("CUSTOMER_USER_ID")] public int CUSTOMER_USER_ID { get; set; }
        [DataMember] [DataNames("DELIVERY_DATE")] public DateTime? DELIVERY_DATE { get; set; }
        [DataMember] [DataNames("PO_RECEIPT_DATE")] public DateTime? PO_RECEIPT_DATE { get; set; }
        [DataMember] [DataNames("VENDOR_COMP_ID")] public int VENDOR_COMP_ID { get; set; }
        [DataMember] [DataNames("VENDOR_COMPANY")] public string VENDOR_COMPANY { get; set; }
        [DataMember] [DataNames("VENDOR_ADDRESS")] public string VENDOR_ADDRESS { get; set; }
        [DataMember] [DataNames("PO_STATUS")] public string PO_STATUS { get; set; }
        [DataMember] [DataNames("PO_TYPE")] public string PO_TYPE { get; set; }
        [DataMember] [DataNames("DELIVERY_COMPLETED")] public string DELIVERY_COMPLETED { get; set; }
        [DataMember] [DataNames("PO_CLOSED_DATE")] public DateTime? PO_CLOSED_DATE { get; set; }
        [DataMember] [DataNames("VENDOR_CODE")] public string VENDOR_CODE { get; set; }
        [DataMember] [DataNames("GST_NUMBER")] public string GST_NUMBER { get; set; }
        [DataMember] [DataNames("VENDOR_GST_NUMBER")] public string VENDOR_GST_NUMBER { get; set; }
        [DataMember] [DataNames("GST_ADDR")] public string GST_ADDR { get; set; }
        [DataMember] [DataNames("VENDOR_ID")] public int VENDOR_ID { get; set; }
        [DataMember] [DataNames("VENDOR_PRIMARY_EMAIL")] public string VENDOR_PRIMARY_EMAIL { get; set; }
        [DataMember] [DataNames("VENDOR_PRIMARY_PHONE_NUMBER")] public string VENDOR_PRIMARY_PHONE_NUMBER { get; set; }
        [DataMember] [DataNames("PO_CREATOR")] public string PO_CREATOR { get; set; }
        [DataMember] [DataNames("PO_CREATOR_NAME")] public string PO_CREATOR_NAME { get; set; }
        [DataMember] [DataNames("PO_RELEASE_DATE")] public DateTime? PO_RELEASE_DATE { get; set; }
        [DataMember] [DataNames("TOTAL_VALUE")] public decimal TOTAL_VALUE { get; set; }
        [DataMember] [DataNames("PO_DATE")] public DateTime? PO_DATE { get; set; }
        [DataMember] [DataNames("VENDOR_EXPECTED_DELIVERY_DATE")] public DateTime? VENDOR_EXPECTED_DELIVERY_DATE { get; set; }
        [DataMember] [DataNames("VENDOR_EXPECTED_DELIVERY_DATE_STRING")] public string VENDOR_EXPECTED_DELIVERY_DATE_STRING { get; set; }
        [DataMember] [DataNames("IS_PO_ACK")] public int IS_PO_ACK { get; set; }
        [DataMember] [DataNames("VENDOR_ACK_STATUS")] public string VENDOR_ACK_STATUS { get; set; }
        [DataMember] [DataNames("VENDOR_ACK_REJECT_COMMENTS")] public string VENDOR_ACK_REJECT_COMMENTS { get; set; }
        [DataMember] [DataNames("EMAIL_SENT_DATE")] public DateTime? EMAIL_SENT_DATE { get; set; }
        [DataMember] [DataNames("PROJECT_ID")] public int PROJECT_ID { get; set; }
        [DataMember] [DataNames("PROJECT_NAME")] public string PROJECT_NAME { get; set; }
        [DataMember] [DataNames("PROJ_ID")] public string PROJ_ID { get; set; }
        [DataMember] [DataNames("PACKAGE_ID")] public string PACKAGE_ID { get; set; }
        [DataMember] [DataNames("PACKAGE_NAME")] public string PACKAGE_NAME { get; set; }
        [DataMember] [DataNames("SUB_PACKAGE_ID")] public int SUB_PACKAGE_ID { get; set; }
        [DataMember] [DataNames("SUB_PACKAGE_NAME")] public string SUB_PACKAGE_NAME { get; set; }
        [DataMember] [DataNames("DESCRIPTION")] public string DESCRIPTION { get; set; }
        [DataMember] [DataNames("PO_DESCRIPTION")] public string PO_DESCRIPTION { get; set; }
        [DataMember] [DataNames("PO_LINE_ITEM")] public string PO_LINE_ITEM { get; set; }
        [DataMember] [DataNames("ACK_QTY")] public decimal ACK_QTY { get; set; }
        [DataMember] [DataNames("ACK_DATE")] public DateTime? ACK_DATE { get; set; }
        [DataMember] [DataNames("UOM")] public string UOM { get; set; }
        [DataMember] [DataNames("ORDER_QTY")] public decimal ORDER_QTY { get; set; }
        [DataMember] [DataNames("ORDER_QTY_TEMP")] public decimal ORDER_QTY_TEMP { get; set; }
        [DataMember] [DataNames("RECEIVED_QTY")] public decimal RECEIVED_QTY { get; set; }
        [DataMember] [DataNames("LAST_RECEIVED_DATE")] public DateTime? LAST_RECEIVED_DATE { get; set; }
        [DataMember] [DataNames("PO_ITEM_CHANGE_DATE")] public DateTime? PO_ITEM_CHANGE_DATE { get; set; }
        [DataMember] [DataNames("REJECTED_QTY")] public decimal REJECTED_QTY { get; set; }
        [DataMember] [DataNames("REMAINING_QTY")] public decimal REMAINING_QTY { get; set; }
        [DataMember] [DataNames("TOTAL_AMOUNT")] public decimal TOTAL_AMOUNT { get; set; }
        [DataMember] [DataNames("UNIT_PRICE")] public decimal UNIT_PRICE { get; set; }
        [DataMember] [DataNames("VENDOR_ACK_COMMENTS")] public string VENDOR_ACK_COMMENTS { get; set; }
        [DataMember] [DataNames("CGST")] public decimal CGST { get; set; }
        [DataMember] [DataNames("SGST")] public decimal SGST { get; set; }
        [DataMember] [DataNames("IGST")] public decimal IGST { get; set; }
        [DataMember] [DataNames("TAX_AMOUNT")] public decimal TAX_AMOUNT { get; set; }
        [DataMember] [DataNames("TOTAL_COUNT")] public int TOTAL_COUNT { get; set; }
        [DataMember] [DataNames("MAP_TERM_ID")] public int MAP_TERM_ID { get; set; }
        [DataMember] [DataNames("PO_PDF_JSON")] public string PO_PDF_JSON { get; set; }
        [DataMember] [DataNames("PAYMENT_TERMS")] public string PAYMENT_TERMS { get; set; }
        [DataMember] [DataNames("COST_CENTER")] public string COST_CENTER { get; set; }
        [DataMember] [DataNames("WF_ID")] public int WF_ID { get; set; }
        [DataMember] [DataNames("IS_PO_AMENDMENT_DONE")] public bool IS_PO_AMENDMENT_DONE { get; set; }
        [DataMember] [DataNames("CLIENT_NAME")] public string CLIENT_NAME { get; set; }
        [DataMember] [DataNames("ADDRESS")] public string ADDRESS { get; set; }
        [DataMember] [DataNames("SPOC_EMAIL")] public string SPOC_EMAIL { get; set; }
        [DataMember] [DataNames("PAN_NUMBER")] public string PAN_NUMBER { get; set; }
        [DataMember] [DataNames("MULTIPLE_ATTACHMENTS")] public string MULTIPLE_ATTACHMENTS { get; set; }
        [DataMember] [DataNames("PO_SCH_ROW_ID")] public int PO_SCH_ROW_ID { get; set; }
        [DataMember] [DataNames("IS_FROM_PO_AMENDMENT")] public bool IS_FROM_PO_AMENDMENT { get; set; }

        [DataMember] [DataNames("PO_AMEND_TAX_AMOUNT")] public decimal PO_AMEND_TAX_AMOUNT { get; set; }
        [DataMember] [DataNames("PO_AMEND_TOTAL_AMOUNT")] public decimal PO_AMEND_TOTAL_AMOUNT { get; set; }
        [DataMember] [DataNames("PO_AMEND_OVERALL_AMOUNT")] public decimal PO_AMEND_OVERALL_AMOUNT { get; set; }
        [DataMember] [DataNames("NOTIFY_VENDOR")] public int NOTIFY_VENDOR { get; set; }
        [DataMember(Name = "vendorAttachment")]  public List<FileUpload> VendorAttachment { get; set; }
        [DataMember] [DataNames("CREATED_BY")] public int CREATED_BY { get; set; }
        [DataMember] [DataNames("MODIFIED_BY")] public int MODIFIED_BY { get; set; }
        [DataMember] [DataNames("SESSION_ID")] public string SESSION_ID { get; set; }

        [DataMember] [DataNames("PO_VALUE_WORDS")] public string PO_VALUE_WORDS { get; set; }
        [DataMember] [DataNames("CGST_AMOUNT")] public decimal CGST_AMOUNT { get; set; }
        [DataMember] [DataNames("SGST_AMOUNT")] public decimal SGST_AMOUNT { get; set; }
        [DataMember] [DataNames("IGST_AMOUNT")] public decimal IGST_AMOUNT { get; set; }
        [DataMember] [DataNames("PO_VALUE")] public decimal PO_VALUE { get; set; }
        [DataMember] [DataNames("PO_VALUE_WITHOUTTAX")] public decimal PO_VALUE_WITHOUTTAX { get; set; }
        [DataMember] [DataNames("GST_AMOUNT")] public decimal GST_AMOUNT { get; set; }

    }
}